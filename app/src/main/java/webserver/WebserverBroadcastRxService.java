package webserver;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.IBinder;
import android.text.format.Formatter;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import static webserver.GlobalVariables.SP_FingerPrint;
import static webserver.GlobalVariables.TimeandAttendence;
import static webserver.GlobalVariables.mobileWorkFlowConfigure;
import static webserver.GlobalVariables.productVersion;


public class WebserverBroadcastRxService extends Service {
    private static final int BUFFER_SIZE = 512;

    private static final int ENDPOINT_SERVICE_PORT = 34553;
    private static final int LOCALSERVER_SERVICE_PORT = 34552;
    private static final int LOCAL_SERVER_DISCOVERY_SUCESS=354;
    public static boolean isAlive;
    boolean DiscoverySuccess = false;
    private DatagramSocket datagramSocket=null;
    private DatagramSocket Rxsocket=null;
    DatagramSocket socket;
    private String TAG="WebserverBroadcastRxService";
    private SharedPreferences prefs= null;
    private String DeviceMacAddress=null;
    private String remoteMac= null;
    static {
        WebserverBroadcastRxService.isAlive = false;
    }
    public IBinder onBind(Intent paramIntent)
    {
        return null;
    }

    public void onCreate()
    {

        try {
            super.onCreate();
            prefs = getBaseContext().getSharedPreferences(
                    SP_FingerPrint, MODE_PRIVATE);

            WebserverBroadcastRxService.isAlive = true;
            Log.d(TAG, "Discovery Started");

            DeviceMacAddress = GetMacAddress();
            Log.d(TAG,"Device MAC Address is "+DeviceMacAddress);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //Keep a socket open to listen to all the UDP trafic that is destined for this port
                        socket = new DatagramSocket(LOCALSERVER_SERVICE_PORT, InetAddress.getByName("0.0.0.0"));
                        socket.setBroadcast(true);

                        while (true) {
                            Log.d(TAG,"Ready to receive broadcast packets!");

                            //Receive a packet
                            byte[] recvBuf = new byte[15000];
                            DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
                            socket.receive(packet);

                            //Packet received
                            Log.d(TAG,"Discovery packet received from: " + packet.getAddress().getHostAddress());
                            Log.d(TAG,"Discovery packet received to: " + packet.getAddress().getLocalHost());
                            Log.d(TAG,"Packet received; data: " + new String(packet.getData()));

                            //See if the packet holds the right command (message)
                            String message = new String(packet.getData()).trim();
                            Log.d(TAG,"Received message "+message);
                            remoteMac = null;
                            if (message.contains("Signage: Hello LocalServer")) {
                                remoteMac = message.substring(message.lastIndexOf('/')+1);

                                try {
                                    //if (mobileWorkFlowConfigure)
                                    {
                                        if (remoteMac.contains("-")) {
                                            //this has Mac Address and IP Address
                                            String arr1[] = remoteMac.split("-");
                                            remoteMac = arr1[0];

                                            int workflowID = Integer.valueOf(arr1[1]);
                                            Log.d(TAG, "Mac Addr is " + remoteMac);
                                            Log.d(TAG, "Work Flow ID " + workflowID);
                                            int currworkflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

                                            Log.d(TAG,"Current Work Flow ID  "+currworkflowID);

                                            if(currworkflowID != workflowID) {
                                                Log.d(TAG,"Current Workflow and Rx work flow not matching save it ");

                                                SharedPreferences.Editor editor = prefs.edit();
                                                editor.putInt(GlobalVariables.workflowStatus, workflowID);
                                                editor.commit();
                                                SetWorkFlow(workflowID);
                                            }

                                        }
                                    }
                                } catch (Exception obj) {

                                }

                            }
                            Log.d(TAG,"remoteMac = "+remoteMac);

                            //final InetAddress ipAddress = getIpAddress();
                            String retVal=null;
						/*	if (isWifiConnected()) {
								retVal = GetDeviceipWiFiData();
							} else
							if (isEthernetC+36----------------------onnected()) {*/

                            InetAddress destinedIPInet = packet.getAddress().getLocalHost();
                            //String destinedIPInet = packet.getAddress().getHostName();
                            //String destinedIPInet = destinedIPInet; //destinedIPInet.getHostAddress();
                            Log.d(TAG,"Destined IP is "+destinedIPInet);

                            String retreivedIP = prefs.getString("SP_REMOTE_IP",
                                    "");
                            String storedMac = prefs.getString("SP_REMOTE_MAC",
                                    "test");

                            Log.d(TAG,"Stored Mac "+storedMac);

                            String remoteIP  = packet.getAddress().getHostAddress();

                            String currremoteIPAddr = getRemoteIP();

                            Log.d(TAG,"Prev Destined IP is "+currremoteIPAddr+" Remote IP is"+remoteIP);
                            //if (remoteMac != null && (storedMac == null || remoteMac.equals(storedMac))) {
                            if (remoteMac != null && (storedMac.equals("test") || storedMac == null || remoteMac.equals(storedMac))) {
                                if (currremoteIPAddr == null || !currremoteIPAddr.equals(remoteIP)) {

                                    Log.d(TAG, "Saving Remote IP is" + remoteIP+" Remote Mac "+remoteMac);
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putString("SP_REMOTE_IP", remoteIP.trim());
                                    editor.putString("SP_REMOTE_MAC", remoteMac.trim());
                                    editor.commit();
                                }
                                //} else
                                //	Log.d(TAG,"Not Saving Remote IP ");

                                retVal = logLocalIpAddresses();
                                //}
                                Log.d(TAG,"IP Address is "+retVal);

                                Log.d(TAG,"Local Server IP Address is "+retVal+"  DestIP is  "+packet.getAddress().getHostAddress());
                                Log.d(TAG,"Local Server IP Address is "+remoteIP+"  DestIP is  "+packet.getAddress().getHostAddress());
                                String Global_TerminalID = String.valueOf(prefs.getInt(GlobalVariables.SP_TerminalID, -1));
                                Log.d(TAG,"Local Server Termianl ID is "+Global_TerminalID);

                                int workflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

                                Log.d(TAG,"Work Flow ID  "+workflowID);

                                //byte[] bytes = ("Signage: Hello LocalServer"+retVal)
                                byte[] bytes = ("Signage: Hello LocalServer /"+retVal+"-"+DeviceMacAddress+"-"+Global_TerminalID+"-"+workflowID)
                                        .getBytes();


                                //if (message.equals("DISCOVER_FUIFSERVER_REQUEST")) {
                                //	byte[] sendData = "DISCOVER_FUIFSERVER_RESPONSE".getBytes();

                                //Send a response
                                DatagramPacket sendPacket = new DatagramPacket(bytes, bytes.length, packet.getAddress(), ENDPOINT_SERVICE_PORT);
                                socket.send(sendPacket);

                                Log.d(TAG,"Sent packet to: " + sendPacket.getAddress().getHostAddress());
                                Log.d(TAG,"Shuttindown App");
                            } else
                                Log.d(TAG,"Not Sending or Not Saving Remote IP ");

                            try {
                                Thread.sleep(5000);
                            } catch (Exception ojb) {

                            }
                            //stopSelf();
//							}
                        }
                    } catch (Exception  ex) {
                        //Logger.getLogger(DiscoveryThread.class.getName()).log(Level.SEVERE, null, ex);
                        Log.d(TAG,"Got socket exception "+ex.getMessage());
                    }
                }

            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    @Override
    public void onDestroy()
    {
        Log.e(TAG,"Ondestroy");


        super.onDestroy();

        try {
            this.datagramSocket.close();

            this.Rxsocket.close();
        } catch (Exception obj) {

        }

    }

    public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    {
        return IntentService.START_NOT_STICKY;
    }

    private InetAddress getSubnetBroadcastAddr() {
        final InetAddress ipAddress = this.getIpAddress();
        InetAddress broadcast = null;
        try {
            final Iterator<InterfaceAddress> iterator = NetworkInterface
                    .getByInetAddress(ipAddress).getInterfaceAddresses()
                    .iterator();
            while (iterator.hasNext()) {
                broadcast = iterator.next().getBroadcast();
            }
            return broadcast;
        } catch (SocketException ex) {
            ex.printStackTrace();
            Log.d(TAG, "getBroadcast" + ex.getMessage());
            return null;
        }
    }

    private InetAddress getIpAddress() {
        InetAddress inetAddress = null;
        try {
            final Enumeration<NetworkInterface> networkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                final NetworkInterface networkInterface = networkInterfaces
                        .nextElement();
                final Enumeration<InetAddress> inetAddresses = networkInterface
                        .getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    final InetAddress inetAddress2 = inetAddresses
                            .nextElement();
                    Log.d("LocalSerer"," IP Address are "+inetAddress2);
                    if (!inetAddress2.isLoopbackAddress()
                            && (networkInterface.getDisplayName().contains(
                            "wlan0") || networkInterface
                            .getDisplayName().contains("eth0"))) {
                        Log.d("LocalSerer"," This is Wlan0 or eth0 - IP Address are "+inetAddress2);
                        inetAddress = inetAddress2;
                    }
                }
            }
            return inetAddress;
        } catch (SocketException ex) {
            Log.d(TAG, ex.toString());
            return null;
        }
    }

    public static boolean isAlive() {
        return WebserverBroadcastRxService.isAlive;
    }




    /*public void onDestroy() {
        super.onDestroy();
        DiscoverAadServer.isAlive = false;
    } */
    boolean IsServerDiscoverySuccess(){


        return DiscoverySuccess;

    }
    private void ProcessReceivedPacket(String buff, InetAddress DestIPAddr, int Port) {
        int responseCode=0;
        String tokenID=null;
        byte[] bytes;
        DatagramPacket datagramPacket;
        DatagramPacket datagramPacket2;
        BufferedReader bufferedReader;

        Log.e(TAG,"Received Discover Response"+buff);
        DiscoverySuccess =true;
		/*if (buff.contains("Event")) {
			responseCode = Integer.parseInt(buff.substring(buff.indexOf("<Event>")+7,buff.indexOf("</Event")));
			tokenID = buff.substring(buff.indexOf("<ID>")+4,buff.indexOf("</ID"));
		}
		if (responseCode == LOCAL_SERVER_DISCOVERY_SUCESS) {
			Log.e(TAG,"It is success");
		String ipAddr=DestIPAddr.toString();;

		//stopSelf();
		} else {
			Log.e(TAG,"Discovery Failure");
		}*/

        Log.e(TAG,"Sending Controller: Hello LocalServer");
        try {
            //final InetAddress ipAddress = this.getIpAddress();
            String retVal=null;
		/*	if (isWifiConnected()) {
				retVal = GetDeviceipWiFiData();
			} else
			if (isEthernetConnected()) {

				retVal = logLocalIpAddresses();
			}*/
            Log.d(TAG,"IP Address is "+retVal);

            Log.d(TAG,"Local Server IP Address is "+retVal+"  DestIP is  "+DestIPAddr+" Mac "+DeviceMacAddress);
            bytes = ("Signage: Hello LocalServer /"+retVal+"-"+DeviceMacAddress)
                    .getBytes();
            datagramPacket = new DatagramPacket(bytes,
                    bytes.length, DestIPAddr,
                    ENDPOINT_SERVICE_PORT);

            Log.d(TAG, "\nSending packet "
                    + " to Endpoint");
            datagramSocket.send(datagramPacket);
            Log.e(TAG, "Sent the datagram");
        } catch (Exception obj) {
            Log.d(TAG,"Got Sending Pkt exception "+obj.toString());
        }
    }
    public String GetDeviceipWiFiData()
    {

        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        @SuppressWarnings("deprecation")

        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        return ip;

    }

    public static boolean validateIPAddress(final String ip) {
        Log.d("NewSplash","inValidateIPAddress"+ip);
        try {
            String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

            return ip.matches(PATTERN);
        } catch (Exception obj) {
            return false;
        }
    }
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }
    public String logLocalIpAddresses() {
        Enumeration<NetworkInterface> nwis;
        String ipAddr = null;
        try {
            nwis = NetworkInterface.getNetworkInterfaces();
            while (nwis.hasMoreElements()) {

                NetworkInterface ni = nwis.nextElement();
                for (InterfaceAddress ia : ni.getInterfaceAddresses())  {
                    try {
                        if ((ia.getNetworkPrefixLength() != 64) && (ni.getDisplayName().contains("eth") || ni.getDisplayName().contains("wlan"))) {
                            ipAddr = ia.getAddress().toString();

                            //ipAddr = ipAddr.substring(ipAddr.lastIndexOf("/") + 1);
                        }
                        Log.i(TAG, "log LocalIP Addres"  +String.format("%s: %s/%d",
                                ni.getDisplayName(), ia.getAddress(), ia.getNetworkPrefixLength()));
                    } catch (Exception obj) {

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ipAddr;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }

    private String GetMacAddress() {
        String mac="06:5b:21:78:a5";

        if (isEthernetConnected()) {
            mac = readEthernetMacAddress();
        } else {
            //= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            mac = wifiInf.getMacAddress();

            if (mac.contains("02:00:00:00:00:00"))
                mac = getMacAddr();
        }
        Log.d(TAG,"Mac Address is "+mac);
        return mac;
    }
    private String readEthernetMacAddress()
    {
        String macAddr = null;

        try {
            File myFile = new File("/sys/class/net/eth0/address");
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(
                    fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            macAddr = aBuffer.toString().trim();
            myReader.close();
        } catch (Exception ob) {
            return null;
        }
        //	Toast.makeText(getBaseContext(), "Done reading SD 'mysdfile.txt'",
        //		Toast.LENGTH_SHORT).show();
        return macAddr;
    }
    private String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            Log.d("NewSplash","Not able to get mac address");
        }
        return "02:00:00:00:00:00";
    }

    private void SetWorkFlow(int workflow) {
        Log.d(TAG,"Current work flow is "+workflow);

        if (productVersion == 1) {
            GlobalVariables.WorkFlowVal=1;
            Log.d(TAG,"Work Flow is only Access Control - V0 ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=0;
            GlobalVariables.WebServer=false; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            TimeandAttendence = false; //true;
            GlobalVariables.adminVerification=true;
            GlobalVariables.RestrictedAccess = false;

        } else
        if (workflow==1) {
            GlobalVariables.WorkFlowVal=1;
            Log.d(TAG,"Work Flow is only Access Control ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.adminVerification=true;
            GlobalVariables.RestrictedAccess =false;
        } else
        if (workflow ==2) {

            GlobalVariables.WorkFlowVal=2;
            Log.d(TAG,"Work Flow is Supervisiorory Enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess =false;
            GlobalVariables.adminVerification=true;

        } else

        if (workflow ==3) {

            GlobalVariables.WorkFlowVal=3;
            Log.d(TAG,"Work Flow is Normal enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess =false;
            GlobalVariables.adminVerification=false;

        } else
        if (workflow ==4) {

            GlobalVariables.WorkFlowVal=4;
            Log.d(TAG,"Work Flow is for Time and Attendence");

            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = true; //true;
            GlobalVariables.RestrictedAccess =false;
        } else
        if (workflow ==5) {

            GlobalVariables.WorkFlowVal=5;
            Log.d(TAG,"Work Flow is for Restricted Access");

            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = true; //true;
            GlobalVariables.TimeandAttendence =false;
        }
        else
            Log.d(TAG,"Work Flow not supported "+workflow);
    }
}
