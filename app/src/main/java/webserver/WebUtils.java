package webserver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.android.volley.VolleyError;

import lumidigm.network.IResult;
import lumidigm.services.VolleyService;

import static android.content.Context.MODE_PRIVATE;
import static com.android.volley.Request.Method.GET;

public class WebUtils{
    private String TAG="WebUtils";
    private SharedPreferences prefs=null;

    public boolean SendingMessageWithStateWeb(Context Appctx, String status, String personID, String state) {
        Log.d(TAG,"in SendMessagewith web SendMessage "+status);

        String requestType="120";
        prefs = Appctx.getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);


       // if (GlobalVariables.MobileDevice)
         //   return false;


        if (GlobalVariables.AppSupport != 1)
            return false;

  /*      if (!isEthernetConnected()) {
            Log.d(TAG,"SendingMEssageWithStats Not sending as it is Wifi");
        }*/

        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
     /*   if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }
*/

        Log.d(TAG,"Getting IP Address");
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+state;
        //   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();

        if (personID != null)
            url="http://"+remoteIP+":8080/?username="+status+"&state="+state+"&personID="+personID;


        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: 123"+requestType+" Resoibse us"+response);

            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: 123 "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    //String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, Appctx).tokenBase(GET, url, null, requestType);
        return true;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);
        return remoteIPAddr;
    }
}
