package webserver;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
//import android.widget.Toast;


public class USBCommands  {

	private String TAG="USBCommands";

	public void executeADBCommands(Context Appctx,String action, String personID, String state ) {

		Process process = null;
		DataOutputStream os = null;

		//If not USB - Come out
		if (GlobalVariables.AppSupport !=2) {
			Log.d(TAG,"It is Not USB COming out");
			return;
		}
		try {
			Log.d(TAG,"Going to Excecute ADB Commands ");
			process = Runtime.getRuntime().exec("su");
			os = new DataOutputStream(process.getOutputStream());


			if (action == null) {
				//os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell\n");
				//os.writeBytes("adb -s AINBLJFI4PEQTOI7 pull /mnt/sdcard/status.txt /mnt/mystatus.txt \n");
				os.writeBytes("adb -s KFFI8PQSD6EQNRYT pull /mnt/sdcard/status.txt /mnt/mystatus.txt \n");

			} else {
				Log.d(TAG,"ADB state is "+action+"  State is "+state);
				if (state != null) {
				//	os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state+"  \n");
					os.writeBytes("adb -s KFFI8PQSD6EQNRYT shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state+"  \n");

					//.activites.processmain
					Log.d(TAG, "It is adb -s KFFI8PQSD6EQNRYT shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state);
				} else {
					//os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action+"  \n");

					os.writeBytes("adb -s KFFI8PQSD6EQNRYT shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action+"  \n");
					//			Log.d(TAG,"adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action);
				}
			}


			os.writeBytes("exit\n");
			//os.flush();
			process.waitFor();
		} catch (Exception e) {
			Toast.makeText(Appctx, "Error: " + e.getMessage(),
					Toast.LENGTH_SHORT).show();
		} finally {
			try {
				//     if (os != null) {
				//           os.close();
				//   }
				process.destroy();
			} catch (Exception e) {
				Toast.makeText(Appctx,
						"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		}
		// TODO Auto-generated method stub

	}
	public void SaveDeviceStatustoRemoteDevice(String action, String state) {

		Log.d(TAG,"In SaveDeviceStatustoRemoteDevice"+action +"  "+state);
		try {

			File myFile = new File(Environment.getExternalStorageDirectory()
					+ File.separator + "mydevicestatus.txt");

			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

			if (state != null)
				myOutWriter.append(action+";"+state);
			else
				myOutWriter.append(action);

			myOutWriter.close();
			fOut.close();
		} catch (Exception obj ) {

		}
	}
	public void RemoveRemoteDeviceStatus() {
		String verVal = null;
		String REMOTE_DEVICE_STATUS_FILE = "mydevicestatus.txt";

		try {
			File myFile = new File(Environment.getExternalStorageDirectory()
					+ File.separator + REMOTE_DEVICE_STATUS_FILE);
			//File myFile = new File("/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE); //Both are same

			Log.d(TAG, "File Checking is " + "/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE);

			if (myFile.exists()) {
				myFile.delete();
			}
		} catch (Exception obj) {

		}
	}
}
