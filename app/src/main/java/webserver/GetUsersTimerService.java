package webserver;

		import java.util.List;
		import android.app.ActivityManager;
		import android.app.ActivityManager.RunningTaskInfo;
		import android.app.AlarmManager;
		import android.app.PendingIntent;
		import android.app.Service;
		import android.content.ComponentName;
		import android.content.Context;
		import android.content.Intent;
		import android.content.SharedPreferences;
		import android.os.Build;
		import android.os.Handler;
		import android.os.IBinder;
		import android.os.StrictMode;
		import android.util.Log;

		import com.android.volley.RequestQueue;
		import com.android.volley.toolbox.Volley;

		import database.AppDatabase;
		import lumidigm.utils.DisplayStats;
		import webserver.GlobalVariables;

public class GetUsersTimerService extends Service {

	static Context context;

	public static Handler mHandler = new Handler();
	SharedPreferences prefs;

	AlarmManager alarmManager;
	PendingIntent pendingIntent;

	static long lastSystemTime = 0;

	AppDatabase appDatabase;
	String TAG="GetUsersTimerService";
	static boolean netAvailablilityStatus = false;
	public static RequestQueue requestQueue=null;


	@Override
	public void onCreate() {
		Log.d(TAG, " Started - on Create");
		super.onCreate();
		// testPostOnlineCount = 0;

	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {

		netAvailablilityStatus = false;
		super.onDestroy();
		mHandler.removeCallbacksAndMessages(null);

	}

	// Runs a thread in background - check schedule updates.
	private void Repeate() {

		new Thread(new Runnable() {

			@Override
			public void run() {

				while (true) {
					try {

						final long diff = (System.currentTimeMillis() - lastSystemTime) / 1000;

						Log.d(TAG,
								"initial   Online diff is " + diff);
						// SetHealthyStatusPostScreenOnline();
						 if (diff > 9)
						{
							// lastSystemTime = System.currentTimeMillis();

							mHandler.post(new Runnable() {

								@Override
								public void run() {

									// long diff = (System.currentTimeMillis() -
									// lastSystemTime)/1000;

									Log.d(TAG,
											"  Online diff is " + diff);
									// SetHealthyStatusPostScreenOnline();
									//if (diff > 9)
									{

										lastSystemTime = System
												.currentTimeMillis();

									//	ActivityManager am = (ActivityManager) getBaseContext()
									//			.getSystemService(
									//					Context.ACTIVITY_SERVICE);
									//	List<RunningTaskInfo> tasks = am
									//			.getRunningTasks(1);
										if (isNetAlive()) {
											if (netAvailablilityStatus) {
												Log.d(TAG, "send Queured Reports");
												SendDisplayStatsAPI();
												//Every 6 hours starting the DisplayStats service
											} else
												Log.d(TAG, " Not sending queued Reports - Net is Offline ");
										} else {
											Log.d(TAG,
													"Net is not there ");
										}
										//				runningCount++;
									}
								}

							});

						}
						//Integer timeout = 30 * 1000;
						Integer timeout = 5 * 1000;

						Thread.sleep(timeout);

					} catch(Exception e){

					}
				}
				//	} // end while

			}
		}).start();
	}

	@Override
	public void onStart(Intent intent, int startId) {

		Log.d(TAG, "On Start");
		super.onStart(intent, startId);
		if (Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		context = getBaseContext();
		appDatabase = AppDatabase.getAppDatabase(getApplicationContext());
		prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
				MODE_PRIVATE);

		netAvailablilityStatus=true;
		Repeate();
	}


	@Override
	public boolean onUnbind(Intent intent) {

		return super.onUnbind(intent);
	}

	public boolean isNetAlive() {

		/*
		 * Issue of Wifi Connectiving and getting
		 * getSystemService(CONNECTIVITY_SERVICE) is returning false for
		 * Columbasia Hebbal devices Chand: 15 March 2017
		 */

		return true;

		// Chand - uncomment later

		/*
		 * ConnectivityManager connMgr; NetworkInfo networkInfo = null; try {
		 * connMgr = (ConnectivityManager)
		 * getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
		 * networkInfo = connMgr.getActiveNetworkInfo(); } catch (Exception e) {
		 * Log.d("PostScreenOnline","isNetAlive - connectiving Mgr exception ");
		 * } if (networkInfo != null && networkInfo.isConnected()) return true;
		 * else return false;
		 */
	}
	private void SendDisplayStatsAPI() {

		long lastConfigupdate = prefs.getLong(GlobalVariables.last_Config_SP,0);
		String lastConfigTImeUpdate = prefs.getString(GlobalVariables.last_Config_DATE_SP,null);

		Log.d(TAG,"SendDisplayStatsAPI ");
		if (requestQueue == null)
			requestQueue= Volley.newRequestQueue(getApplicationContext());
		else
			Log.d(TAG,"requestQueue is not empty");

		DisplayStats.sendDeviceStatusInfo(getApplicationContext(),lastConfigupdate,lastConfigTImeUpdate,prefs,appDatabase);
	}
}
