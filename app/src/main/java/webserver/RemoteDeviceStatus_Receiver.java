package webserver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.luxand.facerecognition.FaceFailureActivity;
import com.luxand.facerecognition.FaceSuccessActivity;
import com.luxand.facerecognition.MainActivity;

import org.json.JSONObject;

import java.io.File;

import belgiumeida.CardDataVariables;
import lumidigm.HomeActivity;
import lumidigm.captureandmatch.activites.FingerScannActivity;

import static android.content.Context.MODE_PRIVATE;
/*
am broadcast -n  "lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver" --es action "finger" --es state "none"
 */
public class RemoteDeviceStatus_Receiver extends BroadcastReceiver {

    final String TAG = "DeviceStatusBroadcastRx";
    public CardDataVariables cardData;
    public RemoteDeviceStatus_Receiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "Remote Device Receiver ");
        try {
            // If it is visible then trigger the task else do nothing
                    //String currAction = "finger"; //face,card,finger,audio
                    //String state = null; //complete,
            if (intent != null) {
                String currAction = intent.getStringExtra("action");
                String state = intent.getStringExtra("state");
                Log.d(TAG, " Broadcast Receiver currAction " + currAction + "  State is " + state);
                Toast.makeText(context, "My Broadcst Receiver :::" + currAction + " state " + state, Toast.LENGTH_SHORT).show();
                RemoveRemoteDeviceStatus();
              //  sendUpdatedData(context, currAction, state);
                String username = currAction;
                String currState = state;
                String personID=null;

                Log.d(TAG,"User Name is "+username+"Curr State is "+currState);

                if (GlobalVariables.MobileDevice == true) {
                    //These are message for the Mobile Device
                    Log.d(TAG, "Request for Mobile Device");
                    if (username.equals("fingersuccess")) {
                        Log.d("WebServer", "Got Finger Success");
                        Intent intent2 = new Intent(context, FaceSuccessActivity.class);//SuccessActivity
                        intent2.putExtra("case", "success");
                        intent2.putExtra("FromWeb", true);

                      //  intent.putExtra("personName",currState);
                        Log.d(TAG,"Check IN status is "+personID);
                        if (personID != null)
                            intent.putExtra("personID",personID);
                        Log.d(TAG,"Person name is "+currState);
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */

                        context.startActivity(intent);
                    } else if (username.equals("fingerfailure")) {

                        Log.d("WebServer", "Got Finger Failure");
                        Intent intent2 = new Intent(context, FaceFailureActivity.class);//SuccessActivity
                        intent2.putExtra("case", "failure");
                        intent2.putExtra("FromWeb", true);

				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */


                        context.startActivity(intent2);
                    } else if (username.equals("AdminFingerSuccess")) {
                        Log.d(TAG,"Got Admin Finger Success");
                        sendBroadCast(context,"lumidigm.captureandmatch.mobile.fingerverified","AdminFingerSuccess",currState);
                    } else if (username.equals("AdminFingerFailure")) {
                        Log.d(TAG,"Got Admin Finger Failure");
                        sendBroadCast(context,"lumidigm.captureandmatch.mobile.fingerverified","AdminFingerFailure",currState);
                    } else if (username.equals("UserFingerTemplateResponse")) {
                        Log.d(TAG,"Got UserFingerTemplateResponse");
                        sendBroadCast(context,"lumidigm.captureandmatch.mobile.fingerenroll","UserFingerTemplateResponse",currState);
                    }else if (username.equals("UserFingerTemplateVerifyResponse")) {
                        Log.d(TAG,"Got UserFingerTemplateVerifyResponse");
                        sendBroadCast(context,"lumidigm.captureandmatch.mobile.verifyenroll","UserFingerTemplateVerifyResponse",currState);
                    }

                }  else
                if (GlobalVariables.MobileDevice == false) {
                    //Log.d()
                    //Messages for the Non Mobile devices
                    Log.d(TAG,"Request for non Mobile Device");
                    if (username.equals("AdminFingerVerification")) {
                        Log.d(TAG,"Got Admin Finger Verification Initiate ");
                        sendBroadCast(context,"lumidigm.captureandmatch.activites.processmain","AdminFingerVerification",currState);
                    } else
                    if (username.equals("UserFingerAddTemplate")) {
                        Log.d(TAG,"Got UserFingerAddTemplate Initiate UserID is "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.enroll","UserFingerAddTemplate",currState);
                    } else

                    if (username.equals("GoToEnroll")) {
                        Log.d(TAG,"Got EnrollTypeSelect Initiate UserID is "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.processmain","GoToEnroll",currState);
                    } else
                    if (username.equals("EnrollTypeSelect")) {
                        Log.d(TAG,"Got EnrollTypeSelect Initiate UserID is "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.processmain","EnrollTypeSelect",currState);
                    } else
                    if (username.equals("UserFingerComeOut")) {
                        Log.d(TAG,"Got UserFingerComeOut "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.enroll","UserFingerComeOut",currState);
                    } else

                    if (username.equals("Glow_Led")) {
                        Log.d(TAG,"Got Glow_Led "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.processmain","Glow_Led",currState);
                    }
                    else if (username.equals("Stop_Led")) {
                        Log.d(TAG,"Got Stop_Led "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.processmain","Stop_Led",currState);
                    }
                    else if (username.equals("quit")) {
                        Log.d(TAG,"Got quit "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.processmain","quit",currState);
                    }
                    else if (username.equals("restart")) {
                        Log.d(TAG,"Got restart "+currState);

                        sendBroadCast(context,"lumidigm.captureandmatch.activites.processmain","restart",currState);
                    }
                }


            } else
                Toast.makeText(context, "My Broadcst Receiver Intent is null" , Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context,"My Broadcst Receiver goot crashed" , Toast.LENGTH_SHORT).show();
            Log.d(TAG,"Got Crashed while broadcast receiver"+e.getMessage());
            //sendUpdatedData(context,"finger",null);
        }
    }

    public void sendUpdatedData(Context context, String username, String currState) {
        Log.d(TAG,"Got the SendUpdateData");
        //Intent intent =  new Intent();
        //intent.setClassName("com.test","com.test.Activity");
        //context.startActivity(intent);
        Toast.makeText(context, "My Broadcst Receiver sendUpdateData" + username + " state " + currState, Toast.LENGTH_SHORT).show();
        Log.d("WebServer","Current Message is "+username+" State is"+currState);
        if (username.equals("audio")) {
            Log.d("HomeActivity","Starting Audio Main Activity");
        /*    Intent intent= new Intent(context, audio.MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            Bundle mBundle = new Bundle();
            if (currState != null)
                mBundle.putString("CurrState",currState);

            mBundle.putBoolean("FromWeb",true);
            intent.putExtras(mBundle);
            context.startActivity(intent);
*/
        }
        if (username.equals("finger")) {
            Log.d("Boadcast Receiver","Starting Finger Activity");
            Intent intent= new Intent(context, FingerScannActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle mBundle = new Bundle();
            if (currState != null)
                mBundle.putString("CurrState",currState);
            else {
                GlobalVariables.tokenID=124;
                GlobalVariables.reqStatus= false;

                GlobalVariables.reqID=1; //For FingerPrint
            }
            mBundle.putBoolean("FromWeb",true);
            intent.putExtras(mBundle);
            context.startActivity(intent);

        }
        if (username.equals("face")) {
            Log.d("HomeActivity","Starting Face Activity");
            Intent intent= new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle mBundle = new Bundle();

            if (currState != null)
                mBundle.putString("CurrState",currState);

            mBundle.putBoolean("FromWeb",true);
            intent.putExtras(mBundle);
            context.startActivity(intent);

        }
        if (username.equals("home")) {
            Log.d("HomeActivity","Starting Home Activity");
            Intent intent= new Intent(context, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean("FromWeb",true);
            intent.putExtras(mBundle);
            context.startActivity(intent);
        } else
        if (username.equals("fingerresponse")) {
            if (GlobalVariables.reqStatus) {
                Log.d("HomeActivity","Finger Prints Ready Sending response");

                //if (tokenID == GlobalVariables.tokenID)
                String currmsg="<body><html>tokenID="+GlobalVariables.tokenID+"" +
                        "\n;Description="+GlobalVariables.personDescription+
                        "\n;Status="+GlobalVariables.personStatus;

                GlobalVariables.personDescription=null;
                GlobalVariables.tokenID=0;
                GlobalVariables.reqStatus= false;
                GlobalVariables.reqID=0; //For FingerPrint


            } else {



            }

        }


    }
    private void RemoveRemoteDeviceStatus() {
        String verVal = null;
        String REMOTE_DEVICE_STATUS_FILE = "mydevicestatus.txt";

        Log.d(TAG,"RemoveRemoteDeviceStatusFile");
        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + REMOTE_DEVICE_STATUS_FILE);
            //File myFile = new File("/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE); //Both are same

            Log.d(TAG, "File Checking is " + Environment.getExternalStorageDirectory()
                    + File.separator + REMOTE_DEVICE_STATUS_FILE);

            if (myFile.exists()) {
                myFile.delete();
            }
        } catch (Exception obj) {

        }
    }
    private void sendBroadCast(final Context context,String Source, String status, String state) {
        Log.e("Main","SendBroadcast"+status);
        try {

            final Intent intent = new Intent(Source);
            intent.putExtra("EVENT", status);
            intent.putExtra("STATE", state);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            Log.d(TAG,"Sending Broadcast event ");
        } catch (Exception oj) {
            Log.d("WebServer","Got Crashed while Sending Broadcast "+oj.getMessage());
        }
    }

    private JSONObject CardDataJson() {
        try {
            String jsonStr = "{\"name\": \"i30\", \"brand\": \"Hyundai\"}";

            String CardData = "{\"FileVersion\": \"+FileVersion+\","+
                    "\"CardNumber\": \""+cardData.CardNumber+"\","+
                    "\"ChipNumber\": \""+cardData.ChipNumber+"\","+
                    "\"ValidityeBegin\": \""+cardData.ValidityeBegin+"\","+
                    "\"ValidityEnd\": \""+cardData.ValidityEnd+"\","+
                    "\"NationalNumber\": \""+cardData.NationalNumber+"\","+
                    "\"CardDeliveryMunspality\": \""+cardData.CardDeliveryMunspality+"\","+
                    "\"Name\": \""+cardData.Name+"\","+
                    "\"Name_2\": \""+cardData.Name_2+"\","+
                    "\"Nationality\": \""+cardData.Nationality+"\","+
                    "\"BirthLoc\": \""+cardData.BirthLoc+"\","+
                    "\"BirthDate\": \""+cardData.BirthDate+"\","+
                    "\"Sex\": \""+cardData.Sex+"\","+
                    "\"nobleCondition\": \""+cardData.NobleCondition+"\","+
                    "\"documentType\": \""+cardData.DocumentType+"\","+
                    "\"specialStatus\": \""+cardData.SpecialStatus+"\","+
                    "\"hashPicture\": \""+cardData.HashPicture+"\","+
                    "\"specialOrganization\": \""+cardData.SpecialOrganization+"\","+
                    "\"MemberofFamilyType\": \""+cardData.MemberofFamilyType+"\","+
                    "\"DateandCountryofProtection\": \""+cardData.DateandCountryofProtection+"\","+
                    "}";

            // convert to json object
            JSONObject json = new JSONObject(jsonStr);

            Log.d(TAG,"JSON Value is "+json.toString());

            return json;
        } catch (Exception obj) {

        }
        return null;
    }
}