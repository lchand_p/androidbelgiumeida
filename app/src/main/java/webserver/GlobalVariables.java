package webserver;

public class GlobalVariables {

	/*-----------VARIABLES FOR SHARED PREFERENCES------------*/
	public static final String SP_DIGIT9 = "Digit9";
	public static long tokenID=121;
	public static int reqID=0;
	public static boolean reqStatus=false;
	public static String personDescription = null;
	public static String personStatus=null;

	public static String currAction=null;
	public static String curState=null;

	public static boolean FirstUser=true;
	public static String loggedInUser=null;

	//Keep it for FingerPrint Testing
	//public static boolean NoFingerPrintSupport=true;

	//
	//Parameters for Frost

	//For Sentinel
	/*
		TerminalStatus - TV
		VisitorGuardStation
		Start/Exit Visitor
		Block Guard Inside Block(v2) & Outside Block(v2)
			* Forward path & reverse path
		Booth Guard Inside Block(v2) & Outside Block(v2)
			*Forward Path & Reverse Path
	 */

	//For Forst
	/*
		Access Control
		Time & Attendance
		Restricted Access
	 */

    public static boolean MobileDevice = true; //true;//false; //true; //false;//it has no Network
    public static boolean FaceTesting = true;//false; //true;
    public static boolean FaceandFinger=MobileDevice;
    public static boolean FaceICA=false;
    public  static boolean FaceBelgium=true; //true;
    public static boolean FaceBelgiumTest=true;
    public static int CardReadStatus=0; //0-idle, 1-inpgoress, 2 -success for face,3 - success for carddata, 4 - failed
    public static boolean NoFingerPrintSupport=false;
    public static boolean PaxtonSupport=false;

	public static boolean FaceEnrollmentStatus=true;
	public static boolean FaceContinousDetection=true;
	public static int MaxFaceDetectionCount=5;
	public static int currFaceDetectionCount=0;


/*
	public static boolean MobileDevice = true; //true;//false; //true; //false;//it has no Network
	public static boolean FaceTesting = true;//false; //true;
	public static boolean FaceandFinger=MobileDevice;
	public static boolean FaceICA=false;
	public static boolean NoFingerPrintSupport=true;
	public static boolean PaxtonSupport=true;
*/


 /*
	public static boolean MobileDevice = true; //true;//false; //true; //false;//it has no Network
	public static boolean FaceTesting = true;//false; //true;
	public static boolean FaceandFinger=MobileDevice;
	public static boolean FaceICA=true;
	//Keep it for FingerPrint Testing
	public static boolean NoFingerPrintSupport=true;

	*/
	//Frost -1 1; Sentinel - 2
	public static int selectedProduct=1; //1
	public static boolean M21Support=false; //true;
	public static boolean M320Support=false; //true;
	public static boolean ProductV4Support=true; //false;

//below no need
/*	public static boolean MobileDevice = true; //true; //false;//it has no Network
	public static boolean FaceandFinger=true;
	//Frost -1 1; Sentinel - 2
	public static int selectedProduct=1; //1
	public static boolean M21Support=true; //true;
	public static boolean M320Support=false; //true;
	public static boolean ProductV4Support=false; //false;*/
	//For Web Server
	//0 for Normal, 1 for WebSupport, 2 for USB

	public static int productVersion=2; // V0 - 1, V2 - 2
	public static boolean ledSupport=false; //
	public static int AppSupport=1; //For V0 it is 0, for V2 it is 1
	public static boolean faceTab=MobileDevice;//For Mobile
	public static int vcomdatabaseSupport=0;
	//Frost -1 1; Sentinel - 2
 	//For Web Server
	//0 for Normal, 1 for WebSupport, 2 for USB
 	public static int USBExecute=0;
	public static boolean WebServer=false; //true; //true; //For V0 it is false, for V2 it is true
	public static boolean mobileWorkFlowConfigure=true;
	public static int WorkFlowVal=1; //0-validate, 1 - supervisor enroll, 2 - normal enroll 3 - time and attendence

//above for Frost
	// end testing chand

//Below for Sentinel


/*
	public static boolean MobileDevice = false; //true for frost; //false for sentinel;//it has no Network
	public static boolean faceTab=MobileDevice;//For Mobile
	public static int vcomdatabaseSupport=0;

	public static boolean FaceandFinger=false; //true for frost - false for sentinel
	//Frost -1 1; Sentinel - 2
	public static int selectedProduct=2; //1

	public static boolean M21Support=false;
	public static boolean M320Support=false; //true;
	public static boolean ProductV4Support=false; //true for Frost for V4
	//For Web Server
	//0 for Normal, 1 for WebSupport, 2 for USB
	public static int productVersion=2; // V0 - 1, V2 - 2
	public static boolean ledSupport=false; //
	public static int USBExecute=0;
	public static int AppSupport=1; //For V0 it is 0, for V2 it is 1
	public static boolean WebServer=false; //true; //true; //For V0 it is false, for V2 it is true
	public static boolean mobileWorkFlowConfigure=true;
	public static int WorkFlowVal=1; //0-validate, 1 - supervisor enroll, 2 - normal enroll 3 - time and attendence
*/


	//4 - Restricted Access
	//6 - TV
	//7 - Terminal - Start / Exit
	//8 - Guard Station - Waiting Area
//Above fro Sentinel

	public static int MaxThresholdVal=30000;
	public static boolean SupportWebFaceDetection=false;//true;
	public static boolean SupportWebFaceDetectonInitated=true;

	public static String personID = null;
	public static String SSOIPAddress=  "http://139.59.10.127"; //"http://13.55.135.106";//"http://139.59.10.127"; //"http://192.168.43.161"; //"http://139.59.10.127"; //192.168.3.7";
	public static String SP_FingerPrint="FaceDetection";
	public static String SP_ServerIP="SP_SSO_SERVERIP";
	public static boolean FaceDetectionCompleteStatus=false;
	//Chand enable or disable for Supervision Enrollment mode
	public static boolean onlyVerification=false;
	//supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
	//above true means - no verification and enrollment only with password
	public static boolean supervisionEnrolmentStatus=true;
	public static boolean supervisorUser=true; //For Mobile - Admin Verification
	public static boolean TimeandAttendence = false;
	public static boolean  RestrictedAccess =false;
	//for second
	public static boolean adminVerification=true; //In Second Admin Verification added

	public static int EnrollCount=0;
	public static String SP_TerminalID="SP_TERMINAL_ID";
	public static int  TerminalID=-1;
	public static int supervisorUserID=-1;
	public static String  UserID_Test="SP_USER_ID";
	public static String last_Config_SP="lastConfigdetails";

	public static String last_Config_DATE_SP="lastConfigdatedetails";
	public static String pending_terminal_config_Update="pendingConfigUpdate";

    public static String pending_users_Config_Update="pendingusersConfigUpdate";
	public static String workflowStatus="workFlowStatus";
	public static String localworkflowStatus="localworkFlowStatus";
	public static int comeOut=0;

	//FOr the first time after Hardreboot - Finger print Module is going off, after first press

	public static boolean TV_Support =false;
	public static boolean Terminal_Support =false;
	public static boolean GuardStation_WaitingArea =false;
	public static boolean Detainee_Checkpoint=false;
	public static boolean Detainee_outsideBlock=false;
	public static boolean  Guard_In_Block=false;
	public static boolean  Escort_BoothGuardStation=false;
	public static boolean  Detainee_Terminal_StartExit=false;
	public static boolean  Escort_TaskList=false;
	public static boolean  FaceDetectionDetaineeIdentificationforV4=false;
	public static boolean WaitingAreaVisitorVerification=false;
	public static String OtherBockIP= "192.168.43.146"; //"192.168.3.71";
	public static boolean sentinelWorkFlowChangeOnUserLogon=false;
	public static boolean BoothGuardStation_Outside=false;
	public static String FirstTimeConfigDownload="firsttimeconfig";
    public static String ManuallyEnrollUsedID="manuallyenrolleduserid";

    //For ICA
	public static String personName=null;
	public static String IDNumber=null;
	public static String  ArabicName=null;
	public static String expiryDate=null;
	public static String Nationality=null;
	public static String Gender=null;
	public static int FaceCardReadCount=0;
	public static String UserID="91.jpg";
	/*
	(Workflowname.equals("tv_support")) { currWorkFlow=6;
    (Workflowname.equals("terminal_startexit")) currWorkFlow=7;
    (Workflowname.equals("guardstation_waitingarea currWorkFlow=8;
    (Workflowname.equals("detainee_checkpoints"))  currWorkFlow=9;
     (Workflowname.equals("detainee_outside_block")) { currWorkFlow=10;
     (Workflowname.equals("guard_in_block")) {  currWorkFlow=11;
     (Workflowname.equals("escort_boothguard_station")) currWorkFlow=12;
       (Workflowname.equals("detainee_terminal_startexit") currWorkFlow=13;
        (Workflowname.equals("escort_task_select")) { currWorkFlow=14;

	 */
	//6 - Terminal - Start / Exit
	//7 - Guard Station - Waiting Area


	/* UserID - 9 , 19, 36, 47
	Role Name
	1. Security
	2. generalstaff
	3. Administrator
	4. kioskuser
	5. security2
	6. security3
	7. TestRole (w1)
	8. operator 0
	9 main horror
	 */
}

