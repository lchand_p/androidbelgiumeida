package webserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.StatFs;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;
//import android.widget.Toast;


public class USBService extends Service {

	static Context context;
	Handler handler;
	Runnable r;
	
	String url = null;
//	public static ContentHandler dBHelper = null;
	public static Handler mHandler2 = new Handler();
	SharedPreferences prefs;

	private long lastSystemTime = 0;
	private String TAG= "USBDevice";

	private boolean currStatus = true;
	private long maxSecSleep=3;
	private long curSecCount=0;
	private String REMOTE_DEVICE_STATUS_FILE = "mydevicestatus.txt";
	private static String prevBuffer=null;
	// static Integer testPostOnlineCount = 0;
	@Override
	public void onCreate() {
		Log.d(TAG, "DisplayStats Started - on Create");
		super.onCreate();
		// testPostOnlineCount = 0;
		try {
		//	if (GlobalVariables.GetCrashAnalyticsStatus != 0)
		//		Fabric.with(this, new Crashlytics());
		} catch (Exception obj) {

		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {

		Log.d(TAG, " USBDevice Started - on Destroy ");
		try {
			
		  handler.removeCallbacksAndMessages(null);
		} catch (Exception obj) {
			
		}

		super.onDestroy();
	}
	private void Repeat() {
		handler = new Handler();
		Log.w("creating handler", ":" + handler);
		r = new Runnable() {
			public void run() {

	//			System.gc();
	
				 long diff = (System.currentTimeMillis() -lastSystemTime)/1000;

					Log.d(TAG,
							"Display Stats Display Stats Online diff is " + diff);

					if (diff > 1) {

						lastSystemTime = System
								.currentTimeMillis();
						/*if (currStatus) {
							currStatus = true;

						}*/
						currStatus = true;
						//if (curSecCount > maxSecSleep) {
						Log.d(TAG,"maxCount is "+maxSecSleep+" Curr Status is "+currStatus+" Curr Sec Cout "+curSecCount+" Cur Action is "+GlobalVariables.currAction);
						//if (currStatus && (curSecCount > maxSecSleep)) {
						if (currStatus) {
							Log.d(TAG,"Going for ADB Exection ");
							curSecCount=0;
							if (GlobalVariables.currAction != null) {
								StartingADB(GlobalVariables.currAction,GlobalVariables.curState);
								GlobalVariables.currAction=null;
								GlobalVariables.curState = null;
							} else
								StartingADB(null, null);
						}
						curSecCount=+2;
					}

				handler.postDelayed(this, (4) * 1000);
				Log.w("entering to postdelay", "postdelay");
			
			}

		};

		handler.postDelayed(r, 200);

	}


	@Override
	public void onStart(Intent intent, int startId) {

		Log.d(TAG, "On Start");
		super.onStart(intent, startId);
		if (Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		context = getBaseContext();

		Repeat();

	}



	private void StartingADB(String action, String state) {
		// TODO Auto-generated method stub
		Log.d(TAG, "StartingADB ");

		// isConnected();

		AsyncTask<String, String, String> verifyDBTaskP1;
		verifyDBTaskP1 = new AsyncTask<String, String, String>() {

			@Override
			protected String doInBackground(String... arg0) {
				String action = arg0[0];
				String state = arg0[1];

				Log.d(TAG,"Starging ADB Params "+action+" state is"+state);
				try {
					executeADBCommands(action,state);
					return "success";
				} catch (Exception ex) {
					//Toast.makeText(getApplicationContext(), "No rooted device", 3000).show();
					ex.printStackTrace();
					return "failure";
				}
			}

			protected void onPostExecute(String result) {
				Log.d(TAG, "ADB COmmands reading completed "+result);
				if (result.equals("success")) {
				 String remoteStatus=	getRemoteDeviceStatus();
				 String Action=remoteStatus;
				 String state = null;

					Log.d(TAG, "Remote Status is " + remoteStatus);
				 if (remoteStatus != null) {


					 try  {
						 String[] splits = remoteStatus.split(";");

						 Log.d(TAG, "splits.size: " + splits.length);
						 if (splits.length >1 )  {
						 	Action = splits[0];
						 	state = splits[1];
						 }
					 } catch (Exception obj) {
					 	Log.d(TAG,"Got Exception while parsing file"+remoteStatus);
					 }

					 //if (state != null)
					 {
					 	//means only Action & state

						 //String currAction = intent.getStringExtra("action");
						 //String state = intent.getStringExtra("state");

						 try {
							 //Intent broadcastedIntent = new Intent();

							 Intent broadcastedIntent=new Intent(USBService.this, RemoteDeviceStatus_Receiver.class);
							 //Bundle mBundle = new Bundle();

                             if (Action != null)
                             	broadcastedIntent.putExtra("action", Action);

							 if (state != null)
							 	broadcastedIntent.putExtra("state", state);

							 //broadcastedIntent.setAction("webserver.RECEIVE_REMOTE_DEVICE_STATUS");
							 sendBroadcast(broadcastedIntent);
							 Log.d(TAG,"Send to Broadcast Receiver");

						 } catch (Exception ts) {
						 	Log.d(TAG,"Got Excepton while returning ts ");
						 }
					 }
				 }
				}
				currStatus = true;
			}

		};
		try {
			verifyDBTaskP1.execute(action, state, null);
		} catch (Exception obj) {

		}
	}
	private void executeADBCommands(String action, String state ) {

		Process process = null;
		DataOutputStream os = null;

		try {
			Log.d(TAG,"Going to Excecute ADB Commands ");
			process = Runtime.getRuntime().exec("su");
			os = new DataOutputStream(process.getOutputStream());


			if (action == null) {
				//os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell\n");
				String remoteFile = Environment.getExternalStorageDirectory()
						+ File.separator + REMOTE_DEVICE_STATUS_FILE;
				String localFile = "/mnt/sdcard/"+REMOTE_DEVICE_STATUS_FILE; //Both are same
		//		Log.d(TAG,"Reading File is adb -s AINBLJFI4PEQTOI7 pull "+localFile+" "+remoteFile+" \n");
				//os.writeBytes("adb -s AINBLJFI4PEQTOI7 pull /mnt/sdcard/status.txt /mnt/mystatus.txt \n");
				//os.writeBytes("adb -s AINBLJFI4PEQTOI7 pull "+localFile+" /mnt/sdcard/mystatus.txt \n");


			//	os.writeBytes("adb -s AINBLJFI4PEQTOI7 pull "+remoteFile+" "+localFile+" \n");
				os.writeBytes("adb -s KFFI8PQSD6EQNRYT pull "+remoteFile+" "+localFile+" \n");
				//Remove the localFile
				//os.writeBytes("adb -s AINBLJFI4PEQTOI7 rm "+remoteFile+" \n");

			} else {
				Log.d(TAG,"ADB state is "+action+"  State is "+state);
				if (state != null) {
					os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state+"  \n");
		//			Log.d(TAG, "It is adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state);
				} else {
					os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action+"  \n");
		//			Log.d(TAG,"adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action);
				}
			}


			os.writeBytes("exit\n");
			//os.flush();
			process.waitFor();
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(),
					Toast.LENGTH_SHORT).show();
		} finally {
			try {
			//	if (os != null) {
					//os.close(); //crashing - so commented
		//		}
				process.destroy();
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(),
						"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		}

		/*Toast.makeText(
				getApplicationContext(),
				"Edit saved and a backup was made at "
						+ Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/build.prop.bak",
				Toast.LENGTH_SHORT).show();
*/
		// TODO Auto-generated method stub

	}

	private String getRemoteDeviceStatus() {
		String verVal=null;

		try {
			//File myFile = new File(Environment.getExternalStorageDirectory()
			//		+ File.separator + REMOTE_DEVICE_STATUS_FILE);
			File myFile = new File("/mnt/sdcard/"+REMOTE_DEVICE_STATUS_FILE); //Both are same

			Log.d(TAG,"File Checking is "+"/mnt/sdcard/"+REMOTE_DEVICE_STATUS_FILE);

			if (myFile.exists()) {
				Log.d(TAG,"File Exists and reading contents");
				FileInputStream fIn = new FileInputStream(myFile);
				BufferedReader myReader = new BufferedReader(new InputStreamReader(
						fIn));
				String aDataRow = "";
				String aBuffer = "";
				while ((aDataRow = myReader.readLine()) != null) {
					aBuffer += aDataRow + "\n";
				}
				verVal = aBuffer.toString().trim();
				Log.d(TAG, "Reading Contents is  " + verVal);
				myReader.close();

				Log.d(TAG,"Deleting the current file after processing");
				myFile.delete();
			} else {
					prevBuffer=null;
					Log.d(TAG,"File is null");
					return null;
			}
		} catch (Exception obj) {
			Log.d(TAG,"Got exception while reading Launcher version file");

			return null;
		}
		if ((prevBuffer != null) && prevBuffer.equals(verVal)) {
			Log.d(TAG,"Pevious buffer is same");
			return null;
		} else
			prevBuffer = verVal;

		return verVal;
	}
}
