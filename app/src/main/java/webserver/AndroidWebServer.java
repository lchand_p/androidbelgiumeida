package webserver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.Map;

import com.luxand.facerecognition.FaceFailureActivity;
import com.luxand.facerecognition.FaceSuccessActivity;
import com.luxand.facerecognition.MainActivity;
//import facerecognition.MainActivity;
//import facerecognition.MainActivity;
import fi.iki.elonen.NanoHTTPD;
import lumidigm.captureandmatch.activites.Enroll;
import lumidigm.captureandmatch.activites.FingerPrintEnrollActivity;
import lumidigm.captureandmatch.activites.FingerPrintEnrollmentInProgress;
import lumidigm.captureandmatch.activites.FingerScannActivity;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.captureandmatch.activites.UserAuthenticationActivity;
import frost.timeandattendance.TerminalFaceCheckInActivity;
//import lumidigm.ftreaderexa.FirstActivity;
//import com.luxand.facerecognition.MainActivity;
/**
 * Created by Mikhael LOPEZ on 14/12/2015.
 */
public class AndroidWebServer extends NanoHTTPD {

    Context appContext=null;
    private String TAG="WebServer";
    public AndroidWebServer(int port, Context curcntx) {
        super(port);
        appContext = curcntx;
    }
    public AndroidWebServer(int port) {
        super(port);
    }

    public AndroidWebServer(String hostname, int port) {
        super(hostname, port);
    }

    @Override
    public Response serve(IHTTPSession session) {
        String msg = "<html><body><h1>Hello server</h1>\n";
        Map<String, String> parms = session.getParms();

        if (parms.get("username") == null) {
            msg += "<form action='?' method='get'>\n  <p>Your name: <input type='text' name='username'></p>\n" + "</form>\n";
        } else {
            String currState=null;
            String username = null;
            String tokenID=null;
            String personID=null;
            String ldapUser = null;
            try {
                username = parms.get("username");
                currState = parms.get("state");
                personID = parms.get("personID");
                ldapUser = parms.get("ldapUser");
                if (ldapUser != null)
                    Log.d("WebServer","LDAP User is "+ldapUser);

                if (personID != null)
                    Log.d("WEBServer","Received PersonID"+personID);
            } catch (Exception obj) {

            }

            try {
                if (personID != null)
                GlobalVariables.personID = personID;
            } catch (Exception obj) {

            }
            try {
                Log.d("WebServer"," Token id taking ");
                tokenID = parms.get("tokenID");
                Log.d("WebServer","User ID is "+tokenID);
            } catch (Exception obj) {

            }
            //String username = parms.get("username");
            msg += "<p>Hello, " + parms.get("username") + "!</p>";

            Log.d("WebServer","Current Message is "+username+" State is"+currState);

            if (GlobalVariables.MobileDevice == true) {
                //These are message for the Mobile Device
                Log.d(TAG, "Request for Mobile Device");
                if (username.equals("fingersuccess")) {
                    Log.d("WebServer", "Got Finger Success");

                      Intent intent = new Intent(appContext, FaceSuccessActivity.class);//SuccessActivity
                    if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3 )
                         intent = new Intent(appContext, TerminalFaceCheckInActivity.class);

                    intent.putExtra("case", "success");
                    intent.putExtra("FromWeb", true);

                    intent.putExtra("personName",currState);

                    if (personID != null)
                        intent.putExtra("personID",personID);
                    Log.d(TAG,"Check IN status is "+personID);
                    Log.d(TAG,"Person name is "+currState);
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */

                    appContext.startActivity(intent);
                } else if (username.equals("fingerfailure")) {

                    Log.d("WebServer", "Got Finger Failure");
                    Intent intent = new Intent(appContext, FaceFailureActivity.class);//SuccessActivity
                    intent.putExtra("case", "failure");
                    intent.putExtra("FromWeb", true);

				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */


                    appContext.startActivity(intent);
                } else if (username.equals("AdminFingerSuccess")) {
                    Log.d(TAG,"Got Admin Finger Success");
                    sendBroadCast(appContext,"lumidigm.captureandmatch.mobile.fingerverified","AdminFingerSuccess",currState);
                } else if (username.equals("AdminFingerFailure")) {
                    Log.d(TAG,"Got Admin Finger Failure");
                    sendBroadCast(appContext,"lumidigm.captureandmatch.mobile.fingerverified","AdminFingerFailure",currState);
                } else if (username.equals("UserFingerTemplateResponse")) {
                    Log.d(TAG,"Got UserFingerTemplateResponse");
                    sendBroadCast(appContext,"lumidigm.captureandmatch.mobile.fingerenroll","UserFingerTemplateResponse",currState);
                }else if (username.equals("UserFingerTemplateVerifyResponse")) {
                    Log.d(TAG,"Got UserFingerTemplateVerifyResponse");
                    sendBroadCast(appContext,"lumidigm.captureandmatch.mobile.verifyenroll","UserFingerTemplateVerifyResponse",currState);
                }


            }  else
            if (GlobalVariables.MobileDevice == false) {
                //Log.d()
                //Messages for the Non Mobile devices
                Log.d(TAG,"Request for non Mobile Device");
                if (username.equals("AdminFingerVerification")) {
                    Log.d(TAG,"Got Admin Finger Verification Initiate ");
                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.processmain","AdminFingerVerification",currState);
                } else
                if (username.equals("UserFingerAddTemplate")) {
                    Log.d(TAG,"Got UserFingerAddTemplate Initiate UserID is "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.enroll","UserFingerAddTemplate",currState);
                } else

                if (username.equals("GoToEnroll")) {
                    Log.d(TAG,"Got EnrollTypeSelect Initiate UserID is "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.processmain","GoToEnroll",currState);
                } else
                if (username.equals("EnrollTypeSelect")) {
                    Log.d(TAG,"Got EnrollTypeSelect Initiate UserID is "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.processmain","EnrollTypeSelect",currState);
                } else
                if (username.equals("UserFingerComeOut")) {
                    Log.d(TAG,"Got UserFingerComeOut "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.enroll","UserFingerComeOut",currState);
                } else

                if (username.equals("Glow_Led")) {
                    Log.d(TAG,"Got Glow_Led "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.processmain","Glow_Led",currState);
                }
                else if (username.equals("Stop_Led")) {
                    Log.d(TAG,"Got Stop_Led "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.processmain","Stop_Led",currState);
                }
                else if (username.equals("quit")) {
                    Log.d(TAG,"Got quit "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.processmain","quit",currState);
                }
                else if (username.equals("restart")) {
                    Log.d(TAG,"Got restart "+currState);

                    sendBroadCast(appContext,"lumidigm.captureandmatch.activites.processmain","restart",currState);
                }
           // }
                       } else
            if (username.equals("card")) {
                Log.d("HomeActivity","Starting Card Activity");
                Intent intent= null; //new Intent(appContext, FirstActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle mBundle = new Bundle();
                if (currState != null)
                    mBundle.putString("CurrState",currState);
                GlobalVariables.FaceDetectionCompleteStatus= false;
                mBundle.putBoolean("FromWeb",true);
                intent.putExtras(mBundle);
                appContext.startActivity(intent);

            }
            if (username.equals("finger")) {
                Log.d("HomeActivity","Starting Finger Activity");
                Intent intent= new Intent(appContext, FingerScannActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle mBundle = new Bundle();
                if (currState != null)
                    mBundle.putString("CurrState",currState);
                else {
                    if (tokenID != null)
                        GlobalVariables.tokenID=124;
                    GlobalVariables.reqStatus= false;

                    GlobalVariables.personDescription=null;
                    GlobalVariables.reqStatus= false;

                }
                mBundle.putBoolean("FromWeb",true);
                intent.putExtras(mBundle);
                appContext.startActivity(intent);

                String retVal="username=admin;password=admin";
                //String retVal="{\"username\"=\"admin\",\"password\"=\"admin\"}";
                return newFixedLengthResponse(retVal);
                //return newFixedLengthResponse("12300");
            }
            if (username.equals("face") && !GlobalVariables.MobileDevice && GlobalVariables.SupportWebFaceDetection
                    && GlobalVariables.SupportWebFaceDetectonInitated) {
                try {
                  //  GlobalVariables.SupportWebFaceDetectonInitated=false;
                    if (currState != null && currState.equals("success")) {

                        //Sending Success & Update the Finger Print Module - to stop
                        GlobalVariables.personDescription="face matching";
                         GlobalVariables.reqStatus = true;

                        Intent intent= new Intent(appContext, UserAuthenticationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle mBundle = new Bundle();
                        //if (currState != null)
                           // mBundle.putString("CurrState","comeOut");
                        //else {
                          //  if (tokenID != null)
                            //    GlobalVariables.tokenID=124;
                           // GlobalVariables.reqStatus= false;

                          //  GlobalVariables.personDescriptio  n=null;
                          //  GlobalVariables.reqStatus= false;

                       // }

                        //mBundle.p("FromWeb",personID);
                        Log.d("WebServer","TokenID "+tokenID);

                        mBundle.putString("User_ID",tokenID);
                        mBundle.putBoolean("FromWeb",true);

                        //fromWeb = getIntent().getBooleanExtra("FromWeb", false);
                        //currState = getIntent().getStringExtra("CurrState");

                        intent.putExtras(mBundle);
                        appContext.startActivity(intent);

  //                     sendBroadCast(appContext,"Success");
                       GlobalVariables.FaceDetectionCompleteStatus = true;
                    } else if (currState != null && currState.equals("failure")) {

                        //Let us Wait till finger print module is success or failure

                        //GlobalVariables.personDescription="face failure";
                        //GlobalVariables.reqStatus = true;
                        //sendBroadCast(appContext,"Success");
                    }
                } catch (Exception obj) {

                }
            } else
		if (username.equals("fingerenroll") && GlobalVariables.SupportWebFaceDetection
                    ) {
                try {
			Log.d("WebServer","It is Web Server - FingerEnrollroll");
                    //GlobalVariables.SupportWebFaceDetectonInitated=false;
                    if (currState != null && currState.equals("dialpad")) {

                        //Sending Success & Update the Finger Print Module - to stop

                        Intent intent= new Intent(appContext, Enroll.class);


                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle mBundle = new Bundle();
                        //if (currState != null)
                        //    mBundle.putString("CurrState","comeOut");
                        mBundle.putBoolean("FromWeb",true);
                        intent.putExtras(mBundle);
                        appContext.startActivity(intent);

                    } else
                    if (currState != null && currState.equals("enrollment")) {

                        //Sending Success & Update the Finger Print Module - to stop

                        Log.d("WebServer","Got Finger Print enrollment");
                        Intent intent= new Intent(appContext, FingerPrintEnrollActivity.class);


                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle mBundle = new Bundle();
                        //if (currState != null)
                        //    mBundle.putString("CurrState","comeOut");
                        mBundle.putBoolean("FromWeb",true);
                        mBundle.putString("userid",tokenID);

                        if (ldapUser != null)
                            mBundle.putString("ldapUser",ldapUser);
                        Log.d("WebServer", " Received UserID "+tokenID+"  ldapUser "+ldapUser);
                        intent.putExtras(mBundle);
                        appContext.startActivity(intent);

                    } else
                    if (currState != null && currState.equals("enrollmentsuccess")) {

                        //Sending Success & Update the Finger Print Module - to stop

                        Log.d("WebServer","Got Finger Print enrollment");
                        Intent intent= new Intent(appContext, FingerPrintEnrollmentInProgress.class);


                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle mBundle = new Bundle();
                        //if (currState != null)
                        //    mBundle.putString("CurrState","comeOut");
                        mBundle.putBoolean("FromWeb",true);
                        mBundle.putString("userid",tokenID);

                        if (ldapUser != null)
                        mBundle.putString("ldapUser",ldapUser);

                        mBundle.putString("EnrolmentStatus","success");

                        Log.d("WebServer","Got Enrollment Success"+tokenID+"  "+ldapUser);
                        intent.putExtras(mBundle);
                        appContext.startActivity(intent);

                    }
                    else if (currState != null && currState.equals("enrollmentfailure")) {

                        //Let us Wait till finger print module is success or failure

                        //GlobalVariables.personDescription="face failure";
                        //GlobalVariables.reqStatus = true;

                        Log.d("WebServer","Got Finger Print enrollment");
                        Intent intent= new Intent(appContext, FingerPrintEnrollmentInProgress.class);


                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        Bundle mBundle = new Bundle();
                        //if (currState != null)
                        //    mBundle.putString("CurrState","comeOut");
                        mBundle.putBoolean("FromWeb",true);
                        mBundle.putString("userid",tokenID);
                        mBundle.putString("EnrolmentStatus","failure");

                        Log.d("WebServer","Got Enrollment Failure");
                        intent.putExtras(mBundle);
                        appContext.startActivity(intent);
                    }
                } catch (Exception obj) {

                }
            } else

            if (username.equals("face")) {
                Log.d("HomeActivity","Starting Face Activity");
                Intent intent= new Intent(appContext, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle mBundle = new Bundle();

                if (currState != null)
                    mBundle.putString("CurrState",currState);

                mBundle.putBoolean("FromWeb",true);
                intent.putExtras(mBundle);
                appContext.startActivity(intent);
                String retVal="username=admin;password=admin";
                return newFixedLengthResponse(retVal);

            }
            if (username.equals("home")) {
                Log.d("HomeActivity","Starting Home Activity");
                //Intent intent= new Intent(appContext, HomeActivity.class);
                Intent intent= new Intent(appContext, Main2Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("FromWeb",true);
                intent.putExtras(mBundle);
                appContext.startActivity(intent);
            } else
            if (username.equals("fingerresponse")) {
                Log.d("HomeActivity","Got Fnger Response "+GlobalVariables.reqStatus);
                if (GlobalVariables.reqStatus) {
                    Log.d("HomeActivity","Finger Prints Ready Sending response"+GlobalVariables.loggedInUser);

                    //if (tokenID == GlobalVariables.tokenID)
                    //String currmsg="<body><html>tokenID="+GlobalVariables.tokenID+"" +
                      //      "\n;Description="+GlobalVariables.personDescription+
                        //    "\n;Status="+GlobalVariables.personStatus;

                    String retVal="Failure";
                    //retVal="username=admin;password=admin";
                    if (GlobalVariables.personDescription != null && GlobalVariables.personDescription.equals("finger matching")) {

                        Log.d("Web"," It is finger matching "+GlobalVariables.loggedInUser);
                        if (GlobalVariables.loggedInUser != null)
                            retVal = "username="+GlobalVariables.loggedInUser+";password=demo@123";
                        else
                        if (GlobalVariables.FirstUser)
                        retVal = "username=mburkett;password=demo@123";
                        else
                            retVal="username=nwilson;password=demo@123";


                        Log.d("Home","First User is "+retVal);
                        GlobalVariables.loggedInUser=null;
                    }
                    else
                    if (GlobalVariables.personDescription != null && GlobalVariables.personDescription.equals("face matching")) {
                        Log.d("Web","PersonID is "+GlobalVariables.personID);

                        //Retrieve person ID related
                        if (GlobalVariables.personID != null) {
                            //Get the PersonID related username;

                            retVal = "username="+GlobalVariables.personID+";password=demo@123";
                            //GlobalVariables.personID = null;
                        } else

                        retVal = "username=nwilson;password=demo@123";
                    } else
                    if (GlobalVariables.personDescription != null && GlobalVariables.personDescription.equals("not matching"))
                        retVal = "Failure";

                    //String currmsg="<body><html>Status="+GlobalVariables.personDescription;
                    Log.d("Web"," AUthentication Status "+retVal);
                    GlobalVariables.personDescription=null;
                    GlobalVariables.tokenID=0;
                    GlobalVariables.personID = null;
                    GlobalVariables.reqStatus= false;
                    GlobalVariables.reqID=0; //For FingerPrint
                    return newFixedLengthResponse(retVal);
//                    return newFixedLengthResponse( currmsg + "</body></html>\n" );
                } else {
                    String currmsg="<body><html>InProgress";
                    return newFixedLengthResponse("InProgress");
                    //return newFixedLengthResponse( currmsg + "</body></html>\n" );
                }

            }

        }
        //return newFixedLengthResponse( msg + "</body></html>\n" );
        return newFixedLengthResponse("success");
    }
    //  sendBroadCast(appContext,"Success");
    private void sendBroadCast(final Context context,String Source, String status, String state) {
        Log.e("Main","SendBroadcast"+status);
        try {

            final Intent intent = new Intent(Source);
            intent.putExtra("EVENT", status);
            intent.putExtra("STATE", state);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            Log.d(TAG,"Sending Broadcast event ");
        } catch (Exception oj) {
            Log.d("WebServer","Got Crashed while Sending Broadcast "+oj.getMessage());
        }
    }

}
