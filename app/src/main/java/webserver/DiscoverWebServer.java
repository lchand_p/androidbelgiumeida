package webserver;

import android.app.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.IBinder;
import android.util.*;

import java.util.*;
import android.content.*;
import android.widget.Toast;

import java.net.*;
import java.io.*;

import static webserver.GlobalVariables.SP_FingerPrint;
import static webserver.GlobalVariables.mobileWorkFlowConfigure;


public class DiscoverWebServer extends Service {
	private static final int BUFFER_SIZE = 512;
 
	private static final int ENDPOINT_SERVICE_PORT = 34553;
	private static final int LOCALSERVER_SERVICE_PORT = 34552;
	private static final int LOCAL_SERVER_DISCOVERY_SUCESS=354;
	public static boolean isAlive;
	boolean DiscoverySuccess = false;
	private DatagramSocket datagramSocket=null;
	private DatagramSocket Rxsocket=null;
	private String TAG="DiscoverWebServer";
	private String DeviceMacAddress= null;
	private SharedPreferences prefs;
	//private boolean mobileWorkFlowConfigure =true;
	static {
		DiscoverWebServer.isAlive = false;
	}
	 public IBinder onBind(Intent paramIntent)
	  {
	    return null;
	  }
	/*public DiscoverAadServer() {
		super("DiscoverAadServer");
	}

	public void onHandleIntent(final Intent intent) {
		*/
	 public void onCreate()
	  {
		 try { 
	    super.onCreate();
			 DiscoverWebServer.isAlive = true;
			Log.d(TAG, "Discovery Started");
			 prefs = getBaseContext().getSharedPreferences(
					 SP_FingerPrint, MODE_PRIVATE);


			 DeviceMacAddress = GetMacAddress();
			 Log.d(TAG,"Device MAC Address is "+DeviceMacAddress);



			new Thread(new Runnable() {
				@Override
				public void run() {

					Log.e(TAG,"Before DiscoverAad Server"+DiscoverWebServer.this);
					Log.e(TAG,"aa Before DiscoverAad Server"+DiscoverWebServer.this);
					Log.e(TAG,"GetSubnetBoradcast got");

					int num;
					byte[] bytes;
					boolean serverFound = false;
					DatagramPacket datagramPacket;
					DatagramPacket datagramPacket2;
					BufferedReader bufferedReader;
					String response = "";
					String line;
					byte[] buffer = new byte[BUFFER_SIZE];
					int	counter=0;
					
					Log.e(TAG,"Befor New Datagram");
					try {
						datagramSocket = new DatagramSocket();
						Rxsocket = new DatagramSocket(ENDPOINT_SERVICE_PORT);
						Rxsocket.setSoTimeout(5*1000);
						num = 0;
						while (true) {
							if (IsServerDiscoverySuccess()) {
								Log.e(TAG,"Already Discovery succes, so continue");
								//Thread.sleep(10000);
								//Thread.sleep(1000);
								//	updateCurrentVisit();
								//	connectToLocalAadServer(DiscoverAadServer.this);
								datagramSocket.close();
								Rxsocket.close();
								stopSelf();
								return;
							} else {
								Log.e(TAG,"Not success, going ahead "+counter);

							}
							if (counter > 20) {
								Log.e(TAG,"Not success, exiting");
								stopSelf();
								}
							counter++;
							//CURRENTLY SENDING ONLY ONE PACKET, NEEDS TO CHECK RESPONSE, BASED ON THAT
							//TAKE DECISION

							//if (num >= MAX_PACKETS) {
							//	continue;
							//}
							//num++;

							//DiscoverAadServer.this.broadcastMessage(1);
							InetAddress subnetBroadcastAddr = InetAddress.getByName("255.255.255.255");

							//datagramSocket.setSoTimeout(1000 * (int) Math.pow(
							//		2.0, n));
							Log.e(TAG,"Sending Controller: Hello LocalServer/"+DeviceMacAddress);

							bytes = ("Signage: Hello LocalServer/" + DeviceMacAddress)
									.getBytes();

							if (mobileWorkFlowConfigure == true ) {
								int workflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

								Log.d(TAG, "Support of Configure WorkFlow ID  " + workflowID);

								bytes = ("Signage: Hello LocalServer /" + DeviceMacAddress + "-" + workflowID)
										.getBytes();
							}

							datagramPacket = new DatagramPacket(bytes,
									bytes.length, subnetBroadcastAddr,
									LOCALSERVER_SERVICE_PORT);
							Log.e(TAG,"Sending pakcet ");
							System.out.println("\nSending packet " + num
									+ " to LocalServer");
							datagramSocket.send(datagramPacket);
							Log.e(TAG,"Sent the datagram");

							final DatagramPacket receivedPacket = new DatagramPacket(buffer, BUFFER_SIZE);
							Log.e(TAG,"Waiting to receive");
							
							Rxsocket.receive(receivedPacket);
							
							String dataBuff = new String( receivedPacket.getData(), 0,
									receivedPacket.getLength() );
							Log.e(TAG,  "Receied Pkt"+dataBuff);

							String remoteIP  = receivedPacket.getAddress().getHostAddress();
							Log.d(TAG,"Received Device IP is "+remoteIP);

							final InetAddress newDeviceAddress = receivedPacket.getAddress();
							final int newDevicePort = receivedPacket.getPort();
							Log.e(TAG,"Received IP and Port"+newDevicePort);
							//ProcessReceivedPacket(dataBuff, newDeviceAddress, newDevicePort);
							ProcessReceivedPacket(dataBuff, remoteIP, newDevicePort);
				
							
							try {
								Thread.sleep(5* 1000);
							//	System.out.println("Media Manager, Wokeup from sleep");

							}
							
							catch (Exception e) {
								e.printStackTrace();
							}
							//datagramSocket.close();
						}

					} catch (Exception e) {
					//	e.printStackTrace();
						Log.e(TAG,"Local Server Not Responding - Socket Timeout");
						datagramSocket.close();
						Rxsocket.close();

						stopSelf();					}
				}
			}).start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	 @Override
	  public void onDestroy()
	  {
		Log.e(TAG,"Ondestroy");
	

	    super.onDestroy();

		this.datagramSocket.close();
	
		this.Rxsocket.close();
	           
	  }

	  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
	  {
	    return IntentService.START_NOT_STICKY;
	  }

	private InetAddress getSubnetBroadcastAddr() {
		final InetAddress ipAddress = this.getIpAddress();
		InetAddress broadcast = null;
		try {
			final Iterator<InterfaceAddress> iterator = NetworkInterface
					.getByInetAddress(ipAddress).getInterfaceAddresses()
					.iterator();
			while (iterator.hasNext()) {
				broadcast = iterator.next().getBroadcast();
			}
			return broadcast;
		} catch (SocketException ex) {
			ex.printStackTrace();
			Log.d(TAG, "getBroadcast" + ex.getMessage());
			return null;
		}
	}

	private InetAddress getIpAddress() {
		InetAddress inetAddress = null;
		try {
			final Enumeration<NetworkInterface> networkInterfaces = NetworkInterface
					.getNetworkInterfaces();
			while (networkInterfaces.hasMoreElements()) {
				final NetworkInterface networkInterface = networkInterfaces
						.nextElement();
				final Enumeration<InetAddress> inetAddresses = networkInterface
						.getInetAddresses();
				while (inetAddresses.hasMoreElements()) {
					final InetAddress inetAddress2 = inetAddresses
							.nextElement();
					if (!inetAddress2.isLoopbackAddress()
							&& (networkInterface.getDisplayName().contains(
									"wlan0") || networkInterface
									.getDisplayName().contains("eth0"))) {
						inetAddress = inetAddress2;
					}
				}
			}
			return inetAddress;
		} catch (SocketException ex) {
			Log.d(TAG, ex.toString());
			return null;
		}
	}

	public static boolean isAlive() {
		return DiscoverWebServer.isAlive;
	}




	/*public void onDestroy() {
		super.onDestroy();
		DiscoverAadServer.isAlive = false;
	} */
	boolean IsServerDiscoverySuccess(){


		return DiscoverySuccess;

	}
	private void ProcessReceivedPacket(String buff, String  DestIPAddr, int Port) {
		int responseCode=0;
		String tokenID=null;
		String ipAddr = null;
		String remoteMacAddr = null;
		int TerminalID = -1;
		int workflowID=-1;

		Log.e(TAG,"Received Discover Response"+buff);

		if (buff.contains("Hello LocalServer")) {
			//responseCode = Integer.parseInt(buff.substring(buff.indexOf("<Event>")+7,buff.indexOf("</Event")));
			//tokenID = buff.substring(buff.indexOf("<ID>")+4,buff.indexOf("</ID"));
			ipAddr = buff.substring(buff.lastIndexOf('/')+1);
			try {
				if (ipAddr.contains("-")) {
					//this has Mac Address and IP Address
					String arr1[] = ipAddr.split("-");
					ipAddr = arr1[0];
					remoteMacAddr = arr1[1];
					TerminalID = Integer.valueOf(arr1[2]);
					workflowID = Integer.valueOf(arr1[3]);
					Log.d(TAG, "IP Addr is " + arr1[0] + "  Remote MacAddr is " + arr1[1]+"Terminal ID is "+TerminalID);
					Log.d(TAG,"Work Flow ID "+workflowID);
				}
			} catch (Exception obj) {

			}
			Log.d(TAG,"IPAddr is "+ipAddr);
//			String retreivedIP = GetDeviceSIPServer("smart.blynkmedia.com");
			String retreivedIP = prefs.getString("SP_REMOTE_IP",
					"");
			String prevMac = prefs.getString("SP_REMOTE_MAC",
					"test");

			int prevTermianlID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);

			Log.d(TAG,"Prev Terminal ID is "+prevTermianlID);

			Log.d(TAG,"Retreived IP is "+retreivedIP);

			int prevworkflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

			Log.d(TAG,"Prev Mac is "+prevMac+" remoteMacAddr is "+remoteMacAddr);
			if (((prevMac.equals("test")  || prevMac == null) && remoteMacAddr != null) || (prevMac != null) && prevMac.equals(remoteMacAddr)) {


				//if ((prevTermianlID !=TerminalID) || ((prevMac.equals("test")  || prevMac == null) && remoteMacAddr != null) || (prevMac != null) && prevMac.equals(remoteMacAddr)) {

					if ((retreivedIP == null || !retreivedIP.equals(DestIPAddr.trim())) || (prevTermianlID != TerminalID) ||
							(prevworkflowID != workflowID)) {
					//if (retreivedIP ==null || !retreivedIP.equals(ipAddr.trim())) {


					//Log.d(TAG, "Remote IP Addr is " + retreivedIP);
					Log.d(TAG, "Remote IP Addr is " + DestIPAddr);

					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("SP_REMOTE_IP", DestIPAddr.trim()); //ipAddr.trim());
					editor.putString("SP_REMOTE_MAC", remoteMacAddr.trim());
						editor.putInt(GlobalVariables.SP_TerminalID, TerminalID);

						if (mobileWorkFlowConfigure == true ) {
							Log.d(TAG,"Not Taking the Mobile Work Flow ");
						} else
						editor.putInt(GlobalVariables.workflowStatus, workflowID);

						editor.commit();

					DiscoverySuccess =true;
					Log.d(TAG,"Received Terminal ID is "+TerminalID);
					Log.d(TAG," Previous current work flow ID is"+workflowID);
					Log.d(TAG, "Both the IPS are not equal" + ipAddr + "  " + retreivedIP);
					//	SaveDNSHostFile(ipAddr);
					//	UpdateHostsFile(ipAddr);
					Log.d(TAG, "Restarting the App as change in Local server");
					try {
						Thread.sleep(1000);
					} catch (Exception ojb) {

					}
					stopSelf();
					//System.exit(0);
				} else
					Log.d(TAG, "Both IP are equal no updation ");
			} else
				Log.d(TAG,"Not Taking the Mac ");
		}
		 else {
			Log.e(TAG,"Discovery Failure");
		}
	}
	private Integer UpdateHostsFile(String tmp) {
		Process process = null;
		DataOutputStream os = null;

		try {
			process = Runtime.getRuntime().exec("su");
			os = new DataOutputStream(process.getOutputStream());
			//os.writeBytes("mount -o remount,rw -t yaffs2 /dev/block/mtdblock4 /system\n");
			os.writeBytes("mount -o remount,rw /system\n");

			String pathhosts_file = Environment.getExternalStorageDirectory()
					+ File.separator + "hosts";
			File myFile = new File(pathhosts_file);
			if (myFile.exists()) {
				Log.d(TAG,"Hosts File exists  "+pathhosts_file);
			} else
				Log.d(TAG,"No Hosts file exists ");

			File myFile2 = new File("/etc/hosts");
			if (myFile2.exists()) {
				Log.d(TAG,"etc Hosts File exists  /etc/hosts");
				//os.writeBytes("mv -f /system/etc/hosts /system/etc/hosts.bak\n");
			} else
				Log.d(TAG,"etc No Hosts file exists ");
			Log.d(TAG,"before busybox cp ");

			//os.writeBytes("cp "+pathhosts_file+" /system/etc/hosts\n");
			String pathhosts_file2 = "/mnt/sdcard/hosts";
			os.writeBytes("busybox cp -f " + pathhosts_file2+  " /system/etc/hosts\n");
			Log.d(TAG,"after busybox cp ");

			os.writeBytes("chmod 644 /system/etc/hosts\n");

			Log.d(TAG,"UpdateTask "+pathhosts_file);
//					+ Environment.getExternalStorageDirectory()
//					.getAbsolutePath() + "/buildprop.tmp\n");

			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();
			Thread.sleep(1000);
			Log.d(TAG,"Success UpdateTask ");

		} catch (Exception e) {
			Log.d(TAG,  "Update Hosts Error: " + e.getMessage());
			return 0;
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				process.destroy();
			} catch (Exception e) {
				Log.d(TAG,  "Update Hosts Error: " + e.getMessage());
				return 0;
			}
		}
		return 1;
		//tempFile = Environment.getExternalStorageDirectory().getAbsolutePath()
		//		+ "/buildprop.tmp";
	}
	private void SaveDNSHostFile(String IPAddr) {

		File myFile = null;
		String details=null;
		if (IPAddr != null)
			details = "127.0.0.1 localhost\n"+IPAddr+" smart.blynkmedia.com\n";
		else
			details = "127.0.0.1 localhost\n";

		Log.d(TAG,"in SaveDNSHostFile "+details);
		try {
			String pathhosts_file = Environment.getExternalStorageDirectory()
					+ File.separator + "hosts";
			File myTestFile = new File(pathhosts_file);
			if (myTestFile.exists())
				myTestFile.delete();

			//myFile = new File(Environment.getExternalStorageDirectory()
			//		+ File.separator + "tvoverlayprocess.txt");
			myFile = new File(pathhosts_file);

			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			myOutWriter.append(details);
			myOutWriter.close();
			fOut.close();


		} catch (Exception e) {

		}
	}
	private String GetDeviceSIPServer(String str) {
		String retVal=null;
		String pathhosts_file = "/etc/hosts";
		Log.d(TAG," in GetDeviceSIPServer DNS file  "+str);

		File myTestFile = new File(pathhosts_file);

		if (myTestFile.exists()) {
			Log.d(TAG,"Host file exists ");
			try {
				Scanner scanner = new Scanner(myTestFile);
				Log.d(TAG,"Searching DNS file for "+myTestFile);
				//now read the file line by line...
				int lineNum = 0;
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					if (line.contains(str)) {
						Log.d(TAG,"DNS is available "+str);
						//line.replaceFirst(str,"     ");
						retVal  = line.substring(0,line.lastIndexOf("smart.blynkmedia.com"));
						return retVal.trim();
					}
				}
			} catch (FileNotFoundException e) {
				//handle this
				Log.d(TAG,"host dns Got exception ");
			}
		} else
			Log.d(TAG,"No Hosts file ");
		Log.d(TAG,"Coming out ");
		return null;

	}

	private String getRemoteIP() {
		String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
				"");

		Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

		return remoteIPAddr;
	}
	private Boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}

	public Boolean isWifiConnected(){
		if(isNetworkAvailable()){
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
		}
		return false;
	}

	public Boolean isEthernetConnected(){
		if(isNetworkAvailable()){
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
		}
		return false;
	}
	private String GetMacAddress() {
		String mac="06:5b:21:78:a5";

		if (isEthernetConnected()) {
			mac = readEthernetMacAddress();
		} else {
			//= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInf = wifiMan.getConnectionInfo();
			mac = wifiInf.getMacAddress();

			if (mac.contains("02:00:00:00:00:00"))
				mac = getMacAddr();
		}
		Log.d(TAG,"Mac Address is "+mac);
		return mac;
	}
	private String readEthernetMacAddress()
	{
		String macAddr = null;

		try {
			File myFile = new File("/sys/class/net/eth0/address");
			FileInputStream fIn = new FileInputStream(myFile);
			BufferedReader myReader = new BufferedReader(new InputStreamReader(
					fIn));
			String aDataRow = "";
			String aBuffer = "";
			while ((aDataRow = myReader.readLine()) != null) {
				aBuffer += aDataRow + "\n";
			}
			macAddr = aBuffer.toString().trim();
			myReader.close();
		} catch (Exception ob) {
			return null;
		}
		//	Toast.makeText(getBaseContext(), "Done reading SD 'mysdfile.txt'",
		//		Toast.LENGTH_SHORT).show();
		return macAddr;
	}
	private String getMacAddr() {
		try {
			List<NetworkInterface> all =Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface nif : all) {
				if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

				byte[] macBytes = nif.getHardwareAddress();
				if (macBytes == null) {
					return "";
				}

				StringBuilder res1 = new StringBuilder();
				for (byte b : macBytes) {
					res1.append(Integer.toHexString(b & 0xFF) + ":");
				}

				if (res1.length() > 0) {
					res1.deleteCharAt(res1.length() - 1);
				}
				return res1.toString();
			}
		} catch (Exception ex) {
			Log.d("NewSplash","Not able to get mac address");
		}
		return "02:00:00:00:00:00";
	}

}
