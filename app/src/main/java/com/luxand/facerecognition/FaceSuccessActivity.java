package com.luxand.facerecognition;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;

import Led.DeviceManager;
import database.AppDatabase;
import frost.EnrollTypeActivityNew;
import frost.UserIdActivity;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import lumidigm.captureandmatch.activites.LaunchActivity;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
//import LicenceActivity;
import frost.restrictedaccess.LicenceActivity;
import frost.timeandattendance.TerminalFaceCheckInActivity;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static webserver.GlobalVariables.FaceICA;
import static webserver.GlobalVariables.TimeandAttendence;
import static webserver.GlobalVariables.selectedProduct;

import android.widget.Button;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by colors on 17/10/18.
 */

public class FaceSuccessActivity extends Activity implements IResult {
    //RelativeLayout successlayout;
    String TAG="FaceSuccessActivity";
    private SharedPreferences prefs =  null;
    private boolean fromWeb= false;
    private String personID = null;
    private String personName = null;
    lumidigm.captureandmatch.entity.User user=null;
    private SharedPreferences Face,Finger;
    //TextView textView;
    String encodedData;
    private static boolean forEnrolment  =false;
    private TextView personnameID=null;
    private boolean adminVerification=false;
    private TextView WelcomeText=null;
    private Button continueButton= null;
    private Button cancelButton= null;
    private Button Checkin=null;
    private Button Checkout=null;

    private String checkinstatus=null;
    AppDatabase appDatabase;
    private String source="face";
    private DeviceManager ledStatus=null;
    private boolean continueButtonPressed=false;
    private boolean cancelButtonPressed =false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.user_success);
        //successlayout = findViewById(R.id.successlayout);
        //Intent intent = getIntent();

        String successcase = "success";

        try {
            if (GlobalVariables.ledSupport ==true) {
                if (GlobalVariables.MobileDevice == false) {
                    ledStatus = new DeviceManager();
                    /* set Pin Direction
                     * @param PIN pin number from GPIO map
                     * @param Direction in : for input , out : for output
                     */
                    // public static void setPinDir(@NonNull String PIN,@NonNull EnPinDirection Direction){

                    // EnPinDirection Direction= new ("out");
                    Log.d(TAG, "Set Pin DIrection");
                    ledStatus.setPinDir("13", "out");
                    Log.d(TAG, "Set Pin Output ");
                    ledStatus.setPinOn("13");
                    Log.d(TAG, "Set Pin Out complete");
                }
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Crashed while enabling ");
        }
        personName = null;
        forEnrolment = false;
        prefs = getBaseContext().getSharedPreferences(
                GlobalVariables.SP_FingerPrint, MODE_PRIVATE);

        appDatabase = AppDatabase.getAppDatabase(this);

        try {
            user = getIntent().getExtras().getParcelable("User");
            // allTasks = getIntent().getExtras().getParcelable("task");


            if (user  != null)
                personName = user.getUsername().trim();
            else
                Log.d(TAG,"Not Received user Info ");

            Log.d(TAG,"Person Name is "+personName);
            //personName = user.getUsername().trim();
            Log.d(TAG," User Name is "+personName);
        } catch (Exception ob) {
            Log.d(TAG,"Got Exception while reading User ");
            user = null;
            personName = null;
        }

        try {
            checkinstatus = getIntent().getStringExtra("checkinstatus");
            Log.d(TAG,"CheckIn Status is "+checkinstatus);
        } catch (Exception obj) {
            Log.d(TAG,"Check in exceptionis ");
            checkinstatus = null;
        }
        try {
            adminVerification=getIntent().getBooleanExtra("adminVerification",false);
            Log.d(TAG,"Admi Verification is "+adminVerification);
        }catch (Exception obj) {
            adminVerification=false;
        }

        try {
            try {
                source = getIntent().getStringExtra("source");
                Log.d(TAG, "From Source is " + source);
            } catch (Exception obj33) {
                source = null;
            }
            try {
                fromWeb = getIntent().getBooleanExtra("FromWeb", false);
                successcase = getIntent().getStringExtra("case");
                personID = getIntent().getStringExtra("personID");

                Log.d(TAG,"Received PersonID is "+personID);


                if (user == null && personID !=null) {
                    Log.d(TAG,"user is null - so fetching person"+personID);
                    user = appDatabase.userDao().countUsersBasedonUserID(personID);

                    if (user == null)
                        Log.d(TAG,"Failed in fetching user data ");

                 }
                if (personName == null)
                personName = getIntent().getStringExtra("personName").toString();
                Log.d("SuccessActivity", "Curr State is " + successcase+" from Web "+fromWeb+" ID is "+personID);
            } catch (Exception obj) {

            }

        } catch (Exception tpexcep) {
            source = null;
        }

        Log.d(TAG,"Person Name is "+personName);

        if (source != null && source.equals("face")) {

            Log.d(TAG,"Admin Verification status is "+adminVerification);
            if (adminVerification == true)
                new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"Glow_Led",null,"Success");
            Log.d(TAG,"TimeandAttendence is "+TimeandAttendence);

         if (FaceICA) {
             Log.d(TAG,"It is Face ICA Testing");
             setContentView(R.layout.face_user_successforsmartcard);

         } else
        if (successcase != null && successcase.equals("forenrollment")) {
            Log.d(TAG,"It is for Enrollment & setting as true ");
            Face=getSharedPreferences("face",MODE_PRIVATE);
            Face.edit().putBoolean("face", true).apply();
            forEnrolment = true;
            setContentView(R.layout.face_user_enrollsuccessnew);

            continueButton = findViewById(R.id.contbutton);
            cancelButton = findViewById(R.id.cancelbutton);

            Finger=getSharedPreferences("finger",MODE_PRIVATE);
           // Finger.edit().putBoolean("finger", true).apply();

            if (user != null) {

                UpdatePaxtonenrollment(user.getUserId());

                Log.d(TAG,"UpdateFaceEnrollment  "+user.getUserId());
                UpdateFaceEnrollment(user);
            }
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG,"on continueButton Button ");
                     continueButtonPressed=true;
                }

            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG,"on cancelButton Button ");

                    {
                         cancelButtonPressed =true;
                    }
                }
            });
        }   else if (TimeandAttendence) {
            Log.d(TAG,"Itis TIme and Attendance ");
                setContentView(R.layout.face_user_success_timeandattendance);
                Checkin = findViewById(R.id.checkinbutton);
                Checkout = findViewById(R.id.checkoutbutton);


                Checkin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckInPage();
                    }
                });
                Checkout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckInPage();
                    }
                });

            if (user.getCheckInOut() == 1) {
                Checkin.setEnabled(false);
                Checkin.setBackgroundResource(R.drawable.rounded_button_grey);

                //Checkin.setBackgroundColor(R.drawable.rounded_button_grey);
                // Checkin.setBackgroundColor(android.R.color.tertiary_text_light);

                Checkout.setEnabled(true);
            } else {
                Checkin.setEnabled(true);
                Checkout.setEnabled(false);

                Checkout.setBackgroundResource(R.drawable.rounded_button_grey);
               // Checkout.setBackgroundColor(Color.LTGRAY);
                //Checkout.setBackgroundColor(android.R.color.tertiary_text_light);
            }

            }
        else if (GlobalVariables.ProductV4Support == true && selectedProduct == 2) {
            Log.d(TAG,"It is Sentinel ");
            setContentView(R.layout.face_user_successnew_sentinel);
        } else
            {
            Log.d(TAG, "It is not for enrollment");



            setContentView(R.layout.face_user_successnew);
        }
            Log.d(TAG,"Before getting personname ");
            personnameID = findViewById(R.id.personname);
            WelcomeText = findViewById(R.id.welcome_text1);

            Log.d(TAG,"first Received personName is "+personName);
            if (personName != null)
                personnameID.setText(personName);

            if (forEnrolment == true)
                WelcomeText.setText("Your Face is successfully Enrolled");
        }
        else {
            if (GlobalVariables.TimeandAttendence == true) {
                setContentView(R.layout.user_success_timeandattendance);

                Checkin = findViewById(R.id.checkinbutton);
                Checkout  = findViewById(R.id.checkoutbutton);


                Checkin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckInPage();
                    }
                });
                Checkout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckInPage();
                    }
                });

                if (user != null) {
                    if (user.getCheckInOut() == 1) {
                        Checkin.setEnabled(false);
                        Checkin.setBackgroundResource(R.drawable.rounded_button_grey);

                        //Checkin.setBackgroundColor(R.drawable.rounded_button_grey);
                        // Checkin.setBackgroundColor(android.R.color.tertiary_text_light);

                        Checkout.setEnabled(true);
                    } else {
                        Checkin.setEnabled(true);
                        Checkout.setEnabled(false);

                        Checkout.setBackgroundResource(R.drawable.rounded_button_grey);
                        // Checkout.setBackgroundColor(Color.LTGRAY);
                        //Checkout.setBackgroundColor(android.R.color.tertiary_text_light);
                    }
                } else
                    Log.d(TAG,"User Data is null");
            }
            else if (FaceICA) {

                Log.d(TAG,"It is Face ICA Testing");
                setContentView(R.layout.face_user_successforsmartcard);
            } else
                setContentView(R.layout.user_success);
        try {

            personnameID = findViewById(R.id.username);

            Log.d(TAG, "second Received personName is " + GlobalVariables.personName);

            if (FaceICA) {
                if (GlobalVariables.personName != null) {
                    personnameID.setText(GlobalVariables.personName);
                }

            } else
            if (personName != null)
                personnameID.setText(personName.toString());

        } catch (Exception obj) {
        Log.d(TAG,"Test got exception");
        }
        }
        try {
            Log.d(TAG,"Before PlayBeep");

            // playBeep();
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("NotAUth","Got Exception while setting Full Screen");
        }

        Log.d(TAG,"Got SelectAPI Respone");
        try {
            if (forEnrolment == false && adminVerification == false) {
                encodedData = user.getEncoded_data();
                String IPAddr = user.getIp_addr();
                Log.d(TAG, "Encoded Data is " + encodedData + " IP Address " + IPAddr);
                String requestType = "101";
                if (encodedData != null) {
                    //              MyProgressDialog.show(OpentDoor.this, R.string.wait_message);
                    String url = "http://" + IPAddr + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                    Log.d(TAG, "onReceive: " + url);
                    // new VolleyService(getApplicationContext(), FaceSuccessActivity.this).tokenBase(GET, url, null, "133");

                    new VolleyService(new IResult() {
                        @Override
                        public void notifySuccess(String requestType, Object response) {

                            Log.d(TAG, "notifySuccess: " + requestType + " Resoibse us" + response);
                        }


                        @Override
                        public void notifyError(String requestType, VolleyError error) {
                            Log.d(TAG, "notifyError: " + error.getMessage());
                            try {
                                //String fingerprint = "testfingerprint";
                                Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");

                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }, this).tokenBase(GET, url, null, requestType);
                }
            }
                Log.d(TAG,"No User Data ");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // if (intent.putExtra("personID",names[0]);)

        //For Finger Print
        if (adminVerification || forEnrolment) {
            try {
                Log.d(TAG,"Sending EnrollTypeSelect ");
                new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "GoToEnroll", null, "test");
            } catch(Exception obj) {
                Log.d(TAG,"Got Exception ");
            }
        }

        int workflowID = prefs.getInt(GlobalVariables.localworkflowStatus, -1);

      //  if (workflowID == 1 || workflowID ==2) {

            Log.d(TAG,"for Enrolment status is "+forEnrolment);

        Log.d(TAG,"Time attendance "+TimeandAttendence+" admin Verifi"+adminVerification+" for enrollment"+forEnrolment);
        //if (forEnrolment == false)

        int timeOut = 2000;
        if (adminVerification || forEnrolment)
            timeOut = 6000;
        else
        if (workflowID ==1 || workflowID ==2)
            // if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal ==2 )
            timeOut = 2000;//10;
        Log.d(TAG,"TimeOut Value is "+timeOut);
        Log.d(TAG,"Time Attendance "+TimeandAttendence+" admin verification "+adminVerification+" for enrol"+forEnrolment);
        //For Timeand attandence don't start the timer - keep the buttons for check in or checkout
        if (!TimeandAttendence || adminVerification || forEnrolment )
        {


            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Log.d(TAG, "for Enrolment status is " + forEnrolment);

                    Log.d(TAG,"Restricted Access status is "+GlobalVariables.RestrictedAccess);

                    if (GlobalVariables.RestrictedAccess == true ) {
                        Log.d(TAG,"It is Restricted Access ");

                        Intent intent = new Intent(FaceSuccessActivity.this, LicenceActivity.class);

                        if (personID != null)
                            intent.putExtra("personID",personID);
                        else

                            Log.d(TAG,"Not attached personID ");
                        try {
                        Log.d(TAG,"Sending Person ID "+personID);


                        Bundle bundle = new Bundle();

                        bundle.putParcelable("User", user);

                        if (user != null)
                            Log.d(TAG,"USER Name is "+user.getUsername());
                        else
                            Log.d(TAG,"User is Null");
                        intent.putExtras(bundle);

                        //intent.putExtra("userid", personID);
                        startActivity(intent);

                        finishAffinity();


                    } catch (Exception obj) {
                        Log.d(TAG,"Got eception while sending "+obj.getMessage());
                    }
                    } else
                    if (adminVerification == true) {
                        Log.d(TAG, "Admin Verification is true ");
                        //Intent intent = new Intent(FaceSuccessActivity.this, Enroll.class);
                     try {
                        Intent intent = new Intent(FaceSuccessActivity.this, UserIdActivity.class);
                        //intent.putExtra("adminverification","success");


                        //intent = new Intent(getContext(), Enroll.class);

                        //intent.putExtra("userid","test" );
                        intent.putExtra("adminVerification", true);
                        intent.putExtra("userid", "test");
                        intent.putExtra("fromsupervisor", true);

                        //intent.putExtra("userid", personID);
                        startActivity(intent);
                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"Stop_Led",null,"Success");

                        finishAffinity();

                    } catch (Exception obj) {
                        Log.d(TAG,"Got eception while sending "+obj.getMessage());
                    }
                    } else if (forEnrolment == true) {
                        /*if (continueButtonPressed) {

                        } else */
                        Log.d(TAG,"For Enrollment is true "+cancelButtonPressed +" enrollmentButtonPressed ");
                       // if (cancelButtonPressed) {

                        try {
                            Log.d(TAG, "Selected Cancel ");
                            Intent intent = new Intent(FaceSuccessActivity.this, FaceTabLaunchActivity.class);

                            if (selectedProduct == 2)
                                intent = new Intent(FaceSuccessActivity.this, LaunchActivity.class);

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            Bundle mBundle = new Bundle();
                            mBundle.putBoolean("Enrolment", false);
                            mBundle.putBoolean("adminVerification", false);
                            // intent.putExtra("userid", editText.getText().toString());
                            intent.putExtras(mBundle);
                            startActivity(intent);
                            finishAffinity();
                        } catch (Exception obj) {
                            Log.d(TAG,"Got eception while sending "+obj.getMessage());
                        }
                    } else {
                        Log.d(TAG,"Going for CheckIn");
                        CheckInPage();
                    }

                    //  finish();
                }
            }, timeOut);

        } else
            Log.d(TAG,"Not starting the Timer ");
    }
    private boolean SendingMessageWithState(String status, String requestType, String ID, String state) {
        Log.d("Success", "SendMessage " + status);
        //String url="http://192.168.43.24:8080/?username="+status;
        GlobalVariables.currAction=status;
        GlobalVariables.curState=state;

        //Only for Web
        if (GlobalVariables.AppSupport ==1)
            SendingMessageWithStateWeb(status,requestType,ID, state);
        return true;
    }

    private boolean SendingMessageWithStateWeb(String status, String requestType, String ID, String state) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;

/*
        if (!isEthernetConnected()) {
            Log.d(TAG,"SendingMEssageWithStats Not sending as it is Wifi");
        }
*/

        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
/*
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }
*/


        Log.d(TAG,"Getting IP Address");
        //   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+state;

        if (ID != null) {
            try {
                //lumidigm.captureandmatch.entity.User user1 = null;
                //  Log.d(TAG, "notifySuccess: " + user.toString());
                //AppDatabase appDatabase = AppDatabase.getAppDatabase(FaceSuccessActivity.this);
                //user1 = appDatabase.userDao().countUsersBasedonUserID(ID); // != null) {
               // Log.d(TAG, "For User ID " + ID + "ID is"  + " User Name is " + user1.getUsername() + user1.getFullname() + "Real User ID" + user1.get_id());

                url = "http://" + remoteIP + ":8080/?username=" + status + "&state=" + state + "&personID=" + "test";//user1.getUsername();

            } catch (Exception obj) {
                Log.d(TAG,"Got Exception obj"+obj.getMessage());
            }
                  }
        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);

            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

        return true;
    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }



    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d("MainActivity", "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d("MainActivity","Got exception "+obj.toString());
        }
        return false;
    }
    public void CheckInPage(){

        Log.d(TAG,"CheckIn");

        Intent i = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);


        if (selectedProduct == 2)
            i = new Intent(getApplicationContext(),MainActivity_FaceandFinger.class);

        if (source != null && !source.equals("face")) {
            i.putExtra("source","finger");
        } else
            i.putExtra("source","face");


        finish();
        Bundle bundle = new Bundle();
        bundle.putParcelable("User", user);

        i.putExtras(bundle);
        //  Intent intent = new Intent(this,CheckInActivity.class);
        startActivity(i);
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        try {
        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d(TAG,"Going to Enrol Main ");

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        if (selectedProduct == 2)
            intent = new Intent(this,LaunchActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();



    } catch (Exception obj) {
        Log.d(TAG,"Got eception while sending "+obj.getMessage());
    }
        super.onBackPressed();
    }

    private void UpdateFaceEnrollment(lumidigm.captureandmatch.entity.User user) {

        if (user != null) {
            Log.d(TAG, "User ID is " + user.get_id());


            JSONObject jsonObject = new JSONObject();


            try {




                Log.d(TAG,"Added User template timestamp ");
                jsonObject.put("uid", user.getUserId());
//need username  capture from ldap api(uid), this is needed for logging in
                //check_userid_ldap

                //	if (ldapUser != null)
                //		jsonObject.put("username", ldapUser);


                Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                long currts = (curCalendar.getTimeInMillis() / 1000L);
                jsonObject.put("user_template_timestamp", currts);
                Log.d(TAG,"Timestamp to be updated for timestamp "+currts);


                jsonObject.put("template_1", user.getTemplate1());
                jsonObject.put("template_2", user.getTemplate2());
                //   jsonObject.put("template_3", user.getTemplate3());
                jsonObject.put("template_3", user.getUserImage());

                Log.d(TAG,"Sending Face Template is "+user.getUserImage());

                //String facetem1 = Base64.encodeToString(test, Base64.DEFAULT);
                //              jsonObject.put("templateface", user.getTemplateFace());

                //user.setTemplateFace(facetem1);
//                MainActivity_Sentinel.appDatabase.userDao().update(user);

                Log.d(TAG,"Please Wait sending ");
//										MyProgressDialog.show(FingerPrintEnrollActivity.this, R.string.wait_message);
                String url = GlobalVariables.SSOIPAddress + Constants.UPDATER_USERFINGERPRINT;

                new VolleyService(FaceSuccessActivity.this, FaceSuccessActivity.this).tokenBase(POST, url, jsonObject, "101");

                //

            } catch (Exception obj) {

            }
        }
    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        Log.d(TAG, "notifySuccess: " + response.toString());

        // [{"deleted":false,"status":0,"tags":[],"_id":"5b4f2583ee0c1d1e1e884a29","subject":"ticket targus user","issue":"<p>ticket targus user<\/p>\n","group":"5b4df86b86596b6051cc3bc8","type":"5b4df86b86596b6051cc3bc7","priority":1,"date":"2018-07-18T11:33:23.439Z","comments":[],"notes":[],"attachments":[],"history":[{"action":"ticket:created","description":"Ticket was created.","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:33:23.445Z","_id":"5b4f2583ee0c1d1e1e884a2a"},{"action":"ticket:set:type","description":"Ticket type set to: Issue","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:34.762Z","_id":"5b4f2642ee0c1d1e1e884a2c"},{"action":"ticket:set:type","description":"Ticket type set to: Task","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:38.938Z","_id":"5b4f2646ee0c1d1e1e884a2d"},{"action":"ticket:set:assignee","description":"Assignee was cleared","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:48:17.863Z","_id":"5b4f2901ee0c1d1e1e884a2e"},{"action":"ticket:set:assignee","description":"roooop was set as assignee","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T12:00:46.705Z","_id":"5b4f2beeee0c1d1e1e884a30"}],"subscribers":["5b4df86b86596b6051cc3bc9","5b4f193a607f6f318db8f9b9"],"owner":"5b4df86b86596b6051cc3bc9","uid":1010,"__v":4,"assignee":"5b4f193a607f6f318db8f9b9"}]

        if (requestType.equals("102")) {
            Log.d(TAG,"Send 101 Main Activity ");
            MyProgressDialog.dismiss();

            // } else if (requestType.equals("133")) {
            Log.d(TAG,"Got response ");
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG,"133 Go to Main Activity");
                 try {
                    Intent i = new Intent(FaceSuccessActivity.this, LauncherActivity.class);
                    // Bundle bn = new Bundle();

                    // bn.putParcelable("User", user);
                    // i.putExtras(bn);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    //     if (homeTimer != null)
                    //       homeTimer.cancel();

                    //   homeTimer = null;
                    //    finish();
                    finishAffinity();

                } catch (Exception obj) {
                    Log.d(TAG,"Got eception while sending "+obj.getMessage());
                }
                }
            },2000);
        }
    }




    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "notifyError: " + error);
        // MyProgressDialog.dismiss();
    }
    private void UpdatePaxtonenrollment(String userid) {
        try
        {
            if (GlobalVariables.PaxtonSupport == true) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("userid", userid);
                jsonObject.put("token_type", 1); //type 0 for finger, 1 for face
                String url = GlobalVariables.SSOIPAddress + Constants.UPDATE_PAXTON_ENROLLMENT;

                Log.d(TAG, "Sending Paxton Enrollment for User ID " + userid + " URL " + url);
                new VolleyService(FaceSuccessActivity.this, FaceSuccessActivity.this).tokenBase(POST, url, jsonObject, "105");

            }
        } catch(Exception obj)
        {
            Log.d(TAG, "Got the Exception while token parsing for Paxton " + obj.getMessage());
        }
    }
}
