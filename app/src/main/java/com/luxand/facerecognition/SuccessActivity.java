package com.luxand.facerecognition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;

import database.AppDatabase;
import frost.EnrollTypeActivityNew;
import lumidigm.HomeActivity;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;

/**
 * Created by colors on 17/10/18.
 */

public class SuccessActivity extends Activity {
    RelativeLayout successlayout;
    String TAG="Success Activity";
    private SharedPreferences prefs =  null;
    private boolean fromWeb= false;
    private String personID = null;
    private static boolean forEnrolment =false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success_activity);
        successlayout = findViewById(R.id.successlayout);
        //Intent intent = getIntent();

        String successcase = "success";

        prefs = getBaseContext().getSharedPreferences(
                GlobalVariables.SP_FingerPrint, MODE_PRIVATE);
        forEnrolment =false;

        try {
            fromWeb = getIntent().getBooleanExtra("FromWeb", false);
            successcase = getIntent().getStringExtra("case");
            personID= getIntent().getStringExtra("personID");

            Log.d("SuccessActivity", "Curr State is " + successcase+" from Web "+fromWeb+" ID is "+personID);

        } catch (Exception tpexcep) {
        }

        //Only for WebSupport
        if ((GlobalVariables.AppSupport == 1) && (!fromWeb)) {
            SendingMessageWithState("face","191",personID,successcase);
        }

        if (successcase != null && successcase.equals("forenrollment")) {
            forEnrolment= true;
            SendingMessageWithState("face","191",personID,"success");
            successlayout.setBackground(getResources().getDrawable(R.drawable.fr_access_granted));
        } else
        if(successcase != null && successcase.equals("success")){
            successlayout.setBackground(getResources().getDrawable(R.drawable.fr_access_granted));
        }else {
            successlayout.setBackground(getResources().getDrawable(R.drawable.fr_access_denied));
        }

       // if (intent.putExtra("personID",names[0]);)


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
/*
                if (forEnrolment == true ) {
                    Log.d(TAG,"It is for Enrollment Continuing the EnrollTypeActivity "+personID);
                    Intent intent = new Intent(SuccessActivity.this, EnrollTypeActivityNew.class);
                    intent.putExtra("userid", personID);
                    startActivity(intent);
                    finishAffinity();
                } else */{
                    //SendingMessageWithState("home","191",null);
                    //Intent intent = new Intent(SuccessActivity.this, FaceTabLaunchActivity.class);
                    Intent intent = new Intent(SuccessActivity.this, MainActivity.class);
                    //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                    //startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean("Enrolment", false);
                    mBundle.putBoolean("adminVerification", false);
                    // intent.putExtra("userid", editText.getText().toString());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    finishAffinity();

                }
              //  finish();
            }
        }, 6000);


    }
    private boolean SendingMessageWithState(String status, String requestType, String ID, String state) {
        Log.d("Success", "SendMessage " + status);
        //String url="http://192.168.43.24:8080/?username="+status;
        GlobalVariables.currAction=status;
        GlobalVariables.curState=state;

        //Only for Web
        if (GlobalVariables.AppSupport ==1)
            SendingMessageWithStateWeb(status,requestType,ID, state);
        return true;
    }

    private boolean SendingMessageWithStateWeb(String status, String requestType, String ID, String state) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;

        if (!isEthernetConnected()) {
            Log.d(TAG,"SendingMEssageWithStats Not sending as it is Wifi");
        }

        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
/*
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }
*/


        Log.d(TAG,"Getting IP Address");
        //   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+state;

        if (ID != null) {
            try {
                lumidigm.captureandmatch.entity.User user1 = null;
                //  Log.d(TAG, "notifySuccess: " + user.toString());
                AppDatabase appDatabase = AppDatabase.getAppDatabase(SuccessActivity.this);
                user1 = appDatabase.userDao().countUsersBasedonUserID(ID); // != null) {
                Log.d(TAG, "For User ID " + ID + "ID is"  + " User Name is " + user1.getUsername() + user1.getFullname() + "Real User ID" + user1.get_id());

                url = "http://" + remoteIP + ":8080/?username=" + status + "&state=" + state + "&personID=" + user1.getUsername();

            } catch (Exception obj) {
                Log.d(TAG,"Got Exception obj"+obj.getMessage());
            }
                  }
        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
                /*if (requestType.equals("100")) {
                    //Intent i= new Intent(this, audio.MainActivity.class);

                    Intent i = new Intent(FirstActivity.this, MainActivity.class);

                    startActivity(i);
                } else if (requestType.equals("101")) {
                    Intent i = new Intent(FirstActivity.this, FingerScannActivity.class);
                    startActivity(i);
                } else if (requestType.equals("102")) {
                    Intent i = new Intent(FirstActivity.this, FirstActivity.class);
                    startActivity(i);
                } else if (requestType.equals("103")) {
                    Intent i = new Intent(FirstActivity.this, audio.MainActivity.class);
                    startActivity(i);
                } else if (requestType.equals("104")) {
                    Intent i = new Intent(FirstActivity.this, HomeActivity.class);
                    startActivity(i);
                }*/
            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);
                    /*if (requestType.equals("100")) {
                        //Intent i= new Intent(this, audio.MainActivity.class);

                        Intent i = new Intent(FirstActivity.this, MainActivity.class);

                        startActivity(i);
                    } else if (requestType.equals("101")) {
                        Intent i = new Intent(FirstActivity.this, FingerScannActivity.class);
                        startActivity(i);
                    } else if (requestType.equals("102")) {
                        Intent i = new Intent(FirstActivity.this, FirstActivity.class);
                        startActivity(i);
                    } else if (requestType.equals("103")) {
                        Intent i = new Intent(FirstActivity.this, audio.MainActivity.class);
                        startActivity(i);
                    } else if (requestType.equals("104")) {
                        Intent i = new Intent(FirstActivity.this, HomeActivity.class);
                        startActivity(i);
                    }*/

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

        return true;
    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }



    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d("MainActivity", "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d("MainActivity","Got exception "+obj.toString());
        }
        return false;
    }

}
