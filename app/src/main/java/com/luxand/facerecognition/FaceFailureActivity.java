package com.luxand.facerecognition;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import database.AppDatabase;
import belgiumeida.FacePhotoReadProgress;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.GET;
import static webserver.GlobalVariables.FaceICA;

/**
 * Created by colors on 17/10/18.
 */

public class FaceFailureActivity extends Activity {
    //    RelativeLayout successlayout;
    String TAG="FaceFailureActivity";
    private SharedPreferences prefs =  null;
    private boolean fromWeb= false;
    private String personID = null;
    private boolean fromSuperVisior=false;

    private LinearLayout mainlayut;
    private ImageView imageHint;
    private TextView welcomeText1;
    private String source=null;
    private boolean adminVerification=false;
    private AppDatabase appDatabase=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //successlayout = findViewById(R.id.successlayout);
        //Intent intent = getIntent();

        String successcase = "success";

        Log.d(TAG,"Face Status is"+GlobalVariables.IDNumber+"  Count is "+GlobalVariables.FaceCardReadCount);

        if (FaceICA == true && GlobalVariables.IDNumber == null && GlobalVariables.FaceCardReadCount == 1) {
            GotoFaceInProgress();
        } else {
            setContentView(R.layout.not_authorized_face);
            prefs = getBaseContext().getSharedPreferences(
                    GlobalVariables.SP_FingerPrint, MODE_PRIVATE);

            appDatabase = AppDatabase.getAppDatabase(FaceFailureActivity.this);

            startWeb();

            try {
                fromWeb = getIntent().getBooleanExtra("FromWeb", false);
                successcase = getIntent().getStringExtra("case");
                personID = getIntent().getStringExtra("personID");
                source = getIntent().getStringExtra("source");

                Log.d("SuccessActivity", "Curr State is " + successcase + " from Web " + fromWeb + " ID is " + personID);

            } catch (Exception tpexcep) {
                source = null;
            }
            try {
                adminVerification = getIntent().getBooleanExtra("adminVerification", false);
                Log.d(TAG, "Admi Verification is " + adminVerification);
            } catch (Exception obj) {
                adminVerification = false;
            }
            Log.d(TAG, "SOurce is " + source);
            try {
                fromSuperVisior = getIntent().getExtras().getBoolean("fromsupervisor");
                Log.d("NotAUthorized ", "SuperVisor status is " + fromSuperVisior);
            } catch (Exception obj) {
                fromSuperVisior = false;
            }

            if (source != null && source.equals("face")) {
                setContentView(R.layout.not_authorized_face);
            } else
                setContentView(R.layout.not_authorized);

            try {
                Log.d("NotAuthorized", "Before PlayBeep");

                // playBeep();
                Log.d("NotAUth", "on Create ");
                View decorView = getWindow().getDecorView();
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

            } catch (Exception obj) {
                Log.d("NotAUth", "Got Exception while setting Full Screen");
            }

            if (GlobalVariables.MobileDevice == true) {
                Log.d(TAG, "Sending Glow_Led ");
                new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "Glow_Led", null, "Reject");
            }
            //  TextView textView=(TextView)findViewById(R.id.welcome);
            mainlayut = (LinearLayout) findViewById(R.id.mainlayut);
            imageHint = (ImageView) findViewById(R.id.image_hint);
            welcomeText1 = (TextView) findViewById(R.id.welcome_text1);
            //View overlay = findViewById(R.id.mylayout);
/*
        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
            //   int attempts = 2-EnrollCount;

            Log.d("NotAUthorized", "SUpervisior status is " + fromSuperVisior);


            if (adminVerification) {
                welcomeText1.setText("You do not have Authorization\n" +
                        "to enroll any user.");
            } else if (!fromSuperVisior)
                welcomeText1.setText("You do not have Authorization \n" +
                        "to access this door");
            else
                welcomeText1.setText("You do not have Authorization\n" +
                        "to enroll any user.");


            //imageHint.setImageResource(R.drawable.notasks);

            Log.d("NotAUthorized", "Before User setting");
       /*  if (fromSuperVisior)
            textView.setText("Enrollment");
        else
            textView.setText("Identification");
*/

        /*

        //Only for WebSupport
        if ((GlobalVariables.AppSupport == 1) && (!fromWeb)) {
            SendingMessageWithState("face","191",personID,successcase);
        }

        if (successcase.equals("forenrollment")) {
            SendingMessageWithState("face","191",personID,"success");
            successlayout.setBackground(getResources().getDrawable(R.drawable.fr_access_granted));
        } else
        if(successcase.equals("success")){
            successlayout.setBackground(getResources().getDrawable(R.drawable.fr_access_granted));
        }else {
            successlayout.setBackground(getResources().getDrawable(R.drawable.fr_access_denied));
        }
*/

            // if (intent.putExtra("personID",names[0]);)


            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (GlobalVariables.MobileDevice == true) {
                        Log.d(TAG, "Sending Stop_Led ");
                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "Stop_Led", null, "Reject");
                    }
                    //SendingMessageWithState("home","191",null);
                    //Intent intent = new Intent(SuccessActivity.this, FaceTabLaunchActivity.class);
                    //  Intent intent= new Intent(FaceFailureActivity.this, MainActivity.class);
                    //    Intent intent= new Intent(FaceFailureActivity.this, MainActivity_Sentinel.class);
                    Intent intent = new Intent(FaceFailureActivity.this, Main2Activity.class);

                    if (GlobalVariables.FaceandFinger == true)
                        intent = new Intent(FaceFailureActivity.this, MainActivity_FaceandFinger.class);


                    try {
                    //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                    //startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean("Enrolment", false);
                    mBundle.putBoolean("adminVerification", false);
                    // intent.putExtra("userid", editText.getText().toString());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    finishAffinity();


                } catch (Exception obj) {
                    Log.d(TAG,"Got eception while sending "+obj.getMessage());
                }
                    //  finish();
                }
            }, 2000);
        }

    }
    private boolean SendingMessageWithState(String status, String requestType, String ID, String state) {
        Log.d("Success", "SendMessage " + status);
        //String url="http://192.168.43.24:8080/?username="+status;
        GlobalVariables.currAction=status;
        GlobalVariables.curState=state;

        //Only for Web
        if (GlobalVariables.AppSupport ==1)
            SendingMessageWithStateWeb(status,requestType,ID, state);
        return true;
    }

    private boolean SendingMessageWithStateWeb(String status, String requestType, String ID, String state) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;

        if (!isEthernetConnected()) {
            Log.d(TAG,"SendingMEssageWithStats Not sending as it is Wifi");
        }

        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }


        Log.d(TAG,"Getting IP Address");
        //   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+state;

        if (ID != null) {
            try {
                lumidigm.captureandmatch.entity.User user1 = null;
                //  Log.d(TAG, "notifySuccess: " + user.toString());
                AppDatabase appDatabase = AppDatabase.getAppDatabase(FaceFailureActivity.this);
                user1 = appDatabase.userDao().countUsersBasedonUserID(ID); // != null) {
                Log.d(TAG, "For User ID " + ID + "ID is"  + " User Name is " + user1.getUsername() + user1.getFullname() + "Real User ID" + user1.get_id());

                url = "http://" + remoteIP + ":8080/?username=" + status + "&state=" + state + "&personID=" + user1.getUsername();

            } catch (Exception obj) {
                Log.d(TAG,"Got Exception obj"+obj.getMessage());
            }
        }
        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
                /*if (requestType.equals("100")) {
                    //Intent i= new Intent(this, audio.MainActivity.class);

                    Intent i = new Intent(FirstActivity.this, MainActivity.class);

                    startActivity(i);
                } else if (requestType.equals("101")) {
                    Intent i = new Intent(FirstActivity.this, FingerScannActivity.class);
                    startActivity(i);
                } else if (requestType.equals("102")) {
                    Intent i = new Intent(FirstActivity.this, FirstActivity.class);
                    startActivity(i);
                } else if (requestType.equals("103")) {
                    Intent i = new Intent(FirstActivity.this, audio.MainActivity.class);
                    startActivity(i);
                } else if (requestType.equals("104")) {
                    Intent i = new Intent(FirstActivity.this, HomeActivity.class);
                    startActivity(i);
                }*/
            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);
                    /*if (requestType.equals("100")) {
                        //Intent i= new Intent(this, audio.MainActivity.class);

                        Intent i = new Intent(FirstActivity.this, MainActivity.class);

                        startActivity(i);
                    } else if (requestType.equals("101")) {
                        Intent i = new Intent(FirstActivity.this, FingerScannActivity.class);
                        startActivity(i);
                    } else if (requestType.equals("102")) {
                        Intent i = new Intent(FirstActivity.this, FirstActivity.class);
                        startActivity(i);
                    } else if (requestType.equals("103")) {
                        Intent i = new Intent(FirstActivity.this, audio.MainActivity.class);
                        startActivity(i);
                    } else if (requestType.equals("104")) {
                        Intent i = new Intent(FirstActivity.this, HomeActivity.class);
                        startActivity(i);
                    }*/

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

        return true;
    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }



    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d("MainActivity", "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d("MainActivity","Got exception "+obj.toString());
        }
        return false;
    }

    private void startWeb()
    {

        final int DEFAULT_PORT = 8085;
        Context app_context = getApplicationContext();
        WebICAImageCapture_Server server= new WebICAImageCapture_Server(DEFAULT_PORT,app_context);
        try
        {
            server.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String data = intent.getStringExtra("Data");
//            user_data.setText(data);

            String perName=intent.getStringExtra("Name");
            String IDNum=intent.getStringExtra("IDNum");
            String ArabicName=intent.getStringExtra("ArabicName");
            String expiryDate=intent.getStringExtra("expiryDate");
            String Nationality=intent.getStringExtra("Nationality");
            String Gender=null;


            Log.d(TAG,"Expiry Date is "+expiryDate);
            Log.d(TAG,"Nationality  is "+Nationality);

            //expiryDate="+expiryData+"&Nationality
            GlobalVariables.personName=null;
            GlobalVariables.IDNumber=null;
            GlobalVariables.ArabicName=null;
            GlobalVariables.expiryDate=null;
            GlobalVariables.Nationality=null;
            GlobalVariables.Gender=null;


            Log.d(TAG,"Person Name is "+perName+"  ID Number is"+IDNum);

            if (perName != null)
                GlobalVariables.personName=perName;

            if (IDNum != null)
                GlobalVariables.IDNumber=IDNum;

            if (ArabicName != null)
                GlobalVariables.ArabicName=ArabicName;

            if (expiryDate != null)
                GlobalVariables.expiryDate=expiryDate;

            if (Nationality != null)
                GlobalVariables.Nationality=Nationality;


            if (Gender != null)
                GlobalVariables.Gender=Gender;

            Log.d(TAG,"Got the Face Detection Thread"+data.length());

            //Get the File

            //data = data.replaceAll("   ","+");
            //data = data.replaceAll("  ","+");
            data = data.replaceAll(" ","+");

            Log.d(TAG,"Got the Face Detection"+data);

            //byte[] photo =toByteArray(data);
            byte[] photo = Base64.decode(data, Base64.DEFAULT);
            Log.d(TAG,"Got the Face Detection"+photo.length);
            if (photo == null || photo.length <= 0) {
                Log.d(TAG,"Photo Contents are null ");
                return;
            }//if()
            //Create  a bitmap.
            //set to the imageview
//chand added below 2 commands
            Bitmap bmp = BitmapFactory.decodeByteArray(photo, 0, photo.length);
            // bmp.compress(Bitmap.CompressFormat.JPEG);
            try {
                Log.d("Test","Getting the file ");

                List<lumidigm.captureandmatch.entity.User> users = appDatabase.userDao().getAll();

                if (users != null) {
                    Log.d(TAG, "Get User Count is " + users.size());

                    String userID = (users.get(0).get_id());

                    String filename = Environment.getExternalStorageDirectory()
                            + File.separator
                            + "displayImages" + File.separator + userID + ".jpg";

                    try {
                        File f = new File(filename);

                        if (f.exists())
                            f.delete();
                    } catch (Exception obj) {

                    }
                    Log.d(TAG,"File Name is "+filename);

                    FileOutputStream out = new FileOutputStream(filename,false);
					/*try {
						out.write(("").getBytes());
					} catch (Exception Obj) {
						Log.d(TAG,"Clear file exception");
					}*/
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

                    Log.d(TAG, "Update File ");
                    // bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
                    //bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                    out.close();
                } else
                    Log.d(TAG,"Not found user ");
            } catch (Exception obj) {
                Log.d("Test","Got the exception ");
            }
            //imageview.setImageBitmap(bmp);
            Log.d("data", "data from server"+ data);
        }
    };


     @Override
    protected void onStart() {
        super.onStart();

        if (FaceICA == true) {
            IntentFilter intentFilter = new IntentFilter("lumidigm.captureandmatch"); //lumidigm.captureandmatch");
            registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (FaceICA == true)
            unregisterReceiver(broadcastReceiver);
    }
    private void GotoFaceInProgress(){
        Intent intent = new Intent(this, FacePhotoReadProgress.class);
        startActivity(intent);
        finish();

//        finish();
    }
}
