package com.luxand.facerecognition;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.android.volley.VolleyError;

//import facerecognition.FSDK;//
//import facerecognition.FSDK.HTracker;
import database.AppDatabase;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceSettings;
import lumidigm.captureandmatch.activites.Main2Activity;
//import lumidigm.ftreaderexa.FirstActivity;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import frost.timeandattendance.TerminalFaceCheckInActivity;
import webserver.GlobalVariables;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import com.luxand.FSDK;
import com.luxand.FSDK.HTracker;

import static com.android.volley.Request.Method.GET;
import static com.luxand.facerecognition.MainActivity.EnrolmentStatus;

import  android.content.pm.PackageManager;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	private static final String TAG = "MainActivity";
	private boolean mIsFailed = false;
	private Preview mPreview;
	private ProcessImageAndDrawResults mDraw;
	private final String database = "Memory50.dat";
	private final String help_text = "Luxand Face Recognition\n\nJust tap any detected face and name it. The app will recognize this face further. For best results, hold the device at arm's length. You may slowly rotate the head for the app to memorize you at multiple views. The app can memorize several persons. If a face is not recognized, tap and name it again.\n\nThe SDK is available for mobile developers: www.com.luxand.com/facesdk";
	public static float sDensity = 1.0f;
	private boolean fromWeb=false;
	private String currState = null;
	private SharedPreferences prefs =  null;
	public static String userID=null;
	//private String TAG="FaceDetection";
	private boolean CameraStatus = false;
	public  static boolean EnrolmentStatus = false;
	public   static TextView tpView;
	public static TextView tpView0;
	public static View buttons2;
	public static LayoutInflater inflater;
	public static lumidigm.captureandmatch.entity.User user=null;
	public static AppDatabase appDatabase;
	public static Integer comeOut=0;
	public static boolean adminVerification=false;
	public static boolean adminVerified = false;
	public static String IdentifiedUserName =null;
    android.support.v7.app.AlertDialog alertDialog =null;
	//private long lastSystemTime = 0;
	//public FSDK.FSDK_IMAGEMODE mCameraImageMode = new FSDK.FSDK_IMAGEMODE();
	public void showErrorAndClose(String error, int code) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(error + ": " + code)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			})
			.show();		
	}
	
	public void showMessage(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
				}
			})
			.setCancelable(false) // cancel with button only
			.show();		
	}
	
    private void resetTrackerParameters() {
	    int errpos[] = new int[1];
       FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "ContinuousVideoFeed=true;FacialFeatureJitterSuppression=0;RecognitionPrecision=1;Threshold=0.996;Threshold2=0.9995;ThresholdFeed=0.97;MemoryLimit=2000;HandleArbitraryRotations=false;DetermineFaceRotationAngle=false;InternalResizeWidth=70;FaceDetectionThreshold=3;", errpos);
		//FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "HandleArbitraryRotations=false;DetectExpression=true;DetermineFaceRotationAngle=false;InternalResizeWidth=100;FaceDetectionThreshold=5", errpos);
        if (errpos[0] != 0) {
            showErrorAndClose("Error setting tracker parameters, position", errpos[0]);
        }
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		//setContentView(R.layout.launch_activity);
		sDensity = getResources().getDisplayMetrics().scaledDensity;

		prefs = getBaseContext().getSharedPreferences(
				GlobalVariables.SP_FingerPrint, MODE_PRIVATE);
		comeOut=0;
		IdentifiedUserName =null;
		adminVerified = false;
		adminVerification = false;
		try {
			try {
				fromWeb = getIntent().getBooleanExtra("FromWeb", false);
				currState = getIntent().getStringExtra("CurrState");
				Log.d("MainActivity", "Curr State is " + currState);
			} catch (Exception tpexcep) {
				currState=null;
			}

			try {
				EnrolmentStatus = getIntent().getBooleanExtra("Enrolment", false);
				Log.d(TAG,"Face Recognition enrolment Status is "+EnrolmentStatus);

			} catch (Exception obj) {

			}
			try {
				adminVerification=false;
				adminVerification = getIntent().getBooleanExtra("adminVerification", false);
				Log.d(TAG,"Face  Admin Verification Status is "+adminVerification);

			} catch (Exception obj) {

			}
			try {

				//ldapUser = getIntent().getExtras().getString("ldapUser");
				userID = getIntent().getExtras().getString("userid");
				user = getIntent().getExtras().getParcelable("userData");
				Log.d(TAG,"Face Recognition "+userID);
				Log.d(TAG,"Face Name is "+user.getFullname());

			} catch (Exception obj) {

			}
			appDatabase = AppDatabase.getAppDatabase(MainActivity.this);
			List<lumidigm.captureandmatch.entity.User> users = appDatabase.userDao().getAll();

			//intent.putExtra("userid", editText.getText().toString());
			Log.d(TAG,"Curr State of Camera is "+currState);
			if (currState != null &&  currState.equals("success")) {
				CameraStatus = false;

				//CameraStatus = false;
				GlobalVariables.personDescription="face matching";
				GlobalVariables.reqStatus= true;

				Log.d(TAG,"Time is more than 20 seconds - Not Right");
				Intent intent = new Intent(this,FaceSuccessActivity.class);//SuccessActivity
				intent.putExtra("case","success");
				intent.putExtra("FromWeb",true);

				intent.putExtra("source","face");
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */

				this.startActivity(intent);
			} else if (currState != null && currState.equals("failure")) {
				CameraStatus = false;
				GlobalVariables.personDescription="face not matching";
				GlobalVariables.reqStatus= true;

				Log.d(TAG,"Time is more than 20 seconds - Not Right");
				Intent intent = new Intent(this,FaceFailureActivity.class);//SuccessActivity
				intent.putExtra("case","failure");
				intent.putExtra("FromWeb",true);

				intent.putExtra("source","face");
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","failure");
				mBundle.putBoolean("FromWeb",true);
*/
				this.startActivity(intent);


			} else {
				Log.d(TAG, "Checking Camera");
				Camera cp = null;
				//cp = Camera.open(0);
				//cp.release();
				Log.d(TAG, "Camera Status checking ");
				PackageManager pm = this.getPackageManager();
				if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
					CameraStatus = true;
					Log.d(TAG,"Camer Status istrue for Feature ANY CAMERA");
				} else {
					CameraStatus = true;
					Log.d(TAG,"Camer Status istrue for Feature ANY CAMERA not true");
				}

				//lastSystemTime = System.currentTimeMillis();

				//lastSystemTime = 0;
				//cp.release();
				//Camera.release();
				//CameraStatus =true;
				Log.d(TAG, "Camera is AVailable");
			}
		} catch (Exception ojf) {
			CameraStatus = false;
			Log.d(TAG, "Camera Status                                                         if false" + ojf.getMessage());
		}
		//int res = FSDK.ActivateLibrary("Uw3aCK45gqXYjd7GNHf60l7D3oDd02rdxPqpfyf4gqVryuSwAvrct7VmifKjr7gEWuJP76R+dcuDUVInAvXsd7kwOQwoLA9eD/qvUyVZaeFGjpd0quslvHdLLKr719vn/1EoqGj3aPn6vsDZIPbqv6WicPxS6XniNvR20x7oEAw=");

		try {
			if (CameraStatus) {
				Log.d(TAG,"Before Activate Library");
				int res = FSDK.ActivateLibrary(("EMvnS0fIF7exIRspEoQuM7Io1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISljUOqA="));
			//	int res = FSDK.ActivateLibrary("OfuGFjnj3oJ5xekQC0FSoMmbaGlC6qtsC9YccC65G0aq8Q6OpJJUG07Y47zkTW0ILsLEiuEw5W4GylYgeqkUCcci1199GRvvqEvKS9hinpbVgoXiWZ5EmgCUThta0Ja6Pbk0IvSF3DGfardNSw8GX+F9UVObiIZUdmUCDY8crTs=");
				//int res = FSDK.ActivateLibrary("EMvnS0flF7exlRspEoQuM7lo1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISIjUOqA=");
				///int res = FSDK.ActivateLibrary("EMvnS0flF7exlRspEoQuM7lo1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHlSljUOqA=");
				//int res = FSDK.ActivateLibrary("EMvnS0fIF7exIRspEoQuM7Io1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POIhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISIjUOqA=");
				prefs = getBaseContext().getSharedPreferences(
						GlobalVariables.SP_FingerPrint, MODE_PRIVATE);
				if (res != FSDK.FSDKE_OK) {
					mIsFailed = true;
					showErrorAndClose("FaceSDK activation failed", res);
				} else {


					if (CameraStatus) {
						Log.d(TAG,"Before FSDK Initialize ");
						FSDK.Initialize();

						// Hide the window title (it is done in manifest too)
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
						requestWindowFeature(Window.FEATURE_NO_TITLE);

						// Lock orientation
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

						// Camera layer and drawing layer
						mDraw = new ProcessImageAndDrawResults(this);
						mPreview = new Preview(this, mDraw);
						mDraw.mTracker = new HTracker();

						Log.d(TAG,"mpReview is created ");
						String templatePath = this.getApplicationInfo().dataDir + "/" + database;
						Log.d(TAG,"Before Load Memory "+templatePath);
						if (FSDK.FSDKE_OK != FSDK.LoadTrackerMemoryFromFile(mDraw.mTracker, templatePath)) {
							res = FSDK.CreateTracker(mDraw.mTracker);
							if (FSDK.FSDKE_OK != res) {
								showErrorAndClose("Error creating tracker", res);
							}
						}

						resetTrackerParameters();

						this.getWindow().setBackgroundDrawable(new ColorDrawable()); //black background

						setContentView(mPreview); //creates MainActivity contents
						addContentView(mDraw, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

						// Menu
						//if (EnrolmentStatus)
						{
                            LayoutInflater inflater1 = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						View buttons = inflater1.inflate(R.layout.bottom_menu, null);
						if (EnrolmentStatus)
							buttons = inflater1.inflate(R.layout.bottom_menu_enroll, null);

						//buttons.findViewById(R.id.helpButton).setOnClickListener(this);
						//buttons.findViewById(R.id.clearButton).setOnClickListener(this);
							TextView tpView1 = buttons.findViewById(R.id.textViewFaceHeader); //facetextview);
                            TextView tpView2 = buttons.findViewById(R.id.textViewFace);
						//	if (!EnrolmentStatus)
						//	tpView1.setText("...");

							addContentView(buttons, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
						//Chand added

							//buttons.setEnabled(false);

							Log.d(TAG,"For Buttons TOp");
							View buttons3 = inflater1.inflate(R.layout.top_menu, null);
                            //buttons2. = (Button) findViewById(R.id.settings_new);
						    MainActivity.inflater = inflater1;
						    MainActivity.buttons2 = buttons;
						    MainActivity.tpView = tpView1;
						    MainActivity.tpView0=tpView2;

								buttons3.findViewById(R.id.settings_new).setOnClickListener(this);
						//	buttons2.findViewById(R.id.clearButton1).setOnClickListener(this);

							addContentView(buttons3, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));


						}
					} else
						Log.d(TAG, "No Camera ");
				}
			}
		} catch (Exception objd) {

		}
	}

	@Override
	public void onClick(View view) {

        if (view.getId() == R.id.settings_new) {
            //showMessage(help_text);
            Log.d(TAG, "Initiate Face Settings ");
           // startSettings();

	//		Log.d("FaceDetction","GoBack to Home Activity");
			Intent intent= new Intent(this, FaceSettings.class);
		//		Intent intent= new Intent(this, AdminVerificationActivityMobile.class);
			//Intent intent= new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			this.startActivity(intent);
			finishAffinity();
        } /*else
            if (view.getId() == R.id.helpButton) {
			showMessage(help_text);
			Log.d(TAG,"Initiate Enroll ");

		} else if (view.getId() == R.id.clearButton) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Are you sure to clear the memory?" )
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
                    public void onClick(DialogInterface dialogInterface, int j) {
						pauseProcessingFrames();
						FSDK.ClearTracker(mDraw.mTracker);
						resetTrackerParameters();
						resumeProcessingFrames();
					}
				})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
                    public void onClick(DialogInterface dialogInterface, int j) {
					}
				})
				.setCancelable(false) // cancel with button only
				.show();
		}*/
	}
    private void startSettings() {

        Log.d(TAG,"In SHow Licence App");
        LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText1);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Android Settings");
        builder.setMessage("Password Verification");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordText = subEditText.getText().toString().trim();
                Log.d(TAG,"Password Text is "+passwordText);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                String settingPwd = "123"; //getsettingspwd();
                Log.d(TAG,"App Pwd is "+settingPwd);
                if (passwordText.equals("argus@542")) {
                    Log.d(TAG,"Going for Shutdown");
                    //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
                    //Shutdown();
                } else
                if (passwordText.equals("argus@543")) {
                    Log.d(TAG,"Going for Reboot");
                    //reboot();
                } else
                if (passwordText.equals(settingPwd)) {

                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    finish();

                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

                    // finish();
                }
            }
        });



        builder.show();
    }

	@Override
	public void onPause() {
		super.onPause();
		if (CameraStatus) {
			pauseProcessingFrames();
			String templatePath = this.getApplicationInfo().dataDir + "/" + database;
			FSDK.SaveTrackerMemoryToFile(mDraw.mTracker, templatePath);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (mIsFailed)
            return;
		if (CameraStatus)
        	resumeProcessingFrames();
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		if (fromWeb) {
			Log.d("FaceDetction","GoBack to Home Activity");
			Intent intent= new Intent(this, Main2Activity.class);
			//Intent intent= new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

			this.startActivity(intent);
			finish();
		}
		super.onBackPressed();
	}

	public boolean SendingMessage(String status, String requestType) {
		Log.d(TAG,"SendMessage "+status);
		//String url="http://192.168.43.24:8080/?username="+status;


		String remoteIP = getRemoteIP();
		if (remoteIP == null || remoteIP == "") {
			Log.d(TAG," No IP ");
			return false;
		}
		if (!isEthernetConnected()) {
			if (!isConnectedInWifi()) {
				Log.d(TAG,"Network not connected");
				return false;
			}
		}


		Log.d(TAG,"Getting IP Address");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
		String url="http://"+remoteIP+":8080/?username="+status;

		Log.d(TAG, "send url: "+url);
		new VolleyService(new IResult() {
			@Override
			public void notifySuccess(String requestType, Object response) {

				Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
				/*if (requestType.equals("100")) {
					//Intent i= new Intent(this, audio.MainActivity.class);

					Intent i = new Intent(MainActivity.this, audio.MainActivity.class);

					startActivity(i);
				} else if (requestType.equals("101")) {
					Intent i = new Intent(MainActivity.this, FingerScannActivity.class);
					startActivity(i);
				} else if (requestType.equals("102")) {
					Intent i = new Intent(MainActivity.this, FirstActivity.class);
					startActivity(i);
				} else if (requestType.equals("103")) {
					Intent i = new Intent(MainActivity.this, audio.MainActivity.class);
					startActivity(i);
				} else if (requestType.equals("104")) {
					Intent i = new Intent(MainActivity.this, HomeActivity.class);
					startActivity(i);
				}*/
			}


			@Override
			public void notifyError(String requestType, VolleyError error) {
				Log.d(TAG, "notifyError: "+error.getMessage());
				try {
					//String fingerprint = "testfingerprint";
					Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
					String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
					//           playAudioFile(2,filePath);
			/*		if (requestType.equals("100")) {
						//Intent i= new Intent(this, audio.MainActivity.class);

						Intent i = new Intent(MainActivity.this, audio.MainActivity.class);

						startActivity(i);
					} else if (requestType.equals("101")) {
						Intent i = new Intent(MainActivity.this, FingerScannActivity.class);
						startActivity(i);
					} else if (requestType.equals("102")) {
						Intent i = new Intent(MainActivity.this, FirstActivity.class);
						startActivity(i);
					} else if (requestType.equals("103")) {
						Intent i = new Intent(MainActivity.this, audio.MainActivity.class);
						startActivity(i);
					} else if (requestType.equals("104")) {
						Intent i = new Intent(MainActivity.this, HomeActivity.class);
						startActivity(i);
					}*/

				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}, this).tokenBase(GET, url, null, requestType);

		return true;
	}
	private Boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}

	public Boolean isWifiConnected(){
		if(isNetworkAvailable()){
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
		}
		return false;
	}

	public Boolean isEthernetConnected(){
		if(isNetworkAvailable()){
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
		}
		return false;
	}



	private String getRemoteIP() {
		String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
				"");

		Log.d("MainActivity", "Remote IP Addr is " + remoteIPAddr);

		return remoteIPAddr;
	}
	public boolean isConnectedInWifi() {

		try {
			WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
			NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
					&& wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
				return true;
			}
		} catch (Exception obj) {
			Log.d("MainActivity","Got exception "+obj.toString());
		}
		return false;
	}

	private void pauseProcessingFrames() {
		mDraw.mStopping = 1;
		
		// It is essential to limit wait time, because mStopped will not be set to 0, if no frames are feeded to mDraw
		for (int i=0; i<100; ++i) {
			if (mDraw.mStopped != 0) break; 
			try { Thread.sleep(10); }
			catch (Exception ex) {}
		}
	}
	
	private void resumeProcessingFrames() {
		mDraw.mStopped = 0;
		mDraw.mStopping = 0;
	}
}


class FaceRectangle {
	public int x1, y1, x2, y2;
}

// Draw graphics on top of the video
class ProcessImageAndDrawResults extends View {
	private static final String TAG = "ProcessImageAndDrawResu";
	public HTracker mTracker;
	
	final int MAX_FACES = 5;
	final FaceRectangle[] mFacePositions = new FaceRectangle[MAX_FACES];
	final long[] mIDs = new long[MAX_FACES];
	final Lock faceLock = new ReentrantLock();
	int mTouchedIndex;
	boolean popup = false;
	long mTouchedID;
	int mStopping;
	int mStopped;
	
	Context mContext;
	Paint mPaintGreen, mPaintBlue, mPaintBlueTransparent;
	byte[] mYUVData;
	byte[] mRGBData;
	int mImageWidth, mImageHeight;
	boolean first_frame_saved;
	boolean rotated;
	private int handlercount = 0;
	private long  lastSystemTime=0;
	//private FSDK.FSDK_IMAGEMODE mCameraImageMode = new FSDK.FSDK_IMAGEMODE();
	//lastSystemTime = System.currentTimeMillis();
	int GetFaceFrame(FSDK.FSDK_Features Features, FaceRectangle fr)
	{
		if (Features == null || fr == null)
			return FSDK.FSDKE_INVALID_ARGUMENT;

	    float u1 = Features.features[0].x;
	    float v1 = Features.features[0].y;
	    float u2 = Features.features[1].x;
	    float v2 = Features.features[1].y;
	    float xc = (u1 + u2) / 2;
	    float yc = (v1 + v2) / 2;
	    int w = (int) Math.pow((u2 - u1) * (u2 - u1) + (v2 - v1) * (v2 - v1), 0.5);
	    
	    fr.x1 = (int)(xc - w * 1.6 * 0.9);
	    fr.y1 = (int)(yc - w * 1.1 * 0.9);
	    fr.x2 = (int)(xc + w * 1.6 * 0.9);
	    fr.y2 = (int)(yc + w * 2.1 * 0.9);
	    if (fr.x2 - fr.x1 > fr.y2 - fr.y1) {
	        fr.x2 = fr.x1 + fr.y2 - fr.y1;
	    } else {
	        fr.y2 = fr.y1 + fr.x2 - fr.x1;
	    }

		return 0;
	}
	
	
	public ProcessImageAndDrawResults(Context context) {
		super(context);
		
		mTouchedIndex = -1;
		
		mStopping = 0;
		mStopped = 0;
		rotated = false;
		mContext = context;
		mPaintGreen = new Paint();
		mPaintGreen.setStyle(Paint.Style.FILL);
		mPaintGreen.setColor(Color.GREEN);
		mPaintGreen.setTextSize(18 * MainActivity.sDensity);
		mPaintGreen.setTextAlign(Align.CENTER);
		mPaintBlue = new Paint();
		mPaintBlue.setStyle(Paint.Style.FILL);
		mPaintBlue.setColor(Color.BLUE);
		mPaintBlue.setTextSize(18 * MainActivity.sDensity);
		mPaintBlue.setTextAlign(Align.CENTER);
		
		mPaintBlueTransparent = new Paint();
		mPaintBlueTransparent.setStyle(Paint.Style.STROKE);
		mPaintBlueTransparent.setStrokeWidth(2);
		mPaintBlueTransparent.setColor(Color.BLUE);
		mPaintBlueTransparent.setTextSize(25);
		
		//mBitmap = null;
		mYUVData = null;
		mRGBData = null;
		
		first_frame_saved = false;
    }

	@Override
	protected void onDraw(Canvas canvas) {
		Log.d("Preview","onDraw being called ");
		lumidigm.captureandmatch.entity.User userData=null;
		if (mStopping == 1) {
			mStopped = 1;
			super.onDraw(canvas);
			//onFinishInflate();
			//MainActivity.finish();
			return;
		}
		
		if (mYUVData == null || mTouchedIndex != -1) {
			super.onDraw(canvas);
			return; //nothing to process or name is being entered now
		}
		
		int canvasWidth = canvas.getWidth();
		//int canvasHeight = canvas.getHeight();

		if (lastSystemTime ==0) {
		//	lastSystemTime++;
			lastSystemTime= System.currentTimeMillis();
		} else {
			final long diff = (System.currentTimeMillis() - lastSystemTime) / 1000;

			Log.d(TAG,"Draw Curr Diff is "+diff);

		/*	if ((diff > 30) && MainActivity.EnrolmentStatus) {
				handlercount = 0;
				mStopping = 1;
			//	MainActivity.onBackPressed();
			} else*/
			//if ((diff > 8) && (!MainActivity.EnrolmentStatus)) {
/*
				if ((diff > 30) && (!MainActivity.EnrolmentStatus)) {
			//if ((diff > 15)) {




						GlobalVariables.personDescription="face not matching";
				GlobalVariables.reqStatus= true;


				Log.d(TAG,"Time is more than 20 seconds - Not Right New");
				Intent intent = new Intent(getContext(),SuccessActivity.class);
				intent.putExtra("case","failure");
				intent.putExtra("FromWeb",false);
				*/
/*Bundle mBundle = new Bundle();
				mBundle.putString("case","failure");
				mBundle.putBoolean("FromWeb",false);*//*

				getContext().startActivity(intent);
				handlercount = 0;
				mStopping = 1;

			}
*/
		}


		// Convert from YUV to RGB
		decodeYUV420SP(mRGBData, mYUVData, mImageWidth, mImageHeight);

		//30 frames
		//if (lastSystemTime > 30*20) {
		//	Log.d(TAG,"Last System Time is "+lastSystemTime);
		//}
		// Load image to FaceSDK
		Log.d(TAG,"Before FSDK.HImage");
		FSDK.HImage Image = new FSDK.HImage();
		FSDK.FSDK_IMAGEMODE imagemode = new FSDK.FSDK_IMAGEMODE();
		imagemode.mode = FSDK.FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_24BIT;

		Log.d(TAG,"Before LoadImageFromBuffer");
		FSDK.LoadImageFromBuffer(Image, mRGBData, mImageWidth, mImageHeight, mImageWidth*3, imagemode);
		FSDK.MirrorImage(Image, false);
		FSDK.HImage RotatedImage = new FSDK.HImage();
		FSDK.CreateEmptyImage(RotatedImage);

		//it is necessary to work with local variables (onDraw called not the time when mImageWidth,... being reassigned, so swapping mImageWidth and mImageHeight may be not safe)
		int ImageWidth = mImageWidth;
		//int ImageHeight = mImageHeight;
		if (rotated) {
			ImageWidth = mImageHeight;
			//ImageHeight = mImageWidth;
			FSDK.RotateImage90(Image, -1, RotatedImage);
		} else {
			FSDK.CopyImage(Image, RotatedImage);
		}
		FSDK.FreeImage(Image);

		// Save first frame to gallery to debug (e.g. rotation angle)
		/*
		if (!first_frame_saved) {				
			first_frame_saved = true;
			String galleryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
			FSDK.SaveImageToFile(RotatedImage, galleryPath + "/first_frame.jpg"); //frame is rotated!
		}
		*/
		
		long IDs[] = new long[MAX_FACES];
		long face_count[] = new long[1];
		
		FSDK.FeedFrame(mTracker, 0, RotatedImage, face_count, IDs);
		FSDK.FreeImage(RotatedImage);
		Log.d(TAG,"FreeImage FeedFrame");
		faceLock.lock();
			
		for (int i=0; i<MAX_FACES; ++i) {
			mFacePositions[i] = new FaceRectangle();
			mFacePositions[i].x1 = 0;
			mFacePositions[i].y1 = 0;
			mFacePositions[i].x2 = 0;
			mFacePositions[i].y2 = 0;
			mIDs[i] = IDs[i];
		}
		
		float ratio = (canvasWidth * 1.0f) / ImageWidth;


		Log.d(TAG,"Face Count is "+face_count[0]);
		for (int i = 0; i < (int)face_count[0]; ++i) {
			FSDK.FSDK_Features Eyes = new FSDK.FSDK_Features(); 
			FSDK.GetTrackerEyes(mTracker, 0, mIDs[i], Eyes);
		
			GetFaceFrame(Eyes, mFacePositions[i]);
			mFacePositions[i].x1 *= ratio;
			mFacePositions[i].y1 *= ratio;
			mFacePositions[i].x2 *= ratio;
			mFacePositions[i].y2 *= ratio;

			Log.d(TAG,"Set FaceFrame ");
		}
		
		faceLock.unlock();
		
		int shift = (int)(22 * MainActivity.sDensity);

		// Mark and name faces
		Log.d(TAG,"For Mark and Name face facecount "+face_count[0]);

		for (int i=0; i<face_count[0]; ++i) {
			canvas.drawRect(mFacePositions[i].x1, mFacePositions[i].y1, mFacePositions[i].x2, mFacePositions[i].y2, mPaintBlueTransparent);
			
			boolean named = false;
			String names[] = new String[1];
			if (IDs[i] != -1) {

				FSDK.GetAllNames(mTracker, IDs[i], names, 1024);
				Log.d(TAG,"GetALlNames length"+names[0].length());
				if (names[0] != null && names[0].length() > 0) {
					Log.d(TAG, "onDraw: "+names[0]);

					Log.d(TAG,"Before setting text view "+names[0]);
					try {

						String username=null;
						try {
							List<lumidigm.captureandmatch.entity.User> users = MainActivity.appDatabase.userDao().getAll();
							Log.d(TAG, "Get User Count is " + users.size());
							for (lumidigm.captureandmatch.entity.User user1 : users) {

								String userId1 = user1.getUserId();
								Log.d(TAG, "Rcv d User id is " + userId1);
								if (userId1.equals(names[0])) {
									username = user1.getFullname();
									Log.d(TAG, "Matching User ID is  " + userId1 + " Name is " + user1.getFullname());
									//}
									///user = user1;
									userData = user1;
									if (MainActivity.adminVerification == true) {
										Integer RoleID = user1.getRoleID();
										String rolename = user1.getRoleName();
										Log.d(TAG, "Role ID of user " + userId1 + " Role ID " + RoleID + "Role name is " + rolename);
										if (RoleID == 3) {
											Log.d(TAG, "Role ID is matching ");
											MainActivity.adminVerified = true;
										} else
											Log.d(TAG, "Role ID is not verified ");

									}
									MainActivity.IdentifiedUserName=username;
								}
								//Log.d(TAG, "doInBackground: " + user.toString());
								//String fingarprint1 = user.getTemplate1();
							}
							//For testing
							if (users.size() ==0)
							MainActivity.adminVerified = true;
						} catch (Exception obj) {
							Log.d(TAG,"Got Exception ");
							//for testing

						}

					/*	LayoutInflater inflater = (LayoutInflater) MainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						View buttons = inflater.inflate(R.layout.bottom_menu, null);
						//buttons.findViewById(R.id.helpButton).setOnClickListener(this);
						//buttons.findViewById(R.id.clearButton).setOnClickListener(this);
						MainActivity.addContentView(buttons, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
						//Chand added

						//buttons.setEnabled(false);

						Log.d(TAG,"For Buttons TOp");
						View buttons2 = inflater.inflate(R.layout.top_menu, null);

						MainActivity.tpView = buttons2.findViewById(R.id.facetextview);
						tpView.setText("First One");
						//	buttons2.findViewById(R.id.helpButton1).setOnClickListener(this);
						//	buttons2.findViewById(R.id.clearButton1).setOnClickListener(this);

						MainActivity.addContentView(buttons2, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
*/
						Log.d(TAG,"Before inflater");
						//MainActivity.buttons2 = MainActivity.inflater.inflate(R.layout.top_menu, null);
						//MainActivity.tpView = MainActivity.buttons2.findViewById(R.id.facetextview);
						Log.d(TAG, "Before settext ");



/*
						if (MainActivity.adminVerification == true && MainActivity.adminVerified == true)
							MainActivity.tpView0.setText("Your Admin Role is successfully verified");
						else
						if (MainActivity.adminVerification == true && MainActivity.adminVerified == false)
							MainActivity.tpView0.setText("Your Admin Role is unAuthorized");
						else*/
                        if (EnrolmentStatus == true)
                            MainActivity.tpView0.setText("Your Face is successfully Enrolled");
                        else
							MainActivity.tpView0.setText("Your Face is successfully Identified");

						Log.d(TAG,"Username is "+username+" user id is "+names[0]);

                        if (username != null)
							MainActivity.tpView.setText(username);
					//	MainActivity.tpView.setText(names[0]+" "+username);
                        else
							MainActivity.tpView.setText(names[0]);

					} catch (Exception ojb) {
					Log.d(TAG,"Got Exception while Textview setting "+ojb.getMessage());
					}

					canvas.drawText(names[0], (mFacePositions[i].x1+mFacePositions[i].x2)/2, mFacePositions[i].y2+shift, mPaintBlue);
					named = true;
				} else if (EnrolmentStatus == true) {
					Log.d(TAG,"Going for Face Enrollment ");
				}
				else {
					Log.d(TAG,"Not Valid User");
					MainActivity.tpView0.setText("You are not Authorized User");

					GlobalVariables.personDescription="face not matching";
					Log.d(TAG,"Face not Matching is success new");
					GlobalVariables.reqStatus= true;
					Log.d(TAG,"Come out is "+MainActivity.comeOut);
					if (MainActivity.comeOut == 0) {
						MainActivity.comeOut = 1;
/*

						Handler handler = new Handler();
						handler.postDelayed(new Runnable() {
							@Override
							public void run() {
*/

								Log.d(TAG, "Face identification Failure");


								//String descoveredID = names[0];

								Intent intent = new Intent(getContext(), FaceFailureActivity.class); //SuccessActivity

								//There is a change in the Identified User Name
								if (MainActivity.IdentifiedUserName != null) {

									Log.d(TAG,"Valid User "+MainActivity.IdentifiedUserName);
									intent = new Intent(getContext(), FaceSuccessActivity.class);

									if (MainActivity.adminVerification == true && MainActivity.adminVerified == true) {

										Log.d(TAG,"Going for Face Enroll ");

										//intent = new Intent(getContext(), FaceEnroll.class);
										//intent.putExtra("adminverification","success");


										//intent = new Intent(getContext(), Enroll.class);

										//intent.putExtra("userid","test" );
										intent.putExtra("adminVerification",true );

									}  else
									if (!EnrolmentStatus ) {
										Log.d(TAG,"Enrollment Status if false - TerminalFaceCheckInActivity ");
										if (GlobalVariables.WorkFlowVal != 4 && GlobalVariables.WorkFlowVal != 5) {

											intent = new Intent(getContext(), TerminalFaceCheckInActivity.class);
										 } else
										 	Log.d(TAG,"Work Flow Status is TIme and attendance ");
										Log.d(TAG,"CheckIn");
									//	Intent i = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);
										//finish();

										List<lumidigm.captureandmatch.entity.User> users = MainActivity.appDatabase.userDao().getAll();
										Log.d(TAG, "Get User Count is " + users.size());
										lumidigm.captureandmatch.entity.User userData2 = null;
										for (lumidigm.captureandmatch.entity.User user1 : users) {

											String userId1 = user1.getUserId();
											Log.d(TAG, "Rcv d User id is " + userId1);
											if (userId1.equals(names[0])) {
											//	username = user1.getFullname();
												Log.d(TAG, "Matching User ID is  " + userId1 + " Name is " + user1.getFullname());
												//}
												///user = user1;
												userData2 = user1;
											}
										}
										//final lumidigm.captureandmatch.entity.User curUser =userData;

										Bundle bundle = new Bundle();
										if (userData2 != null)
										bundle.putParcelable("User", userData2);

										intent.putExtras(bundle);
										//  Intent intent = new Intent(this,CheckInActivity.class);
										//startActivity(i);
									}

									intent.putExtra("personName",MainActivity.IdentifiedUserName);

								}
								intent.putExtra("FromWeb", false);


								if (MainActivity.adminVerification == true && MainActivity.adminVerified == false) {

									Log.d(TAG,"AdminVerification Failure ");

									intent.putExtra("adminVerification",true); //to Show Unauthorized access
									intent.putExtra("case","failure");

								} else
								if (EnrolmentStatus)
									intent.putExtra("case", "forenrollment");
								else
									intent.putExtra("case", "failure");


								intent.putExtra("source","face");

								//intent.putExtra("personID", descoveredID);
								getContext().startActivity(intent);
								// getContext().finish();

								mStopping = 1;

			/*				}
						}, 50); //5000);*/
					}
					//Chand added


				}
			}

			if(named){
			//if (named && !MainActivity.EnrolmentStatus) {
				GlobalVariables.personDescription="face matching";
				Log.d(TAG,"Face Matching is success new");
				GlobalVariables.reqStatus= true;
 				Log.d(TAG,"Come out is "+MainActivity.comeOut);
				final lumidigm.captureandmatch.entity.User curUser =userData;
				Log.d(TAG,"Curr User Data is "+userData.getFullname());
				if (MainActivity.comeOut == 0) {
					MainActivity.comeOut = 1;
/*

					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
*/

							Log.d(TAG, "Face identification success scucess");


							String descoveredID = names[0];


							Intent intent = new Intent(getContext(), FaceSuccessActivity.class); //SuccessActivity

							Log.d(TAG,"Work Flow status is "+GlobalVariables.WorkFlowVal);


							if (EnrolmentStatus)
								intent.putExtra("case", "forenrollment");
							else if (MainActivity.adminVerification == true && MainActivity.adminVerified == false) {

								Log.d(TAG, "AdminVerification Failure ");
								intent = new Intent(getContext(), FaceFailureActivity.class);
								intent.putExtra("adminVerification", true);
								intent.putExtra("case", "failure");
							} else if (MainActivity.adminVerification == true && MainActivity.adminVerified == true) {

								Log.d(TAG, "Going for Face Enroll ");

							//	intent = new Intent(getContext(), FaceEnroll.class);
								intent.putExtra("case", "success");
								intent.putExtra("adminVerification", true);
							}
							else {
							//test
								if (GlobalVariables.WorkFlowVal != 4 && GlobalVariables.WorkFlowVal != 5) {
									intent = new Intent(getContext(), TerminalFaceCheckInActivity.class);
								}
								Log.d(TAG,"Sending CheckIn");
								Bundle bundle = new Bundle();
								if (curUser != null)
									bundle.putParcelable("User", curUser);

								intent.putExtras(bundle);

								//below
								intent.putExtra("case", "success");

							}
							intent.putExtra("FromWeb", false);
/*
							if (MainActivity.EnrolmentStatus)
								intent.putExtra("case", "forenrollment");
							else*/


							intent.putExtra("personID", descoveredID);

							intent.putExtra("source", "face");

							Bundle bundle = new Bundle();

							if (curUser != null) {
								Log.d(TAG,"User Full name "+curUser.getFullname());
								bundle.putParcelable("User", curUser);
								intent.putExtras(bundle);
							} else
								Log.d(TAG,"Curr User is null");


 							getContext().startActivity(intent);
							// getContext().finish();

							mStopping = 1;

							MainActivity.comeOut = 1;
/*
						}
					}, 10);//10
*/
					//}, 4000);
				}
				//Chand added

			}else {
				//Chand added
				if (EnrolmentStatus)
				{
			//		canvas.drawText("Tap to Enroll", (mFacePositions[i].x1 + mFacePositions[i].x2) / 2, mFacePositions[i].y2 + shift, mPaintGreen);
					canvas.drawText("Tap to Enroll", (mFacePositions[i].x1 + mFacePositions[i].x2) / 2, mFacePositions[i].y2 + shift, mPaintBlue);
				}


             /*   String name = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("MYLABEL", "defaultStringIfNothingFound");
				if(names[0]!= null){
					if(!name.equals(names[0])){

						if(handlercount<1){
							handlercount++;
							if(popup){
								final Handler handler = new Handler();
								handler.postDelayed(new Runnable() {
									@Override
									public void run() {


										Log.d(TAG,"Face Matching is success going");
										GlobalVariables.personDescription="face matching";
										GlobalVariables.reqStatus= true;
										Intent intent = new Intent(getContext(),SuccessActivity.class);
										if (MainActivity.EnrolmentStatus)
											intent.putExtra("case","forenrollment");
										else
										intent.putExtra("case","success");
										intent.putExtra("FromWeb",true);
			 							intent.putExtra("personID",names[0]);
										getContext().startActivity(intent);
										handlercount = 0;
										mStopping = 1;
										//Chand added

									}
								}, 4000);

							}

						}

					}
				}
*/			}

		}
		
		super.onDraw(canvas);      
	} // end onDraw method

	
	@Override
	public boolean onTouchEvent(MotionEvent event) { //NOTE: the method can be implemented in Preview class

		Log.d(TAG,"onTouchEvent");
		String Userid = MainActivity.userID;
		//SharedPreferences Face;
		if (!EnrolmentStatus)
			return true;

		Log.d(TAG,"onTouchEvent"+event.getAction());
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			int x = (int)event.getX();
			int y = (int)event.getY();
			
			faceLock.lock();
			FaceRectangle rects[] = new FaceRectangle[MAX_FACES];
			long IDs[] = new long[MAX_FACES];
			for (int i=0; i<MAX_FACES; ++i) {
				rects[i] = new FaceRectangle();
				rects[i].x1 = mFacePositions[i].x1;
				rects[i].y1 = mFacePositions[i].y1;
				rects[i].x2 = mFacePositions[i].x2;
				rects[i].y2 = mFacePositions[i].y2;
				IDs[i] = mIDs[i];
			}
			faceLock.unlock();
			
			for (int i=0; i<MAX_FACES; ++i) {
				if (rects[i] != null && rects[i].x1 <= x && x <= rects[i].x2 && rects[i].y1 <= y && y <= rects[i].y2 + 30) {
					mTouchedID = IDs[i];
					popup = true;
					mTouchedIndex = i;

					Log.d(TAG,"Rcvd UserID is "+Userid);
					if (Userid  != null && Userid.length() > 0) {
						FSDK.LockID(mTracker, mTouchedID);
						String userName = Userid;
						FSDK.SetName(mTracker, mTouchedID, userName);
						Log.d(TAG,"Saved the User name "+userName);
						//PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("MYLABEL", userName).apply();
						popup = false;
						if (userName.length() <= 0)
							FSDK.PurgeID(mTracker, mTouchedID);

						FSDK.UnlockID(mTracker, mTouchedID);
						mTouchedIndex = -1;

						//Face=getApplicationWindowToken().getSharedPreferences("face",MODE_PRIVATE);
						//Face.edit().putBoolean("face", false).apply();

					} else
						Log.d(TAG,"User ID is not valid ");

					/*
					// requesting name on tapping the face	
					final EditText input = new EditText(mContext);
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setMessage("Enter person's name" )
						.setView(input)
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							@Override
                            public void onClick(DialogInterface dialogInterface, int j) {
								FSDK.LockID(mTracker, mTouchedID);
								String userName = input.getText().toString();
								FSDK.SetName(mTracker, mTouchedID, userName);
								PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("MYLABEL", userName).apply();
								popup = false;
								if (userName.length() <= 0) FSDK.PurgeID(mTracker, mTouchedID);

								FSDK.UnlockID(mTracker, mTouchedID);
								mTouchedIndex = -1;
							}
						})
						.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							@Override
                            public void onClick(DialogInterface dialogInterface, int j) {
								mTouchedIndex = -1;
								popup = false;
							}
						})
						.setCancelable(false) // cancel with button only
						.show();
					*/
					break;
				}
			}
		}
		return true;
	}
	
	static public void decodeYUV420SP(byte[] rgb, byte[] yuv420sp, int width, int height) {
		final int frameSize = width * height;
		int yp = 0;
		for (int j = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++) {
				int y = (0xff & ((int) yuv420sp[yp])) - 16;
				if (y < 0) y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}	
				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);
				if (r < 0) r = 0; else if (r > 262143) r = 262143;
				if (g < 0) g = 0; else if (g > 262143) g = 262143;
				if (b < 0) b = 0; else if (b > 262143) b = 262143;
				
				rgb[3*yp] = (byte) ((r >> 10) & 0xff);
				rgb[3*yp+1] = (byte) ((g >> 10) & 0xff);
				rgb[3*yp+2] = (byte) ((b >> 10) & 0xff);
				++yp;
			}
		}
	}  
} // end of ProcessImageAndDrawResults class


// Show video from camera and pass frames to ProcessImageAndDraw class
class Preview extends SurfaceView implements SurfaceHolder.Callback {
	Context mContext;
	SurfaceHolder mHolder;
	Camera mCamera;
	ProcessImageAndDrawResults mDraw;
	boolean mFinished;

	Preview(Context context, ProcessImageAndDrawResults draw) {
		super(context);      
		mContext = context;
		mDraw = draw;
		
		//Install a SurfaceHolder.Callback so we get notified when the underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		Log.d("Preview","Got the Preview");
	}

	//SurfaceView callback
	public void surfaceCreated(SurfaceHolder holder) {
		mFinished = false;
				
		// Find the ID of the camera
		int cameraId = 0;
		boolean frontCameraFound = false;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Log.d("Preview","Got the Preview Created "+Camera.getNumberOfCameras());

		for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
			Camera.getCameraInfo(i, cameraInfo);

			//if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				cameraId = i;
				frontCameraFound = true;
			}
		}

		Log.d("MainAct","Camera3 ID is "+cameraId);
		//frontCameraFound= true; //chand for testing
		if (frontCameraFound) {
			Log.d("Preview","Front Camera found");
			mCamera = Camera.open(cameraId);
		} else {
			Log.d("Preview","Back Camera Opened");
			mCamera = Camera.open(0);
		//	mCamera = Camera.open();
		}

		if (mCamera == null)
			Log.d("Preview","Opened Camera is null");

		if (holder == null)
			Log.d("Preview","Rcvd Holder is null");
		try {
			Log.d("Preview","setPReviewDisplay");
			mCamera.setPreviewDisplay(holder);

			Log.d("Preview","Before SetPreview Callback");
			// Preview callback used whenever new viewfinder frame is available
			mCamera.setPreviewCallback(new PreviewCallback() {
					public void onPreviewFrame(byte[] data, Camera camera) {
					if ( (mDraw == null) || mFinished )
						return;
		
					if (mDraw.mYUVData == null) {
						// Initialize the draw-on-top companion
						Camera.Parameters params = camera.getParameters();
						mDraw.mImageWidth = params.getPreviewSize().width;
						mDraw.mImageHeight = params.getPreviewSize().height;
						mDraw.mRGBData = new byte[3 * mDraw.mImageWidth * mDraw.mImageHeight]; 
						mDraw.mYUVData = new byte[data.length];			
					}

					Log.d("Preview","Before System arraycode ");
					// Pass YUV data to draw-on-top companion
					System.arraycopy(data, 0, mDraw.mYUVData, 0, data.length);
					mDraw.invalidate();
				}
			});
		} 
		catch (Exception exception) {
			Log.d("Preview","Camera Failed Exception "+exception.toString());
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setMessage("Cannot open camera" )
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						android.os.Process.killProcess(android.os.Process.myPid());
					}
				})
				.show();
			if (mCamera != null) {
				mCamera.release();
				mCamera = null;
			}
		}
	}

	//SurfaceView callback
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.
		mFinished = true;
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}
	
	//SurfaceView callback, configuring camera
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

		if (mCamera == null) return;
		Log.d("PReview","SurfaceChanged ");
		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		Camera.Parameters parameters = mCamera.getParameters();

		//Keep uncommented to work correctly on phones:
		//This is an undocumented although widely known feature
		/**/
		if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
			parameters.set("orientation", "portrait");
			mCamera.setDisplayOrientation(90); // For Android 2.2 and above
			mDraw.rotated = true;
		} else {
			parameters.set("orientation", "landscape");
			mCamera.setDisplayOrientation(0); // For Android 2.2 and above
		}
		/**/
		        int width = 0;
        int height = 0;
        try {

			// choose preview size closer to 640x480 for optimal performance
			List<Size> supportedSizes = parameters.getSupportedPreviewSizes();

			for (Size s : supportedSizes) {
				Log.d("Face","Supported sizes "+s.width+"  "+s.height);
				if ((width - 640) * (width - 640) + (height - 480) * (height - 480) >
						(s.width - 640) * (s.width - 640) + (s.height - 480) * (s.height - 480)) {

					width = s.width;
					height = s.height;
					Log.d("Face","Selected width"+width+" height "+s.height);
				}
			}
		} catch (Exception obj) {
        	Log.d("Face","Got Exception ");

		}
		//try to set preferred parameters
		try {
		    if (width*height > 0) {
                parameters.setPreviewSize(width, height);
            }
            //parameters.setPreviewFrameRate(10);
			parameters.setSceneMode(Camera.Parameters.SCENE_MODE_PORTRAIT);
			parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
			mCamera.setParameters(parameters);
		} catch (Exception ex) {
		}
		mCamera.startPreview();
		
		parameters = mCamera.getParameters();
	    Camera.Size previewSize = parameters.getPreviewSize();
	    makeResizeForCameraAspect(1.0f / ((1.0f * previewSize.width) / previewSize.height));
	}
	
	private void makeResizeForCameraAspect(float cameraAspectRatio){
		LayoutParams layoutParams = this.getLayoutParams();
		int matchParentWidth = this.getWidth();           
		int newHeight = (int)(matchParentWidth/cameraAspectRatio);
		if (newHeight != layoutParams.height) {
			layoutParams.height = newHeight;
			layoutParams.width = matchParentWidth;    
			this.setLayoutParams(layoutParams);
			this.invalidate();
		}		
	}
} // end of Preview class
