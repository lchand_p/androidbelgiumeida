package com.luxand.facerecognition;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ftsafe.DK;
import com.ftsafe.Utility;
import com.ftsafe.readerScheme.FTException;
import com.ftsafe.readerScheme.FTReader;
import com.luxand.FSDK;
import com.luxand.FSDK.HTracker;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import belgiumeida.BelgiumEidaActivity;
import belgiumeida.CardDataVariables;
import belgiumeida.carddatabase.CardUser;
import belgiumeida.carddatabase.UserCardDatabase;
import database.AppDatabase;
//import frost.
;
import belgiumeida.FacePhotoReadProgress;
import belgiumeida.FaceSuccessBelgiumEida;
import frost.FaceSuccessICA;
import frost.timeandattendance.TerminalFaceCheckInActivity;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceSettings;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.vcomdroid.VCOM;
//import sentinel.visitor_terminal_entryexit.terminal_entryexit.sentinel_activity_entryexit_user_success;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;

import static com.ftsafe.DK.USB_LOG;
import static com.luxand.facerecognition.MainActivity_FaceandFinger.EnrolmentStatus;
import static webserver.GlobalVariables.FaceBelgium;
import static webserver.GlobalVariables.FaceBelgiumTest;
import static webserver.GlobalVariables.FaceICA;
import static webserver.GlobalVariables.FaceTesting;
import static webserver.GlobalVariables.M21Support;
import static webserver.GlobalVariables.M320Support;
import static webserver.GlobalVariables.MaxFaceDetectionCount;
import static webserver.GlobalVariables.NoFingerPrintSupport;
import static webserver.GlobalVariables.ProductV4Support;
import static webserver.GlobalVariables.supervisionEnrolmentStatus;

public class MainActivity_FaceandFinger extends Activity implements OnClickListener {
	private static final String TAG = "MainAFaceandFinger";
	private boolean mIsFailed = false;
	private FaceandFingerPreview mPreview;
	private FaceandFingerProcessImageAndDrawResults mDraw;
	private final String database = "Memory50.dat";
	private final String help_text = "Luxand Face Recognition\n\nJust tap any detected face and name it. The app will recognize this face further. For best results, hold the device at arm's length. You may slowly rotate the head for the app to memorize you at multiple views. The app can memorize several persons. If a face is not recognized, tap and name it again.\n\nThe SDK is available for mobile developers: www.com.luxand.com/facesdk";
	public static float sDensity = 1.0f;
	private boolean fromWeb = false;
	private String currState = null;
	private SharedPreferences prefs = null;
	public static String userID = null;
	//private String TAG="FaceDetection";
	private boolean CameraStatus = false;
	public static boolean EnrolmentStatus = false;
	public static TextView tpView;
	public static TextView tpView0;
	public static View buttons2;
	public static LayoutInflater inflater;
//	public static lumidigm.captureandmatch.entity.User user = null;
	public static AppDatabase appDatabase;
	public static UserCardDatabase appCardDatabase;
	public static Integer comeOut = 0;
	public static boolean adminVerification = false;
	public static boolean adminVerified = false;
	public static String IdentifiedUserName = null;
	android.support.v7.app.AlertDialog alertDialog = null;
	//private long lastSystemTime = 0;
	//public FSDK.FSDK_IMAGEMODE mCameraImageMode = new FSDK.FSDK_IMAGEMODE();

	private static final int DEFAULT_PORT = 8085;
	private int mPADResult;
	boolean mIsWaitForFingerClearRunning = false;
	private boolean mCancelCapture = false;
	boolean mWaitForFingerClear = false;
	boolean mCaptureInProgress = false;
 	private Bitmap mFingerImage;
	private byte[] mTemplate;
	byte[] mProbeTemplate;

	//private int mPADResult;
	private int mTimeOut = 500;
 	private String mMatchLevel = "MEDIUM";
	private String mPADLevel = "MEDIUM";

	private boolean goingForEnroll = true;
	public static boolean comeOutofApp = false;
	private boolean notauthorized = false;
	public static boolean BioSDKInitialized = false;
	public static boolean appStopped = false;
	public static int workflowID = 1;
	private boolean adminVerificationInitiated = false; //true;


	public boolean inCapture = false;
	public static boolean inCaptureTest = false;

	public static boolean check = true;
	private VCOM mVCOM = null;
    public static VCOM mVCOM2 = null;
	Bitmap mBitmapMatch;

	public static boolean toastcheck = true;
	public static int fingermoduleCount = 0;
	public static int  fingerPrintStarted =-1;
	public static boolean faceChecking=false;
	public static int captureCount=0;
	public static boolean FaceGetFromCardInitiated=false;
	//private static String personName=null;
	//private static String IDNumber=null;
	private CardDataVariables cardData; //

	//FTReader
	boolean dynamCardReading=true;
	FTReader ftReader=null;
	private  boolean cardIn=false;
	private static int readerNotRespondingCount=0;
	public Handler mHandler;
	String[] readerNames;

	private int cardCount= 0;
	private boolean fromBelgiumEidaActivity=false;
	private boolean newCardOut=false;
	public static int cardReadStatus=0; //ready-0, notready=1 //, waiting =2
	public void showErrorAndClose(String error, int code) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(error + ": " + code)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						android.os.Process.killProcess(android.os.Process.myPid());
					}
				})
				.show();
	}

	public void showMessage(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
					}
				})
				.setCancelable(false) // cancel with button only
				.show();
	}


	private boolean cardDataUpdateDatabase2(String userID) {

		try {
			Log.d(TAG,"in cardDataUpdateDatabase");
			Log.d(TAG,"Update database cardnumber "+userID);

			List<CardUser> cardUser = MainActivity_FaceandFinger.appCardDatabase.carduserDao().getAll();

			Log.d(TAG,"Current Users are "+cardUser.size());

			CardUser currCardUser =MainActivity_FaceandFinger.appCardDatabase.carduserDao().countUsersBasedonCardNumber(userID);
			if (currCardUser != null) {

//				if (MainActivity_FaceandFinger.appCardDatabase.carduserDao().countUsers(userID) != null) {
				//appDatabase.userDao().update(user1);
				//	GlobalVariables.CardReadStatus=5; //Already read these data
				Log.d(TAG, "Data is already there - no need Enroll");
				return false;
			} else {
				belgiumeida.carddatabase.CardUser data = new CardUser();

				Log.d(TAG,"Going for Inserting card number "+userID);

				data.setChipNumber("1001");
				data.setCardNumber(userID);
				data.setChipNumber("1001");
				data.setCustomerName("Chand");
				data.setCustomerName2("Lakshmi Chand");
				data.setFirstLetterName("LAKS");
				data.setNationality("Indian");
				data.setNationalNumber("100");
				data.setNobleCondition("lovely");
				data.setValidtyBegin("100");
				data.setValidityEnd("100");

				MainActivity_FaceandFinger.appCardDatabase.carduserDao().insertAll(data);
				return true;
			}
			//Send to the Server
		} catch (Exception obj) {
			Log.d(TAG,"Updating Card data Got exception "+obj.getMessage());
		}
		return true;
	}
	private void resetTrackerParameters() {
		int errpos[] = new int[1];
		FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "ContinuousVideoFeed=true;Threshold2=0.9995;ThresholdFeed=0.97;MemoryLimit=2000;HandleArbitraryRotations=false;DetermineFaceRotationAngle=false;InternalResizeWidth=90;FaceDetectionThreshold=3", errpos);
	//	FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "ContinuousVideoFeed=true;FacialFeatureJitterSuppression=0;RecognitionPrecision=1;Threshold=0.996;Threshold2=0.9995;ThresholdFeed=0.97;MemoryLimit=2000;HandleArbitraryRotations=false;DetermineFaceRotationAngle=false;InternalResizeWidth=70;FaceDetectionThreshold=3;", errpos);
		//FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "HandleArbitraryRotations=false;DetectExpression=true;DetermineFaceRotationAngle=false;InternalResizeWidth=100;FaceDetectionThreshold=5", errpos);
		if (errpos[0] != 0) {
			showErrorAndClose("Error setting tracker parameters, position", errpos[0]);
		}
	}

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		//setContentView(R.layout.launch_activity);
		sDensity = getResources().getDisplayMetrics().scaledDensity;
        VCOM mVCOM2 = null;

		prefs = getBaseContext().getSharedPreferences(
				GlobalVariables.SP_FingerPrint, MODE_PRIVATE);
		comeOut = 0;
		IdentifiedUserName = null;
		adminVerified = false;
		adminVerification = false;
		appStopped = false;
		FaceGetFromCardInitiated=false;
		check = true;
		fromBelgiumEidaActivity=false;
		newCardOut=false;

		Log.d(TAG,"Card Read Status is "+cardReadStatus);

		if (cardReadStatus == 1)
			cardReadStatus = 0;
		MainActivity_FaceandFinger.BioSDKInitialized = false;
		try {
			try {
				fromWeb = getIntent().getBooleanExtra("FromWeb", false);
				currState = getIntent().getStringExtra("CurrState");
				Log.d("FaceFinger", "Curr State is " + currState);
			} catch (Exception tpexcep) {
				currState = null;
			}

			try {
				fromBelgiumEidaActivity = getIntent().getBooleanExtra("fromBelgiumEidaActivity",false);

				Log.d(TAG,"From Belgium EIda activity ");
			} catch (Exception e3) {

			}
			try {
				EnrolmentStatus = getIntent().getBooleanExtra("Enrolment", false);
				Log.d(TAG, "Face Recognition enrolment Status is " + EnrolmentStatus);

				//if (EnrolmentStatus == true)
				//	dynamCardReading=false;

			} catch (Exception obj) {

			}
			try {
				adminVerification = false;
				adminVerification = getIntent().getBooleanExtra("adminVerification", false);
				Log.d(TAG, "Face  Admin Verification Status is " + adminVerification);

			} catch (Exception obj) {

			}
			try {

				//ldapUser = getIntent().getExtras().getString("ldapUser");
				userID = getIntent().getExtras().getString("userid");
				//user = getIntent().getExtras().getParcelable("userData");
				Log.d(TAG, "Face Recognition " + userID);
			//	Log.d(TAG, "Face Name is " + user.getFullname());

			} catch (Exception obj) {

			}
			appDatabase = AppDatabase.getAppDatabase(MainActivity_FaceandFinger.this);
			//List<lumidigm.captureandmatch.entity.User> users = appDatabase.userDao().getAll();
			appCardDatabase = UserCardDatabase.getAppDatabase(MainActivity_FaceandFinger.this);

		//	cardDataUpdateDatabase("121");

			workflowID = prefs.getInt(GlobalVariables.localworkflowStatus, -1);
			captureCount=0;
			//intent.putExtra("userid", editText.getText().toString());
			Log.d(TAG, "Curr State of Camera is " + currState);
			if (currState != null && currState.equals("success")) {
				CameraStatus = false;

				//CameraStatus = false;
				GlobalVariables.personDescription = "face matching";
				GlobalVariables.reqStatus = true;

				Log.d(TAG, "Time is more than 20 seconds - Not Right");
				Intent intent = new Intent(this, FaceSuccessActivity.class);//SuccessActivity
				intent.putExtra("case", "success");
				intent.putExtra("FromWeb", true);

				intent.putExtra("source", "face");
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */

				this.startActivity(intent);
			} else if (currState != null && currState.equals("failure")) {
				CameraStatus = false;
				GlobalVariables.personDescription = "face not matching";
				GlobalVariables.reqStatus = true;

				Log.d(TAG, "Time is more than 20 seconds - Not Right");
				Intent intent = new Intent(this, FaceFailureActivity.class);//SuccessActivity
				intent.putExtra("case", "failure");
				intent.putExtra("FromWeb", true);

				intent.putExtra("source", "face");
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","failure");
				mBundle.putBoolean("FromWeb",true);
*/
				this.startActivity(intent);


			} else {
				Log.d(TAG, "Checking Camera");
				Camera cp = null;
				//cp = Camera.open(0);
				//cp.release();
				Log.d(TAG, "Camera Status checking ");
				PackageManager pm = this.getPackageManager();
				if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
					CameraStatus = true;
					Log.d(TAG, "Camer Status istrue for Feature ANY CAMERA");
				} else {
					CameraStatus = true;
					Log.d(TAG, "Camer Status istrue for Feature ANY CAMERA not true");
				}

				//lastSystemTime = System.currentTimeMillis();

				//lastSystemTime = 0;
				//cp.release();
				//Camera.release();
				//CameraStatus =true;
				Log.d(TAG, "Camera is AVailable");
			}
		} catch (Exception ojf) {
			CameraStatus = false;
			Log.d(TAG, "Camera Status                                                         if false" + ojf.getMessage());
		}
		//int res = FSDK.ActivateLibrary("Uw3aCK45gqXYjd7GNHf60l7D3oDd02rdxPqpfyf4gqVryuSwAvrct7VmifKjr7gEWuJP76R+dcuDUVInAvXsd7kwOQwoLA9eD/qvUyVZaeFGjpd0quslvHdLLKr719vn/1EoqGj3aPn6vsDZIPbqv6WicPxS6XniNvR20x7oEAw=");

		//Card Reader
		if (FaceBelgium && dynamCardReading) {
			Log.d(TAG,"Dynamic Card Reading is true ");
			//CameraStatus=false;
			try {
				refreshDevices();
				mHandler = new HintHandler();
				cardStatusread();

				Log.d(TAG,"Card Read Status is "+cardReadStatus);

				if (cardReadStatus == 2) {
					Log.d(TAG,"Process Card In . it is pending ");

					/*final Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
				*/			//Do something after 100ms
							Log.d(TAG,"USB 1234");
					//		usbFound();
							cardIn();
					/*	}
					}, 1000);*/
				}
			} catch (Exception obj) {
				Log.d(TAG,"Got the exception while reading handler ");
			}
		} else
			Log.d(TAG,"Card Reading is disabled ");
		//Card Reader

		comeOutofApp=false;
		int TerminalId = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
	    if (supervisionEnrolmentStatus == false) {
			Log.d(TAG, "It is Not superVisionEnrollmentStatus");
			//if (GlobalVariables.MobileDevice == false)
			if (M320Support == true ) {
				mVCOM = new VCOM(this);
				mVCOM2 = mVCOM;
				fingerPrintStarted = 0;
			}
		}

		try {

			if (cardReadStatus == 2) {
				Log.d(TAG,"Not Starting the Camera "+cardReadStatus);

			} else
			if (CameraStatus) {
				Log.d(TAG, "Before Activate Library");
				int res = FSDK.ActivateLibrary("PaBHT9o0Htms6kvI6o10rFlZKseWhzEcW/NsLkNLV6M20EkAVB8Z2ave61alLevrBh2af/xFv7ToXsBGc9Ya+9XmzCIgZc8V4PMosaeYI3loB9/B6QHc5dgghoU6iOHeenRyRffIT2v8G4aXhCpag7NTT7sHY5HzC8clFLxvx2I=");

				//	int res = FSDK.ActivateLibrary(("EMvnS0fIF7exIRspEoQuM7Io1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISljUOqA="));
				//	int res = FSDK.ActivateLibrary("OfuGFjnj3oJ5xekQC0FSoMmbaGlC6qtsC9YccC65G0aq8Q6OpJJUG07Y47zkTW0ILsLEiuEw5W4GylYgeqkUCcci1199GRvvqEvKS9hinpbVgoXiWZ5EmgCUThta0Ja6Pbk0IvSF3DGfardNSw8GX+F9UVObiIZUdmUCDY8crTs=");
				//int res = FSDK.ActivateLibrary("EMvnS0flF7exlRspEoQuM7lo1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISIjUOqA=");
				///int res = FSDK.ActivateLibrary("EMvnS0flF7exlRspEoQuM7lo1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHlSljUOqA=");
				//int res = FSDK.ActivateLibrary("EMvnS0fIF7exIRspEoQuM7Io1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POIhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISIjUOqA=");
				prefs = getBaseContext().getSharedPreferences(
						GlobalVariables.SP_FingerPrint, MODE_PRIVATE);
				if (res != FSDK.FSDKE_OK) {
					mIsFailed = true;
					showErrorAndClose("FaceSDK activation failed", res);
				} else {


					if (CameraStatus) {
						Log.d(TAG, "Before FSDK Initialize ");
						FSDK.Initialize();

						// Hide the window title (it is done in manifest too)
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
						requestWindowFeature(Window.FEATURE_NO_TITLE);

						// Lock orientation
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

						// Camera layer and drawing layer
						mDraw = new FaceandFingerProcessImageAndDrawResults(this);
						mPreview = new FaceandFingerPreview(this, mDraw);
						mDraw.mTracker = new HTracker();

						Log.d(TAG, "mpReview is created ");
						String templatePath = this.getApplicationInfo().dataDir + "/" + database;
						Log.d(TAG, "Before Load Memory " + templatePath);
						if (FSDK.FSDKE_OK != FSDK.LoadTrackerMemoryFromFile(mDraw.mTracker, templatePath)) {
							res = FSDK.CreateTracker(mDraw.mTracker);
							if (FSDK.FSDKE_OK != res) {
								showErrorAndClose("Error creating tracker", res);
							}
						}

						resetTrackerParameters();

						this.getWindow().setBackgroundDrawable(new ColorDrawable()); //black background

						setContentView(mPreview); //creates MainActivity_FaceandFinger contents
						addContentView(mDraw, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

						// Menu
						//if (EnrolmentStatus)
						{
							LayoutInflater inflater1 = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
							View buttons = inflater1.inflate(R.layout.bottom_menu, null);
							Log.d(TAG,"Enrolment Status is"+EnrolmentStatus);
							if (NoFingerPrintSupport)
								buttons = inflater1.inflate(R.layout.bottom_menu_fingerprint, null);

							if (EnrolmentStatus)
								buttons = inflater1.inflate(R.layout.bottom_menu_enroll, null);
							else
							if (FaceICA || FaceBelgium)
								buttons = inflater1.inflate(R.layout.bottom_menu_ica, null);

							//buttons.findViewById(R.id.helpButton).setOnClickListener(this);
							//buttons.findViewById(R.id.clearButton).setOnClickListener(this);
							TextView tpView1 = buttons.findViewById(R.id.textViewFaceHeader); //facetextview);
							TextView tpView2 = buttons.findViewById(R.id.textViewFace);

							if (NoFingerPrintSupport)
								buttons.findViewById(R.id.fingerprint).setOnClickListener(this);
							else
							if ((EnrolmentStatus == false) && (FaceICA || FaceBelgium)) {
								buttons.findViewById(R.id.icaface).setOnClickListener(this);

								Log.d(TAG,"ICA Face testing ");
								//	if (!EnrolmentStatus)
								//	tpView1.setText("...");
							}

							addContentView(buttons, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
							//Chand added

							//buttons.setEnabled(false);

							Log.d(TAG, "For Buttons TOp");
							View buttons3 = inflater1.inflate(R.layout.top_menu, null);
							//buttons2. = (Button) findViewById(R.id.settings_new);
							MainActivity_FaceandFinger.inflater = inflater1;
							MainActivity_FaceandFinger.buttons2 = buttons;
							MainActivity_FaceandFinger.tpView = tpView1;
							MainActivity_FaceandFinger.tpView0 = tpView2;

							buttons3.findViewById(R.id.settings_new).setOnClickListener(this);
							//	buttons2.findViewById(R.id.clearButton1).setOnClickListener(this);

							addContentView(buttons3, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
						}
					} else
						Log.d(TAG, "No Camera ");
				}
			}
		} catch (Exception objd) {

		}
 		if (FaceICA == true || FaceBelgium ==true)
			startWeb();
		//	TestFunc();

	/*	final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				//Do something after 100ms
				Log.d(TAG,"USB 1234");
				usbFound();
			}
		}, 3000);
*/
	}

	public class HintHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Log.d(TAG, "handleMessage:11 " + msg.what);
			Log.d(TAG, "handleMessage:12 " + msg.obj);

			String log = null;

			switch (msg.what) {
				case -1:
					Log.d(TAG,"It is Text view -1 ");
					// textView.setText("");
					return;
				case 0:
					log = msg.obj.toString();

					Log.d(TAG,"Got the Message 0"+log);
					break;
				case DK.USB_IN:
					Log.d(TAG,"DK.USB_IN");
					usbFound();
					return;
				case USB_LOG:
					Log.d(TAG, "USB_LOG handleMessage:  " + "usbin "+cardIn+"  cardoutnew"+newCardOut);
				//	cardIn();
					//if (newCardOut)
					{
						Log.d(TAG,"Treated as Card In for USB_LOG for previous cardout ");
					//	cardIn();
					}
					String curmsg1 = String. valueOf(msg.obj);

					if (curmsg1.contains("5002")) {
						Log.d(TAG,"End with 5002 so treating as card out ");
						cardOut();
						Log.d(TAG,"Card Read Status is out is "+cardReadStatus);
						if (cardReadStatus == 2)
							cardReadStatus=1;

					} else if (curmsg1.contains("5003")) {

						Log.d(TAG,"End with 5002 so treating as card in ");
						if (cardReadStatus == 0) {
							Log.d(TAG,"card Read Status ");
							cardIn();
						} else {
							cardReadStatus = 2;
							Log.d(TAG,"Card Read Status needs to be handled ");
						}

						Log.d(TAG,"Card Read Status is in "+cardReadStatus);
					}

					return;
				case 1234:
					Log.d(TAG,"USB 1234");
					final Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							//Do something after 100ms
							usbFound();
						}
					}, 3000);


					return;
				case DK.USB_OUT:
					Log.d(TAG,"DK_USB_OUT");
					// textView.append("NOTICE------->USB Device Out\n");
					// updateSpinnerA_14(new String[0]);
				try {
						ftReader.readerClose();
					} catch (FTException e) {
						showLog(e.getMessage());
					}
						try {
						try {
							//new FTReaderTest(PORT).SCardListReaders();
						} catch (Exception e) {
							showLog("button61 TEST---------->" + e.getMessage());
						}
						ftReader.readerPowerOff(0);
					} catch (Exception e) {
						showLog("PowerOff Failed--------------->" + e.getMessage());
					}

					return;
				case DK.PCSCSERVER_LOG:
					Log.d(TAG,"It is PCServer Log ");
					// textView.append("[PcscServerLog]:"+msg.obj+"\n");
					return;
				case DK.BT3_NEW:
					Log.d(TAG,"Got DK_BT3_NEW");
					BluetoothDevice device = (BluetoothDevice) msg.obj;

					return;
				case DK.BT4_NEW:
					Log.d(TAG,"Got DK_BT4_NEW");
					BluetoothDevice mDevice = (BluetoothDevice) msg.obj;

					return;
				default:
					Log.d(TAG, "It is Default "+msg.what+"  Card Mask In is "+DK.CARD_IN_MASK);

					Log.d(TAG, "It is Default "+msg.what+"  Card Mask In is "+DK.CARD_OUT_MASK);
					int test=msg.what & DK.CARD_IN_MASK;

					Log.d(TAG,"Final test val is "+test+" card read status is "+cardReadStatus);

					if ((msg.what & DK.CARD_IN_MASK) == DK.CARD_IN_MASK) {
						Log.d(TAG, "CARD_IN_MASK handleMessage: " + "card in");

						//cardReadStatus = 2;

						if (cardReadStatus == 0) {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {

									Log.d(TAG, "Card In ");

									if (cardIn) {
										Log.d(TAG, "It is already Card In ");
									} else
										//if (!WebSupport)
										cardIn();
								}
							});
						} //else
							//cardReadStatus = 2;
						Log.d(TAG,"Card Read Status is "+cardReadStatus);
					return;
					} else if ((msg.what & DK.CARD_OUT_MASK) == DK.CARD_OUT_MASK) {
						Log.d(TAG, "CARD_OUT_MASK handleMessage: " + "card out");
						cardOut();
						if (cardReadStatus == 2)
							cardReadStatus=1;

						return;
					}
					break;
			}
			if (log == null) {
				//  textView.append("OBJ---------->"+"what:"+msg.what+"   obj:"+msg.obj+"\n");
			} else {
				// textView.append("LOG---------->"+log+"\n");
			}
		}
	}
	private int getSpinnerA_14() throws Exception {
		return 1;
	}
	public void cardStatus(int val) {

		try {
			Log.d(TAG,"Getting the Card Status ");
			int index = getSpinnerA_14();
			//devmodel = readerNames[index];
			Log.d(TAG," Get Spinnder Index"+" Name ");
			int status  = val;
			//Chand added this temporarily remove it
			//if (val == 1)
			//  status = DK.CARD_PRESENT_ACTIVE;
			//else
			try {
				status = ftReader.readerGetSlotStatus(index);

				Log.d(TAG,"Card Reader Status is "+status);
				try {
					//Chand Get all the Card Status
					Log.d(TAG, "Spinner Max Count is " + cardCount);

					Log.d(TAG,"Card Present ID"+DK.CARD_PRESENT_ACTIVE+ " Inactive ID "+DK.CARD_PRESENT_INACTIVE+
							"No Card ID "+DK.CARD_NO_PRESENT);

					//Last 2 are Junk IDs and getting as card Present, so removed them
					if  (cardCount == 4)
						cardCount=cardCount-2;

					for (int i=0; i < cardCount; i++) {
						int currstatus2 = ftReader.readerGetSlotStatus(i);
						String devmodel2 = readerNames[i];
						Log.d(TAG,"Spinner index "+i+" Status is "+currstatus2+" Its model is"+devmodel2);

						if (currstatus2 == DK.CARD_PRESENT_ACTIVE) {
							status = currstatus2;
							String devmodel = readerNames[i];
							Log.d(TAG,"Final Status is"+status+"  Dev model"+devmodel);
							break;
						} else
						if (currstatus2 == DK.CARD_PRESENT_INACTIVE) {
							status = currstatus2;
							String devmodel = readerNames[i];
							Log.d(TAG,"InActive Status is"+status+"  Dev model"+devmodel);
						}
					}
				} catch (Exception obj) {
					Log.d(TAG,"Got Exception while reading spinners");
					finishAffinity();
					//System.exit(0);
                 /*   Intent i=new Intent(FirstActivity.this, FirstActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(i);
                 */   return;
				}
				//Crashing here
			} catch (Exception obj) {
             /*   Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                */

				Log.d(TAG,"Reboot device as GetSlotStatus Failed ");
           /*     try {
                    finishAffinity();
                    //commented by Chand on 17 May 2017 for oscope device restarting periodically
                    //stopSelf();
                    //System.exit(0);
                    Runtime.getRuntime()
                            .exec("reboot" );

                    Log.d(TAG,"Device got rebooted ");
                } catch (Exception ex) {

                    ex.printStackTrace();
                }
*//*
                finishAffinity();
               // System.exit(0);
                Intent i=new Intent(FirstActivity.this, FirstActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);
            */    Log.d(TAG,"Restarting App as readerGetSlotStatus Failed ");
				return;
			}

			Log.d(TAG, "cardStatus: "+status);
			Log.d(TAG," Get Spinnder Index"+index+" Name "+"card status"+status);

			switch (status) {
				//Chand added CARD_PRESENT_INACTIVE here -as card Plugin not getting eveing ACTIVE
				case DK.CARD_PRESENT_INACTIVE:
					showLog("SlotStatus: card present and no powered IN_ACTIVE");
					//break;
				case DK.CARD_PRESENT_ACTIVE:
					Log.d(TAG,"CardStatus is PRESENT_ACTIVE ");

					Log.d(TAG,"Card In Status is "+cardIn);
					if (cardIn == true) {
						Log.d(TAG,"Card Already In - so No Progress ");
					} else {
						cardIn = true;
						{
							cardIn();
							Log.d(TAG,"Not ICA SUpport");
						}
						showLog("SlotStatus: card present and powered");
					}
					break;

				case DK.CARD_NO_PRESENT:
					showLog("SlotStatus: card CARD_NO_PRESENT");
					cardOut();
					break;

			}
		} catch (Exception e) {
			showLog("GetSlotStatus failed---------->" + e.getMessage());
		}
	}

	public void cardOut() {
		Log.d(TAG,"Card is Out ");
		cardIn = false;
		fromBelgiumEidaActivity=false;
        CardDataVariables.CardNumber=null;
        GlobalVariables.currFaceDetectionCount=0;
		newCardOut=true;
	}

	public void cardStatusread() {

		boolean status=true;
		Log.d(TAG,"In CardStatusRead ");
		try {
			ftReader = new FTReader(this, mHandler, DK.FTREADER_TYPE_USB);
			// ftReader = new FTReader(this, mHandler, DK.FTREADER_TYPE_USB, DK.FTREADER_JAR);
			ftReader.readerFind();
			Log.d(TAG,"CardReadStatus Success");
			showLog("Device Found");
			showLog(" Device Found---------->");
		} catch (FTException e) {
			//Chand Crashing
			status=false;
			showLog("No Device Found---------->" + e.getMessage());
			Log.d(TAG,"No Device Found");
			Log.d(TAG,"Reboot device as No Device Found ");
			try {
				Intent i=new Intent(this, MainActivity_FaceandFinger.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(i);
				CameraStatus=false;
				finishAffinity();

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		if (status == true) {
			try {
				readerNames = ftReader.readerOpen(null);
			} catch (Exception obj) {
				try {
					Log.d(TAG,"GOt the ");
					Intent i = new Intent(this, MainActivity_FaceandFinger.class);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(i);
					CameraStatus = false;
					finishAffinity();

				} catch (Exception ex) {

					ex.printStackTrace();
					readerNames = null;
				}
				Log.d(TAG, "readerOpen Failed Restarting App");
			}

			if (readerNames != null) {
				for (int i = 0; i < readerNames.length; i++) {
					Log.d(TAG, "onClick: " + readerNames[i]);
				}

				try {
					int type = ftReader.readerGetType();
					Log.d(TAG, "Type is " + type);
				} catch (Exception obj) {
					Log.d(TAG, "Got the exception while reader Get");
					Intent i = new Intent(this, MainActivity_FaceandFinger.class);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(i);
					CameraStatus = false;
					finishAffinity();

				}
			}
		}
	}

	public void usbFound() {
		try {

			Log.d(TAG,"USB Found ");
			try {
				readerNames = ftReader.readerOpen(null);
			} catch (Exception obj) {
				try {
					Log.d(TAG,"Reboot as reader Open Failed ");
					finishAffinity();

				} catch (Exception ex) {

					ex.printStackTrace();
				}

				Log.d(TAG,"readerOpen Failed Restarting App");
			}
			//Sometimes - Crashes here
			for (int i = 0; i < readerNames.length; i++) {
				Log.d(TAG, "onClick: " + readerNames[i]);
			}
			//updateSpinnerA_14(readerNames);


			///////////////////////////////////////////////////////////////////////////////////
			try {
				int type = ftReader.readerGetType();
				switch (type) {
					case DK.READER_R301E:
						showLog("ReaderType:R301E");
						break;
					case DK.READER_BR301FC4:
						showLog("ReaderType:BR301FC4");
						break;
					case DK.READER_BR500:
						showLog("ReaderType:Br500");
						break;
					case DK.READER_R502_CL:
						showLog("ReaderType:R502-CL");
						break;
					case DK.READER_R502_DUAL:
						showLog("ReaderType:R502-Dual");
						break;
					case DK.READER_BR301:
						showLog("ReaderType:bR301");
						break;
					case DK.READER_IR301_LT:
						showLog("ReaderType:iR301-LT");
						break;
					case DK.READER_IR301:
						showLog("ReaderType:iR301");
						break;
					case DK.READER_VR504:
						showLog("ReaderType:VR504");
						break;
					default:
						showLog("ReaderType:Unknow");
						break;
				}

				///////////////////////////////////////////////////////////////////////////////////
				//Chand - Crashing while reading - readrGetSerialNumber()
				try {
					byte[] serial = ftReader.readerGetSerialNumber();
					Log.d(TAG, "onClick: " + Utility.bytes2HexStr(serial));
					showLog("SerialNumber:" + Utility.bytes2HexStr(serial));
					//serialnum = Utility.bytes2HexStr(serial);
					readerNotRespondingCount = 0;
				} catch (Exception e) {
					showLog("GetSerialNum ERROR-----> Reboot the device " + e.getMessage());
					Log.d(TAG,"Open Device Failed Restarting App as readerGetSlotStatus Failed ");
					finishAffinity();
					readerNotRespondingCount++;
					if (readerNotRespondingCount > 4) {
						Runtime.getRuntime()
								.exec("reboot");
					} else
						System.exit(0);
				}

				///////////////////////////////////////////////////////////////////////////////////
				try {

					String ver = ftReader.readerGetFirmwareVersion();


					showLog("FirmwareVerson:" + ver);
				//	devversion = ver;

					Log.d(TAG, "onClick: " + ver);
				} catch (Exception e) {
//					showLog("GetFirmwareVersion Failed------->" + e.getMessage());
				}
				///////////////////////////////////////////////////////////////////////////////////
				try {
					byte[] uid = ftReader.readerGetUID();
//					showLog("UID:" + Utility.bytes2HexStr(uid));
					Log.d(TAG, "onClick: " + Utility.bytes2HexStr(uid));
				//	devuid = Utility.bytes2HexStr(uid);
				} catch (Exception e) {
//					showLog("GetUID Failed--------------->" + e.getMessage());
				}

 				cardStatus(1);

			} catch (Exception e) {
//				showLog("GetType Failed------------>" + e.getMessage());
			}


		} catch (Exception e2) {
//			showLog("Open Device Failed------------>" + e2.getMessage());


		}
	}

	public    String getCardList() {
		Log.d(TAG,"Get Card List ");
		try {
			if (ftReader != null) {
				String readerNames[] = ftReader.readerOpen(null);

				if (readerNames != null) {
					int len = readerNames.length;
					Log.d(TAG, "Reader Len is " + len + "  readernames " + readerNames[0]);

             /*       try {
                        ftReader.readerPowerOn(0);
                    } catch (Exception obj)  {
                        Log.d(TAG,"Got Exception while Read Power On ");
                    }*/
					String retVal = null;
					for (int i=0;i<readerNames.length;i++) {
						if (i==0)
							retVal =readerNames[i];
						else retVal = retVal +readerNames[i];
					}
					//return readerNames[0];

					Log.d(TAG,"RetVal is "+retVal);

					return retVal;
				}
			} else
				Log.d(TAG,"FT Reader is null");
		} catch (Exception obj ) {
			Log.d(TAG,"Got the Reader Open Exception "+obj.getMessage());
		}
		return null;
	}
	private void showLog(String st) {
		Log.d(TAG,st);
	}
	public void cardIn() {

		Log.d(TAG,"Card In from fromBelgiumEidaActivity "+cardIn);
		//getCardList();

		if (FaceBelgium && dynamCardReading && (fromBelgiumEidaActivity == true && cardReadStatus !=2 )) {
			Log.d(TAG,"Got the Belgium Eida Activity status true ");
			fromBelgiumEidaActivity=false;
			cardIn=false;
			cardReadStatus=1;
		} else {
			try {
				if (ftReader != null) {
					ftReader.readerClose();
					//	ftReader.readerPowerOff(0);
					//Plugin_ATR(1);
					ftReader = null;
				}
			} catch (Exception obj) {
				ftReader = null;
				Log.d(TAG, "Got exception while closing " + obj.getMessage());
			}
			try {
				GlobalVariables.IDNumber = null;

				Log.d("FaceDetction", "GoBack to Belgium Eida Activity 11");
				Intent intent = new Intent(this, BelgiumEidaActivity.class);
				//Intent intent= new Intent(this, HomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				GlobalVariables.currFaceDetectionCount = 0;
				this.startActivity(intent);
				appStopped = true;
				cardReadStatus=1;
				Log.d(TAG,"Card Read Status is "+cardReadStatus);
				finishAffinity();
			} catch (Exception obj) {
				Log.d(TAG,"Got the Exception while sending ");
			}
		}
	}
	private void refreshDevices()
	{
		try {
			UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
			// Get the list of attached devices
			HashMap<String, UsbDevice> devices = manager.getDeviceList();

			// Iterate over all devices
			Iterator<String> it = devices.keySet().iterator();
			Log.d(TAG, "Checking Number of devices");
			while (it.hasNext()) {
				String deviceName = it.next();
				UsbDevice device = devices.get(deviceName);

				String VID = Integer.toHexString(device.getVendorId()).toUpperCase();
				String PID = Integer.toHexString(device.getProductId()).toUpperCase();
				Log.d(TAG, deviceName + "Number of devices " + VID + ":" + PID + " " + manager.hasPermission(device) + "\n");
			}
		} catch (Exception obj) {

		}
	}
	//above devices for Card Reader
	@Override
	public void onClick(View view) {

		Log.d(TAG,"onClick ");
		if (view.getId() == R.id.settings_new) {
			//showMessage(help_text);
			Log.d(TAG, "Initiate Face Settings ");
			// startSettings();

			//		Log.d("FaceDetction","GoBack to Home Activity");
			Intent intent = new Intent(this, FaceSettings.class);
			//		Intent intent= new Intent(this, AdminVerificationActivityMobile.class);
			//Intent intent= new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			this.startActivity(intent);

			cardReadStatus=1;
			Log.d(TAG,"Card Read Status is "+cardReadStatus);

			appStopped = true;
			comeOutofApp = true;
			if (M320Support == true ) {
			if (mVCOM != null) {
				mVCOM.Close();
				mVCOM = null;
				mVCOM2 = null;

				fingerPrintStarted = -1;
			}
				Log.d(TAG,"On Destroy mvCOM got close ");
			}
			if (M21Support == true) {
				appStopped = true;
				try {
					if (BioSDKInitialized == true) {

						Log.d(TAG, "onBack releaseBioSDKAPI");
				//		BioSDKFactory.releaseBioSDKAPI();
					} else

						Log.d(TAG, "onBack already releaseBioSDKAPI");

					BioSDKInitialized = false;
				} catch (Exception obj) {

				}
			}

			finishAffinity();
		} else
		if (view.getId() == R.id.fingerprint) {

			Log.d(TAG, "Initiate Finger Print ");
			// startSettings();

			//		Log.d("FaceDetction","GoBack to Home Activity");
			Intent intent = new Intent(this, Main2Activity.class);
			//		Intent intent= new Intent(this, AdminVerificationActivityMobile.class);
			//Intent intent= new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			this.startActivity(intent);

			cardReadStatus=1;
			Log.d(TAG,"Card Read Status is "+cardReadStatus);
			appStopped = true;
			comeOutofApp = true;
		} else
		if (view.getId() == R.id.icaface) {
		  //  FaceGetFromCardInitiated=true;
			if (FaceICA) {
				//sendDataforICAFace();
				GlobalVariables.IDNumber = null;
				GlobalVariables.FaceCardReadCount = 1;
				sendDataforICAFace();

				Log.d(TAG, "Test ICA Face");
			} else {
				try {
					GlobalVariables.IDNumber = null;
					//GlobalVariables.FaceCardReadCount = 1;
					//sendDataforICAFace();
					//CardDataJson();

					Log.d("FaceDetction", "GoBack to Belgium Eida Activity");
					Intent intent = new Intent(this, BelgiumEidaActivity.class);
					//Intent intent= new Intent(this, HomeActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					GlobalVariables.currFaceDetectionCount = 0;
					this.startActivity(intent);
					appStopped = true;

					cardReadStatus=1;
					Log.d(TAG,"Card Read Status is "+cardReadStatus);
					finishAffinity();
				} catch (Exception obj) {
					Log.d(TAG,"Got the Exception while sending ac");
				}
			}
		}

		/*else
            if (view.getId() == R.id.helpButton) {
			showMessage(help_text);
			Log.d(TAG,"Initiate Enroll ");

		} else if (view.getId() == R.id.clearButton) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Are you sure to clear the memory?" )
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
                    public void onClick(DialogInterface dialogInterface, int j) {
						pauseProcessingFrames();
						FSDK.ClearTracker(mDraw.mTracker);
						resetTrackerParameters();
						resumeProcessingFrames();
					}
				})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
                    public void onClick(DialogInterface dialogInterface, int j) {
					}
				})
				.setCancelable(false) // cancel with button only
				.show();
		}*/
	}

	private void startSettings() {

		Log.d(TAG, "In SHow Licence App");
		LayoutInflater inflater = LayoutInflater.from(MainActivity_FaceandFinger.this);
		View subView = inflater.inflate(R.layout.settings_dialog, null);
		final EditText subEditText = (EditText) subView.findViewById(R.id.dialogEditText1);

		android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
		builder.setTitle("Android Settings");
		builder.setMessage("Password Verification");
		builder.setView(subView);
		builder.setCancelable(false);
		alertDialog = builder.create();


		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String passwordText = subEditText.getText().toString().trim();
				Log.d(TAG, "Password Text is " + passwordText);
				//  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

				String settingPwd = "123"; //getsettingspwd();
				Log.d(TAG, "App Pwd is " + settingPwd);
				if (passwordText.equals("argus@542")) {
					Log.d(TAG, "Going for Shutdown");
					//startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
					//Shutdown();
				} else if (passwordText.equals("argus@543")) {
					Log.d(TAG, "Going for Reboot");
					//reboot();
				} else if (passwordText.equals(settingPwd)) {

					startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
					finish();

				}
			}
		});

		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				{
					alertDialog.cancel();
					// Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

					// finish();
				}
			}
		});

		builder.show();
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "MainAtivity on Destroy before VCOM CLose");
//		Log.d(TAG, "onDestroy inCapture " + inCapture + "  Test" + inCaptureTest);
		appStopped = true;
		try {
			if (ftReader != null)
				ftReader.readerClose();
		} catch (Exception obj) {
			Log.d(TAG,"Got the exception on destroy ");
		}
		if (M21Support == true) {
			try {
			 } catch (Exception obj) {
				Log.d(TAG, "Go for the release of BioSDKAPI");
			}
			if (M320Support == true) {
			if (mVCOM != null) {
				mVCOM.Close();
				mVCOM = null;

				mVCOM2 = null;
				fingerPrintStarted = -1;
			}
				Log.d(TAG,"On Destroy mvCOM got close ");
			}
		}
	}

	@Override
	public void onPause() {

		super.onPause();

		Log.d(TAG, "Called onPause ");
		Log.d(TAG, "on Pause comeOut count is" + GlobalVariables.comeOut);

 		  if (supervisionEnrolmentStatus == false) {
			if (M320Support == true ) {
 			if (mVCOM != null) {
				Log.d(TAG, "VCOM Close ");
				mVCOM.Close();
				mVCOM = null;
				mVCOM2 = null;
				fingerPrintStarted = -1;
			}
			} else
				Log.d(TAG, "MainActivity on Pause VCOM not Close ");
		} else
			Log.d(TAG, "It is in Non SuperVisionernolemnt mode ");

 		  try {
			  if (CameraStatus) {
				  pauseProcessingFrames();
				  String templatePath = this.getApplicationInfo().dataDir + "/" + database;
				  FSDK.SaveTrackerMemoryToFile(mDraw.mTracker, templatePath);
			  }
		  } catch (Exception obj) {
 		  	Log.d(TAG,"Got Exception while on Pause ");
		  }
	}

	@Override
	public void onResume() {

		Log.d(TAG, "Called onResume ");
		super.onResume();
		if (mIsFailed)
			return;
		if (CameraStatus)
			resumeProcessingFrames();

		appStopped=false;

		adminVerificationInitiated = false;
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		if (( check)) {
			Log.d(TAG, "MainActivity onResume  fore CaptureAndmMatchTask");


			if (supervisionEnrolmentStatus == false) {

			} else
				Log.d(TAG,"Supervision enrolment Status is true ");

			Log.d(TAG, "In Capture Status when check false is " + inCapture + "  prev " + inCaptureTest);

			try {
				if (M21Support == true) {
					Log.d(TAG, "M21Support Chand enabled so onResume nothing ");

					try {
                       /* Log.d(TAG, "M21 Initializing ");
                        BioSDKFactory.initializeBioSDKAPI(getApplicationContext());
                        Log.d(TAG, "Initializing BIOSDK successful ");
                        initEngine();
*/
						//initializeDevice();
						//onEnroll();

					} catch (Exception obj) {
						Log.d(TAG,"Got exception while on Resume "+obj.getMessage());
					}
				} else if (supervisionEnrolmentStatus == false) {
					Log.d(TAG,"FingerPrint Started Status is "+fingerPrintStarted);

				} else
					Log.d(TAG, "In Non SUpervision enrollment Mode ");
			} catch (Exception obj) {
				Log.d(TAG, "OnResume got exception ");
			}
		}
		try {
			{
				Log.d(TAG,"Chand onResume testing ");
				LayoutInflater inflater1 = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View buttons = inflater1.inflate(R.layout.bottom_menu, null);
				Log.d(TAG,"Enrolment Status is"+EnrolmentStatus);
				if (NoFingerPrintSupport)
					buttons = inflater1.inflate(R.layout.bottom_menu_fingerprint, null);

				Log.d(TAG,"Test 1");
				if (EnrolmentStatus)
					buttons = inflater1.inflate(R.layout.bottom_menu_enroll, null);
				else
				if (FaceICA || FaceBelgium)
					buttons = inflater1.inflate(R.layout.bottom_menu_ica, null);

				Log.d(TAG,"Test 2");
				//buttons.findViewById(R.id.helpButton).setOnClickListener(this);
				//buttons.findViewById(R.id.clearButton).setOnClickListener(this);
				TextView tpView1 = buttons.findViewById(R.id.textViewFaceHeader); //facetextview);
				TextView tpView2 = buttons.findViewById(R.id.textViewFace);


				Log.d(TAG,"Test 3");
				if (NoFingerPrintSupport)
					buttons.findViewById(R.id.fingerprint).setOnClickListener(this);
				else
				/*if (FaceICA || FaceBelgium) {*/
					if ((EnrolmentStatus == false) && (FaceICA || FaceBelgium)) {
						Log.d(TAG,"Test 4");
						buttons.findViewById(R.id.icaface).setOnClickListener(this);

					Log.d(TAG,"ICA Face testing ");
					//	if (!EnrolmentStatus)
					//	tpView1.setText("...");
				}

				Log.d(TAG,"Test 5");
				addContentView(buttons, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				//Chand added

				//buttons.setEnabled(false);

				Log.d(TAG, "For Buttons TOp");
				View buttons3 = inflater1.inflate(R.layout.top_menu, null);
				//buttons2. = (Button) findViewById(R.id.settings_new);
				MainActivity_FaceandFinger.inflater = inflater1;
				MainActivity_FaceandFinger.buttons2 = buttons;
				MainActivity_FaceandFinger.tpView = tpView1;
				MainActivity_FaceandFinger.tpView0 = tpView2;

				buttons3.findViewById(R.id.settings_new).setOnClickListener(this);
				//	buttons2.findViewById(R.id.clearButton1).setOnClickListener(this);

				addContentView(buttons3, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			}
		} catch (Exception obj) {
			Log.d(TAG,"Got the Exception while onResume showing the top menu "+obj.getMessage());
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		if (fromWeb) {
			Log.d("FaceDetction", "GoBack to Home Activity");
			Intent intent = new Intent(this, Main2Activity.class);
			//Intent intent= new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

			this.startActivity(intent);

			cardReadStatus=1;
			Log.d(TAG,"Card Read Status is "+cardReadStatus);
			appStopped = true;
			finish();
		}

		super.onBackPressed();
	}

	public boolean SendingMessage(String status, String requestType) {
		Log.d(TAG, "SendMessage " + status);
		//String url="http://192.168.43.24:8080/?username="+status;


		String remoteIP = getRemoteIP();
		if (remoteIP == null || remoteIP == "") {
			Log.d(TAG, " No IP ");
			return false;
		}
		if (!isEthernetConnected()) {
			if (!isConnectedInWifi()) {
				Log.d(TAG, "Network not connected");
				return false;
			}
		}


		Log.d(TAG, "Getting IP Address");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
		String url = "http://" + remoteIP + ":8080/?username=" + status;

		Log.d(TAG, "send url: " + url);
		new VolleyService(new IResult() {
			@Override
			public void notifySuccess(String requestType, Object response) {

				Log.d(TAG, "notifySuccess: " + requestType + " Resoibse us" + response);
				/*if (requestType.equals("100")) {
					//Intent i= new Intent(this, audio.MainActivity_FaceandFinger.class);

					Intent i = new Intent(MainActivity_FaceandFinger.this, audio.MainActivity_FaceandFinger.class);

					startActivity(i);
				} else if (requestType.equals("101")) {
					Intent i = new Intent(MainActivity_FaceandFinger.this, FingerScannActivity.class);
					startActivity(i);
				} else if (requestType.equals("102")) {
					Intent i = new Intent(MainActivity_FaceandFinger.this, FirstActivity.class);
					startActivity(i);
				} else if (requestType.equals("103")) {
					Intent i = new Intent(MainActivity_FaceandFinger.this, audio.MainActivity_FaceandFinger.class);
					startActivity(i);
				} else if (requestType.equals("104")) {
					Intent i = new Intent(MainActivity_FaceandFinger.this, HomeActivity.class);
					startActivity(i);
				}*/
			}


			@Override
			public void notifyError(String requestType, VolleyError error) {
				Log.d(TAG, "notifyError: " + error.getMessage());
				try {
					//String fingerprint = "testfingerprint";
					Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
					String filePath = Environment.getExternalStorageDirectory() + "/displayImages/error.mp3";
					//           playAudioFile(2,filePath);
			/*		if (requestType.equals("100")) {
						//Intent i= new Intent(this, audio.MainActivity_FaceandFinger.class);

						Intent i = new Intent(MainActivity_FaceandFinger.this, audio.MainActivity_FaceandFinger.class);

						startActivity(i);
					} else if (requestType.equals("101")) {
						Intent i = new Intent(MainActivity_FaceandFinger.this, FingerScannActivity.class);
						startActivity(i);
					} else if (requestType.equals("102")) {
						Intent i = new Intent(MainActivity_FaceandFinger.this, FirstActivity.class);
						startActivity(i);
					} else if (requestType.equals("103")) {
						Intent i = new Intent(MainActivity_FaceandFinger.this, audio.MainActivity_FaceandFinger.class);
						startActivity(i);
					} else if (requestType.equals("104")) {
						Intent i = new Intent(MainActivity_FaceandFinger.this, HomeActivity.class);
						startActivity(i);
					}*/

				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}, this).tokenBase(GET, url, null, requestType);

		return true;
	}

	private Boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}

	public Boolean isWifiConnected() {
		if (isNetworkAvailable()) {
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
		}
		return false;
	}

	public Boolean isEthernetConnected() {
		if (isNetworkAvailable()) {
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
		}
		return false;
	}


	private String getRemoteIP() {
		String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
				"");

		Log.d("test", "Remote IP Addr is " + remoteIPAddr);

		return remoteIPAddr;
	}

	public boolean isConnectedInWifi() {

		try {
			WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
			NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
					&& wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
				return true;
			}
		} catch (Exception obj) {
			Log.d("MainAFaceandFinger", "Got exception " + obj.toString());
		}
		return false;
	}

	private void pauseProcessingFrames() {
		try {
			mDraw.mStopping = 1;

			// It is essential to limit wait time, because mStopped will not be set to 0, if no frames are feeded to mDraw
			for (int i = 0; i < 100; ++i) {
				if (mDraw.mStopped != 0) break;
				try {
					Thread.sleep(10);
				} catch (Exception ex) {
				}
			}
		} catch (Exception obj) {
			Log.d(TAG,"pausprocessing frace exception");
		}
	}

	private void resumeProcessingFrames() {
		try {
			mDraw.mStopped = 0;
			mDraw.mStopping = 0;
		} catch (Exception obj) {

			Log.d(TAG,"resumeProcessingFrames frace exception");
		}
	}


	/*
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof IFragmentListener) {
                mListener = (IFragmentListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }


        @Override
        public void onStart() {
            super.onStart();

        }


        @Override
        public void onHiddenChanged(boolean hidden) {
            if (hidden) {
                BioSDKDevice device = mListener.getConnectedDevice();
                if (device != null) {
                    //        mFingerFeedbackTxtView.setText("");
                    //      mEnrollButton.setText("ENROLL");
                    mCaptureInProgress = false;
                    device.cancel_async();
                }
            }
        }
    */

	public void playBeep() {
		MediaPlayer m = new MediaPlayer();
		try {
			if (m.isPlaying()) {
				m.stop();
				m.release();
				m = new MediaPlayer();
			}

			AssetFileDescriptor descriptor = getAssets().openFd("shutter.mp3");
			m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();

			m.prepare();
			m.setVolume(1f, 1f);
			m.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//}


 	private void GoBack() {
			Log.d(TAG, "Go Back ");

     /*   Intent i = new Intent(getApplicationContext(), LaunchActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK  );
        startActivity(i);*/

		cardReadStatus=1;
		Log.d(TAG,"Card Read Status is "+cardReadStatus);
			finishAffinity();
		}

		private Bitmap GetBitmap() {
			try {
				Log.d(TAG, "in Get BitMap");
				int sizeX = mVCOM.GetCompositeImageSizeX();
				int sizeY = mVCOM.GetCompositeImageSizeY();
				if (sizeX > 0 && sizeY > 0) {
					Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
					int[] colors = new int[sizeX * sizeY];
					if (mVCOM.GetCompositeImage(colors) == 0) {
						bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
					} else {
						Log.i(TAG, "Unable to get composite image.");
					}
					return bm;
				} else {
					Log.i(TAG, "Composite image is too small.");
					// ...so we create a blank one-pixel bitmap and return it
					Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
					return bm;
				}
			} catch (Exception obj) {
				//Chand aadded
				Log.d(TAG, "GetBitMap is null got exception" + obj.getMessage());

				Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
				return bm;

			}
		}
	//}
	private void TestFunc() {

		Log.d(TAG,"in TestFun");
		String data;// = intent.getStringExtra("Data");
//            user_data.setText(data);

	//	Log.d(TAG,"Got the Face Detection Thread"+data.length());

		//Get the File


		//data =
		//		"/9j/4AAQSkZJRgABAQEASABIAAD/2wBDABUOEBIQDRUSERIYFhUZHzQiHx0dH0AuMCY0TENQT0tDSUhUXnlmVFlyWkhJaY9qcnyAh4iHUWWUn5ODnXmEh4L/2wBDARYYGB8cHz4iIj6CVklWgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoL/wAARCACaAIIDASIAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAAQBAgMFBv/EADIQAAEEAAQEBQMEAQUAAAAAAAEAAgMRBBIhMUFRYXEFEyIygTORsRQjNFJCJHKhwdH/xAAYAQADAQEAAAAAAAAAAAAAAAAAAQIDBP/EAB0RAQEBAQADAQEBAAAAAAAAAAABEQISITEDQVH/2gAMAwEAAhEDEQA/APQIQhQQQhCAFClQgBCguAWc00UTbe4N7pBqhKxY6CR2USC uiZu0AKFZQmEKFJUFAQoKsoSCvwhShBGfKd0R5TlgfEH8Im/dVOPm4NZ9itPEvKGfKPNQWgbuCQm8UmZoBGT/tP/AKubi/FZ2gk5cx2AtT6Eu/HamxOHhH7kzW/CRxHisLWkRPzHnVBefkkdPckht1qo11BIPBLFyOhLiZ5NfMJHK1gZXOB0OYcLWbPMG5b8hD2uu7FdKSxolryTTgQUxh8fNhzbXkt/q42EoM90bq9AQoLm61tyTwPQ4XxWOX0yjy3dToU4JQ4WDY6Lybn20EbDkpw NnwxPlP9N7EWCiIvL1E IETM1WrxyMlFscD2XOwc7cY1tuLgdxyT0MbI3lrBQrZOxDVQVKgpGEIpCQYho5IIAFlWWU7vSAOK2cxeQNJLiAuNjpBJKSwabBdTFuAw7gTVilxZDTiQs79dH5c tUGnH45raNjSC4bjglbObotYj6tHCuiGkMOkLWgMAHcLMEyaNsLfM0s9R yrEWk5WvPwkvFHQ0NaPZZUKNWAmzGXOBfY EPja/QMI6AbparxKBhrM00SaWbmuL62NrpR4R2hcMvRLzx5JE5UWNPCp5MPKLNtvUL0jSDICKILeC8oyWnAceK9B4Y8vhZZurH4KaOofQhCSUIQhIM0vP7gmFjO3QFa1zOb4lfkitr1XLLbF9V2sUzzIHgb1ouTHWYgjbms66vyu8sGwufJQ2TLPD3nZzRa3wzRq7mU4yWNnuPylbWkk/peLw1xZT36cgU1DgY4TbW69VrHi4XGg60017CEvbSZ/GDYrR5DRsFq6QMvZJTSySmmPyjojDtxrI1rRWi52OZ6c43bqmv07AATK5zupVCywQdQj4zrlZdWuHHVelwcXkxRNIo6/hcBsZEojr2vpeomFGPurjPqrIQhJKPhClCAyUOGYUVKFq5SbhRIXIxUYixD62Oq7EpBeSFzfE4nFhkbwFFZ1t e6mJtwMriLWgmigb7S5wGtC1ELf8ATsG3pCIY/LLqAIduHC7UumLRy/qA50cRIbV7cflXZI9r266HhyV2NbHFlY0MHGtLVGt9YOwGw6oqprfFG47 6XaxrnkSOcBrVBOubce3BLtjrb7JSqpWPCuje4zyOfpTQCbv5VoGPbo91903kBA3QIwN07U JN8AOKBJIa4Wa5hdrOZIIy73ZgCuY8fus6OT8RuIkcHAolR1JhhCEJs0IR8IQGahShauUk4USs8YA7Dd0xO3K  BWTxnYWlZdR0fj1lz/WGHoxN7BbNiHZLwWxxjPA6Hom2OUV08jy YsqoafMJ4ApgEAWUrKLBZrTjwKF07Tcl5h2S7WgkEOsKrWUwNtxHUoiaI6A0aNgE8LW VVeVIeFV6R2l3gF7L5roxsy4Eu4k38LnHWZvddUC/Dz2Kvlh1UqFA2UoQPlCLQgMkIQtXKpMLjPRKp3dLztDXAgUEqcpWdpbIHcxorxuvuiQW3qNlmw8VjZjs478vbeV2wCyMgY7XUlaGiLWYiFlxsngiNftXEjiPb9lDpHD3MtSHPAoADqVZrHuNucCmrYq14dtmBPMK7iSBagitaVS4n4Uo Jj s3uuoyjgnjkCuZhml0hfWgXSh/iyjv8Aha8/HP1d7UafSD0UqkZ9DeyupMIQhAZoQhauUKHtzMpShBk3jKaKwd6HbGuabxNUOaWe3Mx3ZZ2NOLZUB1jdax80pmLSto5hWpUY65TTa1sqziAEo6VoGh15hR54qySR2TwaYkKVc8k0qvnLhQtWjjIbrqUvg noK8ltck7h/wCNIB1/C5uEdbXD ppdHBm2SBay nN450xi m1XWcX0x8q6habQhCAooQSsnzhug3Wrlanqs5JQ0aalVZHJKbeS1q0MbWVTR3O6i9NOeLS7WmQlxulplygcNVcj1AcBqhw2SrXnnCIaA5zeRpUfFRtM4huV2cDugDM1RfVdHPuFmxArQQNq91fJRU5jVI08ZZAHLRwpqhoJcomNNKQXwA9Ejv7OtOxyOjcSNjuEpgW1A3qCf 0yNloxqWChSuFQaKwKRJtChCCYNhkk1e7KOS1ZEyMaN15lX/xVhsj6U5kRdqrgavqrO4KD7R8flClGGx/wofsrD2N7KHbBMM3CxrsRqsGAsflO3BMv0qljN/inZvKublSWquUclo3YqqxbqgUsZRmdl56JkbFZN uFXP1PXqN2CgAtAqDgteJWvTBBCqCeO4Vwqn6ikJQoQjA//9k=";
		data =  "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDABUOEBIQDRUSERIYFhUZHzQiHx0dH0AuMCY0TENQT0tDSUhUXnlmVFlyWkhJaY9qcnyAh4iHUWWUn5ODnXmEh4L/2wBDARYYGB8cHz4iIj6CVklWgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoL/wAARCACaAIIDASIAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAAQBAgMFBv/EADIQAAEEAAQEBQMEAQUAAAAAAAEAAgMRBBIhMUFRYXEFEyIygTORsRQjNFJCJHKhwdH/xAAYAQADAQEAAAAAAAAAAAAAAAAAAQIDBP/EAB0RAQEBAQADAQEBAAAAAAAAAAABEQISITEDQVH/2gAMAwEAAhEDEQA/APQIQhQQQhCAFClQgBCguAWc00UTbe4N7pBqhKxY6CR2USC uiZu0AKFZQmEKFJUFAQoKsoSCvwhShBGfKd0R5TlgfEH8Im/dVOPm4NZ9itPEvKGfKPNQWgbuCQm8UmZoBGT/tP/AKubi/FZ2gk5cx2AtT6Eu/HamxOHhH7kzW/CRxHisLWkRPzHnVBefkkdPckht1qo11BIPBLFyOhLiZ5NfMJHK1gZXOB0OYcLWbPMG5b8hD2uu7FdKSxolryTTgQUxh8fNhzbXkt/q42EoM90bq9AQoLm61tyTwPQ4XxWOX0yjy3dToU4JQ4WDY6Lybn20EbDkpw NnwxPlP9N7EWCiIvL1E IETM1WrxyMlFscD2XOwc7cY1tuLgdxyT0MbI3lrBQrZOxDVQVKgpGEIpCQYho5IIAFlWWU7vSAOK2cxeQNJLiAuNjpBJKSwabBdTFuAw7gTVilxZDTiQs79dH5c tUGnH45raNjSC4bjglbObotYj6tHCuiGkMOkLWgMAHcLMEyaNsLfM0s9R yrEWk5WvPwkvFHQ0NaPZZUKNWAmzGXOBfY EPja/QMI6AbparxKBhrM00SaWbmuL62NrpR4R2hcMvRLzx5JE5UWNPCp5MPKLNtvUL0jSDICKILeC8oyWnAceK9B4Y8vhZZurH4KaOofQhCSUIQhIM0vP7gmFjO3QFa1zOb4lfkitr1XLLbF9V2sUzzIHgb1ouTHWYgjbms66vyu8sGwufJQ2TLPD3nZzRa3wzRq7mU4yWNnuPylbWkk/peLw1xZT36cgU1DgY4TbW69VrHi4XGg60017CEvbSZ/GDYrR5DRsFq6QMvZJTSySmmPyjojDtxrI1rRWi52OZ6c43bqmv07AATK5zupVCywQdQj4zrlZdWuHHVelwcXkxRNIo6/hcBsZEojr2vpeomFGPurjPqrIQhJKPhClCAyUOGYUVKFq5SbhRIXIxUYixD62Oq7EpBeSFzfE4nFhkbwFFZ1t e6mJtwMriLWgmigb7S5wGtC1ELf8ATsG3pCIY/LLqAIduHC7UumLRy/qA50cRIbV7cflXZI9r266HhyV2NbHFlY0MHGtLVGt9YOwGw6oqprfFG47 6XaxrnkSOcBrVBOubce3BLtjrb7JSqpWPCuje4zyOfpTQCbv5VoGPbo91903kBA3QIwN07U JN8AOKBJIa4Wa5hdrOZIIy73ZgCuY8fus6OT8RuIkcHAolR1JhhCEJs0IR8IQGahShauUk4USs8YA7Dd0xO3K  BWTxnYWlZdR0fj1lz/WGHoxN7BbNiHZLwWxxjPA6Hom2OUV08jy YsqoafMJ4ApgEAWUrKLBZrTjwKF07Tcl5h2S7WgkEOsKrWUwNtxHUoiaI6A0aNgE8LW VVeVIeFV6R2l3gF7L5roxsy4Eu4k38LnHWZvddUC/Dz2Kvlh1UqFA2UoQPlCLQgMkIQtXKpMLjPRKp3dLztDXAgUEqcpWdpbIHcxorxuvuiQW3qNlmw8VjZjs478vbeV2wCyMgY7XUlaGiLWYiFlxsngiNftXEjiPb9lDpHD3MtSHPAoADqVZrHuNucCmrYq14dtmBPMK7iSBagitaVS4n4Uo Jj s3uuoyjgnjkCuZhml0hfWgXSh/iyjv8Aha8/HP1d7UafSD0UqkZ9DeyupMIQhAZoQhauUKHtzMpShBk3jKaKwd6HbGuabxNUOaWe3Mx3ZZ2NOLZUB1jdax80pmLSto5hWpUY65TTa1sqziAEo6VoGh15hR54qySR2TwaYkKVc8k0qvnLhQtWjjIbrqUvg noK8ltck7h/wCNIB1/C5uEdbXD ppdHBm2SBay nN450xi m1XWcX0x8q6habQhCAooQSsnzhug3Wrlanqs5JQ0aalVZHJKbeS1q0MbWVTR3O6i9NOeLS7WmQlxulplygcNVcj1AcBqhw2SrXnnCIaA5zeRpUfFRtM4huV2cDugDM1RfVdHPuFmxArQQNq91fJRU5jVI08ZZAHLRwpqhoJcomNNKQXwA9Ejv7OtOxyOjcSNjuEpgW1A3qCf 0yNloxqWChSuFQaKwKRJtChCCYNhkk1e7KOS1ZEyMaN15lX/xVhsj6U5kRdqrgavqrO4KD7R8flClGGx/wofsrD2N7KHbBMM3CxrsRqsGAsflO3BMv0qljN/inZvKublSWquUclo3YqqxbqgUsZRmdl56JkbFZN uFXP1PXqN2CgAtAqDgteJWvTBBCqCeO4Vwqn6ikJQoQjA//9k=";
	//	data =  "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDABUOEBIQDRUSERIYFhUZHzQiHx0dH0AuMCY0TENQT0tDSUhUXnlmVFlyWkhJaY9qcnyAh4iHUWWUn5ODnXmEh4L/2wBDARYYGB8cHz4iIj6CVklWgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoL/wAARCACaAIIDASIAAhEBAxEB/8QAGgAAAgMBAQAAAAAAAAAAAAAAAAQBAgMFBv/EADIQAAEEAAQEBQMEAQUAAAAAAAEAAgMRBBIhMUFRYXEFEyIygTORsRQjNFJCJHKhwdH/xAAYAQADAQEAAAAAAAAAAAAAAAAAAQIDBP/EAB0RAQEBAQADAQEBAAAAAAAAAAABEQISITEDQVH/2gAMAwEAAhEDEQA/APQIQhQQQhCAFClQgBCguAWc00UTbe4N7pBqhKxY6CR2USC+uiZu0AKFZQmEKFJUFAQoKsoSCvwhShBGfKd0R5TlgfEH8Im/dVOPm4NZ9itPEvKGfKPNQWgbuCQm8UmZoBGT/tP/AKubi/FZ2gk5cx2AtT6Eu/HamxOHhH7kzW/CRxHisLWkRPzHnVBefkkdPckht1qo11BIPBLFyOhLiZ5NfMJHK1gZXOB0OYcLWbPMG5b8hD2uu7FdKSxolryTTgQUxh8fNhzbXkt/q42EoM90bq9AQoLm61tyTwPQ4XxWOX0yjy3dToU4JQ4WDY6Lybn20EbDkpw+NnwxPlP9N7EWCiIvL1E+IETM1WrxyMlFscD2XOwc7cY1tuLgdxyT0MbI3lrBQrZOxDVQVKgpGEIpCQYho5IIAFlWWU7vSAOK2cxeQNJLiAuNjpBJKSwabBdTFuAw7gTVilxZDTiQs79dH5c+tUGnH45raNjSC4bjglbObotYj6tHCuiGkMOkLWgMAHcLMEyaNsLfM0s9R+yrEWk5WvPwkvFHQ0NaPZZUKNWAmzGXOBfY+EPja/QMI6AbparxKBhrM00SaWbmuL62NrpR4R2hcMvRLzx5JE5UWNPCp5MPKLNtvUL0jSDICKILeC8oyWnAceK9B4Y8vhZZurH4KaOofQhCSUIQhIM0vP7gmFjO3QFa1zOb4lfkitr1XLLbF9V2sUzzIHgb1ouTHWYgjbms66vyu8sGwufJQ2TLPD3nZzRa3wzRq7mU4yWNnuPylbWkk/peLw1xZT36cgU1DgY4TbW69VrHi4XGg60017CEvbSZ/GDYrR5DRsFq6QMvZJTSySmmPyjojDtxrI1rRWi52OZ6c43bqmv07AATK5zupVCywQdQj4zrlZdWuHHVelwcXkxRNIo6/hcBsZEojr2vpeomFGPurjPqrIQhJKPhClCAyUOGYUVKFq5SbhRIXIxUYixD62Oq7EpBeSFzfE4nFhkbwFFZ1t+e6mJtwMriLWgmigb7S5wGtC1ELf8ATsG3pCIY/LLqAIduHC7UumLRy/qA50cRIbV7cflXZI9r266HhyV2NbHFlY0MHGtLVGt9YOwGw6oqprfFG47+6XaxrnkSOcBrVBOubce3BLtjrb7JSqpWPCuje4zyOfpTQCbv5VoGPbo91903kBA3QIwN07U+JN8AOKBJIa4Wa5hdrOZIIy73ZgCuY8fus6OT8RuIkcHAolR1JhhCEJs0IR8IQGahShauUk4USs8YA7Dd0xO3K++BWTxnYWlZdR0fj1lz/WGHoxN7BbNiHZLwWxxjPA6Hom2OUV08jy+YsqoafMJ4ApgEAWUrKLBZrTjwKF07Tcl5h2S7WgkEOsKrWUwNtxHUoiaI6A0aNgE8LW+VVeVIeFV6R2l3gF7L5roxsy4Eu4k38LnHWZvddUC/Dz2Kvlh1UqFA2UoQPlCLQgMkIQtXKpMLjPRKp3dLztDXAgUEqcpWdpbIHcxorxuvuiQW3qNlmw8VjZjs478vbeV2wCyMgY7XUlaGiLWYiFlxsngiNftXEjiPb9lDpHD3MtSHPAoADqVZrHuNucCmrYq14dtmBPMK7iSBagitaVS4n4Uo+Jj+s3uuoyjgnjkCuZhml0hfWgXSh/iyjv8Aha8/HP1d7UafSD0UqkZ9DeyupMIQhAZoQhauUKHtzMpShBk3jKaKwd6HbGuabxNUOaWe3Mx3ZZ2NOLZUB1jdax80pmLSto5hWpUY65TTa1sqziAEo6VoGh15hR54qySR2TwaYkKVc8k0qvnLhQtWjjIbrqUvg+noK8ltck7h/wCNIB1/C5uEdbXD+ppdHBm2SBay+nN450xi+m1XWcX0x8q6habQhCAooQSsnzhug3Wrlanqs5JQ0aalVZHJKbeS1q0MbWVTR3O6i9NOeLS7WmQlxulplygcNVcj1AcBqhw2SrXnnCIaA5zeRpUfFRtM4huV2cDugDM1RfVdHPuFmxArQQNq91fJRU5jVI08ZZAHLRwpqhoJcomNNKQXwA9Ejv7OtOxyOjcSNjuEpgW1A3qCf+0yNloxqWChSuFQaKwKRJtChCCYNhkk1e7KOS1ZEyMaN15lX/xVhsj6U5kRdqrgavqrO4KD7R8flClGGx/wofsrD2N7KHbBMM3CxrsRqsGAsflO3BMv0qljN/inZvKublSWquUclo3YqqxbqgUsZRmdl56JkbFZN+uFXP1PXqN2CgAtAqDgteJWvTBBCqCeO4Vwqn6ikJQoQjA//9k=";
		data = data.replaceAll(" ","+");
		Base64.decode(data, Base64.DEFAULT);
		byte[] photo = Base64.decode(data, Base64.DEFAULT);
		Log.d(TAG,"Got the Face Detection Thread"+data.length());
		Log.d(TAG,"Got the Face Detection"+data);

		if (photo == null || photo.length <= 0) {
			Log.d(TAG,"Photo Contents are null ");
			return;
		}//if()
		//Create  a bitmap.
		//set to the imageview
//chand added below 2 commands
		Bitmap bmp = BitmapFactory.decodeByteArray(photo, 0, photo.length);
		// bmp.compress(Bitmap.CompressFormat.JPEG);
		try {
			Log.d("Test","Getting the file ");

			List<lumidigm.captureandmatch.entity.User> users = MainActivity_FaceandFinger.appDatabase.userDao().getAll();

			if (users != null) {
				Log.d(TAG, "Get User Count is " + users.size());

				String userID = (users.get(0).get_id());

				String filename = Environment.getExternalStorageDirectory()
						+ File.separator
						+ "displayImages" + File.separator + userID + ".jpg";

				Log.d(TAG,"File Name is "+filename);

				FileOutputStream out = new FileOutputStream(filename,false);

/*try {
						out.write(("").getBytes());
					} catch (Exception Obj) {
						Log.d(TAG,"Clear file exception");
					}*/

				bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

				Log.d(TAG, "Update File ");
				// bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
				//bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
				out.close();
			} else
				Log.d(TAG,"Not found user ");
		} catch (Exception obj) {
			Log.d("Test","Got the exception ");
		}
		//imageview.setImageBitmap(bmp);
		Log.d("data", "data from server"+ data);

	};


	private void startWeb()
	{
		Context app_context = getApplicationContext();
		WebICAImageCapture_Server server= new WebICAImageCapture_Server(DEFAULT_PORT,app_context);
		try
		{
			server.start();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String data = intent.getStringExtra("Data");
//            user_data.setText(data);

			String perName=intent.getStringExtra("Name");
			String IDNum=intent.getStringExtra("IDNum");
			String ArabicName=intent.getStringExtra("ArabicName");
			String expiryDate=intent.getStringExtra("expiryDate");
			String Nationality=intent.getStringExtra("Nationality");
			String Gender=null;


			Log.d(TAG,"Expiry Date is "+expiryDate);
			Log.d(TAG,"Nationality  is "+Nationality);

			//expiryDate="+expiryData+"&Nationality
			GlobalVariables.personName=null;
			GlobalVariables.IDNumber=null;
			GlobalVariables.ArabicName=null;
			GlobalVariables.expiryDate=null;
			GlobalVariables.Nationality=null;
			GlobalVariables.Gender=null;


			Log.d(TAG,"Person Name is "+perName+"  ID Number is"+IDNum);

			if (perName != null)
				GlobalVariables.personName=perName;

			if (IDNum != null)
				GlobalVariables.IDNumber=IDNum;

			if (ArabicName != null)
				GlobalVariables.ArabicName=ArabicName;

			if (expiryDate != null)
				GlobalVariables.expiryDate=expiryDate;

			if (Nationality != null)
				GlobalVariables.Nationality=Nationality;


			if (Gender != null)
				GlobalVariables.Gender=Gender;

			Log.d(TAG,"Got the Face Detection Thread"+data.length());

			//Get the File

			//data = data.replaceAll("   ","+");
			//data = data.replaceAll("  ","+");
			data = data.replaceAll(" ","+");

			Log.d(TAG,"Got the Face Detection"+data);

			//byte[] photo =toByteArray(data);
			byte[] photo = Base64.decode(data, Base64.DEFAULT);
			Log.d(TAG,"Got the Face Detection"+photo.length);
			if (photo == null || photo.length <= 0) {
				Log.d(TAG,"Photo Contents are null ");
				return;
			}//if()
			//Create  a bitmap.
			//set to the imageview
//chand added below 2 commands
			Bitmap bmp = BitmapFactory.decodeByteArray(photo, 0, photo.length);
			// bmp.compress(Bitmap.CompressFormat.JPEG);
			try {
				Log.d("Test","Getting the file ");

				List<lumidigm.captureandmatch.entity.User> users = MainActivity_FaceandFinger.appDatabase.userDao().getAll();

				if (users != null) {
					Log.d(TAG, "Get User Count is " + users.size());

					String userID = (users.get(0).get_id());

					String filename = Environment.getExternalStorageDirectory()
							+ File.separator
							+ "displayImages" + File.separator + userID + ".jpg";

						GlobalVariables.UserID=userID+".jpg";
					try {
						File f = new File(filename);

						if (f.exists())
							f.delete();
					} catch (Exception obj) {

					}
					Log.d(TAG,"File Name is "+filename);

					FileOutputStream out = new FileOutputStream(filename,false);
					/*try {
						out.write(("").getBytes());
					} catch (Exception Obj) {
						Log.d(TAG,"Clear file exception");
					}*/
					bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

					Log.d(TAG, "Update File ");
					// bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
					//bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
					out.close();
				} else
					Log.d(TAG,"Not found user ");
				} catch (Exception obj) {
				Log.d("Test","Got the exception ");
			}
			//imageview.setImageBitmap(bmp);
			Log.d("data", "data from server"+ data);
 		}
	};

	public void sendData(String msg){

		try {
			RequestQueue mQueue = Volley.newRequestQueue(this);

			Log.d("h", " inside http call");

			Log.d("h", " message from data " + msg);

			// "http://192.168.3.41:8084/?username=";
			String url = "http://" + GlobalVariables.OtherBockIP + ":8084/?username=" + msg; // GuardOutsideblock_GlobalVariable.url+msg;

			Log.d(TAG, "URL is " + url);
			JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
					new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {

						}
					}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					error.printStackTrace();
					Log.d("h", " inside http call error" + error);
				}
			});
			mQueue.add(request);
		} catch (Exception obj) {
			Log.d(TAG,"Got the exception of"+obj.getMessage());
		}
	}
	public void sendDataforICAFace(){

		try {
			RequestQueue mQueue = Volley.newRequestQueue(this);

			Log.d("h", " inside http call");


			// "http://192.168.3.41:8084/?username=";
			String url = "http://" + GlobalVariables.OtherBockIP + ":8085/?username=initiate" ; // GuardOutsideblock_GlobalVariable.url+msg;

			Log.d(TAG, "URL is " + url);
			JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
					new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {

						}
					}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					error.printStackTrace();
					Log.d("h", " inside http call error" + error);
				}
			});
			mQueue.add(request);
		} catch (Exception obj) {
			Log.d(TAG,"Got the exception of"+obj.getMessage());
		}
	}
	@Override
	protected void onStart() {
		super.onStart();

		if (FaceICA == true) {
			IntentFilter intentFilter = new IntentFilter("lumidigm.captureandmatch"); //lumidigm.captureandmatch");
			registerReceiver(broadcastReceiver, intentFilter);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (FaceICA == true)
			unregisterReceiver(broadcastReceiver);
	}

	private JSONObject CardDataJson() {
		try {

			Log.d(TAG,"Card Status is "+GlobalVariables.CardReadStatus);
			String jsonStr = "{\"name\": \"i30\", \"brand\": \"Hyundai\"}";


			String CardData = "{\"FileVersion\": \""+cardData.FileVersion+"\","+
					"\"CardNumber\": \""+cardData.CardNumber+"\","+
					"\"ChipNumber\": \""+cardData.ChipNumber+"\","+
					"\"ValidityeBegin\": \""+cardData.ValidityeBegin+"\","+
					"\"ValidityEnd\": \""+cardData.ValidityEnd+"\","+
					"\"NationalNumber\": \""+cardData.NationalNumber+"\","+
					"\"CardDeliveryMunispality\": \""+cardData.CardDeliveryMunspality+"\","+
					"\"Name\": \""+cardData.Name+"\","+
					"\"Name_2\": \""+cardData.Name_2+"\","+
					"\"Nationality\": \""+cardData.Nationality+"\","+
					"\"BirthLoc\": \""+cardData.BirthLoc+"\","+
					"\"BirthDate\": \""+cardData.BirthDate+"\","+
					"\"Sex\": \""+cardData.Sex+"\","+
					"\"NobleCondition\": \""+cardData.NobleCondition+"\","+
					"\"documentType\": \""+cardData.DocumentType+"\","+
					"\"SpecialStatus\": \""+cardData.SpecialStatus+"\","+
					"\"HashPicture\": \""+cardData.HashPicture+"\","+
					"\"SpecialOrganization\": \""+cardData.SpecialOrganization+"\","+
					"\"MemberofFamilyType\": \""+cardData.MemberofFamilyType+"\","+
					"\"DateandCountryofProtection\": \""+cardData.DateandCountryofProtection+"\""+
					"}";




			CardData = CardData.replaceAll("\"null\"","null");
			// convert to json object
			JSONObject json = new JSONObject(CardData);

			Log.d(TAG,"JSON Value is "+json);

			return json;
		} catch (Exception obj) {
			Log.d(TAG,"GOt the Exception "+obj.getMessage());
		}
		return null;
	}
}

class FaceRectangleFaceFinger {
	public int x1, y1, x2, y2;
}

// Draw graphics on top of the video
class FaceandFingerProcessImageAndDrawResults extends View {
	private static final String TAG = "ProcessImageAndDrawResu";
	public HTracker mTracker;

	final int MAX_FACES = 5;
	final FaceRectangleFaceFinger[] mFacePositions = new FaceRectangleFaceFinger[MAX_FACES];
	final long[] mIDs = new long[MAX_FACES];
	final Lock faceLock = new ReentrantLock();
	int mTouchedIndex;
	boolean popup = false;
	long mTouchedID;
	int mStopping;
	int mStopped;

	Context mContext;
	Paint mPaintGreen, mPaintBlue, mPaintBlueTransparent;
	byte[] mYUVData;
	byte[] mRGBData;
	int mImageWidth, mImageHeight;
	boolean first_frame_saved;
	boolean rotated;
	private int handlercount = 0;
	private long  lastSystemTime=0;
	FSDK.HImage RotatedImage=null;
	FSDK.HImage Image=null;

	lumidigm.captureandmatch.entity.User IdentifiedUser=null;
	//private FSDK.FSDK_IMAGEMODE mCameraImageMode = new FSDK.FSDK_IMAGEMODE();
	//lastSystemTime = System.currentTimeMillis();
	int GetFaceFrame(FSDK.FSDK_Features Features, FaceRectangleFaceFinger fr)
	{
		if (Features == null || fr == null)
			return FSDK.FSDKE_INVALID_ARGUMENT;

	    float u1 = Features.features[0].x;
	    float v1 = Features.features[0].y;
	    float u2 = Features.features[1].x;
	    float v2 = Features.features[1].y;
	    float xc = (u1 + u2) / 2;
	    float yc = (v1 + v2) / 2;
	    int w = (int) Math.pow((u2 - u1) * (u2 - u1) + (v2 - v1) * (v2 - v1), 0.5);

	    fr.x1 = (int)(xc - w * 1.6 * 0.9);
	    fr.y1 = (int)(yc - w * 1.1 * 0.9);
	    fr.x2 = (int)(xc + w * 1.6 * 0.9);
	    fr.y2 = (int)(yc + w * 2.1 * 0.9);
	    if (fr.x2 - fr.x1 > fr.y2 - fr.y1) {
	        fr.x2 = fr.x1 + fr.y2 - fr.y1;
	    } else {
	        fr.y2 = fr.y1 + fr.x2 - fr.x1;
	    }

		return 0;
	}


	public FaceandFingerProcessImageAndDrawResults(Context context) {
		super(context);

		mTouchedIndex = -1;

		mStopping = 0;
		mStopped = 0;
		rotated = false;
		mContext = context;
		mPaintGreen = new Paint();
		mPaintGreen.setStyle(Paint.Style.FILL);
		mPaintGreen.setColor(Color.GREEN);
		mPaintGreen.setTextSize(18 * MainActivity_FaceandFinger.sDensity);
		mPaintGreen.setTextAlign(Align.CENTER);
		mPaintBlue = new Paint();
		mPaintBlue.setStyle(Paint.Style.FILL);
		mPaintBlue.setColor(Color.BLUE);
		mPaintBlue.setTextSize(18 * MainActivity_FaceandFinger.sDensity);
		mPaintBlue.setTextAlign(Align.CENTER);

		mPaintBlueTransparent = new Paint();
		mPaintBlueTransparent.setStyle(Paint.Style.STROKE);
		mPaintBlueTransparent.setStrokeWidth(2);
		mPaintBlueTransparent.setColor(Color.BLUE);
		mPaintBlueTransparent.setTextSize(25);

		//mBitmap = null;
		mYUVData = null;
		mRGBData = null;

		first_frame_saved = false;
    }

	@Override
	protected void onDraw(Canvas canvas) {
		Log.d("Preview","onDraw being called ");
		//lumidigm.captureandmatch.entity.User userData=null;
		String userID=null;
		if ((mStopping == 1) || (MainActivity_FaceandFinger.appStopped == true)) {
			//if (BioSDKInitialized == true ) {
				Log.d(TAG,"onDestroy releaseBioSDKAPI"+mStopping+"  app stopped  status"+MainActivity_FaceandFinger.appStopped);
				//BioSDKFactory.releaseBioSDKAPI();
			//}
			mStopped = 1;
			Log.d(TAG,"Returning from here ");
			super.onDraw(canvas);
			//onFinishInflate();
			//MainActivity_FaceandFinger.finish();

			return;
		}

		if (mYUVData == null || mTouchedIndex != -1) {
			Log.d(TAG,"Coming out of YUV Data ");
			super.onDraw(canvas);
			return; //nothing to process or name is being entered now
		}

		int canvasWidth = canvas.getWidth();
		//int canvasHeight = canvas.getHeight();

		if (lastSystemTime ==0) {
		//	lastSystemTime++;
			lastSystemTime= System.currentTimeMillis();
		} else {
			final long diff = (System.currentTimeMillis() - lastSystemTime) / 1000;

			Log.d(TAG,"Draw Curr Diff is "+diff);

		/*	if ((diff > 30) && MainActivity_FaceandFinger.EnrolmentStatus) {
				handlercount = 0;
				mStopping = 1;
			//	MainActivity_FaceandFinger.onBackPressed();
			} else*/
			//if ((diff > 8) && (!MainActivity_FaceandFinger.EnrolmentStatus)) {
/*
				if ((diff > 30) && (!MainActivity_FaceandFinger.EnrolmentStatus)) {
			//if ((diff > 15)) {




						GlobalVariables.personDescription="face not matching";
				GlobalVariables.reqStatus= true;


				Log.d(TAG,"Time is more than 20 seconds - Not Right New");
				Intent intent = new Intent(getContext(),SuccessActivity.class);
				intent.putExtra("case","failure");
				intent.putExtra("FromWeb",false);
				*/
/*Bundle mBundle = new Bundle();
				mBundle.putString("case","failure");
				mBundle.putBoolean("FromWeb",false);*//*

				getContext().startActivity(intent);
				handlercount = 0;
				mStopping = 1;

			}
*/
		}


		// Convert from YUV to RGB
		decodeYUV420SP(mRGBData, mYUVData, mImageWidth, mImageHeight);

		//30 frames
		//if (lastSystemTime > 30*20) {
		//	Log.d(TAG,"Last System Time is "+lastSystemTime);
		//}
		// Load image to FaceSDK
		Log.d(TAG,"Before FSDK.HImage");
		FSDK.HImage Image = new FSDK.HImage();
		FSDK.FSDK_IMAGEMODE imagemode = new FSDK.FSDK_IMAGEMODE();
		imagemode.mode = FSDK.FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_24BIT;

		Log.d(TAG,"Before LoadImageFromBuffer");
		FSDK.LoadImageFromBuffer(Image, mRGBData, mImageWidth, mImageHeight, mImageWidth*3, imagemode);
		FSDK.MirrorImage(Image, false);
		//FSDK.HImage RotatedImage = new FSDK.HImage();
		//Chand Commented above - made it global

		if (RotatedImage != null)
			FSDK.FreeImage(RotatedImage);

		RotatedImage = new FSDK.HImage();

		FSDK.CreateEmptyImage(RotatedImage);

		//it is necessary to work with local variables (onDraw called not the time when mImageWidth,... being reassigned, so swapping mImageWidth and mImageHeight may be not safe)
		int ImageWidth = mImageWidth;
		//int ImageHeight = mImageHeight;
		if (rotated) {
			ImageWidth = mImageHeight;
			//ImageHeight = mImageWidth;
			FSDK.RotateImage90(Image, -1, RotatedImage);

		} else {
			FSDK.CopyImage(Image, RotatedImage);
		}
		FSDK.FreeImage(Image);

		// Save first frame to gallery to debug (e.g. rotation angle)
		/*
		if (!first_frame_saved) {
			first_frame_saved = true;
			String galleryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
			FSDK.SaveImageToFile(RotatedImage, galleryPath + "/first_frame.jpg"); //frame is rotated!
		}
		*/

		long IDs[] = new long[MAX_FACES];
		long face_count[] = new long[1];

		FSDK.FeedFrame(mTracker, 0, RotatedImage, face_count, IDs);
		//FSDK.FreeImage(RotatedImage);

//Chand added below code and commented above
		if (face_count[0]  <= 0) {
			FSDK.FreeImage(RotatedImage);
			RotatedImage = null;
		}
		Log.d(TAG,"FreeImage FeedFrame");
		faceLock.lock();

		for (int i=0; i<MAX_FACES; ++i) {
			mFacePositions[i] = new FaceRectangleFaceFinger();
			mFacePositions[i].x1 = 0;
			mFacePositions[i].y1 = 0;
			mFacePositions[i].x2 = 0;
			mFacePositions[i].y2 = 0;
			mIDs[i] = IDs[i];
		}

		float ratio = (canvasWidth * 1.0f) / ImageWidth;

		Log.d(TAG,"Face Count is "+face_count[0]);
		for (int i = 0; i < (int)face_count[0]; ++i) {
			FSDK.FSDK_Features Eyes = new FSDK.FSDK_Features();
			FSDK.GetTrackerEyes(mTracker, 0, mIDs[i], Eyes);

			GetFaceFrame(Eyes, mFacePositions[i]);
			mFacePositions[i].x1 *= ratio;
			mFacePositions[i].y1 *= ratio;
			mFacePositions[i].x2 *= ratio;
			mFacePositions[i].y2 *= ratio;

			Log.d(TAG,"Set FaceFrame ");
		}

		faceLock.unlock();

		int shift = (int)(22 * MainActivity_FaceandFinger.sDensity);

		// Mark and name faces
		Log.d(TAG,"For Mark and Name face facecount "+face_count[0]);

		// Mark and name faces
		Log.d(TAG,"Check For Mark and Name face facecount "+face_count[0]);
		boolean FaceStatus=false;
		//Chand added
		int i1=0;
		if ((FaceTesting == true && face_count[0] > 0) || (FaceBelgium == true && face_count[0] > 0 )) {
			Log.d(TAG,"Going for the CheckMatch Face ");
			canvas.drawRect(mFacePositions[i1].x1, mFacePositions[i1].y1, mFacePositions[i1].x2, mFacePositions[i1].y2, mPaintBlueTransparent);

			Log.d(TAG,"Now Check inthe Database ");
			MainActivity_FaceandFinger.faceChecking=true;
			FaceStatus =CheckMatchFace();
			MainActivity_FaceandFinger.faceChecking=false;

			if (FaceStatus == true) {
				MainActivity_FaceandFinger.tpView0.setText("Your Face is successfully Identified");
			//	userData = IdentifiedUser;

				if (FaceICA) {
					if (GlobalVariables.IDNumber == null) {
						Log.d(TAG,"ID Number is null Face Status is made null");
						FaceStatus = false;
						MainActivity_FaceandFinger.tpView0.setText("Your Face is not Identified");
			//			userData = null;
					}
				}
			} else
				MainActivity_FaceandFinger.tpView0.setText("Your Face is not Identified");
		}

		//remove above code
		for (int i=0; i<face_count[0]; ++i) {
			canvas.drawRect(mFacePositions[i].x1, mFacePositions[i].y1, mFacePositions[i].x2, mFacePositions[i].y2, mPaintBlueTransparent);

			boolean named = false;
			String names[] = new String[1];
			if (IDs[i] != -1) {

				FSDK.GetAllNames(mTracker, IDs[i], names, 1024);
				Log.d(TAG,"GetALlNames length"+names[0].length());

				//below code chand usexr
				//if (face_count[0] > 0)
				//	cardDataUpdateDatabase(names[0]);
				//For Testing

				if (names[0].length() ==0 && MainActivity_FaceandFinger.faceChecking== true) {
					Log.d(TAG,"Face checking is Processing is in progress pls wait ");
				} else
				if (names[0] != null && names[0].length() > 0) {
					Log.d(TAG, "onDraw: "+names[0]);

					Log.d(TAG,"Before setting text view "+names[0]);
					try {

						String username=null;
						try {
							//List<lumidigm.captureandmatch.entity.User> users = MainActivity_FaceandFinger.appDatabase.userDao().getAll();
							//Log.d(TAG, "Get User Count is " + users.size());
							//for (lumidigm.captureandmatch.entity.User user1 : users)
							{

								String userId1 = MainActivity_FaceandFinger.userID; //user1.getUserId();
								Log.d(TAG, "Rcv d User id is " + userId1);
								Log.d(TAG,"User Name is "+names[0]);

								if (userId1 != null && userId1.equals(names[0])) {
									username = userId1; //user1.getFullname();
									Log.d(TAG, "Matching User ID is  " + userId1 + " Name is " + username);
									//}
									///user = user1;
			//						userData = user1;
									if (MainActivity_FaceandFinger.adminVerification == true) {
										Integer RoleID = 3; //user1.getRoleID();
									//	String rolename = user1.getRoleName();
								//		Log.d(TAG, "Role ID of user " + userId1 + " Role ID " + RoleID + "Role name is " + rolename);
										if (RoleID == 3) {
											Log.d(TAG, "Role ID is matching ");
											MainActivity_FaceandFinger.adminVerified = true;
										} else
											Log.d(TAG, "Role ID is not verified ");

									}
									MainActivity_FaceandFinger.IdentifiedUserName=username;
								}
								//Log.d(TAG, "doInBackground: " + user.toString());
								//String fingarprint1 = user.getTemplate1();
							}
							//For testing
							//if (users.size() ==0)
							MainActivity_FaceandFinger.adminVerified = true;
						} catch (Exception obj) {
							Log.d(TAG,"Got Exception ");
							//for testing

						}

                        if (EnrolmentStatus == true)
                            MainActivity_FaceandFinger.tpView0.setText("Your Face is successfully Enrolled");
                        else
							MainActivity_FaceandFinger.tpView0.setText("Your Face is successfully Identified");

						Log.d(TAG,"Username is "+username+" user id is "+names[0]);

                        if (username != null)
							MainActivity_FaceandFinger.tpView.setText(username);
					//	MainActivity_FaceandFinger.tpView.setText(names[0]+" "+username);
                        else
							MainActivity_FaceandFinger.tpView.setText(names[0]);

					} catch (Exception ojb) {
					Log.d(TAG,"Got Exception while Textview setting "+ojb.getMessage());
					}

					canvas.drawText(names[0], (mFacePositions[i].x1+mFacePositions[i].x2)/2, mFacePositions[i].y2+shift, mPaintBlue);
					named = true;
				}else if ((FaceStatus == true ) && (FaceBelgium) && ( GlobalVariables.FaceContinousDetection==true ||
						(CardDataVariables.CardNumber != null && GlobalVariables.currFaceDetectionCount < MaxFaceDetectionCount))) { // && GlobalVariables.IDNumber !=null) {
					Log.d(TAG, "Belgium Face is true checking");
					Intent intent = new Intent(getContext(), FaceSuccessBelgiumEida.class);
					Bundle bundle = new Bundle();
					intent.putExtras(bundle);
					intent.putExtra("FromWeb", false);
					GlobalVariables.currFaceDetectionCount++;
					Log.d(TAG,"Current Face Detection Count chand is "+GlobalVariables.currFaceDetectionCount);
					Log.d(TAG, "Identified User name is " + MainActivity_FaceandFinger.IdentifiedUserName);
					if (MainActivity_FaceandFinger.IdentifiedUserName != null) {
						GlobalVariables.personName = MainActivity_FaceandFinger.IdentifiedUserName;
						MainActivity_FaceandFinger.tpView.setText(GlobalVariables.personName);
					}
					else {
						GlobalVariables.personName = names[0];
						//if (userData2 != null)
						//	intent.putExtra("personName",userData2.getFullname());
						MainActivity_FaceandFinger.tpView.setText(names[0]);
					}
					Log.d(TAG,"User Name is "+names[0]);
						MainActivity_FaceandFinger.tpView.setText(names[0]);
					getContext().startActivity(intent);
					mStopping = 1;
					MainActivity_FaceandFinger.comeOut = 1;

					MainActivity_FaceandFinger.cardReadStatus=1;
					Log.d(TAG,"Card Read Status is "+MainActivity_FaceandFinger.cardReadStatus);
				}
				else if ((FaceStatus == true ) && (FaceICA) && GlobalVariables.IDNumber !=null) {
					Log.d(TAG,"Face ICA is true");
					Intent intent = new Intent(getContext(), FaceSuccessICA.class);

					lumidigm.captureandmatch.entity.User userData2 = IdentifiedUser;
 					Bundle bundle = new Bundle();
					if (userData2 != null)
						bundle.putParcelable("User", userData2);

					intent.putExtras(bundle);
 					intent.putExtra("FromWeb", false);

					if (userData2 != null)
						intent.putExtra("personName",userData2.getFullname());
					getContext().startActivity(intent);
					mStopping = 1;
					MainActivity_FaceandFinger.comeOut = 1;

					MainActivity_FaceandFinger.cardReadStatus=1;
					Log.d(TAG,"Card Read Status is "+MainActivity_FaceandFinger.cardReadStatus);
				}
				//else if (FaceStatus == true && ((GlobalVariables.FaceContinousDetection == true) || GlobalVariables.currFaceDetectionCount < MaxFaceDetectionCount)) {
				//else if (FaceStatus == true) {
				else if (FaceStatus == true && FaceBelgium == false) {
					Log.d(TAG,"FaceSuccess is  true");
					Intent intent = new Intent(getContext(), TerminalFaceCheckInActivity.class);
					//				List<lumidigm.captureandmatch.entity.User> users = MainActivity_Sentinel.appDatabase.userDao().getAll();
					//Log.d(TAG, "Get User Count is " + users.size());
					lumidigm.captureandmatch.entity.User userData2 = IdentifiedUser;
				/*for (lumidigm.captureandmatch.entity.User user1 : users) {

					String userId1 = user1.getUserId();
					Log.d(TAG, "Rcv d User id is " + userId1);
					if (userId1.equals(names[0])) {
						//	username = user1.getFullname();
						Log.d(TAG, "Matching User ID is  " + userId1 + " Name is " + user1.getFullname());
						//}
						///user = user1;
						userData2 = user1;

					}
				}*/
					//final lumidigm.captureandmatch.entity.User curUser =userData;

					Bundle bundle = new Bundle();
					if (userData2 != null)
						bundle.putParcelable("User", userData2);

					intent.putExtras(bundle);
					//  Intent intent = new Intent(this,CheckInActivity.class);
					//startActivity(i);
					//}

					//	intent.putExtra("personName",MainActivity_Sentinel.IdentifiedUserName);



 				intent.putExtra("FromWeb", false);

					if (userData2 != null)
						intent.putExtra("personName",userData2.getFullname());
					getContext().startActivity(intent);
					mStopping = 1;
					MainActivity_FaceandFinger.comeOut = 1;

					MainActivity_FaceandFinger.cardReadStatus=1;
					Log.d(TAG,"Card Read Status is "+MainActivity_FaceandFinger.cardReadStatus);
				}
				else if (EnrolmentStatus == true) {
					Log.d(TAG,"Going for Face Enrollment ");
				}
				else {
					Log.d(TAG,"Not Valid User");
					MainActivity_FaceandFinger.tpView0.setText("You are not Authorized User");

					GlobalVariables.personDescription="face not matching";
					Log.d(TAG,"Face not Matching is success new");
					GlobalVariables.reqStatus= true;
					Log.d(TAG,"Come out is "+MainActivity_FaceandFinger.comeOut);
					if (MainActivity_FaceandFinger.comeOut == 0) {
						MainActivity_FaceandFinger.comeOut = 1;

								Log.d(TAG, "Face identification Failure");


								//String descoveredID = names[0];

								Intent intent = new Intent(getContext(), FaceFailureActivity.class); //SuccessActivity
                                MainActivity_FaceandFinger.comeOutofApp = true;

/*
						if (FaceBelgium == true && MainActivity_FaceandFinger.IdentifiedUserName == null) {
							intent = new Intent(getContext(), BelgiumEidaActivity.class);
							Log.d(TAG,"It is Face Progress initiation ");
						} else*/

                                if (FaceBelgium == true && MainActivity_FaceandFinger.IdentifiedUserName != null &&
										(GlobalVariables.FaceContinousDetection == true || (CardDataVariables.CardNumber != null && GlobalVariables.currFaceDetectionCount < MaxFaceDetectionCount))) {

									intent = new Intent(getContext(), FaceSuccessBelgiumEida.class);
									GlobalVariables.currFaceDetectionCount++;
									intent.putExtra("personName",MainActivity_FaceandFinger.IdentifiedUserName);
									Log.d(TAG,"It is Face Success Belgium chand 222 ");
								} else
                                if (MainActivity_FaceandFinger.IdentifiedUserName == null && FaceICA == true
								&& GlobalVariables.IDNumber == null && GlobalVariables.FaceCardReadCount > 0 && GlobalVariables.FaceCardReadCount < 5) {
                                	intent = new Intent(getContext(), FacePhotoReadProgress.class);
									GlobalVariables.FaceCardReadCount++;
									Log.d(TAG,"ID Number is null so coming out ");
                                } else
									GlobalVariables.FaceCardReadCount=0;
								//There is a change in the Identified User Name
								if (FaceBelgium == false && MainActivity_FaceandFinger.IdentifiedUserName != null) {
									Log.d(TAG,"Valid User "+MainActivity_FaceandFinger.IdentifiedUserName);
									intent = new Intent(getContext(), FaceSuccessActivity.class);

									if (FaceBelgium == true && MainActivity_FaceandFinger.IdentifiedUserName != null) {
										intent = new Intent(getContext(), FaceSuccessBelgiumEida.class);
										GlobalVariables.personName=MainActivity_FaceandFinger.IdentifiedUserName;
										Log.d(TAG,"It is Face Success Belgium checking chand 555");
										GlobalVariables.currFaceDetectionCount++;
									} else
									if (MainActivity_FaceandFinger.adminVerification == true && MainActivity_FaceandFinger.adminVerified == true) {

										Log.d(TAG,"Going for Face Enroll ");

										//intent = new Intent(getContext(), FaceEnroll.class);
										//intent.putExtra("adminverification","success");


										//intent = new Intent(getContext(), Enroll.class);

										//intent.putExtra("userid","test" );
										intent.putExtra("adminVerification",true );

									}  else
									if (!EnrolmentStatus ) {
										Log.d(TAG,"Enrollment Status if false - TerminalFaceCheckInActivity ");
										//if (GlobalVariables.WorkFlowVal != 4 && GlobalVariables.WorkFlowVal != 5) {
										if (MainActivity_FaceandFinger.workflowID !=4 && MainActivity_FaceandFinger.workflowID != 5) {
											intent = new Intent(getContext(), TerminalFaceCheckInActivity.class);
										 } else
										 	Log.d(TAG,"Work Flow Status is TIme and attendance ");

										Log.d(TAG,"CheckIn");
									//	Intent i = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);
										//finish();

							//			List<lumidigm.captureandmatch.entity.User> users = MainActivity_FaceandFinger.appDatabase.userDao().getAll();
							//			Log.d(TAG, "Get User Count is " + users.size());
							/*			lumidigm.captureandmatch.entity.User userData2 = null;
										for (lumidigm.captureandmatch.entity.User user1 : users) {

											String userId1 = user1.getUserId();
											Log.d(TAG, "Rcv d User id is " + userId1);
											if (userId1.equals(names[0])) {
											//	username = user1.getFullname();
												Log.d(TAG, "Matching User ID is  " + userId1 + " Name is " + user1.getFullname());
												//}
												///user = user1;
												userData2 = user1;
											}
										}
							*/			//final lumidigm.captureandmatch.entity.User curUser =userData;

										Bundle bundle = new Bundle();
							//			if (userData2 != null)
							//			bundle.putParcelable("User", userData2);

										intent.putExtras(bundle);
										//  Intent intent = new Intent(this,CheckInActivity.class);
										//startActivity(i);
									}

									intent.putExtra("personName",MainActivity_FaceandFinger.IdentifiedUserName);

								}
								intent.putExtra("FromWeb", false);


								if (MainActivity_FaceandFinger.adminVerification == true && MainActivity_FaceandFinger.adminVerified == false) {

									Log.d(TAG,"AdminVerification Failure ");

									intent.putExtra("adminVerification",true); //to Show Unauthorized access
									intent.putExtra("case","failure");

								} else
								if (EnrolmentStatus) {
									intent.putExtra("case", "forenrollment");
								//	GlobalVariables.CardReadStatus=0;
								} else {
									intent.putExtra("case", "failure");
								//	GlobalVariables.CardReadStatus=0;
								}
								intent.putExtra("source","face");

								//intent.putExtra("personID", descoveredID);
								getContext().startActivity(intent);
								// getContext().finish();


						MainActivity_FaceandFinger.cardReadStatus=1;
						Log.d(TAG,"Card Read Status is "+MainActivity_FaceandFinger.cardReadStatus);
								Log.d(TAG,"in mstopping releaseBioSDKAPI first");
								mStopping = 1;
								 {
									try {
										MainActivity_FaceandFinger.comeOutofApp = true;


//*/
										/*
										if (mVCOM != null) {
											mVCOM.Close();
											mVCOM=null;
											//fingerPrintStarted=-1;
											Log.d(TAG,"On Destroy mvCOM got close ");
										}
*/
									} catch (Exception obj) {
										Log.d(TAG,"Got crashed while face failure "+obj.getMessage());
									}
								}

			/*				}
						}, 50); //5000);*/
					}
					//Chand added


				}
			}

			if(named){
			//if (named && !MainActivity_FaceandFinger.EnrolmentStatus) {
				GlobalVariables.personDescription="face matching";
				Log.d(TAG,"Face Matching is success new");
				GlobalVariables.reqStatus= true;
				GlobalVariables.FaceCardReadCount=0;
 				Log.d(TAG,"Come out is "+MainActivity_FaceandFinger.comeOut);
				//final lumidigm.captureandmatch.entity.User curUser =userData;
				//Log.d(TAG,"Curr User Data is "+userData.getFullname());
				CardUser cardData = CheckIsItCardEnrollment(names[0]);
				if (cardData != null)
					Log.d(TAG,"Card Data is  "+cardData.getCardNumber());

				if (MainActivity_FaceandFinger.comeOut == 0) {
					MainActivity_FaceandFinger.comeOut = 1;
							Log.d(TAG, "Face identification success scucess");
							String descoveredID = names[0];
							Intent intent = new Intent(getContext(), FaceSuccessActivity.class); //SuccessActivity
							Log.d(TAG,"Work Flow status is "+MainActivity_FaceandFinger.workflowID); //.WorkFlowVal);

							if (EnrolmentStatus)
								intent.putExtra("case", "forenrollment");
							else if ((FaceBelgium || FaceBelgiumTest) && cardData != null) {
								Log.d(TAG,"It is Card Data ");
								intent = new Intent(getContext(), FaceSuccessBelgiumEida.class);
								Log.d(TAG,"It is Face Belgium New Coung chand 5");
								GlobalVariables.currFaceDetectionCount++;
								GlobalVariables.personName=names[0];
								CardDataVariables.Name=cardData.getCustomerName();
								CardDataVariables.Name_2=cardData.getCustomerName2();
								CardDataVariables.CardNumber = names[0];

								CardDataVariables.Nationality = cardData.getNationality();
								CardDataVariables.ValidityEnd=cardData.getValidityEnd();

								GlobalVariables.currFaceDetectionCount++;
							} else
								if (FaceBelgium && GlobalVariables.currFaceDetectionCount < MaxFaceDetectionCount) {
								 intent = new Intent(getContext(), FaceSuccessBelgiumEida.class);
								 Log.d(TAG,"It is Face Belgium New Coung chand 3");
								GlobalVariables.currFaceDetectionCount++;
								GlobalVariables.personName=names[0];
								GlobalVariables.currFaceDetectionCount++;
							} else

								if (MainActivity_FaceandFinger.adminVerification == true && MainActivity_FaceandFinger.adminVerified == false) {

								Log.d(TAG, "AdminVerification Failure ");
								intent = new Intent(getContext(), FaceFailureActivity.class);
								intent.putExtra("adminVerification", true);
								intent.putExtra("case", "failure");
							} else if (MainActivity_FaceandFinger.adminVerification == true && MainActivity_FaceandFinger.adminVerified == true) {

								Log.d(TAG, "Going for Face Enroll ");

							//	intent = new Intent(getContext(), FaceEnroll.class);
								intent.putExtra("case", "success");
								intent.putExtra("adminVerification", true);
							}
							else {
							//test
								if (MainActivity_FaceandFinger.workflowID != 4 && MainActivity_FaceandFinger.workflowID != 5) {

									//	if (GlobalVariables.WorkFlowVal != 4 && GlobalVariables.WorkFlowVal != 5) {
									intent = new Intent(getContext(), TerminalFaceCheckInActivity.class);
								}
								Log.d(TAG,"Sending CheckIn");
								Bundle bundle = new Bundle();
							//	if (curUser != null)
							//		bundle.putParcelable("User", curUser);

								intent.putExtras(bundle);

								//below
								intent.putExtra("case", "success");

							}
							intent.putExtra("FromWeb", false);
/*
							if (MainActivity_FaceandFinger.EnrolmentStatus)
								intent.putExtra("case", "forenrollment");
							else*/


							intent.putExtra("personID", descoveredID);

							intent.putExtra("source", "face");

							Bundle bundle = new Bundle();

							/*if (curUser != null) {
								Log.d(TAG,"User Full name "+curUser.getFullname());
								bundle.putParcelable("User", curUser);
								intent.putExtras(bundle);
							} else*/
								Log.d(TAG,"Curr User is null");


 							getContext().startActivity(intent);
							// getContext().finish();

					MainActivity_FaceandFinger.comeOutofApp = true;

					MainActivity_FaceandFinger.cardReadStatus=1;
					Log.d(TAG,"Card Read Status is "+MainActivity_FaceandFinger.cardReadStatus);
//					/*
/*					try {

                        if (MainActivity_FaceandFinger.mVCOM2 != null) {
                            MainActivity_FaceandFinger.mVCOM2.Close();
                            MainActivity_FaceandFinger.mVCOM2 = null;
                            //fingerPrintStarted=-1;
                            Log.d(TAG, "On Destroy mvCOM got close ");
                        }
                    } catch (Exception obj) {
					    Log.d(TAG,"Face mCOM Failure ");
                    }*/
//*/
							mStopping = 1;

							try {

							} catch (Exception obj) {
								Log.d(TAG,"crashed while release BiosdkAPI "+obj.getMessage());
							}
					MainActivity_FaceandFinger.comeOut = 1;
/*
						}
					}, 10);//10
*/
					//}, 4000);
				}
				//Chand added

			}else {
				//Chand added
				if (EnrolmentStatus)
				{
			//		canvas.drawText("Tap to Enroll", (mFacePositions[i].x1 + mFacePositions[i].x2) / 2, mFacePositions[i].y2 + shift, mPaintGreen);
					canvas.drawText("Tap to Enroll", (mFacePositions[i].x1 + mFacePositions[i].x2) / 2, mFacePositions[i].y2 + shift, mPaintBlue);
				}
  			}

		}

		super.onDraw(canvas);
	} // end onDraw method

	private boolean CheckMatchFace() {
		try {

			boolean status =false;
			Log.d("Test", " in CheckMatchFace ");
			//	FSDK.HImage ImageTest=null;

			FSDK.HImage ImageTest = new FSDK.HImage();

			Log.d(TAG, "AFter free Iamge ");

			Image = new FSDK.HImage();

			float Similarity[] = new float[4];
			float MatchingThreshold[] = new float[4];
//0.2
			FSDK.GetMatchingThresholdAtFAR((float) 0.1, MatchingThreshold);

			Log.d(TAG, "Matching Threshold is " + MatchingThreshold[0] + "  " + MatchingThreshold[1]);

			FSDK.FSDK_FaceTemplate template2 = new FSDK.FSDK_FaceTemplate();
			FSDK.GetFaceTemplate(RotatedImage, template2);

			//FSDK.FSDK_FaceTemplate template3 = new FSDK.FSDK_FaceTemplate();
			lastSystemTime = System.currentTimeMillis();

			String Userid = "91";
			String galleryPath =
					Environment.getExternalStorageDirectory()
							+ File.separator
							+ "displayImages" + File.separator + Userid + ".jpg";


			Log.d(TAG,"User ID is "+Userid);

			FSDK.FSDK_FaceTemplate template4 = new FSDK.FSDK_FaceTemplate();
			Log.d(TAG,"Before GetAll Users new ");
			MainActivity_FaceandFinger.IdentifiedUserName=null;

			//List<lumidigm.captureandmatch.entity.User> users = MainActivity_FaceandFinger.appDatabase.userDao().getAll();

			//Log.d(TAG, "Get User Count is " + users.size());
			//for (lumidigm.captureandmatch.entity.User user1 : users)
			//int iUserID=995;
			if (CardDataVariables.CardNumber != null)  {
				//iUserID=CardDataVariables.CardNumber;

			//for (iUserID=991;iUserID<=995;iUserID++)
		//	for (iUserID=1;iUserID<2;iUserID++)
		//	{

				String currUserID = CardDataVariables.CardNumber;
				currUserID = "995"; //just for testing

				//currUserID = currUserID+"new"

				//	Log.d(TAG,"UserID checking is "+user1.get_id());

				Log.d(TAG,"UserID checking is "+currUserID);
				/*Log.d(TAG,"Template is "+user1.getTemplateFace());
				byte[] mTmplCapture1 = Base64.decode(user1.getTemplateFace(), Base64.DEFAULT);*/
				//Log.d(TAG,"Template is "+user1.getTemplate3());
				galleryPath =
						Environment.getExternalStorageDirectory()
								+ File.separator
								+ "displayImages" + File.separator + currUserID + ".jpg";

	//							+ "displayImages" + File.separator + iUserID + ".jpg";
				Log.d(TAG,"Getting the Path of UserID "+galleryPath);

				//if (galleryPath)

				File fp = new File(galleryPath);

				if (fp.exists()) {
					FSDK.FSDK_FaceTemplate template3 = new FSDK.FSDK_FaceTemplate();
					FSDK.LoadImageFromFile(ImageTest, galleryPath); //frame is rotated!
					FSDK.GetFaceTemplate(ImageTest, template3);

					byte[] test = new byte[template3.template.length];
					test= template3.template;

					Log.d(TAG,"Size of Template is "+template3.template.length);
					Log.d(TAG,"Size of Template is "+template4.template.length);

					Log.d(TAG," Template is "+test);

					String tem1 = Base64.encodeToString(test, Base64.DEFAULT);
					Log.d(TAG,"Template is "+tem1);
					byte[] mTmplCapture1 = Base64.decode(tem1, Base64.DEFAULT);
					template4.template = mTmplCapture1;

					FSDK.MatchFaces(template2, template4, Similarity);
					//Only Match Fasces for - 100 templates it is taking 1520 milli second

					Log.d(TAG, "Template similarity is " + Similarity[0] + "  " + Similarity[1]);
					Log.d(TAG, "Template similarity is " + Similarity[2] + "  " + Similarity[3]);
					Log.d(TAG, "Matching Threshold is " + MatchingThreshold[0]);

					if (Similarity[0] > MatchingThreshold[0]) {
						Log.d(TAG,"Face Matching Success ");
						status= true;
						//IdentifiedUser = user1;
						MainActivity_FaceandFinger.IdentifiedUserName=currUserID; //String.valueOf(iUserID);
						GlobalVariables.personName=currUserID; //String.valueOf(iUserID);
						//break;
					} else {
						Log.d(TAG,"Face Matching Failure ");
						status= false;
					}
				}  else
		/*		if (user1.getTemplate3() != null) {
					byte[] mTmplCapture1 = Base64.decode(user1.getTemplate3(), Base64.DEFAULT);
					template4.template = mTmplCapture1;

					FSDK.MatchFaces(template2, template4, Similarity);
					//Only Match Fasces for - 100 templates it is taking 1520 milli second


					Log.d(TAG, "Template similarity is " + Similarity[0] + "  " + Similarity[1]);
					//Log.d(TAG, "Template similarity is " + Similarity[2] + "  " + Similarity[3]);

					if (Similarity[0] > MatchingThreshold[0]) {
						Log.d(TAG, "Face Matching Success " + user1.getFullname());
						status = true;
						IdentifiedUser = user1;
						break;
					} else {
						Log.d(TAG, "Face Matching Failure ");
						status = false;
						IdentifiedUser = null;
					}
				} else*/
					Log.d(TAG,"Template is null");

				IdentifiedUser = null;
				status =false;
			}
			return status;
		} catch (Exception ojb) {
			MainActivity_FaceandFinger.IdentifiedUserName=null;
			Log.d(TAG,"Got Exception and got crashed ");
		}
		return false;
	}

	private CardUser CheckIsItCardEnrollment(String cardID) {
		CardUser currData=null;

		Log.d(TAG,"It is card Configured "+cardID);
		CardUser currCardUser =MainActivity_FaceandFinger.appCardDatabase.carduserDao().countUsersBasedonCardNumber(cardID);
		if (currCardUser != null) {
	 		return currCardUser;
		} else
			Log.d(TAG,"No Card Data for this cardid  "+cardID);

		return null;
	}
	private boolean cardDataUpdateDatabase(String userID) {

		try {
			Log.d(TAG,"in cardDataUpdateDatabase");
				Log.d(TAG,"Update database cardnumber "+userID);

			List<CardUser> cardUser = MainActivity_FaceandFinger.appCardDatabase.carduserDao().getAll();

			Log.d(TAG,"Current Users are "+cardUser.size());

			CardUser currCardUser =MainActivity_FaceandFinger.appCardDatabase.carduserDao().countUsersBasedonCardNumber(userID);
			if (currCardUser != null) {
				//appDatabase.userDao().update(user1);
			//	GlobalVariables.CardReadStatus=5; //Already read these data
				Log.d(TAG, "Data is already there - no need Enroll"+currCardUser.getCardNumber());
				return false;
			} else {
				belgiumeida.carddatabase.CardUser data = new CardUser();

				Log.d(TAG,"Going for Inserting card number "+userID);


				data.setChipNumber("1001");
				data.setCardNumber(userID);
				data.setChipNumber("1001");
				data.setCustomerName("Chand");
				data.setCustomerName2("Lakshmi Chand");
				data.setFirstLetterName("LAKS");
				data.setNationality("Indian");
				data.setNationalNumber("100");
				data.setNobleCondition("lovely");
				data.setValidtyBegin("100");
				data.setValidityEnd("100");

				MainActivity_FaceandFinger.appCardDatabase.carduserDao().insertAll(data);
				return true;
			}
			//Send to the Server
		} catch (Exception obj) {
			Log.d(TAG,"Updating Card data Got exception "+obj.getMessage());
		}
		return true;
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) { //NOTE: the method can be implemented in Preview class

		Log.d(TAG,"onTouchEvent");
		String Userid = MainActivity_FaceandFinger.userID;
		//SharedPreferences Face;
		if (!EnrolmentStatus)
			return true;

		Log.d(TAG,"onTouchEvent"+event.getAction());
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			int x = (int)event.getX();
			int y = (int)event.getY();
			
			faceLock.lock();
			FaceRectangleFaceFinger rects[] = new FaceRectangleFaceFinger[MAX_FACES];
			long IDs[] = new long[MAX_FACES];
			for (int i=0; i<MAX_FACES; ++i) {
				rects[i] = new FaceRectangleFaceFinger();
				rects[i].x1 = mFacePositions[i].x1;
				rects[i].y1 = mFacePositions[i].y1;
				rects[i].x2 = mFacePositions[i].x2;
				rects[i].y2 = mFacePositions[i].y2;
				IDs[i] = mIDs[i];
			}
			faceLock.unlock();
			
			for (int i=0; i<MAX_FACES; ++i) {
				if (rects[i] != null && rects[i].x1 <= x && x <= rects[i].x2 && rects[i].y1 <= y && y <= rects[i].y2 + 30) {
					mTouchedID = IDs[i];
					popup = true;
					mTouchedIndex = i;

					Log.d(TAG,"Rcvd UserID is "+Userid);
					if (Userid  != null && Userid.length() > 0) {

						if ((FaceTesting == true) || (FaceBelgium == true)) {
							Log.d(TAG,"Face Testing is true ");
							if (RotatedImage != null) {
								//first_frame_saved = true;
								String galleryPath =
										Environment.getExternalStorageDirectory()
												+ File.separator
												+ "displayImages" + File.separator + Userid + "new.jpg";
								Log.d(TAG, "Saving the Rotated Image to " + galleryPath);
								FSDK.SaveImageToFile(RotatedImage, galleryPath); //frame is Rrotated!
								//FreeImage

								FSDK.FreeImage(RotatedImage);
								RotatedImage = null;
							}
						}
						//Chand added below
						// requesting name on tapping the face
					//	final EditText input = new EditText(mContext);
	/*					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
						builder.setMessage("Enroll the User "+Userid )
						//		.setView(input)
								.setPositiveButton("Enroll", new DialogInterface.OnClickListener() {
									@Override public void onClick(DialogInterface dialogInterface, int j) {
										FSDK.LockID(mTracker, mTouchedID);
					//					String userName = input.getText().toString();
										//String userName =

										String userName = Userid;
										Log.d(TAG,"User name is "+Userid);
										FSDK.SetName(mTracker, mTouchedID, userName);
										if (userName.length() <= 0) FSDK.PurgeID(mTracker, mTouchedID);
										FSDK.UnlockID(mTracker, mTouchedID);
										mTouchedIndex = -1;
									}
								})
								.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
									@Override public void onClick(DialogInterface dialogInterface, int j) {
										mTouchedIndex = -1;
									}
								})
								.setCancelable(false) // cancel with button only
								.show();
						popup = false;
	*/					//Chand added above

						FSDK.LockID(mTracker, mTouchedID);
						String userName = Userid;
						FSDK.SetName(mTracker, mTouchedID, userName);
						Log.d(TAG,"Saved the User name "+userName);
						//PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("MYLABEL", userName).apply();
						popup = false;
						if (userName.length() <= 0)
							FSDK.PurgeID(mTracker, mTouchedID);

						FSDK.UnlockID(mTracker, mTouchedID);
						mTouchedIndex = -1;

						//Face=getApplicationWindowToken().getSharedPreferences("face",MODE_PRIVATE);
						//Face.edit().putBoolean("face", false).apply();

					} else
						Log.d(TAG,"User ID is not valid ");

					break;
				}
			}
		}
		return true;
	}
	
	static public void decodeYUV420SP(byte[] rgb, byte[] yuv420sp, int width, int height) {
		final int frameSize = width * height;
		int yp = 0;
		for (int j = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++) {
				int y = (0xff & ((int) yuv420sp[yp])) - 16;
				if (y < 0) y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}	
				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);
				if (r < 0) r = 0; else if (r > 262143) r = 262143;
				if (g < 0) g = 0; else if (g > 262143) g = 262143;
				if (b < 0) b = 0; else if (b > 262143) b = 262143;
				
				rgb[3*yp] = (byte) ((r >> 10) & 0xff);
				rgb[3*yp+1] = (byte) ((g >> 10) & 0xff);
				rgb[3*yp+2] = (byte) ((b >> 10) & 0xff);
				++yp;
			}
		}
	}


} // end of FaceandFingerProcessImageAndDrawResults class


// Show video from camera and pass frames to ProcessImageAndDraw class
class FaceandFingerPreview extends SurfaceView implements SurfaceHolder.Callback {
	Context mContext;
	SurfaceHolder mHolder;
	Camera mCamera;
	FaceandFingerProcessImageAndDrawResults mDraw;
	boolean mFinished;

	FaceandFingerPreview(Context context, FaceandFingerProcessImageAndDrawResults draw) {
		super(context);      
		mContext = context;
		mDraw = draw;
		
		//Install a SurfaceHolder.Callback so we get notified when the underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		Log.d("Preview","Got the Preview");
	}

	//SurfaceView callback
	public void surfaceCreated(SurfaceHolder holder) {
		mFinished = false;
				
		// Find the ID of the camera
		int cameraId = 0;
		boolean frontCameraFound = false;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Log.d("Preview","Got the Preview Created "+Camera.getNumberOfCameras());

		for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
			Camera.getCameraInfo(i, cameraInfo);

			//Camera.CameraInfo.CAMERA_FACING_BACK
			//if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				cameraId = i;
				frontCameraFound = true;
			}
		}

		Log.d("MainAct","Camera3 ID is "+cameraId);
		//frontCameraFound= true; //chand for testing
		if (frontCameraFound) {
			Log.d("Preview","Front Camera found");
			mCamera = Camera.open(cameraId);
		} else {
			Log.d("Preview","Back Camera Opened");
				try {
				mCamera = Camera.open(0);
			}catch (Exception e){

			}
		//	mCamera = Camera.open();
		}

		if (mCamera == null) {
			Log.d("Preview", "Opened Camera is null");
			//mStopping=1;
			MainActivity_FaceandFinger.appStopped=true;
		} else
		if (holder == null)
			Log.d("Preview","Rcvd Holder is null");
		else {
			try {
				Log.d("Preview", "setPReviewDisplay");
				mCamera.setPreviewDisplay(holder);

				Log.d("Preview", "Before SetPreview Callback");
				// Preview callback used whenever new viewfinder frame is available
				mCamera.setPreviewCallback(new PreviewCallback() {
					public void onPreviewFrame(byte[] data, Camera camera) {
						if ((mDraw == null) || mFinished)
							return;

						if (mDraw.mYUVData == null) {
							// Initialize the draw-on-top companion
							Camera.Parameters params = camera.getParameters();
							mDraw.mImageWidth = params.getPreviewSize().width;
							mDraw.mImageHeight = params.getPreviewSize().height;
							mDraw.mRGBData = new byte[3 * mDraw.mImageWidth * mDraw.mImageHeight];
							mDraw.mYUVData = new byte[data.length];
						}

						Log.d("Preview", "Before System arraycode ");
						// Pass YUV data to draw-on-top companion
						System.arraycopy(data, 0, mDraw.mYUVData, 0, data.length);
						mDraw.invalidate();
					}
				});
			} catch (Exception exception) {
				Log.d("Preview", "Camera Failed Exception " + exception.toString());
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setMessage("Cannot open camera")
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								android.os.Process.killProcess(android.os.Process.myPid());
							}
						})
						.show();
				if (mCamera != null) {
					mCamera.release();
					mCamera = null;
				}
			}
		}
	}

	//SurfaceView callback
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.
		mFinished = true;
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}
	
	//SurfaceView callback, configuring camera
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

		if (mCamera == null) return;
		Log.d("PReview","SurfaceChanged ");
		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		try {
			Camera.Parameters parameters = mCamera.getParameters();

			//Keep uncommented to work correctly on phones:
			//This is an undocumented although widely known feature
			/**/

			Log.d("Preview", "surfaceChanged " + M320Support);

			if (M320Support == true) {
				if (ProductV4Support == true) {
					Log.d("tag", "It is V4Support");
					if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
						Log.d("tag", "It is Not Portrait");
						//	parameters.set("orientation", "portrait");
						//		mCamera.setDisplayOrientation(0); // For Android 2.2 and above
						parameters.set("orientation", "portrait");
						mCamera.setDisplayOrientation(0); // For Android 2.2 and above
						//mDraw.rotated = true;

//			mDraw.rotated = true;
						Log.d("tag", "inside if ");
					} else {
						Log.d("tag", "inside else new -  it is potrait ");
						parameters.set("orientation", "landscape");
						mCamera.setDisplayOrientation(180); // For Android 2.2 and above
					}
				} else if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
					parameters.set("orientation", "portrait");
					mCamera.setDisplayOrientation(0); // For Android 2.2 and above
//			mDraw.rotated = true;
					Log.d("tag", "inside if ");
				} else {
					Log.d("tag", "inside else ");
					//	parameters.set("orientation", "landscape");
					//	mCamera.setDisplayOrientation(0); // For Android 2.2 and above

					parameters.set("orientation", "portrait");
					mCamera.setDisplayOrientation(90); // For Android 2.2 and above
					//mCamera.setDisplayOrientation(0); // For Android 2.2 and above

					mDraw.rotated = true;
				}
			} else if (ProductV4Support == true) {
				if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
					parameters.set("orientation", "portrait");
					mCamera.setDisplayOrientation(0); // For Android 2.2 and above
//			mDraw.rotated = true;
					Log.d("tag", "inside if ");
				} else {
					Log.d("tag", "inside else ");
					parameters.set("orientation", "landscape");
					mCamera.setDisplayOrientation(180); // For Android 2.2 and above
				}

			} else {
				int orientation = Configuration.ORIENTATION_LANDSCAPE;

				if (ProductV4Support == true)
					orientation = Configuration.ORIENTATION_PORTRAIT;

				if (this.getResources().getConfiguration().orientation != orientation) {
					//	if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
					parameters.set("orientation", "portrait");
					mCamera.setDisplayOrientation(90); // For Android 2.2 and above
					mDraw.rotated = true;
				} else {
					parameters.set("orientation", "landscape");
					mCamera.setDisplayOrientation(0); // For Android 2.2 and above
				}
			}
			//Needed


			/**/
			int width = 0;
			int height = 0;
			try {

				// choose preview size closer to 640x480 for optimal performance
				List<Size> supportedSizes = parameters.getSupportedPreviewSizes();

				for (Size s : supportedSizes) {
					Log.d("Face", "Supported sizes " + s.width + "  " + s.height);
				/*	if ((width - 640) * (width - 640) + (height - 480) * (height - 480) >
							(s.width - 640) * (s.width - 640) + (s.height - 480) * (s.height - 480)) {*/

                    if ((width - 480) * (width - 480) + (height - 640) * (height - 640) >
                            (s.width - 480) * (s.width - 480) + (s.height - 640) * (s.height - 640)) {
						width = s.width;
						height = s.height;
						Log.d("Face", "Selected width" + width + " height " + s.height);
					}
				}
			} catch (Exception obj) {
				Log.d("Face", "Got Exception ");

			}
			//try to set preferred parameters
			try {

			    Log.d("Chand", "Current Width is " + width + " height is" + height);
                width = 480;
                height = 640;

                Log.d("Chand", "Current Width is " + width + " height is" + height);

                if (width * height > 0) {
					parameters.setPreviewSize(width, height);
				}
				//parameters.setPreviewSize(176, 144);

				Log.d("Chand", "Chand modified the size 140, x00");
				//parameters.setPreviewFrameRate(10);
				parameters.setSceneMode(Camera.Parameters.SCENE_MODE_PORTRAIT);
				parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
				mCamera.setParameters(parameters);
			} catch (Exception ex) {
			}
			mCamera.startPreview();

			parameters = mCamera.getParameters();
			Size previewSize = parameters.getPreviewSize();
			makeResizeForCameraAspect(1.0f / ((1.0f * previewSize.width) / previewSize.height));
		} catch (Exception obj) {
			Log.d("Chand","Got exception while surface"+obj.getMessage());

		}
	}
	
	private void makeResizeForCameraAspect(float cameraAspectRatio){
		LayoutParams layoutParams = this.getLayoutParams();
		int matchParentWidth = this.getWidth();           
		int newHeight = (int)(matchParentWidth/cameraAspectRatio);
		if (newHeight != layoutParams.height) {
			layoutParams.height = newHeight;
			layoutParams.width = matchParentWidth;    
			this.setLayoutParams(layoutParams);
			this.invalidate();
		}		
	}
} // end of Preview class
