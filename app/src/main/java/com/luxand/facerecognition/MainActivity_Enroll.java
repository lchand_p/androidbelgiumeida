package com.luxand.facerecognition;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.android.volley.VolleyError;

//import facerecognition.FSDK;//
//import facerecognition.FSDK.HTracker;
import lumidigm.HomeActivity;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FingerScannActivity;
import lumidigm.captureandmatch.activites.Main2Activity;

import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import com.luxand.FSDK;
import com.luxand.FSDK.HTracker;
import static com.android.volley.Request.Method.GET;
import static com.luxand.FSDK.FSDKE_OK;

import  android.content.pm.PackageManager;
public class MainActivity_Enroll extends Activity implements OnClickListener {
	private static final String TAG = "MainActivity_Enroll";
	private boolean mIsFailed = false;
	private Preview_Enroll mPreview;
	private ProcessImageAndDrawResults_Enroll mDraw;
	private final String database = "New_Memory50.dat";
	private final String help_text = "Luxand Face Recognition\n\nJust tap any detected face and name it. The app will recognize this face further. For best results, hold the device at arm's length. You may slowly rotate the head for the app to memorize you at multiple views. The app can memorize several persons. If a face is not recognized, tap and name it again.\n\nThe SDK is available for mobile developers: www.com.luxand.com/facesdk";
	public static float sDensity = 1.0f;
	private boolean fromWeb=false;
	private String currState = null;
	private SharedPreferences prefs =  null;

	//private String TAG="FaceDetection";
	private boolean CameraStatus = false;
	public  static boolean EnrolmentStatus = false;
	//String tp="RlNESwYAAAAIAAAAAAAAAAcAAAAAAAAAAQAAAAEAAAAFAAAANjEyMgACAAAAAQAAABAEAABmc2RrAwAAABAEAABDlKtCzEWxPSKF0r0xW+q9rGgLvNbT6bu0Erk8SlWNPCNzUD0UpKM8H3GJPNvEVL0H5cS7+L0cPIjOjb1V0a+8rHgrPaSzx7xVc1K8W/eyvEpX7Dwr3gI9iB+NPA8C/b2xXaW9gk1hPLFDEjwZb7q700OPPdXzaj3eC3k9mKVnPaKWnj2vlFi8rocDvl3Vn718PFq9t214PepZWD3Up+C9MfwPvRRDgD235GI9f+BxvZOYrT02jjW9qVRDvTIkTD2ikT09zmj+vdRgyD3RGW29IvlVvZwdM7ztOyA9/OZMveQVijyqECc9OyYwvHJXkL3wSYg9vVQjvRDiHT19seq8Oal/PYRxfz0V5x09SES3vcYgmj1l/ge8iS/GPNNuTb1sWQc9PFWSPOiuA73Gz8s81c4cveQBrD2W6Hg7v8lfvbicaLyaLcy9jPkgPqQfDj2bhGQ9CE0DvQTS4z2FCmA9I5LFPR991DzUaqc9qKlOPSMNVTztf+c9WBDcvI7TBD2ZRW28a7oNPSonS71irIw8wiQlPI6EzLytA609ZyRRvbdzuL2RBn+9SZcBPkHngb3fQwM81HO/O5BzDj35NGg9uJOGPEB4Ir11rNW9TXcdvKvGmz0GP+68Dp0hvMRQBT2nOgQ91/YTPdMTr7yecpS9uPemvVbZGr7sYly9Wnu9vLtMMzwnpUK9WOeBvQF+Ir2KC+y9SyTKPRkB2D2zL0S9GxIAPRz7PT00HJm9DZfUvB8uTDzguCu9OUDgPX+fgT1jbSU+d6spvSaYPL3Kqnw9I2t8vftsyzvwxVI9CYcmvQ40gz3dlZS8D4Y7vVoQDD7JF7O9eliwvXRztL30LNE9qktfvbCk+D0gZhG+opSevbSjvbyuJVS9Ir0bu0Y08ryKDsy7ZRGevf9WmjwohRg9iUVSvcosqb1Hj7i9iTKvUJIm72oN0W9JpTXvXL9VT3bIyS9E3wAvZ1YhzwFaLu94MEfPUDVF721vwo9FzcOvUajHzsNH7C8rXHFvSW+VTzLcxm9NyF/vN6K1j3eE+i8SzpVPA6yJ70C9AK9wmv6Pfvqar1JyuE8Qr9/vclmZD0qE0C8ngvqu5P9m70rov29KZFEPeMueTwC09e9xqXPvN2otrwVCLw8qLB6vZJl2L1tc909Dia8PBDTWD2eFJU8dTvwu02y0b0pmxe9Mhc8vRHM/LwBSYs9O7EOPaZRkT3qlCU+5CGbPKn5nzzRCgw85vuWu6hyIr1qc507EKcWPXLLpz1xkIA8Oo9TPZWa9rzkrAu9wmRGvVHNkT3GUiG98Rx0vVCk6jxXDJK9HpGkPWDzLr24jLC9jJSPvQLGP72UQgE9kduCPKlgDTwoLBg+Z28kPAMAAAAAAAAAAgAAAAAAAAABAAAAAAAAAABgAAAAYAAAAAAkAACQiYJ8eHNvbGpmZGFdWVNKQTk1NDMxLy4tLS0tLS0sLCwtMDE0Nzo9P0FCREVHSElKTE1OUFFSUlRYXWRpa2pueYWKjI2RlZmdoKSloZN6ibG9wMLDw8PDw8PCwsHBwcGQiYN9eHRvbGllY19cWVFIPjc0MzEvLi0tLSwsLS0tLS0tMDIzNjo8P0FCREZHSElLTU5PUVJSU1RYXGBjZ2lxe4WJiYqPlZmfpKalo5qJa4KxvcDBwcLCwsLCwcHAwMCSiYJ9eHNwbGllYl9cV01FOzUzMjAvLi0tLS0tLS4vLzAwMTE0ODs9QEFCRUZISktNTk9QUVJSU1RXXGFkZmt1f4WIiIqOk5qeo6eopKCVeWOLt7y+v8DBwcHBwMDAwMCQiIF8d3Nva2hlYl5ZU0ANjMyMS4uLS0uLi8wMDEzMzMzMzQ1OTw+QEJDRUdKS01OT1BQUFFRUlNWXGJmaGx3f4WIiIqQlZqeoKSmpqGbiWZilre8vr/AwMDAwMC/v7+NhoB7dnNva2dkYFxVT0U8NDIwLy4ti8wMTIzNDQ2Njc2Njc4Oj1AQkNER0lLS0xOT1BPTk5QUVRaX2Roa294fYOHiIqRmJyfoaappqKckXVZbaO3u72/v7/Av7+/v7+Jg356dnJta2djXlZPRj85MzIvLi0tLjAyMzM1Nzg6Ojo6ODk7PUBCQ0RHSElKS0tMTU1NTU1PUlZbY2tucnZ6f4KHi46Ump6hpKmtraadlIFhWYWwuLu9vb6/v76+vr2GgXx4dHBsaWVgWU5GPzk2MzEvLS0tLjAyNDU3ODs9PT49PDw+P0FDREVHSElJSUlKS0xLS0tNUlhfZ21ydnl8f4GFiY2UmZ6gpKmtsK2nm4dqVGSZtLi6vL2+vb29vb2Cfnl2c29rZ2NeVUk/OTYzMjAuLS0uLzIzNDY4Oj1AQkFAPz9AQUJDREVHSElIR0dISUlJSkpMUldeZmtwdHd6e31/g4mPlpyfpKirrq6on5ByV1l5q7e3ury8vb29vb1+e3h0cW5pZV5XTEA6NTQyMC4tLi4vMDI0NTc5Oz1AQ0NCQEBBQUJDREVHSEhGRERFRkZISUtMT1RZYmhscHR0dHZ3fIGIkpufoqerra6pn5F6XFJnjqq0uLu8vLy8vLx6eHVzb2xoY1pRQzg0MzMxLy0tLi8vMDEyMzU3OTs+QUJBQUBBQkJDQ0RGR0VDQUJBQ0NERkhKS09TWmJpbW9zc3R1dXh+hZSgpKeprK6sopF8YlZdepyutbm7u7u8u7t2dHJwbmpmYVhLPTMwMDEwLi0tLi8vLzAwMDEyNDc6PT4/QEBBQkJDQ0NERUNAPj8/Pj9AQUNERUhNUlhgY2hrbnBzdXZ4eHuTo6aprK6upZN6YVhbZ4Sjsbe5ubq6urp0c3Bua2hkXlJDNS8uLzAvLS0sLS0sLCwrLCsrLS8zNjg5Oz0/QEFCQkJCQkE+Ozs7Ojg6PD4+Pj9DSlBZXmBgYmVpaWtwc25th6Opra6vp5N9Y1dXX3CQqbK2uLm5ublycG5saGVgWEs7LiosLi4sKyopJyYlyMiISEhISUoKy8xNDc5PD0+Pz8+Pj06ODc2NDMzNTg7PT5ARUpRU1NVV1pcXVxcXWJlaoqjra+vqZV+aFpWWl93mamxtba2trZwbmxrZ2RfVEY1JycrLiwnJSMhIB0cGhgXFhYYGR0hJCcqLTAyNTc4OTo7Ozg2MzExLy0rKzAzNjY6P0BCQUE/PkJFS05QV1hdYWyRp6+wqph+aF9YWVtkiKKssbS1tbVtbGppZmJdTz8sIiQoKSckIB0aFxYVExIQDg4RExYbHyIkJCcrLjAwMTU3ODczLysrLCkmJikrLCouNjo5NDEuLzIzNDtFTllfYmZ2lqiuq5uCaF1WT1RZdZ2pr7K0tLRsamhnZGBZSzkmICIkJSIdGRUTERAPDQwMCwsLDhAUGBseHh8iJiorKy8zNjQwLSooKiolIyIjJicrMjU0MCsoJykxN0BKUVhdY2lrh6esqp6FaFxbTU5XZoqhqLCzs7NqaGdlYlxTRDIhHCAhIR0YEw4ODAoJBwcHCAkJCgoOERQXFxcaHyIkJiovMzQxLCgpKionJCMlKCwuMjQxKygoKyw1QElTW2BjaGtscZeurqOLbF5bVlBZYn2ZpayxsrJpZ2ZjYFhMOSocGR0dHBcTDwwLCQgHBgUGBgcICAkKDA8RExMUGBscICcsMjQzLykoKy4vKyksLzM1NTQzMC4tLS42QlBZX2ZqbHNzcIChrauUcV5a09SXnCRo6iusLFoZmRgW1NHNiQYGBkaGRQQDQwLCQgGBQUFBQYFBgcICQsNDhASExcXGSAnMTM0NS4sLzQ2NjQ0NDc2NTUzMzEvLSwwOkpXYGZxeHt/fXyTqaubel1ZWlZOVmaKo6mtr7BnZWNfW1NFNCEWFRYYFhMQDgwLCgkIBgYFBQUFBQQFBggJCgwNEBMUFhskMTc4OTg";
	//private long lastSystemTime = 0;
	//public FSDK.FSDK_IMAGEMODE mCameraImageMode = new FSDK.FSDK_IMAGEMODE();
	 static byte testbyte[] = new byte[100000];
	  private String testbyte2 ="RlNESwYAAAAMAAAAAAAAAAwAAAAAAAAAAQAAAAEAAAAFAAAAMTE2MgABAAAAAQAAABAEAABmc2Rr\n" +
			 "AwAAABAEAABtrLdCQ6oOvI49pL1/wum9YQBkvUqHwrvKL4S8WacnPdpWJD1CV4q99enkPHSBvL3k\n" +
			 "WAa83gCAPLQGh70DwXS9PPu6PYyw2jwP44K8B3gnvRD6FrzM0yk9EupoPRPGW70TfLW9agTNuzAa\n" +
			 "Hr1Lvo+9Z5gPPmGbsLsjs809g0sjPChJvDxnafC8bizNvfv8AL56zIq9LEUoPWEKlbzrR8k8FG4K\n" +
			 "vaIJWj1PyYE9MtmKujUctT1YpcC8q73DvHA6CT5mrIU9zthcvV9+Kz6juM28OkO+vJ8uE721G9Q7\n" +
			 "L1asvXE7UztO9Bs9SEiDO4eHRbzv7YU9610wO9vDJj3o5Ym9xXOiPTZSOLyz9SY7yailvTM/wTz+\n" +
			 "gBq9YtjWO2Tizr3HcUg90dbOPE7no73lUZW8f5SNvfJLirph76w9FBR8vEe0Dr2YEZi9jXSkPfKL\n" +
			 "nTyMprk9JVUGvScFFT5wgKo9zsOgPc/hBT1s6yg9euSuPY9SpDwn4y09r16ivKU8dT3z/8U7BhpF\n" +
			 "PbnSqTwbMAS7VK33vIyW8bzWHos9R3levVz8yr1IGT29To4LPuZSjL2qcDk9flTvPLggoz3pKW49\n" +
			 "gRArveT1cL1iN9K8NcF2uq7gij2cgEe8TjB0vcpOrj1CCl09gPowvANiWLx5p/W84Ie8vQPZMb7J\n" +
			 "idS9dOxmvKjuvryz2xS8VYi9vcCYk73Plbq9aMTDvMQp3j1c/469862NPV5VZr0E63m9uMEpveLR\n" +
			 "TD36BKC9oR70PaZNZjsIrQY+O4dKvX8KIj0UHpE9KtQLvdAQ3bxjD1i9MstWvct4dj2AmBG9Aceb\n" +
			 "vfXcVD3tciK+ogTwvCrRRb3rlLA8N3hJvUp0zjxiCwq+pw3bvVqXtDuUhS+97P4MPZ3nXr3D6ZE9\n" +
			 "xpnevVy9JjxujrW8EVmmPGHGM72Vthi9aAv4vbjunL1YmJW9CiXHvfPkLD0/Qkm97KuruyWfnjw/\n" +
			 "esS8DXp4Pdk5Ej3DnjQ9+NysvTqYKTxwVzw81fDTva07LjxvTCa9iyFKveu85j21S9U8jnERPfyy\n" +
			 "V701OFC9Vg4DPeBrlr1kuVw9nzktvZP0sD3dGW+7lPbMu/Z3DL7xV9y9QVK/PWoKPrwly7S9FGCO\n" +
			 "vHhwRLwDEym9PZwEvfot4L0gSYo8uG1xvcfiEj2BAbY8EEy7PAao3r1NK4W91sQdvSlbVD1Kjsg9\n" +
			 "H+tzPU2TSj3tv7Q9dq2oPZFSD72Uykm9h0NWPcmZDL3TZHo8r/IQvLwgcz0x4F0961FAPf8/Nr3N\n" +
			 "qiO9hxByvTHPEzx/Xjq9jeWKvaiYi73zHLm9rv5aPYRJyLvh/I29jHldvIACX7vejvQ9aLcbvU+f\n" +
			 "5Dwuy4w8vSimOwIAAAAAAAAAAgAAAAAAAAABAAAAAAAAAABgAAAAYAAAAAAkAACrqr/c4eDf397d\n" +
			 "2tjUua6ngoaMdFNIREFAPTs6OTg2NTU0NDQ1Nzk6PD5BQ0VHR0dGRkZGSk1NTEtMVFxpbXBzbnOD\n" +
			 "lqCgo6u1vMHN2eLl5OTf1s+0gmBmeo+co6SlpqaqqqzD3d/f397c2dfRyKmaaIqCbFBGQ0A/PDo5\n" +
			 "ODc1MzMzMzM0NTc5PD9BQ0VGR0dGR0ZHSEtMTExMUllla2xvanB/k5ybnqq2vsPR2ePm4eHd2M65\n" +
			 "kmZofomVnqOjpaaoqamtx93f3t3b2dbRybR5XI5+Yk1HQj8+Ozo5ODU0NDMyMjI0NTY4Oz5BQ0RF\n" +
			 "R0dER0dISUtOT09PVFlfZmxsa3SClJiXnam5wcXM09rh4OPZz9S9mHJmeYCKmp+foqWmqKmqr8vd\n" +
			 "3dza2dbRw7FjXItwVEpGQT48Ojg4NzQ0NDQzMjI0NTc5Oz5AQ0ZGR0dHSUhJTE1PUVNSVVxhZGhp\n" +
			 "cHuIlZeYn6m6w8fK2Nzk5erk3NnFmXlpc39/i5WYoKSnp6emqa/N3Nva2NXOsYZSYZFwT0hFQT07\n" +
			 "Ojk4NzY2NjY2NDQ1Njc6PD9CREZGSElJS0tMTk5QU1VVVV5kaGlpdIWQlJeZoa25wsnN2eDk6+rk\n" +
			 "29HBo4lscoSIhIyOoKOnp6amqKiw0NnX2dTNqm1IaYNpUEdDQT07Ojo6OTk5OTk5OTg3ODk6PkFD\n" +
			 "RERFSEpLTE1OTk9SU1VWVl9na21vdomOlZiaoLC9wsrR1t7l6urn4tbAo4t2cISDgoiGn6Wnp6am\n" +
			 "pqWlstLU1dHKoFFDc4NwWUZCQT48Ozw7Ozs7Ozs8Ozs5OTo7P0JERUdISk1KTU5PUFBSU1VVWWNo\n" +
			 "bnJzeoiNmJueqrXBw8rS1tvk6ero4dzGnod3Z3N3gYyRnKGnp6ampaWkpLHMycK9g0ZFdH1pUEVC\n" +
			 "QT48PDw8PDw9Pj4/Pz08PD5AQkVGSElKTE1LTE5QUVJSUlRXXGhsdHV5fo6OmJ+hr7nAxcvQ19rk\n" +
			 "6uzr5dHBpI59bGt0gYmKmJmmpqampaSko6OvwLukZT9AcHRlUURBQT48PDw8PD0/QEFBQD49PkJD\n" +
			 "REZHSElLS0pJS01PUFFQUVZZXmRwfnt/gpGSmKGlrbW/xczQ197i5+3u4820oZB9a2RthY+LhYyl\n" +
			 "paWlpaSko6Oho6yiTT8+cnVjUERCPz48PDw8PT4/QEJCQUA+QEJEREZGR0lKSklGSExOT09OT1ZY\n" +
			 "YGl0goGBhYySlp+hqK7Ayc/R097j6+/v6NWym5N9em1vdYKMgpOkpaWlpaWko6OhnJmBTTs9bm9g\n" +
			 "VEZCPz8+PT09PT4/QEFDQ0FAQUNEREZGRkhKSUdGSExNT09OTlVYXGl1gISAfomKjZaapa69y9DU\n" +
			 "1N/j6u7w69C5m5V8dXloZX2Kj56Uo6SkpaWlop+clJJ0Qjk6am1gVUlDQUFAPz49PT09P0FDQ0JA\n" +
			 "QUNEQ0RERkdJR0RDRUpMTU1OTlNYWmdxcnt7en2Cg4iRlqKyxNLU1N3g6u7z6tG7oY9+fW1fWWx5\n" +
			 "dpt+lKKkpKWlo56dmpBoSjQ1ZWxiWk5JQkFAPTw7PD09PkBCQkA+P0FDRERDQ0VHQkBAQ0dISUpM\n" +
			 "Tk9UWFtnbHR6enl7goOFipWfn7DN1Nvf6O/19uHBp4yAfHVfWl54gp58fZGho6Sko5+dl49yTTQz\n" +
			 "Y2liYVlLQz89Ojk4OTs6Oz0/Pz49P0FCRENCQkJGQj8/QUJERUVHS0xPU1diaWxzd3h8f4CBgouO\n" +
			 "ioqjxd3h5O/09u/NrIyFcXRlXGF3iZB+e3yQn6Kjop+bl5d9VTs0YGNla1pDPjw6Ojo5ODc2Nzk7\n" +
			 "PDw8Pj9AQUA/P0FCQDw7PD4+Pj9AQ0ZJS1FRU15mZGdwbW1sZ257fIWKoMTg5u7x9vbks42GdXFy\n" +
			 "ZF90f4h+fXl3iZyho6OcjZCNZjwyYGhmWUE0NjU0MzMzNDEwMTM1NTY3ODk6PDs7PDw7OjY1Njg4\n" +
			 "ODg5Ojk8P0JITlZXUU1PWFRVV15tdYuUm67N5O3x9PXyw5KEdXFzZF1mcnp/fHpzaoGXpKuomHR1\n" +
			 "aDwxV19LNi8uLSopKCgoKCUmKCosLS4wLzAyNTY2Nzg4NjMyMjEwLy8xMjI3Ojc9QUBBQTA2RUJB\n" +
			 "RlJfa3KElaSx0erw8vP00pmFem1pZVxjdHGBfHt2bm93q7i0s5BsXD4xVUI0KCQlJCMjIyIgHRsc\n" +
			 "HyMlJiYnKSgqLjAuMTQ1MzAsKysqKCcoKysrLC02NDIwMCssLzAyPUpddYCPnqOgtdno7vP136WG\n" +
			 "hG5pZmdrfHyCfnt5c3Nztse6uH6MbkkyRjcnIh8cGxoZGRgXFxcYGhseICEjIyIkKCkqLTEzMCwo\n" +
			 "JiYnJCMkJSQmJi0vLSkoJyUlKS84RVJdanV+kKKvttDm6fD146eBglhaUlZbcX6CgHx4cHByt87I\n" +
			 "vnexgUA7PjEmHx0ZGBUUFBQTFBUVFhcZGh0dGxseIiMmKi4uLis";
	public void showErrorAndClose(String error, int code) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(error + ": " + code)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			})
			.show();		
	}
	
	public void showMessage(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
				}
			})
			.setCancelable(false) // cancel with button only
			.show();		
	}
	
    private void resetTrackerParameters() {
	    int errpos[] = new int[1];
        FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "ContinuousVideoFeed=true;FacialFeatureJitterSuppression=0;RecognitionPrecision=1;Threshold=0.996;Threshold2=0.9995;ThresholdFeed=0.97;MemoryLimit=2000;HandleArbitraryRotations=false;DetermineFaceRotationAngle=false;InternalResizeWidth=70;FaceDetectionThreshold=3;", errpos);
        if (errpos[0] != 0) {
            showErrorAndClose("Error setting tracker parameters, position", errpos[0]);
        }
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sDensity = getResources().getDisplayMetrics().scaledDensity;

		prefs = getBaseContext().getSharedPreferences(
				"FaceDetection", MODE_PRIVATE);

		try {
			try {
				fromWeb = getIntent().getBooleanExtra("FromWeb", false);
				currState = getIntent().getStringExtra("CurrState");
				Log.d("MainActivity", "Curr State is " + currState);
			} catch (Exception tpexcep) {
				currState=null;
			}

			try {
				EnrolmentStatus = getIntent().getBooleanExtra("Enrolment", false);
				Log.d(TAG,"Face Recognition enrolment Status is "+EnrolmentStatus);

			} catch (Exception obj) {

			}
			Log.d(TAG,"Curr State of Camera is "+currState);
			if (currState != null &&  currState.equals("success")) {
				CameraStatus = false;

				//CameraStatus = false;
				GlobalVariables.personDescription="face matching";
				GlobalVariables.reqStatus= true;

				Log.d(TAG,"Time is more than 20 seconds - Not Right");
				Intent intent = new Intent(this,SuccessActivity.class);
				intent.putExtra("case","success");
				intent.putExtra("FromWeb",true);

				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */

				this.startActivity(intent);
			} else if (currState != null && currState.equals("failure")) {
				CameraStatus = false;
				GlobalVariables.personDescription="face not matching";
				GlobalVariables.reqStatus= true;

				Log.d(TAG,"Time is more than 20 seconds - Not Right");
				Intent intent = new Intent(this,SuccessActivity.class);
				intent.putExtra("case","failure");
				intent.putExtra("FromWeb",true);

				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","failure");
				mBundle.putBoolean("FromWeb",true);
*/
				this.startActivity(intent);


			} else {
				Log.d(TAG, "Checking Camera");
				Camera cp = null;
				//cp = Camera.open(0);
				//cp.release();
				Log.d(TAG, "Camera Status checking ");
				PackageManager pm = this.getPackageManager();
				if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
					CameraStatus = true;

				//lastSystemTime = System.currentTimeMillis();

				//lastSystemTime = 0;
				//cp.release();
				//Camera.release();
				//CameraStatus =true;
				Log.d(TAG, "Camera is AVailable");
			}
		} catch (Exception ojf) {
			CameraStatus = false;
			Log.d(TAG, "Camera Status                                                         if false" + ojf.getMessage());
		}
		//int res = FSDK.ActivateLibrary("Uw3aCK45gqXYjd7GNHf60l7D3oDd02rdxPqpfyf4gqVryuSwAvrct7VmifKjr7gEWuJP76R+dcuDUVInAvXsd7kwOQwoLA9eD/qvUyVZaeFGjpd0quslvHdLLKr719vn/1EoqGj3aPn6vsDZIPbqv6WicPxS6XniNvR20x7oEAw=");

		try {
			if (CameraStatus) {
				int res = FSDK.ActivateLibrary(("EMvnS0fIF7exIRspEoQuM7Io1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISljUOqA="));
			//	int res = FSDK.ActivateLibrary("OfuGFjnj3oJ5xekQC0FSoMmbaGlC6qtsC9YccC65G0aq8Q6OpJJUG07Y47zkTW0ILsLEiuEw5W4GylYgeqkUCcci1199GRvvqEvKS9hinpbVgoXiWZ5EmgCUThta0Ja6Pbk0IvSF3DGfardNSw8GX+F9UVObiIZUdmUCDY8crTs=");
				//int res = FSDK.ActivateLibrary("EMvnS0flF7exlRspEoQuM7lo1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISIjUOqA=");
				///int res = FSDK.ActivateLibrary("EMvnS0flF7exlRspEoQuM7lo1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POlhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHlSljUOqA=");
				//int res = FSDK.ActivateLibrary("EMvnS0fIF7exIRspEoQuM7Io1U6anar/WAvU4AeNQksNWnmWhvfPGmFehj7UFerc5AkBjT1AztF6+GdX42YET+POIhxvH2B3Jz4ci2zL101BCM/e9WwhaHYrNkMwVESEFmGMLH89ePVwYAzhkOOnnP16vZi5oenU+aHISIjUOqA=");
				prefs = getBaseContext().getSharedPreferences(
						"FaceDetection", MODE_PRIVATE);
				if (res != FSDKE_OK) {
					mIsFailed = true;
					showErrorAndClose("FaceSDK activation failed", res);
				} else {


					if (CameraStatus) {
						FSDK.Initialize();

						// Hide the window title (it is done in manifest too)
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
						requestWindowFeature(Window.FEATURE_NO_TITLE);

						// Lock orientation
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

						// Camera layer and drawing layer
						mDraw = new ProcessImageAndDrawResults_Enroll(this);
						mPreview = new Preview_Enroll(this, mDraw);
						mDraw.mTracker = new HTracker();

						String templatePath = this.getApplicationInfo().dataDir + "/" + database;
						if (FSDKE_OK != FSDK.LoadTrackerMemoryFromFile(mDraw.mTracker, templatePath)) {
							res = FSDK.CreateTracker(mDraw.mTracker);
							if (FSDKE_OK != res) {
								showErrorAndClose("Error creating tracker", res);
							}
						}

						//Chand added for Clear Tracker
                      //  FSDK.ClearTracker(mDraw.mTracker);



						resetTrackerParameters();

					/*	//Test Code by Chand
						byte[] mTmplCapture1 = Base64.decode(testbyte2, Base64.DEFAULT);

						FSDK.LoadTrackerMemoryFromBuffer(mDraw.mTracker, mTmplCapture1);
*/
						//Remove it
						this.getWindow().setBackgroundDrawable(new ColorDrawable()); //black background

						setContentView(mPreview); //creates MainActivity contents
						addContentView(mDraw, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));


						// Menu
						if (EnrolmentStatus) {
						LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						View buttons = inflater.inflate(R.layout.bottom_menu, null);
						//buttons.findViewById(R.id.helpButton).setOnClickListener(this);
						//buttons.findViewById(R.id.clearButton).setOnClickListener(this);

						//Chand added

							//buttons.setEnabled(false);

						addContentView(buttons, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
						}
					} else
						Log.d(TAG, "No Camera ");
				}
			}
		} catch (Exception objd) {

		}
	}

	@Override
	public void onClick(View view) {
		/*if (view.getId() == R.id.helpButton) {
			showMessage(help_text);
		} else if (view.getId() == R.id.clearButton) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Are you sure to clear the memory?" )
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
                    public void onClick(DialogInterface dialogInterface, int j) {
						pauseProcessingFrames();
						FSDK.ClearTracker(mDraw.mTracker);
						resetTrackerParameters();
						resumeProcessingFrames();
					}
				})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
                    public void onClick(DialogInterface dialogInterface, int j) {
					}
				})
				.setCancelable(false) // cancel with button only
				.show();
		}*/
	}
	
	@Override
	public void onPause() {
		super.onPause();
		if (CameraStatus) {
			pauseProcessingFrames();
			String templatePath = this.getApplicationInfo().dataDir + "/" + database;
			FSDK.SaveTrackerMemoryToFile(mDraw.mTracker, templatePath);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (mIsFailed)
            return;
		if (CameraStatus)
        	resumeProcessingFrames();
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		if (fromWeb) {
			Log.d("FaceDetction","GoBack to Home Activity");
			Intent intent= new Intent(this, Main2Activity.class);
			//Intent intent= new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

			this.startActivity(intent);
			finish();
		}
		super.onBackPressed();
	}

	public boolean SendingMessage(String status, String requestType) {
		Log.d(TAG,"SendMessage "+status);
		//String url="http://192.168.43.24:8080/?username="+status;


		String remoteIP = getRemoteIP();
		if (remoteIP == null || remoteIP == "") {
			Log.d(TAG," No IP ");
			return false;
		}
		if (!isEthernetConnected()) {
			if (!isConnectedInWifi()) {
				Log.d(TAG,"Network not connected");
				return false;
			}
		}


		Log.d(TAG,"Getting IP Address");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
		String url="http://"+remoteIP+":8080/?username="+status;

		Log.d(TAG, "send url: "+url);
		new VolleyService(new IResult() {
			@Override
			public void notifySuccess(String requestType, Object response) {

				Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
				/*if (requestType.equals("100")) {
					//Intent i= new Intent(this, audio.MainActivity.class);

					Intent i = new Intent(MainActivity.this, audio.MainActivity.class);

					startActivity(i);
				} else if (requestType.equals("101")) {
					Intent i = new Intent(MainActivity.this, FingerScannActivity.class);
					startActivity(i);
				} else if (requestType.equals("102")) {
					Intent i = new Intent(MainActivity.this, FirstActivity.class);
					startActivity(i);
				} else if (requestType.equals("103")) {
					Intent i = new Intent(MainActivity.this, audio.MainActivity.class);
					startActivity(i);
				} else if (requestType.equals("104")) {
					Intent i = new Intent(MainActivity.this, HomeActivity.class);
					startActivity(i);
				}*/
			}


			@Override
			public void notifyError(String requestType, VolleyError error) {
				Log.d(TAG, "notifyError: "+error.getMessage());
				try {
					//String fingerprint = "testfingerprint";
					Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
					String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
					//           playAudioFile(2,filePath);
			/*		if (requestType.equals("100")) {
						//Intent i= new Intent(this, audio.MainActivity.class);

						Intent i = new Intent(MainActivity.this, audio.MainActivity.class);

						startActivity(i);
					} else if (requestType.equals("101")) {
						Intent i = new Intent(MainActivity.this, FingerScannActivity.class);
						startActivity(i);
					} else if (requestType.equals("102")) {
						Intent i = new Intent(MainActivity.this, FirstActivity.class);
						startActivity(i);
					} else if (requestType.equals("103")) {
						Intent i = new Intent(MainActivity.this, audio.MainActivity.class);
						startActivity(i);
					} else if (requestType.equals("104")) {
						Intent i = new Intent(MainActivity.this, HomeActivity.class);
						startActivity(i);
					}*/

				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}, this).tokenBase(GET, url, null, requestType);

		return true;
	}
	private Boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}

	public Boolean isWifiConnected(){
		if(isNetworkAvailable()){
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
		}
		return false;
	}

	public Boolean isEthernetConnected(){
		if(isNetworkAvailable()){
			ConnectivityManager cm
					= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
		}
		return false;
	}



	private String getRemoteIP() {
		String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
				"");

		Log.d("MainActivity", "Remote IP Addr is " + remoteIPAddr);

		return remoteIPAddr;
	}
	public boolean isConnectedInWifi() {

		try {
			WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
			NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
					&& wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
				return true;
			}
		} catch (Exception obj) {
			Log.d("MainActivity","Got exception "+obj.toString());
		}
		return false;
	}

	private void pauseProcessingFrames() {
		mDraw.mStopping = 1;
		
		// It is essential to limit wait time, because mStopped will not be set to 0, if no frames are feeded to mDraw
		for (int i=0; i<100; ++i) {
			if (mDraw.mStopped != 0) break; 
			try { Thread.sleep(10); }
			catch (Exception ex) {}
		}
	}
	
	private void resumeProcessingFrames() {
		mDraw.mStopped = 0;
		mDraw.mStopping = 0;
	}
}


class FaceRectangle_Enroll{
	public int x1, y1, x2, y2;
}

// Draw graphics on top of the video
class ProcessImageAndDrawResults_Enroll extends View {
	private static final String TAG = "ProcessImageAndDrawResu";
	public HTracker mTracker;
	
	final int MAX_FACES = 5;
	final FaceRectangle_Enroll[] mFacePositions = new FaceRectangle_Enroll[MAX_FACES];
	final long[] mIDs = new long[MAX_FACES];
	final Lock faceLock = new ReentrantLock();
	int mTouchedIndex;
	boolean popup = false;
	long mTouchedID;
	int mStopping;
	int mStopped;
	
	Context mContext;
	Paint mPaintGreen, mPaintBlue, mPaintBlueTransparent;
	byte[] mYUVData;
	byte[] mRGBData;
	int mImageWidth, mImageHeight;
	boolean first_frame_saved;
	boolean rotated;
	private int handlercount = 0;
	private long  lastSystemTime=0;
	//private FSDK.FSDK_IMAGEMODE mCameraImageMode = new FSDK.FSDK_IMAGEMODE();
	//lastSystemTime = System.currentTimeMillis();
	int GetFaceFrame(FSDK.FSDK_Features Features, FaceRectangle_Enroll fr)
	{
		if (Features == null || fr == null)
			return FSDK.FSDKE_INVALID_ARGUMENT;

	    float u1 = Features.features[0].x;
	    float v1 = Features.features[0].y;
	    float u2 = Features.features[1].x;
	    float v2 = Features.features[1].y;
	    float xc = (u1 + u2) / 2;
	    float yc = (v1 + v2) / 2;
	    int w = (int) Math.pow((u2 - u1) * (u2 - u1) + (v2 - v1) * (v2 - v1), 0.5);
	    
	    fr.x1 = (int)(xc - w * 1.6 * 0.9);
	    fr.y1 = (int)(yc - w * 1.1 * 0.9);
	    fr.x2 = (int)(xc + w * 1.6 * 0.9);
	    fr.y2 = (int)(yc + w * 2.1 * 0.9);
	    if (fr.x2 - fr.x1 > fr.y2 - fr.y1) {
	        fr.x2 = fr.x1 + fr.y2 - fr.y1;
	    } else {
	        fr.y2 = fr.y1 + fr.x2 - fr.x1;
	    }

		return 0;
	}
	
	
	public ProcessImageAndDrawResults_Enroll(Context context) {
		super(context);
		
		mTouchedIndex = -1;
		
		mStopping = 0;
		mStopped = 0;
		rotated = false;
		mContext = context;
		mPaintGreen = new Paint();
		mPaintGreen.setStyle(Paint.Style.FILL);
		mPaintGreen.setColor(Color.GREEN);
		mPaintGreen.setTextSize(18 * MainActivity_Enroll.sDensity);
		mPaintGreen.setTextAlign(Align.CENTER);
		mPaintBlue = new Paint();
		mPaintBlue.setStyle(Paint.Style.FILL);
		mPaintBlue.setColor(Color.BLUE);
		mPaintBlue.setTextSize(18 * MainActivity_Enroll.sDensity);
		mPaintBlue.setTextAlign(Align.CENTER);
		
		mPaintBlueTransparent = new Paint();
		mPaintBlueTransparent.setStyle(Paint.Style.STROKE);
		mPaintBlueTransparent.setStrokeWidth(2);
		mPaintBlueTransparent.setColor(Color.BLUE);
		mPaintBlueTransparent.setTextSize(25);
		
		//mBitmap = null;
		mYUVData = null;
		mRGBData = null;
		
		first_frame_saved = false;
    }

	@Override
	protected void onDraw(Canvas canvas) {
		if (mStopping == 1) {
			mStopped = 1;
			super.onDraw(canvas);
			//onFinishInflate();
			return;
		}
		
		if (mYUVData == null || mTouchedIndex != -1) {
			super.onDraw(canvas);
			return; //nothing to process or name is being entered now
		}
		
		int canvasWidth = canvas.getWidth();
		//int canvasHeight = canvas.getHeight();

		if (lastSystemTime ==0) {
		//	lastSystemTime++;
			lastSystemTime= System.currentTimeMillis();
		} else {
			final long diff = (System.currentTimeMillis() - lastSystemTime) / 1000;

			Log.d(TAG,"Curr Diff is "+diff);

		/*	if ((diff > 30) && MainActivity.EnrolmentStatus) {
				handlercount = 0;
				mStopping = 1;
			//	MainActivity.onBackPressed();
			} else*/
			if ((diff > 8) && (!MainActivity_Enroll.EnrolmentStatus)) {
			//if ((diff > 15)) {


				GlobalVariables.personDescription="face not matching";
				GlobalVariables.reqStatus= true;


				Log.d(TAG,"Time is more than 20 seconds - Not Right");
				Intent intent = new Intent(getContext(),SuccessActivity.class);
				intent.putExtra("case","failure");
				intent.putExtra("FromWeb",false);
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","failure");
				mBundle.putBoolean("FromWeb",false);*/
				getContext().startActivity(intent);
				handlercount = 0;
				mStopping = 1;
			}
		}


		// Convert from YUV to RGB
		decodeYUV420SP(mRGBData, mYUVData, mImageWidth, mImageHeight);

		//30 frames
		//if (lastSystemTime > 30*20) {
		//	Log.d(TAG,"Last System Time is "+lastSystemTime);
		//}
		// Load image to FaceSDK
		FSDK.HImage Image = new FSDK.HImage();
		FSDK.FSDK_IMAGEMODE imagemode = new FSDK.FSDK_IMAGEMODE();
		imagemode.mode = FSDK.FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_24BIT;
		FSDK.LoadImageFromBuffer(Image, mRGBData, mImageWidth, mImageHeight, mImageWidth*3, imagemode);
		FSDK.MirrorImage(Image, false);
		FSDK.HImage RotatedImage = new FSDK.HImage();
		FSDK.CreateEmptyImage(RotatedImage);
		
		//it is necessary to work with local variables (onDraw called not the time when mImageWidth,... being reassigned, so swapping mImageWidth and mImageHeight may be not safe)
		int ImageWidth = mImageWidth;
		//int ImageHeight = mImageHeight;
		if (rotated) {
			ImageWidth = mImageHeight;
			//ImageHeight = mImageWidth;
			FSDK.RotateImage90(Image, -1, RotatedImage);
		} else {
			FSDK.CopyImage(Image, RotatedImage);
		}
		FSDK.FreeImage(Image);

		// Save first frame to gallery to debug (e.g. rotation angle)
		/*
		if (!first_frame_saved) {				
			first_frame_saved = true;
			String galleryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
			FSDK.SaveImageToFile(RotatedImage, galleryPath + "/first_frame.jpg"); //frame is rotated!
		}
		*/
		
		long IDs[] = new long[MAX_FACES];
		long face_count[] = new long[1];
		
		FSDK.FeedFrame(mTracker, 0, RotatedImage, face_count, IDs);
		FSDK.FreeImage(RotatedImage);
								
		faceLock.lock();
			
		for (int i=0; i<MAX_FACES; ++i) {
			mFacePositions[i] = new FaceRectangle_Enroll();
			mFacePositions[i].x1 = 0;
			mFacePositions[i].y1 = 0;
			mFacePositions[i].x2 = 0;
			mFacePositions[i].y2 = 0;
			mIDs[i] = IDs[i];
		}
		
		float ratio = (canvasWidth * 1.0f) / ImageWidth;



		for (int i = 0; i < (int)face_count[0]; ++i) {
			FSDK.FSDK_Features Eyes = new FSDK.FSDK_Features(); 
			FSDK.GetTrackerEyes(mTracker, 0, mIDs[i], Eyes);
		
			GetFaceFrame(Eyes, mFacePositions[i]);
			mFacePositions[i].x1 *= ratio;
			mFacePositions[i].y1 *= ratio;
			mFacePositions[i].x2 *= ratio;
			mFacePositions[i].y2 *= ratio;
		}
		
		faceLock.unlock();
		
		int shift = (int)(22 * MainActivity_Enroll.sDensity);

		// Mark and name faces
		for (int i=0; i<face_count[0]; ++i) {
			canvas.drawRect(mFacePositions[i].x1, mFacePositions[i].y1, mFacePositions[i].x2, mFacePositions[i].y2, mPaintBlueTransparent);
			
			boolean named = false;
			String names[] = new String[1];
			if (IDs[i] != -1) {

				FSDK.GetAllNames(mTracker, IDs[i], names, 1024);
				if (names[0] != null && names[0].length() > 0) {
					Log.d(TAG, "onDraw: "+names[0]);
					canvas.drawText(names[0], (mFacePositions[i].x1+mFacePositions[i].x2)/2, mFacePositions[i].y2+shift, mPaintBlue);
					named = true;
				}
			}
			if(named){
			//if (named && !MainActivity.EnrolmentStatus) {
				GlobalVariables.personDescription="face matching";
				Log.d(TAG,"Face Matching is success");
				GlobalVariables.reqStatus= true;
/*
                try {
                    byte testbyte[] = new byte[50000];
                    Log.d(TAG,"Going to SaveTrackerMemoryToBuffer");

				//	mCameraImageMode.mode = FSDK.FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_32BIT;
                    int tmpVal = FSDK.SaveTrackerMemoryToBuffer(mTracker, testbyte);

                    Log.d(TAG,"FSDK_OK Val "+FSDKE_OK);
                    Log.d(TAG,"SaveTrackerMemoryToBuffer retVal is"+tmpVal);
                    if (tmpVal == 0) {
                       // String str = new String(testbyte); // for UTF-8 encoding
						String str = 	Base64.encodeToString(testbyte, Base64.DEFAULT);
                        Log.d("Test", "Rcvd buffr is " + str);
						//byte[] mTmplCapture1 = Base64.decode(str, Base64.DEFAULT);
                       *//* byte b[] = str.getBytes();
                        //byte[] b = string.getBytes(Charset.forName("UTF-8"));
                        FSDK.LoadTrackerMemoryFromBuffer(mTracker, b);
                        Log.d("Test", "Load Track is complete");*//*
                    } else
                        Log.d(TAG,"Got the SaveTrackerMemoryToBuffer as null");
                }catch (Exception obj) {
                    Log.d("Test","Got Crashed "+obj.getMessage());
                }*/
				String descoveredID = names[0];
				Intent intent = new Intent(getContext(),SuccessActivity.class);

				intent.putExtra("FromWeb",false);

				if (MainActivity_Enroll.EnrolmentStatus)
					intent.putExtra("case","forenrollment");
				else
				intent.putExtra("case","success");

				intent.putExtra("personID",descoveredID);

				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",false);*/
				//intent.putExtra("case","success");
				getContext().startActivity(intent);
				mStopping = 1;

				//Chand added



			}else {
				//Chand added
				if (MainActivity_Enroll.EnrolmentStatus)
				{
					canvas.drawText("Tap to name", (mFacePositions[i].x1 + mFacePositions[i].x2) / 2, mFacePositions[i].y2 + shift, mPaintGreen);
				}
                String name = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("MYLABEL", "defaultStringIfNothingFound");
				if(names[0]!= null){
					if(!name.equals(names[0])){

						if(handlercount<1){
							handlercount++;
							if(popup){
								final Handler handler = new Handler();
								handler.postDelayed(new Runnable() {
									@Override
									public void run() {


										Log.d(TAG,"Face Matching is success going");
										GlobalVariables.personDescription="face matching";
										GlobalVariables.reqStatus= true;
										Intent intent = new Intent(getContext(),SuccessActivity.class);
										if (MainActivity_Enroll.EnrolmentStatus)
											intent.putExtra("case","forenrollment");
										else
										intent.putExtra("case","success");
										intent.putExtra("FromWeb",true);
										/*Bundle mBundle = new Bundle();
										mBundle.putString("case","failure");
										mBundle.putBoolean("FromWeb",false);*/
										intent.putExtra("personID",names[0]);
										getContext().startActivity(intent);
										handlercount = 0;
										mStopping = 1;
										//Chand added

									}
								}, 4000);

							}

						}

					}
				}


			}

		}
		
		super.onDraw(canvas);      
	} // end onDraw method

	
	@Override
	public boolean onTouchEvent(MotionEvent event) { //NOTE: the method can be implemented in Preview class


		if (!MainActivity_Enroll.EnrolmentStatus)
			return true;

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			int x = (int)event.getX();
			int y = (int)event.getY();
			
			faceLock.lock();
			FaceRectangle_Enroll rects[] = new FaceRectangle_Enroll[MAX_FACES];
			long IDs[] = new long[MAX_FACES];
			for (int i=0; i<MAX_FACES; ++i) {
				rects[i] = new FaceRectangle_Enroll();
				rects[i].x1 = mFacePositions[i].x1;
				rects[i].y1 = mFacePositions[i].y1;
				rects[i].x2 = mFacePositions[i].x2;
				rects[i].y2 = mFacePositions[i].y2;
				IDs[i] = mIDs[i];
			}
			faceLock.unlock();
			
			for (int i=0; i<MAX_FACES; ++i) {
				if (rects[i] != null && rects[i].x1 <= x && x <= rects[i].x2 && rects[i].y1 <= y && y <= rects[i].y2 + 30) {
					mTouchedID = IDs[i];
					popup = true;
					mTouchedIndex = i;
					
					// requesting name on tapping the face	
					final EditText input = new EditText(mContext);
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setMessage("Enter person's name" )
						.setView(input)
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							@Override
                            public void onClick(DialogInterface dialogInterface, int j) {
								FSDK.LockID(mTracker, mTouchedID);
								String userName = input.getText().toString();
								FSDK.SetName(mTracker, mTouchedID, userName);
								PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("MYLABEL", userName).apply();
								popup = false;
								if (userName.length() <= 0) FSDK.PurgeID(mTracker, mTouchedID);

								FSDK.UnlockID(mTracker, mTouchedID);

								mTouchedIndex = -1;

								try {
									//byte testbyte[] = new byte[50000];
									Log.d(TAG,"Going to SaveTrackerMemoryToBuffer");

									//	mCameraImageMode.mode = FSDK.FSDK_IMAGEMODE.FSDK_IMAGE_COLOR_32BIT;
									int tmpVal = FSDK.SaveTrackerMemoryToBuffer(mTracker, MainActivity_Enroll.testbyte);

									Log.d(TAG,"FSDK_OK Val "+FSDKE_OK);
									Log.d(TAG,"SaveTrackerMemoryToBuffer retVal is"+tmpVal);
									if (tmpVal == 0) {
										tmpVal = FSDK.ClearTracker(mTracker);
										//Log.d(TAG,"Clear Tracker "+tmpVal);
										// String str = new String(testbyte); // for UTF-8 encoding
										String str = 	Base64.encodeToString(MainActivity_Enroll.testbyte, Base64.DEFAULT);
										Log.d("Test", "Rcvd buffr is " + str);
										byte[] mTmplCapture1 = Base64.decode(str, Base64.DEFAULT);

                      				  FSDK.LoadTrackerMemoryFromBuffer(mTracker, mTmplCapture1);
                        				Log.d("Test", "Load Track is complete");
									} else
										Log.d(TAG,"Got the SaveTrackerMemoryToBuffer as null");
								}catch (Exception obj) {
									Log.d("Test","Got Crashed "+obj.getMessage());
								}

							}
						})
						.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							@Override
                            public void onClick(DialogInterface dialogInterface, int j) {
								mTouchedIndex = -1;
								popup = false;
							}
						})
						.setCancelable(false) // cancel with button only
						.show();
					
					break;
				}
			}
		}
		return true;
	}
	
	static public void decodeYUV420SP(byte[] rgb, byte[] yuv420sp, int width, int height) {
		final int frameSize = width * height;
		int yp = 0;
		for (int j = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++) {
				int y = (0xff & ((int) yuv420sp[yp])) - 16;
				if (y < 0) y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}	
				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				int b = (y1192 + 2066 * u);
				if (r < 0) r = 0; else if (r > 262143) r = 262143;
				if (g < 0) g = 0; else if (g > 262143) g = 262143;
				if (b < 0) b = 0; else if (b > 262143) b = 262143;
				
				rgb[3*yp] = (byte) ((r >> 10) & 0xff);
				rgb[3*yp+1] = (byte) ((g >> 10) & 0xff);
				rgb[3*yp+2] = (byte) ((b >> 10) & 0xff);
				++yp;
			}
		}
	}  
} // end of ProcessImageAndDrawResults_Enroll class


// Show video from camera and pass frames to ProcessImageAndDraw class
class Preview_Enroll extends SurfaceView implements SurfaceHolder.Callback {
	Context mContext;
	SurfaceHolder mHolder;
	Camera mCamera;
	ProcessImageAndDrawResults_Enroll mDraw;
	boolean mFinished;

	Preview_Enroll(Context context, ProcessImageAndDrawResults_Enroll draw) {
		super(context);      
		mContext = context;
		mDraw = draw;
		
		//Install a SurfaceHolder.Callback so we get notified when the underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	//SurfaceView callback
	public void surfaceCreated(SurfaceHolder holder) {
		mFinished = false;
				
		// Find the ID of the camera
		int cameraId = 0;
		boolean frontCameraFound = false;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Log.d("ManAct","Number of Cameras "+Camera.getNumberOfCameras());
		for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
			Camera.getCameraInfo(i, cameraInfo);
			//if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				cameraId = i;
				frontCameraFound = true;
			}
		}
		Log.d("MainAct","Camera ID is "+cameraId);
		if (frontCameraFound) {
			mCamera = Camera.open(cameraId);
		} else {
			mCamera = Camera.open();
		}
		
		try {
			mCamera.setPreviewDisplay(holder);
			
			// Preview callback used whenever new viewfinder frame is available
			mCamera.setPreviewCallback(new PreviewCallback() {
				public void onPreviewFrame(byte[] data, Camera camera) {
					if ( (mDraw == null) || mFinished )
						return;
		
					if (mDraw.mYUVData == null) {
						// Initialize the draw-on-top companion
						Camera.Parameters params = camera.getParameters();
						mDraw.mImageWidth = params.getPreviewSize().width;
						mDraw.mImageHeight = params.getPreviewSize().height;
						mDraw.mRGBData = new byte[3 * mDraw.mImageWidth * mDraw.mImageHeight]; 
						mDraw.mYUVData = new byte[data.length];			
					}
	
					// Pass YUV data to draw-on-top companion
					System.arraycopy(data, 0, mDraw.mYUVData, 0, data.length);
					mDraw.invalidate();
				}
			});
		} 
		catch (Exception exception) {
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setMessage("Cannot open camera" )
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						android.os.Process.killProcess(android.os.Process.myPid());
					}
				})
				.show();
			if (mCamera != null) {
				mCamera.release();
				mCamera = null;
			}
		}
	}

	//SurfaceView callback
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.
		mFinished = true;
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}
	
	//SurfaceView callback, configuring camera
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		if (mCamera == null) return;
		
		// Now that the size is known, set up the camera parameters and begin
		// the preview.
		Camera.Parameters parameters = mCamera.getParameters();

		//Keep uncommented to work correctly on phones:
		//This is an undocumented although widely known feature
		/**/
		if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
			parameters.set("orientation", "portrait");
			mCamera.setDisplayOrientation(90); // For Android 2.2 and above
			mDraw.rotated = true;
		} else {
			parameters.set("orientation", "landscape");
			mCamera.setDisplayOrientation(0); // For Android 2.2 and above
		}
		/**/
		
        // choose preview size closer to 640x480 for optimal performance
        List<Size> supportedSizes = parameters.getSupportedPreviewSizes();
        int width = 0;
        int height = 0;
        for (Size s: supportedSizes) {
            if ((width - 640)*(width - 640) + (height - 480)*(height - 480) > 
                    (s.width - 640)*(s.width - 640) + (s.height - 480)*(s.height - 480)) {
                width = s.width;
                height = s.height;
            }
        }
				
		//try to set preferred parameters
		try {
		    if (width*height > 0) {
                parameters.setPreviewSize(width, height);
            }
            //parameters.setPreviewFrameRate(10);
			parameters.setSceneMode(Camera.Parameters.SCENE_MODE_PORTRAIT);
			parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
			mCamera.setParameters(parameters);
		} catch (Exception ex) {
		}
		mCamera.startPreview();
		
		parameters = mCamera.getParameters();
	    Camera.Size previewSize = parameters.getPreviewSize();
	    makeResizeForCameraAspect(1.0f / ((1.0f * previewSize.width) / previewSize.height));
	}
	
	private void makeResizeForCameraAspect(float cameraAspectRatio){
		LayoutParams layoutParams = this.getLayoutParams();
		int matchParentWidth = this.getWidth();           
		int newHeight = (int)(matchParentWidth/cameraAspectRatio);
		if (newHeight != layoutParams.height) {
			layoutParams.height = newHeight;
			layoutParams.width = matchParentWidth;    
			this.setLayoutParams(layoutParams);
			this.invalidate();
		}		
	}
} // end of Preview class
