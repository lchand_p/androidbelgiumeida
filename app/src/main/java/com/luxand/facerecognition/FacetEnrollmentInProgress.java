package com.luxand.facerecognition;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.captureandmatch.activites.SelectDoor;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;


public class FacetEnrollmentInProgress extends Activity implements View.OnClickListener, IResult {

    private static final String TAG = "FaceEnrollInprogress";
    private TextView msg;
    private LinearLayout centerImage;
    private ImageView imageview;
    private TextView hint;
    String userid;

    public static boolean check=true;
    public static boolean toastcheck=true;
    Button scanagain;
    User user;
    SharedPreferences prefs=null;
    private String EnrollStatus=null;
    private String ldapUser = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faceenrollmentinprogress);


        try {
            EnrollStatus = getIntent().getStringExtra("EnrolmentStatus");
            Log.d(TAG,"Enrollment Status "+EnrollStatus);
        } catch (Exception obj) {

        }


        try {
            userid = getIntent().getExtras().getString("userid");
         //   user = getIntent().getExtras().getParcelable("userData");
        } catch (Exception obj) {

        }
        try {
        user = getIntent().getExtras().getParcelable("userData");
    } catch (Exception obj) {
        user = null;
    }
        try {

            ldapUser = getIntent().getExtras().getString("ldapUser");
        } catch (Exception obj) {

        }
        Log.d(TAG,"User ID is "+userid+"  ldapUser "+ldapUser);

            View overlay = findViewById(R.id.mylayout);


        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);


        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }



        if (user != null) {
            Log.d(TAG, "User ID is " + user.get_id());


            JSONObject jsonObject = new JSONObject();


            try {




                Log.d(TAG,"Added User template timestamp ");
                jsonObject.put("uid", user.getUserId());
//need username  capture from ldap api(uid), this is needed for logging in
                //check_userid_ldap

                //	if (ldapUser != null)
                //		jsonObject.put("username", ldapUser);


                Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                long currts = (curCalendar.getTimeInMillis() / 1000L);
                jsonObject.put("user_template_timestamp", currts);
                Log.d(TAG,"Timestamp to be updated for timestamp "+currts);


                jsonObject.put("template_1", user.getTemplate1());
                jsonObject.put("template_2", user.getTemplate2());
             //   jsonObject.put("template_3", user.getTemplate3());
                jsonObject.put("template_3", user.getUserImage());


                //String facetem1 = Base64.encodeToString(test, Base64.DEFAULT);
  //              jsonObject.put("templateface", user.getTemplateFace());

                //user.setTemplateFace(facetem1);
//                MainActivity_Sentinel.appDatabase.userDao().update(user);

                Log.d(TAG,"Please Wait sending ");
//										MyProgressDialog.show(FingerPrintEnrollActivity.this, R.string.wait_message);
                String url = GlobalVariables.SSOIPAddress + Constants.UPDATER_USERFINGERPRINT;

                	new VolleyService(FacetEnrollmentInProgress.this, FacetEnrollmentInProgress.this).tokenBase(POST, url, jsonObject, "101");

                //

            } catch (Exception obj) {

            }
        }
       //
       // scanagain=(Button)findViewById(R.id.scan_again);
/*
        msg = (TextView) findViewById(R.id.msg);

        if (EnrollStatus == null) {
            if ( ldapUser != null)
                msg.setText("Employee #" + userid + " " + ldapUser);
            else
                msg.setText("Employee #" + userid + " " + user.getFullname());

            centerImage = (LinearLayout) findViewById(R.id.center_image);
            imageview = (ImageView) findViewById(R.id.imageview);
            hint = (TextView) findViewById(R.id.hint);
            findViewById(R.id.cancel).setOnClickListener(this);
            if ( ldapUser != null)
                hint.setText(Html.fromHtml("<b>"
                        + ldapUser + "," + "</b>" + "<br />" + "<small>" + "Finger print enrolment is in Progress"
                        + "</small>" + "<br />"));
                else
            hint.setText(Html.fromHtml("<b>"
                    + user.getFullname() + "," + "</b>" + "<br />" + "<small>" + "Finger print enrolment is in Progress"
                    + "</small>" + "<br />"));
        } else if (EnrollStatus.equals("success")) {
            hint = (TextView) findViewById(R.id.hint);
            //findViewById(R.id.cancel).setOnClickListener(this);
            msg.setText("Employee ");
            Log.d(TAG,"Finger Print Success");
            hint.setText(Html.fromHtml("<b>"
                      + "</b>" + "<br />" + "<small>" + "Finger print enrolment Success"
                    + "</small>" + "<br />"));
            Log.d(TAG,"Showing Finger Print Success");
        } else if (EnrollStatus.equals("failure")) {
            hint = (TextView) findViewById(R.id.hint);
            //findViewById(R.id.cancel).setOnClickListener(this);
            msg.setText("Employee ");
            Log.d(TAG,"Finger print enrolment failure Retry");
            hint.setText(Html.fromHtml("<b>"
                      + "</b>" + "<br />" + "<small>" + "Finger print enrolment failure Retry"
                    + "</small>" + "<br />"));
        }*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                //TODO implement
                Intent i = new Intent(this, FaceTabLaunchActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        check=true;
       /* if(check)
            new CaptureAndMatchTask().execute(0);*/
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    private boolean SendingMessageWeb(String status, String requestType) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;


        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }


        Log.d(TAG,"Getting IP Address "+status+"  ");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        //String url="http://"+remoteIP+":8080/?username="+status;
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+requestType;;
        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
                //finish();


            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";

                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

        return true;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.toString());
        }
        return false;
    }
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        Log.d(TAG, "notifySuccess: " + response.toString());

        // [{"deleted":false,"status":0,"tags":[],"_id":"5b4f2583ee0c1d1e1e884a29","subject":"ticket targus user","issue":"<p>ticket targus user<\/p>\n","group":"5b4df86b86596b6051cc3bc8","type":"5b4df86b86596b6051cc3bc7","priority":1,"date":"2018-07-18T11:33:23.439Z","comments":[],"notes":[],"attachments":[],"history":[{"action":"ticket:created","description":"Ticket was created.","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:33:23.445Z","_id":"5b4f2583ee0c1d1e1e884a2a"},{"action":"ticket:set:type","description":"Ticket type set to: Issue","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:34.762Z","_id":"5b4f2642ee0c1d1e1e884a2c"},{"action":"ticket:set:type","description":"Ticket type set to: Task","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:38.938Z","_id":"5b4f2646ee0c1d1e1e884a2d"},{"action":"ticket:set:assignee","description":"Assignee was cleared","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:48:17.863Z","_id":"5b4f2901ee0c1d1e1e884a2e"},{"action":"ticket:set:assignee","description":"roooop was set as assignee","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T12:00:46.705Z","_id":"5b4f2beeee0c1d1e1e884a30"}],"subscribers":["5b4df86b86596b6051cc3bc9","5b4f193a607f6f318db8f9b9"],"owner":"5b4df86b86596b6051cc3bc9","uid":1010,"__v":4,"assignee":"5b4f193a607f6f318db8f9b9"}]

        if (requestType.equals("101")) {
            Log.d(TAG,"Send 101 Main Activity ");
            MyProgressDialog.dismiss();

       // } else if (requestType.equals("133")) {
            Log.d(TAG,"Got response ");
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG,"133 Go to Main Activity");
                    Intent i = new Intent(FacetEnrollmentInProgress.this, LauncherActivity.class);
                    // Bundle bn = new Bundle();

                    // bn.putParcelable("User", user);
                    // i.putExtras(bn);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    //     if (homeTimer != null)
                    //       homeTimer.cancel();

                    //   homeTimer = null;
                //    finish();
                    finishAffinity();
                }
            },2000);
        }
    }




    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "notifyError: " + error);
       // MyProgressDialog.dismiss();


        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,"133 Go to Main Activity");
                Intent i = new Intent(FacetEnrollmentInProgress.this, LauncherActivity.class);
                // Bundle bn = new Bundle();

                // bn.putParcelable("User", user);
                // i.putExtras(bn);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                //     if (homeTimer != null)
                //       homeTimer.cancel();

                //   homeTimer = null;
               // finish();
                finishAffinity();
            }
        },2000);
    }
}
