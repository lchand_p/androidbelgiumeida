package com.luxand.facerecognition;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.JsonReader;
import android.util.Log;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Map;

import belgiumeida.CardDataVariables;
import fi.iki.elonen.NanoHTTPD;
import webserver.GlobalVariables;

public class WebICAImageCapture_Server extends NanoHTTPD
{
    Context appContext;

    String personName;
    private CardDataVariables cardData;

    public WebICAImageCapture_Server(int port, Context context)
    {
        super(port);
        appContext = context;
    }

    @Override
    public Response serve(IHTTPSession session)
    {
        String msg = "<html><body><h1>Hello server</h1>\n";
        Map<String, String> parms = session.getParms();

        String getrequest = null;

        getrequest = parms.get("getrequest");
        if (getrequest != null && getrequest.contains("photo")) {

            String rcvdBuf= getImageBuffer();
            if (rcvdBuf != null && (GlobalVariables.CardReadStatus == 0 || GlobalVariables.CardReadStatus == 2 || GlobalVariables.CardReadStatus ==3)) {
                Log.d("Web","Sending The Photo Buffer "+rcvdBuf);
                Log.d("Web","Sending The Buffer  len "+rcvdBuf);

                return newFixedLengthResponse(rcvdBuf);
            } else
                return newFixedLengthResponse("NoPhoto" + "</body></html>\n");
        } else
        if (getrequest != null && getrequest.contains("carddata")) {

            JSONObject jsonVal = CardDataJson();
            String rcvdBuf= jsonVal.toString();

            if (rcvdBuf != null && (GlobalVariables.CardReadStatus == 0 || GlobalVariables.CardReadStatus == 2 || GlobalVariables.CardReadStatus ==3)) {
                Log.d("Web","Sending the Card Data "+rcvdBuf);
                return newFixedLengthResponse(rcvdBuf);
            } else
                return newFixedLengthResponse("NoData" + "</body></html>\n");
        }
        else
            return newFixedLengthResponse("Not Supported" + "</body></html>\n");
        /*
        if (parms.get("username") == null)
        {
            msg += "<form action='?' method='get'>\n " +
                   "<p>Your name: <input type='text' name='username'></p>\n" + "</form>\n";
        } else
            {
                msg += "<p>Hello, " + parms.get("username") + "!</p>";
                user = parms.get("username");
                IDNumber=parms.get("IDNum");
                personName=parms.get("Name");
                expiryDate=parms.get("expiryDate");
                Nationality=parms.get("Nationality");



                ArabicName=parms.get("ArabicName");


                // Log.d("inside","inside server " +user);
              //  Intent intent = new Intent("com.example.detainee");
                Intent intent = new Intent("lumidigm.captureandmatch"); //lumidigm.captureandmatch");
                intent.putExtra("Data",user);
                intent.putExtra("IDNum",IDNumber);
                intent.putExtra("Name",personName);
                intent.putExtra("expiryDate",expiryDate);
                intent.putExtra("Nationality",Nationality);
                intent.putExtra("ArabicName",ArabicName);


                String IDNum=intent.getStringExtra("IDNum");
                //String ArabicName=intent.getStringExtra("ArabicName");
                String expiryDate=intent.getStringExtra("expiryDate");
                String Nationality=intent.getStringExtra("Nationality");
                String Gender=null;
                appContext.sendBroadcast(intent);
            }
        return newFixedLengthResponse(msg + "</body></html>\n");*/
    }
    public String getImageBuffer() {

        String userName = null;
        String userName2 = null;

        if ( GlobalVariables.personName != null) {
            userName = GlobalVariables.personName + ".jpg";
            userName2 = GlobalVariables.personName + "new.jpg";
        }
        if (userName == null) {
            userName = GlobalVariables.UserID;
            userName2 = GlobalVariables.UserID;
        }

        Log.d("Server","User Name is "+userName+" "+GlobalVariables.UserID+"  "+GlobalVariables.personName);
        String filename = Environment.getExternalStorageDirectory()
                + File.separator + "displayImages" + File.separator + userName; //"1.jpg";

        String filename2 = Environment.getExternalStorageDirectory()
                + File.separator + "displayImages" + File.separator + userName2; //"1.jpg";

        Log.d("Server","Reading the File "+filename);
        File fp = new File(filename);
        File fp2 = new File(filename2);
        if ((GlobalVariables.CardReadStatus == 0 || GlobalVariables.CardReadStatus == 2 || GlobalVariables.CardReadStatus ==3)) {
            try {
                Log.d("Server", "File Exists so reading the file");
                if (fp.exists()) {
                    Bitmap bm = BitmapFactory.decodeFile(fp.getAbsolutePath());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();
                    Log.d("Server", "File buffer length is " + b.length);
                    // return b;
                    String strBuff = Base64.encodeToString(b, Base64.DEFAULT);
                    strBuff = strBuff.replaceAll(" ", "+");
                    return strBuff;
                } else
                if (fp2.exists()) {
                    Bitmap bm = BitmapFactory.decodeFile(fp2.getAbsolutePath());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();
                    Log.d("Server", "File buffer length is " + b.length);
                    // return b;
                    String strBuff = Base64.encodeToString(b, Base64.DEFAULT);
                    strBuff = strBuff.replaceAll(" ", "+");
                    return strBuff;
                }
            } catch (Exception obj) {
                Log.d("Server","Got Exception while parsing data ");

              }
//            return convertByteArrayToHexString(b);
        }

        return null;
    }

    private JSONObject CardDataJson() {
        try {

            Log.d("WebServer","Card Status is "+GlobalVariables.CardReadStatus);
         //   String jsonStr = "{\"name\": \"i30\", \"brand\": \"Hyundai\"}";


            String CardData = "{\"FileVersion\": \""+cardData.FileVersion+"\","+
                    "\"CardNumber\": \""+cardData.CardNumber+"\","+
                    "\"ChipNumber\": \""+cardData.ChipNumber+"\","+
                    "\"ValidityeBegin\": \""+cardData.ValidityeBegin+"\","+
                    "\"ValidityEnd\": \""+cardData.ValidityEnd+"\","+
                    "\"NationalNumber\": \""+cardData.NationalNumber+"\","+
                    "\"CardDeliveryMunispality\": \""+cardData.CardDeliveryMunspality+"\","+
                    "\"SurName\": \""+cardData.Name+"\","+
                    "\"FirstName\": \""+cardData.Name_2+"\","+
                    "\"Nationality\": \""+cardData.Nationality+"\","+
                    "\"BirthLoc\": \""+cardData.BirthLoc+"\","+
                    "\"BirthDate\": \""+cardData.BirthDate+"\","+
                    "\"Sex\": \""+cardData.Sex+"\","+
                    "\"NobleCondition\": \""+cardData.NobleCondition+"\","+
                    "\"DocumentType\": \""+cardData.DocumentType+"\","+
                    "\"SpecialStatus\": \""+cardData.SpecialStatus+"\","+
                    "\"HashPicture\": \""+cardData.HashPicture+"\","+
                    "\"SpecialOrganization\": \""+cardData.SpecialOrganization+"\","+
                    "\"MemberofFamilyType\": \""+cardData.MemberofFamilyType+"\","+
                    "\"DateandCountryofProtection\": \""+cardData.DateandCountryofProtection+"\""+
                    "}";


            CardData = CardData.replaceAll("\"null\"","null");
            // convert to json object
            JSONObject json = new JSONObject(CardData);

            Log.d("WebServer","JSON Value is "+json);

            return json;
        } catch (Exception obj) {
            Log.d("WebServer","GOt the Exception "+obj.getMessage());
        }
        return null;
    }
}
