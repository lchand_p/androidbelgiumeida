package frost;


import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.Main2Activity;

public class UserFaceActivity extends AppCompatActivity {

    private Button button;
    private Button button1;

    private TextView textView;
    SharedPreferences Face;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_face);


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        textView = (TextView)findViewById(R.id.textView16);

        button = (Button) findViewById(R.id.button11);
        button.setVisibility(View.INVISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EnrollTypeActivity();
            }
        });

        button1 = (Button) findViewById(R.id.button12);
        button1.setVisibility(View.INVISIBLE);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enrollActivity();
            }
        });

        autoVerify();
    }

    public void autoVerify(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                button.setVisibility(View.VISIBLE);
                button1.setVisibility(View.VISIBLE);
                textView.setText("Face Successfully Registered");


            }
        },2000);
    }

    public void enrollActivity(){
        //Intent intent = new Intent(this,EnrollActivity.class);
        Intent intent = new Intent(this, Main2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void EnrollTypeActivity(){
        Intent intent = new Intent(this,EnrollTypeActivity.class);
        Face=getSharedPreferences("face",MODE_PRIVATE);
        Face.edit().putBoolean("face", true).apply();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
