package frost.restrictedaccess;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import database.AppDatabase;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import webserver.WebUtils;

public class LicenceActivity extends AppCompatActivity {

    private ImageView imageView3,imageView4,imageView5;
    private int current_image,current_image4,current_image5;
    int[] images = {R.drawable.rectangleline,R.drawable.rectangle};
    lumidigm.captureandmatch.entity.User user=null;
    private Button button=null;
    private String personName = null;
    private String TAG="LicenseActivity";
    private TextView personNameTx = null;
    private AppDatabase appDatabase;
    private String personID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licence);


        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        try {
            user = getIntent().getExtras().getParcelable("User");
            // allTasks = getIntent().getExtras().getParcelable("task");

            if (user  != null)
                personName = user.getFullname().trim();
            Log.d(TAG,"Person Name is "+personName);

        } catch (Exception ob) {
            Log.d(TAG,"Got Exception while reading User ");
            user = null;
            personName = null;
        }

        appDatabase = AppDatabase.getAppDatabase(this);

        if (user == null) {
            try {
              personID = getIntent().getStringExtra("personID");

            Log.d(TAG,"Received PersonID is "+personID);


            if (user == null && personID !=null) {
                Log.d(TAG,"user is null - so fetching person"+personID);
                user = appDatabase.userDao().countUsersBasedonUserID(personID);

                if (user == null)
                    Log.d(TAG,"Failed in fetching user data ");

             }
           // if (personName == null)
             //   personName = getIntent().getStringExtra("personName").toString();
          //  Log.d("SuccessActivity", "Curr State is " + successcase+" from Web "+fromWeb+" ID is "+personID);
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception "+obj.getMessage());
        }

    }
        button = (Button)findViewById(R.id.button);
        personNameTx = (TextView) findViewById(R.id.personname);

        if (personName != null)
            personNameTx.setText(personName);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                siteToolActivity();
            }
        });

        imageView3 = (ImageView)findViewById(R.id.imageView3);
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickImage();
            }
        });


        imageView4 = (ImageView)findViewById(R.id.imageView4);
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickImage4();
            }
        });


        imageView5 = (ImageView)findViewById(R.id.imageView5);
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickImage5();
            }
        });


    }

    public void siteToolActivity(){
        Intent intent = new Intent(this,ValidatingLicenseActivity.class);

        if (user != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("User", user);

            intent.putExtras(bundle);
        }

        if (personID != null)
            intent.putExtra("personID",personID);
        else
            Log.d(TAG,"Not attached personID ");

        startActivity(intent);
    }

    public void onclickImage5(){

        current_image5 = current_image5 % images.length;
        imageView5.setImageResource(images[current_image5]);
        current_image5++;
    }

    public void onclickImage4(){

        current_image4 = current_image4 % images.length;
        imageView4.setImageResource(images[current_image4]);
        current_image4++;
    }

    public void onclickImage(){

        current_image = current_image % images.length;
        imageView3.setImageResource(images[current_image]);
        current_image++;
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d("LicenseActivity","Going to Enrol Main ");

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();

        super.onBackPressed();
    }

}
