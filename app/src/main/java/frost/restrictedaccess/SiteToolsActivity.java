package frost.restrictedaccess;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import webserver.WebUtils;

public class SiteToolsActivity extends AppCompatActivity {

    private ImageView imageView6,imageView7,imageView8,imageView9,imageView10,imageView11;
    private int current_image6,current_image7,current_image8,current_image9,current_image10,current_image11;
    int[] images6 = {R.drawable.specline,R.drawable.spec};
    int[] images7 = {R.drawable.capline,R.drawable.cap};
    int[] images8 = {R.drawable.shoeline,R.drawable.shoe};
    int[] images9 = {R.drawable.jacketline,R.drawable.jacket};
    int[] images10 = {R.drawable.gloveline,R.drawable.glove};
    int[] images11 = {R.drawable.googleline,R.drawable.google};

    private Button button2;
    lumidigm.captureandmatch.entity.User user=null;
    private String personName = null;
    private String TAG="SiteToolsActivity";
    private TextView personNameTx = null;
    private String personID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_tools);



        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        try {
            user = getIntent().getExtras().getParcelable("User");
            // allTasks = getIntent().getExtras().getParcelable("task");


            if (user  != null)
                personName = user.getFullname().trim();
            Log.d(TAG,"Person Name is "+personName);

        } catch (Exception ob) {
            Log.d(TAG,"Got Exception while reading User ");
            user = null;
            personName = null;
        }

        personID = getIntent().getStringExtra("personID");

        Log.d(TAG,"Received PersonID is "+personID);

        button2 = (Button)findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotocheckinactivity();
            }
        });

        personNameTx = (TextView) findViewById(R.id.personname);

        if (personName != null)
            personNameTx.setText(personName);

        imageView6 = (ImageView)findViewById(R.id.imageView6);
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spec();
            }
        });

        imageView7 = (ImageView)findViewById(R.id.imageView7);
        imageView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cap();
            }
        });

        imageView8 = (ImageView)findViewById(R.id.imageView8);
        imageView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoe();
            }
        });

        imageView9 = (ImageView)findViewById(R.id.imageView9);
        imageView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jacket();
            }
        });

        imageView10 = (ImageView)findViewById(R.id.imageView10);
        imageView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                glove();
            }
        });

        imageView11 = (ImageView)findViewById(R.id.imageView11);
        imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gogle();
            }
        });

    }

    public void gotocheckinactivity(){
        Intent intent = new Intent(this, PreferencesActivity.class);
        if (user != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("User", user);

            intent.putExtras(bundle);
        }

        if (personID != null)
            intent.putExtra("personID",personID);
        else
            Log.d(TAG,"Not attached personID ");

        startActivity(intent);
    }

    public void gogle(){
        current_image11 = current_image11 % images11.length;
        imageView11.setImageResource(images11[current_image11]);
        current_image11++;
    }

    public void glove(){
        current_image10 = current_image10 % images10.length;
        imageView10.setImageResource(images10[current_image10]);
        current_image10++;
    }

    public void jacket(){
        current_image9 = current_image9 % images9.length;
        imageView9.setImageResource(images9[current_image9]);
        current_image9++;
    }

    public void shoe(){
        current_image8 = current_image8 % images8.length;
        imageView8.setImageResource(images8[current_image8]);
        current_image8++;
    }

    public void cap(){
        current_image7 = current_image7 % images7.length;
        imageView7.setImageResource(images7[current_image7]);
        current_image7++;
    }

    public void spec(){
        current_image6 = current_image6 % images6.length;
        imageView6.setImageResource(images6[current_image6]);
        current_image6++;
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d("SiteTools","Going to Enrol Main ");

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();

        super.onBackPressed();
    }

}
