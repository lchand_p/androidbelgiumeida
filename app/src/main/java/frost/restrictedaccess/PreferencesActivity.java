package frost.restrictedaccess;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
//import pl.droidsonroids.gif.GifImageView;
import frost.timeandattendance.CheckInRestrictedAccess;
import webserver.WebUtils;

public class PreferencesActivity extends AppCompatActivity {

    private TextView textView;
   // private GifImageView gifImageView;
    private ImageView imageView;
    lumidigm.captureandmatch.entity.User user=null;

    private String TAG="PreferencesActivity";
    private String personName = null;
    private TextView personNameTx = null;
    private Button button=null;
    private String personID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference_select);



        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        try {
            user = getIntent().getExtras().getParcelable("User");
            // allTasks = getIntent().getExtras().getParcelable("task");


            if (user  != null)
                personName = user.getFullname().trim();
            Log.d(TAG,"Person Name is "+personName);

        } catch (Exception ob) {
            Log.d(TAG,"Got Exception while reading User ");
            user = null;
            personName = null;
        }

        personID = getIntent().getStringExtra("personID");

        Log.d(TAG,"Received PersonID is "+personID);

        button = (Button) findViewById(R.id.continue3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CheckInRestrictedAccess.class);
                if (user != null) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("User", user);

                    intent.putExtras(bundle);
                }


                if (personID != null)
                    intent.putExtra("personID",personID);
                else
                    Log.d(TAG,"Not attached personID ");

                if (personID != null)
                    intent.putExtra("personID",personID);
                else
                    Log.d(TAG,"Not attached personID ");

                startActivity(intent);
            }
        });
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d("ValidatingLicense","Going to Enrol Main ");

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();

        super.onBackPressed();
    }

}
