package frost.restrictedaccess;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

//import lumidigm.GifImageView;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
//import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import webserver.WebUtils;

public class ValidatingLicenseActivity extends AppCompatActivity {

    private TextView textView;
    private GifImageView gifImageView;
    private ImageView imageView;
    lumidigm.captureandmatch.entity.User user=null;

    private String TAG="ValidatingLicenseActivity";
    private String personName = null;
    private TextView personNameTx = null;
    private String personID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validating_license);



        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        try {
            user = getIntent().getExtras().getParcelable("User");
            // allTasks = getIntent().getExtras().getParcelable("task");


            if (user  != null)
                personName = user.getFullname().trim();
            Log.d(TAG,"Person Name is "+personName);

        } catch (Exception ob) {
            Log.d(TAG,"Got Exception while reading User ");
            user = null;
            personName = null;
        }

        personID = getIntent().getStringExtra("personID");

        Log.d(TAG,"Received PersonID is "+personID);

        imageView = (ImageView)findViewById(R.id.imageView13);
        imageView.setVisibility(View.INVISIBLE);

        textView = (TextView)findViewById(R.id.textView14);
        gifImageView = (GifImageView)findViewById(R.id.gif12);

        personNameTx = (TextView) findViewById(R.id.personname);

        if (personName != null)
            personNameTx.setText(personName);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                textView.setText("Successfully license verified.");
                gifImageView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.VISIBLE);

            }
        }, 3000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(ValidatingLicenseActivity.this,SiteToolsActivity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelable("User", user);

                intent.putExtras(bundle);

                if (personID != null)
                    intent.putExtra("personID",personID);
                else
                    Log.d(TAG,"Not attached personID ");

                startActivity(intent);

            }
        }, 6000);


    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d("ValidatingLicense","Going to Enrol Main ");

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();

        super.onBackPressed();
    }

}
