package frost;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.HomeEnrollActivity;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FingerPrintEnrollActivity;
import lumidigm.captureandmatch.activites.M21FingerPrintEnrollActivity;
import lumidigm.captureandmatch.activites.Main2Activity;

import static webserver.GlobalVariables.M21Support;

public class EnrollTypeActivity extends AppCompatActivity {

    private Button face;
    private Button finger;
    private Button voice;

    private TextView textView;

    private ImageView imageView21,imageView22,imageView23;

    SharedPreferences Finger,Voice,Face;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll_type);


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        textView = (TextView)findViewById(R.id.textView21);
        textView.setVisibility(View.INVISIBLE);

        imageView21 = (ImageView)findViewById(R.id.imageView21);
        imageView21.setVisibility(View.INVISIBLE);

        imageView22 = (ImageView)findViewById(R.id.imageView22);
        imageView22.setVisibility(View.INVISIBLE);

        imageView23 = (ImageView)findViewById(R.id.imageView23);
        imageView23.setVisibility(View.INVISIBLE);

        Finger=getSharedPreferences("finger",MODE_PRIVATE);
        Voice=getSharedPreferences("voice",MODE_PRIVATE);
        Face=getSharedPreferences("face",MODE_PRIVATE);



        face = (Button) findViewById(R.id.button6);
        face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserFaceActivity();
            }
        });

        finger = (Button) findViewById(R.id.button7);
        finger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserFingerActivity();
            }
        });

        voice = (Button) findViewById(R.id.button8);
        voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserVoiceActivity();
            }
        });



        if(Finger.getBoolean("finger", false)) {
            imageView22.setVisibility(View.VISIBLE);

        }
        if(Voice.getBoolean("voice", false)){

            imageView23.setVisibility(View.VISIBLE);


        }
        if(Face.getBoolean("face", false)){
            imageView21.setVisibility(View.VISIBLE);

        }

        if(Finger.getBoolean("finger", false) &&
                Voice.getBoolean("voice", false) &&
                Face.getBoolean("face", false)){

            ReturnToEnroll();
        }
    }

    public void ReturnToEnroll(){

        textView.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences.Editor face = getSharedPreferences("face",MODE_PRIVATE).edit();
                face.clear();
                face.commit();

                SharedPreferences.Editor finger = getSharedPreferences("finger",MODE_PRIVATE).edit();
                finger.clear();
                finger.commit();

                SharedPreferences.Editor voice = getSharedPreferences("voice",MODE_PRIVATE).edit();
                voice.clear();
                voice.commit();


//                Intent intent = new Intent(EnrollTypeActivity.this,EnrollActivity.class);
                Intent intent = new Intent(EnrollTypeActivity.this, Main2Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }, 4000);


    }

    public void UserFaceActivity(){
        Intent intent = new Intent(this,UserFaceActivity.class);
        startActivity(intent);
    }

    public void UserFingerActivity(){
        //Intent intent = new Intent(this,UserFingerActivity.class);
        Log.d("EnrollmentType","User Finger Activity");
        if (M21Support ==true) {
            Intent intent = new Intent(this, M21FingerPrintEnrollActivity.class);
            startActivity(intent);

        } else {
            Intent intent = new Intent(this, FingerPrintEnrollActivity.class);
            startActivity(intent);
        }
    }

    public void UserVoiceActivity(){
        Intent intent = new Intent(this, UserVoiceActivity.class);
        startActivity(intent);
    }
}
