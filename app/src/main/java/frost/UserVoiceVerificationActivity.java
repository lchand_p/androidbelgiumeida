package frost;


import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.Main2Activity;

public class UserVoiceVerificationActivity extends AppCompatActivity {

    private TextView textView20,textView19,textView17;
    private ImageView imageView17,imageView18;
    private Button button13,button14;

    SharedPreferences Voice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_voice_verification);


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        button13 = (Button)findViewById(R.id.button13);
        button13.setVisibility(View.INVISIBLE);
        button13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EnrollTypeActivity();
            }
        });

        button14 = (Button)findViewById(R.id.button14);
        button14.setVisibility(View.INVISIBLE);
        button14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enrollActivity();
            }
        });

        textView20 = (TextView)findViewById(R.id.textView20);
        textView17 = (TextView)findViewById(R.id.textView17);
        textView19 = (TextView)findViewById(R.id.textView19);
        imageView18 = (ImageView)findViewById(R.id.imageView18);

        imageView17 = (ImageView)findViewById(R.id.imageView17);
        imageView17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickImage();
            }
        });

    }

    public void enrollActivity(){
       // Intent intent = new Intent(this,EnrollActivity.class);

        Intent intent = new Intent(this, Main2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }

    public void EnrollTypeActivity(){
        Intent intent = new Intent(this,EnrollTypeActivityNew.class);

        Voice=getSharedPreferences("voice",MODE_PRIVATE);
        Voice.edit().putBoolean("voice", true).apply();

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }


    public void onClickImage(){

        textView20.setText("Your voice was successfully verified");
        textView19.setVisibility(View.INVISIBLE);
        textView17.setVisibility(View.INVISIBLE);
        imageView18.setVisibility(View.INVISIBLE);
        button13.setVisibility(View.VISIBLE);
        button14.setVisibility(View.VISIBLE);



    }
}
