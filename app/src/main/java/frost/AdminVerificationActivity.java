package frost;


import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toolbar;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.SupervisiorIdentity2Activity;

public class AdminVerificationActivity extends AppCompatActivity {

    private ImageView imageView;
    private ImageView imageView2;
    private ImageView imageView3;

//    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_verification);

//        ActionBar actionBar = getSupportActionBar();
//        getSupportActionBar().setElevation(0);
//        actionBar.setLogo(R.drawable.appbar);
//        actionBar.setDisplayUseLogoEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);
//        actionBar.setDisplayShowTitleEnabled(false);
//

//        toolbar = (Toolbar)findViewById(R.id.appbar);


        imageView = (ImageView) findViewById(R.id.imageView1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminFaceVerified();
            }
        });

        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminFingerVerified();
            }
        });

        imageView3 = (ImageView) findViewById(R.id.imageView6);
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminVoiceVerified();
            }
        });
    }

    public void adminVoiceVerified(){
        Intent intent = new Intent(this,VoiceVerifiedActivity.class);
        startActivity(intent);
    }

    public void adminFingerVerified(){
     //   Intent intent = new Intent(this,FIngerVerifiedActivity.class);
      //  startActivity(intent);
        Log.d("AdminVerification","Going for Supervision Identification ");
        Intent i = new Intent(this, SupervisiorIdentity2Activity.class);
        startActivity(i);
    }

    public void adminFaceVerified(){

        Intent intent = new Intent(this,FaceVerifiedActivity.class);
        startActivity(intent);
    }
}
