package frost;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.captureandmatch.R;
import webserver.GlobalVariables;
import webserver.WebUtils;

public class UserFingerActivity extends AppCompatActivity {

    private TextView textView9;
    private TextView textView11;

    private ImageView imageView;
    private int current_image;
    int[] images = {R.drawable.put_your_finger_page,R.drawable.sample_1,R.drawable.sample_2,R.drawable.successful_finger_verification};
    private String userid;
    private String TAG="UserFingerActivity";

    private BroadcastReceiver mUpdateReceiver;
    private IntentFilter mUpdateFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_finger);


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        textView9 = (TextView) findViewById(R.id.textView9);
        textView11 = (TextView) findViewById(R.id.textView11);

        imageView = (ImageView) findViewById(R.id.imageView10);

        RegisterInterReceiver();
     //   RegisterReceiver();
        try {

            //ldapUser = getIntent().getExtras().getString("ldapUser");
            userid = getIntent().getExtras().getString("userid");
          //  user = getIntent().getExtras().getParcelable("userData");
            Log.d("UserFingerActivity","Face Recognition "+userid);
            //Log.d(TAG,"Face Name is "+user.getFullname());
            Log.d("UserFingerActivity","Before SendingMessage");
            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerAddTemplate",userid,userid);

            Log.d("UserFingerActivity","After SendingMessage");
        } catch (Exception obj) {
            Log.d("UserFingerActivity","Got exception");
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  onImageClick();
            }
        });

    }

    public void onImageClick(){

        current_image++;
        current_image = current_image % images.length;
        imageView.setImageResource(images[current_image]);

        if(current_image == 3){
           textView9.setText("Fingerprint Successfully Registered");
           textView11.setText("");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent splashscreen = new Intent(UserFingerActivity.this,UserFingerVerificationActivity.class);

                    splashscreen.putExtra("userid", userid);
                    Log.d(TAG,"In User FingerActivity  "+userid);
                    startActivity(splashscreen);
                    finish();
                }
            },1000);


        }
    }
    private void RegisterInterReceiver() {

        //registerReceiver(this.FingerScanActivity,
        //      this.mUpdateReceiver);

        if (GlobalVariables.AppSupport == 1) {
            Log.d(TAG, "In RegisterItnerReceiver");
            this.mUpdateFilter = new IntentFilter("lumidigm.captureandmatch.mobile.fingerenroll");

            try {
                this.mUpdateReceiver = new BroadcastReceiver() {
                    public void onReceive(final Context context, final Intent intent) {
                        final String state = intent.getStringExtra("STATE");
                        final String  Status = intent.getStringExtra("EVENT");


                        Log.e("Main", "UserFingerTemplateResponse Received UI Event" + Status+" "+state);

                        {
                            if (Status != null && Status.equals("UserFingerTemplateResponse")) {
                                Log.d(TAG," It is UserFingerTemplateResponse");
                                try {
                                    if (state != null && state.equals("First_Success")) {

                                        Log.d(TAG," It is UserFingerTemplateResponse"+state);
                                        current_image++;
                                        current_image = current_image % images.length;
                                        imageView.setImageResource(images[current_image]);

                                    } else if (state != null && state.equals("Second_Success")) {

                                        Log.d(TAG," It is UserFingerTemplateResponse"+state);
                                        current_image++;
                                        current_image = current_image % images.length;
                                        imageView.setImageResource(images[current_image]);

                                    } else if (state != null && state.equals("Third_Success")) {
                                        Log.d(TAG," It is UserFingerTemplateResponse"+state);
                                        current_image++;
                                        current_image = current_image % images.length;
                                        imageView.setImageResource(images[current_image]);

                                        if(current_image == 3) {
                                            textView9.setText("Fingerprint Successfully Registered");
                                            textView11.setText("");
                                        }
                                    } else   if (state != null && state.equals("FingerPrint_Verify"))
                                    {
                                        Log.d(TAG," It is UserFingerTemplateResponse "+state );

                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent splashscreen = new Intent(UserFingerActivity.this,UserFingerVerificationActivity.class);
                                                splashscreen.putExtra("userid", userid);
                                                Log.d(TAG,"In User FingerActivity second "+userid);
                                                startActivity(splashscreen);
                                                finish();
                                            }
                                        },1000);
                                    }

                                    Log.d(TAG, "Got event for the  " + Status);
                                } catch (Exception obj) {

                                }

                            }
                        }
                    }
                };
            } catch (Exception obj) {
                Log.d(TAG, "Got Crashed WHile Intent FIlter " + obj.getMessage());
            }
        }
        //   getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
    }
    private void UnRegisterReceiver() {
        Log.d(TAG,"Got UnRegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG,"Getting unRegister");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.mUpdateReceiver);
                //this.mUpdateReceiver = null;
            } catch (Exception obj) {
                Log.d(TAG,"Got crashed while unregister "+obj.getMessage());
            }
        }
    }

    private void RegisterReceiver() {
        Log.d(TAG, "Got RegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG,"Register Receiver ");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mUpdateReceiver, mUpdateFilter);
            } catch (Exception obj) {
                Log.d(TAG,"RegisterReceiver crashed "+obj.getMessage());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Main2Activity.check=true;
        RegisterReceiver();
    }
    @Override
    protected void onPause() {
        super.onPause();
        UnRegisterReceiver();
        // Main2Activity.check=true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UnRegisterReceiver();
        //Main2Activity.check=false;
    }
}
