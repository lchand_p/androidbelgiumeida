package frost;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxand.facerecognition.MainActivity;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import lumidigm.captureandmatch.activites.SupervisiorIdentity2Activity;
import webserver.WebUtils;

public class AdminVerificationActivityMobile extends AppCompatActivity {

    private ImageView imageView;
    private ImageView imageView2;
    private ImageView imageView3;

//    private Toolbar toolbar;
    private String userId=null;
    private boolean fromSuperVisor = false;
    private TextView personname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_verification);

//        ActionBar actionBar = getSupportActionBar();
//        getSupportActionBar().setElevation(0);
//        actionBar.setLogo(R.drawable.appbar);
//        actionBar.setDisplayUseLogoEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);
//        actionBar.setDisplayShowTitleEnabled(false);
//

//        toolbar = (Toolbar)findViewById(R.id.appbar);

        try {
            userId = getIntent().getExtras().getString("userid");
            fromSuperVisor = getIntent().getExtras().getBoolean("fromsupervisor");

            Log.d("AdminVerifiAc","User ID is "+userId+" fromSupervisor"+fromSuperVisor);

        } catch (Exception obj)  {
            userId = null;
            fromSuperVisor = false;
        }

        imageView = (ImageView) findViewById(R.id.imageView1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminFaceVerified();
            }
        });

        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminFingerVerified();
            }
        });

        imageView3 = (ImageView) findViewById(R.id.imageView6);
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminVoiceVerified();
            }
        });
    }

    public void adminVoiceVerified(){
        Intent intent = new Intent(this,VoiceVerifiedActivity.class);

        //intent.putExtra("userid", userId);
        // if (ldapUser != null)
        //intent.putExtra("ldapUser", ldapuser);

        startActivity(intent);
    }

    public void adminFingerVerified(){
        Intent intent = new Intent(this,FIngerVerifiedActivity.class);


        //personname = findViewById(R.id.textView5);

      //  intent.putExtra("userid", userId);

        startActivity(intent);
      //  Log.d("AdminVerification","Going for Supervision Identification ");
      //  Intent i = new Intent(this, SupervisiorIdentity2Activity.class);
      //  startActivity(i);
    }




    public void adminFaceVerified(){

        //Intent intent = new Intent(this,FaceVerifiedActivity.class);
        //startActivity(intent);
        Log.d("AdminVerification","Starting AdminFaceVerified ");
        Intent intent= new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment",false);
        mBundle.putBoolean("adminVerification",true);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d("AdminVeri","Going to Enrol Main ");

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();

        super.onBackPressed();
    }

}
