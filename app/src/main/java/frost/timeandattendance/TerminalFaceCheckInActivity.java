package frost.timeandattendance;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
//import com.luxand.facerecognition.MainActivity;
import com.luxand.facerecognition.MainActivity_FaceandFinger;


import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import database.AppDatabase;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.network.HttpsTrustManager;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.POST;
import static lumidigm.constants.Constants.ARGUS_CONTROLLER_STATUS;
import static lumidigm.constants.Constants.CHECKINSTATUS;

public class TerminalFaceCheckInActivity extends Activity implements IResult {

    private TextView textView,textView9;

    private static final String TAG = "TerminalCheckInActivity";

    private ImageView doorImage;

    lumidigm.captureandmatch.entity.User user=null;
    //TextView textView;
    String encodedData;

    AppDatabase appDatabase;
    //AllTasks allTasks;
    LinearLayoutManager linearLayoutManager;

    //AlarmService alarmService;
    private TextView doorname;
    private TextView name;
    private TextView date;
    private TextView checkinText;
    private SharedPreferences prefs=null;
    public int i1 = 0;
    private CountDownTimer countDownTimer;

    TextView welcometext;
    TextView usernameDet=null;
    String personID= null;
    String fromSource= null;

   // private static boolean CheckInStatus=true;

    // private CircularProgressIndicator circularProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

 //       setContentView(R.layout.user_success);


        setContentView(R.layout.activity_check_in);


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        String date = dateFormat.format(Calendar.getInstance().getTime());

        textView = (TextView)findViewById(R.id.textView8);
        textView.setText(currentTime);

        textView9 = (TextView)findViewById(R.id.textView9);

        textView9.setText(date);

        Log.d(TAG,"Current Date is"+currentTime+" Data is "+date);
        doorname = (TextView) findViewById(R.id.doorname);

        appDatabase = AppDatabase.getAppDatabase(this);
        //  alarmService=new AlarmService();
//        View overlay = findViewById(R.id.mylayout);
        usernameDet= findViewById(R.id.username);

        doorImage= findViewById(R.id.doorImage);
        welcometext  = findViewById(R.id.welcome);
        checkinText = findViewById(R.id.checkin);
        Log.d(TAG,"If it is Checkout ");
        /*doorImage.setImageResource(R.drawable.check_out);
        checkinText.setText("Check Out at");
        welcometext.setText("Welcome");*/
        try {
            Log.d(TAG,"Before Play Beep ");
            //  playBeep();

            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        //      startService(i);
        Log.d(TAG,"In OPenDoor ");
        int minutes = 2;
        int milliseconds = minutes * 60 * 1000;
        try {
            //if (user != null)
            {
                user = getIntent().getExtras().getParcelable("User");
                Log.d(TAG,"User is "+user.getUsername());
              //  usernameDet.setText(user.getFullname());
                usernameDet.setText(user.getUsername());

                // allTasks = getIntent().getExtras().getParcelable("task");
            }
        } catch (Exception ob) {
            Log.d(TAG,"Got Exception while reading User ");
            user = null;
        }


        try {
            fromSource = getIntent().getStringExtra("source");
            Log.d(TAG,"From Source is "+fromSource);
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.getMessage());
        }
        try {
            if (user == null) {
                personID = getIntent().getStringExtra("personID");
                Log.d(TAG,"PersonID "+personID);
                if (user == null && personID != null) {
                    user = appDatabase.userDao().countUsersBasedonUserID(personID);
                    Log.d(TAG,"Got the User details "+user.getFullname());
                    usernameDet.setText(user.getFullname());

                    Log.d(TAG,"OpenDoor IP Address is "+user.getIp_addr()+"  Encoded data is "+user.getEncoded_data());

                    doorname.setText("Door #"+user.getDoor());


                    Log.d(TAG,"Check in Status is "+user.getCheckInOut());

                }
            } else
                Log.d(TAG,"Got the User "+user.get_id());
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while TerminalFaceCheckIn ");
        }
        //

        String response=null;


        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);

        //For Access Control and Supervision enrollment it is always show checkin ");



        try {
            Log.d(TAG,"User Check out val is "+user.getCheckInOut());
            int workflowID = prefs.getInt(GlobalVariables.localworkflowStatus, -1);

            Log.d(TAG,"Local Work Flow is "+workflowID);
              if (workflowID == 1 || workflowID ==2) {
           // if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal ==2 ) {
            response = "TimeAttendanceCheckIn";
            // CheckInStatus =true;
            user.setCheckInOut(1);

            Log.d(TAG,"Checkout as 1");
            appDatabase.userDao().update(user);
            doorImage.setImageResource(R.drawable.check_in1);
            checkinText.setText("Check In at");
            Log.d(TAG,"For Access Control and Supervision enrollment it is always show checkin ");
        } else
        if (user.getCheckInOut() == 1)
       // if (CheckInStatus)
        {
            response = "TimeAttendanceCheckOut";
          //  CheckInStatus =false;
            user.setCheckInOut(0);
            appDatabase.userDao().update(user);

            Log.d(TAG,"1 Now User check out val is "+user.getCheckInOut());
            doorImage.setImageResource(R.drawable.check_out);
            checkinText.setText("Check Out at");

            Log.d(TAG,"Checkint as zero userID is"+user.getUserId());
            UpdateCheckInStatus(user.getUserId(),0);
        }
        else {
            response = "TimeAttendanceCheckIn";
           // CheckInStatus =true;
            user.setCheckInOut(1);

            appDatabase.userDao().update(user);
            Log.d(TAG,"0 Now User check out val is "+user.getCheckInOut());
            doorImage.setImageResource(R.drawable.check_in1);
            checkinText.setText("Check In at");


            Log.d(TAG,"TimeAttendanceCheckIn set 1 to userID"+user.getUserId());

            UpdateCheckInStatus(user.getUserId(),1);
        }
        if (user != null && response != null) {

            String responseVal = response + "-" + user.getFullname() + "-" + date + " " + currentTime;

            Log.d(TAG, "response is " + response + " Response Val " + responseVal);

            //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), response, null, responseVal);

       /* Log.d(TAG,"In SelectDoor All Tasks val is "+allTasks.toString());
        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put("con_hash_key", allTasks.getTags().get(0).getHash_key().trim());
            jsonObject.put("con_aes_key", allTasks.getTags().get(0).getAes_key().trim());
            jsonObject.put("door", allTasks.getTags().get(0).getDoor());
            jsonObject.put("relay", allTasks.getTags().get(0).getRelay());
            Log.d(TAG, "onClick: "+jsonObject);
            Log.d(TAG, "onCreate: "+ARGUS_CONTROLLER);
        } catch (JSONException e) {
            e.printStackTrace();
        }
*/
        }
        if (ARGUS_CONTROLLER_STATUS.contains("serviceme"))
            HttpsTrustManager.allowAllSSL();


            //  name.setText(user.getFullname());
            // doorname.setText(user.getDoor());
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception "+obj.toString());
        }
        //Send the Log to the server
        //Like Reports

        Log.d(TAG,"Got SelectAPI Respone");
        try {
            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"Glow_Led",user.getUserId(),"Success");
            encodedData=user.getEncoded_data();
            String IPAddr = user.getIp_addr();
            Log.d(TAG,"Encoded Data is "+encodedData+" IP Address "+IPAddr);

            if (encodedData != null ) {
                MyProgressDialog.show( this, R.string.wait_message);
                String url = "http://" + IPAddr + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                Log.d(TAG, "onReceive: " + url);
          //      new VolleyService(this, TerminalFaceCheckInActivity.this).tokenBase(GET, url, null, "133");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* try {
            Log.d(TAG,"Sending fingersuccess username is "+user.getFullname());
            SendingMessageWithStateWeb("checkin","101",user.getFullname());
        } catch (Exception obj) {
            Log.d(TAG,"It is exception");
        }*/

        MyProgressDialog.dismiss();

        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,"133 Go to Main Activity");
                //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut","102","UserFingerComeOut");
                //SendingMessageWithState("home","191",null);
                //Intent intent = new Intent(SuccessActivity.this, FaceTabLaunchActivity.class);
                if (GlobalVariables.MobileDevice == true ) {
                    Log.d(TAG,"Sending Stop_Led ");
                    new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"Stop_Led",null,"Success");
                }
              //  Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);

                // Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //Intent intent = new Intent(getApplicationContext(), MainActivity_Sentinel.class);
                //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                //startActivity(intent);
               // if (fromSource != null && fromSource.equals("facefinger"))
                    if (GlobalVariables.FaceandFinger == true)
                         intent = new Intent(getApplicationContext(), FaceTabLaunchActivity.class);

                    if (GlobalVariables.selectedProduct == 2)
                        intent = new Intent(getApplicationContext(), MainActivity_FaceandFinger.class);

                //   intent = new Intent(getApplicationContext(), MainActivity_FaceandFinger.class);

                Log.d(TAG,"from Source is "+fromSource);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("Enrolment", false);
                mBundle.putBoolean("adminVerification", false);
                // intent.putExtra("userid", editText.getText().toString());
                intent.putExtras(mBundle);
                startActivity(intent);
                finishAffinity();                //     if (homeTimer != null)
                //       homeTimer.cancel();

                //   homeTimer = null;
                //finish();
                //finishAffinity();
            }
        },5000);//500, 5000 //currently 2000

        //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        //Date date1 = null;//You will get date object relative to server/client timezone wherever it is parsed

    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        Log.d(TAG, "notifySuccess: " + response.toString());

        // [{"deleted":false,"status":0,"tags":[],"_id":"5b4f2583ee0c1d1e1e884a29","subject":"ticket targus user","issue":"<p>ticket targus user<\/p>\n","group":"5b4df86b86596b6051cc3bc8","type":"5b4df86b86596b6051cc3bc7","priority":1,"date":"2018-07-18T11:33:23.439Z","comments":[],"notes":[],"attachments":[],"history":[{"action":"ticket:created","description":"Ticket was created.","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:33:23.445Z","_id":"5b4f2583ee0c1d1e1e884a2a"},{"action":"ticket:set:type","description":"Ticket type set to: Issue","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:34.762Z","_id":"5b4f2642ee0c1d1e1e884a2c"},{"action":"ticket:set:type","description":"Ticket type set to: Task","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:38.938Z","_id":"5b4f2646ee0c1d1e1e884a2d"},{"action":"ticket:set:assignee","description":"Assignee was cleared","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:48:17.863Z","_id":"5b4f2901ee0c1d1e1e884a2e"},{"action":"ticket:set:assignee","description":"roooop was set as assignee","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T12:00:46.705Z","_id":"5b4f2beeee0c1d1e1e884a30"}],"subscribers":["5b4df86b86596b6051cc3bc9","5b4f193a607f6f318db8f9b9"],"owner":"5b4df86b86596b6051cc3bc9","uid":1010,"__v":4,"assignee":"5b4f193a607f6f318db8f9b9"}]

        if (requestType.equals("101")) {
            Log.d(TAG,"Send 101 Main Activity ");
            MyProgressDialog.dismiss();

            //AlertPopUp alertPopUp = new AlertPopUp(this);
            //alertPopUp.show();

            //startAlert(encodedData,allTasks.getTags().get(0).getIp_addr());

            Intent i = new Intent(this, Main2Activity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
            this.startActivity(i);

        } else if(requestType.equals("102")){
/*

            Log.d(TAG,"Got SelectAPI Respone");
            try {
                encodedData=((JSONObject)response).getString("encoded_data");
                 // allTasks.getTags().get(0).getIp_addr()
                if (encodedData != null ) {
                    MyProgressDialog.show(getApplicationContext(), R.string.wait_message);
                    String url = "http://" + user.getIp_addr() + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;

                    //  String url = "http://" + allTasks.getTags().get(0).getIp_addr() + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                    // String url = "http://172.16.16.110" + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                    // String url = "http://192.168.0.98" + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                    Log.d(TAG, "onReceive: " + url);
                    new VolleyService(this, getApplicationContext()).tokenBase(GET, url, null, "133");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            MyProgressDialog.dismiss();
*/


        } else if (requestType.equals("133")) {
        } else if (requestType.equals("211")) {
            Log.d(TAG,"Got CHeckInStatus ");
        }
    }




    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "notifyError: " + error);
       // MyProgressDialog.dismiss();
    }



    public void startAlert(String data,String ip) {






        int i = 0; //Integer.parseInt(count.getText().toString());





    }

    @Override
    protected void onPause() {
        super.onPause();
        //countDownTimer.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //countDownTimer.cancel();
    }


     @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
    private void UpdateCheckInStatus(String user_id, int status ) {

        Log.d(TAG, "Update Task Status ");

        JSONObject jsonObject = new JSONObject();
        try {
            String currts = null;
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = GlobalVariables.SSOIPAddress + CHECKINSTATUS;
            Log.d(TAG, "Device LOG URL is " + URL);
            JSONObject jsonBody = new JSONObject();

            try {
                Log.d(TAG, "Get Time of Day");
                currts = Long.toString(System.currentTimeMillis() / 1000L);
                try {
                    Log.d(TAG, "Get Time of Day");
                    Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                    //Added 4 & half hours to show the Indian time on the Server - Testing - 25/07/2019
                    //Date currentTime2 = curCalendar.getInstance().getTime();
                    // currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                    currts = Long.toString(curCalendar.getTimeInMillis() / 1000L + 16200);

                    String curr1 = Long.toString(System.currentTimeMillis() / 1000L);
                    // jsonObject2.put("logged_in_time_secs", currts);
                    Log.d(TAG, "Current Time stamp is GMT " + currts);
                    Log.d(TAG, "Current Time stamp is " + curr1);


                    jsonBody.put("checkIn", currts);
                    jsonBody.put("userId", user_id);
                    jsonBody.put("CheckInOut", status);

                } catch (Exception obj) {

                }
                Log.d(TAG, "Current Time stamp is " + currts);


            } catch (Exception obj) {
                Log.d(TAG, "Got Exception during JSON Body");
            }

            final String requestBody = jsonBody.toString();
            Log.d(TAG, "Post " + requestBody);
            new VolleyService(TerminalFaceCheckInActivity.this, TerminalFaceCheckInActivity.this).tokenBase(POST, URL, jsonBody, "211");

        } catch (Exception obj) {

        }
    }
    public void playBeep() {
        MediaPlayer m = new MediaPlayer();
        try {
            if (m.isPlaying()) {
                m.stop();
                m.release();
                m = new MediaPlayer();
            }

            AssetFileDescriptor descriptor = getAssets().openFd( "shutter.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            m.prepare();
            m.setVolume(1f, 1f);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}