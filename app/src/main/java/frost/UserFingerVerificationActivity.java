package frost;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.captureandmatch.activites.UserNotAvailable;
import webserver.GlobalVariables;
import webserver.WebUtils;

public class UserFingerVerificationActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView;
    private  TextView textView1;

    private Button button;
    private Button button1;
    private String userId=null;
    private boolean fromSuperVisor = false;

    private String TAG="UserFingerVerificationActivity";

    private BroadcastReceiver mUpdateReceiver;
    private IntentFilter mUpdateFilter;
    SharedPreferences Finger;
    SharedPreferences Face;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_finger_verification);

        Log.d("UserFingerVerification","UserFingerVerificationActivity ");


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        try {
            userId = getIntent().getExtras().getString("userid");
            fromSuperVisor = getIntent().getExtras().getBoolean("fromsupervisor");

            Log.d("UserFigerVerification","User ID is "+userId+" fromSupervisor"+fromSuperVisor);

        } catch (Exception obj)  {
            userId = null;
            fromSuperVisor = false;
        }

        RegisterInterReceiver();

        button = (Button) findViewById(R.id.button9);
        button.setVisibility(View.INVISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EnrollTypeActivity();
            }
        });

        button1 = (Button) findViewById(R.id.button10);
        button1.setVisibility(View.INVISIBLE);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   enrollActivity();
                new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

                Log.d(TAG,"Sending Cancel ");
                //SendingMessageWithState("home","191",null);
                //Intent intent = new Intent(SuccessActivity.this, FaceTabLaunchActivity.class);
                Intent intent = new Intent(UserFingerVerificationActivity.this, FaceTabLaunchActivity.class);
                //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                //startActivity(intent);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("Enrolment", false);
                mBundle.putBoolean("adminVerification", false);
                // intent.putExtra("userid", editText.getText().toString());
                intent.putExtras(mBundle);
                startActivity(intent);
                finishAffinity();

            }
        });

        textView = (TextView) findViewById(R.id.textView12);
        textView1 = (TextView) findViewById(R.id.textView15);

        imageView = (ImageView) findViewById(R.id.imageView14);
        /*imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageClick();
            }
        });*/
    }

    public void enrollActivity(){
       // Intent intent = new Intent(this,EnrollActivity.class);

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.finish();
    }

    public void EnrollTypeActivity(){

        Finger=getSharedPreferences("finger",MODE_PRIVATE);
        Finger.edit().putBoolean("finger", true).apply();

        Face=getSharedPreferences("face",MODE_PRIVATE);


        //if(Face.getBoolean("face", true))
        {
          //  imageView22.setVisibility(View.VISIBLE);
           /* Log.d(TAG,"Face is already true so going to Enrol");
            enrollActivity();
        } else {*/

            Log.d(TAG,"Face is false so going to EnrolTypeActivity");
            Intent intent = new Intent(this, EnrollTypeActivityNew.class);
//        intent.putExtra("Keyword","Value");

//        EnrollTypeActivity enroll = new EnrollTypeActivity();
//        enroll.Tickmark("b");


            intent.putExtra("userid", userId);
            Log.d(TAG,"User FingerVerification Activity User ID is "+userId);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            this.finish();
        }
    }

    public void onImageClick(){

        imageView.setImageResource(R.drawable.successful_finger_verification);
        textView.setText("Your fingerprint was successfully verified");
        textView1.setText("");
        button.setVisibility(View.VISIBLE);
        button1.setVisibility(View.VISIBLE);

    }
    public void FailureonImageClick(){

        imageView.setImageResource(R.drawable.unauthorized);
        textView.setText("Your fingerprint verification failed");
        textView1.setText("");
        button.setVisibility(View.VISIBLE);
        button1.setVisibility(View.VISIBLE);

    }
    private void RegisterInterReceiver() {

        //registerReceiver(this.FingerScanActivity,
        //      this.mUpdateReceiver);

        if (GlobalVariables.AppSupport == 1) {
            Log.d(TAG, "In RegisterItnerReceiver");
            this.mUpdateFilter = new IntentFilter("lumidigm.captureandmatch.mobile.verifyenroll");

            try {
                this.mUpdateReceiver = new BroadcastReceiver() {
                    public void onReceive(final Context context, final Intent intent) {
                        final String state = intent.getStringExtra("STATE");
                        final String  Status = intent.getStringExtra("EVENT");

                        Log.e("Main", "UserFingerTemplateResponse Received UI Event" + Status+" "+state);
                        {
                            if (Status != null && Status.equals("UserFingerTemplateVerifyResponse")) {

                                Log.d(TAG," It is UserFingerTemplateVerifyResponse"+state );
                                try {
                                       if (state != null && state.equals("FingerPrint_Verify_Sucess"))
                                    {
                                        Log.d(TAG," It is UserFingerTemplateVerifyResponse "+state );
                                        onImageClick();/*
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent splashscreen = new Intent(UserFingerVerificationActivity.this,UserFingerVerificationActivity.class);
                                                startActivity(splashscreen);
                                                finish();
                                            }
                                        },4000);*/
                                    }  else
                                       if (state != null && state.equals("FingerPrint_Verify_Failure"))
                                       {
                                           FailureonImageClick();
                                           Log.d(TAG," It is UserFingerTemplateVerifyResponse "+state );
/*
                                           new Handler().postDelayed(new Runnable() {
                                               @Override
                                               public void run() {
                                                   Intent splashscreen = new Intent(UserFingerVerificationActivity.this, UserNotAvailable.class);
                                                   startActivity(splashscreen);
                                                   finish();
                                               }
                                           },4000);*/
                                       }

                                    Log.d(TAG, "Got event for the  " + Status);
                                } catch (Exception obj) {

                                }

                            }
                        }
                    }
                };
            } catch (Exception obj) {
                Log.d(TAG, "Got Crashed WHile Intent FIlter " + obj.getMessage());
            }
        }
        //   getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
    }
    private void UnRegisterReceiver() {
        Log.d(TAG,"Got UnRegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG,"Getting unRegister");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.mUpdateReceiver);
                //this.mUpdateReceiver = null;
            } catch (Exception obj) {
                Log.d(TAG,"Got crashed while unregister "+obj.getMessage());
            }
        }
    }

    private void RegisterReceiver() {
        Log.d(TAG, "Got RegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG,"Register Receiver ");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mUpdateReceiver, mUpdateFilter);
            } catch (Exception obj) {
                Log.d(TAG,"RegisterReceiver crashed "+obj.getMessage());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Main2Activity.check=true;
        RegisterReceiver();
    }
    @Override
    protected void onPause() {
        super.onPause();
        UnRegisterReceiver();
        // Main2Activity.check=true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UnRegisterReceiver();
        //Main2Activity.check=false;
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d(TAG,"Going to Enrol Main ");

        Intent intent = new Intent(this, FaceTabLaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();

        super.onBackPressed();
    }

}
