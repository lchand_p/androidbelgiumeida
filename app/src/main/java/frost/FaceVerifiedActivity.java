package frost;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import lumidigm.captureandmatch.R;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.Enroll;

public class FaceVerifiedActivity extends AppCompatActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_verified);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userIdActivity();
            }
        });
    }

    public void userIdActivity(){

        Log.d("FaceVerifiedAcitivity","UserID starting Enroll");
       // Intent intent = new Intent(this, Enroll.class);
        Intent intent = new Intent(this, UserIdActivity.class);
        startActivity(intent);
    }
}
