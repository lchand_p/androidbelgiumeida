package frost;


import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxand.facerecognition.FaceSuccessActivity;
import com.luxand.facerecognition.MainActivity_FaceandFinger;

import org.json.JSONObject;

import java.lang.reflect.Type;

import database.AppDatabase;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.Enroll;
import lumidigm.captureandmatch.activites.FaceTabLaunchActivity;
import lumidigm.captureandmatch.activites.LaunchActivity;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.captureandmatch.activites.UserNotAvailable;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.GET;
import static lumidigm.constants.Constants.CHECK_USER;
import static webserver.GlobalVariables.FaceandFinger;
import static webserver.GlobalVariables.productVersion;
import static webserver.GlobalVariables.selectedProduct;

public class UserIdActivity extends AppCompatActivity implements  IResult {

    private Button button;
    private Button cancel;
    private EditText UserID;

    AlertDialog alertDialog =null;
    private String TAG="UserIDActivity";

    private String userId = null;
    private String ldapuser = null;
    SharedPreferences Face,Finger,Voice;
    AppDatabase appDatabase;
    private Button back=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (selectedProduct == 1)
            setContentView(R.layout.activity_user_id);
        else
            setContentView(R.layout.activity_user_id_sentinel);



        Log.d(TAG,"Selected Product is "+selectedProduct);
        button = (Button) findViewById(R.id.button4);
        UserID = (EditText) findViewById(R.id.user_id);
        cancel = (Button) findViewById(R.id.button5);

        if (selectedProduct == 1) {
            back = (Button) findViewById(R.id.home3);
            Log.d(TAG,"Got the Home3");
        }
        //public void EnrollTypeActivity(){
        //  Intent intent = new Intent(this,EnrollTypeActivity.class);

        Face=getSharedPreferences("face",MODE_PRIVATE);
        Face.edit().putBoolean("face", false).apply();

        Finger=getSharedPreferences("finger",MODE_PRIVATE);
        Finger.edit().putBoolean("finger", false).apply();

        Voice=getSharedPreferences("voice",MODE_PRIVATE);
        Voice.edit().putBoolean("voice", false).apply();

        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //startActivity(intent);
            //finish();
       // }

        appDatabase = AppDatabase.getAppDatabase(UserIdActivity.this);

        Log.d(TAG,"Enroll onCreate ");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String UserIDVal =  UserID.getText().toString();
                Log.d("UserID","Value of User ID is "+UserIDVal);

                //Check whether this USerID is available
               // new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"AdminFingerVerification","102",UserIDVal);
              //  EnrollTypeActivity(UserIDVal);
                userId= UserIDVal;

                Log.d(TAG,"Notify Failure - "+GlobalVariables.mobileWorkFlowConfigure);
                Log.d(TAG,"Chek User is  "+ (GlobalVariables.supervisorUser));

                boolean serverNotThere=true;//For Testing - For Frost when no network, below should  be there - ohterwise check from server
                 // if (GlobalVariables.mobileWorkFlowConfigure==true) {

                if (serverNotThere) {
                    Log.d(TAG,"Server Not There"+userId);
                    Log.d(TAG,"It is User Face Activity ");
                    //  Intent intent= new Intent(this, MainActivity.class);
                    //Intent intent= new Intent(this, MainActivity_Sentinel.class);
                    Intent intent= new Intent(getApplicationContext(), MainActivity_FaceandFinger.class);
                    if (FaceandFinger) {
                        intent= new Intent(getApplicationContext(), MainActivity_FaceandFinger.class);
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
                    Bundle mBundle = new Bundle();

                   // new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

                    mBundle.putBoolean("Enrolment",true);
                    intent.putExtra("userid", userId);

                    intent.putExtras(mBundle);

                    startActivity(intent);
                    finishAffinity();
                } else
                if (serverNotThere) {
                //if (GlobalVariables.supervisorUser==false) {
                    try {
                        lumidigm.captureandmatch.entity.User user = appDatabase.userDao().countUsersBasedonUserID(userId);
                        if (user != null) {
                            Log.d(TAG, "Got the User details Needs to update" + user.getFullname());

                            Intent intent = new Intent(getApplicationContext(), EnrollTypeActivityNew.class);

                            intent.putExtra("userid", userId);
                            // if (ldapUser != null)

                            intent.putExtra("updateUser",true);
                            intent.putExtra("ldapUser", ldapuser);
                            // else {
                            Bundle bn = new Bundle();
                            if (user != null)
                                bn.putParcelable("userData", user);
                            intent.putExtras(bn);
                            // }
                            startActivity(intent);
                        }
                    } catch (Exception obj) {
                        Log.d(TAG,"SuperVisor User got exception ");
                    }
                } else {
                    Log.d(TAG,"Sending Check UserID API  "+CHECK_USER+UserIDVal);
                      new VolleyService(UserIdActivity.this, UserIdActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + UserIDVal, null, "101");
                  }
            }
        });
        if (back != null) {
            Log.d(TAG,"Getting the onClick Listenere ");
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // String UserIDVal =  UserID.getText().toString();
                    Log.d("UserID", "Cancel Button ");

                    //             new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

                    Log.d(TAG, "Going to Enrol Main ");
                    Intent intent = new Intent(UserIdActivity.this, LaunchActivity.class);
                    //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                    //startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean("Enrolment", false);
                    mBundle.putBoolean("adminVerification", false);
                    // intent.putExtra("userid", editText.getText().toString());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    finishAffinity();

                    //Check whether this USerID is available
                    // new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"AdminFingerVerification","102",UserIDVal);
                    //  EnrollTypeActivity(UserIDVal);
                    // userId= UserIDVal;
                    //  new VolleyService(UserIdActivity.this, UserIdActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + UserIDVal, null, "101");
                }
            });
        } else
            Log.d(TAG,"Back is null");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // String UserIDVal =  UserID.getText().toString();
                Log.d("UserID","Cancel Button ");

   //             new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

                Log.d(TAG,"Going to Enrol Main ");
                Intent intent = new Intent(UserIdActivity.this, LaunchActivity.class);
                //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                //startActivity(intent);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("Enrolment", false);
                mBundle.putBoolean("adminVerification", false);
                // intent.putExtra("userid", editText.getText().toString());
                intent.putExtras(mBundle);
                startActivity(intent);
                finishAffinity();

                //Check whether this USerID is available
                // new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"AdminFingerVerification","102",UserIDVal);
                //  EnrollTypeActivity(UserIDVal);
               // userId= UserIDVal;
              //  new VolleyService(UserIdActivity.this, UserIdActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + UserIDVal, null, "101");
            }
        });
    }

    public void EnrollTypeActivity(String UserID){
        Log.d("UserID","EnrollTypeActivity Value of UserID is "+UserID);
        Intent intent = new Intent(this,EnrollTypeActivityNew.class);
        intent.putExtra("userid", UserID);
        startActivity(intent);
    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        Log.d(TAG, "notifySuccess: " + response.toString());
        Log.d(TAG,"Request Type is "+requestType);
        if (requestType.equals("105")) {
            //Store the LdapUsername
            try {
                JSONObject obj = new JSONObject(response.toString());

                // JSONObject jobject = new JSONObject(response.toString());
                //if (obj != null)
                {
                    ldapuser = obj.optString("uid");
                    Log.d(TAG,"user is ldap "+ldapuser);
                    String UIDNum = obj.optString("uidNumber");

                    Log.d(TAG,"LDAP User ID is "+ldapuser+"  UID is "+UIDNum);
                    if (ldapuser != null ) {
                        Log.d(TAG,"Going for CHECK_USER User "+userId+"  "+ GlobalVariables.SSOIPAddress + CHECK_USER + userId);
                        new VolleyService(UserIdActivity.this, UserIdActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + userId, null, "110");
                    } else
                        Log.d(TAG,"Enroll Failure as User is Not Available ");
                }
            } catch (Exception obj) {
                Log.d(TAG,"Got Exception obj"+obj.getMessage());
            }
            //   new VolleyService(Enroll.this, Enroll.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + editText.getText().toString(), null, "101");

            MyProgressDialog.dismiss();
        } else
        if (requestType.equals("201")) {
            new VolleyService(UserIdActivity.this, UserIdActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + userId, null, "101");
        } else {
            MyProgressDialog.dismiss();


            if (requestType.equals("110") || requestType.equals("101")) {

                Log.d(TAG,"Got the Request Type -101 "+response.toString());
                Gson gson = new Gson();
                Type listType = new TypeToken<lumidigm.captureandmatch.entity.User>() {
                }.getType();

                lumidigm.captureandmatch.entity.User user = gson.fromJson(response.toString(), listType);


          //       new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"EnrollTypeSelect","102",userId;

                if (user.getTemplate1() != null && !user.getTemplate1().isEmpty() &&
                        user.getTemplate2() != null && !user.getTemplate2().isEmpty() &&
                        user.getTemplate3() != null && !user.getTemplate3().isEmpty()) {


                    //Chand Added
                   // Toast.makeText(this, "User Finger Prints Already Available", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"User is there, Chand Added - Home Enroll Activity Activity ");
                    //  Intent intent = new Intent(this, FingerPrintEnrollActivity.class);
                    Intent intent = new Intent(this, EnrollTypeActivityNew.class);
                    intent.putExtra("userid", userId);

                    intent.putExtra("fromsupervisor",true );

                    Log.d(TAG,"LDAP User ID is "+ldapuser);
                    // if (ldapUser != null)
                    //  intent.putExtra("ldapUser", ldapuser);
                    // else {
                    Bundle bn = new Bundle();
                    if (user != null)
                        bn.putParcelable("userData", user);
                    intent.putExtras(bn);
                    // }

                    //intent.putExtra("userid","test" );
                    //startActivity(intent);

                    startActivity(intent);

                    //  Toast.makeText(this, "Already you have added finger prints,so you can go to login and update your profile", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(this, "New User, Finger prints not available", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"No User - so Going to AlreadyExitUserActivity");
                   /* Intent intent = new Intent(this, AlreadyExituserActivity.class);

                    intent.putExtra("userid", editText.getText().toString());
                    Bundle bn = new Bundle();
                    bn.putBoolean("check", false);

                    bn.putParcelable("userData", user);
                    if (ldapuser != null)
                        intent.putExtra("ldapUser", ldapuser);
                    intent.putExtras(bn);
                    startActivity(intent);
*/
                    //chand commented above
                    //Intent intent = new Intent(this, FingerPrintEnrollActivity.class);
                    Intent intent = new Intent(this, EnrollTypeActivityNew.class);

                    intent.putExtra("userid", userId);
                    // if (ldapUser != null)
                    intent.putExtra("ldapUser", ldapuser);
                    // else {
                    Bundle bn = new Bundle();
                    if (user != null)
                        bn.putParcelable("userData", user);
                    intent.putExtras(bn);
                    // }
                    startActivity(intent);

                }
            }
        }
    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG,"Notify Error request Type is "+requestType);

        Log.d(TAG, "notifyError: " + error);



        MyProgressDialog.dismiss();

        //String id=userId;

        /*Log.d(TAG,"Notify Failure - "+GlobalVariables.supervisorUser);
        if (GlobalVariables.supervisorUser==false) {
            try {
                lumidigm.captureandmatch.entity.User user = appDatabase.userDao().countUsersBasedonUserID(userId);
                if (user != null) {
                    Log.d(TAG, "Got the User details Needs to update" + user.getFullname());

                    Intent intent = new Intent(this, EnrollTypeActivityNew.class);

                    intent.putExtra("userid", userId);
                    // if (ldapUser != null)
                    intent.putExtra("ldapUser", ldapuser);
                    // else {
                    Bundle bn = new Bundle();
                    if (user != null)
                        bn.putParcelable("userData", user);
                    intent.putExtras(bn);
                    // }
                    startActivity(intent);
                }
            } catch (Exception obj) {
                Log.d(TAG,"SuperVisor User got exception ");
            }
        } else*/
        if ((requestType.equals("110")) || (requestType.equals("101"))) {
//            Toast.makeText(this, "User Not Available", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "User Doesn't exit", Toast.LENGTH_SHORT).show();

            //Chand Added
            Log.d(TAG,"Chand Added - User Not Available ");
            Intent intent = new Intent(this, FrostUserNotAvailable.class);

            intent.putExtra("userid", userId);

            //intent.putExtras(bn);
            // }
            startActivity(intent);

            /*Intent intent = new Intent(this, AlreadyExituserActivity.class);

            intent.putExtra("userid", editText.getText().toString());
            Bundle bn = new Bundle();
            bn.putBoolean("check", false);

            //bn.putParcelable("userData", user);
            if (ldapuser != null)
                intent.putExtra("ldapUser", ldapuser);

            intent.putExtras(bn);
            startActivity(intent);
*/
/*
            Log.d(TAG,"Chand Added - Finger Print Enroll Activity ");
            Intent intent = new Intent(this, FingerPrintEnrollActivity.class);

            intent.putExtra("userid", editText.getText().toString());
            // if (ldapUser != null)
            intent.putExtra("ldapUser", ldapuser);
            // else {
            Bundle bn = new Bundle();
           // if (user != null)
            //    bn.putParcelable("userData", user);
            intent.putExtras(bn);
            // }
            startActivity(intent);*/
        } else if (requestType.equals("105")) {
            Toast.makeText(this, "User Doesn't exit", Toast.LENGTH_SHORT).show();

            //Chand Added
            Log.d(TAG,"Chand Added - User Not Available ");
            Intent intent = new Intent(this, UserNotAvailable.class);

            intent.putExtra("userid", userId);

            //intent.putExtras(bn);
            // }
            startActivity(intent);

            ///String text = "Employee number " + "<font color=#d50c20>" + id + "</font>" + " not found Please re-enter......";
            //textView.setText(Html.fromHtml(text));
            //textView.setText("Employee number "+editText.getText().toString() +" not found Please re-enter......");
            //editText.setText("");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Main2Activity.check=true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Main2Activity.check=false;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d("UserID","onBackPressed Button ");

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");

        Log.d(TAG,"Going to Enrol Main ");

        Intent intent = new Intent(UserIdActivity.this, LaunchActivity.class);

        //   Intent intent = new Intent(UserIdActivity.this, FaceTabLaunchActivity.class);
         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment", false);
        mBundle.putBoolean("adminVerification", false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finishAffinity();

        super.onBackPressed();
    }
}
