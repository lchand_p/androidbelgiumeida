package frost;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import lumidigm.captureandmatch.R;

public class UserVoiceActivity extends AppCompatActivity {

    private TextView text;
    private TextView textView13;
    private TextView textView18;

    private ImageView imageView16;
    private ImageView imageView;
    private int current_image;
    int[] images = {R.drawable.voiceenroll,R.drawable.voice_sample_1,R.drawable.voice_sample_2,R.drawable.voice_sample_3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_voice);


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        text = (TextView) findViewById(R.id.textView14);

        textView13 = (TextView) findViewById(R.id.textView13);
        textView18 = (TextView)findViewById(R.id.textView18);

        imageView16 = (ImageView)findViewById(R.id.imageView16);

        imageView = (ImageView) findViewById(R.id.imageView15);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageClick();
            }
        });
    }

    public void onImageClick(){

        current_image++;
        current_image = current_image % images.length;
        imageView.setImageResource(images[current_image]);

        if(current_image == 1){
            text.setText("Say hello !!!");
        }else if (current_image == 2){
            text.setText("second teext");
        }

        if(current_image == 3){

            imageView16.setVisibility(View.INVISIBLE);
            text.setVisibility(View.INVISIBLE);
            textView13.setVisibility(View.INVISIBLE);
            textView18.setText("Voice Successfully Verified");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                  Intent intent = new Intent(UserVoiceActivity.this,UserVoiceVerificationActivity.class);
                  startActivity(intent);
                }
            },2000);



        }

    }
}
