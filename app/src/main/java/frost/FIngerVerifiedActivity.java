package frost;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.luxand.facerecognition.MainActivity;
//import com.luxand.facerecognition.SuccessActivity;

import database.AppDatabase;
import lumidigm.captureandmatch.R;

import lumidigm.captureandmatch.R;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.GET;

public class FIngerVerifiedActivity extends AppCompatActivity {

    private Button button;
    private String TAG="FingerVerifiedActivity";
    ;
    SharedPreferences prefs=null;
    private BroadcastReceiver mUpdateReceiver;
    private IntentFilter mUpdateFilter;
    private TextView personnameID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_finger_verified);
        setContentView(R.layout.activity_main2_supervisor);


        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        prefs = getBaseContext().getSharedPreferences(
                GlobalVariables.SP_FingerPrint, MODE_PRIVATE);

        Log.d(TAG,"Before WebUtil Sending Message with state ");
        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"AdminFingerVerification",null,"Initiated");
       // SendingMessageWithState("AdminFingerVerification","400",null,"Initiated");

        //Response shall be Either AdminFingerVerification Success or Failure

        /*
        button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userIdActivity();
            }
        });*/
        //For Testing
       // AdminFingerSuccess();
        RegisterInterReceiver();
      //  RegisterReceiver();
    }

    public void userIdActivity(){
        Intent intent = new Intent(this, UserIdActivity.class);
        startActivity(intent);
    }


    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }



    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d("MainActivity", "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d("MainActivity","Got exception "+obj.toString());
        }
        return false;
    }
    private void AdminFingerSuccess(String personName) {
        Log.d(TAG,"Admin Finger Success"+personName);

        setContentView(R.layout.activity_finger_verified);
        //setContentView(R.layout.activity_main2_supervisor);
        personnameID = findViewById(R.id.textView5);
        personnameID.setText(personName);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                userIdActivity();
                //  finish();
            }
        }, 4000);
    }

    private void AdminNotAUthorized() {
        Log.d(TAG,"Admin Not Finger Success");

        setContentView(R.layout.admin_not_authorized);
        //setContentView(R.layout.activity_main2_supervisor);
      //  personnameID = findViewById(R.id.textView5);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,"Starting MainActivity ");
                Intent intent= new Intent(FIngerVerifiedActivity.this, MainActivity.class);
                //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                //startActivity(intent);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("Enrolment",false);
                mBundle.putBoolean("adminVerification",false);
                // intent.putExtra("userid", editText.getText().toString());
                intent.putExtras(mBundle);
                startActivity(intent);
                finishAffinity();
                //  finish();
            }
        }, 4000);
    }
    private void RegisterInterReceiver() {

        //registerReceiver(this.FingerScanActivity,
        //      this.mUpdateReceiver);

        if (GlobalVariables.AppSupport == 1) {
            Log.d(TAG, "In RegisterItnerReceiver");
            this.mUpdateFilter = new IntentFilter("lumidigm.captureandmatch.mobile.fingerverified");

            try {
                this.mUpdateReceiver = new BroadcastReceiver() {
                    public void onReceive(final Context context, final Intent intent) {
                        final String state = intent.getStringExtra("STATE");
                        final String  Status = intent.getStringExtra("EVENT");

                        Log.e("Main", "artisecure.activites Received UI Event" + Status+" "+state);

                        {
                            if (Status != null && Status.equals("AdminFingerSuccess")) {

                                Log.d(TAG, "Got event for the  " + Status);
                                AdminFingerSuccess(state);
                            } else
                            if (Status != null && Status.equals("AdminFingerFailure")) {

                                Log.d(TAG, "Got event for the  " + Status);
                              //  setContentView(R.layout.admin_not_authorized);
                                AdminNotAUthorized();
                            }
                        }
                    }
                };
            } catch (Exception obj) {
                Log.d(TAG, "Got Crashed WHile Intent FIlter " + obj.getMessage());
            }
        }
        //   getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
    }
    private void UnRegisterReceiver() {
        Log.d(TAG,"Got UnRegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG,"Getting unRegister");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.mUpdateReceiver);
                //this.mUpdateReceiver = null;
            } catch (Exception obj) {
                Log.d(TAG,"Got crashed while unregister "+obj.getMessage());
            }
        }
    }

    private void RegisterReceiver() {
        Log.d(TAG, "Got RegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG,"Register Receiver ");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mUpdateReceiver, mUpdateFilter);
            } catch (Exception obj) {
                Log.d(TAG,"RegisterReceiver crashed "+obj.getMessage());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        RegisterReceiver();
         getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

     }

    @Override
    protected void onPause() {
        super.onPause();
        UnRegisterReceiver();
        Log.d(TAG," on Pause VCOM Close ");

        //Chand added below
        //mVCOM=null;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UnRegisterReceiver();
        Log.d(TAG," on on Destroy ");

        UnRegisterReceiver();
        //Chand added below
        //mVCOM=null;

    }
}
