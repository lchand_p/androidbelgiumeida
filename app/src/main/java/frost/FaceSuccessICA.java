package frost;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxand.facerecognition.MainActivity_FaceandFinger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import lumidigm.captureandmatch.R;
import webserver.Bitmaps;
import webserver.GlobalVariables;

import static webserver.GlobalVariables.FaceICA;

public class FaceSuccessICA extends AppCompatActivity {

    private TextView textView,textView9,personnameID;
    private ImageView photoImage;
    private TextView  IDNumber,ArabicName,expiryDate, Nationality; // = findViewById(R.id.ArabicName);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_check_in);

        if (GlobalVariables.IDNumber == null) {
            Log.d("Chand","ID Number is null");
            returntogif();
        } else {
        Log.d("Chand","It is Face ICA Testing");
        setContentView(R.layout.face_user_successforsmartcard);

        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        try {

            personnameID = findViewById(R.id.personname);

            Log.d("ICA", "second Received personName is " + GlobalVariables.personName);
/*
            if (FaceICA) {
                if (GlobalVariables.personName != null) {
                    personnameID.setText(GlobalVariables.personName);
                }
            }*/
        } catch (Exception obj) {

        }

        try {
            photoImage = findViewById(R.id.image_success);
           /* String filename = Environment.getExternalStorageDirectory()
                    + File.separator + "displayImages" + File.separator
                    + "1.jpg";


            File fp= new File(filename);

            Bitmap bm = BitmapFactory.decodeFile(fp.getAbsolutePath());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
           // bm.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
            byte[] photo = baos.toByteArray();

            if (photo == null || photo.length <= 0) {
                return;
            }//if()
            //Create  a bitmap.
            //set to the imageview
*/

                IDNumber = findViewById(R.id.IDNumber);
                ArabicName = findViewById(R.id.ArabicName);
                expiryDate = findViewById(R.id.expiryDate);
                Nationality = findViewById(R.id.nationality);

            if (GlobalVariables.IDNumber != null) {
                  IDNumber.setText("IDNumber      : "+GlobalVariables.IDNumber);
            }  else
                IDNumber.setVisibility(View.INVISIBLE);


            if (GlobalVariables.personName != null) {
                personnameID.setText("Name:"+GlobalVariables.personName);
            }  else
                personnameID.setVisibility(View.INVISIBLE);

            if (GlobalVariables.ArabicName != null) {
                ArabicName.setText("ArabicName    : "+GlobalVariables.ArabicName);
            } else
                ArabicName.setVisibility(View.INVISIBLE);

             if (GlobalVariables.expiryDate != null) {
                expiryDate.setText("ExpiryDate    : "+GlobalVariables.expiryDate);
            }  else
                expiryDate.setVisibility(View.INVISIBLE);

            if (GlobalVariables.Nationality != null) {
                Nationality.setText("Nationality   : "+ GlobalVariables.Nationality);
            }  else
                Nationality.setVisibility(View.INVISIBLE);


            // GlobalVariables.personName=null;
            //GlobalVariables.IDNumber=null;
            /*GlobalVariables.ArabicName=null;
            GlobalVariables.expiryDate=null;
            GlobalVariables.Nationality=null;
            GlobalVariables.Gender=null;
*/

            new ProcessImage().execute();
          //  photoImage.setImageBitmap(Bitmaps.decodeSampledBitmapFromBytes(photo, 150, 150));

          //  Log.d("PublicData","Photo Height is"+photoImage.getMaxHeight()+" Width is "+photoImage.getMaxWidth());

        } catch (Exception obj) {

        }
       /* String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        String date = dateFormat.format(Calendar.getInstance().getTime());


        textView = (TextView)findViewById(R.id.textView8);
        textView.setText(currentTime);

        textView9 = (TextView)findViewById(R.id.textView9);
        textView9.setText(date);
*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                returntogif();
            }
        },4000);
        }
    }

    public void returntogif(){
        Intent intent = new Intent(this, MainActivity_FaceandFinger.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment",false);
        mBundle.putBoolean("adminVerification",false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finish();

//        finish();
    }
    private class ProcessImage extends AsyncTask<String, String, byte[]> {

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected byte[] doInBackground(String... paramArrayOfString) {

            Log.d("Test","Close Connection chand");

            //ConnectionController.closeConnection();

            Log.d("Tst","Close connection ");
            //ConnectionController.cleanup();

            String filename = Environment.getExternalStorageDirectory()
                    + File.separator + "displayImages" + File.separator
                    + "1.jpg";


            try {
                File fp = new File(filename);


                Bitmap bm = BitmapFactory.decodeFile(fp.getAbsolutePath());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
                byte[] photo = baos.toByteArray();

                if (photo == null || photo.length <= 0) {
//                return;
                }//if()
                //Create  a bitmap.
                //set to the imageview

                // photoImage.setImageBitmap(Bitmaps.decodeSampledBitmapFromBytes(photo, 150, 150));


                return photo;
            } catch (Exception obj) {
                return null;
            }
           // return "test";

        }

        protected void onPostExecute(byte[] photo) {
            Log.d("Test", "on Post response is ");

            photoImage = findViewById(R.id.image_success);

            if (photo != null)
            photoImage.setImageBitmap(Bitmaps.decodeSampledBitmapFromBytes(photo, 150, 150));
        }
    }

}
