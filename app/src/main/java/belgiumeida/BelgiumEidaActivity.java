package belgiumeida;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
//import android.support.design.widget.TextInputLayout;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


//import com.ftsafe.readerScheme.FTReader;
import com.luxand.facerecognition.MainActivity_FaceandFinger;
import com.luxand.facerecognition.WebICAImageCapture_Server;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import belgiumeida.carddatabase.CardUser;
import belgiumeida.carddatabase.UserCardDatabase;
import belgiumeida.tlv.BerTag;
import belgiumeida.tlv.BerTlv;
import belgiumeida.tlv.BerTlvBuilder;
import belgiumeida.tlv.BerTlvParser;
import belgiumeida.tlv.BerTlvs;
import belgiumeida.tlv.HexUtil;
import database.AppDatabase;
import lumidigm.captureandmatch.R;
import webserver.GlobalVariables;

import static belgiumeida.Tool.convertByteArrayToHexString;
import static belgiumeida.Tool.toByteArray;
import static com.feitian.readerdk.Tool.Tool.byte2HexStr2;


/*

MF - Master File
DF - Dedicated File
EF - Elementary File

MF 3f00 -  2 byte file identifier -  - hexa decimal notation val
DF  DF01
ID File 4031

#define BEID_FILE_ID					"3F00DF014031"
#define BEID_FILE_ID_SIGN				"3F00DF014032"
#define BEID_FILE_ADDRESS				"3F00DF014033"
#define BEID_FILE_PHOTO					"3F00DF014035"

Belpic applet A0 00 00 00 30 29 05 70 00 AD 13 10 01 01 FF
DF(BELPIC) A0 00 00 01 77 50 4B 43 53 2D 31 35
DF(ID) (not on applet V1.0) A0 00 00 01 77 49 64 46 69 6C 65 73


DF(ID) (not on applet V1.0) A0 00 00 01 77 49 64 46 69 6C 65 73

command: 00 a4 04 00 0c A0 00 00 01 77 49 64 46 69 6C 65 73
output: 6121

61	XX	I	Command successfully executed; ‘XX’ bytes of data are available and can be requested using GET RESPONSE.

It can be either by AID, Path, or File ID
Above is AID
- command:
00 a4 02 00 02 3F 00 (mf)
-sUCCESS IS 90 00
00 a4 02 00 02 DF 01 (df)
00 a4 02 00 02 40 35 (pHOTO fILE)

Belgium EID:

Instruction
a4 = select file
c0 - read data
ca - read resonse
b0 - read binary file
da - put data

Photo File:
JPGEG Image size is 4035 bytes
Current Resolution is width 140 x height 200 pixels, grey levels: 8 bits

 */
public class BelgiumEidaActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "BelgiumEidaActivity";
  //  private static CardManager mOperation;
    private Button mBtnGetVersion;
    private Button mBtnPowerUp;
    private Button mBtnSendApdu;
    private EditText mEtRecvApduData;
    private EditText mPhysicalNumber;
   // FTReader ftReader=null;

    private boolean executionStatus=true;
      private static CountDownLatch latch = null;

    //private static ArtiSecureSmartCardReader cardReader = null;
    private static FeitianCardReader cardReader = null;

    public static final int FILESCTRUCTURE_VERSION_BIN_TYPE_1 = 0x00;
    public static final int CARDNUMBER_ASCII_TYPE_12_1 = 0x01;
    public static final int CHIP_NUMBER_BINA_TYPE_16_2 = 0x02;
    public static final int CARD_VALIDITY_BEGIN_ASCII_TYPE_10_3 = 0x03;
    public static final int CARD_VALIDITY_END_ASCII_TYPE_10_4 = 0x04;
    public static final int CARD_DEL_MUNSPALITY_UTF_TYPE_5 = 0x05;
    public static final int NATIONAL_NUMBER_ASCII_TYPE_11_6 = 0x06;
    public static final int NAME_UTF_TYPE_110_7 = 0x07;
    public static final int NAME_2_UTF_TYPE_95_8 = 0x08;
    public static final int FIRSTLETTER_GIVENNAME3_2_UTF_TYPE_95_8 = 0x09;
    public static final int NATIONALITY_UTF_TYPE_85_10 = 0x0A;
    public static final int BIRTH_LOC_TYPE_80_11 = 0x0B;
    public static final int BIRTHDATE_TYPE_12_12 = 0x0C;
    public static final int SEX_ASCII_TYPE_1_14 = 0x0D;
    public static final int NOBLE_CONDITION_UTF_TYPE_1_15 = 0x0E;
    public static final int DOCUMENT_TYPE_1_15 = 0x0F;
    public static final int SPECIALSTATUS_UTF_TYPE_1_16 = 0x10;
    public static final int HASH_PICTURE_BINARY_TYPE_1_17 = 0x11;
    public static final int SPECIAL_ORGNISATION_TYPE_1_19 = 0x13;
    public static final int MEMBER_OF_FAMILY_TYPE_1_20 = 0x14;

    public static final int DATE_AND_COUNTRY_OF_PRODTECTION_TYPE_1_21 = 0x15;
/*

    private String FileVersion=null;
    private String CardNumber=null;
    private String ChipNumber=null;
    private String ValidityeBegin=null;
    private String ValidityEnd = null;
    private String NationalNumber=null;
    private String CardDeliveryMunspality=null;
    private String Name=null;
    private String Nationality=null;
    private String BirthLoc=null;
    private String Sex=null;
    private String Name_2=null;
    private String FirstLetterName=null;
    private String BirthLocation=null;
    private String BirthDate=null;
    private String NobleCondition=null;
    private String DocumentType=null;
    private String SpecialStatus=null;
    private String HashPicture=null;
    private String SpecialOrganization=null;

    private String MemberofFamilyType=null;
    private String DateandCountryofProtection=null;
*/

    private boolean selectBelgiumEida=false; //When Real card is there , set it as true otherwise set it as false
    private AppDatabase appDatabase=null;

    public CardDataVariables cardData=null;


    public static void start(Context context) {
        Log.d(TAG, "in Start ");
        context.startActivity(new Intent(context, BelgiumEidaActivity.class));
        Log.d(TAG, "after contactSPUCardActivity");
    }

    //   @Override
    protected void InitializeCardReader() {
        //    super.init();
        Log.d(TAG, "in init ");
        executionStatus=true;
        try {
            try {
                GlobalVariables.CardReadStatus = 1;
                //   Config config = new Config(path, baudrate);
                // ArtiSecureSmartCardReader    tpReader=new ArtiSecureSmartCardReader(this);
                cardReader = new FeitianCardReader(this);
                RemoveLogFile();
                cardReader.OpenNew();

                Log.d(TAG, "Open New ");

                Init_NewPlugin1(this);
                Log.d(TAG, "Initialize New ");
            } catch (Exception obj) {
                executionStatus=false;
                Close();
                cardReader=null;
                GlobalVariables.CardReadStatus=4;
                Log.d(TAG,"Card Reader Initialization Failure"+obj.getMessage());
                ReturntoMainActivity();
            }
        //InitiateATR();

            if (cardReader != null) {

                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "Going for Initialize ATR ");

                            listReaders();
                            InitiateATR();
                        }
                    }, 1000);
                } catch (Exception obj) {
                    Close();
                    cardReader=null;
                    executionStatus=false;
                    ReturntoMainActivity();
                    Log.d(TAG,"Close Plugin exception ");
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (cardReader != null) {

                            try {
                            Log.d(TAG, "Going to Get the Photo");
                            boolean status1= GetthePhotoFromSmartCard();
                            Log.d(TAG, "Finished getting from smart card ");

                            if (status1 == true ) {
                                Log.d(TAG, "Initiate ATR ");
                                InitiateATR();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d(TAG, "Going for Initialize ATR ");

                                        // listReaders();
                                        // InitiateATR();

                                        //if (selectBelgiumEida)
                                        GetCardDetailsFromCard();
                                        Close();
                                       // returntogif();
                                        if (GlobalVariables.CardReadStatus == 5)
                                            ShowCardReadDataAvailable();
                                        else
                                            ReturntoMainActivity();
                                    }
                                }, 1000);
                            }  else {
                                Log.d(TAG,"Failure while parsing coming out ");
                                Close();
                                ReturntoMainActivity();

                            }
                            ;} catch (Exception obj) {
                                executionStatus=false;
                                Log.d(TAG,"Got exception while parsing 2 coming out ");
                                Close();
                                ReturntoMainActivity();
                            }

                        }
                    }
                }, 3000);
            }
        } catch (Exception obj) {
            executionStatus=false;
            Log.d(TAG,"Intializecard Reader Exception ");
            Close();
            ReturntoMainActivity();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("Chand","It is Face ICA Testing");
    setContentView(R.layout.face_user_progressforsmartcard);

    appDatabase = AppDatabase.getAppDatabase(this);

    startWeb();

    try {
        Log.d("Test", "on Create ");
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    } catch (Exception obj) {
        Log.d("Test","Got Exception while setting Full Screen");
    }

        InitializeCardReader();
      //  initView();
     /*   byte[]  data= GetCardDataTest();
        ProcessCarddata(data);
*/


      /*  new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Going for Initialize ATR ");
                returntogif();

            }
        }, 5000);*/

    }

    private void ReturntoMainActivity(){
        Log.d(TAG,"Starting the FaceandFinger Activity");
        if (executionStatus == true ) {
            Intent intent = new Intent(this, MainActivity_FaceandFinger.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean("Enrolment", false);
            mBundle.putBoolean("adminVerification", false);

            mBundle.putBoolean("fromBelgiumEidaActivity", true); //to Show Unauthorized access

            // intent.putExtra("userid", editText.getText().toString());
            intent.putExtras(mBundle);
            startActivity(intent);

            finish();
        } else {
            try {
                Log.d(TAG,"Got the exception so coming out");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Going for finishAffinity ");
                        finishAffinity();                    }
                }, 2000);


            } catch (Exception oj) {
                Log.d(TAG,"Got the finish Affinity exception "+oj.getMessage());
            }
        }

//        finish();
    }
    private void ShowCardReadDataAvailable(){
        Log.d(TAG,"Starting the Enrollment Activity");
        Intent intent = new Intent(this, CardReadAlreadyBelgiumEida.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        Bundle mBundle = new Bundle();
        mBundle.putString("cardnumber",CardDataVariables.CardNumber);
        mBundle.putBoolean("Enrolment",false);
        mBundle.putBoolean("adminVerification",false);

        mBundle.putBoolean("fromBelgiumEidaActivity",true); //to Show Unauthorized access

        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);

        finish();

//        finish();
    }

    private void startWeb()
    {

        final int DEFAULT_PORT = 8085;
        Context app_context = getApplicationContext();
        WebICAImageCapture_Server server= new WebICAImageCapture_Server(DEFAULT_PORT,app_context);
        try
        {
            server.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String data = intent.getStringExtra("Data");
//            user_data.setText(data);


            //Get the File

            //data = data.replaceAll("   ","+");
            //data = data.replaceAll("  ","+");
            data = data.replaceAll(" ","+");

            Log.d(TAG,"Got the Face Detection"+data);

            //byte[] photo =toByteArray(data);
            byte[] photo = Base64.decode(data, Base64.DEFAULT);
            Log.d(TAG,"Got the Face Detection"+photo.length);
            if (photo == null || photo.length <= 0) {
                Log.d(TAG,"Photo Contents are null ");
                return;
            }//if()
            //Create  a bitmap.
            //set to the imageview
//chand added below 2 commands
            Bitmap bmp = BitmapFactory.decodeByteArray(photo, 0, photo.length);
            // bmp.compress(Bitmap.CompressFormat.JPEG);
            try {
                Log.d("Test","Getting the file ");

                List<lumidigm.captureandmatch.entity.User> users = appDatabase.userDao().getAll();

                if (users != null) {
                    Log.d(TAG, "Get User Count is " + users.size());

                    String userID = (users.get(0).get_id());

                    String filename = Environment.getExternalStorageDirectory()
                            + File.separator
                            + "displayImages" + File.separator + userID + ".jpg";

                    try {
                        File f = new File(filename);

                        if (f.exists())
                            f.delete();
                    } catch (Exception obj) {

                    }
                    Log.d(TAG,"File Name is "+filename);

                    FileOutputStream out = new FileOutputStream(filename,false);
					/*try {
						out.write(("").getBytes());
					} catch (Exception Obj) {
						Log.d(TAG,"Clear file exception");
					}*/
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

                    Log.d(TAG, "Update File ");
                    // bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
                    //bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                    out.close();
                } else
                    Log.d(TAG,"Not found user ");
            } catch (Exception obj) {
                Log.d("Test","Got the exception ");
            }
            //imageview.setImageBitmap(bmp);
            Log.d("data", "data from server"+ data);
        }
    };


    @Override
    protected void onStart() {
        super.onStart();

       /* if (FaceBelgium == true) {
            IntentFilter intentFilter = new IntentFilter("lumidigm.captureandmatch"); //lumidigm.captureandmatch");
            registerReceiver(broadcastReceiver, intentFilter);
        }*/
    }

    @Override
    protected void onStop() {
        super.onStop();

        /*if (FaceBelgium == true)
            unregisterReceiver(broadcastReceiver);*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
          /*  case R.id.btn_get_version:
                Log.d(TAG,"button click version ");
                getVersion();
                break;
            case R.id.btn_power_up:
                powerUp();
                break;
            case R.id.btn_send:
                sendApdu();
                break;*/
            default:
                break;
        }
    }

    private void sendApdu() {
        showMsg("send Apdu......");


    }

    private byte[] getData() {
        String dataStr = "0012"; //mTilApduData.getEditText().getText().toString().trim();
        byte[] arr3 = {(byte) 0x80, (byte) 0xC0, (byte) 0x02, (byte) 0xA1, (byte) 0x08};

//        return Utils.hexString2ByteArray(dataStr);
        return arr3;
    }


    private void powerUp() {
        showMsg("Power Up......");

        Log.d(TAG, "Power Up start ");

        contactCpuPowerUp();

        Log.d(TAG, "Power Up end ");
    }

    int versionStatus = 0;

    public void getVersion() {
        Log.d(TAG, "in getVersion");

        showMsg("Get Version......");
        // latch = new CountDownLatch(1);
        // children[0] = new DeviceThread(latch);

        versionStatus = 0;

        //waitForDeviceThreadsToComplete();
        Log.d(TAG, "Completed the getVersion " + versionStatus);
         Log.d(TAG, "coming out" + versionStatus);
    }

    public void getVersion1() {
        Log.d(TAG, "in getVersion");
        showMsg("Get Version......");
        versionStatus = 0;
         Log.d(TAG, "Completed the getVersion " + versionStatus);
         Log.d(TAG, "coming out" + versionStatus);
    }

    public void clearEditTextError(EditText editText) {
        editText.setError(null);
        editText.setText("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: ");
      //  mOperation.close();
    }
/*
    @Override
    public void onDataReceived(final byte[] bytes, final int i) {
        runOnUiThread(() -> showMsg(Utils.byteArray2HexString(bytes, true, true, 0, i)));
    }*/

     void showMsg(String tp) {
        Log.d(TAG, "Got the Show " + tp);
    }

    private void GettheImage() {

        Log.d(TAG, "Get the Image ");

        /*
         * Select AIU
         * Select File
         * Get the Binary File
         */

        //byte[] sendapdu = { (byte) 0x80,(byte)  0xC0,(byte) 0x02,(byte)  0xA1,(byte)  0x08};

        //00a404000ca00000024300130000000101
        //00A404000CA00000024300130000000101
        byte[] sendapdu2 = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xa0, (byte) 0x00, (byte) 0x00
                , (byte) 0x02, (byte) 0x43, (byte) 0x00, (byte) 0x13, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01};

        byte[] test = Plugin_APDU(sendapdu2, 12, 1);

        //command: 00a404000ca00000024300130000000101
        //output:
        //6121
        //
        //command: 00a40000020202

        if (test != null) {
            Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu2));
            Log.d(TAG, "length of APDU response is " + test.length);
            //    Log.d(TAG, "Received APDU" + (test));

            Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
            //byte[] test2= cardReader.sendApdu(sendapdu2);
        }
        //         00 a4 00 00 02 02 03

        byte[] sendapdu3 = {(byte) 0x00, (byte) 0xa4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x02};

        // byte[] sendapdu3 = { (byte) 0x00,(byte)  0xa4,(byte) 0x00,(byte)  0x00,(byte)  0x02, (byte)  0x02,(byte)  0x03 };
        byte[] test3 = Plugin_APDU(sendapdu3, 4, 1);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu3));

        if (test3 != null)
            Log.d(TAG, "3 Rcvd APDU Array is " + convertByteArrayToHexString(test3));


        //00 c0 00 00 12
        //command: 00c0000012

        byte[] sendapdu4 = {(byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x12};
        byte[] test4 = Plugin_APDU(sendapdu4, 4, 1);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu4));
        if (test4 != null)
            Log.d(TAG, "4 Rcvd APDU Array is " + convertByteArrayToHexString(test4));

        //00 b0 00 00 00
        byte[] sendapdu5 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0x00, (byte) 0x00};


        Log.d(TAG, "Send APDU is first before" + convertByteArrayToHexString(sendapdu5));

        byte[] test5 = Plugin_APDU(sendapdu5, 5, 1);

        Log.d(TAG, "Send APDU is first " + convertByteArrayToHexString(sendapdu5));
        if (test5 != null)
            Log.d(TAG, "5 Rcvd APDU Array is " + convertByteArrayToHexString(test5));

        //00b00000ff
        byte[] sendapdu6 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0x00, (byte) 0xff};
        byte[] test6 = Plugin_APDU(sendapdu6, 2, 1);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu6));

        if (test6 != null)
            Log.d(TAG, "6 Rcvd APDU Array is " + convertByteArrayToHexString(test6));

        //command: 00b000ff00
        byte[] sendapdu7 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0x00};
        byte[] test7 = Plugin_APDU(sendapdu7, 2, 1);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu7));

        if (test7 != null)
            Log.d(TAG, "7 Rcvd APDU Array is " + convertByteArrayToHexString(test7));


        //command: 00b000ffff
        byte[] sendapdu8 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0xff};
        byte[] test8 = Plugin_APDU(sendapdu8, 5, 1);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu8));

        if (test7 != null)
            Log.d(TAG, "8 Rcvd APDU Array is " + convertByteArrayToHexString(test8));


        //command: 00b001feff
        byte[] sendapdu9 = {(byte) 0x00, (byte) 0xb0, (byte) 0x01, (byte) 0xfe, (byte) 0xff};
        byte[] test9 = Plugin_APDU(sendapdu9, 5, 1);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu9));

        if (test7 != null)
            Log.d(TAG, "9 Rcvd APDU Array is " + convertByteArrayToHexString(test9));


        //command: 00b001feff
        byte[] sendapdu10 = {(byte) 0x00, (byte) 0xb0, (byte) 0x05, (byte) 0xfa, (byte) 0xff};
        byte[] test10 = Plugin_APDU(sendapdu10, 5, 1);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu10));

        if (test10 != null)
            Log.d(TAG, "10 Rcvd APDU Array is " + convertByteArrayToHexString(test10));


        //00 b0 0e f1 ff
//The photo is stored as .jpeg on the card, just read file 4035 (don't forget the chop off the status words (last 2 bytes)), and save it as a binary file.


        //00b002fdff

        //00b003fcff

        //00b006f9ff

        //00b007f8ff

        //00b000ffff
/*
        String hex = Integer.toHexString(123);
        //Byte.parseByte(123);
        Log.d(TAG,"Got completed and coming out ");
        Integer i = new Integer(11534336);

        String hex2 = Integer.toHexString(i);
        System.out.println("Hex value: "+hex2);

        Byte[] byt3=Byte.parseByte(hex2);*/

        String FHex = "FF";
        String THex = "FF";

        // String test2=Integer.toHexString(255);
        // String test11=Integer.toHexString(29527900‬);

        //long ts=2952790013;
      /*  int num1=Integer.parseInt(FHex, 16);
        int num2=Integer.parseInt(THex, 16);

        int sum = num1+num2;

        String hex2 = Integer.toHexString(sum);

*/
        //  Log.d(TAG,"Hexa String first is "+test2);

        /*long   MAX_VALUE = 2952790012
        long max = 9223372036854775807;//Inclusive;
        long yourmilliseconds = 1222402191823;
        String test12=Long.toHexString(MAX_VALUE‬);*/

        //  Log.d(TAG,"Hexa String sec is "+test11);

        //B0 0000‬
        //B0 00FF  - 255
        //B0 01FE‬    500
        //B0 02FD‬    725

        //B0 03FC‬    1000

        //B0 04FB   1255

        //B0 05FA‬  1500

        //B0 06F9‬ - 1750
        //B0 07F8‬  - 2000

        //B0 08F7‬
        //B0 08F7‬   - 2250


        //B0 0651  - 2500
        //B0 0750‬    2750
        //B0 084F‬    3000
        //B0 094E‬     3250


        //B0 0A4D‬  - 3500
        //B0 0B4C‬   - 3750
        //B0 0C4B   - 4000
        //B0 0D4A‬     4250

        //B0 0E49  - 4500
        //B0 0F48‬    4750
        //B0 1047‬    5000
        //B0 1146‬    5250


        //4035 size

        //B0 0000‬


        //For Test

        //00 B0 00 ff

        /*

        command: 00a404000ca00000024300130000000101
        6121

        command:00a40000020203
        6112

        command: 00c0000012
        8510000002030100043dc000c0000000006a9000

        command: 00b0000000
        6cff

        command: 00b00000ff
        output:
        700301c7e3050002495243060004201903074307000420240307a3080000a3090034d8b3d8a7d984d9852cd985d8a7d8acd8af2cd985d8b5d984d8ad2cd8a8d986d8afd8b12cd8a7d984d982d8add8b7d8a7d986d98ae30a0000e30b002353616c656d2c4d616a65642c4d75736c65682c42616e6461722c416c71616874616e69e30c00014da30d0010d8a7d984d8b3d8b9d988d8afd98ad987e336000c536175646920417261626961e30e0003534155430f000420020505a3370000e338000644616d6d616d631201004edf9b30494b292945277ecb9ebd88b0d681b955ea270579c6d67559c863116a6419633a0c2acbe4131504736270a069072cd99d9000

        command: 00b000ff00
        output:
        6cff */
    }

    private boolean GetthePhotoFromSmartCard() {
        String base = "00b0";
        String offset = "0000";
        int curroffset = 0;
        int lengthtoRetrieve = 2295; //4050;

        //Photo is 4025 bytes
        if (selectBelgiumEida)
            lengthtoRetrieve = 4050;

        String exctractlength = "ff";
        byte[] totalbyte;

        byte[] currbytes = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0xff};
        int lengthBytes = currbytes.length;

        byte[] destination = new byte[lengthtoRetrieve + 255];


        Log.d(TAG,"in GetthePhotoFromSmartCard");
        lengthBytes = 0;

        //Select the Photo File
        boolean status = SelectPhotoFile();

        if (status == true) {
            try {
                for (int i = 0; curroffset < lengthtoRetrieve; i++) {

                    curroffset = Integer.parseInt(offset, 16) + (i * 255);

                    String currVal = Integer.toHexString(curroffset);

                    if (currVal.length() == 3)
                        currVal = "0" + currVal;
                    else if (currVal.length() == 2)
                        currVal = "00" + currVal;
                    else if (currVal.length() == 1)
                        currVal = "000" + currVal;


                    Log.d(TAG, "New Hex Value:  " + base + currVal + exctractlength);
                    String HexValue = base + currVal + exctractlength;

                    byte[] sendapdu10 = toByteArray(HexValue); //{ (byte) 0x00,(byte)  0xb0,(byte) 0x05,(byte)  0xfa,(byte)  0xff};
                    byte[] test10 = Plugin_APDU(sendapdu10, 5, 1);
                    if (test10 != null) {
                        Log.d(TAG, "Send APDU is +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));


                        Log.d(TAG, "Checking size ");
                        byte[] temp1 = (test10);

                        if ((test10.length == 2) && temp1[1] == (byte) 0x00) {
                            //if (temp1[1] == 0x00) {
                            //  Log.d(TAG,"Coming out as zero zero");
                            //}
                            Log.d(TAG, "Coming out +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));
                            Log.d(TAG, "Length is 2 bytes so coming out");
                            break;
                        } else if (test10 != null) {
                            Log.d(TAG, "10 Rcvd APDU Array is " + convertByteArrayToHexString(test10));
                            System.arraycopy(test10, 0, destination, lengthBytes, test10.length - 2);
                            //as getting 9000 at end - removing that
                            writeDataToFileNew(convertByteArrayToHexString(test10));

                            lengthBytes = lengthBytes + test10.length - 2;
                            Log.d(TAG, "Length Bytes is" + lengthBytes);
                        }
                    }
                }
                Log.d(TAG, "New Bytes:  " + lengthBytes);
                writeDataToFileNew("Total length is " + lengthBytes);
                writeDataToFileNew(convertByteArrayToHexString(destination));

                // String temp = Base64.encodeToString(destination, Base64.DEFAULT);
                //writeDataToFileNew(temp);


                SaveBinaryFile(destination);
                GlobalVariables.CardReadStatus = 2;

                Log.d(TAG, "Retrievedlednth:  " + convertByteArrayToHexString(destination));
                return true;
            } catch (Exception obj) {
                executionStatus=false;
                Log.d(TAG, "Got the Exception while reading the card ");
                return false;
                //returntogif();
            }
        }
        //Bitmap bmp = BitmapFactory.decodeByteArray(destination, 0, destination.length);

        // bmp.compress(Bitmap.CompressFormat.JPEG);
       /* try {
            if (bmp != null) {
                Log.d("Test", "Getting the file ");

                String filename = Environment.getExternalStorageDirectory() + "/displayImages/test100.jpeg";
                FileOutputStream out = new FileOutputStream(filename);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

                // bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
                //bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
            } else
                Log.d(TAG, "Bit map is null");
        } catch (Exception obj) {
            Log.d("Test", "Issue with parsing " + obj.getMessage());
        }*/
       return false;
    }

    private boolean  SelectPhotoFile() {
        Log.d(TAG, "Get the Image ");

        /*
         * Select AIU
         * Select File
         * Get the Binary File
         */
/*

        1. ‘02’ (the data field shall contain a File ID)
        2. ‘08’ (the data field shall contain an absolute path)
        3. ‘04’ (the data field shall contain an AID)
*/

        /*
        command: 00 a4 04 00 0c A0 00 00 01 77 49 64 46 69 6C 65 73
        output: 6121

        61	XX	I	Command successfully executed; ‘XX’ bytes of data are available and can be requested using GET RESPONSE.

        00 a4 02 00 02 40 35 (pHOTO fILE)

        It can be either by AID, Path, or File ID
        Above is AID
                - command:
        00 a4 02 00 02 3F 00 (mf)
                -sUCCESS IS 90 00
        00 a4 02 00 02 DF 01 (df)
        00 a4 02 00 02 40 35 (pHOTO fILE)
        */


        try {
            if (selectBelgiumEida == true) {

                //Option 1:
                byte[] belgium_apdu_aid = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x77, (byte)
                        0x49, (byte) 0x64, (byte) 0x46, (byte) 0x69, (byte) 0x6C, (byte) 0x65, (byte) 0x73};

                byte[] belgium_apdu_select_photofile = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x40, (byte) 0x35};// (Photo fILE)


                //Option 2
                byte[] belgium_apdu_selectfile_mf = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x3F, (byte) 0x00};// (mf)
                //         -sUCCESS IS 90 00
                byte[] belgium_apdu_selectfile_df = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0xDF, (byte) 0x01};// (df)
                byte[] belgium_apdu_selectfile3_photofile = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x40, (byte) 0x35};// (ID fILE)


                //option 3:
                byte[] belgium_apdu_selectfile_abspath = {(byte) 0x00, (byte) 0xa4, (byte) 0x08, (byte) 0x00, (byte) 0x06, (byte) 0x3F, (byte) 0x00, (byte) 0xDF, (byte) 0x01, (byte) 0x40, (byte) 0x35};// (Photo fILE)
                //3F00 DF01 4035

                byte[] test = Plugin_APDU(belgium_apdu_aid, belgium_apdu_aid.length, 1);
                //  byte[] test = Plugin_APDU(belgium_apdu_aid, 12, 0);

                //command: 00a404000ca00000024300130000000101
                //output:
                //6121
                //
                //command: 00a40000020202

                if (test != null) {
                    Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(belgium_apdu_aid));
                    Log.d(TAG, "length of APDU response is " + test.length);
                    //    Log.d(TAG, "Received APDU" + (test));

                    Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
                    //byte[] test2= cardReader.sendApdu(sendapdu2);
                }
                byte[] test3 = Plugin_APDU(belgium_apdu_select_photofile, belgium_apdu_select_photofile.length, 1);

                if (test3 != null)
                    Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(belgium_apdu_select_photofile));


            } else {
                //byte[] sendapdu = { (byte) 0x80,(byte)  0xC0,(byte) 0x02,(byte)  0xA1,(byte)  0x08};


                //AID - 00A404000CA00000024300130000000101
                byte[] icaapdu2_aid = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xa0, (byte) 0x00, (byte) 0x00
                        , (byte) 0x02, (byte) 0x43, (byte) 0x00, (byte) 0x13, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01};

                //select - file         00 a4 00 00 02 02 03
                byte[] apdu_selectfile = {(byte) 0x00, (byte) 0xa4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x02};

                //#define BEID_FILE_PHOTO					"3F00DF014035"

                byte[] test = Plugin_APDU(icaapdu2_aid, 12, 1);
                //  byte[] test = Plugin_APDU(belgium_apdu_aid, 12, 0);

                //command: 00a404000ca00000024300130000000101
                //output:
                //6121
                //
                //command: 00a40000020202

                if (test != null) {
                    Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(icaapdu2_aid));
                    Log.d(TAG, "length of APDU response is " + test.length);
                    //    Log.d(TAG, "Received APDU" + (test));

                    Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
                    //byte[] test2= cardReader.sendApdu(sendapdu2);
                }
                // byte[] sendapdu3 = { (byte) 0x00,(byte)  0xa4,(byte) 0x00,(byte)  0x00,(byte)  0x02, (byte)  0x02,(byte)  0x03 };
                byte[] test3 = Plugin_APDU(apdu_selectfile, 4, 1);
                Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(apdu_selectfile));

                //byte[] test4 = Plugin_APDU(belgium_apdu_selectfile1, 4, 0);
                //byte[] test5 = Plugin_APDU(belgium_apdu_selectfile2, 4, 0);
                //byte[] test6 = Plugin_APDU(belgium_apdu_selectfile3, 4, 0);

                if (test3 != null)
                    Log.d(TAG, "3 Rcvd APDU Array is " + convertByteArrayToHexString(test3));


                //00 c0 00 00 12
                //command: 00c0000012
            }
            return true;
        } catch (Exception obj) {
            executionStatus=false;
            Log.d(TAG,"Got the exception oe");
            return false;
        }
    }

    private static void writeDataToFileNew(String data) {
        try {
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "mylogs1.txt");

            Log.d(TAG, "External Storage file is " + Environment.getExternalStorageState());
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            writeDataToFile(fileOutputStream, data);
            fileOutputStream.close();
        } catch (FileNotFoundException ex) {
            Log.e("Test", ex.getMessage(), ex);
        } catch (IOException ex) {
            Log.e("Test", ex.getMessage(), ex);
        }
    }


    private static void writeDataToFile(FileOutputStream fileOutputStream, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

            Log.d(TAG, "Saving files" + data);
            bufferedWriter.write(data);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (FileNotFoundException ex) {
            Log.e("Test", ex.getMessage(), ex);
        } catch (IOException ex) {
            Log.e("Test", ex.getMessage(), ex);
        }
    }

    private static void RemoveLogFile() {
        try {
            //  long deviceupTime = ((SystemClock.elapsedRealtime() / 1000));

            //if (deviceupTime < 300)
            { //Before 5 minutes - remove that file
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "mylogs1.txt");
                File file2 = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "myImage.jpg");

                file2.delete();
                Log.d(TAG, "Removing the Log File");
                file.delete();
            }
        } catch (Exception obj) {
            Log.d(TAG, "Got the exception while removing file");
        }
    }

    private void SaveBinaryFile(byte[] bFile) {
        final String UPLOAD_FOLDER = Environment.getExternalStorageDirectory()
                + File.separator + "displayImages"+File.separator+CardDataVariables.CardNumber+".jpg";
         //       + File.separator + "displayImages"+File.separator+"996.jpg";
//995
        Log.d(TAG,"Saving Binary file as "+UPLOAD_FOLDER);
        FileInputStream fileInputStream = null;

        try {

        //    File file = new File("C:\\temp\\testing1.txt");
/*
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "myImage.jpg");

            byte[] bFile = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
*/

            //save bytes[] into a file
            writeBytesToFileClassic(bFile, UPLOAD_FOLDER);

       //     System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void writeBytesToFileClassic2(byte[] bFile, String fileDest) {

        FileOutputStream fileOuputStream = null;

        Log.d(TAG,"Saving the File"+fileDest);
        Log.d(TAG,"WriteBytestoFileClassic");

        try {
            fileOuputStream = new FileOutputStream(fileDest);
            fileOuputStream.write(bFile);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOuputStream != null) {
                try {
                    fileOuputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private static void writeBytesToFileClassic(byte[] photo, String fileDest) {



        Log.d(TAG, "Got the Face Detection" + photo.length);
        if (photo == null || photo.length <= 0) {
            Log.d(TAG, "Photo Contents are null ");
            return;
        }//if()
        //Create  a bitmap.
        //set to the imageview
//chand added below 2 commands

        // bmp.compress(Bitmap.CompressFormat.JPEG);
        try {
            Bitmap bmp = BitmapFactory.decodeByteArray(photo, 0, photo.length);
            Log.d("Test", "Getting the file ");

            String users = "996.jpg"; //995
            if (users != null) {
                Log.d(TAG, "Get User Count is " + users);

                String userID = users; //(users.get(0).get_id());

                String filename = fileDest;

                Log.d(TAG, "File name is " + filename);
                try {
                    File f = new File(filename);

                    if (f.exists())
                        f.delete();
                } catch (Exception obj) {

                }
                Log.d(TAG, "File Name is " + filename);

                FileOutputStream out = new FileOutputStream(filename, false);
					/*try {
						out.write(("").getBytes());
					} catch (Exception Obj) {
						Log.d(TAG,"Clear file exception");
					}*/
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

                Log.d(TAG, "Update File ");
                // bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
                //bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
            } else
                Log.d(TAG, "Not found user ");
        } catch (Exception obj) {
            try {
                File f = new File(fileDest);

                if (f.exists())
                    f.delete();
            } catch (Exception obj2) {

            }
            Log.d("Test", "Got the exception "+obj.getMessage());
        }
        //imageview.setImageBitmap(bmp);
     //   Log.d("data", "data from server" + data);
    }
    private void InitiateATR() {
    Log.d(TAG, "Before waiting powerUp");
 /*   new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
            //    powerUp();

*/

 try {
     Log.d(TAG, "Before  powerUp");
     byte[] test = Plugin_ATRNew(1);
     Log.d(TAG, "After  powerUp");
  /*      }
    }, 2000);
*/
 } catch (Exception obj) {

 }
}

    private void GetCardDetailsFromCard() {
        String base = "00b0";
        String offset = "0000";
        int curroffset = 0;
        int lengthtoRetrieve = 4050; //1000; //4050;


        String exctractlength = "ff";

        Log.d(TAG,"GetCardDetailsFromCard");

        byte[] totalbyte;
        byte[] currbytes = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0xff};
        int lengthBytes2 = currbytes.length;
        byte[] destination = new byte[lengthtoRetrieve + 255];

        lengthBytes2 = 0;

        try {
            //Select the ID File
            boolean status = SelectGetCardDataFile();

            if (status == true) {
                for (int i = 0; curroffset < lengthtoRetrieve; i++) {

                    curroffset = Integer.parseInt(offset, 16) + (i * 255);
                    String currVal = Integer.toHexString(curroffset);

                    if (currVal.length() == 3)
                        currVal = "0" + currVal;
                    else if (currVal.length() == 2)
                        currVal = "00" + currVal;
                    else if (currVal.length() == 1)
                        currVal = "000" + currVal;

                    Log.d(TAG, "New Hex Value:  " + base + currVal + exctractlength);
                    String HexValue = base + currVal + exctractlength;

                    byte[] sendapdu10 = toByteArray(HexValue); //{ (byte) 0x00,(byte)  0xb0,(byte) 0x05,(byte)  0xfa,(byte)  0xff};
                    byte[] test10 = Plugin_APDU(sendapdu10, 5, 1);
                    Log.d(TAG, "Send APDU is +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));

                    Log.d(TAG, "Checking size ");
                    byte[] temp1 = (test10);

                    if ((test10.length == 2) && temp1[1] == (byte) 0x00) {
                        //if (temp1[1] == 0x00) {
                        //  Log.d(TAG,"Coming out as zero zero");
                        //}
                        Log.d(TAG, "Coming out +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));
                        Log.d(TAG, "Length is 2 bytes so coming out");
                        break;
                    } else if (test10 != null) {
                        Log.d(TAG, "10 Rcvd APDU Array is " + convertByteArrayToHexString(test10));
                        System.arraycopy(test10, 0, destination, lengthBytes2, test10.length - 2);
                        //as getting 9000 at end - removing that
                        writeDataToFileNew(convertByteArrayToHexString(test10));

                        lengthBytes2 = lengthBytes2 + test10.length - 2;
                        Log.d(TAG, "Length Bytes is" + lengthBytes2);
                    }
                }
                Log.d(TAG, "New Bytes:  " + lengthBytes2);
                writeDataToFileNew("Total length is " + lengthBytes2);
                writeDataToFileNew(convertByteArrayToHexString(destination));
                //SaveBinaryFile(destination);

                if (selectBelgiumEida == false) {
                    //Log.d(TAG)
                    byte[] destination1 = GetCardDataTest(); //for Test included - remove it later

                    Log.d(TAG, "Processcarddata ");
                    ProcessCarddata(destination1);
                } else
                    ProcessCarddata(destination);
                //String temp = Base64.encodeToString(destination, Base64.DEFAULT);
                //writeDataToFileNew(temp);

                // Log.d(TAG, "Retrievedlednth:  " + convertByteArrayToHexString(destination));
            } else {
            }

        } catch (Exception obj) {
            Log.d(TAG,"Got the exception ");
        }
    }

    private boolean SelectGetCardDataFile() {

        Log.d(TAG, "Get the SelectGetCardDataFile ");

        /*
         * Select AIU
         * Select File
         * Get the Binary File
         */

        /*
        command: 00 a4 04 00 0c A0 00 00 01 77 49 64 46 69 6C 65 73
        output: 6121

        61	XX	I	Command successfully executed; ‘XX’ bytes of data are available and can be requested using GET RESPONSE.

        It can be either by AID, Path, or File ID
        Above is AID
                - command:
        00 a4 02 00 02 3F 00 (mf)
                -sUCCESS IS 90 00
        00 a4 02 00 02 DF 01 (df)
        00 a4 02 00 02 40 31 (pHOTO fILE)
        */

        try {

            if (selectBelgiumEida) {
                //option 1:
                byte[] belgium_apdu_aid = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x77, (byte)
                        0x49, (byte) 0x64, (byte) 0x46, (byte) 0x69, (byte) 0x6C, (byte) 0x65, (byte) 0x73};
                byte[] belgium_apdu_select_idfile = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0x40, (byte) 0x31};// (ID fILE)

                //option -2:
                byte[] belgium_apdu_selectfile_mf = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0x3F, (byte) 0x00};// (mf)
                //         -sUCCESS IS 90 00
                byte[] belgium_apdu_selectfile2_df = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0xDF, (byte) 0x01};// (df)
                byte[] belgium_apdu_selectfile3_idfile = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0x40, (byte) 0x31};// (ID fILE)

                //option 3
                byte[] belgium_apdu_selectfile_abspath_idfile = {(byte) 0x00, (byte) 0xa4, (byte) 0x08, (byte) 0x00, (byte) 0x06, (byte) 0x3f, (byte) 0x00, (byte) 0xdf, (byte) 0x01, (byte) 0x40, (byte) 0x31};// (ID fILE)


                byte[] test = Plugin_APDU(belgium_apdu_aid, 12, 1);


                //command: 00a404000ca00000024300130000000101
                //output:
                //6121
                //
                //command: 00a40000020202

                if (test != null) {
                    Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(belgium_apdu_aid));
                    Log.d(TAG, "length of APDU response is " + test.length);
                    //    Log.d(TAG, "Received APDU" + (test));

                    Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
                    //byte[] test2= cardReader.sendApdu(sendapdu2);
                }
                // byte[] sendapdu3 = { (byte) 0x00,(byte)  0xa4,(byte) 0x00,(byte)  0x00,(byte)  0x02, (byte)  0x02,(byte)  0x03 };
                byte[] test3 = Plugin_APDU(belgium_apdu_select_idfile, 4, 1);
                Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(belgium_apdu_select_idfile));

                //byte[] test4 = Plugin_APDU(belgium_apdu_selectfile1, 4, 0);
                //byte[] test5 = Plugin_APDU(belgium_apdu_selectfile2, 4, 0);
                //byte[] test6 = Plugin_APDU(belgium_apdu_selectfile3, 4, 0);

                if (test3 != null)
                    Log.d(TAG, "3 Rcvd APDU Array is " + convertByteArrayToHexString(test3));

            } else {
                //AID - 00A404000CA00000024300130000000101
                byte[] icaapdu2_aid = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xa0, (byte) 0x00, (byte) 0x00
                        , (byte) 0x02, (byte) 0x43, (byte) 0x00, (byte) 0x13, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01};


                //select - file         00 a4 00 00 02 02 03
                byte[] apdu_selectfile = {(byte) 0x00, (byte) 0xa4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x02};

                byte[] test = Plugin_APDU(icaapdu2_aid, 12, 1);


                if (test != null) {
                    Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(icaapdu2_aid));
                    Log.d(TAG, "length of APDU response is " + test.length);
                    //    Log.d(TAG, "Received APDU" + (test));

                    Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
                    //byte[] test2= cardReader.sendApdu(sendapdu2);
                }
                // byte[] sendapdu3 = { (byte) 0x00,(byte)  0xa4,(byte) 0x00,(byte)  0x00,(byte)  0x02, (byte)  0x02,(byte)  0x03 };
                byte[] test3 = Plugin_APDU(apdu_selectfile, 4, 1);
                Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(apdu_selectfile));

                if (test3 != null)
                    Log.d(TAG, "3 Rcvd APDU Array is " + convertByteArrayToHexString(test3));

                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got the exception obj");
            return false;
        }
        return false;
    }
    private String LOG="CardReader";
    private void ProcessData() {
        byte[] bytes = HexUtil.parseHex("50045649534157131000023100000033D44122011003400000481F");


        BerTlvParser parser = new BerTlvParser();
        BerTlvs tlvs = parser.parse(bytes, 0, bytes.length);


        //
      //  BerTlvLogger.log("    ", tlvs, LOG);

        List<BerTlv> test= tlvs.getList();
        int count = test.size();
        Log.d(TAG,"Count is "+count);
        for (int i=0;i<count;i++) {
            String Val = test.get(i).getTextValue();
            Log.d(TAG,"Value is "+Val);
            String te2=test.get(i).getHexValue();

            Log.d(TAG," TE Valis "+te2);
            BerTag to = test.get(i).getTag();
            String te = to.toString();
            Log.d(TAG,"TAG is "+to);
            Log.d(TAG,"TAG te is "+te);

        }
        /* Output was:
         Count is 2
            Value is VISA
03-13 15:13:57.785  4718  4718 D ContactCpuCardActivity:  TE Valis 56495341
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG is - 50
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG te is - 50
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: Value is À€1À€À€À€3ï¿½A"@À€À€H
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity:  TE Valis 1000023100000033D44122011003400000481F
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG is - 57
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG te is - 57
*/

        // Tag val2 = test.get(0).getTag();



      //  BerTlv a = tlvs.getList()
        byte[] bytes2 =  new BerTlvBuilder()
                .addHex(new BerTag(0x50), "56495341")
                .addHex(new BerTag(0x57), "1000023100000033D44122011003400000481F")
                .buildArray();
    }
private void ProcessCarddata(byte[] data) {
    Log.d(TAG, "Process Card Data ");
     // Tester      tlvtest =new Tester();
    Log.d(TAG, "Running the Testing ");
    // tlvtest.runTesting();


    //adf

    //TlvBox boxes = new TlvBox();
   // 39 31 39 39 38 36 39 35 36 35 38 39
   // 00  02 02 03 02 0c 39 39 38 36 39 35 36 35 38 39 35 35
  //  62 65 6c 67 69 75 6d
   // 31 30 2f 30 37 2f 31 39 37 33
    //32 36 2f 30 39 2f 32 30 33 30
 //   30 39 39 38 36 39 35 36 35 38 39

    Log.d(TAG,"Byte aray is "+convertByteArrayToHexString(data));
    Log.d(TAG,"Size is "+data.length);

    /*
    public static final int FILESCTRUCTURE_BIN_TYPE_1 = 0x00;
    public static final int CARDNUMBER_ASCII_TYPE_1 = 0x01;
    public static final int CHIP_NUMBER_BINA_TYPE_2 = 0x02;
    public static final int CARD_VALIDITY_BEGIN_ASCII_TYPE_3 = 0x03;
    public static final int CARD_VALIDITY_END_ASCII_TYPE_4 = 0x04;
    public static final int CARD_DEL_MUNSPALITY_UTF_TYPE_5 = 0x05;
    public static final int NATIONAL_NUMBER_ASCII_TYPE_6 = 0x06;
    public static final int NAME_UTF_TYPE_7 = 0x07;
    public static final int NAME_2_UTF_TYPE_8 = 0x08;
    public static final int NATIONALITY_TYPE_9 = 0x0A;
    public static final int BIRTH_LOC_TYPE_9 = 0x0B;
    public static final int BIRTHDATE_LOC_TYPE_9 = 0x0C;
    public static final int SEC_LOC_TYPE_9 = 0x0D;
*/

    try {
        Log.d(TAG, "Value of Hex is " + byte2HexStr2(data, data.length) + " len is " + data.length);
        byte[] bytes = HexUtil.parseHex(byte2HexStr2(data, data.length)); //HexUtil.parseHex("50045649534157131000023100000033D44122011003400000481F");

        BerTlvParser parser = new BerTlvParser();
        BerTlvs tlvs = parser.parse(bytes, 0, bytes.length);

        //
        //  BerTlvLogger.log("    ", tlvs, LOG);

        List<BerTlv> test = tlvs.getList();
        int count = test.size();
        Log.d(TAG, "Count is " + count);
        for (int i = 0; i < count; i++) {

            try {
                String Value = test.get(i).getTextValue();
                Log.d(TAG, "Value is " + Value + " len is " + Value.length());
                String te2 = test.get(i).getHexValue();

                Log.d(TAG, " TE Valis " + te2);
                BerTag to = test.get(i).getTag();
                String te = to.toString();
                String te1 = te.substring(te.length() - 2);
                Log.d(TAG, "te1 is " + te1);
                Log.d(TAG, "TAG is " + to);
                Log.d(TAG, "TAG te is " + te);

                int tag = Integer.parseInt(te1, 16);
                //int tag = Integer.valueOf(te1);
                Log.d(TAG, "TAG Val is " + tag);

                switch (tag) {
                    case FILESCTRUCTURE_VERSION_BIN_TYPE_1:
                        // FileVersion=Value;
                        cardData.FileVersion = Value;
                        Log.d(TAG, "File Version is " + cardData.FileVersion);
                        break;

                    case CARDNUMBER_ASCII_TYPE_12_1:
                        //CardNumber=Value;
                        cardData.CardNumber = Value;
                        Log.d(TAG, "Card Number is " + cardData.CardNumber);
                        break;

                    case CHIP_NUMBER_BINA_TYPE_16_2:
                        //ChipNumber=Value;
                        cardData.ChipNumber = Value;
                        Log.d(TAG, "Chip Number " + cardData.ChipNumber);
                        break;

                    case CARD_VALIDITY_BEGIN_ASCII_TYPE_10_3:
                        //ValidityeBegin=Value;
                        cardData.ValidityeBegin = Value;

                        Log.d(TAG, "Validity Begin " + cardData.ValidityeBegin);
                        break;

                    case CARD_VALIDITY_END_ASCII_TYPE_10_4:
                        //ValidityEnd=Value;
                        cardData.ValidityEnd = Value;
                        Log.d(TAG, "Validity End " + cardData.ValidityEnd);
                        break;

                    case CARD_DEL_MUNSPALITY_UTF_TYPE_5:
                        //CardDeliveryMunspality=Value;
                        cardData.CardDeliveryMunspality = Value;
                        Log.d(TAG, "Card Del Munsp" + cardData.CardDeliveryMunspality);
                        break;

                    case NATIONAL_NUMBER_ASCII_TYPE_11_6:
                        //NationalNumber=Value;
                        cardData.NationalNumber = Value;

                        Log.d(TAG, "Nation Number " + cardData.NationalNumber);
                        break;

                    case NAME_UTF_TYPE_110_7:
                        //Name=Value;
                        cardData.Name = Value;
                        Log.d(TAG, "Name is " + cardData.Name);
                        break;

                    case NAME_2_UTF_TYPE_95_8:
                        //Name_2=Value;
                        cardData.Name_2 = Value;
                        Log.d(TAG, "Name_2 is " + cardData.Name_2);
                        break;

                    case FIRSTLETTER_GIVENNAME3_2_UTF_TYPE_95_8:
                        //FirstLetterName=Value;
                        cardData.FirstLetterName = Value;
                        Log.d(TAG, "First Letter 3 Name " + cardData.FirstLetterName);
                        break;
                    case NATIONALITY_UTF_TYPE_85_10:
                        //Nationality=Value;
                        cardData.Nationality = Value;
                        Log.d(TAG, "Nationality " + cardData.Nationality);
                        break;

                    case BIRTH_LOC_TYPE_80_11:
                        //BirthLocation=Value;
                        cardData.BirthLocation = Value;
                        Log.d(TAG, "BirthLocation " + cardData.BirthLocation);
                        break;

                    case BIRTHDATE_TYPE_12_12:
                        //BirthDate=Value;
                        cardData.BirthDate = Value;
                        Log.d(TAG, "Birth date " + cardData.BirthDate);
                        break;

                    case SEX_ASCII_TYPE_1_14:
                        //Sex=Value;
                        cardData.Sex = Value;
                        Log.d(TAG, "Sex is" + cardData.Sex);
                        break;
                    case NOBLE_CONDITION_UTF_TYPE_1_15:
                        //NobleCondition=Value;
                        cardData.NobleCondition = Value;
                        Log.d(TAG, "Noble Condition is " + cardData.NobleCondition);
                        break;
                    case DOCUMENT_TYPE_1_15:
                        //DocumentType=Value;
                        cardData.DocumentType = Value;
                        Log.d(TAG, "Document Type is " + cardData.DocumentType);
                        break;

                    case SPECIALSTATUS_UTF_TYPE_1_16:
                        //SpecialStatus=Value;
                        cardData.SpecialStatus = Value;
                        Log.d(TAG, "Special Status is " + cardData.SpecialStatus);
                        break;

                    case HASH_PICTURE_BINARY_TYPE_1_17:
                        //HashPicture=Value;
                        cardData.HashPicture = Value;
                        break;
                    case SPECIAL_ORGNISATION_TYPE_1_19:
                        //SpecialOrganization=Value;
                        cardData.SpecialOrganization = Value;
                        Log.d(TAG, "Special originzation " + cardData.SpecialOrganization);
                        break;

                    case MEMBER_OF_FAMILY_TYPE_1_20:
                        ///MemberofFamilyType=Value;
                        cardData.MemberofFamilyType = Value;
                        Log.d(TAG, "Member of Family Type is " + cardData.MemberofFamilyType);
                        break;

                    case DATE_AND_COUNTRY_OF_PRODTECTION_TYPE_1_21:
                        //DateandCountryofProtection=Value;
                        cardData.DateandCountryofProtection = Value;
                        Log.d(TAG, "datenadCountry of protect" + cardData.DateandCountryofProtection);
                        break;
                    default:
                        Log.d(TAG, "It is Default " + tag + " & Value is " + Value);
                        break;
                }
            } catch (Exception obj) {

                Log.d(TAG, "Got the Exception while parsing " + i);
            }
        }

        GlobalVariables.CardReadStatus = 3;
        Log.d(TAG,"Able to read the Card Data successfully ");
        boolean status = cardDataUpdateDatabase();
        CardDataJson();
    } catch (Exception obj) {
        Log.d(TAG,"Got the Exception ");
    }
    }
    byte[] Plugin_APDU(byte[] tp, int len, int index) {
        byte[] retVal=null;

        retVal = cardReader.Plugin_APDU(tp,len,index);

        return retVal;
    }
    private void Init_NewPlugin1(Context cp) {
        cardReader.Init_NewPlugin(cp);
    }
    private void contactCpuPowerUp() {

        try {
            byte[] Atr = cardReader.Plugin_ATR(1);
        } catch (Exception obj) {

        }
    }
    private byte[] Plugin_ATRNew(int val) {
        byte[] retVal=null;

        try {

            byte[] Atr = cardReader.Plugin_ATR(1);
        } catch (Exception obj) {

        }
        return retVal;
    }
    private String listReaders() {
        byte[] retVal=null;

        try {
            String test = cardReader.getCardList();
            Log.d(TAG,"List Readers"+test);
        } catch (Exception obj) {

        }
        return null;
    }
    private void Close() {
        try {
            //cardReader.Close();
            cardReader.Close_plugin();
        } catch (Exception d2) {
            Log.d(TAG,"Closing the Plugin ");
        }
    }
    private byte[] GetCardDataTest() {

        Log.d(TAG,"GetCardDataTest ");

        //        (byte) CARDNUMBER_ASCII_TYPE_12_1, (byte) 0x0c, (byte) 0x39,(byte) 0x31,(byte) 0x39,(byte) 0x39,(byte) 0x38,(byte) 0x36, (byte) 0x39,(byte) 0x35,(byte) 0x36,(byte) 0x35,(byte) 0x38,(byte) 0x39,
        byte[] arr3 = {(byte) FILESCTRUCTURE_VERSION_BIN_TYPE_1, (byte) 0x02, (byte) 0x32, (byte) 0x33,
                (byte) CARDNUMBER_ASCII_TYPE_12_1, (byte) 0x0c, (byte) 0x39,(byte) 0x31,(byte) 0x39,(byte) 0x39,(byte) 0x38,(byte) 0x36, (byte) 0x39,(byte) 0x35,(byte) 0x36,(byte) 0x35,(byte) 0x38,(byte) 0x39,

                  (byte) CHIP_NUMBER_BINA_TYPE_16_2,  (byte) 0x10, (byte) 0x34, (byte) 0x33,(byte) 0x37,(byte) 0x35,(byte) 0x35,(byte) 0x31,(byte) 0x32, (byte) 0x32,(byte) 0x34,(byte) 0x39,(byte) 0x38,(byte) 0x34,
                (byte) 0x35,(byte) 0x30,(byte) 0x30,(byte) 0x32,
                (byte) CARD_VALIDITY_BEGIN_ASCII_TYPE_10_3, (byte) 0x0a, (byte) 0x31,(byte) 0x30,(byte) 0x2f,(byte) 0x30,(byte) 0x37,(byte) 0x2f, (byte) 0x31,(byte) 0x39,(byte) 0x37,(byte) 0x33,
                (byte) CARD_VALIDITY_END_ASCII_TYPE_10_4, (byte) 0x0a, (byte) 0x32,(byte) 0x36,(byte) 0x2f,(byte) 0x30,(byte) 0x39,(byte) 0x2f, (byte) 0x32,(byte) 0x30,(byte) 0x33,(byte) 0x30,
                (byte) NATIONAL_NUMBER_ASCII_TYPE_11_6, (byte) 0x0b, (byte) 0x30,(byte) 0x39,(byte) 0x39,(byte) 0x38,(byte) 0x36,(byte) 0x39, (byte) 0x35,(byte) 0x36,(byte) 0x35,(byte) 0x38,(byte) 0x39,
                (byte) NAME_UTF_TYPE_110_7, (byte) 0x05, (byte) 0x63,(byte) 0x68,(byte) 0x61,(byte) 0x6e,(byte) 0x64,
                (byte) NAME_2_UTF_TYPE_95_8, (byte)  0xd,  (byte)  0x4c, (byte)   0x61, (byte)  0x6b, (byte)  0x73, (byte)  0x68, (byte)  0x6d, (byte)  0x69, (byte)  0x20, (byte)  0x43, (byte)  0x68, (byte)  0x61, (byte)  0x6e, (byte)  0x64,
                (byte) NATIONALITY_UTF_TYPE_85_10, (byte) 0x07, (byte) 0x62,(byte) 0x65,(byte) 0x6c,(byte) 0x67,(byte) 0x69,(byte) 0x75, (byte) 0x6d,
                (byte) SEX_ASCII_TYPE_1_14, (byte) 0x01, (byte) 0x4d
        };

        return arr3;
    }

    private boolean cardDataUpdateDatabase() {

        try {
            Log.d(TAG,"in cardDataUpdateDatabase");
            UserCardDatabase appDatabase = UserCardDatabase.getAppDatabase(this);
            Log.d(TAG,"Update database cardnumber "+CardDataVariables.CardNumber);

            List<CardUser> cardUser = MainActivity_FaceandFinger.appCardDatabase.carduserDao().getAll();

            Log.d(TAG,"Current Users are "+cardUser.size());

            if (CardDataVariables.CardNumber != null) {
                CardUser currCardUser = MainActivity_FaceandFinger.appCardDatabase.carduserDao().countUsersBasedonCardNumber(CardDataVariables.CardNumber);
                if (currCardUser != null) {
                    //appDatabase.userDao().update(user1);
                    GlobalVariables.CardReadStatus = 5; //Already read these data
                    Log.d(TAG, "Data is already there - no need Enroll" + currCardUser.getCardNumber());
                    return false;
                } else {

                    CardUser data = new CardUser();
                    Log.d(TAG, "Going for Inserting card number " + CardDataVariables.CardNumber);
                    data.setCardNumber(CardDataVariables.CardNumber);
                    data.setChipNumber(CardDataVariables.ChipNumber);
                    data.setCustomerName(CardDataVariables.Name);
                    data.setCustomerName2(CardDataVariables.Name_2);
                    data.setFirstLetterName(CardDataVariables.FirstLetterName);
                    data.setNationality(CardDataVariables.Nationality);
                    data.setNationalNumber(CardDataVariables.NationalNumber);
                    data.setNobleCondition(CardDataVariables.NobleCondition);
                    data.setValidtyBegin(CardDataVariables.ValidityeBegin);
                    data.setValidityEnd(CardDataVariables.ValidityEnd);
                    appDatabase.carduserDao().insertAll(data);
                    return true;
                }
            }
            //Send to the Server
        } catch (Exception obj) {
            Log.d(TAG,"Updating Card data Got exception "+obj.getMessage());
        }
        return false;
    }
    private boolean cardDataUpdateDatabase(String userID) {

        try {
            Log.d(TAG,"in cardDataUpdateDatabase");
            Log.d(TAG,"Update database cardnumber "+userID);

            List<CardUser> cardUser = MainActivity_FaceandFinger.appCardDatabase.carduserDao().getAll();

            Log.d(TAG,"Current Users are "+cardUser.size());

            CardUser currCardUser =MainActivity_FaceandFinger.appCardDatabase.carduserDao().countUsersBasedonCardNumber(userID);
            if (currCardUser != null) {
                //appDatabase.userDao().update(user1);
                //	GlobalVariables.CardReadStatus=5; //Already read these data
                Log.d(TAG, "Data is already there - no need Enroll"+currCardUser.getCardNumber());
                return false;
            } else {
                belgiumeida.carddatabase.CardUser data = new CardUser();

                Log.d(TAG,"Going for Inserting card number "+userID);


                data.setChipNumber("1001");
                data.setCardNumber(userID);
                data.setChipNumber("1001");
                data.setCustomerName("Chand");
                data.setCustomerName2("Lakshmi Chand");
                data.setFirstLetterName("LAKS");
                data.setNationality("Indian");
                data.setNationalNumber("100");
                data.setNobleCondition("lovely");
                data.setValidtyBegin("100");
                data.setValidityEnd("100");

                MainActivity_FaceandFinger.appCardDatabase.carduserDao().insertAll(data);
                return true;
            }
            //Send to the Server
        } catch (Exception obj) {
            Log.d(TAG,"Updating Card data Got exception "+obj.getMessage());
        }
        return true;
    }
    private JSONObject CardDataJson() {
        try {

            Log.d(TAG,"Card Status is "+GlobalVariables.CardReadStatus);
            String jsonStr = "{\"name\": \"i30\", \"brand\": \"Hyundai\"}";

            String CardData = "{\"FileVersion\": \""+cardData.FileVersion+"\","+
                    "\"CardNumber\": \""+cardData.CardNumber+"\","+
                    "\"ChipNumber\": \""+cardData.ChipNumber+"\","+
                    "\"ValidityeBegin\": \""+cardData.ValidityeBegin+"\","+
                    "\"ValidityEnd\": \""+cardData.ValidityEnd+"\","+
                    "\"NationalNumber\": \""+cardData.NationalNumber+"\","+
                    "\"CardDeliveryMunispality\": \""+cardData.CardDeliveryMunspality+"\","+
                    "\"Name\": \""+cardData.Name+"\","+
                    "\"Name_2\": \""+cardData.Name_2+"\","+
                    "\"Nationality\": \""+cardData.Nationality+"\","+
                    "\"BirthLoc\": \""+cardData.BirthLoc+"\","+
                    "\"BirthDate\": \""+cardData.BirthDate+"\","+
                    "\"Sex\": \""+cardData.Sex+"\","+
                    "\"NobleCondition\": \""+cardData.NobleCondition+"\","+
                    "\"DocumentType\": \""+cardData.DocumentType+"\","+
                    "\"SpecialStatus\": \""+cardData.SpecialStatus+"\","+
                    "\"HashPicture\": \""+cardData.HashPicture+"\","+
                    "\"SpecialOrganization\": \""+cardData.SpecialOrganization+"\","+
                    "\"MemberofFamilyType\": \""+cardData.MemberofFamilyType+"\","+
                    "\"DateandCountryofProtection\": \""+cardData.DateandCountryofProtection+"\""+
                    "}";

            CardData = CardData.replaceAll("\"null\"","null");
            // convert to json object
            JSONObject json = new JSONObject(CardData);

            Log.d(TAG,"JSON Value is "+json);


            return json;
        } catch (Exception obj) {
            Log.d(TAG,"GOt the Exception "+obj.getMessage());
        }
        return null;
    }
}
