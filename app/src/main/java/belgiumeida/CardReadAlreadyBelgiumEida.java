package belgiumeida;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.luxand.facerecognition.MainActivity_FaceandFinger;

import java.io.ByteArrayOutputStream;
import java.io.File;

import lumidigm.captureandmatch.R;
import webserver.Bitmaps;
import webserver.GlobalVariables;

public class CardReadAlreadyBelgiumEida extends AppCompatActivity {

    private TextView textView,textView9,personnameID;
    private ImageView photoImage;
    private TextView  IDNumber,personName,expiryDate, Nationality; // = findViewById(R.id.ArabicName);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     {
        Log.d("Chand","It is Face ICA Testing");
        setContentView(R.layout.face_user_availableforsmartcard);

        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }

        try {

            personnameID = findViewById(R.id.personname);

            Log.d("ICA", "second Received personName is " + GlobalVariables.personName);
         } catch (Exception obj) {

        }

        try {
            photoImage = findViewById(R.id.image_success);

                IDNumber = findViewById(R.id.IDNumber);
                personName = findViewById(R.id.ArabicName);
                expiryDate = findViewById(R.id.expiryDate);
                Nationality = findViewById(R.id.nationality);

            if (CardDataVariables.CardNumber != null) {
                  IDNumber.setText("CardNumber     : "+CardDataVariables.CardNumber);
            }     else
                IDNumber.setVisibility(View.INVISIBLE);


            if (CardDataVariables.Name != null) {
                personnameID.setText("SurName           : "+ CardDataVariables.Name);//GlobalVariables.personName);
            } else  {
                String userName=GlobalVariables.UserID;
                if (userName != null && userName.contains(".jpg"))
                    userName=userName.substring(0,userName.length()-4);

                if (userName != null)
                    personnameID.setText("FirstName         : "+ userName);//GlobalVariables.personName);
                else
                    personnameID.setVisibility(View.INVISIBLE);
                //userName=
            }

            if (CardDataVariables.Name_2 != null) {
                personName.setText("FirstName         : "+CardDataVariables.Name_2);
            } else
                personName.setVisibility(View.INVISIBLE);

             if (CardDataVariables.ValidityEnd != null) {
                expiryDate.setText("ExpiryDate        : "+CardDataVariables.ValidityEnd);
            }  else
                expiryDate.setVisibility(View.INVISIBLE);

            if (CardDataVariables.Nationality != null) {
                Nationality.setText("Nationality        : "+ CardDataVariables.Nationality);
            }  else
                Nationality.setVisibility(View.INVISIBLE);

            new ProcessImage().execute();
          //  photoImage.setImageBitmap(Bitmaps.decodeSampledBitmapFromBytes(photo, 150, 150));

          //  Log.d("PublicData","Photo Height is"+photoImage.getMaxHeight()+" Width is "+photoImage.getMaxWidth());

        } catch (Exception obj) {

        }
       /* String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        String date = dateFormat.format(Calendar.getInstance().getTime());


        textView = (TextView)findViewById(R.id.textView8);
        textView.setText(currentTime);

        textView9 = (TextView)findViewById(R.id.textView9);
        textView9.setText(date);
*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                returntogif();
            }
        },4000);
        }
    }

    public void returntogif(){
        Intent intent = new Intent(this, MainActivity_FaceandFinger.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        Bundle mBundle = new Bundle();
        Log.d("FaceSucces","Current card Read Status is "+
                GlobalVariables.CardReadStatus);
        //1- initial state , 2- photo there 3 - success - 4 failure - 5 - Already Available
       /* if (GlobalVariables.CardReadStatus == 3) {
            Log.d("FaceSucces","Going for the Enrollment");
            GlobalVariables.CardReadStatus=0;
            mBundle.putString("userid", CardDataVariables.CardNumber);
            //intent.putExtra("userid", userId);
            mBundle.putBoolean("Enrolment",true);
        } else*/
        mBundle.putBoolean("Enrolment",false);
        mBundle.putBoolean("adminVerification",false);
        mBundle.putBoolean("fromBelgiumEidaActivity",true);


        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finish();

//        finish();
    }
    private class ProcessImage extends AsyncTask<String, String, byte[]> {

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected byte[] doInBackground(String... paramArrayOfString) {

            Log.d("Test","Show Image "+GlobalVariables.personName);
            //ConnectionController.closeConnection();
            Log.d("Tst","Close connection ");
            //ConnectionController.cleanup();

            try {

                if (GlobalVariables.personName != null) {
                    String filename = Environment.getExternalStorageDirectory()
                            + File.separator + "displayImages" + File.separator
                            + GlobalVariables.personName+".jpg";
                    Log.d("Success","Current filename is "+filename);

                    String filename2 = Environment.getExternalStorageDirectory()
                            + File.separator + "displayImages" + File.separator
                            + GlobalVariables.personName+"new.jpg";
                    File fp2 = new File(filename);
                    File fp = new File(filename2);
                    if (fp.exists()) {
                        Bitmap bm = BitmapFactory.decodeFile(fp.getAbsolutePath());
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Log.d("Image","File name exists retrieving");
                        bm.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
                        byte[] photo = baos.toByteArray();

                        Log.d("Image","returning the length "+photo.length);
                        if (photo == null || photo.length <= 0) {
                            return null;
                        } else {
//                return;
                            // }//if()
                            //Create  a bitmap.
                            //set to the imageview

                            // photoImage.setImageBitmap(Bitmaps.decodeSampledBitmapFromBytes(photo, 150, 150));

                            Log.d("Image","Returning Photo");
                            return photo;
                        }
                    } else
                    if (fp2.exists()) {
                        Bitmap bm = BitmapFactory.decodeFile(fp2.getAbsolutePath());
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Log.d("Image","File name exists retrieving");
                        bm.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
                        byte[] photo = baos.toByteArray();

                        Log.d("Image","returning the length "+photo.length);
                        if (photo == null || photo.length <= 0) {
                            return null;
                        } else {
                            Log.d("Image","Returning Photo");
                            return photo;
                        }
                    }
                    else
                        Log.d("Image","Filename not there ");
                }
            } catch (Exception obj) {
                return null;
            }
           // return "test";
            return null;
        }

        protected void onPostExecute(byte[] photo) {
            Log.d("Test", "on Post response is ");

            photoImage = findViewById(R.id.image_success);

            if (photo != null)
            photoImage.setImageBitmap(Bitmaps.decodeSampledBitmapFromBytes(photo, 150, 150));
        }
    }

}
