/*
 *
 * You are free to use this example code to generate similar functionality
 * tailored to your own specific needs.
 *
 * This example code is provided by Lumidigm Inc. for illustrative purposes only.
 * This example has not been thoroughly tested under all conditions.  Therefore
 * Lumidigm Inc. cannot guarantee or imply reliability, serviceability, or
 * functionality of this program.
 *
 * This example code is provided by Lumidigm Inc. “AS IS” and without any express
 * or implied warranties, including, but not limited to the implied warranties of
 * merchantability and fitness for a particular purpose.
 *
 */

package belgiumeida;

import android.util.Base64;
import android.util.Log;

class Tool
{
  public static byte[] hexStringToBytes(String hexString)
  {
    if ((hexString == null) || (hexString.equals(""))) {
      return null;
    }
    hexString = hexString.toUpperCase();
    int length = hexString.length() / 2;
    char[] hexChars = hexString.toCharArray();
    byte[] d = new byte[length];
    for (int i = 0; i < length; i++)
    {
      int pos = i * 2;
      d[i] = ((byte)(charToByte(hexChars[pos]) << 4 | charToByte(hexChars[(pos + 1)])));
    }
    return d;
  }
  
  public static char[] ToCharArray(byte[] buf)
  {
    if (buf == null) {
      return null;
    }
    char[] bb = new char[buf.length];
    for (int i = 0; i < bb.length; i++) {
      bb[i] = ((char)buf[i]);
    }
    return bb;
  }
  
  public static char[] bmpToRaw(byte[] bmpfileData)
  {
    int startloc = 0;
    int endLoc = 0;
    byte[] bmpData = new byte[307200];
    
    System.arraycopy(bmpfileData, 54, bmpData, 0, 307200);
    
    byte[] RAWModData = new byte[102400];
    byte[] RAWData = new byte[102400];
    int j = 0;
    int i = 0;
    for (j = 0; i < 307200; j++)
    {
      RAWModData[j] = bmpData[i];i += 3;
    }
    startloc = 0;
    for (endLoc = 102144; startloc < 102400; endLoc -= 256)
    {
      System.arraycopy(RAWModData, startloc, RAWData, endLoc, 256);startloc += 256;
    }
    return ToCharArray(RAWData);
  }
  
  public static char[] HexToCharArray(String text)
  {
    if (text == null) {
      return null;
    }
    try
    {
      text = text.replaceAll(" ", "");
      
      return ToCharArray(hexStringToBytes(text));
    }
    catch (Exception e)
    {
      Logger.e("Exception occured : " + e.getMessage());
    }
    return null;
  }
  
  public static String CharArrayToBase64(char[] buf)
  {
    if (buf == null) {
      return null;
    }
    return Base64.encodeToString(ToByteArray(buf), 0);
  }
  
  public static byte[] ToByteArray(char[] buf)
  {
    if (buf == null) {
      return null;
    }
    byte[] bb = new byte[buf.length];
    for (int i = 0; i < bb.length; i++) {
      bb[i] = ((byte)buf[i]);
    }
    return bb;
  }
  
  private static byte charToByte(char c)
  {
    return (byte)"0123456789ABCDEF".indexOf(c);
  }
  
  public static String byte2HexStr(byte[] src, int len)
  {
    StringBuilder stringBuilder = new StringBuilder("");
    if ((src == null) || (src.length <= 0)) {
      return null;
    }
    int n = len;
    if (len > src.length) {
      n = src.length;
    }
    for (int i = 0; i < n; i++)
    {
      int v = src[i] & 0xFF;
      String hv = Integer.toHexString(v);
      if (hv.length() < 2) {
        stringBuilder.append(0);
      }
      stringBuilder.append(hv + " ");
    }
    return stringBuilder.toString();
  }
  
  public static String byte2HexStr2(byte[] src, int len)
  {
    StringBuilder stringBuilder = new StringBuilder("");
    if ((src == null) || (src.length <= 0)) {
      return null;
    }
    int n = len;
    if (len > src.length) {
      n = src.length;
    }
    for (int i = 0; i < n; i++)
    {
      int v = src[i] & 0xFF;
      String hv = Integer.toHexString(v);
      if (hv.length() < 2) {
        stringBuilder.append(0);
      }
      stringBuilder.append(hv);
    }
    return stringBuilder.toString();
  }
  
  public static String convertByteArrayToHexString(byte[] arrayBytes)
  {
    StringBuffer stringBuffer = new StringBuffer();
    Log.d("Test","Length of bytes is "+arrayBytes.length);
    for (int i = 0; i < arrayBytes.length; i++) {
      stringBuffer.append(Integer.toString((arrayBytes[i] & 0xFF) + 256, 16)
      
        .substring(1));
    }
    return stringBuffer.toString();
  }
  
  public static byte[] toByteArray(String hexString)
  {
    int hexStringLength = hexString.length();
    byte[] byteArray = null;
    int count = 0;
    for (int i = 0; i < hexStringLength; i++)
    {
      char c = hexString.charAt(i);
      if (((c >= '0') && (c <= '9')) || ((c >= 'A') && (c <= 'F')) || ((c >= 'a') && (c <= 'f'))) {
        count++;
      }
    }
    byteArray = new byte[(count + 1) / 2];
    boolean first = true;
    int len = 0;
    for (int i = 0; i < hexStringLength; i++)
    {
      char c = hexString.charAt(i);
      int value;
      //int value;
      if ((c >= '0') && (c <= '9'))
      {
        value = c - '0';
      }
      else
      {
       // int value;
        if ((c >= 'A') && (c <= 'F'))
        {
          value = c - 'A' + 10;
        }
        else
        {
         // int value;
          if ((c >= 'a') && (c <= 'f')) {
            value = c - 'a' + 10;
          } else {
            value = -1;
          }
        }
      }
      if (value >= 0)
      {
        if (first)
        {
          byteArray[len] = ((byte)(value << 4));
        }
        else
        {
          int tmp214_212 = len; byte[] tmp214_211 = byteArray;tmp214_211[tmp214_212] = ((byte)(tmp214_211[tmp214_212] | value));
          len++;
        }
        first = !first;
      }
    }
    return byteArray;
  }
}


