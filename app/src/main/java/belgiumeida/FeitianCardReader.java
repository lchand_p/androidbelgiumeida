package belgiumeida;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.ftsafe.DK;
import com.ftsafe.readerScheme.FTException;
import com.ftsafe.readerScheme.FTReader;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import static belgiumeida.Tool.convertByteArrayToHexString;

//import static ae.emiratesid.idcard.toolkit.plugin.artisecuresmartcard.Tool.convertByteArrayToHexString;

public  class FeitianCardReader {
    static {
        //System.loadLibrary("BMReader");
       // System.loadLibrary("FeitianLib");

         // System.loadLibrary("artisecuresmartcard");
    }

    private static final String TAG = "[CardReader]";
    private static final String ACTION_USB_PERMISSION = "com.lumidigm.vcom.USB_PERMISSION";
    private static Context mContext;
    private static UsbDeviceConnection mConn = null;

    //private static final MorphoDevice morphoDevice = new MorphoDevice();
    private static String sensorName;
    private static final int INVALID_PLUGIN_CONTEXT = 196;
    private static final int PLUGIN_NOT_SUPPORTED = 301;

    public   static FTReader ftReader=null;
    public static String[] readerNames=null;

    public boolean isConnected=false;
    public  FeitianCardReader(Context context) {
        mContext = context;
        FindDevice();
    }

    public static int Open() {
        //FTReader ftReaderTest=null;
        //AppCardReaderTest test = new AppCardReaderTest();
        Log.d(TAG, "In Card Reader Open ");
        if (mConn == null || mConn.getFileDescriptor() == -1) {
            FindDevice();
            if (mConn == null || mConn.getFileDescriptor() == -1) {
                return -1;
            }
        }
        try {
          Log.d(TAG,"Before FTReader");

            //ftReader = new FTReader(mContext, mHandler, DK.FTREADER_TYPE_USB, DK.FTREADER_JAR);
            ftReader = new FTReader(mContext, null, DK.FTREADER_TYPE_USB);
            Log.d(TAG, "ftReader is opened ");

            if (ftReader != null) {
                Log.d(TAG, "FTREader is not null");
                //        ftReader = ftReader;
            } else
                Log.d(TAG, "FTReader is null");

            Log.d(TAG, "ftpReader Device Find");
            try {
                ftReader.readerFind();

                // showLog(1234, "Device Found");
                Log.d(TAG," Device Found---------->");
            } catch (FTException e) {
                Log.d(TAG,"No Device Found---------->" + e.getMessage());
            }
            //}
            int rc;

//            String[] readerNames2 = null;
            try {
                //if (ftReader != null)
                {
                    Log.d(TAG, "Read Read names ");
                    readerNames = ftReader.readerOpen(null);


                } //else
                //Log.d(TAG,"Not able to read Open ");
                if (readerNames != null)
                    //Log.d(TAG,"Read ReaderNames "+readerNames.length);
                    //if (readerNames.length >=1)
                    Log.d(TAG, "Reader Name is " + readerNames[0]);
                else
                    Log.d(TAG, "Reader Names returned null");
            } catch (Exception obj) {
                Log.d(TAG, "While Reading Names crashed " + obj.getMessage());
            }
            //For Testing
            Plugin_ATR(1);
            //rc = JNI_Open(mConn.getFileDescriptor());
        /*if (readerNames != null) {
            mConn.close();
            Log.i(TAG, "Close the  device.");
        } else {
            Log.i(TAG, "Unable to  opened. the device ");
        }*/
        } catch (Exception ofje) {
            Log.d(TAG,"Got Exception "+ofje.getMessage());
        }
        return 100;
    }
    public static int OpenNew() {
        FTReader ftReaderTest=null;
        //AppCardReaderTest test = new AppCardReaderTest();
        Log.d(TAG, "In Card Reader Open ");
        if (mConn == null || mConn.getFileDescriptor() == -1) {
            FindDevice();
            if (mConn == null || mConn.getFileDescriptor() == -1) {
                return -1;
            }
        }

        ftReaderTest = new FTReader(mContext,null,DK.FTREADER_TYPE_USB);
        if (ftReaderTest != null) {
            Log.d(TAG, "FTREader is not null");
            ftReader = ftReaderTest;
        } else
            Log.d(TAG,"FTReader is null");

        Log.d(TAG,"ftpReader ");
        try {
            ftReaderTest.readerFind();

            // showLog(1234, "Device Found");
            Log.d(TAG," Device Found---------->");
        } catch (FTException e) {
            Log.d(TAG,"No Device Found---------->" + e.getMessage());
        }
        //}
        int rc;

        String[] readerNames2=null;
        try {
            //if (ftReader != null)
            {
                Log.d(TAG,"Read Read names ");
                readerNames2 = ftReaderTest.readerOpen(null);


            } //else
            //Log.d(TAG,"Not able to read Open ");
            if (readerNames2 != null)
                //Log.d(TAG,"Read ReaderNames "+readerNames.length);
                //if (readerNames.length >=1)
                Log.d(TAG,"Reader Name is "+readerNames2[0]);
            else
                Log.d(TAG,"Reader Names returned null");
        } catch (Exception obj) {
            Log.d(TAG,"While Reading Names crashed "+obj.getMessage());
        }
         return 100;
    }

    public static void Close() {

        Log.d(TAG, "In Biometric Close ");
        //JNI_Close();
        if (mConn != null) {
            mConn.close();
            mConn = null;
        }
       // try {
            //ftReader.readerClose();
            //ftReader = null;
       // } catch (FTException e) {
      //      showLog(e.getMessage());
        //}

        Log.i(TAG, "Device closed.");
    }

    public  int Ping() {

       return 0;
    }

    // Capture - capture a composite image and its derived template. 
    public  int Capture() {

        Log.d(TAG, "In Biometric Capture ");
        int rc;
        rc = Ping();
        if (rc != 0)
            return -1;
      //  rc = JNI_Capture();
        return rc;
    }

    // Match - perform the matching algorithm on two templates.
    public int Match(byte[] tmpl1, byte[] tmpl2) {

        Log.d(TAG, "In Biometric Match ");
        int rc;
        rc = Ping();
        if (rc != 0)
            return -1;
      //  rc = JNI_Match(tmpl1, tmpl2);
        return rc;
    }
 private static void FindDevice() {

        Log.d(TAG, "In CardReader FindDevice ");

        String className = mContext.getClass().getName();
        String className2 = mContext.getClass().getCanonicalName();

        Log.d(TAG,"Current Class Name is  "+className+"  "+className2);


        UsbManager usbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);

        HashMap<String, UsbDevice> map = usbManager.getDeviceList();
        Collection<UsbDevice> coll = map.values();
        Iterator<UsbDevice> iter = coll.iterator();
        UsbDevice devFound = null;
        int count = 0;
        while (iter.hasNext()) {
            UsbDevice dev = iter.next();
            int vid = dev.getVendorId();
            int pid = dev.getProductId();
            //096e &060D - Fietien Card Reader
            //0x1fae & 0x212c - HID Finger Print
            if ((vid == 0x0525 && pid == 0x3424) ||
                    ((vid == 0x096e) && pid == 0x060D) ||
                    ((vid == 0x8086) && pid == 0x3b34))
            //        (vid == 0x1fae && pid == 0x212c))
            {
                if (count == 0)
                    devFound = dev;
                count++;
            }
        }

        if (count == 0) {
            Log.i(TAG, "No device found.");
        } else if (count == 1) {
      //  } else if (count <= 2) {
            Log.i(TAG, "Device found." + count);
            if (!usbManager.hasPermission(devFound)) {
                Intent intent = new Intent(ACTION_USB_PERMISSION);
                PendingIntent permissionIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
                IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                mContext.registerReceiver(mUsbReceiver, filter);
                usbManager.requestPermission(devFound, permissionIntent);
            } else {
                mConn = usbManager.openDevice(devFound);
                if (mConn != null)
                    Log.i(TAG, "Device FD: " + mConn.getFileDescriptor());
            }
        } else {
            Log.i(TAG, "Multiple devices not supported.");
        }
    }

    private static final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice dev = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (dev != null) {
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            Log.i(TAG, "Permission obtained to use device " + dev + ".");
                            UsbManager usbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
                            mConn = usbManager.openDevice(dev);
                            if (mConn != null) {
                                Log.i(TAG, "Device FD: " + mConn.getFileDescriptor());
                            } else {
                                Log.i(TAG, "USBManager failed to open device.");
                            }
                        } else {
                            Log.i(TAG, "Permission denied for device " + dev);
                        }
                    } else {
                        Log.i(TAG, "NULL USB device.");
                    }
                    context.unregisterReceiver(this);
                }
            }
        }
    };

    public void unregisterReciver() {
        mContext.unregisterReceiver(mUsbReceiver);
    }


    //New Plugins Added by L. Chand
    //New Pluoidgins Added by L. Chand
    public static void InitPlugin4() {
        Logger.d("Digit9::initPlugin():: enters null");
        //Init_Plugin(null);

        return;
    }
    public static void InitPlugin3() {
        Logger.d("Digit9::initPlugin():: enters null");
        //Init_Plugin(null);

        return;
    }
    //public static void InitPlugin2() {
    public static void Init_NewPlugin(Context cp) {

        //=null;
        Logger.d("CardReader:Init_NewPlugin():: enters Context");
        if ((null == cp) || (!(cp instanceof Context)))
        {
            Logger.e("CardReader::initPlugin():: error");
            return; //196;
        }

        //Context cp = (Context) cp;;
        if (cp == null) {
            Logger.d("digit9::Got Null context ");
        } else {
            Logger.d("Digit9 Valid Context");
            mContext = cp;
        }
        Log.d(TAG,"Find Deice Open");
        //Open();
        FindDevice();
        if (mConn != null)
        Log.d(TAG,"Returning File Descriptor "+mConn.getFileDescriptor());
        //Open();
       // cardStatusread();

        return; // mConn.getFileDescriptor();
    }
    public static int Init_NewPlugin1(Context cp) {

        //=null;
        Logger.d("CardReader:Init_NewPlugin():: enters Context");
        if ((null == cp) || (!(cp instanceof Context))) {
            Logger.e("CardReader::initPlugin():: error");
            return 10;
        }

        //Context cp = (Context) cp;;
        if (cp == null) {
            Logger.d("digit9::Got Null context ");
        } else {
            Logger.d("Digit9 Valid Context");
            if (mContext == null) {
                mContext = cp;
                Open();
            }
            else
                if (ftReader == null) {
                    try {
                       // ftReader = new FTReader(MainActivity.this,mHandler,DK.FTREADER_TYPE_USB);

                        //ftReader = new FTReader(mContext, mHandler, DK.FTREADER_TYPE_USB, DK.FTREADER_JAR);
                        ftReader = new FTReader(mContext, null, DK.FTREADER_TYPE_USB);
                        Log.d(TAG, "ftReader is opened ");
                    } catch (Exception obj) {
                        Log.d(TAG,"Got Exception while FTReader "+obj.getMessage());
                    }
                } else

                //{
                Log.d(TAG, "Mcontext is valid ");
/*
                try {
                    if (mContext == cp)
                        Log.d(TAG, "Received Proper Contest");
                } catch (Exception obj) {
                    Log.d(TAG, "Got the Exceptoin " + obj.getMessage());
                }*/
            //}
        }
        Log.d(TAG, "Find Deice Open");
        //    Open();
        FindDevice();
        if (readerNames != null) {
            Log.d(TAG, "Reader Name is " + readerNames[0]);

        }else
            Log.d(TAG,"No Readers");

        if (mConn != null) {
            Log.d(TAG, "Returning File Descriptor " + mConn.getFileDescriptor());
            //  Open();
            ///  USBDevicePowerOn(0);
            getCardList();
            // cardStatusread();
            return mConn.getFileDescriptor();
        } else
            return 0;
    }

      public static void ListReaders() {
      //public static String ListReaders() {

          //=null;
          Logger.d("CardReader:ListReaders():: enters Context");

          Log.d(TAG, "Find Deice Open");
          //    Open();
          FindDevice();
          if (readerNames != null) {
              Log.d(TAG, "Reader Name is " + readerNames[0]);

          }else
              Log.d(TAG,"No Readers");

          if (mConn != null)
              Log.d(TAG,"Returning File Descriptor "+mConn.getFileDescriptor());
          //  Open();
          ///  USBDevicePowerOn(0);

          String retVal = getCardList();
          Log.d(TAG,"RetVal is "+retVal);
          return;
      }

    private static void getDeviceName() {
	/*	USBManager.getInstance().initialize(context, "ae.emiratesid.taztagplugin.USB_ACTION");
		Integer nbUsbDevice = new Integer(0);

		int result = morphoDevice.initUsbDevicesNameEnum(nbUsbDevice);
		Logger.d("permission Result" + result);

		sensorName = morphoDevice.getUsbDeviceName(0);

		morphoDevice.closeDevice();*/
    }

      public static int Plugin_card_connectNew() {
          Logger.d("Digit9::connect()");

	/*	morphoDevice.openUsbDevice(sensorName, 0);
		if ((sensorName == null) || (sensorName.equals("")))
		{

			Logger.d("Sensor Name ::" + sensorName);
			return 1;
		}*/
          return 0;
      }
    public static int Plugin_card_connect(String reader) {
        Logger.d("Digit9::connect()"+reader);

	/*	morphoDevice.openUsbDevice(sensorName, 0);
		if ((sensorName == null) || (sensorName.equals("")))
		{

			Logger.d("Sensor Name ::" + sensorName);
			return 1;
		}*/
        return 0;
    }
      public static byte[] Plugin_ATR(int val) {
          Logger.d("Digit9::Plugin_ATR() "+val);
          byte[] Atr =null;
          try {

              if (ftReader != null) {
                  Atr = ftReader.readerPowerOn(1);

                  if (Atr != null)
                    Log.d(TAG, "Byte Array is " + convertByteArrayToHexString(Atr));
                  else
                      Log.d(TAG,"ReaderPowerOn No Data ");

	/*	morphoDevice.openUsbDevice(sensorName, 0);
		if ((sensorName == null) || (sensorName.equals("")))
		{

			Logger.d("Sensor Name ::" + sensorName);
			return 1;
		}*/
              } else
                  Log.d(TAG,"FTReader is null");
          } catch (Exception obj) {
            Log.d(TAG,"Got Exception while atr "+obj.getMessage());
          }
          return Atr;
      }
    public  void testCheck() {
        Logger.d("Digit9 Got Test ");
//        JNI_test();
}
     public static int Plugin_card_disconnectNew() {

        int i=1;
        try {
            Logger.d("Digit9::disconnect() Power Off index "+i);
            ftReader.readerPowerOff(i);
            //unregisterReciver();
            Close();
        } catch (Exception obj) {
            Log.d(TAG,"Got Disconnect Exception "+obj.getMessage());
        }
        //	if (null != morphoDevice) {
        //		morphoDevice.closeDevice();
        //	}
         return 0;
    }

    /*
     * native function prototypes
     */
    private static   Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            super.handleMessage(msg);
            //  TextView textView = (TextView) findViewById(R.id.textView);
            Log.d(TAG,"In Handler "+msg.what);
            String log=null;

            switch (msg.what) {
                case -1:
                    //        textView.setText("");
                    return;
                case 0:
                    log = msg.obj.toString();
                    //      textView.append("LOG---------->"+log+"\n");
                    break;
                case DK.USB_IN:
                    Log.d(TAG,"NOTICE USB Device Connected");
                    //    textView.append("NOTICE------->USB Device In\n");
				/*try {
					ftReader.readerFind();
				} catch (Exception e) {
					textView.append("NOTICE------->USB Device In------->this should not happend------>" + e.getMessage() + "\n");
				}*/
                    break;
                case DK.USB_OUT:
                    Log.d(TAG,"Notice USB Device Out ");
                    //  textView.append("NOTICE------->USB Device Out\n");
                    break;
                case DK.PCSCSERVER_LOG:
                    //  textView.append("[PcscServerLog]:"+msg.obj+"\n");
                    break;

                case DK.BT3_NEW:
                  /*  BluetoothDevice dev1 = (BluetoothDevice) msg.obj;
                    textView.append("[BT3_NEW]:"+dev1.getName()+"\n");
                    arrayForBlueToothDevice.add(dev1);
                    addSpinnerJar(dev1.getName());
*/                    break;

                case DK.BT4_NEW:
  /*                  BluetoothDevice dev2 = (BluetoothDevice) msg.obj;
                    textView.append("[BT4_NEW]:"+dev2.getName()+"\n");
                    arrayForBlueToothDevice.add(dev2);
                    addSpinnerJar(dev2.getName());
*/                    break;

                default:
                    if((msg.what & DK.CARD_IN_MASK) == DK.CARD_IN_MASK){
                        //                  textView.append("NOTICE------->" + "slot"+(msg.what%DK.CARD_IN_MASK)+":card in\n");
                        Log.d(TAG,"NOTICE------->" + "slot"+(msg.what%DK.CARD_IN_MASK)+":card in\n");
                        return;
                    }else if((msg.what & DK.CARD_OUT_MASK) == DK.CARD_OUT_MASK){
                        //                textView.append("NOTICE------->" + "slot"+(msg.what%DK.CARD_OUT_MASK)+":card out\n");
                        Log.d(TAG,"NOTICE------->" + "slot"+(msg.what%DK.CARD_OUT_MASK)+":card out\n");
                        return;
                    }
                    break;
            }
            Log.d(TAG,"Coming out of Handler ");
        }
    };
    /*public   Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            super.handleMessage(msg);
          //  TextView textView = (TextView) findViewById(R.id.textView);
            Log.d(TAG,"In Handler "+msg.what);
            String log=null;

            switch (msg.what) {
                case -1:
            //        textView.setText("");
                    return;
                case 0:
                    log = msg.obj.toString();
              //      textView.append("LOG---------->"+log+"\n");
                    break;
                case DK.USB_IN:
                    Log.d(TAG,"NOTICE USB Device Connected");
                //    textView.append("NOTICE------->USB Device In\n");
				*//*try {
					ftReader.readerFind();
				} catch (Exception e) {
					textView.append("NOTICE------->USB Device In------->this should not happend------>" + e.getMessage() + "\n");
				}*//*
                    break;
                case DK.USB_OUT:
                    Log.d(TAG,"Notice USB Device Out ");
                  //  textView.append("NOTICE------->USB Device Out\n");
                    break;
                case DK.PCSCSERVER_LOG:
                  //  textView.append("[PcscServerLog]:"+msg.obj+"\n");
                    break;

                case DK.BT3_NEW:
                  *//*  BluetoothDevice dev1 = (BluetoothDevice) msg.obj;
                    textView.append("[BT3_NEW]:"+dev1.getName()+"\n");
                    arrayForBlueToothDevice.add(dev1);
                    addSpinnerJar(dev1.getName());
*//*                    break;

                case DK.BT4_NEW:
  *//*                  BluetoothDevice dev2 = (BluetoothDevice) msg.obj;
                    textView.append("[BT4_NEW]:"+dev2.getName()+"\n");
                    arrayForBlueToothDevice.add(dev2);
                    addSpinnerJar(dev2.getName());
*//*                    break;

                default:
                    if((msg.what & DK.CARD_IN_MASK) == DK.CARD_IN_MASK){
      //                  textView.append("NOTICE------->" + "slot"+(msg.what%DK.CARD_IN_MASK)+":card in\n");
                        Log.d(TAG,"NOTICE------->" + "slot"+(msg.what%DK.CARD_IN_MASK)+":card in\n");
                        return;
                    }else if((msg.what & DK.CARD_OUT_MASK) == DK.CARD_OUT_MASK){
        //                textView.append("NOTICE------->" + "slot"+(msg.what%DK.CARD_OUT_MASK)+":card out\n");
                        Log.d(TAG,"NOTICE------->" + "slot"+(msg.what%DK.CARD_OUT_MASK)+":card out\n");
                        return;
                    }
                    break;
            }
            Log.d(TAG,"Coming out of Handler ");
        }
    };*/
    public  void cardStatusread() {
        Log.d(TAG,"Card Status Read ");
        //ftReader = new FTReader(mContext, mHandler, DK.FTREADER_TYPE_USB, DK.FTREADER_JAR);
        ftReader = new FTReader(mContext, null, DK.FTREADER_TYPE_USB);
        //ftReader = new FTReader(mContext, mHandler, DK.FTREADER_TYPE_USB);
        try {

            //ftReader.readerFind();

           // showLog(1234, "Device Found");
           // showLog(" Device Found---------->");
            Log.d(TAG," Device Found---------->");
        } catch (Exception e) {
            Log.d(TAG,"No Device Found---------->" + e.getMessage());
        }
        //usbFound();
    }
    public static void USBDevicePowerOn(int i) {

        try {

            Log.d(TAG,"USB Device Power On "+i);
            if (ftReader != null)
            ftReader.readerPowerOn(i);
            else
                Log.d(TAG,"FTReader is null");
        } catch (Exception obj)  {
            Log.d(TAG,"Got Exception while Read Power On ");
        }
    }
    public  static String getCardList() {

        try {
            if (ftReader != null) {
                String readerNames[] = ftReader.readerOpen(null);

                if (readerNames != null) {
                    int len = readerNames.length;
                    Log.d(TAG, "Reader Len is " + len + "  readernames " + readerNames[0]);

             /*       try {
                        ftReader.readerPowerOn(0);
                    } catch (Exception obj)  {
                        Log.d(TAG,"Got Exception while Read Power On ");
                    }*/
                    String retVal = null;
                    for (int i=0;i<readerNames.length;i++) {
                        if (i==0)
                        retVal =readerNames[i];
                        else retVal = retVal +readerNames[i];

                    }
                    //return readerNames[0];

                    Log.d(TAG,"RetVal is "+retVal);

                    return retVal;
                }
            } else
                Log.d(TAG,"FT Reader is null");
        } catch (Exception obj ) {
            Log.d(TAG,"Got the Reader Open Exception "+obj.getMessage());
        }
        return null;
    }

    public static void Close_plugin() {
        Log.d(TAG,"CLosing Plugin ");
    }
      private static boolean getPermission()
      {
          String permission = "android.permission.READ_PHONE_STATE";
          int res = mContext.checkCallingOrSelfPermission(permission);

          return res == 0;
      }

    /*  public static byte[] getDeviceId()
      {
          Logger.d("getDeviceId()::Called");
          getPermission();

          IMEINumber imeiNumber = new IMEINumber();
          TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService("phone");
          if (Build.VERSION.SDK_INT >= 23)
          {
              int phoneCount = telephonyManager.getPhoneCount();
              ArrayList<IMEI> imeiNumbersList = new ArrayList();
              for (int i = 0; i < phoneCount; i++)
              {
                  IMEI mImeiNumber = new IMEI();
                  mImeiNumber.setIMEINumbr(telephonyManager.getDeviceId(i));
                  imeiNumbersList.add(mImeiNumber);
              }
              imeiNumber.setImeiList(imeiNumbersList);
          }
          return imeiNumber.toString().getBytes();
      }*/

    public static int Plugin_APDU1(byte[] pdu, int len, int index) {
        Logger.d("Digit9::Plugin_APDU() index is "+index+" len is "+len);
        Logger.d("Digit9 APDU is " + pdu);
        byte[] recvVal=null;
        try {

            if (ftReader != null) {
                //DOnt PowerON for every Command, do it at first and disconnect PowerOf
                //Firt power the device
              //  byte[] Atr = ftReader.readerPowerOn(index);

                if (ftReader != null) {
                    recvVal = ftReader.readerXfr(index,pdu);
                    if (recvVal != null)
                    Log.d(TAG, "Rcvd Byte Array is " + convertByteArrayToHexString(recvVal));
                    else
                    Log.d(TAG,"Received ReaderXFR is null");
                }else {
                    Log.d(TAG, "ReaderPowerOn No Data ");
                    return 0;
                }
	        } else {
                Log.d(TAG, "FTReader is null");
                return 0;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while atr "+obj.getMessage());
        }
        return 1;
    }

    public static byte[] Plugin_APDU(byte[] pdu, int len, int index) {
        Logger.d("Digit9::Plugin_APDU() "+index+" len is "+len);
        Logger.d("Digit9::Plugin_APDU() apdu is "+pdu);
        Log.d(TAG, "Rcvd APDU Array is " + convertByteArrayToHexString(pdu));
        byte[] recvVal=null;
        try {
            if (ftReader != null) {
//DOnt PowerON for every Command, do it at first and disconnect PowerOf

                //Firt power the device
                //byte[] Atr = ftReader.readerPowerOn(index);

                if (ftReader != null) {
                    recvVal = ftReader.readerXfr(index,pdu);
                    if (recvVal != null)
                        Log.d(TAG, "Rcvd Byte Array is " + convertByteArrayToHexString(recvVal));
                    else
                        Log.d(TAG,"Received ReaderXFR is null");
                }else {
                    Log.d(TAG, "ReaderPowerOn No Data ");
                    return null;
                }
            } else {
                Log.d(TAG, "FTReader is null");
                return null;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while atr "+obj.getMessage());
        }
        return recvVal;
    }
   }
