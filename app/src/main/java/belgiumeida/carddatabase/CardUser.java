package belgiumeida.carddatabase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "carduser")
public class CardUser implements Parcelable {


  public static  String FileVersion=null;


  /*public static  String Nationality=null;
  public static  String BirthLoc=null;

  public static  String BirthLocation=null;
  public static  String BirthDate=null;
  public static  String DocumentType=null;
  public static  String SpecialStatus=null;
  public static  String HashPicture=null;
  public static  String SpecialOrganization=null;
  public static  String MemberofFamilyType=null;
  public static  String DateandCountryofProtection=null;
*/

    @PrimaryKey
  @NonNull
  @SerializedName("cardNumber")
  @ColumnInfo(name = "cardNumber")
  private  String cardNumber;

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String CardNumber) {
    Log.d("CardUser","Setting Card Number is "+CardNumber);
    this.cardNumber =CardNumber;
    Log.d("CardUser","Setting the card number ");
  }

  @Override
  public String toString() {
    return "CardUser{" +
            "cardNumber='" + cardNumber + '\'' +
            ", chipNumber='" + chipNumber + '\'' +
            ", customerName='" + customerName + '\'' +
            '}';
  }
  @NonNull
  @SerializedName("chipNumber")
  @ColumnInfo(name = "chipNumber")
  private  String chipNumber;


  @NonNull
  public String getChipNumber() {
    return chipNumber;
  }

  public void setChipNumber(@NonNull String ChipNumber) {

    Log.d("CardUser","SetChipNumber ");
    this.chipNumber = ChipNumber;
  }


  @SerializedName("customerName")
  @ColumnInfo(name = "customerName")
  private String customerName;

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String username) {
    this.customerName = username;
  }


  @SerializedName("customerName2")
  @ColumnInfo(name = "customerName2")
  private String customerName2;

  public String getCustomerName2() {
    return customerName2;
  }

  public void setCustomerName2(String username) {
    this.customerName2 = username;
  }

  @SerializedName("firstLetterName")
  @ColumnInfo(name = "firstLetterName")
  private String firstLetterName;

  public String getFirstLetterName() {
    return firstLetterName;
  }

  public void setFirstLetterName(String username) {
    this.firstLetterName = username;
  }

  @SerializedName("ValidtyBegin")
  @ColumnInfo(name = "ValidtyBegin")
  private String ValidtyBegin;

  public String getValidtyBegin() {
    return ValidtyBegin;
  }

  public void setValidtyBegin(String ValidtyBegin) {
    this.ValidtyBegin = ValidtyBegin;
  }



  @SerializedName("validityEnd")
  @ColumnInfo(name = "validityEnd")
  private String validityEnd;

  public String getValidityEnd() {
    return validityEnd;
  }

  public void setValidityEnd(String username) {
    this.validityEnd = username;
  }
    @SerializedName("nationalNumber")
  @ColumnInfo(name = "nationalNumber")
  private String nationalNumber;

  public String getNationalNumber() {
    return nationalNumber;
  }

  public void setNationalNumber(String username) {
    this.nationalNumber = username;
  }

  @SerializedName("nationality")
  @ColumnInfo(name = "nationality")
  private String nationality;

  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  @SerializedName("Sex")
  @ColumnInfo(name = "Sex")
  private String Sex;

  public String getSex() {
    return Sex;
  }

  public void setSex(String personsSex) {
    this.Sex = personsSex;
  }

  @SerializedName("NobleCondition")
  @ColumnInfo(name = "NobleCondition")
  private String NobleCondition;

  public String getNobleCondition() {
    return NobleCondition;
  }

  public void setNobleCondition(String NobleCondition) {
    this.NobleCondition = NobleCondition;
  }

  public CardUser() {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    Log.d("CardUserDuo","WritetoParcel");
    dest.writeString(this.cardNumber);
    dest.writeString(this.chipNumber);
    dest.writeString(this.customerName);
    dest.writeString(this.customerName2);
    dest.writeString(this.firstLetterName);

    dest.writeString(this.ValidtyBegin);
    dest.writeString(this.validityEnd);
    dest.writeString(this.nationalNumber);
    dest.writeString(this.nationality);
    dest.writeString(this.Sex);
    dest.writeString(this.NobleCondition);
  }

  protected CardUser(Parcel in) {

    Log.d("CardUserDuo","Reading Card User ");
    this.cardNumber = in.readString();
    this.chipNumber = in.readString();
    this.customerName = in.readString();
    this.customerName2 = in.readString();
    this.firstLetterName = in.readString();
    this.ValidtyBegin = in.readString();
    this.validityEnd = in.readString();
    this.nationalNumber = in.readString();
    this.nationality = in.readString();
    this.Sex = in.readString();
    this.NobleCondition=in.readString();
   }

  public static final Creator<CardUser> CREATOR = new Creator<CardUser>() {
    @Override
    public CardUser createFromParcel(Parcel source) {
      return new CardUser(source);
    }

    @Override
    public CardUser[] newArray(int size) {
      return new CardUser[size];
    }
  };
}