package belgiumeida.carddatabase;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import belgiumeida.carddatabase.CardUser;

@Dao
public interface CardUserDao {

    @Query("SELECT * FROM carduser")
    List<CardUser> getAll();


    @Query("SELECT * from carduser cardNumber Where :cardNumber1")
    CardUser countUsers(String cardNumber1);


    @Query("SELECT * from carduser Where cardNumber=:cardNumber1")
    CardUser countUsersBasedonCardNumber(String cardNumber1);

    @Insert
    void insertAll(CardUser... users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<CardUser> carduser);

    @Update
    void update(CardUser carduser);

    @Delete
    void delete(CardUser carduser);

    @Query("DELETE FROM carduser")
    void deleteAll();
}
