package belgiumeida;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.renderscript.Element;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
//import com.feitian.readerdk.deviceconnect.AndroidUsbDeviceConnect;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ftsafe.DK;
import com.ftsafe.Utility;
import com.ftsafe.readerScheme.FTException;
import com.ftsafe.readerScheme.FTReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import lumidigm.captureandmatch.R;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.AndroidWebServer;
import webserver.GlobalVariables;

import static belgiumeida.Tool.convertByteArrayToHexString;
import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.ftsafe.DK.USB_LOG;
import static lumidigm.constants.Constants.SERVER_IPADDR;


public class CardReaderApp extends Activity {
    private static final String TAG = "CardReaderApp";
    final static int PORT = 0x096e;

    FTReader ftReader;
    Spinner spinner;
    private TextView msg;
    private TextView submsg;

    private ImageView imageCard, tick;


    String devmodel, serialnum, devversion, devuid, timestamp;
    String[] readerNames;

    public Handler mHandler;
//GifImageView gifImageView;
    private int cardCount= 0;
    public String CardSerialNum=null;
    public String BASE_URL="http://google.co.in";
 //   private AppCardReader digit9Plugin=null;
    //private CardSerialNumberAsync cardSerialNumberAsync=null;
    private BroadcastReceiver mUpdateReceiver;
    private IntentFilter mUpdateFilter;
    private AndroidWebServer androidWebServer;
    private static boolean isStarted = false;
    private static final int DEFAULT_PORT = 8080;
    StringBuilder publicDataString =null;
    JSONObject      publicdataJson=null;

    private SharedPreferences prefs;
    private boolean WebSupport=false;
    public  boolean cardIn=false;
    public static int readerNotRespondingCount=0;
    private boolean ICASupport=false;
    private boolean AppStart=false;
    private boolean bootupstatus=false;

    /*
    This App works on the Feitian Card Reader and ICA Cards
    * Plugin ICA Card - it would read the Card Number,
    *    When that Card Number is matching, it initiates Finger Print Reader
    *       - Checks that Fingerprint is Authorized Access
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_blank);
        spinner = ((Spinner) findViewById(R.id.spinnerA_14));
        msg = (TextView) findViewById(R.id.msg);
        submsg = (TextView) findViewById(R.id.submsg);
        tick = (ImageView) findViewById(R.id.tick);
        imageCard = (ImageView) findViewById(R.id.imageCard);

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);

        try {
            Log.d(TAG, "Starting FingerScanActivity");
            if ((getIntent().getExtras()) != null) {
                bootupstatus = getIntent().getExtras().getBoolean("bootupStatus");
                Log.d(TAG, "Serial Number is " + bootupstatus);

            }  //Log.d(TAG, "mTmplCapture: "+ Arrays.toString(mTmplCapture));
        else
        bootupstatus=false;

        } catch (Exception obj) {
            bootupstatus=false;
        }
        AppStart=false;
       // ReadSSOServerAddressDetails();

        refreshDevices();
        mHandler = new HintHandler();
        cardStatusread();
       // getCardList();

        if (ICASupport) {
          //  RegisterInterReceiver();
          //  RegisterReceiver();

            //if (WebSupport)
              //  startAndroidWebServer();
        }
        if (bootupstatus) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {


                        String filePath = Environment.getExternalStorageDirectory() + "/displayImages/readytouse.mp3";
                        Log.d(TAG, "Playing file " + filePath);
                        playAudioFile(2, filePath);

                    } catch (Exception obj) {
                        Log.d(TAG, "Got Exception ");
                    }

                }
            }, 2000);
        }
    }
    public void cardIn() {
        //gifImageView.setVisibility(View.GONE);
        Log.d(TAG,"Card In");
        msg.setText("Card Inserted");
        submsg.setText("Please wait while we authenticate your card");
        imageCard.setImageResource(R.drawable.card);
        imageCard.setVisibility(View.VISIBLE);
        tick.setVisibility(View.VISIBLE);
       /* UsbManager mUsbManager = (UsbManager) mApplicationContext
                .getSystemService(Context.USB_SERVICE);
        AndroidUsbDeviceConnect androidUsbDeviceConnect = new AndroidUsbDeviceConnect(mUsbManager,)
*/

        getCardList();
        Plugin_ATR(1);

        try {
            Log.d(TAG,"Before Power On ");
            byte[] Atr = ftReader.readerPowerOn(1);

        //Follow Sequence to get the Screen ID
        //00a404000ca00000024300130000000101
        //00c0000021
        //80c002a108

     //   byte[] arr = {0x00,(byte) 0xa4,(byte) 0x4,(byte) 0x0, (byte) 0xc,(byte),0xa0,0x0, 0x00, 0x2, 0x43, 0x0, 0x13, 0x0,0x0,0x0,0x1,0xff}
        byte[] arr =  {(byte)  0x00,(byte) 0xa4,(byte) 0x04,(byte) 0x00,(byte) 0x0c,(byte) 0xa0,
                (byte) 0x00,(byte) 0x00,(byte) 0x02,(byte) 0x43,(byte) 0x00,(byte) 0x13,(byte) 0x00,(byte) 0x00,(byte) 0x00,
                (byte) 0x01,(byte) 0x01};

        Plugin_APDU(arr,17,1);

        Log.d(TAG,"Sending Next APDU ");
        byte[] arr2 = { (byte) 0x00,(byte)  0xC0,(byte) 0x00,(byte)  0x00,(byte)  0x21};
        Plugin_APDU(arr2,5,1);

        Log.d(TAG,"Sending Third APDU");
       byte[] arr3 = { (byte) 0x80,(byte)  0xC0,(byte) 0x02,(byte)  0xA1,(byte)  0x08};
        Log.d(TAG,"Before Power On ");

        //
        //


       byte[] serialNum=Plugin_APDU(arr3,5,1);

       String serialnumstr = Utility.bytes2HexStr(serialNum);
        Log.d(TAG,"After Power On  Serial # is "+serialnumstr);
        if (serialnumstr.length() > 0 )
        CardSerialNum = serialnumstr;
        Plugin_ATR_PowerOff(0);
        Log.d(TAG,"After Power Off ");
            cardSuccussNew(CardSerialNum);
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while Power On ");
            try {
                ftReader.readerClose();

            } catch (Exception obj3) {

            }
            finishAffinity();
            return;
        }

        //Plugin_ATR(1);
        /*final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                cardStatus(1);
            }
        }, 150); //10000 //150
*/


    }
    public void cardSuccuss(String cardserilNum) {
        //gifImageView.setVisibility(View.GONE);
        Log.d(TAG,"Card Success congrats");
        msg.setText("Congratulations");
        submsg.setText("Your card has been validated successfully. Please continue with your biometric validation.");
        imageCard.setImageResource(R.drawable.card);
        imageCard.setVisibility(View.VISIBLE);
        tick.setVisibility(View.GONE);

        final String cardserialnumber =cardserilNum.replace(" ","");
        Log.d(TAG,"Card Success"+cardserialnumber);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
/*                Intent i = new Intent(FirstActivity.this, FingerScannActivity.class);
                i.putExtra("cardserialnumber",cardserialnumber);
              //  i.putExtra("finger", fingerprint);
                startActivity(i);*/
                try {
                    ftReader.readerClose();
                } catch (Exception obj) {

                }
                finish();

            }
        }, 150); //1000


    }
    public void cardSuccussNew(String cardserilNum) {
        //gifImageView.setVisibility(View.GONE);
        Log.d(TAG,"Index of serialnum is "+cardserilNum.indexOf("9000"));
        String serialNum =  cardserilNum.substring(0,cardserilNum.indexOf("9000"));
        Log.d(TAG,"Card Success congrats"+serialNum);
        msg.setText("Congratulations");
        submsg.setText("Your card has been validated successfully. Please continue with your biometric validation.");
        imageCard.setImageResource(R.drawable.card);
        imageCard.setVisibility(View.VISIBLE);
        tick.setVisibility(View.GONE);

        final String cardserialnumber =serialNum.toUpperCase(); //cardserilNum.replace(" ","");
        Log.d(TAG,"Card Success"+cardserialnumber);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
            /*    Intent i = new Intent(FirstActivity.this, FingerScannActivity.class);
                i.putExtra("cardserialnumber",cardserialnumber);
                //  i.putExtra("finger", fingerprint);
                startActivity(i);*/
                try {
                    ftReader.readerClose();
                } catch (Exception obj) {

                }
                finish();

            }
        }, 150); //1000


    }


    public void cardOut() {
      //  gifImageView.setVisibility(View.VISIBLE);
        msg.setText("Insert or flash your card");
        submsg.setText("To Authenticate with Emirates ID profile");
       // imageCard.setVisibility(View.GONE);
        Log.d(TAG,"Card is Out ");
        imageCard.setImageResource(R.drawable.cardwitharrow);
        tick.setVisibility(View.GONE);
        cardIn = false;
    }

    public void validCardData() {
        tick.setVisibility(View.VISIBLE);
        msg.setText("Congratulations");
        String htmlString = "<p>Your card has been validated Successfully.<br>Please continue with your biometric validate</p>";
        submsg.setText(Html.fromHtml(htmlString));
        imageCard.setImageResource(R.drawable.card);
    }

    public void cardStatusread() {

        Log.d(TAG,"In CardStatusRead ");
        try {
            ftReader = new FTReader(this, mHandler, DK.FTREADER_TYPE_USB);
           // ftReader = new FTReader(this, mHandler, DK.FTREADER_TYPE_USB, DK.FTREADER_JAR);
            ftReader.readerFind();
            Log.d(TAG,"CardReadStatus Success");
            showLog(1234, "Device Found");
            showLog(" Device Found---------->");
        } catch (FTException e) {
            //Chand Crashing

            showLog("No Device Found---------->" + e.getMessage());
            Log.d(TAG,"No Device Found");
            Log.d(TAG,"Reboot device as No Device Found ");
            try {
                Intent i=new Intent(this, CardReaderApp.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);

                finishAffinity();
                //commented by Chand on 17 May 2017 for oscope device restarting periodically
                //stopSelf();
                //System.exit(0);
              //  Runtime.getRuntime()
                //        .exec("reboot" );
            } catch (Exception ex) {

                ex.printStackTrace();
            }

        }

        //  usbFound();

    }

    public void usbFound() {
        try {

            Log.d(TAG,"USB Found ");
            try {
                readerNames = ftReader.readerOpen(null);
            } catch (Exception obj) {
                try {
                    Log.d(TAG,"Reboot as reader Open Failed ");
                    finishAffinity();

                } catch (Exception ex) {

                    ex.printStackTrace();
                }

                Log.d(TAG,"readerOpen Failed Restarting App");
            }
            //Sometimes - Crashes here
            for (int i = 0; i < readerNames.length; i++) {
                Log.d(TAG, "onClick: " + readerNames[i]);
            }
            updateSpinnerA_14(readerNames
            );


            ///////////////////////////////////////////////////////////////////////////////////
            try {
                int type = ftReader.readerGetType();
                switch (type) {
                    case DK.READER_R301E:
                        showLog("ReaderType:R301E");
                        break;
                    case DK.READER_BR301FC4:
                        showLog("ReaderType:BR301FC4");
                        break;
                    case DK.READER_BR500:
                        showLog("ReaderType:Br500");
                        break;
                    case DK.READER_R502_CL:
                        showLog("ReaderType:R502-CL");
                        break;
                    case DK.READER_R502_DUAL:
                        showLog("ReaderType:R502-Dual");
                        break;
                    case DK.READER_BR301:
                        showLog("ReaderType:bR301");
                        break;
                    case DK.READER_IR301_LT:
                        showLog("ReaderType:iR301-LT");
                        break;
                    case DK.READER_IR301:
                        showLog("ReaderType:iR301");
                        break;
                    case DK.READER_VR504:
                        showLog("ReaderType:VR504");
                        break;
                    default:
                        showLog("ReaderType:Unknow");
                        break;
                }

                ///////////////////////////////////////////////////////////////////////////////////
                //Chand - Crashing while reading - readrGetSerialNumber()
                try {
                    byte[] serial = ftReader.readerGetSerialNumber();
                    Log.d(TAG, "onClick: " + Utility.bytes2HexStr(serial));
                    showLog("SerialNumber:" + Utility.bytes2HexStr(serial));
                    serialnum = Utility.bytes2HexStr(serial);
                    readerNotRespondingCount = 0;
                } catch (Exception e) {
                    showLog("GetSerialNum ERROR-----> Reboot the device " + e.getMessage());
                    Log.d(TAG,"Open Device Failed Restarting App as readerGetSlotStatus Failed ");
                    finishAffinity();
                    readerNotRespondingCount++;
                   if (readerNotRespondingCount > 4) {
                       Runtime.getRuntime()
                               .exec("reboot");
                   } else
                   System.exit(0);
                }

                ///////////////////////////////////////////////////////////////////////////////////
                try {

                    String ver = ftReader.readerGetFirmwareVersion();


                    showLog("FirmwareVerson:" + ver);
                    devversion = ver;

                    Log.d(TAG, "onClick: " + ver);
                } catch (Exception e) {
                    showLog("GetFirmwareVersion Failed------->" + e.getMessage());
                }
                ///////////////////////////////////////////////////////////////////////////////////
                try {
                    byte[] uid = ftReader.readerGetUID();
                    showLog("UID:" + Utility.bytes2HexStr(uid));
                    Log.d(TAG, "onClick: " + Utility.bytes2HexStr(uid));
                    devuid = Utility.bytes2HexStr(uid);
                } catch (Exception e) {
                    showLog("GetUID Failed--------------->" + e.getMessage());
                }

                ///////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////

               /* try {

                    byte[] atr = ftReader.readerPowerOn(0);
                    Log.d(TAG, "MainActivity power onClick: " + Utility.bytes2HexStr(atr));
                    showLog("ATR : " + Utility.bytes2HexStr(atr));
                } catch (Exception e) {
                    showLog("Poweron Failed-------->" + e.getMessage());
                }*/
//Chand added temporary
                cardStatus(1);

            } catch (Exception e) {
                showLog("GetType Failed------------>" + e.getMessage());
            }


        } catch (Exception e2) {
            showLog("Open Device Failed------------>" + e2.getMessage());

            //Chand can be restarted the device
            /*Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finishAffinity();*/
          /*  try {
                Log.d(TAG,"Open Device Failed Restarting App as readerGetSlotStatus Failed ");
                finishAffinity();
                //commented by Chand on 17 May 2017 for oscope device restarting periodically
                //stopSelf();
                //System.exit(0);
                Runtime.getRuntime()
                        .exec("reboot" );
            } catch (Exception ex) {

                ex.printStackTrace();
            }*/



        }
    }

    public void cardStatus(int val) {

        try {
            Log.d(TAG,"Getting the Card Status ");
            int index = getSpinnerA_14();
            devmodel = readerNames[index];
            Log.d(TAG," Get Spinnder Index"+index+" Name "+devmodel);
            int status  = val;
            //Chand added this temporarily remove it
            //if (val == 1)
              //  status = DK.CARD_PRESENT_ACTIVE;
            //else
            try {
                status = ftReader.readerGetSlotStatus(index);

                try {
                    //Chand Get all the Card Status
                    Log.d(TAG, "Spinner Max Count is " + cardCount);

                    Log.d(TAG,"Card Present ID"+DK.CARD_PRESENT_ACTIVE+ " Inactive ID "+DK.CARD_PRESENT_INACTIVE+
                            "No Card ID "+DK.CARD_NO_PRESENT);

                    //Last 2 are Junk IDs and getting as card Present, so removed them
                    if  (cardCount == 4)
                        cardCount=cardCount-2;

                    for (int i=0; i < cardCount; i++) {
                        int currstatus2 = ftReader.readerGetSlotStatus(i);
                        String devmodel2 = readerNames[i];
                        Log.d(TAG,"Spinner index "+i+" Status is "+currstatus2+" Its model is"+devmodel2);

                        if (currstatus2 == DK.CARD_PRESENT_ACTIVE) {
                            status = currstatus2;
                            devmodel = readerNames[i];
                            Log.d(TAG,"Final Status is"+status+"  Dev model"+devmodel);
                            break;
                        } else
                            if (currstatus2 == DK.CARD_PRESENT_INACTIVE) {
                                status = currstatus2;
                                devmodel = readerNames[i];
                                Log.d(TAG,"InActive Status is"+status+"  Dev model"+devmodel);
                            }

                    }
                } catch (Exception obj) {
                    Log.d(TAG,"Got Exception while reading spinners");
                    finishAffinity();
                    //System.exit(0);
                 /*   Intent i=new Intent(FirstActivity.this, FirstActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(i);
                 */   return;
                }
                //Crashing here
            } catch (Exception obj) {
             /*   Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                */

             Log.d(TAG,"Reboot device as GetSlotStatus Failed ");
           /*     try {
                    finishAffinity();
                    //commented by Chand on 17 May 2017 for oscope device restarting periodically
                    //stopSelf();
                    //System.exit(0);
                    Runtime.getRuntime()
                            .exec("reboot" );

                    Log.d(TAG,"Device got rebooted ");
                } catch (Exception ex) {

                    ex.printStackTrace();
                }
*//*
                finishAffinity();
               // System.exit(0);
                Intent i=new Intent(FirstActivity.this, FirstActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);
            */    Log.d(TAG,"Restarting App as readerGetSlotStatus Failed ");
                return;
            }

            Log.d(TAG, "cardStatus: "+status);

            Log.d(TAG," Get Spinnder Index"+index+" Name "+devmodel+"card status"+status);

            switch (status) {
                //Chand added CARD_PRESENT_INACTIVE here -as card Plugin not getting eveing ACTIVE
                case DK.CARD_PRESENT_INACTIVE:
                    showLog("SlotStatus: card present and no powered IN_ACTIVE");
                    //break;
                case DK.CARD_PRESENT_ACTIVE:
                    Log.d(TAG,"CardStatus is PRESENT_ACTIVE "+CardSerialNum);

                    Log.d(TAG,"Card In Status is "+cardIn);
                    if (cardIn == true) {
                        Log.d(TAG,"Card Already In - so No Progress ");
                    } else {
                        cardIn = true;
                        //if (!ICASupport)
                        {
                            cardIn();
                         Log.d(TAG,"Not ICA SUpport");
                        } /*else
                        if (!WebSupport)
                            Start_EIDAToolkit();
                        else
                            Log.d(TAG, "o is enabled so not starting EIDAToolkit ");*/
                        // String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
                        // String url = (BASE_URL + "devmodel=" + devmodel + "&serialnum=" + CardSerialNum + "&devversion=" + devversion + "&devuid=" + devuid + "&timestamp=" + timeStamp) .replaceAll(" ","%20");
//String url ="http://digit9.sismatik.com/emirates_api/saveandroid.php?devmodel=Philips&serialnum=32568&devversion=12&devuid=6&timestamp=12032018";
                        //Log.d(TAG, "cardStatus: "+url);
                 /*   new VolleyService(new IResult() {
                        @Override
                        public void notifySuccess(String requestType, Object response) {

                            Log.d(TAG, "notifySuccess: "+requestType);
                            JSONObject jsonObject = (JSONObject) response;
                            if(jsonObject.has("records")) {
                                try {
                                    JSONArray jsonObject1 = jsonObject.getJSONArray("records");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject(0);
                                    String fingerprint = jsonObject2.getString("user_auth_key");
                                    Log.d(TAG, "notifySuccess:@@@@@@@@@@@@@@@@ "+fingerprint);
                                  cardSuccuss(fingerprint);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }


                        }

                        @Override
                        public void notifyError(String requestType, VolleyError error) {
                            Log.d(TAG, "notifyError: "+error.getMessage());
                            try {
                                String fingerprint = "testfingerprint";
                                Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ "+fingerprint);
                                //just for testing
                                //cardSuccuss(fingerprint);
                                String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                                playAudioFile(2,filePath);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }, this).tokenBase(GET, url, null, "100");
                   */
                        showLog("SlotStatus: card present and powered");
                    }
                    break;
                /*case DK.CARD_PRESENT_INACTIVE:
                    showLog("SlotStatus: card present and no powered IN_ACTIVE");
                    break;*/
                case DK.CARD_NO_PRESENT:
                    showLog("SlotStatus: card CARD_NO_PRESENT");
                    cardOut();
                    break;

            }
        } catch (Exception e) {
            showLog("GetSlotStatus failed---------->" + e.getMessage());
        }

    }
    public void cardStatusOrig(int val) {

        try {
            Log.d(TAG,"Getting the Card Status ");
            int index = getSpinnerA_14();
            devmodel = readerNames[index];
            Log.d(TAG," Get Spinnder Index"+index+" Name "+devmodel);
            int status  = val;
            //Chand added this temporarily remove it
            //if (val == 1)
            //  status = DK.CARD_PRESENT_ACTIVE;
            //else
            try {
                status = ftReader.readerGetSlotStatus(index);



                try {
                    //Chand Get all the Card Status
                    Log.d(TAG, "Spinner Max Count is " + cardCount);

                    Log.d(TAG,"Card Present ID"+DK.CARD_PRESENT_ACTIVE+ " Inactive ID "+DK.CARD_PRESENT_INACTIVE+
                            "No Card ID "+DK.CARD_NO_PRESENT);

                    for (int i=0; i < cardCount; i++) {
                        int status2 = ftReader.readerGetSlotStatus(i);
                        String devmodel2 = readerNames[i];
                        Log.d(TAG,"Spinner index "+i+" Status is "+status2+" Its model is"+devmodel2);

                    }
                } catch (Exception obj) {
                    Log.d(TAG,"Got Exception while reading spinners");
                }
                //Crashing here
            } catch (Exception obj) {
             /*   Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                */

                Log.d(TAG,"Reboot device as GetSlotStatus Failed ");
           /*     try {
                    finishAffinity();
                    //commented by Chand on 17 May 2017 for oscope device restarting periodically
                    //stopSelf();
                    //System.exit(0);
                    Runtime.getRuntime()
                            .exec("reboot" );

                    Log.d(TAG,"Device got rebooted ");
                } catch (Exception ex) {

                    ex.printStackTrace();
                }
*/

                Log.d(TAG,"Restarting App as readerGetSlotStatus Failed ");
                return;
            }

            Log.d(TAG, "cardStatus: "+status);

            Log.d(TAG," Get Spinnder Index"+index+" Name "+devmodel+"card status"+status);

            switch (status) {
                case DK.CARD_PRESENT_ACTIVE:
                    Log.d(TAG,"CardStatus is PRESENT_ACTIVE");
                    String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
                    String url = (BASE_URL + "devmodel=" + devmodel + "&serialnum=" + serialnum + "&devversion=" + devversion + "&devuid=" + devuid + "&timestamp=" + timeStamp) .replaceAll(" ","%20");
//String url ="http://digit9.sismatik.com/emirates_api/saveandroid.php?devmodel=Philips&serialnum=32568&devversion=12&devuid=6&timestamp=12032018";
                    Log.d(TAG, "cardStatus: "+url);
                 /*   new VolleyService(new IResult() {
                        @Override
                        public void notifySuccess(String requestType, Object response) {

                            Log.d(TAG, "notifySuccess: "+requestType);
                            JSONObject jsonObject = (JSONObject) response;
                            if(jsonObject.has("records")) {
                                try {
                                    JSONArray jsonObject1 = jsonObject.getJSONArray("records");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject(0);
                                    String fingerprint = jsonObject2.getString("user_auth_key");
                                    Log.d(TAG, "notifySuccess:@@@@@@@@@@@@@@@@ "+fingerprint);
                                    cardSuccuss(fingerprint);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }


                        }

                        @Override
                        public void notifyError(String requestType, VolleyError error) {
                            Log.d(TAG, "notifyError: "+error.getMessage());
                            try {
                                String fingerprint = "testfingerprint";
                                Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ "+fingerprint);
                                //just for testing
                                //cardSuccuss(fingerprint);
                                String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                                playAudioFile(2,filePath);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }, this).tokenBase(GET, url, null, "100");
                   */
                 showLog("SlotStatus: card present and powered");
                    break;
                case DK.CARD_PRESENT_INACTIVE:
                    showLog("SlotStatus: card present and no powered IN_ACTIVE");
                    break;
                case DK.CARD_NO_PRESENT:
                    showLog("SlotStatus: card CARD_NO_PRESENT");
                    cardOut();
                    break;

            }
        } catch (Exception e) {
            showLog("GetSlotStatus failed---------->" + e.getMessage());
        }

    }
    private void updateSpinnerA_14(String[] member) {
        ArrayAdapter<String> arr_adapter;
        ArrayList<String> arrayList = new ArrayList<String>();
        cardCount = member.length;
        Log.d(TAG,"Count of the Cards are "+cardCount);
        for (int i = 0; i < member.length; i++) {

            arrayList.add(member[i]);
        }
        arr_adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, arrayList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(R.id.cust_view);
                text.setTextColor(Color.WHITE);
                cardStatus(1);
                return view;
            }
        };
        spinner.setAdapter(arr_adapter);
       /* spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                cardStatus();
            }
        });*/
       try {
         //  int selectionPosition = arr_adapter.getPosition("Feitian R502 Contactless Reader");
           int selectionPosition = arr_adapter.getPosition("Feitian R502 Contact Reader");
           Log.d(TAG,"Selection Position is "+selectionPosition);
           spinner.setSelection(selectionPosition);

       } catch (Exception ob) {
           Log.d(TAG,"Setting Spiner exception ");
       }
        Log.d(TAG, "updateSpinnerA_14: ");
    }

    private int getSpinnerA_14() throws Exception {
        int index = spinner.getSelectedItemPosition();
        if (index < 0) {
            throw new Exception("spinner34 Not Selected");
        }
        Log.d(TAG,"GetSpinner index is "+index);

        return index;
    }

    private void showLog(String log) {
        mHandler.sendMessage(mHandler.obtainMessage(0, log));
    }

    private void showLog(int wat, String log) {
        mHandler.sendMessage(mHandler.obtainMessage(wat, log));
    }

    public class HintHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.d(TAG, "handleMessage:11 " + msg.what);
            Log.d(TAG, "handleMessage:12 " + msg.obj);


            String log = null;

            switch (msg.what) {
                case -1:
                    // textView.setText("");
                    return;
                case 0:
                    log = msg.obj.toString();
                    break;
                case DK.USB_IN:
                    Log.d(TAG,"DK.USB_IN");
                    //Chand commented
                // if (!WebSupport)
                      usbFound();
                    return;
                case USB_LOG:
                    Log.d(TAG, "USB_LOG handleMessage: " + "usbin");
                     return;
                case 1234:
                    Log.d(TAG,"USB 1234");
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            usbFound();
                        }
                    }, 3000);


                    return;
                case DK.USB_OUT:
                    Log.d(TAG,"DK_USB_OUT");
                    // textView.append("NOTICE------->USB Device Out\n");
                    // updateSpinnerA_14(new String[0]);
                    try {
                        ftReader.readerClose();
                    } catch (FTException e) {
                        showLog(e.getMessage());
                    }
                    try {
                        try {
                            //new FTReaderTest(PORT).SCardListReaders();
                        } catch (Exception e) {
                            showLog("button61 TEST---------->" + e.getMessage());
                        }
                        ftReader.readerPowerOff(0);
                    } catch (Exception e) {
                        showLog("PowerOff Failed--------------->" + e.getMessage());
                    }

                    return;
                case DK.PCSCSERVER_LOG:
                    // textView.append("[PcscServerLog]:"+msg.obj+"\n");
                    return;
                case DK.BT3_NEW:
                    Log.d(TAG,"Got DK_BT3_NEW");
                    BluetoothDevice device = (BluetoothDevice) msg.obj;
                    // textView.append("[BT3_NEW]:"+device.getName()+"\n");
                   /* if(device.getName()!=null && device.getName().startsWith("FT_")){
                        arrayForBlueToothDevice.add(device);
                        addSpinnerC_14(device.getName()+" "+device.getAddress());
                    }*/
                    return;
                case DK.BT4_NEW:
                    Log.d(TAG,"Got DK_BT4_NEW");
                    BluetoothDevice mDevice = (BluetoothDevice) msg.obj;
                    //textView.append("[BT4_NEW]:"+mDevice.getName()+"\n");
                  /*  if(mDevice.getName()!=null && mDevice.getName().startsWith("FT_")){
                        arrayForBlueToothDevice.add(mDevice);
                        addSpinnerD_14("["+mDevice.getName()+"]"+mDevice.getAddress());
                    }*/
                    return;
                default:
                    if ((msg.what & DK.CARD_IN_MASK) == DK.CARD_IN_MASK) {
                        Log.d(TAG, "CARD_IN_MASK handleMessage: " + "card in");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Log.d(TAG,"Card In ");
                              if (cardIn) {
                                  Log.d(TAG,"It is already Card In ");
                              } else
                                if (!WebSupport)
                                  cardIn();
                              else
                                  Log.d(TAG,"Not going to Card In");
                            }
                        });

                        // textView.append("NOTICE------->" + "slot"+(msg.what+" "+DK.CARD_IN_MASK)+":card in\n");
                        return;
                    } else if ((msg.what & DK.CARD_OUT_MASK) == DK.CARD_OUT_MASK) {
                        Log.d(TAG, "CARD_OUT_MASK handleMessage: " + "card out");
                        cardOut();
                        //  textView.append("NOTICE------->" + "slot"+(msg.what+" "+DK.CARD_OUT_MASK)+":card out\n");
                        return;
                    }

                    break;
            }
            if (log == null) {
                //  textView.append("OBJ---------->"+"what:"+msg.what+"   obj:"+msg.obj+"\n");
            } else {
                // textView.append("LOG---------->"+log+"\n");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (ICASupport) {
                //UnRegisterReceiver();
                //stopAndroidWebServer();
            }
            ftReader.readerClose();
            //ftReader
        } catch (FTException e) {
            e.printStackTrace();
        }
    }


    private void refreshDevices()
    {
        try {
            UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
            // Get the list of attached devices
            HashMap<String, UsbDevice> devices = manager.getDeviceList();

            //  txtBox.setText("");
            //txtBox.setText("Number of devices: " + devices.size() + "\n");

            // Iterate over all devices
            Iterator<String> it = devices.keySet().iterator();
            Log.d(TAG, "Checking Number of devices");
            while (it.hasNext()) {
                String deviceName = it.next();
                UsbDevice device = devices.get(deviceName);

                String VID = Integer.toHexString(device.getVendorId()).toUpperCase();
                String PID = Integer.toHexString(device.getProductId()).toUpperCase();
                Log.d(TAG, deviceName + "Number of devices " + VID + ":" + PID + " " + manager.hasPermission(device) + "\n");
            }
        } catch (Exception obj) {

        }
    }
    private void playAudioFile(int Request, String mediafile) {
        try {
            Log.d(TAG,"Play Audio File "+Request+" Mediafile"+mediafile);

            MediaPlayer mp = new MediaPlayer();
            switch (Request) {
                //For Success
                case 1:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    break;

                case 2:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    break;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while play Audio "+obj.toString());
        }
    }

    public  void cardStatusread2() {
        Log.d(TAG,"Card Status Read ");
       // ftReader = new FTReader(this, mHandler, DK.FTREADER_TYPE_USB, DK.FTREADER_JAR);
        ftReader = new FTReader(this, mHandler, DK.FTREADER_TYPE_USB);
        //ftReader = new FTReader(mContext, mHandler, DK.FTREADER_TYPE_USB);
        try {

            //ftReader.readerFind();

            // showLog(1234, "Device Found");
            // showLog(" Device Found---------->");
            Log.d(TAG," Device Found---------->");
        } catch (Exception e) {
            Log.d(TAG,"No Device Found---------->" + e.getMessage());
        }
        //usbFound();
    }
    public   void USBDevicePowerOn(int i) {

        try {

            Log.d(TAG,"USB Device Power On "+i);
            if (ftReader != null)
                ftReader.readerPowerOn(i);
            else
                Log.d(TAG,"FTReader is null");
        } catch (Exception obj)  {
            Log.d(TAG,"Got Exception while Read Power On ");
        }
    }
    public    String getCardList() {

        Log.d(TAG,"Get Card List ");
        try {
            if (ftReader != null) {
                String readerNames[] = ftReader.readerOpen(null);

                if (readerNames != null) {
                    int len = readerNames.length;
                    Log.d(TAG, "Reader Len is " + len + "  readernames " + readerNames[0]);

             /*       try {
                        ftReader.readerPowerOn(0);
                    } catch (Exception obj)  {
                        Log.d(TAG,"Got Exception while Read Power On ");
                    }*/
                    String retVal = null;
                    for (int i=0;i<readerNames.length;i++) {
                        if (i==0)
                            retVal =readerNames[i];
                        else retVal = retVal +readerNames[i];

                    }
                    //return readerNames[0];

                    Log.d(TAG,"RetVal is "+retVal);

                    return retVal;
                }
            } else
                Log.d(TAG,"FT Reader is null");
        } catch (Exception obj ) {
            Log.d(TAG,"Got the Reader Open Exception "+obj.getMessage());
        }
        return null;
    }

    public static void Close_plugin() {
        Log.d(TAG,"CLosing Plugin ");
    }

    public  int Plugin_APDU1(byte[] pdu, int len, int index) {
        Log.d(TAG, "Digit9::Plugin_APDU() index is "+index+" len is "+len);
        Log.d(TAG, "Digit9 APDU is " + pdu);
        byte[] recvVal=null;
        try {

            if (ftReader != null) {
                //DOnt PowerON for every Command, do it at first and disconnect PowerOf
                //Firt power the device
                //  byte[] Atr = ftReader.readerPowerOn(index);

                if (ftReader != null) {
                    recvVal = ftReader.readerXfr(index,pdu);
                    Log.d(TAG,"Received Reader Output is  "+recvVal.length);
                    if (recvVal != null)
                        Log.d(TAG, "Rcvd Byte Array is " + convertByteArrayToHexString(recvVal));
                    else
                        Log.d(TAG,"Received ReaderXFR is null");
                }else {
                    Log.d(TAG, "ReaderPowerOn No Data ");
                    return 0;
                }
            } else {
                Log.d(TAG, "FTReader is null");
                return 0;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while atr "+obj.getMessage());
            try {
                ftReader.readerClose();
            } catch (Exception obj2) {
                Log.d(TAG,"Reader Close Error ");
            }
            finishAffinity();
            Log.d(TAG,"Restarting App ");
        }
        return 1;
    }
    public  int Plugin_ATR(int val) {
        Log.d(TAG,"Digit9::Plugin_ATR() "+val);

        try {

            if (ftReader != null) {
                byte[] Atr = ftReader.readerPowerOn(val);
                ftReader.readerPowerOff(val);
                if (Atr != null)
                    Log.d(TAG, "Byte Array is " + convertByteArrayToHexString(Atr));
                else
                    Log.d(TAG,"ReaderPowerOn No Data ");

	/*	morphoDevice.openUsbDevice(sensorName, 0);
		if ((sensorName == null) || (sensorName.equals("")))
		{

			Logger.d("Sensor Name ::" + sensorName);
			return 1;
		}*/
            } else
                Log.d(TAG,"FTReader is null");
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while atr "+obj.getMessage());
          /*  finishAffinity();
        //    System.exit(0);
            Intent i=new Intent(FirstActivity.this, FirstActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(i);*/
        }
        return 0;
    }
    public  int Plugin_ATR_PowerOff(int val) {
        Log.d(TAG,"Digit9::Plugin_ATR()  Power Off "+val);

        try {

            if (ftReader != null) {
                ftReader.readerPowerOff(val);


	/*	morphoDevice.openUsbDevice(sensorName, 0);
		if ((sensorName == null) || (sensorName.equals("")))
		{

			Logger.d("Sensor Name ::" + sensorName);
			return 1;
		}*/
            } else
                Log.d(TAG,"FTReader is null");
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while atr "+obj.getMessage());
        }
        return 0;
    }

    public  byte[] Plugin_APDU(byte[] pdu, int len, int index) {
        Log.d(TAG, "Digit9::Plugin_APDU() "+index+" len is "+len);
        Log.d(TAG, "Digit9::Plugin_APDU() apdu is "+pdu);
        Log.d(TAG, "Rcvd APDU Array is " + convertByteArrayToHexString(pdu));
        byte[] recvVal=null;
        try {
            if (ftReader != null) {
//DOnt PowerON for every Command, do it at first and disconnect PowerOf

                //Firt power the device
              //  byte[] Atr = ftReader.readerPowerOn(index);

                if (ftReader != null) {
                    recvVal = ftReader.readerXfr(index,pdu);

                    Log.d(TAG,"Received Val length is "+recvVal.length);

                     if (recvVal != null)
                        Log.d(TAG, "Rcvd Byte Array is " + convertByteArrayToHexString(recvVal));
                    else
                        Log.d(TAG,"Received ReaderXFR is null");
                }else {
                    Log.d(TAG, "ReaderPowerOn No Data ");
                    return null;
                }
            } else {
                Log.d(TAG, "FTReader is null");
                return null;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while atr "+obj.getMessage());
            try {
                ftReader.readerClose();
            } catch (Exception obj3) {

            }
            finishAffinity();

            Log.d(TAG,"Restarting APp ");
        }
        return recvVal;
    }
    /*public static String convertByteArrayToHexString(byte[] arrayBytes)
    {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xFF) + 256, 16)

                    .substring(1));
        }
        return stringBuffer.toString();
    }

    public static byte[] toByteArray(String hexString)
    {
        int hexStringLength = hexString.length();
        byte[] byteArray = null;
        int count = 0;
        for (int i = 0; i < hexStringLength; i++)
        {
            char c = hexString.charAt(i);
            if (((c >= '0') && (c <= '9')) || ((c >= 'A') && (c <= 'F')) || ((c >= 'a') && (c <= 'f'))) {
                count++;
            }
        }
        byteArray = new byte[(count + 1) / 2];
        boolean first = true;
        int len = 0;
        for (int i = 0; i < hexStringLength; i++)
        {
            char c = hexString.charAt(i);
            int value;
            //int value;
            if ((c >= '0') && (c <= '9'))
            {
                value = c - '0';
            }
            else
            {
                // int value;
                if ((c >= 'A') && (c <= 'F'))
                {
                    value = c - 'A' + 10;
                }
                else
                {
                    // int value;
                    if ((c >= 'a') && (c <= 'f')) {
                        value = c - 'a' + 10;
                    } else {
                        value = -1;
                    }
                }
            }
            if (value >= 0)
            {
                if (first)
                {
                    byteArray[len] = ((byte)(value << 4));
                }
                else
                {
                    int tmp214_212 = len; byte[] tmp214_211 = byteArray;tmp214_211[tmp214_212] = ((byte)(tmp214_211[tmp214_212] | value));
                    len++;
                }
                first = !first;
            }
        }
        return byteArray;
    }*/

    private void Start_EIDAToolkit() {
        //close the current ftReader;
        try {
            Log.d(TAG,"Closing ftReader ");
            ftReader.readerClose();
        } catch (Exception obj) {
            Log.d(TAG,"Getting ftReader Exception ");
        }
     /*   digit9Plugin = new AppCardReader(getApplicationContext());
        //  digit9Plugin = new HIDAppCardReader(HomeActivity.this);
        Log.d("Digit9Plugin","Opening the device ");
        int rc = digit9Plugin.Open();*/

        Log.d(TAG,"Starting the Digit9 Plugin ");
     //   initialize();

    }


          private int getPortFromEditText() {
        //String valueEditText = "8080";
        //return (valueEditText.length() > 0) ? Integer.parseInt(valueEditText) : DEFAULT_PORT;
        return DEFAULT_PORT;
    }
    private void SendCardData(String cardSerialNumber) {

        String requestType = "202";
        //String url=null;//GlobalVariables.SSOIPAddress + GET_ALL_USERS;
        Log.d(TAG,"SendCardData for Card Serial Number "+cardSerialNumber);

        //http://192.168.0.7:8114/user_enroll/insertemaritesuserdetails
       //  String url=SERVER_IPADDR+":8114/user_enroll/insertemaritesuserdetails";
        String url=GlobalVariables.SSOIPAddress+":8114/user_enroll/insertemaritesuserdetails";
        // new VolleyService(FirstActivity.class).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
        Log.d(TAG,"SendCardData for Card Serial Number "+url);

        String jsonData = publicdataJson.toString();
        Log.d(TAG,"JSON Data is "+jsonData);
        try {
            new VolleyService(new IResult() {
                @Override
                public void notifySuccess(String requestType, Object response) {

                    Log.d(TAG, "SendCardData - notifySuccess: "+requestType+" response us"+response);
                    //GlobalVariables.FirstUser = FirstUser_Cap;
                    //fingerRemove(60000);

   //                 Log.d(TAG, "Befor Gson");

                }


                @Override
                public void notifyError(String requestType, VolleyError error) {
                    Log.d(TAG, "SendCardData - notifyError:  is "+error.getCause());
                    Log.d(TAG, "SendCardData - notifyError: "+error.getMessage());
                    try {
                        //String fingerprint = "testfingerprint";
                        Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                        //  String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                        //           playAudioFile(2,filePath);

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }, this).tokenBase(POST, url, publicdataJson, requestType);

        }catch (Exception obj) {

        }
    }
}
