/*
 *
 * You are free to use this example code to generate similar functionality
 * tailored to your own specific needs.
 *
 * This example code is provided by Lumidigm Inc. for illustrative purposes only.
 * This example has not been thoroughly tested under all conditions.  Therefore
 * Lumidigm Inc. cannot guarantee or imply reliability, serviceability, or
 * functionality of this program.
 *
 * This example code is provided by Lumidigm Inc. “AS IS” and without any express
 * or implied warranties, including, but not limited to the implied warranties of
 * merchantability and fitness for a particular purpose.
 *
 */

package belgiumeida;

import android.util.Log;

class Logger {
    private static final String TAG = "_EIDA_Digit9";
    private static final boolean IS_DEBUG = true;

    public static void d(String msg) {
        Log.d("_EIDA_Digit9", msg.isEmpty() ? "No Message in the log statement" : msg);
    }

    public static void e(String msg) {
        Log.e("_EIDA_Digit9", msg.isEmpty() ? "No Message in the log statement" : msg);
    }

    public static void i(String msg) {
        Log.i("_EIDA_Digit9", isEmpty(msg) ? "No message for Debugger" : msg);
    }

    public static void w(String msg) {
        Log.e("_EIDA_Digit9", isEmpty(msg) ? "No message for Debugger" : msg);
    }

    private static boolean isEmpty(String msg) {
        return (msg == null) || (msg.isEmpty());
    }
}
