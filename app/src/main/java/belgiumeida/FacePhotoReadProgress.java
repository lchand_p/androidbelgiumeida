package belgiumeida;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.luxand.facerecognition.MainActivity_FaceandFinger;
import com.luxand.facerecognition.WebICAImageCapture_Server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import database.AppDatabase;
import lumidigm.captureandmatch.R;
import webserver.GlobalVariables;

import static webserver.GlobalVariables.FaceICA;

public class FacePhotoReadProgress extends AppCompatActivity {

    private TextView textView,textView9,personnameID;
    private AppDatabase appDatabase=null;
    private String TAG="PhotoReadProgress";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_check_in);

        Log.d("Chand","It is Face ICA Testing");
        setContentView(R.layout.face_user_progressforsmartcard);

        appDatabase = AppDatabase.getAppDatabase(FacePhotoReadProgress.this);

        startWeb();

        try {
            Log.d("Test", "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d("Test","Got Exception while setting Full Screen");
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                returntogif();
            }
        },2000);
    }

    public void returntogif(){
        Intent intent = new Intent(this, MainActivity_FaceandFinger.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("Enrolment",false);
        mBundle.putBoolean("adminVerification",false);
        // intent.putExtra("userid", editText.getText().toString());
        intent.putExtras(mBundle);
        startActivity(intent);
        finish();

//        finish();
    }

    private void startWeb()
    {

        final int DEFAULT_PORT = 8085;
        Context app_context = getApplicationContext();
        WebICAImageCapture_Server server= new WebICAImageCapture_Server(DEFAULT_PORT,app_context);
        try
        {
            server.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String data = intent.getStringExtra("Data");
//            user_data.setText(data);

            String perName=intent.getStringExtra("Name");
            String IDNum=intent.getStringExtra("IDNum");
            String ArabicName=intent.getStringExtra("ArabicName");
            String expiryDate=intent.getStringExtra("expiryDate");
            String Nationality=intent.getStringExtra("Nationality");
            String Gender=null;


            Log.d(TAG,"Expiry Date is "+expiryDate);
            Log.d(TAG,"Nationality  is "+Nationality);

            //expiryDate="+expiryData+"&Nationality
            GlobalVariables.personName=null;
            GlobalVariables.IDNumber=null;
            GlobalVariables.ArabicName=null;
            GlobalVariables.expiryDate=null;
            GlobalVariables.Nationality=null;
            GlobalVariables.Gender=null;


            Log.d(TAG,"Person Name is "+perName+"  ID Number is"+IDNum);

            if (perName != null)
                GlobalVariables.personName=perName;

            if (IDNum != null)
                GlobalVariables.IDNumber=IDNum;

            if (ArabicName != null)
                GlobalVariables.ArabicName=ArabicName;

            if (expiryDate != null)
                GlobalVariables.expiryDate=expiryDate;

            if (Nationality != null)
                GlobalVariables.Nationality=Nationality;


            if (Gender != null)
                GlobalVariables.Gender=Gender;

            Log.d(TAG,"Got the Face Detection Thread"+data.length());

            //Get the File

            //data = data.replaceAll("   ","+");
            //data = data.replaceAll("  ","+");
            data = data.replaceAll(" ","+");

            Log.d(TAG,"Got the Face Detection"+data);

            //byte[] photo =toByteArray(data);
            byte[] photo = Base64.decode(data, Base64.DEFAULT);
            Log.d(TAG,"Got the Face Detection"+photo.length);
            if (photo == null || photo.length <= 0) {
                Log.d(TAG,"Photo Contents are null ");
                return;
            }//if()
            //Create  a bitmap.
            //set to the imageview
//chand added below 2 commands
            Bitmap bmp = BitmapFactory.decodeByteArray(photo, 0, photo.length);
            // bmp.compress(Bitmap.CompressFormat.JPEG);
            try {
                Log.d("Test","Getting the file ");

                List<lumidigm.captureandmatch.entity.User> users = appDatabase.userDao().getAll();

                if (users != null) {
                    Log.d(TAG, "Get User Count is " + users.size());

                    String userID = (users.get(0).get_id());

                    String filename = Environment.getExternalStorageDirectory()
                            + File.separator
                            + "displayImages" + File.separator + userID + ".jpg";

                    try {
                        File f = new File(filename);

                        if (f.exists())
                            f.delete();
                    } catch (Exception obj) {

                    }
                    Log.d(TAG,"File Name is "+filename);

                    FileOutputStream out = new FileOutputStream(filename,false);
					/*try {
						out.write(("").getBytes());
					} catch (Exception Obj) {
						Log.d(TAG,"Clear file exception");
					}*/
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

                    Log.d(TAG, "Update File ");
                    // bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
                    //bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                    out.close();
                } else
                    Log.d(TAG,"Not found user ");
            } catch (Exception obj) {
                Log.d("Test","Got the exception ");
            }
            //imageview.setImageBitmap(bmp);
            Log.d("data", "data from server"+ data);
        }
    };


    @Override
    protected void onStart() {
        super.onStart();

        if (FaceICA == true) {
            IntentFilter intentFilter = new IntentFilter("lumidigm.captureandmatch"); //lumidigm.captureandmatch");
            registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (FaceICA == true)
            unregisterReceiver(broadcastReceiver);
    }
}
