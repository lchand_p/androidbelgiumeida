package belgiumeida;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ftsafe.readerScheme.FTReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import belgiumeida.tlv.BerTag;
import belgiumeida.tlv.BerTlv;
import belgiumeida.tlv.BerTlvBuilder;
import belgiumeida.tlv.BerTlvParser;
import belgiumeida.tlv.BerTlvs;
import belgiumeida.tlv.HexUtil;
import lumidigm.captureandmatch.R;

import static belgiumeida.Tool.convertByteArrayToHexString;
import static belgiumeida.Tool.toByteArray;
import static com.feitian.readerdk.Tool.Tool.byte2HexStr2;

//import android.support.design.widget.TextInputLayout;


/*

MF - Master File
DF - Dedicated File
EF - Elementary File

MF 3f00 -  2 byte file identifier -  - hexa decimal notation val
DF  DF01
ID File 4031

#define BEID_FILE_ID					"3F00DF014031"
#define BEID_FILE_ID_SIGN				"3F00DF014032"
#define BEID_FILE_ADDRESS				"3F00DF014033"
#define BEID_FILE_PHOTO					"3F00DF014035"

Belpic applet A0 00 00 00 30 29 05 70 00 AD 13 10 01 01 FF
DF(BELPIC) A0 00 00 01 77 50 4B 43 53 2D 31 35
DF(ID) (not on applet V1.0) A0 00 00 01 77 49 64 46 69 6C 65 73


DF(ID) (not on applet V1.0) A0 00 00 01 77 49 64 46 69 6C 65 73

command: 00 a4 04 00 0c A0 00 00 01 77 49 64 46 69 6C 65 73
output: 6121

61	XX	I	Command successfully executed; ‘XX’ bytes of data are available and can be requested using GET RESPONSE.

It can be either by AID, Path, or File ID
Above is AID
- command:
00 a4 02 00 02 3F 00 (mf)
-sUCCESS IS 90 00
00 a4 02 00 02 DF 01 (df)
00 a4 02 00 02 40 35 (pHOTO fILE)

 */
public class BelgiumEida {
    private static final String TAG = "ContactCpuCardActivity";
  //  private static CardManager mOperation;
    private Button mBtnGetVersion;
    private Button mBtnPowerUp;
    private Button mBtnSendApdu;
    private EditText mEtRecvApduData;
    private EditText mPhysicalNumber;
    FTReader ftReader;

    //private static ArtiSecureSmartCardReader cardReader = null;
    public static FeitianCardReader cardReader = null;

    private static final int FILESCTRUCTURE_BIN_TYPE_1 = 0x00;
    private static final int CARDNUMBER_ASCII_TYPE_1 = 0x01;
    private static final int CHIP_NUMBER_BINA_TYPE_2 = 0x02;
    private static final int CARD_VALIDITY_BEGIN_ASCII_TYPE_3 = 0x03;
    private static final int CARD_VALIDITY_END_ASCII_TYPE_4 = 0x04;
    private static final int CARD_DEL_MUNSPALITY_UTF_TYPE_5 = 0x05;
    private static final int NATIONAL_NUMBER_ASCII_TYPE_6 = 0x06;
    private static final int NAME_UTF_TYPE_7 = 0x07;
    private static final int NAME_2_UTF_TYPE_8 = 0x08;
    private static final int NATIONALITY_TYPE_9 = 0x0A;
    private static final int BIRTH_LOC_TYPE_9 = 0x0B;
    private static final int BIRTHDATE_LOC_TYPE_9 = 0x0C;
    private static final int SEC_LOC_TYPE_9 = 0x0D;

    //   @Override
    protected void InitializeCardReader(Context ctx) {
        //    super.init();
        Log.d(TAG, "in init ");

        //   Config config = new Config(path, baudrate);
        // ArtiSecureSmartCardReader    tpReader=new ArtiSecureSmartCardReader(this);
        cardReader = new FeitianCardReader(ctx);
        RemoveLogFile();
        cardReader.OpenNew();

        Log.d(TAG, "Open New ");

        Init_NewPlugin1(ctx);

        Log.d(TAG, "Initialize New ");

        GetCardDetailsPhoto();
    }

    protected void GetCardDetailsPhoto() {
        //InitiateATR();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Going for Initialize ATR ");
                InitiateATR();
            }
        }, 1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Going to Get the Photo");
                  GetthePhotoFromSmartCard();

                 // GetCardDetailsFromCard();
            }
        }, 3000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Before Reading Card Details");
                InitiateATR();
                //GetthePhotoFromSmartCard();

                //GetCardDetailsFromCard();
            }
        }, 15000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG, "Before Reading GetCardDetailsFromCard");
               // GetthePhotoFromSmartCard();

                GetCardDetailsFromCard();
            }
        }, 18000);
    }

    private byte[] getData() {
        String dataStr = "0012"; //mTilApduData.getEditText().getText().toString().trim();
        byte[] arr3 = {(byte) 0x80, (byte) 0xC0, (byte) 0x02, (byte) 0xA1, (byte) 0x08};

//        return Utils.hexString2ByteArray(dataStr);
        return arr3;
    }



    void showMsg(String tp) {
        Log.d(TAG, "Got the Show " + tp);
    }

    private void GettheImage() {

        Log.d(TAG, "Get the Image ");

        /*
         * Select AIU
         * Select File
         * Get the Binary File
         */

        //byte[] sendapdu = { (byte) 0x80,(byte)  0xC0,(byte) 0x02,(byte)  0xA1,(byte)  0x08};

        //00a404000ca00000024300130000000101
        //00A404000CA00000024300130000000101
        byte[] sendapdu2 = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xa0, (byte) 0x00, (byte) 0x00
                , (byte) 0x02, (byte) 0x43, (byte) 0x00, (byte) 0x13, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01};

        byte[] test = Plugin_APDU(sendapdu2, 12, 0);

        //command: 00a404000ca00000024300130000000101
        //output:
        //6121
        //
        //command: 00a40000020202

        if (test != null) {
            Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu2));
            Log.d(TAG, "length of APDU response is " + test.length);
            //    Log.d(TAG, "Received APDU" + (test));

            Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
            //byte[] test2= cardReader.sendApdu(sendapdu2);
        }
        //         00 a4 00 00 02 02 03

        byte[] sendapdu3 = {(byte) 0x00, (byte) 0xa4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x02};

        // byte[] sendapdu3 = { (byte) 0x00,(byte)  0xa4,(byte) 0x00,(byte)  0x00,(byte)  0x02, (byte)  0x02,(byte)  0x03 };
        byte[] test3 = Plugin_APDU(sendapdu3, 4, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu3));

        if (test3 != null)
            Log.d(TAG, "3 Rcvd APDU Array is " + convertByteArrayToHexString(test3));


        //00 c0 00 00 12
        //command: 00c0000012

        byte[] sendapdu4 = {(byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x12};
        byte[] test4 = Plugin_APDU(sendapdu4, 4, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu4));
        if (test4 != null)
            Log.d(TAG, "4 Rcvd APDU Array is " + convertByteArrayToHexString(test4));

        //00 b0 00 00 00
        byte[] sendapdu5 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0x00, (byte) 0x00};


        Log.d(TAG, "Send APDU is first before" + convertByteArrayToHexString(sendapdu5));

        byte[] test5 = Plugin_APDU(sendapdu5, 5, 0);

        Log.d(TAG, "Send APDU is first " + convertByteArrayToHexString(sendapdu5));
        if (test5 != null)
            Log.d(TAG, "5 Rcvd APDU Array is " + convertByteArrayToHexString(test5));

        //00b00000ff
        byte[] sendapdu6 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0x00, (byte) 0xff};
        byte[] test6 = Plugin_APDU(sendapdu6, 2, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu6));

        if (test6 != null)
            Log.d(TAG, "6 Rcvd APDU Array is " + convertByteArrayToHexString(test6));

        //command: 00b000ff00
        byte[] sendapdu7 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0x00};
        byte[] test7 = Plugin_APDU(sendapdu7, 2, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu7));

        if (test7 != null)
            Log.d(TAG, "7 Rcvd APDU Array is " + convertByteArrayToHexString(test7));


        //command: 00b000ffff
        byte[] sendapdu8 = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0xff};
        byte[] test8 = Plugin_APDU(sendapdu8, 5, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu8));

        if (test7 != null)
            Log.d(TAG, "8 Rcvd APDU Array is " + convertByteArrayToHexString(test8));


        //command: 00b001feff
        byte[] sendapdu9 = {(byte) 0x00, (byte) 0xb0, (byte) 0x01, (byte) 0xfe, (byte) 0xff};
        byte[] test9 = Plugin_APDU(sendapdu9, 5, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu9));

        if (test7 != null)
            Log.d(TAG, "9 Rcvd APDU Array is " + convertByteArrayToHexString(test9));


        //command: 00b001feff
        byte[] sendapdu10 = {(byte) 0x00, (byte) 0xb0, (byte) 0x05, (byte) 0xfa, (byte) 0xff};
        byte[] test10 = Plugin_APDU(sendapdu10, 5, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu10));

        if (test10 != null)
            Log.d(TAG, "10 Rcvd APDU Array is " + convertByteArrayToHexString(test10));


        //00 b0 0e f1 ff
//The photo is stored as .jpeg on the card, just read file 4035 (don't forget the chop off the status words (last 2 bytes)), and save it as a binary file.


        //00b002fdff

        //00b003fcff

        //00b006f9ff

        //00b007f8ff

        //00b000ffff
/*
        String hex = Integer.toHexString(123);
        //Byte.parseByte(123);
        Log.d(TAG,"Got completed and coming out ");
        Integer i = new Integer(11534336);

        String hex2 = Integer.toHexString(i);
        System.out.println("Hex value: "+hex2);

        Byte[] byt3=Byte.parseByte(hex2);*/

        String FHex = "FF";
        String THex = "FF";

        // String test2=Integer.toHexString(255);
        // String test11=Integer.toHexString(29527900‬);

        //long ts=2952790013;
      /*  int num1=Integer.parseInt(FHex, 16);
        int num2=Integer.parseInt(THex, 16);

        int sum = num1+num2;

        String hex2 = Integer.toHexString(sum);

*/
        //  Log.d(TAG,"Hexa String first is "+test2);

        /*long   MAX_VALUE = 2952790012
        long max = 9223372036854775807;//Inclusive;
        long yourmilliseconds = 1222402191823;
        String test12=Long.toHexString(MAX_VALUE‬);*/

        //  Log.d(TAG,"Hexa String sec is "+test11);

        //B0 0000‬
        //B0 00FF  - 255
        //B0 01FE‬    500
        //B0 02FD‬    725

        //B0 03FC‬    1000

        //B0 04FB   1255

        //B0 05FA‬  1500

        //B0 06F9‬ - 1750
        //B0 07F8‬  - 2000

        //B0 08F7‬
        //B0 08F7‬   - 2250


        //B0 0651  - 2500
        //B0 0750‬    2750
        //B0 084F‬    3000
        //B0 094E‬     3250


        //B0 0A4D‬  - 3500
        //B0 0B4C‬   - 3750
        //B0 0C4B   - 4000
        //B0 0D4A‬     4250

        //B0 0E49  - 4500
        //B0 0F48‬    4750
        //B0 1047‬    5000
        //B0 1146‬    5250


        //4035 size

        //B0 0000‬


        //For Test

        //00 B0 00 ff

        /*

        command: 00a404000ca00000024300130000000101
        6121

        command:00a40000020203
        6112

        command: 00c0000012
        8510000002030100043dc000c0000000006a9000

        command: 00b0000000
        6cff

        command: 00b00000ff
        output:
        700301c7e3050002495243060004201903074307000420240307a3080000a3090034d8b3d8a7d984d9852cd985d8a7d8acd8af2cd985d8b5d984d8ad2cd8a8d986d8afd8b12cd8a7d984d982d8add8b7d8a7d986d98ae30a0000e30b002353616c656d2c4d616a65642c4d75736c65682c42616e6461722c416c71616874616e69e30c00014da30d0010d8a7d984d8b3d8b9d988d8afd98ad987e336000c536175646920417261626961e30e0003534155430f000420020505a3370000e338000644616d6d616d631201004edf9b30494b292945277ecb9ebd88b0d681b955ea270579c6d67559c863116a6419633a0c2acbe4131504736270a069072cd99d9000

        command: 00b000ff00
        output:
        6cff */
    }

    private void GetthePhotoFromSmartCard() {
        String base = "00b0";
        String offset = "0000";
        int curroffset = 0;
        int lengthtoRetrieve = 2295; //4050;

        String exctractlength = "ff";
        byte[] totalbyte;

        byte[] currbytes = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0xff};
        int lengthBytes = currbytes.length;

        byte[] destination = new byte[lengthtoRetrieve + 255];

        lengthBytes = 0;

        //Select the Photo File
        SelectPhotoFile();

        for (int i = 0; curroffset < lengthtoRetrieve; i++) {

            curroffset = Integer.parseInt(offset, 16) + (i * 255);

            String currVal = Integer.toHexString(curroffset);

            if (currVal.length() == 3)
                currVal = "0" + currVal;
            else if (currVal.length() == 2)
                currVal = "00" + currVal;
            else if (currVal.length() == 1)
                currVal = "000" + currVal;


            Log.d(TAG, "New Hex Value:  " + base + currVal + exctractlength);
            String HexValue = base + currVal + exctractlength;

            byte[] sendapdu10 = toByteArray(HexValue); //{ (byte) 0x00,(byte)  0xb0,(byte) 0x05,(byte)  0xfa,(byte)  0xff};
            byte[] test10 = Plugin_APDU(sendapdu10, 5, 0);
            if (test10 != null) {
                Log.d(TAG, "Send APDU is +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));


            Log.d(TAG, "Checking size ");
            byte[] temp1 = (test10);

            if ((test10.length == 2) && temp1[1] == (byte) 0x00) {
                //if (temp1[1] == 0x00) {
                //  Log.d(TAG,"Coming out as zero zero");
                //}
                Log.d(TAG, "Coming out +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));
                Log.d(TAG, "Length is 2 bytes so coming out");
                break;
            } else if (test10 != null) {
                Log.d(TAG, "10 Rcvd APDU Array is " + convertByteArrayToHexString(test10));
                System.arraycopy(test10, 0, destination, lengthBytes, test10.length - 2);
                //as getting 9000 at end - removing that
                writeDataToFileNew(convertByteArrayToHexString(test10));

                lengthBytes = lengthBytes + test10.length - 2;
                Log.d(TAG, "Length Bytes is" + lengthBytes);
            }
        }
    }
        Log.d(TAG, "New Bytes:  " + lengthBytes);
        writeDataToFileNew("Total length is " + lengthBytes);
        writeDataToFileNew(convertByteArrayToHexString(destination));

       // String temp = Base64.encodeToString(destination, Base64.DEFAULT);
        //writeDataToFileNew(temp);


        SaveBinaryFile(destination);
        Log.d(TAG, "Retrievedlednth:  " + convertByteArrayToHexString(destination));

        //Bitmap bmp = BitmapFactory.decodeByteArray(destination, 0, destination.length);

        // bmp.compress(Bitmap.CompressFormat.JPEG);
       /* try {
            if (bmp != null) {
                Log.d("Test", "Getting the file ");

                String filename = Environment.getExternalStorageDirectory() + "/displayImages/test100.jpeg";
                FileOutputStream out = new FileOutputStream(filename);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

                // bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
                //bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
            } else
                Log.d(TAG, "Bit map is null");
        } catch (Exception obj) {
            Log.d("Test", "Issue with parsing " + obj.getMessage());
        }*/
    }

    private void SelectPhotoFile() {
        Log.d(TAG, "Get the Image ");

        /*
         * Select AIU
         * Select File
         * Get the Binary File
         */

        /*
        command: 00 a4 04 00 0c A0 00 00 01 77 49 64 46 69 6C 65 73
        output: 6121

        61	XX	I	Command successfully executed; ‘XX’ bytes of data are available and can be requested using GET RESPONSE.

        It can be either by AID, Path, or File ID
        Above is AID
                - command:
        00 a4 02 00 02 3F 00 (mf)
                -sUCCESS IS 90 00
        00 a4 02 00 02 DF 01 (df)
        00 a4 02 00 02 40 35 (pHOTO fILE)
        */

        //byte[] sendapdu = { (byte) 0x80,(byte)  0xC0,(byte) 0x02,(byte)  0xA1,(byte)  0x08};

        byte[] belgium_apdu_aid = { (byte) 0x00,(byte)  0xa4,(byte)  0x04,(byte)  0x00,(byte)  0x0c,(byte)  0xA0,(byte)  0x00,(byte)  0x00,(byte)  0x01,(byte)  0x77,(byte)
                0x49,(byte)  0x64,(byte)  0x46,(byte)  0x69,(byte)  0x6C,(byte)  0x65,(byte)  0x73};

        //AID - 00A404000CA00000024300130000000101
        byte[] icaapdu2_aid = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xa0, (byte) 0x00, (byte) 0x00
                , (byte) 0x02, (byte) 0x43, (byte) 0x00, (byte) 0x13, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01};

        //select - file         00 a4 00 00 02 02 03
        byte[] apdu_selectfile = {(byte) 0x00, (byte) 0xa4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x02};

        byte[] belgium_apdu_selectfile1 = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x3F, (byte) 0x00 };// (mf)
        //         -sUCCESS IS 90 00
        byte[] belgium_apdu_selectfile2 = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0xDF, (byte) 0x01};// (df)
        byte[] belgium_apdu_selectfile3 = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x40, (byte) 0x35};// (ID fILE)


        byte[] test = Plugin_APDU(icaapdu2_aid, 12, 0);
        //  byte[] test = Plugin_APDU(belgium_apdu_aid, 12, 0);


        //command: 00a404000ca00000024300130000000101
        //output:
        //6121
        //
        //command: 00a40000020202

        if (test != null) {
            Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(icaapdu2_aid));
            Log.d(TAG, "length of APDU response is " + test.length);
            //    Log.d(TAG, "Received APDU" + (test));

            Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
            //byte[] test2= cardReader.sendApdu(sendapdu2);
        }
        // byte[] sendapdu3 = { (byte) 0x00,(byte)  0xa4,(byte) 0x00,(byte)  0x00,(byte)  0x02, (byte)  0x02,(byte)  0x03 };
        byte[] test3 = Plugin_APDU(apdu_selectfile, 4, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(apdu_selectfile));

        //byte[] test4 = Plugin_APDU(belgium_apdu_selectfile1, 4, 0);
        //byte[] test5 = Plugin_APDU(belgium_apdu_selectfile2, 4, 0);
        //byte[] test6 = Plugin_APDU(belgium_apdu_selectfile3, 4, 0);

        if (test3 != null)
            Log.d(TAG, "3 Rcvd APDU Array is " + convertByteArrayToHexString(test3));



        //00 c0 00 00 12
        //command: 00c0000012

     /*   byte[] sendapdu4 = {(byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x12};
        byte[] test4 = Plugin_APDU(sendapdu4, 4, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu4));
        if (test4 != null)
            Log.d(TAG, "4 Rcvd APDU Array is " + convertByteArrayToHexString(test4));*/
    }

    private static void writeDataToFileNew(String data) {
        try {
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "mylogs1.txt");

            Log.d(TAG, "External Storage file is " + Environment.getExternalStorageState());
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            writeDataToFile(fileOutputStream, data);
            fileOutputStream.close();
        } catch (FileNotFoundException ex) {
            Log.e("Test", ex.getMessage(), ex);
        } catch (IOException ex) {
            Log.e("Test", ex.getMessage(), ex);
        }
    }


    private static void writeDataToFile(FileOutputStream fileOutputStream, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

            Log.d(TAG, "Saving files" + data);
            bufferedWriter.write(data);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (FileNotFoundException ex) {
            Log.e("Test", ex.getMessage(), ex);
        } catch (IOException ex) {
            Log.e("Test", ex.getMessage(), ex);
        }
    }

    private static void RemoveLogFile() {
        try {
            //  long deviceupTime = ((SystemClock.elapsedRealtime() / 1000));

            //if (deviceupTime < 300)
            { //Before 5 minutes - remove that file
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "mylogs1.txt");
                File file2 = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "myImage.jpg");

                file2.delete();
                Log.d(TAG, "Removing the Log File");
                file.delete();
            }
        } catch (Exception obj) {
            Log.d(TAG, "Got the exception while removing file");
        }
    }

    private void SaveBinaryFile(byte[] bFile) {
        final String UPLOAD_FOLDER = Environment.getExternalStorageDirectory()
                + File.separator + "myImage.jpg";

        FileInputStream fileInputStream = null;

        try {

        //    File file = new File("C:\\temp\\testing1.txt");
/*
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "myImage.jpg");

            byte[] bFile = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
*/

            //save bytes[] into a file
            writeBytesToFileClassic(bFile, UPLOAD_FOLDER);

       //     System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void writeBytesToFileClassic(byte[] bFile, String fileDest) {

        FileOutputStream fileOuputStream = null;

        Log.d(TAG,"WriteBytestoFileClassic");
        try {
            fileOuputStream = new FileOutputStream(fileDest);
            fileOuputStream.write(bFile);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOuputStream != null) {
                try {
                    fileOuputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
private void InitiateATR() {
    Log.d(TAG, "Before waiting powerUp");
    new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
            //    powerUp();

            Log.d(TAG, "Before  powerUp");
            byte[] test = Plugin_ATRNew(0);
        }
    }, 2000);

}

    private void GetCardDetailsFromCard() {
        String base = "00b0";
        String offset = "0000";
        int curroffset = 0;
        int lengthtoRetrieve = 4050; //1000; //4050;

        String exctractlength = "ff";

        Log.d(TAG,"GetCardDetailsFromCard");

        byte[] totalbyte;
        byte[] currbytes = {(byte) 0x00, (byte) 0xb0, (byte) 0x00, (byte) 0xff, (byte) 0xff};
        int lengthBytes2 = currbytes.length;
        byte[] destination = new byte[lengthtoRetrieve + 255];

        lengthBytes2 = 0;

        //Select the Photo File
        SelectGetCardDataFile();

        for (int i = 0; curroffset < lengthtoRetrieve; i++) {

            curroffset = Integer.parseInt(offset, 16) + (i * 255);
            String currVal = Integer.toHexString(curroffset);

            if (currVal.length() == 3)
                currVal = "0" + currVal;
            else if (currVal.length() == 2)
                currVal = "00" + currVal;
            else if (currVal.length() == 1)
                currVal = "000" + currVal;

            Log.d(TAG, "New Hex Value:  " + base + currVal + exctractlength);
            String HexValue = base + currVal + exctractlength;

            byte[] sendapdu10 = toByteArray(HexValue); //{ (byte) 0x00,(byte)  0xb0,(byte) 0x05,(byte)  0xfa,(byte)  0xff};
            byte[] test10 = Plugin_APDU(sendapdu10, 5, 0);
            Log.d(TAG, "Send APDU is +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));


            Log.d(TAG, "Checking size ");
            byte[] temp1 = (test10);

            if ((test10.length == 2) && temp1[1] == (byte) 0x00) {
                //if (temp1[1] == 0x00) {
                //  Log.d(TAG,"Coming out as zero zero");
                //}
                Log.d(TAG, "Coming out +" + test10.length + "  " + convertByteArrayToHexString(sendapdu10));
                Log.d(TAG, "Length is 2 bytes so coming out");
                break;
            } else if (test10 != null) {
                Log.d(TAG, "10 Rcvd APDU Array is " + convertByteArrayToHexString(test10));
                System.arraycopy(test10, 0, destination, lengthBytes2, test10.length - 2);
                //as getting 9000 at end - removing that
                writeDataToFileNew(convertByteArrayToHexString(test10));

                lengthBytes2 = lengthBytes2 + test10.length - 2;
                Log.d(TAG, "Length Bytes is" + lengthBytes2);
            }
        }
        Log.d(TAG, "New Bytes:  " + lengthBytes2);
        writeDataToFileNew("Total length is " + lengthBytes2);
        writeDataToFileNew(convertByteArrayToHexString(destination));
        //SaveBinaryFile(destination);

           ProcessCarddata(destination);

        //String temp = Base64.encodeToString(destination, Base64.DEFAULT);
        //writeDataToFileNew(temp);

        Log.d(TAG, "Retrievedlednth:  " + convertByteArrayToHexString(destination));

//        Bitmap bmp = BitmapFactory.decodeByteArray(destination, 0, destination.length);

        // bmp.compress(Bitmap.CompressFormat.JPEG);
  /*      try {
            if (bmp != null) {
                Log.d("Test", "Getting the file ");

                String filename = Environment.getExternalStorageDirectory() + "/displayImages/test100.jpeg";
                FileOutputStream out = new FileOutputStream(filename);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

                // bmp.compress(Bitmap.CompressFormat.PNG, quality, out);
                //bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
            } else
                Log.d(TAG, "Bit map is null");
        } catch (Exception obj) {
            Log.d("Test", "Issue with parsing " + obj.getMessage());
        }*/
    }

    private void SelectGetCardDataFile() {

        Log.d(TAG, "Get the SelectGetCardDataFile ");

        /*
         * Select AIU
         * Select File
         * Get the Binary File
         */

        /*
        command: 00 a4 04 00 0c A0 00 00 01 77 49 64 46 69 6C 65 73
        output: 6121

        61	XX	I	Command successfully executed; ‘XX’ bytes of data are available and can be requested using GET RESPONSE.

        It can be either by AID, Path, or File ID
        Above is AID
                - command:
        00 a4 02 00 02 3F 00 (mf)
                -sUCCESS IS 90 00
        00 a4 02 00 02 DF 01 (df)
        00 a4 02 00 02 40 31 (pHOTO fILE)
        */

        //byte[] sendapdu = { (byte) 0x80,(byte)  0xC0,(byte) 0x02,(byte)  0xA1,(byte)  0x08};

        byte[] belgium_apdu_aid = { (byte) 0x00,(byte)  0xa4,(byte)  0x04,(byte)  0x00,(byte)  0x0c,(byte)  0xA0,(byte)  0x00,(byte)  0x00,(byte)  0x01,(byte)  0x77,(byte)
                0x49,(byte)  0x64,(byte)  0x46,(byte)  0x69,(byte)  0x6C,(byte)  0x65,(byte)  0x73};

        //AID - 00A404000CA00000024300130000000101
        byte[] icaapdu2_aid = {(byte) 0x00, (byte) 0xa4, (byte) 0x04, (byte) 0x00, (byte) 0x0c, (byte) 0xa0, (byte) 0x00, (byte) 0x00
                , (byte) 0x02, (byte) 0x43, (byte) 0x00, (byte) 0x13, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01};


        //select - file         00 a4 00 00 02 02 03
        byte[] apdu_selectfile = {(byte) 0x00, (byte) 0xa4, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x02};

        byte[] belgium_apdu_selectfile1 = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0x3F, (byte) 0x00 };// (mf)
       //         -sUCCESS IS 90 00
        byte[] belgium_apdu_selectfile2 = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0xDF, (byte) 0x01};// (df)
        byte[] belgium_apdu_selectfile3 = {(byte) 0x00, (byte) 0xa4, (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0x40, (byte) 0x31};// (ID fILE)


        byte[] test = Plugin_APDU(icaapdu2_aid, 12, 0);
      //  byte[] test = Plugin_APDU(belgium_apdu_aid, 12, 0);


        //command: 00a404000ca00000024300130000000101
        //output:
        //6121
        //
        //command: 00a40000020202

        if (test != null) {
            Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(icaapdu2_aid));
            Log.d(TAG, "length of APDU response is " + test.length);
            //    Log.d(TAG, "Received APDU" + (test));

            Log.d(TAG, "Received APDU " + convertByteArrayToHexString(test));
            //byte[] test2= cardReader.sendApdu(sendapdu2);
        }
        // byte[] sendapdu3 = { (byte) 0x00,(byte)  0xa4,(byte) 0x00,(byte)  0x00,(byte)  0x02, (byte)  0x02,(byte)  0x03 };
        byte[] test3 = Plugin_APDU(apdu_selectfile, 4, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(apdu_selectfile));

        //byte[] test4 = Plugin_APDU(belgium_apdu_selectfile1, 4, 0);
        //byte[] test5 = Plugin_APDU(belgium_apdu_selectfile2, 4, 0);
        //byte[] test6 = Plugin_APDU(belgium_apdu_selectfile3, 4, 0);

        if (test3 != null)
            Log.d(TAG, "3 Rcvd APDU Array is " + convertByteArrayToHexString(test3));



        //00 c0 00 00 12
        //command: 00c0000012

     /*   byte[] sendapdu4 = {(byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x12};
        byte[] test4 = Plugin_APDU(sendapdu4, 4, 0);
        Log.d(TAG, "Send APDU is " + convertByteArrayToHexString(sendapdu4));
        if (test4 != null)
            Log.d(TAG, "4 Rcvd APDU Array is " + convertByteArrayToHexString(test4));*/
    }
    private String LOG="CardReader";
    private void ProcessData() {
        byte[] bytes = HexUtil.parseHex("50045649534157131000023100000033D44122011003400000481F");

        BerTlvParser parser = new BerTlvParser();
        BerTlvs tlvs = parser.parse(bytes, 0, bytes.length);


        //
      //  BerTlvLogger.log("    ", tlvs, LOG);

        List<BerTlv> test= tlvs.getList();
        int count = test.size();
        Log.d(TAG,"Count is "+count);
        for (int i=0;i<count;i++) {
            String Val = test.get(i).getTextValue();
            Log.d(TAG,"Value is "+Val);
            String te2=test.get(i).getHexValue();

            Log.d(TAG," TE Valis "+te2);
            BerTag to = test.get(i).getTag();
            String te = to.toString();
            Log.d(TAG,"TAG is "+to);
            Log.d(TAG,"TAG te is "+te);

        }
        /* Output was:
         Count is 2
            Value is VISA
03-13 15:13:57.785  4718  4718 D ContactCpuCardActivity:  TE Valis 56495341
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG is - 50
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG te is - 50
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: Value is À€1À€À€À€3ï¿½A"@À€À€H
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity:  TE Valis 1000023100000033D44122011003400000481F
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG is - 57
03-13 15:13:57.786  4718  4718 D ContactCpuCardActivity: TAG te is - 57
*/

        // Tag val2 = test.get(0).getTag();



      //  BerTlv a = tlvs.getList()
        byte[] bytes2 =  new BerTlvBuilder()
                .addHex(new BerTag(0x50), "56495341")
                .addHex(new BerTag(0x57), "1000023100000033D44122011003400000481F")
                .buildArray();
    }
private void ProcessCarddata(byte[] data) {
    Log.d(TAG, "Process Card Data ");
     // Tester      tlvtest =new Tester();
    Log.d(TAG, "Running the Testing ");
    // tlvtest.runTesting();


    //adf

    //TlvBox boxes = new TlvBox();

    byte[] arr3 = {(byte) FILESCTRUCTURE_BIN_TYPE_1, (byte) 0x02, (byte) 0x02, (byte) 0x03,
            (byte) CARDNUMBER_ASCII_TYPE_1, (byte) 0x06, (byte) 0x42,(byte) 0x45,(byte) 0x4,(byte) 0x50,(byte) 0x49,(byte) 0x43};

    Log.d(TAG,"Byte aray is "+convertByteArrayToHexString(arr3));
    Log.d(TAG,"Size is "+arr3.length);

    /*
    public static final int FILESCTRUCTURE_BIN_TYPE_1 = 0x00;
    public static final int CARDNUMBER_ASCII_TYPE_1 = 0x01;
    public static final int CHIP_NUMBER_BINA_TYPE_2 = 0x02;
    public static final int CARD_VALIDITY_BEGIN_ASCII_TYPE_3 = 0x03;
    public static final int CARD_VALIDITY_END_ASCII_TYPE_4 = 0x04;
    public static final int CARD_DEL_MUNSPALITY_UTF_TYPE_5 = 0x05;
    public static final int NATIONAL_NUMBER_ASCII_TYPE_6 = 0x06;
    public static final int NAME_UTF_TYPE_7 = 0x07;
    public static final int NAME_2_UTF_TYPE_8 = 0x08;
    public static final int NATIONALITY_TYPE_9 = 0x0A;
    public static final int BIRTH_LOC_TYPE_9 = 0x0B;
    public static final int BIRTHDATE_LOC_TYPE_9 = 0x0C;
    public static final int SEC_LOC_TYPE_9 = 0x0D;
*/

    Log.d(TAG,"Value of Hex is "+byte2HexStr2(arr3,arr3.length)+" len is "+arr3.length);
    byte[] bytes =  HexUtil.parseHex(byte2HexStr2(arr3,arr3.length)); //HexUtil.parseHex("50045649534157131000023100000033D44122011003400000481F");

    BerTlvParser parser = new BerTlvParser();
    BerTlvs tlvs = parser.parse(bytes, 0, bytes.length);


    //
    //  BerTlvLogger.log("    ", tlvs, LOG);

    List<BerTlv> test= tlvs.getList();
    int count = test.size();
    Log.d(TAG,"Count is "+count);
    for (int i=0;i<count;i++) {
        String Val = test.get(i).getTextValue();
        Log.d(TAG,"Value is "+Val);
        String te2=test.get(i).getHexValue();

        Log.d(TAG," TE Valis "+te2);
        BerTag to = test.get(i).getTag();
        String te = to.toString();
        Log.d(TAG,"TAG is "+to);
        Log.d(TAG,"TAG te is "+te);

    }
    }
    byte[] Plugin_APDU(byte[] tp, int len, int index) {
        byte[] retVal=null;

        retVal = cardReader.Plugin_APDU(tp,len,index);

        return retVal;
    }
    private void Init_NewPlugin1(Context cp) {
        cardReader.Init_NewPlugin(cp);
    }
    private void contactCpuPowerUp() {

        try {
            byte[] Atr = cardReader.Plugin_ATR(1);
        } catch (Exception obj) {

        }
    }
    private byte[] Plugin_ATRNew(int val) {
        byte[] retVal=null;


        try {

            byte[] Atr = cardReader.Plugin_ATR(1);
        } catch (Exception obj) {

        }
        return retVal;
    }
    private void Close() {
        cardReader.Close();
    }
}
