package dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import entity.Fingerentity;

@Dao
public interface FingerDao {

    @Insert
    void insert(Fingerentity... cat);

    @Update
    void update(Fingerentity... cat);

    @Delete
    void delete(Fingerentity... cat);

    @Query("Select * FROM Fingerentity")
    Fingerentity[] loadAll();

}