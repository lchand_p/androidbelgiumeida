package lumidigm.captureandmatch.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllTasks implements Parcelable {

    @Expose
    @SerializedName("updated")
    private String updated;

    @Override
    public String toString() {
        return "AllTasks{" +
                "updated='" + updated + '\'' +
                ", assignee='" + assignee + '\'' +
                ", __v=" + __v +
                ", uid=" + uid +
                ", owner=" + owner +
                ", subscribers=" + subscribers +
                ", history=" + history +
                ", attachments=" + attachments +
                ", notes=" + notes +
                ", comments=" + comments +
                ", date='" + date + '\'' +
                ", priority=" + priority +
                ", type='" + type + '\'' +
                ", group='" + group + '\'' +
                ", issue='" + issue + '\'' +
                ", subject='" + subject + '\'' +
                ", _id='" + _id + '\'' +
                ", tags=" + tags +
                ", status=" + status +
                ", deleted=" + deleted +

                '}';
    }

    @Expose
    @SerializedName("assignee")
    private String assignee;

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public List<String> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<String> subscribers) {
        this.subscribers = subscribers;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
/*

    public String getTerminalID() {
        return terminal_id;
    }

    public void setTerminalID(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    public String getControllerID() {
        return controller_id;
    }

    public void setControllerID(String controller_id) {
        this.controller_id = controller_id;
    }

    public String getGroupID() {
        return group_id;
    }

    public void setGroupID(String  group_id) {
        this.group_id = group_id;
    }

    public String getLocationID() {
        return location_id;
    }

    public void setLocationID(String location_id) {
        this.location_id = location_id;
    }

    public String getRoleID() {
        return location_id;
    }
    public void setRoleID(String Role_id) {
        this.Role_id = Role_id;
    }
    public String getRoleName() {
        return role_name;
    }
    public void setRoleName(String role_name) {
        this.role_name = role_name;
    }
*/

    @Expose
    @SerializedName("__v")
    private int __v;
    @Expose
    @SerializedName("uid")
    private int uid;
    @Expose
    @SerializedName("owner")
    private Owner owner;
    @Expose
    @SerializedName("subscribers")
    private List<String> subscribers;
    @Expose
    @SerializedName("history")
    private List<History> history;
    @Expose
    @SerializedName("attachments")
    private List<String> attachments;
    @Expose
    @SerializedName("notes")
    private List<String> notes;
    @Expose
    @SerializedName("comments")
    private List<String> comments;
    @Expose
    @SerializedName("date")
    private String date;
    @Expose
    @SerializedName("priority")
    private int priority;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("group")
    private String group;
    @Expose
    @SerializedName("issue")
    private String issue;
    @Expose
    @SerializedName("subject")
    private String subject;
    @Expose
    @SerializedName("_id")
    private String _id;
    @Expose
    @SerializedName("tags")
    private List<Tags> tags;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("deleted")
    private boolean deleted;

/*
    @Expose
    @SerializedName("terminal_id")
    private String terminal_id;
    @Expose
    @SerializedName("controller_id")
    private String controller_id;
    @Expose
    @SerializedName("group_id")
    private String group_id;
    @Expose
    @SerializedName("location_id")
    private String location_id;
    @Expose
    @SerializedName("Role_id")
    private String Role_id;
    @Expose
    @SerializedName("role_name")
    private String role_name;
*/

    public static class Owner implements Parcelable {
        @Expose
        @SerializedName("username")
        private String username;
        @Expose
        @SerializedName("_id")
        private String _id;

        @Override
        public String toString() {
            return "Owner{" +
                    "username='" + username + '\'' +
                    ", _id='" + _id + '\'' +
                    '}';
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }


/*
        public String getTerminalID() {
            return terminal_id;
        }

        public void setTerminalID(String terminal_id) {
            this.terminal_id = terminal_id;
        }

        public String getControllerID() {
            return controller_id;
        }

        public void setControllerID(String controller_id) {
            this.controller_id = controller_id;
        }

        public String getGroupID() {
            return group_id;
        }

        public void setGroupID(String  group_id) {
            this.group_id = group_id;
        }

        public String getLocationID() {
            return location_id;
        }

        public void setLocationID(String location_id) {
            this.location_id = location_id;
        }

        public String getRoleID() {
            return location_id;
        }
        public void setRoleID(String Role_id) {
            this.Role_id = Role_id;
        }
        public String getRoleName() {
            return role_name;
        }
        public void setRoleName(String role_name) {
            this.role_name = role_name;
        }
*/

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.username);
            dest.writeString(this._id);
        }

        public Owner() {
        }

        protected Owner(Parcel in) {
            this.username = in.readString();
            this._id = in.readString();
        }

        public static final Creator<Owner> CREATOR = new Creator<Owner>() {
            @Override
            public Owner createFromParcel(Parcel source) {
                return new Owner(source);
            }

            @Override
            public Owner[] newArray(int size) {
                return new Owner[size];
            }
        };
    }

    public static class History implements Parcelable {
        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getDate() {
            return date;
        }

        @Override
        public String toString() {
            return "History{" +
                    "_id='" + _id + '\'' +
                    ", date='" + date + '\'' +
                    ", owner='" + owner + '\'' +
                    ", description='" + description + '\'' +
                    ", action='" + action + '\'' +
                    '}';
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        @Expose
        @SerializedName("_id")
        private String _id;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("owner")
        private String owner;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("action")
        private String action;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this._id);
            dest.writeString(this.date);
            dest.writeString(this.owner);
            dest.writeString(this.description);
            dest.writeString(this.action);
        }

        public History() {
        }

        protected History(Parcel in) {
            this._id = in.readString();
            this.date = in.readString();
            this.owner = in.readString();
            this.description = in.readString();
            this.action = in.readString();
        }

        public static final Creator<History> CREATOR = new Creator<History>() {
            @Override
            public History createFromParcel(Parcel source) {
                return new History(source);
            }

            @Override
            public History[] newArray(int size) {
                return new History[size];
            }
        };
    }

    public static class Tags implements Parcelable {
        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public String getRelay() {
            return relay;
        }

        public void setRelay(String relay) {
            this.relay = relay;
        }

        public String getDoor() {
            return door;
        }

        public void setDoor(String door) {
            this.door = door;
        }

        public String getIp_addr() {
            return ip_addr;
        }

        public void setIp_addr(String ip_addr) {
            this.ip_addr = ip_addr;
        }

        public String getAes_key() {
            return aes_key;
        }

        public void setAes_key(String aes_key) {
            this.aes_key = aes_key;
        }

        public String getHash_key() {
            return hash_key;
        }

        public void setHash_key(String hash_key) {
            this.hash_key = hash_key;
        }

        public int getTerminalID() {
            return terminal_id;
        }

        public void setTerminalID(int terminal_id) {
            this.terminal_id = terminal_id;
        }

        public String getControllerID() {
            return controller_id;
        }

        public void setControllerID(String controller_id) {
            this.controller_id = controller_id;
        }

        public String getGroupID() {
            return group_id;
        }

        public void setGroupID(String  group_id) {
            this.group_id = group_id;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public int getLocationID() {
            return location_id;
        }

        public void setLocationID(int location_id) {
            this.location_id = location_id;
        }

  /*      public String getRoleID() {
            return Role_id;
        }
        public void setRoleID(String Role_id) {
            this.Role_id = Role_id;
        }
        public String getRoleName() {
            return role_name;
        }
        public void setRoleName(String role_name) {
            this.role_name = role_name;
        }
*/
        @Override
        public String toString() {
            return "Tags{" +
                    "__v=" + __v +
                    ", relay='" + relay + '\'' +
                    ", door='" + door + '\'' +
                    ", ip_addr='" + ip_addr + '\'' +
                    ", aes_key='" + aes_key + '\'' +
                    ", hash_key='" + hash_key + '\'' +
                    ", zone='" + zone + '\'' +
                    ", name='" + name + '\'' +
                    ", _id='" + _id + '\'' +
                    ", terminal_id=" + terminal_id +
                    ", controller_id=" + controller_id +
                    ", group_id=" + group_id +
                    ", location=" + location +
                    ", location_id=" + location_id +
                     //   terminal_id, controller_id, group_id,location_id, Role_id,role_name,

                    '}';
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

//terminal_id, controller_id, group_id,location_id, Role_id,role_name, normalized
        @Expose
        @SerializedName("__v")
        private int __v;
        @Expose
        @SerializedName("relay")
        private String relay;
        @Expose
        @SerializedName("door")
        private String door;
        @Expose
        @SerializedName("ip_addr")
        private String ip_addr;
        @Expose
        @SerializedName("aes_key")
        private String aes_key;
        @Expose
        @SerializedName("hash_key")
        private String hash_key;
        @Expose
        @SerializedName("zone")
        private String zone;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("_id")
        private String _id; //next
        @Expose
        @SerializedName("terminal_id")
        private int terminal_id;
        @Expose
        @SerializedName("controller_id")
        private String controller_id;
        @Expose
        @SerializedName("group_id")
        private String group_id;
        @Expose
        @SerializedName("location_id")
        private int location_id;
        @Expose
        @SerializedName("location")
        private String location;
        /*@Expose
        @SerializedName("Role_id")
        private String Role_id;
        @Expose
        @SerializedName("role_name")
        private String role_name;
*/

//terminal_id, controller_id, group_id,location_id, Role_id,role_name,

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.__v);
            dest.writeString(this.relay);
            dest.writeString(this.door);
            dest.writeString(this.ip_addr);
            dest.writeString(this.aes_key);
            dest.writeString(this.hash_key);
            dest.writeString(this.zone);
            dest.writeString(this.name);
            dest.writeString(this._id);
            dest.writeInt(this.terminal_id);
            dest.writeString(this.controller_id);
            dest.writeString(this.group_id);
            dest.writeInt(this.location_id);
            dest.writeString(this.location);
            /*dest.writeString(this.Role_id);
            dest.writeString(this.role_name);*/
        }

        public Tags() {
        }

        protected Tags(Parcel in) {
            this.__v = in.readInt();
            this.relay = in.readString();
            this.door = in.readString();
            this.ip_addr = in.readString();
            this.aes_key = in.readString();
            this.hash_key = in.readString();
            this.zone = in.readString();
            this.name = in.readString();
            this._id = in.readString();
            this.terminal_id = in.readInt();
            this.controller_id= in.readString();
            this.group_id= in.readString();
            this.location_id= in.readInt();

            this.location= in.readString();
            /*this.Role_id= in.readString();
            this.role_name= in.readString();*/
        }

        public static final Creator<Tags> CREATOR = new Creator<Tags>() {
            @Override
            public Tags createFromParcel(Parcel source) {
                return new Tags(source);
            }

            @Override
            public Tags[] newArray(int size) {
                return new Tags[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.updated);
        dest.writeString(this.assignee);
        dest.writeInt(this.__v);
        dest.writeInt(this.uid);
        dest.writeParcelable(this.owner, flags);
        dest.writeStringList(this.subscribers);
        dest.writeTypedList(this.history);
        dest.writeStringList(this.attachments);
        dest.writeStringList(this.notes);
        dest.writeStringList(this.comments);
        dest.writeString(this.date);
        dest.writeInt(this.priority);
        dest.writeString(this.type);
        dest.writeString(this.group);
        dest.writeString(this.issue);
        dest.writeString(this.subject);
        dest.writeString(this._id);
        dest.writeTypedList(this.tags);
        dest.writeInt(this.status);
        dest.writeByte(this.deleted ? (byte) 1 : (byte) 0);
    }

    public AllTasks() {
    }

    protected AllTasks(Parcel in) {
        this.updated = in.readString();
        this.assignee = in.readString();
        this.__v = in.readInt();
        this.uid = in.readInt();
        this.owner = in.readParcelable(Owner.class.getClassLoader());
        this.subscribers = in.createStringArrayList();
        this.history = in.createTypedArrayList(History.CREATOR);
        this.attachments = in.createStringArrayList();
        this.notes = in.createStringArrayList();
        this.comments = in.createStringArrayList();
        this.date = in.readString();
        this.priority = in.readInt();
        this.type = in.readString();
        this.group = in.readString();
        this.issue = in.readString();
        this.subject = in.readString();
        this._id = in.readString();
        this.tags = in.createTypedArrayList(Tags.CREATOR);
        this.status = in.readInt();
        this.deleted = in.readByte() != 0;
    }

    public static final Creator<AllTasks> CREATOR = new Creator<AllTasks>() {
        @Override
        public AllTasks createFromParcel(Parcel source) {
            return new AllTasks(source);
        }

        @Override
        public AllTasks[] newArray(int size) {
            return new AllTasks[size];
        }
    };
}