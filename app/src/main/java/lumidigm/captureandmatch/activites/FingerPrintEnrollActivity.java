package lumidigm.captureandmatch.activites;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hidglobal.biosdk.BioDeviceStatus;
import com.hidglobal.biosdk.BioSDKAPI;
import com.hidglobal.biosdk.BioSDKDevice;
import com.hidglobal.biosdk.BioSDKFactory;
import com.luxand.facerecognition.FaceSuccessActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import frost.timeandattendance.TerminalFaceCheckInActivity;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.constants.Constants;
import database.AppDatabase;
import lumidigm.captureandmatch.entity.User;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.vcomdroid.ANSI378TemplateHelper;
import lumidigm.vcomdroid.Minutiae;
import lumidigm.vcomdroid.VCOM;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.hidglobal.biosdk.BioDeviceStatus.BIOSDK_OK;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_DONE;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_FINGER_PRESENT;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_PROCESSING;
import static lumidigm.constants.Constants.CHECK_USER;
import static lumidigm.constants.Constants.PAXTON_ACCESS_TOKEN;
import static webserver.GlobalVariables.NoFingerPrintSupport;

public class FingerPrintEnrollActivity extends Activity implements View.OnClickListener, IResult {

  private static final String TAG = "FingerPrintEnrollActivi";
  private Toolbar toolbarTop;
  private TextView welcomeText;
  private ImageView home;
  private ImageView imageBack;
  private ImageView imageHome;
  private ImageView imageNext;
  private TextView textBack;
  private ImageView imageHome1;
  private TextView textNext;


  private TextView enrollUser;
  private LinearLayout centerImage;
  private TextView hint;
  private ImageView imageview1;

  // VCOM Java/JNI adapter.
  private VCOM mVCOM;
  // fingerprint images.
  private Bitmap mBitmapCapture;

  // fingerprint ANSI 378 templates.
  private byte[] mTmplCapture;

  private byte[] mTmplCapture1;
  private byte[] mTmplCapture2;


  String userid;
  String ldapUser = null;
  static int value = 0;

  private boolean fromWeb = false;
  private String currState = null;
  User user;
  SharedPreferences prefs = null;
  AlertDialog alertDialog = null;
  private ImageSwitcher imageSwitcher;
  private int currentIndex = 0;
  private final String[] imageNames = {"enroll", "enroll_one", "enroll_two", "enroll_complete"};
  private final int[] imageNamesnew = {R.drawable.enroll, R.drawable.enroll_one, R.drawable.enroll_two, R.drawable.enroll_complete};
  private boolean updateUser = false;
  AppDatabase appDatabase;

  private boolean OutofActivity = false;
  private static long lastUnAuthorizedTime = 0;
  public static int fingermoduleCount = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.finger_print_enroll);
    View overlay = findViewById(R.id.mylayout);

    overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    try {

      Log.d(TAG, "Reading User ID  Finger printenroll ");
      ldapUser = getIntent().getExtras().getString("ldapUser");

      updateUser = getIntent().getExtras().getBoolean("updateUser");
      userid = getIntent().getExtras().getString("userid");
      user = getIntent().getExtras().getParcelable("userData");
      Log.d(TAG, "Got the update User " + updateUser);
    } catch (Exception obj) {
      Log.d(TAG, "Got Exception while gtting user");
      user = null;
      ldapUser = null;
      updateUser = false;
    }
    Log.d(TAG, "ldapUser is " + ldapUser);

    try {
      fromWeb = getIntent().getBooleanExtra("FromWeb", false);
      currState = getIntent().getStringExtra("CurrState");
      userid = getIntent().getStringExtra("userid");
      Log.d(TAG, "Got from Web " + fromWeb + " Rxvd UserID " + userid);

    } catch (Exception obj) {
      currState = null;
      //userID = null;
    }


    try {
      updateUser = getIntent().getExtras().getBoolean("updateUser");
      Log.d(TAG, "Update User status is " + updateUser);
      //   user = getIntent().getExtras().getParcelable("userData");
      //ldapUser = getIntent().getExtras().getString("ldapUser");
      //  userid = getIntent().getExtras().getString("userid");
    } catch (Exception obj) {
      Log.d(TAG, "Got Exception while gtting user");
      //user=null;
      //ldapUser= null;
      updateUser = false;
    }
    appDatabase = AppDatabase.getAppDatabase(FingerPrintEnrollActivity.this);

    prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
            MODE_PRIVATE);
    GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

    Log.d(TAG, "LdapUser is " + ldapUser);


    if (user == null && userid != null) {
      try {
        lumidigm.captureandmatch.entity.User user2 = appDatabase.userDao().countUsersBasedonUserID(userid);
        if (user2 != null) {
          user = user2;
          Log.d(TAG, "Got the User details Needs to update" + user2.getUsername());
        } else
          Log.d(TAG, "No User ");
      } catch (Exception obj) {
        Log.d(TAG, "Got the Exception while reading user ");
      }
    }
    if (user == null && userid != null) {
      //Get the UserData based on the UserID
      Log.d(TAG, "Chand Fetch User Data for UserId" + userid);

      GetUserData(userid);
    } else if ((fromWeb) && (ldapUser == null)) {
      if (userid != null) {
        //Get the UserData based on the UserID
        Log.d(TAG, "Fetch User Data for UserId" + userid);

        GetUserData(userid);
      }
    } else {

      mVCOM = new VCOM(this);

      toolbarTop = (Toolbar) findViewById(R.id.toolbar_top);
      welcomeText = (TextView) findViewById(R.id.welcome_text);
      home = (ImageView) findViewById(R.id.home);
      welcomeText.setVisibility(View.GONE);
      home.setVisibility(View.GONE);


      //enrollUser = (TextView) findViewById(R.id.enroll_user);
      centerImage = (LinearLayout) findViewById(R.id.center_image);
      hint = (TextView) findViewById(R.id.hint);
      // imageview1 = (ImageView) findViewById(R.id.imageview1);
      //imageSwitcher
      imageSwitcher = (ImageSwitcher) findViewById(R.id.imageSwitcher);

      Animation out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
      Animation in = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
      // Set animation when switching images.
      imageSwitcher.setInAnimation(in);
      imageSwitcher.setOutAnimation(out);
      try {
        Log.d(TAG, "on Create ");
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

      } catch (Exception obj) {
        Log.d(TAG, "Got Exception while setting Full Screen");
      }

      imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

        // Returns the view to show Image
        // (Usually should use ImageView)
        @Override
        public View makeView() {
          ImageView imageView = new ImageView(getApplicationContext());

          //imageView.setBackgroundColor(Color.LTGRAY);
          imageView.setScaleType(ImageView.ScaleType.CENTER);

          //ImageSwitcher.LayoutParams params= new ImageSwitcher.LayoutParams(
          //      Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.MATCH_PARENT);
          //imageView.setLayoutParams(params);
          return imageView;
        }
      });
      this.currentIndex = 0;
      this.showImage(this.currentIndex);
      //findViewById(R.id.cancel).setOnClickListener(this);
      userid = getIntent().getExtras().getString("userid");
      //   if (user != null )
      //   enrollUser.setText("Employee #" + userid + " " + user.getFullname());
      //  else
      //    enrollUser.setText("Employee #" + userid);
      // hint.setText("Please scan your right finger");
      if (NoFingerPrintSupport) {

        showImage(1);

        Log.d(TAG, "It is No Finger Print Status ");
        Handler handler = new Handler();
        // tickTemplate1.setVisibility(View.VISIBLE);
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {
            Log.d(TAG, "FingerPrintSuccess first scucess");
            //imageview1.setImageResource(R.drawable.finger_first_success);
            showImage(3);
            SendFingerPrintTest();
          }
        }, 3000);
      } else
        new CaptureTask(0).execute();
    }
  }

  private void showImage(int imgIndex) {
    Log.d(TAG, "Show Image index is " + imgIndex);
        /*String imageName= this.imageNames[imgIndex];
        int resId= getDrawableResIdByName(imageName);
        if(resId!= 0) {
            this.imageSwitcher.setImageResource(resId);
        }*/
    this.imageSwitcher.setBackgroundResource(imageNamesnew[imgIndex]);
  }
  // Find Image ID corresponding to the name of the image (in the drawable folder).

  public int getDrawableResIdByName(String resName) {
    String pkgName = this.getPackageName();
    Log.d(TAG, "PKG Name is " + pkgName);
    // Return 0 if not found.
    int resID = this.getResources().getIdentifier(resName, "drawable", pkgName);
    Log.i(TAG, "Res Name: " + resName + "==> Res ID = " + resID);
    return resID;
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.cancel:
        //TODO implement
        onBackPressed();
        break;
      case R.id.settings_new:
        Log.d(TAG, "On FingerPrintEnrollActivity  Setting");
               /* check = false;
                Intent i1 = new Intent(this, UserAuthenticationActivity.class);
                startActivity(i1);*/
        startSettings();
        //TODO implement
        break;
    }
  }

  @Override
  public void notifySuccess(String requestType, Object response) {
//{"role":"support","deleted":false,"_id":"5b5067d195a76a4d582a3bbc","username":"ram","password":"ram","fullname":"ram","email":"ram","status":"","lastlogin":"2018-07-19T10:28:33.416Z","lastOnline":"2018-07-19T10:28:33.416Z"}
    Log.d(TAG, "notifySuccess: " + response.toString());

    Log.d(TAG, "Got the Request Type " + requestType);
    if (requestType.equals("600")) {
      Log.d(TAG, "It is Paxon ACCESS Token Success");
    } else if (requestType.equals("105")) {
      Log.d(TAG, "It is Paxon Enrollment Success");
    } else if (requestType.equals("101")) {

      Log.d(TAG, "Got the Request Type 101 - UserData Fetch");
      Gson gson = new Gson();
      Type listType = new TypeToken<User>() {
      }.getType();

      User user = gson.fromJson(response.toString(), listType);

      if (user.getTemplate1() != null && !user.getTemplate1().isEmpty() &&
              user.getTemplate2() != null && !user.getTemplate2().isEmpty() &&
              user.getTemplate3() != null && !user.getTemplate3().isEmpty()) {


        Intent intent = new Intent(this, FingerPrintEnrollActivity.class);

        intent.putExtra("userid", userid);
        if (ldapUser != null)
          intent.putExtra("ldapUser", ldapUser);

        if (user != null) {
          Bundle bn = new Bundle();
          bn.putParcelable("userData", user);
          intent.putExtras(bn);
        }
        startActivity(intent);

        Log.d(TAG, "NotifySucces Sending Self Activity ");
        //Toast.makeText(this, "Already you have added finger prints,so you can go to login and update your profile", Toast.LENGTH_SHORT).show();

      } else {
        Log.d(TAG, "User Already Exists");
                /*Intent intent = new Intent(this, AlreadyExituserActivity.class);

                intent.putExtra("userid", editText.getText().toString());
                Bundle bn=new Bundle();
                bn.putBoolean("check",false);
                bn.putParcelable("userData",user);
                intent.putExtras(bn);
                startActivity(intent);*/


        Intent intent = new Intent(this, FingerPrintEnrollActivity.class);

        intent.putExtra("userid", userid);
        if (ldapUser != null)
          intent.putExtra("ldapUser", ldapUser);

        if (user != null) {
          Bundle bn = new Bundle();
          bn.putParcelable("userData", user);
          intent.putExtras(bn);
        }
        startActivity(intent);

      }


    } else {
      Gson gson = new Gson();

      User user1 = gson.fromJson(response.toString(), User.class);
      //  Log.d(TAG, "notifySuccess: " + user.toString());
      AppDatabase appDatabase = AppDatabase.getAppDatabase(FingerPrintEnrollActivity.this);
      if (appDatabase.userDao().countUsers(user1.get_id()) != null) {
        appDatabase.userDao().update(user1);
        Log.d(TAG, "notifySuccess: " + appDatabase.userDao().countUsers(user1.get_id()).toString());
      } else {
        appDatabase.userDao().insertAll(user);
      }


      Log.d(TAG, "Sending UserFingerTeamplateResponse start verify");
      new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "UserFingerTemplateResponse", null, "FingerPrint_Verify");

      //For PaxTon - Send Enroll API;

      UpdatePaxtonenrollment(user1.get_id());

      MyProgressDialog.dismiss();
      Log.d(TAG, "Staerting Verifysuccessenroll " + ldapUser);
      // Intent i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivity.class);
      Intent i = new Intent(FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
      //For No Finger Status Support
      if (NoFingerPrintSupport == true)
        i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivityNew.class);

      i.putExtra("userid", userid);
      if (ldapUser != null)
        i.putExtra("ldapUser", ldapUser);
      if (user1 != null) {
        Bundle bn = new Bundle();
        bn.putParcelable("userData", user1);
        i.putExtras(bn);
      }
      // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(i);
      // finish();
    }
  }

  @Override
  public void notifyError(String requestType, VolleyError error) {
    MyProgressDialog.dismiss();
    Log.d(TAG, "Got Notify Error " + error);
    //  Toast.makeText(this, "UseId already exit,please try again", Toast.LENGTH_SHORT).show();
    /*    if (updateUser == true ) {
            Log.d(TAG,"Sending Update Template Response ");
            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"FingerPrint_Verify");

            MyProgressDialog.dismiss();
            Log.d(TAG,"Staerting Verifysuccessenroll "+ldapUser);
            // Intent i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivity.class);
            Intent i = new Intent(FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
            i.putExtra("userid", userid);
            if (ldapUser != null)
                i.putExtra("ldapUser", ldapUser);
            if (user != null ) {
                Bundle bn = new Bundle();
                bn.putParcelable("userData", user);
                i.putExtras(bn);
            }
            // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }*/
  }


  private class CaptureTask extends AsyncTask<Integer, Integer, Integer> {
    int i = -1;

    public CaptureTask(int i) {
      this.i = i;
    }


    @Override
    protected Integer doInBackground(Integer... i) {
      int rc;

      rc = mVCOM.Open();
      if (rc == 0) {
        rc = mVCOM.Capture();
        if (rc == 0) {

        }
        mVCOM.Close();
      }

      return rc;
    }

    @Override
    protected void onPostExecute(Integer rc) {
      Log.d(TAG, "onPostExecute: " + rc);
      Handler handler = new Handler();
      if (rc == 0) {

        Log.d(TAG, "on PostExecute FingerPrintScan " + i);

        Log.d(TAG, "New FingerPrintSuccess first scucess" + i);

        if (i == 0) {
          new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "UserFingerTemplateResponse", null, "First_Success");
          playBeep();
          //   imageview1.setImageResource(R.drawable.finger_remove_first);
          showImage(1);
        } else if (i == 1) {
          new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "UserFingerTemplateResponse", null, "Second_Success");

          playBeep();
          showImage(2);
          // imageview1.setImageResource(R.drawable.finger_remove_second);
        } else if (i == 2) {
          new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "UserFingerTemplateResponse", null, "Third_Success");

          playBeep();
          showImage(3);
          // imageview1.setImageResource(R.drawable.finger_remove_thired);
        }

        handler.postDelayed(new Runnable() {
          public void run() {


            //DrawMinutiae(mBitmapCapture, mTmplCapture);
            if (i == 0) {
              mBitmapCapture = GetBitmap();
              mTmplCapture = GetTemplate();

              // tickTemplate1.setVisibility(View.VISIBLE);
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                  //  new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse","102","First_Success");

                  Log.d(TAG, "FingerPrintSuccess first scucess" + i);
                  //imageview1.setImageResource(R.drawable.finger_first_success);
                  showImage(1);
                  new CaptureTask(1).execute();
                }
              }, 3000);

              value++;
            } else if (i == 1) {
              mBitmapCapture = GetBitmap();
              mTmplCapture1 = GetTemplate();
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse","102","Second_Success");

                  Log.d(TAG, "FingerPrintSuccess finger_success_Sencond " + i);
                  //  mVCOM.Match(mTmplCapture1, mTmplCapture);
                  //imageview1.setImageResource(R.drawable.finger_success_second);
                  showImage(2);
                  new CaptureTask(2).execute();
                }
              }, 3000);
                   /* if (mVCOM.GetMatchScore() > 50000) {

                        imageTemplate2.setImageBitmap(mBitmapCapture);
                        tickTemplate2.setVisibility(View.VISIBLE);

                    }else{
                        new CaptureTask(1).execute(0);
                    }*/

            } else if (i == 2) {
              //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse","102","Third_Success");

              Log.d(TAG, "FingerPrintSucce  is is 2 " + i);
              mBitmapCapture = GetBitmap();
              mTmplCapture2 = GetTemplate();
              //imageview1.setImageResource(R.drawable.finger_first_success);
              //new CaptureTask(2).execute(0);;
              // tickTemplate3.setVisibility(View.VISIBLE);
              //value++;
              showImage(3);

            }


            if (mTmplCapture1 != null && mTmplCapture1.length > 0 && mTmplCapture != null && mTmplCapture.length > 0 && mTmplCapture2 != null && mTmplCapture2.length > 0) {


              JSONObject jsonObject = new JSONObject();


              try {

                Log.d(TAG, "Added User template timestamp ");
                String tem1 = Base64.encodeToString(mTmplCapture, Base64.DEFAULT);

                String tem2 = Base64.encodeToString(mTmplCapture1, Base64.DEFAULT);

                String tem3 = Base64.encodeToString(mTmplCapture2, Base64.DEFAULT);

                jsonObject.put("uid", userid);
//need username  capture from ldap api(uid), this is needed for logging in
                //check_userid_ldap

                if (ldapUser != null)
                  jsonObject.put("username", ldapUser);


                Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                long currts = (curCalendar.getTimeInMillis() / 1000L);
                jsonObject.put("user_template_timestamp", currts);
                Log.d(TAG, "Timestamp to be updated for timestamp " + currts);

                jsonObject.put("template_1", tem1);
                jsonObject.put("template_2", tem2);
                jsonObject.put("template_3", tem3);

                // if (user.getTemplateFace() !=null)
                //   jsonObject.put("templateface", user.getTemplateFace());

                if (GlobalVariables.supervisorUserID != -1)
                  jsonObject.put("supervisor_userid", GlobalVariables.supervisorUserID);

                GlobalVariables.supervisorUserID = -1;
                Log.d(TAG, "onPostExecute: " + jsonObject);

                if (updateUser == true) {
                  Log.d(TAG, "Going for Update User Templates ");
                  user.setTemplate1(tem1);
                  user.setTemplate2(tem2);
                  user.setTemplate3(tem3);
                  //   AppDatabase appDatabase = AppDatabase.getAppDatabase(FingerPrintEnrollActivity.this);
                  // if (appDatabase.userDao().countUsers(user1.get_id()) != null) {
                  appDatabase.userDao().update(user);

                  Log.d(TAG, "Sending Update Template Response ");
                  new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "UserFingerTemplateResponse", null, "FingerPrint_Verify");

                  MyProgressDialog.dismiss();
                  Log.d(TAG, "Staerting Verifysuccessenroll " + ldapUser);
                  // Intent i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivity.class);
                  Intent i = new Intent(FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
                  i.putExtra("userid", userid);
                  if (ldapUser != null)
                    i.putExtra("ldapUser", ldapUser);
                  if (user != null) {
                    Bundle bn = new Bundle();
                    bn.putParcelable("userData", user);
                    i.putExtras(bn);
                  }
                  // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                  startActivity(i);

                } else {


                  MyProgressDialog.show(FingerPrintEnrollActivity.this, R.string.wait_message);
                  String url = GlobalVariables.SSOIPAddress + Constants.UPDATER_USERFINGERPRINT;

                  Log.d(TAG, "Please Wait " + url);

                  new VolleyService(FingerPrintEnrollActivity.this, FingerPrintEnrollActivity.this).tokenBase(POST, url, jsonObject, "100");

                }
              } catch (JSONException e) {
                e.printStackTrace();
              }


            }
          }
        }, 1000);

        // hintLayout.setVisibility(View.VISIBLE);
        // v.setVisibility(View.VISIBLE);

      } else {
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {

            //  mVCOM.Match(mTmplCapture1, mTmplCapture);
            // imageview1.setImageResource(R.drawable.finger_success_second);

            Log.d(TAG, "Wait Capture Task");
            new CaptureTask(i).execute();
          }
        }, 1000); //3000
        Log.i(TAG, String.format("Capture returned %d", rc));
      }

    }
  }

  private Bitmap GetBitmap() {
    int sizeX = mVCOM.GetCompositeImageSizeX();
    int sizeY = mVCOM.GetCompositeImageSizeY();
    if (sizeX > 0 && sizeY > 0) {
      Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
      int[] colors = new int[sizeX * sizeY];
      if (mVCOM.GetCompositeImage(colors) == 0) {
        bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
      } else {
        Log.i(TAG, "Unable to get composite image.");
      }
      return bm;
    } else {
      Log.i(TAG, "Composite image is too small.");
      // ...so we create a blank one-pixel bitmap and return it
      Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
      return bm;
    }
  }

  private byte[] GetTemplate() {
    int tmplSize = mVCOM.GetTemplateSize();
    if (tmplSize > 0) {
      byte[] tmpl = new byte[tmplSize];
      if (mVCOM.GetTemplate(tmpl) == 0) {
        return tmpl;
      } else {
        Log.i(TAG, "Unable to get template.");
      }
    } else {
      Log.i(TAG, "Template is too small.");
    }
    return new byte[0];
  }

  private void DrawMinutiae(Bitmap bm, byte[] tmpl) {
    int tmplSize = tmpl.length;
    if (tmplSize < 1)
      return;
    if (bm.getWidth() < 2 || bm.getHeight() < 2)
      return;

    ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
    Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
    Canvas canvas = new Canvas(bm);
    Paint paint = new Paint();
    for (int i = 0; i < minutiaeList.length; i++) {
      if (minutiaeList[i].nType == 1)
        paint.setARGB(0xFF, 0xFF, 0x00, 0x00);    // line-ending
      else
        paint.setARGB(0xFF, 0x00, 0xFF, 0x00);    // bifurcation
      int nX = minutiaeList[i].nX;
      int nY = minutiaeList[i].nY;
      canvas.drawCircle(nX, nY, 4, paint);
      double nR = minutiaeList[i].nRotAngle;
      int nX1;
      int nY1;
      nX1 = (int) (nX + (10.0 * Math.cos(nR)));
      nY1 = (int) (nY - (10.0 * Math.sin(nR)));
      canvas.drawLine(nX, nY, nX1, nY1, paint);
      canvas.drawLine(nX + 1, nY, nX1 + 1, nY1, paint);
      canvas.drawLine(nX - 1, nY, nX1 - 1, nY1, paint);
      canvas.drawLine(nX, nY + 1, nX1, nY1 + 1, paint);
      canvas.drawLine(nX, nY - 1, nX1, nY1 - 1, paint);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (mVCOM != null)
      mVCOM.Close();
  }

  public void playBeep() {
    MediaPlayer m = new MediaPlayer();
    try {
      if (m.isPlaying()) {
        m.stop();
        m.release();
        m = new MediaPlayer();
      }

      AssetFileDescriptor descriptor = getAssets().openFd("shutter.mp3");
      m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
      descriptor.close();

      m.prepare();
      m.setVolume(1f, 1f);
      m.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

  }

  private void GetUserData(String userId) {

    if (userId != null) {
      Log.d(TAG, "GetUserData before CHECK USER" + CHECK_USER);
      MyProgressDialog.show(FingerPrintEnrollActivity.this, R.string.wait_message);
      new VolleyService(FingerPrintEnrollActivity.this, FingerPrintEnrollActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + userId.toString(), null, "101");
    }
  }

  private void startSettings() {

    Log.d(TAG, "In SHow Licence App");
    LayoutInflater inflater = LayoutInflater.from(FingerPrintEnrollActivity.this);
    View subView = inflater.inflate(R.layout.settings_dialog, null);
    final EditText subEditText = (EditText) subView.findViewById(R.id.dialogEditText1);

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Android Settings");
    builder.setMessage("Password Verification");
    builder.setView(subView);
    builder.setCancelable(false);
    alertDialog = builder.create();


    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        String passwordText = subEditText.getText().toString().trim();
        Log.d(TAG, "Password Text is " + passwordText);
        //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

        String settingPwd = getsettingspwd();
        Log.d(TAG, "App Pwd is " + settingPwd);
        if (passwordText.equals("argus@542")) {
          Log.d(TAG, "Going for Shutdown");
          //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
          //    Shutdown();
        } else if (passwordText.equals("argus@543")) {
          Log.d(TAG, "Going for Reboot");
          //  reboot();
        } else if (passwordText.equals(settingPwd)) {

          startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
          finish();

        } //else //Kept for Testing by Chand - remove it
        // if (TestWithoutUSB(passwordText) == null)
        //   Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();
      }
    });

    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        {
          alertDialog.cancel();
          // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

          // finish();
        }
      }
    });


    builder.show();
  }

  private String getsettingspwd() {
    String verVal = null;

    try {
      File myFile = new File(Environment.getExternalStorageDirectory()
              + File.separator + "settingspwd.txt");

      FileInputStream fIn = new FileInputStream(myFile);
      BufferedReader myReader = new BufferedReader(new InputStreamReader(
              fIn));
      String aDataRow = "";
      String aBuffer = "";
      while ((aDataRow = myReader.readLine()) != null) {
        aBuffer += aDataRow + "\n";
      }
      verVal = aBuffer.toString().trim();

      Log.d(TAG, "Reading settings pwd from sdfile is " + verVal + " len s " + verVal.length());
      myReader.close();
      if ((verVal != null) && (verVal != " ") && (verVal != "") && verVal.length() > 0)
        return verVal;
    } catch (Exception obj) {
      Log.d(TAG, "Got exception while reading settings pwd");

    }
    return "blynk@123";
  }

  private void SendFingerPrintTest() {

    Log.d(TAG, "In Test SendFingerPrintTest");
    JSONObject jsonObject = new JSONObject();

    String template = "Rk1SACAyMAADRgA1ABcAAAEYAWAAxQDFAQAAAAAsQNEAICsKQCsAJrINQEkAKFYPQMUALDkOgGoA\n" +
            "L6APgDMAOK0LgKoARTUPQEEASKoMQDIASpUHQM0ASCUPgHcAS0kGQJQATD8KQD0AVZwHQHQAVzIG\n" +
            "gI0AYDgJQNMAYCIPQJoAanAJQFEAb5kGQF4AbqkHgCwAcDYOQH0AdFAJQKIAciUPgEoAdzgGgIAA\n" +
            "exMJQMgAfBsPgGUAfqkIgGEAgz8IgNMAhhYPQJMAjRMPQHYAlAENgIIApQQPgJ8AqgoPQKoAywcP\n" +
            "gEQAz6cPQJkA7FYPgGQA+6wPQGEBA08PQJ0BB7MPQLYBDlkPgFMBEaoPgJABIlQOQLcBJQQNgJ4B\n" +
            "LQAKQG8BL64PAh4BAQIeSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgNQECABiHCQCZER7//Ck7\n" +
            "BQCMEhr+KQsAqRQi/f/7wv00wAkAexEWLsD+IgUA6Q9WeAcA9Q9XdGwJAGkUEP/+MSIGAPcdVnvA\n" +
            "CwAkFv1G/sAp/ggA+ypTwkBNCgAiHQBPQUIGAE8qDP3+Vw4A/T1QwSlT//7AKwkAFC4GwGT9OgkA\n" +
            "xTNDwf1G/P8YABtAPf/EfsHBwcHBwcHBw//BxP/C/sLBBgA2OBDD/v3CBAAjRECHBQBDRglDBQD/\n" +
            "V1pUBwBBTBDAwP/AywoAME0t///EwXzCCgDNTUn/wP79//4pDQApTzD/wJbBwcHBmAMAlVJD+wMA\n" +
            "PVgwwBcAFWUrwVLE/sP/wcHCwMLCwMHD/sFvBQCNXrrEiQkA/mZX/v/A/cJLCQD9dlz/Nk8EAFdg\n" +
            "DP3CBgCba1P8+igVABaTIsB3asDAd4trwgUAn3de/hsGAMSAYjj+CAD8oWL+/17ADAAkkiR+dsP+\n" +
            "iRQAGqkaYsP+csL+lsJYwhMAG7ccW8NOwsDDwMDAcMIHAPyybcH+TxMAG8UXUXxvdHSNBwAb0R5t\n" +
            "fgcASdETwMCLBwAb3hrAwf+JBwDs7WQpQhEAI/AadMNRwP/Ew//AfgQA5f5tRAQAZ/wJYRAAKP0X\n" +
            "wMH+wYnBwv7FPgcQJQEewsD//8MHEGcFDMBcxAUQVRIQaQQQcS0GWQ==";

    try {

      Log.d(TAG, "Added User template timestamp ");
      String tem1 = template; //Base64.encodeToString(mTmplCapture, Base64.DEFAULT);

      String tem2 = template; //Base64.encodeToString(mTmplCapture1, Base64.DEFAULT);

      String tem3 = template; //Base64.encodeToString(mTmplCapture2, Base64.DEFAULT);

      jsonObject.put("uid", userid);
//need username  capture from ldap api(uid), this is needed for logging in
      //check_userid_ldap

      if (ldapUser != null)
        jsonObject.put("username", ldapUser);


      Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
      long currts = (curCalendar.getTimeInMillis() / 1000L);
      jsonObject.put("user_template_timestamp", currts);
      Log.d(TAG, "Timestamp to be updated for timestamp " + currts);

      jsonObject.put("template_1", tem1);
      jsonObject.put("template_2", tem2);
      jsonObject.put("template_3", tem3);

      // if (user.getTemplateFace() !=null)
      //   jsonObject.put("templateface", user.getTemplateFace());

      if (GlobalVariables.supervisorUserID != -1)
        jsonObject.put("supervisor_userid", GlobalVariables.supervisorUserID);

      GlobalVariables.supervisorUserID = -1;
      Log.d(TAG, "onPostExecute: " + jsonObject);

     /* if (updateUser == true) {
        Log.d(TAG, "Going for Update User Templates ");
        user.setTemplate1(tem1);
        user.setTemplate2(tem2);
        user.setTemplate3(tem3);
        //   AppDatabase appDatabase = AppDatabase.getAppDatabase(FingerPrintEnrollActivity.this);
        // if (appDatabase.userDao().countUsers(user1.get_id()) != null) {
        appDatabase.userDao().update(user);

        Log.d(TAG, "Sending Update Template Response ");
        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "UserFingerTemplateResponse", null, "FingerPrint_Verify");

        MyProgressDialog.dismiss();
        Log.d(TAG, "Staerting Verifysuccessenroll " + ldapUser);
        // Intent i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivity.class);
        Intent i = new Intent(FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);

        i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivityNew.class);


        i.putExtra("userid", userid);
        if (ldapUser != null)
          i.putExtra("ldapUser", ldapUser);
        if (user != null) {
          Bundle bn = new Bundle();
          bn.putParcelable("userData", user);
          i.putExtras(bn);
        }
        // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

      } else*/
      {


        MyProgressDialog.show(FingerPrintEnrollActivity.this, R.string.wait_message);
        String url = GlobalVariables.SSOIPAddress + Constants.UPDATER_USERFINGERPRINT;

        Log.d(TAG, "Please Wait " + url);

        new VolleyService(FingerPrintEnrollActivity.this, FingerPrintEnrollActivity.this).tokenBase(POST, url, jsonObject, "100");

      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  private void UpdatePaxtonenrollment(String userid1) {
    try
  {
    if (GlobalVariables.PaxtonSupport == true) {
      JSONObject jsonObject = new JSONObject();

      jsonObject.put("userid", userid1);
      jsonObject.put("token_type", 0); //type 0 for finger, 1 for face
      String url = GlobalVariables.SSOIPAddress + Constants.UPDATE_PAXTON_ENROLLMENT;

      Log.d(TAG,"User ID is "+userid1);
      Log.d(TAG, "Sending Paxton Enrollment for User ID " + userid + " URL " + url);
      new VolleyService(FingerPrintEnrollActivity.this, FingerPrintEnrollActivity.this).tokenBase(POST, url, jsonObject, "105");

    }
  } catch(Exception obj)
  {
    Log.d(TAG, "Got the Exception while token parsing for Paxton " + obj.getMessage());
  }
}
}
