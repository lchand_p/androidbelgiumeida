package lumidigm.captureandmatch.activites;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import database.AppDatabase;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.BuildConfig;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.utils.PermissionListener;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static lumidigm.constants.Constants.GET_ALL_USERS;
import static lumidigm.constants.Constants.SERVER_IPADDR;

/**
 * Created by colors on 8/10/18.
 */
/*
public class TestFingerScannActivity extends  AppCompatActivity implements IResult  {

    private static final String TAG = "[VCOM]";
    private TextView msg;
    private TextView submsg;

    private ImageView imageCard, tick;

    // HandlerHelp mHandler;
    private String currState = null;
    private boolean fromWeb=false;
    private SharedPreferences prefs=null;
    private boolean comingfromFingerPrint = false;
    //private MyCountDownTimer  myExitTimer=null;


    String userID=null;
    AppDatabase appDatabase;
   // private BroadcastReceiver mUpdateReceiver;


    byte[] PrevCapture=null;
*/
public class TestFingerScannActivity extends AppCompatActivity implements IResult {
    private static final String TAG = "TestScanActivity";
    Runnable runnable;
    private TextView msg;
    private TextView submsg;

    private ImageView imageCard, tick;
    SharedPreferences prefs;
    Handler handler;
    AppDatabase appDatabase;
    PermissionListener mPermissionListener;
    private Timer InternetCheckTimer = null;
    private boolean InternetFine=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerscann);
        // if the device is plugged in, this will attempt to obtain permission and/or open the device.
        msg = (TextView) findViewById(R.id.msg);
        submsg = (TextView) findViewById(R.id.submsg);
        tick = (ImageView) findViewById(R.id.tick);
        imageCard = (ImageView) findViewById(R.id.imageCard);
        // Log.d(TAG, "onCreate: "+getIntent().getStringExtra("finger"));

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, SERVER_IPADDR);

        appDatabase = AppDatabase.getAppDatabase(TestFingerScannActivity.this);
        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        Log.d(TAG,"Finger Print IP is "+SERVER_IPADDR);
        // check = true;
        try {
            Log.d(TAG, "Starting FingerScanActivity");
            if ((getIntent().getExtras()) != null) {
                //       mTmplCapture = Base64.decode((getIntent().getStringExtra("finger")), Base64.DEFAULT);
                //              comingfromFingerPrint = true;
            }  //Log.d(TAG, "mTmplCapture: "+ Arrays.toString(mTmplCapture));


        } catch (Exception tpexcep) {

        }
        /*try {
            fromWeb = getIntent().getBooleanExtra("FromWeb", false);
            currState = getIntent().getStringExtra("CurrState");
            userID = getIntent().getStringExtra("tokenID");
            Log.d(TAG, "Got from Web " + fromWeb+" Rxvd UserID "+userID);


        } catch (Exception obj) {
            currState = null;
            userID = null;
        }
        Log.d(TAG, "CurrState is " + currState);
        boolean test =true;
*/
        DownloadUserData();
        /*if (currState != null && currState.equals("comeOut")) {
            GlobalVariables.SupportWebFaceDetectonInitated=false;
            Log.d(TAG,"Got ComingOut - GoBack to Home Activity");
            //Intent intent= new Intent(this, HomeActivity.class);
            Intent intent= new Intent(this, Main2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            this.startActivity(intent);
            finish();
        } else*/ {
            //  currFingerPrintCount = 1;
            //GlobalVariables.personDescription = "not matching";
            //GlobalVariables.tokenID=0;
            //GlobalVariables.reqStatus = false;

      /*  StartRemoteApp();

        String filePath = Environment.getExternalStorageDirectory() + "/displayImages/insert_your_finger_to_authenticate.mp3";
        playAudioFile(2, filePath);

        mVCOM = new VCOM(this);
        // mHandler = new HandlerHelp();
        new CaptureAndMatchTask().execute(0); */
            //  database = AppDataBase.getAppDatabase(this);
            ImageView home = (ImageView) findViewById(R.id.home);
      /*  if (myExitTimer == null) {

            myExitTimer = new MyCountDownTimer(45000, 5000);
            myExitTimer.start();
        }
*/

            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        /*try {
            if (currState != null) {
                if (currState != null && currState.equals("fingernotmatch")) {
                    Log.d(TAG, "FingerNot Match");
                    //            fingerNotMatch();
                    // SendingMessageWithState("finger","101","fingernotmatch");
                    msg.setText("Finger not matched & Insert your finger");
                    submsg.setText("To authenticate with Emirates ID profile");

                    // imageCard.setImageResource(R.drawable.finger_first_success);
                    tick.setVisibility(View.GONE);

                } else if (currState != null && currState.equals("fingerremove")) {
                    Log.d(TAG, "It is FInger Remove");
                    msg.setText("Remove your finger");
                    submsg.setText("Please wait while we authenticate your biometric details.");
                    imageCard.setImageResource(R.drawable.finger_remove1);
                    tick.setVisibility(View.GONE);

                    //mVCOM.Close();
                    //fingerNotMatchTest();
                } else if (currState != null && currState.equals("fingersuccess")) {
                    Log.d(TAG, "Finger Success");
                    msg.setText("Congratulations");
                    submsg.setText("Your biometric sample has been validated successfully..");
                    imageCard.setImageResource(R.drawable.finger_first_success);
                    tick.setVisibility(View.VISIBLE);

                }
            }
        } catch (Exception obj) {
            Log.d(TAG, "Got Exception while Processing State");
        }*/
            // RegisterInterReceiver();
        }
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        boolean fromWeb = false;
        Log.d(TAG,"onBackPressed"+fromWeb);
  /*      if (myExitTimer != null) {
            myExitTimer.cancel();
            myExitTimer= null;
        }
  */      if (fromWeb) {
            Log.d(TAG,"GoBack to Home Activity");
            //Intent intent= new Intent(this, HomeActivity.class);
            //Intent intent= new Intent(this, HomeEnrollActivity.class);
            Intent intent= new Intent(this, Main2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            this.startActivity(intent);
            finish();
            //mVCOM.Close();
        }  else
        if (!SendingMessage("home","104")) {
            Log.d(TAG,"Sending   MEssage Failure in MainActivity");
            finish();
        }
        super.onBackPressed();
    }
    private boolean SendingMessage(String status, String requestType) {
        // SendingMessageWithState(status,"11",requestType);
        return false;
    }





    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    public boolean isConnectedInWifi() {

        try {
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.toString());
        }
        return false;
    }
    /*
    @Override
    public void onPause() {
        super.onPause();
        UnRegisterReceiver();
    }
    @Override
    public void onResume() {
        super.onResume();
        RegisterReceiver();
    } */
    private void TestBack() {
        Log.d(TAG,"onBackPressed");
        if (!SendingMessage("home","124")) {
            Log.d(TAG,"Sending   MEssage Failure in MainActivity");
            finish();
        }
        {
            Log.d(TAG,"GoBack to Home Activity");
            //Intent intent= new Intent(this, HomeActivity.class);
            //Intent intent= new Intent(this, HomeEnrollActivity.class);
            Intent intent= new Intent(this, Main2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            this.startActivity(intent);
            //  finish();
            //mVCOM.Close();
        }

    }

    @Override
    public void notifySuccess(String requestType, Object response) {

        Log.d(TAG, "SendGetAllUsers - notifySuccess: "+requestType+" Response is"+response);
        //GlobalVariables.FirstUser = FirstUser_Cap;
        //fingerRemove(60000);

        if (requestType.equals("501")) {
            //Process the
            try {
                Log.d(TAG, "Got all the Users data ");
                Log.d(TAG, "Befor Gson");
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<lumidigm.captureandmatch.entity.User>>() {
                }.getType();
                Log.d(TAG, "Befor allTasks data");
                List<lumidigm.captureandmatch.entity.User> allTasks = gson.fromJson(response.toString(), listType);
                Log.d(TAG, "notifySuccess: " + allTasks.size());
                appDatabase.userDao().deleteAll();
                Log.d(TAG, "After deleteAll");
                appDatabase.userDao().insertAll(allTasks);

                //Now needs to check whether that Finger Print Matches the User Enrolled
                //          new CheckPreviousCaptureFingerMatchTask().execute(0);
            } catch (Exception obj) {

            }
        } else {
            try {
                JSONObject obj = new JSONObject(response.toString());
                String ldapuser;
                // JSONObject jobject = new JSONObject(response.toString());
                //if (obj != null)
                {
                    ldapuser = obj.optString("uid");
                    Log.d(TAG, "user is ldap " + ldapuser);
                    String UIDNum = obj.optString("uidNumber");

                    Log.d(TAG, "LDAP User ID is " + ldapuser + "  UID is " + UIDNum);
                    if (ldapuser != null) {
                        Log.d(TAG, "LDAP User is matching  " + ldapuser);

                        GlobalVariables.FirstUser = true; //FirstUser_Cap;
                        GlobalVariables.loggedInUser = ldapuser;
                        Log.d(TAG, "First User is " + GlobalVariables.loggedInUser);
                        //                fingerRemove(60000);
                    } else {
                        //              ProcessfingerNotMatch();
                        Log.d(TAG, "Enroll Failure as User is Not Available ");
                    }
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got Exception obj" + obj.getMessage());
            }
        }
        MyProgressDialog.dismiss();
    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getCause());
        Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getMessage());
        try {
            //String fingerprint = "testfingerprint";
            Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
            //  String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
            //           playAudioFile(2,filePath);
            MyProgressDialog.dismiss();
            //ProcessfingerNotMatch();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    public void DownloadUserData() {
        Log.d(TAG,"It is DownloadUserData");
        MyProgressDialog.show(TestFingerScannActivity.this, R.string.wait_message);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG,"Sending Get User Data ");
                //SendingMessageWithState("finger","101","fingersuccess");
                String url = GlobalVariables.SSOIPAddress + GET_ALL_USERS;
                Log.d(TAG,"Login url is "+url);
                // new VolleyService(TestFingerScannActivity.this, TestFingerScannActivity.this).tokenBase(GET, url , null, "501");
                new VolleyService(TestFingerScannActivity.this, TestFingerScannActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");


            }
        }, 500);

    }
}

