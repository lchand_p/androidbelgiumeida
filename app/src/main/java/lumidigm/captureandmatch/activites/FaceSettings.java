package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.luxand.facerecognition.MainActivity;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import digit9home.apps.manager.HomeScreen;
import frost.AdminVerificationActivityMobile;
import frost.EnrollActivity;
import frost.UserIdActivity;
import lumidigm.captureandmatch.R;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static webserver.GlobalVariables.FaceDetectionDetaineeIdentificationforV4;
import static webserver.GlobalVariables.selectedProduct;

public class FaceSettings extends Activity implements View.OnClickListener {

//public class FaceSettings extends Activity  {


    private LinearLayout mainlayut;
    private Button settings;
    private Button enrollbutton;
    private Button enrollbasic;

    private String TAG="Settings";
    private TextView enrollView=null;
    private Button workflowbutton;
    private TextView workflowView=null;
    AlertDialog alertDialog =null;
    private Button back=null;
    private Button HomeApps=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facesettings);


       //  enrollbutton=(Button)findViewById(R.id.enrolladmin);

        Log.d("FaceSettings","oncreate ");
       // mainlayut = (LinearLayout) findViewById(R.id.mainlayut);
        ///imageHint = (ImageView) findViewById(R.id.image_hint);
        settings = (Button) findViewById(R.id.settings_new);
        //View overlay = findViewById(R.id.mylayout);

        enrollbasic=(Button)findViewById(R.id.enrollbasic);
        enrollView=(TextView) findViewById(R.id.enrollView);

        workflowbutton=(Button)findViewById(R.id.configureworkflow);
        workflowView= (TextView) findViewById(R.id.configureworkflowtxtview);
        settings.setOnClickListener(this);

        back=(Button)findViewById(R.id.home3);
        back.setOnClickListener(this);


        HomeApps=(Button)findViewById(R.id.homeapps);

        workflowbutton.setVisibility(View.INVISIBLE);

        workflowView.setVisibility(View.INVISIBLE);

        if (GlobalVariables.FaceEnrollmentStatus == false)
            enrollbasic.setVisibility(View.INVISIBLE);

        if (HomeApps != null)
            HomeApps.setOnClickListener(this);

        Log.d(TAG,"Work Flow Status Val is  "+GlobalVariables.WorkFlowVal);
  //      if ( GlobalVariables.WorkFlowVal==2 || GlobalVariables.WorkFlowVal==3 ) {

            //enrollbutton.setOnClickListener(this);
            enrollbasic.setOnClickListener(this);
    /*    } else {
            enrollbasic.setVisibility(View.INVISIBLE);
            enrollView.setVisibility(View.INVISIBLE);
        }
*/
        Log.d(TAG,"WorkFlow Configure Status is "+GlobalVariables.mobileWorkFlowConfigure);
/*
        if (GlobalVariables.mobileWorkFlowConfigure == false) {
            workflowbutton.setVisibility(View.INVISIBLE);
            workflowView.setVisibility(View.INVISIBLE);
        } else*/ {
            Log.d(TAG,"EnableWork Flow button ");
            workflowbutton.setOnClickListener(this);
        }

        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
          /*  case R.id.enrolladmin:
                Log.d("Face","Enroll Admin ");
                Intent intent= new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("Enrolment",false);
                mBundle.putBoolean("adminVerification",true);
               // intent.putExtra("userid", editText.getText().toString());
                intent.putExtras(mBundle);
                startActivity(intent);
                finishAffinity();
                break;*/
            case R.id.settings_new:
               /* check = false;
                Intent i1 = new Intent(this, UserAuthenticationActivity.class);
                startActivity(i1);*/
                //Toast.makeText(Main2Activity.this, "Super User Verification", Toast.LENGTH_SHORT).show();
                //startSettings();
                // GlobalVariables.supervisorUser=true;
               // startSettings();

             //   UserFaceActivity();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                //TODO implement
                break;
            case R.id.enrollbasic:
                Log.d("FaceSetting", "It is Enrollment "+GlobalVariables.WorkFlowVal+" ");
                //Means it is not basic enrollment
/*
                if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 4 || GlobalVariables.WorkFlowVal == 5) {
                    Log.d(TAG,"No Support of Enrolmment ");
                } else
                if ((GlobalVariables.WorkFlowVal == 2) ) {
                    Intent intent1 = new Intent(this, AdminVerificationActivityMobile.class);
                //    Intent intent1 = new Intent(this, EnrollActivity.class);
                    startActivity(intent1);
                }  else {
                    Log.d(TAG,"Sending EnrollTypeSelect "+GlobalVariables.WorkFlowVal);
                  //  new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "GoToEnroll", null, "test");


                    if (GlobalVariables.WorkFlowVal == 3 || (GlobalVariables.WorkFlowVal == 20))*/ {
                       // new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"AdminFingerVerification",null,"Initiated");
                    //if (GlobalVariables.supervisionEnrolmentStatus == false) {
                        Log.d(TAG, "Admin Verification is true ");
                        //Intent intent = new Intent(FaceSuccessActivity.this, Enroll.class);
                        Intent intent = new Intent(this, UserIdActivity.class);

                         intent.putExtra("adminVerification", true);
                        intent.putExtra("userid", "test");
                        intent.putExtra("fromsupervisor", true);

                        //intent.putExtra("userid", personID);
                        startActivity(intent);
                        finishAffinity();
                    }
                //}

                break;
            case R.id.configureworkflow:
                Log.d(TAG,"Configure Work Flow is  Starting ");
                Intent i1 = new Intent(this, WorkflowConfigure.class);

                if (selectedProduct == 2 && FaceDetectionDetaineeIdentificationforV4 == true)
                    i1 = new Intent(this, SettingsActivity.class);

                startActivity(i1);

                break;
            case R.id.home3:
                Log.d(TAG,"Clicked Home ");
                Intent i = new Intent(this, Main2Activity.class);
                if (GlobalVariables.FaceandFinger)
                    i = new Intent(this, FaceTabLaunchActivity.class);

                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finishAffinity();

                break;

            case R.id.homeapps:
                Log.d(TAG,"It is Home Apps");
                Intent i2 = new Intent(this, HomeScreen.class);
                  i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i2);
                finishAffinity();
            default:
                break;
        }
    }
    private void startSettings() {

        Log.d(TAG,"In SHow Licence App");
        LayoutInflater inflater = LayoutInflater.from(FaceSettings.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Android Settings");
        builder.setMessage("Password Verification");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordText = subEditText.getText().toString().trim();
                Log.d(TAG,"Password Text is "+passwordText);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                String settingPwd = getsettingspwd();
                Log.d(TAG,"App Pwd is "+settingPwd);
                if (passwordText.equals("digit9@542")) {
                    Log.d(TAG,"Going for Shutdown");
                    //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
                    //Shutdown();
                } else
                if (passwordText.equals("digit9@543")) {
                    Log.d(TAG,"Going for Reboot");
                    //reboot();
                } else
                if (passwordText.equals(settingPwd)) {

                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    finish();

                } /*else //Kept for Testing by Chand - remove it
                    if (TestWithoutUSB(passwordText) == null)
                        Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();*/
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

                    // finish();
                }
            }
        });



        builder.show();
    }
    private String getsettingspwd() {
        String verVal=null;

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "settingspwd.txt");

            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(
                    fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            verVal = aBuffer.toString().trim();

            Log.d(TAG,"Reading settings pwd from sdfile is "+verVal+" len s "+verVal.length());
            myReader.close();
            if ((verVal != null)  && (verVal != " ") && (verVal != "") && verVal.length() > 0)
                return verVal;
        } catch (Exception obj) {
            Log.d(TAG,"Got exception while reading settings pwd");

        }
        return "digit9@123";
    }
    public void UserFaceActivity(){
        // Intent intent = new Intent(this,UserFaceActivity.class);
        // startActivity(intent);
/*        Log.d(TAG,"It is User Face Activity ");
        //  Intent intent= new Intent(this, MainActivity.class);
        Intent intent= new Intent(this, MainActivity_Sentinel.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        Bundle mBundle = new Bundle();

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut",null,"UserFingerComeOut");
        String userid ="9";
        mBundle.putBoolean("Enrolment",true);
        intent.putExtra("userid", userid);
        Bundle bn = new Bundle();
        bn.putBoolean("check", true);
     //   if (user != null)
       //     bn.putParcelable("userData", user);
        intent.putExtras(mBundle);
        Log.d(TAG,"User ID is "+userid);
        startActivity(intent);*/

        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"AdminFingerVerification",null,"Initiated");
        //if (GlobalVariables.supervisionEnrolmentStatus == false) {
        Log.d(TAG, "Admin Verification is true ");
        //Intent intent = new Intent(FaceSuccessActivity.this, Enroll.class);
        Intent intent = new Intent(this, UserIdActivity.class);

        intent.putExtra("adminVerification", true);
        intent.putExtra("userid", "test");
        intent.putExtra("fromsupervisor", true);

        //intent.putExtra("userid", personID);
        startActivity(intent);
        finishAffinity();

    }
}
