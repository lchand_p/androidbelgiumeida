package lumidigm.captureandmatch.activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.List;

import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import database.AppDatabase;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.vcomdroid.ANSI378TemplateHelper;
import lumidigm.vcomdroid.Minutiae;
import lumidigm.vcomdroid.VCOM;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.GET;
import static lumidigm.constants.Constants.GET_ASSIGNED_TASKS;

public class VerifyEnrolSuccuessorNotActivity extends Activity implements View.OnClickListener {

  private static final String TAG = "VerifyEnrolSuccuessorNo";
  private TextView msg;
  private LinearLayout centerImage;
  private ImageView imageview;
  private TextView hint;
  String userid;


  private VCOM mVCOM;
  public static boolean check=true;
  public static boolean toastcheck=true;
  Button scanagain;
  User user;
  String ldapUser = null;
  SharedPreferences prefs=null;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.verify_enrol_succuessor_not);

    try {
      userid = getIntent().getExtras().getString("userid");
      user = getIntent().getExtras().getParcelable("userData");
      //ldapUser = getIntent().getExtras().getString("ldapUser");

    } catch (Exception obj) {

    }

    try {
      //   userid = getIntent().getExtras().getString("userid");
      // user = getIntent().getExtras().getParcelable("userData");
      ldapUser = getIntent().getExtras().getString("ldapUser");

    } catch (Exception obj) {

    }
    try {
      Log.d(TAG, "on Create ");
      View decorView = getWindow().getDecorView();
      decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
              | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
              | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_FULLSCREEN
              | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    } catch (Exception obj) {
      Log.d(TAG,"Got Exception while setting Full Screen");
    }

    Log.d(TAG,"User ID is "+userid+" ldapUser "+ldapUser);
    View overlay = findViewById(R.id.mylayout);


    prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
            MODE_PRIVATE);
    GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

    overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    // appDatabase = AppDatabase.getAppDatabase(this);
    mVCOM = new VCOM(this);/* new VCOM.PermissionSuccuess() {
            @Override
            public void permissionsSuccuess() {
                new CaptureAndMatchTask().execute(0);
            }

            @Override
            public void onPermissionAlradyDone() {
                new CaptureAndMatchTask().execute(0);
            }
        });*/
    //
    //scanagain=(Button)findViewById(R.id.scan_again);

        /*msg = (TextView) findViewById(R.id.msg);
        if (ldapUser != null)
            msg.setText("Employee #" + userid + " "+ldapUser);
        else
        msg.setText("Employee #" + userid + " "+user.getFullname());
*/
    //centerImage = (LinearLayout) findViewById(R.id.center_image);
    imageview = (ImageView) findViewById(R.id.imageView10);
    //  hint = (TextView) findViewById(R.id.hint);
    findViewById(R.id.cancel).setOnClickListener(this);

/*        if (ldapUser != null) {
            hint.setText(Html.fromHtml("<b>"
                    + ldapUser + "," + "</b>" + "<br />" + "<small>" + "Please scan to authorise this enrolment"
                    + "</small>" + "<br />"));
            SendingMessageWeb("fingerenroll","enrollmentsuccess",ldapUser);
        }
        else*/ {
           /* hint.setText(Html.fromHtml("<b>"
                    + user.getFullname() + "," + "</b>" + "<br />" + "<small>" + "Please scan to authorise this enrolment"
                    + "</small>" + "<br />"));
*/
      Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
        @Override
        public void run() {
          new CaptureAndMatchTask().execute();
        }
      }, 3000);
      //new CaptureAndMatchTask().execute(0);

          /*  scanagain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    scanagain.setVisibility(View.GONE);
                    imageview.setImageResource(R.drawable.enroll_succuess_ornot);

                    new CaptureAndMatchTask().execute(0);
                }
            });*/
    }
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.cancel:
        //TODO implement
        Intent i = new Intent(this, Main2Activity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        break;
    }
  }
  @Override
  protected void onResume() {
    super.onResume();
    check=true;
       /* if(check)
            new CaptureAndMatchTask().execute(0);*/
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (mVCOM != null)
      mVCOM.Close();

  }
  private class CaptureAndMatchTask extends AsyncTask<Integer, Integer, User> {

    @Override
    protected User doInBackground(Integer... i) {
      if(check){
        int rc;
        rc = mVCOM.Open();
        if (rc == 0) {

          rc = mVCOM.Capture();

          if (rc == 0) {
            int tmplSize = mVCOM.GetTemplateSize();
            if (tmplSize > 0) {
              byte[] tmpl = new byte[tmplSize];
              rc = mVCOM.GetTemplate(tmpl);
              if (rc == 0) {

                toastcheck=true;

                //List<User> users = appDatabase.userDao().getAll();


                Log.d(TAG, "doInBackground: " + user.toString());
                String fingarprint1 = user.getTemplate1();
                byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                String fingarprint2 = user.getTemplate2();
                byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

                String fingarprint3 = user.getTemplate3();
                byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);


                rc = mVCOM.Match(tmpl, mTmplCapture1);
                int score1 = mVCOM.GetMatchScore();
                rc = mVCOM.Match(tmpl, mTmplCapture2);
                int score2 = mVCOM.GetMatchScore();
                rc = mVCOM.Match(tmpl, mTmplCapture3);
                int score3 = mVCOM.GetMatchScore();
                Log.d(TAG, "doInBackground: "+((score1 + score2 + score3) / 3));
                if (((score1 + score2 + score3) / 3) > GlobalVariables.MaxThresholdVal) {
                  return user;
                }




              }else{
                toastcheck=false;
              }

            }

          }else{
            toastcheck=false;
          }
          mVCOM.Close();
        }else{
          toastcheck=false;
        }
      }
      return null;
    }

    @Override
    protected void onPostExecute(User user1) {
      //  new CaptureAndMatchTask().execute(0);

      Log.d(TAG,"onPostExecute ");
      GetBitmap();
      if (user1 != null) {
     //   new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateVerifyResponse",null,"FingerPrint_Verify_Sucess");
        check=false;
        //scanagain.setVisibility(View.GONE);
        imageview.setImageResource(R.drawable.enroll_complete);
        //hint.setText("Enrolment Complete!");
        //SendingMessageWeb("fingerenroll","enrollmentsuccess",null);
        Log.d(TAG,"FingerPrint Scan is success ");
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {
                        /*Intent i = new Intent(VerifyEnrolSuccuessorNotActivity.this, Main2Activity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();*/
            // SendingMessageWeb("home","fingercomplete",null);

            Log.d(TAG,"Clearing all the Activities");
                        Intent i = new Intent(VerifyEnrolSuccuessorNotActivity.this, LaunchActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();

            finishAffinity();
          }
        },5000);


        //SendingMessageWeb("home","fingercomplete");




      } else {
        Log.d(TAG,"EnrollmentFailure ");
        check=true;
      //  new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateVerifyResponse",null,"FingerPrint_Verify_Failure");

                /*if (ldapUser != null) {
                    msg.setText("Employee #" + userid + " "+ldapUser);
                    msg.setText(Html.fromHtml("Employee #" + userid + " "+ldapUser +"<br />"+"<font color=#cc0029>" + "<small>" + "NOT ENROLLED"
                            + "</small>" + "<br />" ));
                    scanagain.setVisibility(View.VISIBLE);
                    imageview.setImageResource(R.drawable.scan_error);
                    hint.setText(Html.fromHtml("Authorisation scan" +"<br />"+"<font color=#cc0029>" + "<small>" + "doesn't match"
                            + "</small>" + "<br />"+ ldapUser ));
                } else {

                    msg.setText("Employee #" + userid + " " + user.getFullname());
                    msg.setText(Html.fromHtml("Employee #" + userid + " " + user.getFullname() + "<br />" + "<font color=#cc0029>" + "<small>" + "NOT ENROLLED"
                            + "</small>" + "<br />"));
                    scanagain.setVisibility(View.VISIBLE);
                    imageview.setImageResource(R.drawable.scan_error);
                    hint.setText(Html.fromHtml("Authorisation scan" + "<br />" + "<font color=#cc0029>" + "<small>" + "doesn't match"
                            + "</small>" + "<br />" + user.getFullname()));
                }
                SendingMessageWeb("fingerenroll","enrollmentfailure",ldapUser);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //my_button.setBackgroundResource(R.drawable.defaultcard);
                        //finishAffinity();
                        SendingMessageWeb("home","fingercomplete",ldapUser);

                    }
                }, 3000);
*/
        Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
          @Override
          public void run() {
            //my_button.setBackgroundResource(R.drawable.defaultcard);
            finishAffinity();
            //SendingMessageWeb("home","fingercomplete");

          }
        }, 3000);

      }
    }
  }

  private Bitmap GetBitmap() {
    int sizeX = mVCOM.GetCompositeImageSizeX();
    int sizeY = mVCOM.GetCompositeImageSizeY();
    if (sizeX > 0 && sizeY > 0) {
      Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
      int[] colors = new int[sizeX * sizeY];
      if (mVCOM.GetCompositeImage(colors) == 0) {
        bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
      } else {
        Log.i(TAG, "Unable to get composite image.");
      }
      return bm;
    } else {
      Log.i(TAG, "Composite image is too small.");
      // ...so we create a blank one-pixel bitmap and return it
      Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
      return bm;
    }
  }

  private byte[] GetTemplate() {
    int tmplSize = mVCOM.GetTemplateSize();
    if (tmplSize > 0) {
      byte[] tmpl = new byte[tmplSize];
      if (mVCOM.GetTemplate(tmpl) == 0) {
        return tmpl;
      } else {
        Log.i(TAG, "Unable to get template.");
      }
    } else {
      Log.i(TAG, "Template is too small.");
    }
    return new byte[0];
  }

  private void DrawMinutiae(Bitmap bm, byte[] tmpl) {
    int tmplSize = tmpl.length;
    if (tmplSize < 1)
      return;
    if (bm.getWidth() < 2 || bm.getHeight() < 2)
      return;

    ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
    Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
    Canvas canvas = new Canvas(bm);
    Paint paint = new Paint();
    for (int i = 0; i < minutiaeList.length; i++) {
      if (minutiaeList[i].nType == 1)
        paint.setARGB(0xFF, 0xFF, 0x00, 0x00);    // line-ending
      else
        paint.setARGB(0xFF, 0x00, 0xFF, 0x00);    // bifurcation
      int nX = minutiaeList[i].nX;
      int nY = minutiaeList[i].nY;
      canvas.drawCircle(nX, nY, 4, paint);
      double nR = minutiaeList[i].nRotAngle;
      int nX1;
      int nY1;
      nX1 = (int) (nX + (10.0 * Math.cos(nR)));
      nY1 = (int) (nY - (10.0 * Math.sin(nR)));
      canvas.drawLine(nX, nY, nX1, nY1, paint);
      canvas.drawLine(nX + 1, nY, nX1 + 1, nY1, paint);
      canvas.drawLine(nX - 1, nY, nX1 - 1, nY1, paint);
      canvas.drawLine(nX, nY + 1, nX1, nY1 + 1, paint);
      canvas.drawLine(nX, nY - 1, nX1, nY1 - 1, paint);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (mVCOM != null)
      mVCOM.Close();
  }
  private boolean SendingMessageWeb(String status, String requestType, String ldapuser) {
    Log.d(TAG,"SendMessage "+status);
    //String url="http://192.168.43.24:8080/?username="+status;


    String remoteIP = getRemoteIP();
    if (remoteIP == null || remoteIP == "") {
      Log.d(TAG," No IP ");
      return false;
    }
    if (!isEthernetConnected()) {
      if (!isConnectedInWifi()) {
        Log.d(TAG,"Network not connected");
        return false;
      }
    }


    Log.d(TAG,"Getting IP Address "+status+"  ");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
    //String url="http://"+remoteIP+":8080/?username="+status;

    String url="http://"+remoteIP+":8080/?username="+status+"&state="+requestType+"&tokenID="+userid;

    if (ldapuser != null)
      url="http://"+remoteIP+":8080/?username="+status+"&state="+requestType+"&ldapUser="+ldapuser+"&tokenID="+userid;
    Log.d(TAG, "send url: "+url);

    new VolleyService(new IResult() {
      @Override
      public void notifySuccess(String requestType, Object response) {

        Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
        //finish();


      }


      @Override
      public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "notifyError: "+error.getMessage());
        try {
          //String fingerprint = "testfingerprint";
          Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
          String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";

          //           playAudioFile(2,filePath);

        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    }, this).tokenBase(GET, url, null, requestType);

    return true;
  }
  public boolean isConnectedInWifi() {

    try {
      WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
      NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
      if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
              && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
        return true;
      }
    } catch (Exception obj) {
      Log.d(TAG,"Got exception "+obj.toString());
    }
    return false;
  }
  private Boolean isNetworkAvailable() {
    ConnectivityManager connectivityManager
            = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
  }

  public Boolean isWifiConnected(){
    if(isNetworkAvailable()){
      ConnectivityManager cm
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
      return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
    }
    return false;
  }

  public Boolean isEthernetConnected(){
    if(isNetworkAvailable()){
      ConnectivityManager cm
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
      return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
    }
    return false;
  }
  private String getRemoteIP() {
    String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
            "");

    Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

    return remoteIPAddr;
  }
  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

  }
}
