package lumidigm.captureandmatch.activites;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.ImageView;

import com.android.volley.VolleyError;
//import com.google.firebase.analytics.FirebaseAnalytics;
//import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import database.AppDatabase;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
//import lumidigm.captureandmatch.adapters.PendingTaskAdapter;
//import lumidigm.captureandmatch.database.AppDatabase;
import lumidigm.captureandmatch.entity.User;
import lumidigm.captureandmatch.models.AllTasks;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.utils.Alertwithtwobuttons;
import lumidigm.vcomdroid.ANSI378TemplateHelper;
import lumidigm.vcomdroid.Minutiae;
import lumidigm.vcomdroid.VCOM;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.PUT;
import static lumidigm.constants.Constants.GET_ASSIGNED_TASKS;
import static lumidigm.constants.Constants.LOGOUT;
//import static lumidigm.captureandmatch.constants.Constant.GET_ASSIGNED_TASKS;
//import static lumidigm.captureandmatch.constants.Constant.LOGOUT;

public class UserAuthenticationActivity extends Activity implements View.OnClickListener, IResult {

    private static final String TAG = "UserAuthenticationActiv";
    private TextView enrollUser;
    private LinearLayout figerprintLayout;
    private TextView textTemplate3;
   // private ImageView imageTemplate3;
   // private ImageView tickTemplate3;
    private TextView successText;
    //private LinearLayout update;
   // private LinearLayout myApp;

    // VCOM Java/JNI adapter.
    private VCOM mVCOM;
    // fingerprint images.
    private Bitmap mBitmapCapture;
    User user;
    Bitmap bitmap;

    Button yes;
    Button logout;
    AppDatabase appDatabase;
    LinearLayout logout_layout;
    boolean aBoolean;
    ArrayList<AllTasks> allTasks;
    private Timer homeTimer = null;
    private static boolean ActivityProcessed = false;
    private String user_ID = null;
    SharedPreferences  prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_authentication);
       if(getIntent().getExtras().containsKey("login"))
        aBoolean = getIntent().getExtras().getBoolean("login");

        View overlay = findViewById(R.id.mylayout);

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        appDatabase = AppDatabase.getAppDatabase(this);
  try {
      user = getIntent().getExtras().getParcelable("User");
      try {
          bitmap = getIntent().getExtras().getParcelable("bitmap");
      } catch (Exception obj) {
          bitmap = null;
      }
      } catch (Exception obj) {
      user = null;
      bitmap=null;
  }
        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        try {
           // user_ID= getIntent().getExtras().getParcelable("User_ID");
            //fromWeb = getIntent().getBooleanExtra("FromWeb", false);
            user_ID = getIntent().getStringExtra("User_ID");

            Log.d(TAG,"User ID is "+user_ID);
        } catch (Exception obj) {
            user_ID=null;
        }
       // enrollUser = (TextView) findViewById(R.id.enroll_user);
        figerprintLayout = (LinearLayout) findViewById(R.id.figerprint_layout);
        textTemplate3 = (TextView) findViewById(R.id.text_template3);
       // imageTemplate3 = (ImageView) findViewById(R.id.image_template3);
      //  tickTemplate3 = (ImageView) findViewById(R.id.tick_template3);
        successText = (TextView) findViewById(R.id.success_text);
       // update = (LinearLayout) findViewById(R.id.update);
       // myApp = (LinearLayout) findViewById(R.id.my_app);

       // myApp.setOnClickListener(this);
       // tickTemplate3.setVisibility(View.INVISIBLE);
        logout_layout = (LinearLayout) findViewById(R.id.logout_layout);

        yes = (Button) findViewById(R.id.yes);
        logout = (Button) findViewById(R.id.logout);
        yes.setOnClickListener(this);
        logout.setOnClickListener(this);
        // successText.setText("Place your finger on the sensor");


        if (user != null) {
            Log.d(TAG,"USER Details is  "+user.getFullname());

            textTemplate3.setText("Welcome " + user.getFullname());


            Log.d(TAG,"ABoolean value is "+aBoolean);
            if (aBoolean) {
                successText.setText("Access granted please proceed");
                logout_layout.setVisibility(View.GONE);

                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
  /*                      Intent i = new Intent(UserAuthenticationActivity.this, AllTasksActivity.class);
                        Bundle bn = new Bundle();

                        bn.putParcelable("User", user);
                        i.putExtras(bn);
                        startActivity(i);
                        if (homeTimer != null)
                            homeTimer.cancel();

                        homeTimer = null;
                        finish();
  */                  }
                },5000);



            } else {


                //SharedPreferences prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                String URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id();
                int TerminalId = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                Log.d(TAG,"Terminal ID is "+TerminalId);
                if (TerminalId != -1) {
                    URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id()+"/"+TerminalId;
                } else
                    Log.d(TAG,"Terminal ID is -1 ");

                Log.d(TAG,"Get Assigned Tasks URL is "+URL);

                MyProgressDialog.show(this, R.string.wait_message);
                new VolleyService(UserAuthenticationActivity.this, UserAuthenticationActivity.this).tokenBaseB(GET, URL, null, "100");



               /* successText.setText("You have pending tickets. Do you want to update the status?");
                logout_layout.setVisibility(View.VISIBLE);*/
            }

            UserAthenticationIdleTimerTask();

         /*   Bitmap mBitmapMatch = GetBitmap();
            byte[] mTmplMatch = GetTemplate();
            DrawMinutiae(mBitmapMatch, mTmplMatch);*/

           // imageTemplate3.setImageBitmap(bitmap);
            //tickTemplate3.setVisibility(View.VISIBLE);


        } else  {
            //COming from Web
            if (user_ID != null) {
                MyProgressDialog.show(this, R.string.wait_message);
                Log.d(TAG,"Going for "+GlobalVariables.SSOIPAddress +GET_ASSIGNED_TASKS + user_ID);

                //SharedPreferences  prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                  //      MODE_PRIVATE);
                //SharedPreferences prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);

                int TerminalId = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                Log.d(TAG,"Terminal ID is "+TerminalId);

                String URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id();

                if (TerminalId != -1) {
                    URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id()+"/"+TerminalId;
                } else
                    Log.d(TAG,"Terminal id is -1"+TerminalId);
                Log.d(TAG,"Get Assigned Tasks URL is "+URL);

                new VolleyService(UserAuthenticationActivity.this, UserAuthenticationActivity.this).tokenBaseB(GET, URL, null, "100");
                UserAthenticationIdleTimerTask();
            }
        }





/*

        mVCOM = new VCOM(this, new VCOM.PermissionSuccuess() {
            @Override
            public void permissionsSuccuess() {
                new CaptureAndMatchTask().execute(0);
            }

            @Override
            public void onPermissionAlradyDone() {
                new CaptureAndMatchTask().execute(0);
            }
        });
*/


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

          /*  case R.id.my_app:
                Intent i = new Intent(this, AllTasksActivity.class);
                Bundle bn = new Bundle();

                bn.putParcelable("User", user);
                i.putExtras(bn);
                startActivity(i);


                break;*/
            case R.id.yes:

    /*            Intent i1 = new Intent(this, PendingTasks.class);
                Bundle bn1 = new Bundle();
                bn1.putParcelable("User", user);
                i1.putExtras(bn1);
                startActivity(i1);
                if (homeTimer != null)
                    homeTimer.cancel();

                homeTimer = null;
                finish();*/
                break;
            case R.id.logout:
                Alertwithtwobuttons alertwithtwobuttons = new Alertwithtwobuttons(this, new Alertwithtwobuttons.OnClick() {
                    @Override
                    public void onClickOk() {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("_id", user.get_id());
                            Date currentTime = Calendar.getInstance().getTime();

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            String currentDateandTime = sdf.format(new Date());
                            jsonObject.put("time", currentDateandTime);
                            jsonObject.put("userId", user.getUserId());
                            jsonObject.put("username", user.getUsername());
                            /*if (allTasks!=null &&allTasks.size() > 0) {
                                ArrayList<AllTasks> arrayList = compar(allTasks);
                                if (arrayList.size() > 0) {
                                    jsonObject.put("status", 2);
                                    jsonObject.put("ticket_id", arrayList.get(0).get_id());
                                }
                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "onClickOk: " + jsonObject.toString());
                        MyProgressDialog.show(UserAuthenticationActivity.this, R.string.wait_message);
                        new VolleyService(UserAuthenticationActivity.this, UserAuthenticationActivity.this).tokenBase(PUT, GlobalVariables.SSOIPAddress +LOGOUT, jsonObject, "101");

                    }

                    @Override
                    public void cancelButtonClick() {

                    }
                });

                alertwithtwobuttons.show();


                break;
        }
    }

    public ArrayList<AllTasks> compar(List<AllTasks> allTasks) {
        ArrayList<AllTasks> allTask = new ArrayList<>();
        for (AllTasks allTasks1 : allTasks) {

            if (allTasks1.getStatus() == 1) {
                allTask.add(allTasks1);
            }


        }
        return allTask;
    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        MyProgressDialog.dismiss();
        if (requestType.equals("101")) {
            Log.d(TAG, "notifySuccess: " + response);
            user.setStatus("offline");
            appDatabase.userDao().update(user);
            Intent i = new Intent(UserAuthenticationActivity.this, Main2Activity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            if (homeTimer != null)
                homeTimer.cancel();

            homeTimer = null;
            finish();
        } else if (requestType.equals("100")) {
            Log.d(TAG, "notifySuccess: " + response.toString());

            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<AllTasks>>() {
            }.getType();
            allTasks = gson.fromJson(response.toString(), listType);
            Log.d(TAG, "notifySuccess: " + allTasks.size());
            ArrayList<AllTasks> allTasks1 = compar(allTasks);
            ArrayList<AllTasks> allTasks2 = compar1(allTasks);

            Log.d(TAG,"All Tasks Size is  "+allTasks.size());

            if (allTasks.size() > 0) {
                Log.d(TAG,"All Tasks size more than zero - select door");
                SelectDoor();
            } else
            if (allTasks1.size() > 0 || allTasks2.size() > 0) {

                //successText.setText("You have pending tickets. Do you want to update the status?");
                logout_layout.setVisibility(View.VISIBLE);
            }else{
                //successText.setText("Access granted please proceed");
                logout_layout.setVisibility(View.GONE);

                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        /*Intent i = new Intent(UserAuthenticationActivity.this, AllTasksActivity.class);
                        Bundle bn = new Bundle();

                        bn.putParcelable("User", user);
                        i.putExtras(bn);
                        startActivity(i);
                        if (homeTimer != null)
                            homeTimer.cancel();

                        homeTimer = null;
                        finish();*/
                    }
                },5000);
            }
        }
    }
    public ArrayList<AllTasks> compar1(List<AllTasks> allTasks) {
        ArrayList<AllTasks> allTask = new ArrayList<>();
        for (AllTasks allTasks1 : allTasks) {

            if (allTasks1.getStatus() == 1) {
                allTask.add(allTasks1);
            }


        }
        return allTask;
    }
    @Override
    public void notifyError(String requestType, VolleyError error) {
        MyProgressDialog.dismiss();
        Log.d(TAG, "notifyError: " + error.toString());
    }

    private class CaptureAndMatchTask extends AsyncTask<Integer, Integer, User> {
        @Override
        protected User doInBackground(Integer... i) {
            int rc;
            rc = mVCOM.Open();
            if (rc == 0) {
                rc = mVCOM.Capture();
                if (rc == 0) {
                    int tmplSize = mVCOM.GetTemplateSize();
                    if (tmplSize > 0) {
                        byte[] tmpl = new byte[tmplSize];
                        rc = mVCOM.GetTemplate(tmpl);
                        if (rc == 0) {

                            AppDatabase appDatabase = AppDatabase.getAppDatabase(UserAuthenticationActivity.this);

                            List<User> users = appDatabase.userDao().getAll();

                            for (User user : users) {

                                String fingarprint1 = user.getTemplate1();
                                byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                String fingarprint2 = user.getTemplate2();
                                byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

                                String fingarprint3 = user.getTemplate3();
                                byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);


                                rc = mVCOM.Match(tmpl, mTmplCapture1);
                                int score1 = mVCOM.GetMatchScore();
                                rc = mVCOM.Match(tmpl, mTmplCapture2);
                                int score2 = mVCOM.GetMatchScore();
                                rc = mVCOM.Match(tmpl, mTmplCapture3);
                                int score3 = mVCOM.GetMatchScore();
                                if (((score1 + score2 + score3) / 3) > GlobalVariables.MaxThresholdVal) {
                                    return user;
                                }

                            }

                        }

                    }
                }
                mVCOM.Close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(User rc) {

            if (rc != null) {
                successText.setText("Thanks you... Have a nice day");
            //    enrollUser.setText("Welcome " + rc.getFullname());
                Bitmap mBitmapMatch = GetBitmap();
                byte[] mTmplMatch = GetTemplate();
                DrawMinutiae(mBitmapMatch, mTmplMatch);

               // imageTemplate3.setImageBitmap(mBitmapMatch);
               // tickTemplate3.setVisibility(View.VISIBLE);

            } else {
                successText.setText("your finger is not matched, try again");
            }

/*

            mBitmapMatch = GetBitmap();
            mTmplMatch = GetTemplate();
            DrawMinutiae(mBitmapMatch, mTmplMatch);


            if (mVCOM.GetMatchScore() > 50000) {


                figerprint.setImageResource(R.drawable.fingerprint_green);

            } else {
                figerprint.setImageResource(R.drawable.redfingar);


            }

            hintLayout.setVisibility(View.VISIBLE);
            v.setVisibility(View.VISIBLE);

*/

        }


    }

    @Override
    public void onResume() {
        super.onResume();
//        FirebaseCrash.logcat(Log.ERROR, TAG, "NPE caught");

//        FirebaseAnalytics.getInstance(this).setCurrentScreen(this, "Goto Cart", this.getClass().getSimpleName());
    }

    private Bitmap GetBitmap() {
        int sizeX = mVCOM.GetCompositeImageSizeX();
        int sizeY = mVCOM.GetCompositeImageSizeY();
        if (sizeX > 0 && sizeY > 0) {
            Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
            int[] colors = new int[sizeX * sizeY];
            if (mVCOM.GetCompositeImage(colors) == 0) {
                bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
            } else {
                Log.i(TAG, "Unable to get composite image.");
            }
            return bm;
        } else {
            Log.i(TAG, "Composite image is too small.");
            // ...so we create a blank one-pixel bitmap and return it
            Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            return bm;
        }
    }

    private byte[] GetTemplate() {
        int tmplSize = mVCOM.GetTemplateSize();
        if (tmplSize > 0) {
            byte[] tmpl = new byte[tmplSize];
            if (mVCOM.GetTemplate(tmpl) == 0) {
                return tmpl;
            } else {
                Log.i(TAG, "Unable to get template.");
            }
        } else {
            Log.i(TAG, "Template is too small.");
        }
        return new byte[0];
    }

    private void DrawMinutiae(Bitmap bm, byte[] tmpl) {
        int tmplSize = tmpl.length;
        if (tmplSize < 1)
            return;
        if (bm.getWidth() < 2 || bm.getHeight() < 2)
            return;

        ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
        Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint();
        for (int i = 0; i < minutiaeList.length; i++) {
            if (minutiaeList[i].nType == 1)
                paint.setARGB(0xFF, 0xFF, 0x00, 0x00);    // line-ending
            else
                paint.setARGB(0xFF, 0x00, 0xFF, 0x00);    // bifurcation
            int nX = minutiaeList[i].nX;
            int nY = minutiaeList[i].nY;
            canvas.drawCircle(nX, nY, 4, paint);
            double nR = minutiaeList[i].nRotAngle;
            int nX1;
            int nY1;
            nX1 = (int) (nX + (10.0 * Math.cos(nR)));
            nY1 = (int) (nY - (10.0 * Math.sin(nR)));
            canvas.drawLine(nX, nY, nX1, nY1, paint);
            canvas.drawLine(nX + 1, nY, nX1 + 1, nY1, paint);
            canvas.drawLine(nX - 1, nY, nX1 - 1, nY1, paint);
            canvas.drawLine(nX, nY + 1, nX1, nY1 + 1, paint);
            canvas.drawLine(nX, nY - 1, nX1, nY1 - 1, paint);
        }
    }


    public void visiblelayout(boolean checck) {


        if (checck) {
            logout_layout.setVisibility(View.VISIBLE);
        } else {
            logout_layout.setVisibility(View.GONE);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (homeTimer != null)
            homeTimer.cancel();

        homeTimer = null;
 /*   if (mVCOM != null)
            mVCOM.Close();*/
    }
    //When User is not clicking, Go to Home Screen
    private  void  UserAthenticationIdleTimerTask() {
        //Showing the Progress
        Log.d(TAG,"User Authentication Starting the IdleTimerTask ");
        if (homeTimer == null ) {
            //    Toast.makeText(getApplicationContext(),"Timer Started - on kiosk ", Toast.LENGTH_SHORT).show();

            Log.d(TAG,"User Authentication Starting the IdleTimerTask ");

            homeTimer = new Timer();
            homeTimer.scheduleAtFixedRate(new TimerTask() {

                                              @Override
                                              public void run() {
            if (!ActivityProcessed)
            {
                   Log.d(TAG, "UserAuthentication Timer expired ");
                   UserAuthenticationActivity.this.runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                      Log.d(TAG, "UserAuthentication Timer Running ");
                      Intent i = new Intent(UserAuthenticationActivity.this, Main2Activity.class);
                      i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                      startActivity(i);
                      finish();
                      }
                                                      });
                                                  }
                                              }
                                          },
                    20000,
                    20000);
        } else
            Log.d(TAG,"Not Starting InternetCheck Timer ");
    }
    private void SelectDoor()
        {
            int position=0;

            try {
                Log.d(TAG,"All Tasks List");
                Log.d(TAG,"All Task List is "+allTasks.get(position).getSubject());

                if (allTasks.get(position).getSubject().equals("New") || allTasks.get(position).equals("Open")) {


                } else {
                    // integerAllTasksMap.clear();

                    // integerAllTasksMap.put(position, allTasksList.get(position));



                    Intent i = new Intent(UserAuthenticationActivity.this, SelectDoor.class);
                    Bundle bn = new Bundle();
                    Log.d(TAG,"Sending SelectRack "+position+"  User is "+user);
                    Log.d(TAG,"Sending SelectRack "+allTasks.get(position));
                    Log.d(TAG,"Sending SelectRack for Zero task "+allTasks.get(0));
                    bn.putParcelable("task", allTasks.get(position));
                    bn.putParcelable("User", user);
                    i.putExtras(bn);
                    startActivity(i);
//Chand added finish
                    if (homeTimer != null)
                        homeTimer.cancel();

                    homeTimer = null;
                    finish();

                    // taskAdapter.selectedPosition(integerAllTasksMap);
                }
            } catch (Exception obj){
                Log.d(TAG,"Got Exception in All Task List");
            }


    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
}
