package lumidigm.captureandmatch.activites;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import lumidigm.captureandmatch.R;
import lumidigm.constants.Constants;
import lumidigm.vcomdroid.*;
import webserver.GlobalVariables;

public class MainActivity extends Activity {

	private static final String TAG = "[VCOM]";
	
	// VCOM Java/JNI adapter.
	private VCOM mVCOM;
	// fingerprint images.
	private Bitmap mBitmapCapture;
	private Bitmap mBitmapMatch;
	// fingerprint ANSI 378 templates.
	private byte [] mTmplCapture;
	private byte [] mTmplMatch;
	private SharedPreferences prefs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
				MODE_PRIVATE);
		GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

		// if the device is plugged in, this will attempt to obtain permission and/or open the device.
		mVCOM = new VCOM(this);
		HideCaptureImage();
		HideMatchImage();
		HideSpoofScoreCapture();
		HideSpoofScoreMatch();
		HideMatchScore();
		EnableMatchButton(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void EnableCaptureButton(boolean enabled)
	{
		Button button = (Button) findViewById(R.id.buttonCapture);
		button.setEnabled(enabled);
	}
	
	private void EnableMatchButton(boolean enabled)
	{
		Button button = (Button) findViewById(R.id.buttonMatch);
		button.setEnabled(enabled);
	}

	private void SetSpoofScoreCapture(int spoofScore)
	{
		TextView textView = (TextView) findViewById(R.id.textViewSpoofScoreCapture);
		textView.setText(String.format("Spoof score: %d", spoofScore));
	}
	
	private void SetSpoofScoreMatch(int spoofScore)
	{
		TextView textView = (TextView) findViewById(R.id.textViewSpoofScoreMatch);
		textView.setText(String.format("Spoof score: %d", spoofScore));
	}
	
	private void HideSpoofScoreCapture()
	{
		TextView textView = (TextView) findViewById(R.id.textViewSpoofScoreCapture);
		textView.setText("");
	}
	
	private void HideSpoofScoreMatch()
	{
		TextView textView = (TextView) findViewById(R.id.textViewSpoofScoreMatch);
		textView.setText("");
	}
	
	private void SetMatchScore(int matchScore)
	{
		TextView textView = (TextView) findViewById(R.id.textViewMatchScore);
		textView.setText(String.format("Match score: %d", matchScore));
	}
	
	private void HideMatchScore()
	{
		TextView textView = (TextView) findViewById(R.id.textViewMatchScore);
		textView.setText("");
	}
	
	private void SetErrorText(String s)
	{
		TextView textView = (TextView) findViewById(R.id.textViewMatchScore);
		textView.setText(s);
	}

	private void ShowCaptureImage()
	{
		((ImageView) findViewById(R.id.imageViewCapture)).setAlpha(0xFF);
	}

	private void ShowMatchImage()
	{
		((ImageView) findViewById(R.id.imageViewMatch)).setAlpha(0xFF);
	}
	
	private void HideCaptureImage()
	{
		((ImageView) findViewById(R.id.imageViewCapture)).setAlpha(0);
	}

	private void HideMatchImage()
	{
		((ImageView) findViewById(R.id.imageViewMatch)).setAlpha(0);
	}
	
	private void GUIBusy()
	{
		// we leave the image alone, here...
		EnableCaptureButton(false);
		EnableMatchButton(false);
		SetMatchScore(0);
	}
	
	public void onButtonCapture(View view)
	{
		new CaptureTask().execute(0);
		GUIBusy();
		HideCaptureImage();
		HideMatchImage();
		HideSpoofScoreCapture();
		HideSpoofScoreMatch();
		HideMatchScore();
	}
	
	public void onButtonMatch(View view)
	{
		new CaptureAndMatchTask().execute(0);
		GUIBusy();
		HideMatchImage();
		HideSpoofScoreMatch();
		HideMatchScore();
	}
	
	private class CaptureTask extends AsyncTask<Integer, Integer, Integer>
	{
		@Override
		protected Integer doInBackground(Integer... i)
		{
			int rc;
			rc = mVCOM.Open();
			if (rc == 0)
			{
				rc = mVCOM.Capture();
				mVCOM.Close();
			}
			return rc;
		}
		@Override
		protected void onPostExecute(Integer rc)
		{
			if (rc == 0)
			{
				mBitmapCapture = GetBitmap();
				mTmplCapture = GetTemplate();
				Log.d(TAG, "onPostExecute: "+new String(mTmplCapture));
				DrawMinutiae(mBitmapCapture, mTmplCapture);
				((ImageView) findViewById(R.id.imageViewCapture)).setImageBitmap(mBitmapCapture);
				ShowCaptureImage();
				SetSpoofScoreCapture(mVCOM.GetSpoofScore());
			}
			else
			{
				Log.i(TAG, String.format("Capture returned %d", rc));
				SetErrorText("There was a USB communication issue.  Please try again.");
			}
			EnableCaptureButton(true);
			EnableMatchButton(rc == 0);
		}
	}

	private class CaptureAndMatchTask extends AsyncTask<Integer, Integer, Integer>
	{
		@Override
		protected Integer doInBackground(Integer... i)
		{
			int rc;
			rc = mVCOM.Open();
			if (rc == 0)
			{
				rc = mVCOM.Capture();
				if (rc == 0)
				{
					int tmplSize = mVCOM.GetTemplateSize();
					if (tmplSize > 0)
					{
						byte [] tmpl = new byte[tmplSize];
						rc = mVCOM.GetTemplate(tmpl);
						if (rc == 0)
							rc = mVCOM.Match(tmpl, mTmplCapture);
					}
				}
				mVCOM.Close();
			}
			return rc;
		}
		@Override
		protected void onPostExecute(Integer rc)
		{
			if (rc == 0)
			{
				mBitmapMatch = GetBitmap();
				mTmplMatch = GetTemplate();
				DrawMinutiae(mBitmapMatch, mTmplMatch);
				((ImageView) findViewById(R.id.imageViewMatch)).setImageBitmap(mBitmapMatch);
				ShowMatchImage();
				SetSpoofScoreMatch(mVCOM.GetSpoofScore());
				SetMatchScore(mVCOM.GetMatchScore());
			}
			else
			{
				Log.i(TAG, String.format("Match returned %d", rc));
				SetErrorText("There was a USB communication issue.  Please try again.");
			}
			EnableCaptureButton(true);
			EnableMatchButton(true);
		}
	}
	
	private Bitmap GetBitmap()
	{
		int sizeX = mVCOM.GetCompositeImageSizeX();
		int sizeY = mVCOM.GetCompositeImageSizeY();
		if (sizeX > 0 && sizeY > 0)
		{
			Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
			int [] colors = new int[sizeX * sizeY];
			if (mVCOM.GetCompositeImage(colors) == 0)
			{
				bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
			}
			else
			{
				Log.i(TAG, "Unable to get composite image.");
			}
			return bm;
		}
		else
		{
			Log.i(TAG, "Composite image is too small.");
			// ...so we create a blank one-pixel bitmap and return it
			Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
			return bm;
		}
	}
	
	private byte [] GetTemplate()
	{
		int tmplSize = mVCOM.GetTemplateSize();
		if (tmplSize > 0)
		{
			byte [] tmpl = new byte[tmplSize];
			if (mVCOM.GetTemplate(tmpl) == 0)
			{
				return tmpl;
			}
			else
			{
				Log.i(TAG, "Unable to get template.");
			}
		}
		else
		{
			Log.i(TAG, "Template is too small.");
		}
		return new byte[0];
	}
	
    private void DrawMinutiae(Bitmap bm, byte[] tmpl)
    {
    	int tmplSize = tmpl.length;
    	if (tmplSize < 1)
    		return;
    	if (bm.getWidth() < 2 || bm.getHeight() < 2)
    		return;
    	
        ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
        Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint();
        for (int i = 0; i < minutiaeList.length; i++)
        {
            if(minutiaeList[i].nType == 1)
                paint.setARGB(0xFF, 0xFF, 0x00, 0x00);	// line-ending
            else
            	paint.setARGB(0xFF, 0x00, 0xFF, 0x00);	// bifurcation
            int nX = minutiaeList[i].nX;
            int nY = minutiaeList[i].nY;
            canvas.drawCircle(nX, nY, 4, paint);
            double nR = minutiaeList[i].nRotAngle;
            int nX1;
            int nY1;
            nX1 = (int)(nX + (10.0 * Math.cos(nR)));
            nY1 = (int)(nY - (10.0 * Math.sin(nR)));
            canvas.drawLine(nX,     nY,     nX1,     nY1,     paint);
            canvas.drawLine(nX + 1, nY,     nX1 + 1, nY1,     paint);
            canvas.drawLine(nX - 1, nY,     nX1 - 1, nY1,     paint);
            canvas.drawLine(nX,     nY + 1, nX1,     nY1 + 1, paint);
            canvas.drawLine(nX,     nY - 1, nX1,     nY1 - 1, paint);
        }
    }
}

