package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import database.AppDatabase;
import lumidigm.HomeEnrollActivity;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.captureandmatch.models.AllTasks;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.utils.Alertwithtwobuttons;
import lumidigm.vcomdroid.ANSI378TemplateHelper;
import lumidigm.vcomdroid.Minutiae;
import lumidigm.vcomdroid.VCOM;
import webserver.AndroidWebServer;
import webserver.DiscoverWebServer;
import webserver.GlobalVariables;
import webserver.USBService;
import webserver.WebserverBroadcastRxService;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.PUT;
import static lumidigm.constants.Constants.GET_ASSIGNED_TASKS;
import static lumidigm.constants.Constants.LOGOUT;
import static lumidigm.constants.Constants.SERVER_IPADDR;
import static lumidigm.constants.Constants.SSO_SERVER_DEVICE_STATUS;
import static lumidigm.constants.Constants.USER_LOGIN;
import static webserver.GlobalVariables.EnrollCount;
import static webserver.GlobalVariables.supervisionEnrolmentStatus;
import static webserver.GlobalVariables.supervisorUser;

public class SupervisiorIdentity2Activity extends Activity implements View.OnClickListener, IResult {

//Button Match;Button cap;

    private static final String TAG = "SupeIdentity";
    //Button settings;
    Button enroll;
    ImageView image2;

    // VCOM Java/JNI adapter.
    private VCOM mVCOM=null;
    Bitmap mBitmapMatch;
    User user;
    AppDatabase appDatabase;


    public static boolean check = true;
    public static boolean toastcheck = true;
    public static boolean notauthorized = false;
    //TextView textView;

    private Handler mHandler;
    //Button rescan, help;
    AlertDialog alertDialog =null;
    SharedPreferences prefs=null;
    private AndroidWebServer androidWebServer;
    private static boolean isStarted = false;
    private static final int DEFAULT_PORT = 8080;
    public boolean comeOutofApp=false;
    public boolean inCapture=false;
    public static boolean inCaptureTest=false;
    private Timer sendDeviceStatusTimer = null;
    private final int[] imageNamesnew={R.drawable.enroll, R.drawable.enroll_one,R.drawable.enroll_two,R.drawable.enroll_complete};
    private TextView txtView;
    //ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2_supervisor);
        appDatabase = AppDatabase.getAppDatabase(SupervisiorIdentity2Activity.this);
        View overlay = findViewById(R.id.mylayout);

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        mHandler = new Handler();
        //textView = (TextView) findViewById(R.id.welcome_text1);
        comeOutofApp= false;
        //textView.setOnClickListener(this);
        image2=(ImageView)findViewById(R.id.imageView11);
        //  settings = (Button) findViewById(R.id.settings_new);
        txtView = (TextView) findViewById(R.id.editText11);
        // enroll = (Button) findViewById(R.id.enroll2);
        //image1 = (ImageView) findViewById(R.id.imageView7);
        //  ReadSSOServerAddressDetails();
        if (GlobalVariables.SSOIPAddress == null)
            GlobalVariables.SSOIPAddress = SERVER_IPADDR;

        //   settings.setOnClickListener(this);

        //enroll.setOnClickListener(this);

        notauthorized=false;
        //rescan = findViewById(R.id.rescann);
        //rescan.setOnClickListener(this);
        //help = findViewById(R.id.help);
        //help.setOnClickListener(this);
        Log.d(TAG,"MainActivity oncreate VCOM Open");

        if (supervisionEnrolmentStatus|| supervisorUser) {
            if (!GlobalVariables.MobileDevice) {
                mVCOM = new VCOM(this);
                try {
                    Log.d(TAG,"Set Working Debug ");
                    mVCOM.SetWorkingDB(4);
                } catch (Exception obj) {
                    Log.d(TAG,"SetWorking DB exception ");
                }
            }
        }
        //mVCOM = null;
        /*, new VCOM.PermissionSuccuess() {
            @Override
            public void permissionsSuccuess() {
                new CaptureAndMatchTask().execute(0);
            }

            @Override
            public void onPermissionAlradyDone() {
                new CaptureAndMatchTask().execute(0);
            }
        });*/

        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }
        check=true;
        //imageView.setImageResource(R.drawable.finger_background);
        //textView.setText(getResources().getString(R.string.welcometext));
        //settings.setVisibility(View.VISIBLE);
        //enroll.setVisibility(View.VISIBLE);
        // enroll.setVisibility(View.GONE);
        //  rescan.setVisibility(View.GONE);
        // help.setVisibility(View.GONE);
        //image1.setVisibility((View.VISIBLE));
        Log.d(TAG,"MainActivity oncreate before Capture And Match Task");

        //inCapture=true;
        Log.d(TAG,"In Capture Status is "+inCapture+"  prev "+inCaptureTest);
        //Chand commented - this not needed

        if ((GlobalVariables.vcomdatabaseSupport == 1) && (supervisionEnrolmentStatus || supervisorUser)) {
            if (!GlobalVariables.MobileDevice)
                new DatabaseCaptureAndMatchTask().execute(0);
        } else
        if (supervisionEnrolmentStatus || supervisorUser) {
            if (!GlobalVariables.MobileDevice)
                new CaptureAndMatchTask().execute(0);
        } else
            Log.d(TAG,"Non SUpervision Capture and Match Task ");

       /* textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CaptureAndMatchTask().execute(0);
            }
        });*/
        // new CaptureAndMatchTask().execute(0);
        //  prefs = getBaseContext().getSharedPreferences(
        //        "FaceDetection", MODE_PRIVATE);

   /*     RemoveRemoteDeviceStatus();
        if (GlobalVariables.WebServer) {
            startAndroidWebServer();
        }
        if (GlobalVariables.AppSupport == 1) {
            if (isConnectedInWifi()) {
                Log.d(TAG, "It is Conected Wifi and starting broadcast receiver task");
                Intent it = new Intent(getApplicationContext(), DiscoverWebServer.class);

                getApplicationContext().startService(it);
            } else {
                Log.d(TAG, "It is for Ethernet");
                Intent it = new Intent(getApplicationContext(), WebserverBroadcastRxService.class);
                getApplicationContext().startService(it);
            }
        } else if (GlobalVariables.AppSupport ==2) { //For USB Support
            if (isEthernetConnected()) {
                StartUSBService();
            }
        }
//        sendDeviceStatusInfo();
 //       sendDeviceStatusTimerTask();*/
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        check = true;
        comeOutofApp=false;

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        /*if ((!GlobalVariables.MobileDevice) && (check)) {
            Log.d(TAG,"MainActivity onResume  fore CaptureAndmMatchTask");

            if (supervisionEnrolmentStatus || supervisorUser) {
                if (mVCOM == null) {
                    Log.d(TAG,"Opening VCOM Once again in Resume ");
                    mVCOM = new VCOM(this);
                }
            }
            Log.d(TAG,"In Capture Status when check false is "+inCapture+"  prev "+inCaptureTest);

            try {
                if (supervisionEnrolmentStatus || supervisorUser) {
                    if (inCapture == false) {
                        Log.d(TAG, "inCaputre if false so handling request ");
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "Calling Capture and Match Task ");
                                if ((GlobalVariables.vcomdatabaseSupport == 1)) {
                                    Log.d(TAG,"Starting Database Capture and match ");
                                    new DatabaseCaptureAndMatchTask().execute(0);
                                }
                                else
                                    new CaptureAndMatchTask().execute(0);
                            }
                        }, 1000);
                    }
                } else
                    Log.d(TAG,"In Non SUpervision enrollment Mode ");
            }  catch (Exception obj) {
                Log.d(TAG,"OnResume got exception ");
            }
        }*/
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG," on Pause VCOM Close ");
        if (supervisionEnrolmentStatus || supervisorUser) {
            if (mVCOM != null) {
                mVCOM.Close();
            } else
                Log.d(TAG, "MainActivity on Pause VCOM not Close ");
        } else
            Log.d(TAG,"It is in Non SuperVisionernolemnt mode ");
        //Chand added below
        //mVCOM=null;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.enroll2:
                check = false;
                Log.d(TAG,"CHECK is false ");
                if (supervisionEnrolmentStatus || supervisorUser) {
                    //Intent i = new Intent(this, Enroll.class);
                    Intent i = new Intent(this, HomeEnrollActivity.class);

                    // Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                    // startActivity(i);
                    //TODO implement
                } else {
                    Log.d(TAG,"It is Supervisionmode Enrolment");


                    if (!GlobalVariables.MobileDevice) {

                        //  enroll.setVisibility(View.GONE);
                        //Toast.makeText(SupervisiorIdentity2Activity.this, "Please Verify your finger print", Toast.LENGTH_SHORT).show();

                        Log.d(TAG,"Starting Home Enroll ");
                        //  Intent intent = new Intent(this, Enroll.class);
                        Intent intent = new Intent(this, HomeEnrollActivity.class);

                        // Intent i = new Intent(this, MainActivity.class);

                        intent.putExtra("userid","test" );
                        intent.putExtra("fromsupervisor",true );
                        startActivity(intent);

 /*                       mVCOM = new VCOM(this);

                        Handler handler=new Handler();
                        check=true;
                        Log.d(TAG,"Starting CaptureNad match Task going for sleep");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {

                                Log.d(TAG,"SuperVision Calling Capture and Match Task ");
                                new CaptureAndMatchTask().execute(0);
                            }
                        },1000); */
                        //new CaptureAndMatchTask().execute(0);
                    }
                }
                break;
            case R.id.settings_new:
               /* check = false;
                Intent i1 = new Intent(this, UserAuthenticationActivity.class);
                startActivity(i1);*/
                startSettings();
                //TODO implement
                break;

            case R.id.welcome_text:
                Log.d(TAG,"MainActivity on WelcomeTst CaptureAndMatchTask");
                //Chand Commented here
                // new CaptureAndMatchTask().execute(0);
                break;
            //case R.id.rescann:
            //  Log.d(TAG,"MainActivity on Rescan CaptureAndMatchTask");
            //   new CaptureAndMatchTask().execute(0);
            //  check=true;
            //   imageView.setImageResource(R.drawable.finger_background);
            //      textView.setText(getResources().getString(R.string.welcometext));
            //  settings.setVisibility(View.VISIBLE);
            //  enroll.setVisibility(View.VISIBLE);

            //       rescan.setVisibility(View.GONE);
            //     help.setVisibility(View.GONE);


            //   break;
            //case  R.id.help:

            //  break;
        }
    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        MyProgressDialog.dismiss();
        check = false;
        Log.d(TAG,"CHECK is false now notifysuccess");
        Log.d(TAG, "notifySuccessdddd: "+response.toString());
        if (requestType.equals("105")) {

            try {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<AllTasks>>() {
                }.getType();

                Log.d(TAG, "Response " + response);

                if (response != null) {

                    Log.d(TAG,"Response is not null"+response.toString());
                    ArrayList<AllTasks> allTasks = gson.fromJson(response.toString(), listType);

                    Log.d(TAG,"Checking the response list typew");

                    ArrayList<AllTasks> allTasks1 = allTasksList(allTasks);
                    Log.d(TAG,"Checking the response list typew"+allTasks1.size());
                    Log.d(TAG,"User Status is "+user.getStatus());
                    if (allTasks1.size() > 0) {
                        JSONObject jsonObject2 = new JSONObject();
                        try {
                            jsonObject2.put("_id", user.get_id());
                            Date currentTime = Calendar.getInstance().getTime();
                            Log.d(TAG, "onPostExecute: " + currentTime.toString());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            String currentDateandTime = sdf.format(new Date());


                            jsonObject2.put("logged_in_time", currentDateandTime);
                            try {
                                Log.d(TAG,"Get Time of Day");
                                Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

                                //Date currentTime2 = curCalendar.getInstance().getTime();
                                String currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                                String curr1      = Long.toString(System.currentTimeMillis() / 1000L);
                                jsonObject2.put("logged_in_time_secs", currts);
                                Log.d(TAG,"Current Time stamp is GMT "+currts);
                                Log.d(TAG,"Current Time stamp is "+curr1);

                            } catch (Exception obj) {

                            }
                            Log.d(TAG, "onPostExecute: " + currentDateandTime);
                            Log.d(TAG, "onPostExecute: " + jsonObject2);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        new VolleyService(SupervisiorIdentity2Activity.this, SupervisiorIdentity2Activity.this).tokenBase(PUT, GlobalVariables.SSOIPAddress + USER_LOGIN, jsonObject2, "101");


                        Log.d(TAG,"user.getStatus is "+user.getStatus());
                   /* if (user.getStatus().equals("offline") || user.getStatus().equals("")) {

                        Log.d(TAG,"User Status is Offline ");
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("_id", user.get_id());
                            Date currentTime = Calendar.getInstance().getTime();
                            Log.d(TAG, "onPostExecute: " + currentTime.toString());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            String currentDateandTime = sdf.format(new Date());
                            jsonObject.put("logged_in_time", currentDateandTime);
                            Log.d(TAG, "onPostExecute: " + currentDateandTime);
                            Log.d(TAG, "onPostExecute: " + jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        new VolleyService(Main2Activity.this, Main2Activity.this).tokenBase(PUT, GlobalVariables.SSOIPAddress + USER_LOGIN, jsonObject, "101");
                    } else*/ if (user.getStatus().equals("online") || user.getStatus().equals("on") || user.getStatus().equals("offline")) {
                            //  ArrayList<AllTasks> pendingtasks = allPendingTasks(allTasks1);

                            Log.d(TAG,"User Status is Online ");
                            if (allTasks1.size() > 0) {
                                check = false;
                                Log.d(TAG,"CHECK false before USER AUthentication");
                                Intent i1 = new Intent(SupervisiorIdentity2Activity.this, UserAuthenticationActivity.class);
                                Bundle bn = new Bundle();
                                bn.putBoolean("login", false);

                                bn.putParcelable("User", user);
                                bn.putParcelable("bitmap", mBitmapMatch);
                                i1.putExtras(bn);

                                //Chand added
                                if (inCapture == false) {
                                    Log.d(TAG,"Calling User Authentication ");
                                    finish();
                                }
                                startActivity(i1);
                            } else if (allTasks1.size() > 0) {

                                Intent i1 = new Intent(SupervisiorIdentity2Activity.this, UserAuthenticationActivity.class);
                                Bundle bn = new Bundle();
                                //  bn.putBoolean("login", true);
                                bn.putBoolean("login", false);
                                bn.putParcelable("User", user);
                                bn.putParcelable("bitmap", mBitmapMatch);
                                i1.putExtras(bn);

                                //Chand added
                                if (inCapture == false) {
                                    Log.d(TAG,"Calling User Authentication2 ");
                                    finish();
                                }

                                startActivity(i1);


                            } else {
                                Alertwithtwobuttons alertwithtwobuttons = new Alertwithtwobuttons(this, new Alertwithtwobuttons.OnClick() {
                                    @Override
                                    public void onClickOk() {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("_id", user.get_id());
                                            Date currentTime = Calendar.getInstance().getTime();

                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                            String currentDateandTime = sdf.format(new Date());
                                            jsonObject.put("time", currentDateandTime);
                                            jsonObject.put("status", 2);
                                            jsonObject.put("userId", user.getUserId());

                                            jsonObject.put("username", user.getUsername());
                                            jsonObject.put("status_name", "pending");
                                            //jsonObject.put("uid", allTasks.get(0).getUid());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Log.d(TAG, "onClickOk: " + jsonObject.toString());
                                        MyProgressDialog.show(SupervisiorIdentity2Activity.this, R.string.wait_message);
                                        new VolleyService(SupervisiorIdentity2Activity.this, SupervisiorIdentity2Activity.this).tokenBase(PUT, LOGOUT, jsonObject, "104");

                                    }

                                    @Override
                                    public void cancelButtonClick() {

                                    }
                                });

                                alertwithtwobuttons.show();


                            }
                        }
                    } else {
                        //check = true;

                        //  NotAuthorizedActivity

                        Log.d(TAG,"Starting NotAUthorized ");
                        Intent i = new Intent(this, NotAuthorizedActivity.class);
                        finish();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("userData", user);
                        i.putExtras(bundle);

                        startActivity(i);


                    }
                }
            } catch (Exception obj) {
                Log.d(TAG,"Got Crashed and exception is"+obj.toString());
            }
        } else if (requestType.equals("104")) {
            user.setStatus("offline");
            appDatabase.userDao().update(user);
        } else {
            check = false;
            Log.d(TAG, "CHECK false now notifySuccess: " + response.toString());
            Intent i1 = new Intent(SupervisiorIdentity2Activity.this, UserAuthenticationActivity.class);

            user.setStatus("online");
            appDatabase.userDao().update(user);

            Bundle bn = new Bundle();
            //bn.putBoolean("login", true);
            bn.putBoolean("login", false);
            bn.putParcelable("User", user);
            bn.putParcelable("bitmap", mBitmapMatch);
            i1.putExtras(bn);
            //Chand added
            if (inCapture == false) {
                Log.d(TAG,"Calling User Authentication3 ");
                finish();
            }


            startActivity(i1);
        }
    }

    public ArrayList<AllTasks> allTasksList(ArrayList<AllTasks> allTasks) {
        ArrayList<AllTasks> allTasks1 = new ArrayList<>();

        for (AllTasks allTasks2 : allTasks) {
            if (allTasks2.getStatus() == 0 || allTasks2.getStatus() == 1) {
                allTasks1.add(allTasks2);
            }
        }
        return allTasks1;
    }

    public ArrayList<AllTasks> allPendingTasks(ArrayList<AllTasks> allTasks) {
        ArrayList<AllTasks> allTasks1 = new ArrayList<>();

        for (AllTasks allTasks2 : allTasks) {
            if (allTasks2.getStatus() == 0) {
                allTasks1.add(allTasks2);
            }
        }
        return allTasks1;
    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        MyProgressDialog.dismiss();
        Log.d(TAG, "notifyError: " + error);
        if (requestType.equals("105")) {
            try {
                if (error.toString().contains("not authorized")) {
                    Log.d(TAG,"Self Starting MainActivity");
                    Intent i1 = new Intent(SupervisiorIdentity2Activity.this, SupervisiorIdentity2Activity.class);
                    i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i1);
                    finish();
                    Log.d(TAG, "Got Not Authorized Play Not AUthorized Tone ");
                    //Play Tone - User Not AUthorized
                }
            } catch (Exception obj) {

            }
        }
    }

    private class CaptureAndMatchTask extends AsyncTask<Integer, Integer, User> {

        @Override
        protected User doInBackground(Integer... i) {
            try {
                inCapture = true;
                inCaptureTest = true;
                if (check) {

                    int rc=10;
                    Log.d(TAG, "in CaputreAndMatchTask");
                    if (mVCOM != null)
                        rc = mVCOM.Open();

                    if (rc == 0) {

                        rc = mVCOM.Capture();

                        if (rc == 0) {
                            int tmplSize = mVCOM.GetTemplateSize();
                            if (tmplSize > 0) {
                                byte[] tmpl = new byte[tmplSize];
                                rc = mVCOM.GetTemplate(tmpl);
                                if (rc == 0) {
                                    playBeep();
                                    Log.d(TAG, "first doInBackground: toastcheck" + toastcheck);
                                    toastcheck = true;

                                    List<User> users = appDatabase.userDao().getAll();

                                    for (User user : users) {
                                        try {
                                            Log.d(TAG, "first doInBackground: " + user.toString());
                                            String fingarprint1 = user.getTemplate1();
                                            byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                            String fingarprint2 = user.getTemplate2();
                                            byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

                                            String fingarprint3 = user.getTemplate3();
                                            byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);

                                            rc = mVCOM.Match(tmpl, mTmplCapture1);
                                            int score1 = mVCOM.GetMatchScore();
                                            rc = mVCOM.Match(tmpl, mTmplCapture2);
                                            int score2 = mVCOM.GetMatchScore();
                                            rc = mVCOM.Match(tmpl, mTmplCapture3);
                                            int score3 = mVCOM.GetMatchScore();
                                            if (((score1 + score2 + score3) / 3) > GlobalVariables.MaxThresholdVal) {
                                                //Chand Added 13Oct208 for fixing TrueID issue
                                                //	mVCOM.Close();
                                                //	mVCOM = null;
                                                //above Chand added freshly
                                                notauthorized = false;
                                                Log.d(TAG, "MainActivity return user CaptureAndMatchTask");
                                                return user;
                                            } else {
                                                notauthorized = true;
                                            }
                                        } catch (Exception obj) {
                                            Log.d(TAG,"Got Exception while reading fingerprint");
                                            if (mVCOM != null)
                                                mVCOM.Close();
                                            notauthorized = true;
                                            Log.d(TAG,"Got Exception while reading notauth0orized "+notauthorized);
                                        }
                                    }


                                } else {
                                    Log.d(TAG,"Toast Check is false here");
                                    toastcheck = false;
                                }

                            }
                            /*

                             */
                        } else {
                            toastcheck = false;

                            Log.d(TAG,"Toast Check is false here 2");
                        }
                        //	if (mVCOM !=null)
                        Log.d(TAG,"MainActivity on close VCOM CaptureAndMatchTask ");
                        mVCOM.Close();
                        //mVCOM=null;
                    } else {
                        toastcheck = false;

                        Log.d(TAG,"Toast Check is false here 3 ");
                        //comeOutofApp=true;
                    }
                }
            } catch (Exception obj) {

                try {
                    Log.d(TAG,"Got Exception while capture and match task"+obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    toastcheck = false;
                    notauthorized = true;
                    // mVCOM = null;
                } catch (Exception obj2) {

                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(User user) {
            //  new CaptureAndMatchTask().execute(0);
            inCapture = false;
            inCaptureTest = false;
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            Log.d(TAG,"Got onPostExecute ");

            //notauthorized = false;

            GlobalVariables.supervisorUserID=-1;
            if (notauthorized) {
                Log.d(TAG,"Got User Not Authorized ");
                // Toast.makeText(SupervisiorIdentity2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();

                /*Handler handler=new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {*/
                try {

                    //  if (mVCOM != null)
                    //     mVCOM.Close();
                    EnrollCount++;
                    check=false;
                    Log.d(TAG, "Starting NotAuthorized ");
                    Intent i = new Intent(SupervisiorIdentity2Activity.this, NotAuthorizedActivity.class);
                    i.putExtra("fromsupervisor",true );
                    finish();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } catch (Exception ojb) {
                    Log.d(TAG,"Got exception ");
                }
                  /*  }
                },2000);*/


            } else
            if ((supervisionEnrolmentStatus || supervisorUser) && (user != null)) {

                int RoleID = user.getRoleID();
                String RoleName = user.getRoleName();
                Log.d(TAG,"SuperVision Enrollment Status is "+supervisionEnrolmentStatus+" User RoleID "+RoleID+
                        "RoleName is "+RoleName);
                //When User is Administraro or supervisor - start
                if (RoleID == 3) {

                    //    showImage(3);
                    //   txtView.setText("Admin successfully Authenticated... ");
                    setContentView(R.layout.admin_success);

                    try {
                        GlobalVariables.supervisorUserID = Integer.valueOf(user.getUserId());
                    } catch (Exception obj) {
                        GlobalVariables.supervisorUserID=-1;
                    }
                    Log.d(TAG,"SuperVisor UserID is "+ GlobalVariables.supervisorUserID);

                    Log.d(TAG, "Successfully It is SupervisionEnrolmentStatus is true Starting Enrollment ");

                    EnrollCount = 0;

                    Log.d(TAG,"Before Play Beep");
                    //playBeep();
                    Log.d(TAG,"AFter Play Beep");

                    Handler handler = new Handler();
                    Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG,"Starting Enroll ");
                            Intent i = new Intent(getApplicationContext(), Enroll.class);
                            //Intent i1 = new Intent(getApplicationContext(), HomeEnrollActivity.class);
                            // Intent i = new Intent(this, MainActivity.class);
                            startActivity(i);
                        }
                    }, 3000);
                } else {
                    Log.d(TAG, "User authenticated is not supervisor - coming out ");
                    // startActivity(i);
                    check=false;
                    Log.d(TAG, "Starting NotAuthorized ");
                    Intent i = new Intent(SupervisiorIdentity2Activity.this, NotAuthorizedActivity.class);
                    i.putExtra("fromsupervisor",true );
                    finish();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                //TODO implement
            } else
            if (user != null) {
                EnrollCount=0;
                SupervisiorIdentity2Activity.this.user = user;
                Log.d(TAG,"Main Activity onPostExecute") ;
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                //  Log.d(TAG,"Before Please wiat"+GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id());
                image2.setVisibility((View.VISIBLE));
                enroll.setVisibility(View.VISIBLE);
                try {

                    //    MyProgressDialog.show(SupervisiorIdentity2Activity.this, R.string.wait_message);
                    //    new VolleyService(SupervisiorIdentity2Activity.this, SupervisiorIdentity2Activity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id(), null, "105");
                } catch (Exception obj) {
                    Log.d(TAG,"MyProgressDialog got exception obj - SO Restarting "+obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    finish();
                    System.exit(0);
                }


                Log.d(TAG,"Before Bitmap");
                mBitmapMatch = GetBitmap();
                Log.d(TAG,"AFter Bitmap ");

            } else {
                Log.d(TAG,"onPost Execute not valid User "+check+"  "+toastcheck);
                if (check) {

                    if (toastcheck) {
                        //           imageView.setImageResource(R.drawable.finger_notvalid);

                        // textView.setText("Your fingerprint could not be read or User does not exist");
                        //    settings.setVisibility(View.GONE);
                        //    enroll.setVisibility(View.GONE);

                        // rescan.setVisibility(View.VISIBLE);
                        // help.setVisibility(View.VISIBLE);
                        //   check=false;
                        Log.d(TAG,"CHECK flase ");
                        //Toast.makeText(SupervisiorIdentity2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();
                    }else {

                        // Toast.makeText(Main2Activity.this, "Place your finger on the sensor", Toast.LENGTH_SHORT).show();
                    }
                    if (comeOutofApp) {
                        Log.d(TAG,"Coming out of PostExecute 2");

                        return;
                    }
                    Handler handler=new Handler();
                    Log.d(TAG,"Starting CaptureNad match Task going for sleep 2");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {

                            if (comeOutofApp) {
                                Log.d(TAG,"Coming out of PostExecute 2");
                                return;
                            }
                            Log.d(TAG,"Calling Capture and Match Task 2");
                            //   new SupervisiorIdentity2Activity.CaptureAndMatchTask().execute(0);
                            new CaptureAndMatchTask().execute(0);
                        }
                    },1000);

                } else {
                    Log.d(TAG,"CHECK is false, come out restart App 2");
                  /*  Intent i = new Intent(Main2Activity.this, Main2Activity.class);
                    finishAffinity();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);*/
                }
            }
        }
    }


    private class DatabaseCaptureAndMatchTask extends AsyncTask<Integer, Integer, User> {

        @Override
        protected User doInBackground(Integer... i) {
            try {
                inCapture = true;
                inCaptureTest = true;
                if (check) {

                    int rc = 10;
                    Log.d(TAG, "in DatabaseCaputreAndMatchTask");
                    if (mVCOM != null) {
                        Log.d(TAG, "Opening VCOM in DatabaseCaptureTask");
                        rc = mVCOM.Open();

                    } else
                        Log.d(TAG, "Not opening VCOM");

                    if (rc == 0) {

                        // rc = mVCOM.Capture();

                        Log.d(TAG, "SuperVisor DatabaseCaptureAndMatchTask Databse Support is enabled - Waiting for the VerifyUserRecord");
                        long timeVal = System.currentTimeMillis();
                        int retVal = mVCOM.VerifyUserRecordDatabase(6);
                        long diff = System.currentTimeMillis() - timeVal;
                        Log.d(TAG, "Difference time is " + diff);
                        if (retVal < 0) {
                            notauthorized = false;
                            Log.d(TAG, "Timeout ");
                        } else if (retVal > 0) {
                            Log.d(TAG, "Successful User ID is " + retVal);

                            //playBeep();
                            User user1 = appDatabase.userDao().countUsersBasedonUserID(String.valueOf(retVal));

                            Log.d(TAG, "User Recvd " + user1.getUserId());
                            //  Log.d(TAG, "notifySuccess: " + user.toString());

                            //	if (mVCOM !=null)
                            Log.d(TAG, "MainActivity on close VCOM CaptureAndMatchTask ");
                            //mVCOM.Close();
                            notauthorized = false;
                            return user1;
                        } else {
                            Log.d(TAG, "Not successful ID is " + retVal);
                            //mVCOM.Close();

                            //playBeep();
                            notauthorized = true;
                        }
                        //chand closed
                        mVCOM.Close();
                        Log.d(TAG, "Closed the VCOM");
                    } else {
                        Log.d(TAG, "VCOM is not opened");
                        mVCOM.Close();
                    }
                }
            } catch (Exception obj) {

                try {
                    Log.d(TAG,"Got Exception while capture and match task"+obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    toastcheck = false;
                    notauthorized = true;
                    // mVCOM = null;
                } catch (Exception obj2) {

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            //  new CaptureAndMatchTask().execute(0);
            inCapture = false;
            inCaptureTest = false;
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            Log.d(TAG,"Got onPostExecute ");

            //notauthorized = false;

            GlobalVariables.supervisorUserID=-1;
            if (notauthorized) {
                Log.d(TAG,"Got User Not Authorized ");
                // Toast.makeText(SupervisiorIdentity2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();

                /*Handler handler=new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {*/
                try {

                    //  if (mVCOM != null)
                    //     mVCOM.Close();
                    EnrollCount++;
                    check=false;
                    Log.d(TAG, "Starting NotAuthorized ");
                    Intent i = new Intent(SupervisiorIdentity2Activity.this, NotAuthorizedActivity.class);
                    i.putExtra("fromsupervisor",true );
                    finish();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } catch (Exception ojb) {
                    Log.d(TAG,"Got exception ");
                }
                  /*  }
                },2000);*/


            } else
            if ((supervisionEnrolmentStatus || supervisorUser) && (user != null)) {

                int RoleID = user.getRoleID();
                String RoleName = user.getRoleName();
                Log.d(TAG,"SuperVision Enrollment Status is "+supervisionEnrolmentStatus+" User RoleID "+RoleID+
                        "RoleName is "+RoleName);
                //When User is Administraro or supervisor - start
                if (RoleID == 3) {

                    //    showImage(3);
                    //   txtView.setText("Admin successfully Authenticated... ");
                    setContentView(R.layout.admin_success);

                    try {
                        GlobalVariables.supervisorUserID = Integer.valueOf(user.getUserId());
                    } catch (Exception obj) {
                        GlobalVariables.supervisorUserID=-1;
                    }
                    Log.d(TAG,"SuperVisor UserID is "+ GlobalVariables.supervisorUserID);

                    Log.d(TAG, "Successfully It is SupervisionEnrolmentStatus is true Starting Enrollment ");

                    EnrollCount = 0;

                    Log.d(TAG,"Before Play Beep");
                    //playBeep();
                    Log.d(TAG,"AFter Play Beep");

                    Handler handler = new Handler();
                    Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG,"Starting Enroll ");
                            Intent i = new Intent(getApplicationContext(), Enroll.class);
                            //Intent i1 = new Intent(getApplicationContext(), HomeEnrollActivity.class);
                            // Intent i = new Intent(this, MainActivity.class);
                            startActivity(i);
                        }
                    }, 3000);
                } else {
                    Log.d(TAG, "User authenticated is not supervisor - coming out ");
                    // startActivity(i);
                    check=false;
                    Log.d(TAG, "Starting NotAuthorized ");
                    Intent i = new Intent(SupervisiorIdentity2Activity.this, NotAuthorizedActivity.class);
                    i.putExtra("fromsupervisor",true );
                    finish();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                //TODO implement
            } else
            if (user != null) {
                EnrollCount=0;
                SupervisiorIdentity2Activity.this.user = user;
                Log.d(TAG,"Main Activity onPostExecute") ;
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                //  Log.d(TAG,"Before Please wiat"+GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id());
                image2.setVisibility((View.VISIBLE));
                enroll.setVisibility(View.VISIBLE);
                try {

                    //    MyProgressDialog.show(SupervisiorIdentity2Activity.this, R.string.wait_message);
                    //    new VolleyService(SupervisiorIdentity2Activity.this, SupervisiorIdentity2Activity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id(), null, "105");
                } catch (Exception obj) {
                    Log.d(TAG,"MyProgressDialog got exception obj - SO Restarting "+obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    finish();
                    System.exit(0);
                }


                Log.d(TAG,"Before Bitmap");
                mBitmapMatch = GetBitmap();
                Log.d(TAG,"AFter Bitmap ");

            } else {
                Log.d(TAG,"onPost Execute not valid User "+check+"  "+toastcheck);
                if (check) {

                    if (toastcheck) {
                        //           imageView.setImageResource(R.drawable.finger_notvalid);

                        // textView.setText("Your fingerprint could not be read or User does not exist");
                        //    settings.setVisibility(View.GONE);
                        //    enroll.setVisibility(View.GONE);

                        // rescan.setVisibility(View.VISIBLE);
                        // help.setVisibility(View.VISIBLE);
                        //   check=false;
                        Log.d(TAG,"CHECK flase ");
                        //Toast.makeText(SupervisiorIdentity2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();
                    }else {

                        // Toast.makeText(Main2Activity.this, "Place your finger on the sensor", Toast.LENGTH_SHORT).show();
                    }
                    if (comeOutofApp) {
                        Log.d(TAG,"Coming out of PostExecute 2");

                        return;
                    }
                    Handler handler=new Handler();
                    Log.d(TAG,"Starting CaptureNad match Task going for sleep 2");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {

                            if (comeOutofApp) {
                                Log.d(TAG,"Coming out of PostExecute 2");
                                return;
                            }
                            Log.d(TAG,"Calling Capture and Match Task 2");
                            //   new SupervisiorIdentity2Activity.CaptureAndMatchTask().execute(0);
                            new DatabaseCaptureAndMatchTask().execute(0);
                        }
                    },1000);

                } else {
                    Log.d(TAG,"CHECK is false, come out restart App 2");
                  /*  Intent i = new Intent(Main2Activity.this, Main2Activity.class);
                    finishAffinity();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);*/
                }
            }
        }
    }
    private void showImage(int imgIndex) {
        Log.d(TAG,"Show Image index is "+imgIndex);

        try
        {
            Log.d(TAG,"R.drawable.enroll_complete "+R.drawable.enroll_complete);
            image2.setImageResource(R.drawable.admin_verify_finger_success);
        }catch (Exception obj) {
            Log.d(TAG,"Got Exception obj"+obj.getMessage());
        }
    }
    private Bitmap GetBitmap() {
        try {
            Log.d(TAG,"in Get BitMap");
            int sizeX = mVCOM.GetCompositeImageSizeX();
            int sizeY = mVCOM.GetCompositeImageSizeY();
            if (sizeX > 0 && sizeY > 0) {
                Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
                int[] colors = new int[sizeX * sizeY];
                if (mVCOM.GetCompositeImage(colors) == 0) {
                    bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
                } else {
                    Log.i(TAG, "Unable to get composite image.");
                }
                return bm;
            } else {
                Log.i(TAG, "Composite image is too small.");
                // ...so we create a blank one-pixel bitmap and return it
                Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
                return bm;
            }
        } catch (Exception obj) {
            //Chand aadded
            Log.d(TAG,"GetBitMap is null got exception"+obj.getMessage());

            Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            return bm;

        }
    }

    private byte[] GetTemplate() {
        try {
            //if (mVCOM != null)
            {
                int tmplSize = mVCOM.GetTemplateSize();
                if (tmplSize > 0) {
                    byte[] tmpl = new byte[tmplSize];
                    if (mVCOM.GetTemplate(tmpl) == 0) {
                        return tmpl;
                    } else {
                        Log.i(TAG, "Unable to get template.");
                    }
                } else {
                    Log.i(TAG, "Template is too small.");
                }
                return new byte[0];
            } /*else {
//Chand added
                 // Toast.makeText(ActivityMain.this, "GetTemplate Invalid mCOM return null", Toast.LENGTH_LONG).show();
		  Log.d(TAG,"Invalid mCOM  - return null");
		return null;
	    } */
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception Invalid mCOM  - return null");
            return null;
        }
    }

    private void DrawMinutiae(Bitmap bm, byte[] tmpl) {
        int tmplSize = tmpl.length;
        if (tmplSize < 1)
            return;
        if (bm.getWidth() < 2 || bm.getHeight() < 2)
            return;

        ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
        Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint();
        for (int i = 0; i < minutiaeList.length; i++) {
            if (minutiaeList[i].nType == 1)
                paint.setARGB(0xFF, 0xFF, 0x00, 0x00);    // line-ending
            else
                paint.setARGB(0xFF, 0x00, 0xFF, 0x00);    // bifurcation
            int nX = minutiaeList[i].nX;
            int nY = minutiaeList[i].nY;
            canvas.drawCircle(nX, nY, 4, paint);
            double nR = minutiaeList[i].nRotAngle;
            int nX1;
            int nY1;
            nX1 = (int) (nX + (10.0 * Math.cos(nR)));
            nY1 = (int) (nY - (10.0 * Math.sin(nR)));
            canvas.drawLine(nX, nY, nX1, nY1, paint);
            canvas.drawLine(nX + 1, nY, nX1 + 1, nY1, paint);
            canvas.drawLine(nX - 1, nY, nX1 - 1, nY1, paint);
            canvas.drawLine(nX, nY + 1, nX1, nY1 + 1, paint);
            canvas.drawLine(nX, nY - 1, nX1, nY1 - 1, paint);
        }
    }

    @Override
    public void onStop() {

        super.onStop();
        Log.d(TAG,"onStop Main2Activity Activity Stopping Timer ");

        if (sendDeviceStatusTimer != null)
            sendDeviceStatusTimer.cancel();
        sendDeviceStatusTimer=null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"MainAtivity on Destroy before VCOM CLose");
        Log.d(TAG,"onDestroy inCapture "+inCapture+"  Test"+inCaptureTest);
        if (inCapture == false) {
            if (mVCOM != null)
                mVCOM.Close();
            else
                Log.d(TAG, "MainAtivity on Destroy before already VCOM CLose");
        } else
            Log.d(TAG,"inCapture is true - so not VCOM CLose ");
        //mVCOM = null;
        //Chand added above
        comeOutofApp=true;
        if (sendDeviceStatusTimer != null)
            sendDeviceStatusTimer.cancel();
        sendDeviceStatusTimer = null;
    }
    public void playBeep() {
        MediaPlayer m = new MediaPlayer();
        try {
            if (m.isPlaying()) {
                m.stop();
                m.release();
                m = new MediaPlayer();
            }

            AssetFileDescriptor descriptor = getAssets().openFd( "shutter.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            m.prepare();
            m.setVolume(1f, 1f);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void startSettings() {

        Log.d(TAG,"In SHow Licence App");
        LayoutInflater inflater = LayoutInflater.from(SupervisiorIdentity2Activity.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Android Settings");
        builder.setMessage("Password Verification");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordText = subEditText.getText().toString().trim();
                Log.d(TAG,"Password Text is "+passwordText);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                String settingPwd = getsettingspwd();
                Log.d(TAG,"App Pwd is "+settingPwd);
                if (passwordText.equals("argus@542")) {
                    Log.d(TAG,"Going for Shutdown");
                    //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
                    Shutdown();
                } else
                if (passwordText.equals("argus@543")) {
                    Log.d(TAG,"Going for Reboot");
                    reboot();
                } else
                if (passwordText.equals(settingPwd)) {

                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    finish();

                } else //Kept for Testing by Chand - remove it
                    if (TestWithoutUSB(passwordText) == null)
                        Toast.makeText(SupervisiorIdentity2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

                    // finish();
                }
            }
        });



        builder.show();
    }
    private String getsettingspwd() {
        String verVal=null;

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "settingspwd.txt");

            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(
                    fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            verVal = aBuffer.toString().trim();

            Log.d(TAG,"Reading settings pwd from sdfile is "+verVal+" len s "+verVal.length());
            myReader.close();
            if ((verVal != null)  && (verVal != " ") && (verVal != "") && verVal.length() > 0)
                return verVal;
        } catch (Exception obj) {
            Log.d(TAG,"Got exception while reading settings pwd");

        }
        return "blynk@123";
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
    private static void Shutdown() {
        Log.d(TAG,"Shutdown the system ");

        try {
            //
            Process proc = Runtime.getRuntime().exec(
                    //		new String[] { "su", "-c", "am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task"});
                    //adb shell su -c 'am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task'
                    new String[] { "su", "-c", "reboot -p" });
            proc.waitFor();
        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), "No rooted device", 3000).show();
            ex.printStackTrace();
        }
    }
    private static void reboot() {
        Log.d(TAG,"Reboot the system ");

        try {
            //
            Process proc = Runtime.getRuntime().exec(
                    //		new String[] { "su", "-c", "am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task"});
                    //adb shell su -c 'am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task'
                    new String[] { "su", "-c", "reboot " });
            proc.waitFor();
        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), "No rooted device", 3000).show();
            ex.printStackTrace();
        }
    }
    private String TestWithoutUSB(String userID) {
        List<User> users = appDatabase.userDao().getAll();
        Log.d(TAG,"testWithoutUSB - UserID is "+userID);

        for (User user : users) {

            Log.d(TAG, "User Strings Vals: " + user.toString());
            Log.d(TAG, "User Full Name Strings Vals: " + user.getFullname());
            Log.d(TAG, "User ID Strings Vals: " + user.get_id());
            Log.d(TAG, "User User ID Strings Vals: " + user.getUserId());
            Log.d(TAG, "User UserName Strings Vals: " + user.getUsername());
            Log.d(TAG, "User UserStatus Strings Vals: " + user.getStatus());

            if (userID.equals(user.getUserId())) {
                Log.d(TAG,"Matched User Get the Assigned Tasks");
                SupervisiorIdentity2Activity.this.user = user;

                //Chand commented here
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                //      MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                //       new VolleyService(Main2Activity.this, Main2Activity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id(), null, "105");


                mBitmapMatch = GetBitmap();
                return user.get_id();
            }
/*
        String fingarprint1 = user.getTemplate1();
        byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

        String fingarprint2 = user.getTemplate2();
        byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

        String fingarprint3 = user.getTemplate3();
        byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);


        rc = mVCOM.Match(tmpl, mTmplCapture1);
        int score1 = mVCOM.GetMatchScore();
        rc = mVCOM.Match(tmpl, mTmplCapture2);
        int score2 = mVCOM.GetMatchScore();
        rc = mVCOM.Match(tmpl, mTmplCapture3);
        int score3 = mVCOM.GetMatchScore();
        if (((score1 + score2 + score3) / 3) > 50000) {
            return user;
        }
        */

        }
        return null;
    }
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public String GetDeviceipWiFiData()
    {

        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        //    @SuppressWarnings("deprecation")
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());


        Log.d(TAG,"GetDeviceWifiData"+ip);
        return ip;

    }
    private String getIpAccess() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        Log.d(TAG,"Before Reading IP Address");
        @SuppressWarnings("deprecation")
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        final String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        final String serverIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (0 & 0xff));
        //return "http://" + formatedIpAddress + ":";
        return serverIpAddress;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }

    private void StartUSBService() {

        if (!GlobalVariables.MobileDevice) {
            Log.d(TAG, "It is Start USB Service");
            stopService(new Intent(this,
                    USBService.class));

            startService(new Intent(this,
                    USBService.class));
        } else
            Log.d(TAG,"Not Staring USB Service as it is Mobile device");
    }
    private void executeADBCommands(String action, String state ) {

        Process process = null;
        DataOutputStream os = null;

        //If not USB - Come out
        if (GlobalVariables.AppSupport !=2) {
            Log.d(TAG,"It is Not USB COming out");
            return;
        }
        try {
            Log.d(TAG,"Going to Excecute ADB Commands ");
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());


            if (action == null) {
                //os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell\n");
                os.writeBytes("adb -s AINBLJFI4PEQTOI7 pull /mnt/sdcard/status.txt /mnt/mystatus.txt \n");
            } else {
                Log.d(TAG,"ADB state is "+action+"  State is "+state);
                if (state != null) {
                    os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state+"  \n");
                    Log.d(TAG, "It is adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state);
                } else {
                    os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action+"  \n");
                    //			Log.d(TAG,"adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action);
                }
            }


            os.writeBytes("exit\n");
            //os.flush();
            process.waitFor();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        } finally {
            try {
                //     if (os != null) {
                //           os.close();
                //   }
                process.destroy();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        // TODO Auto-generated method stub

    }
    private void SaveDeviceStatustoRemoteDevice(String action, String state) {

        Log.d(TAG,"In SaveDeviceStatustoRemoteDevice"+action +"  "+state);
        try {

            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "mydevicestatus.txt");

            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

            if (state != null)
                myOutWriter.append(action+";"+state);
            else
                myOutWriter.append(action);

            myOutWriter.close();
            fOut.close();
        } catch (Exception obj ) {

        }
    }
    private void RemoveRemoteDeviceStatus() {
        String verVal = null;
        String REMOTE_DEVICE_STATUS_FILE = "mydevicestatus.txt";

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + REMOTE_DEVICE_STATUS_FILE);
            //File myFile = new File("/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE); //Both are same

            Log.d(TAG, "File Checking is " + "/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE);

            if (myFile.exists()) {
                myFile.delete();
            }
        } catch (Exception obj) {

        }
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.toString());
        }
        return false;
    }
    //region Start And Stop AndroidWebServer
    private boolean startAndroidWebServer() {
        Log.d("HomeActivity","in Start Web Server");
        if (!isStarted) {
            int port = getPortFromEditText();
            try {
                if (port == 0) {
                    throw new Exception();
                }
                androidWebServer = new AndroidWebServer(port, this);
                androidWebServer.start();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                //Snackbar.make(coordinatorLayout, "The PORT " + port + " doesn't work, please change it between 1000 and 9999.", Snackbar.LENGTH_LONG).show();
            }
        }
        return false;
    }
    private boolean stopAndroidWebServer() {
        if (isStarted && androidWebServer != null) {
            androidWebServer.stop();
            return true;
        }
        return false;
    }
    private int getPortFromEditText() {
        //String valueEditText = "8080";
        //return (valueEditText.length() > 0) ? Integer.parseInt(valueEditText) : DEFAULT_PORT;
        return DEFAULT_PORT;
    }
    private void sendDeviceStatusInfo() {
        Log.d(TAG,"sendDeviceStatusInfo ");
        try {
            //   String screen_id = screen_Id;
            //  String display_name = screenNamee1;
            String display_type = "Digit9Terminal";  //DigitalSignage,Kisok,InteractiveSignage
            String category = "Finance"; //Hotel, HealthCare, Hospital
            String orientation = "horizontal"; //HoHoritonTal or Vertical
            /*try {
                orientation = getScreenOrientation(this);
            } catch (Exception obj) {
                Log.d(TAG, "Got Exception orientation ");
            }
            Log.d(TAG, "Orientation is " + orientation);
            if (GlobalVariables.orientation == 1) orientation = "Vertical"; //Portrait
            else if (GlobalVariables.orientation == 2)
                orientation = "Horizontal"; //Landscape
*/
            Log.d(TAG, "Orientation is " + orientation);
            String lattitude = "1.3222";
            String longitude = "1.52333";
/*

            if (longitude_g != 0)
                longitude = String.valueOf(longitude_g);
            if (latitude_g != 0)
                lattitude = String.valueOf(latitude_g);
*/

/*
            String area = "Marathahalli";
            String city = "Bangalore";
            String state = "Karnataka";
            String country = "India";
            String zip_code = "560076";
            String age = "5";
            String footfall = "0"; //server side
            String income_group = "high"; //server side
*/

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            String currentDateTime = sdf.format(cal.getTime());
            String currts=null;
            try {
                Log.d(TAG,"Get Time of Day");
                currts =  Long.toString(System.currentTimeMillis() / 1000L);
                try {
                    Log.d(TAG,"Get Time of Day");
                    Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

                    //Date currentTime2 = curCalendar.getInstance().getTime();
                    currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                    String curr1      = Long.toString(System.currentTimeMillis() / 1000L);
                    // jsonObject2.put("logged_in_time_secs", currts);
                    Log.d(TAG,"Current Time stamp is GMT "+currts);
                    Log.d(TAG,"Current Time stamp is "+curr1);

                } catch (Exception obj) {

                }
                Log.d(TAG,"Current Time stamp is "+currts);
            } catch (Exception obj) {

            }

            String dwell_time = "0"; //currentDateTime;  //seconds
            Log.d(TAG, "Current Dwell Time is " + dwell_time);
            String ethnicity = "India";
            String impressions = "8000"; //from reports server should calculate
            String model = Build.MODEL; //"MT955";
            String os = Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT].getName();
            //"Android";
            String processor = Build.MANUFACTURER; //"MTK500";
            String ram = "1 GB";

            Log.d(TAG, "Total Ram Size is " + totalRamMemorySize() + " Free Memory is " + freeRamMemorySize());
            ram = String.valueOf(totalRamMemorySize()) + " MB";
            String androidVersion = Build.MANUFACTURER
                    + " " + Build.MODEL + " " + Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT].getName();
            Log.d(TAG, "Android details " + androidVersion);


            String Memory = getTotalExternalMemorySize(); // + " "+getAvailableInternalMemorySize();
            String storage_available = getAvailableExternalMemorySize(); //can check external Storage

            Log.d(TAG, "Total Memory is " + Memory);
            Log.d(TAG, "Available Memory is " + storage_available);
            String storage = Memory;
            ;
            try {
                ActivityManager actManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
                actManager.getMemoryInfo(memInfo);
                long totalMemory = memInfo.totalMem;
                Log.d(TAG, "Total Memory is " + totalMemory);
            } catch (Exception obj) {

            }


            String height = "32"; //display resolution 1024x 720P
            String width = "32";
/*
            if (GlobalVariables.Width != 0)
                width = String.valueOf(GlobalVariables.Width);

            if (GlobalVariables.Height != 0)
                height = String.valueOf(GlobalVariables.Height);
*/

            Log.d(TAG, "WIndow Width is " + width + " Height is " + height);
            String rtc = "true"; //detect
            String wifi = "false";
            String ethernet = "false";
            String threeg = "false";
            String ip = null;
            String localwifiSSID = null;
            if (isWifiConnected()) {
                wifi = "true";
                ip = GetDeviceipWiFiData();
                //   localwifiSSID = wifiSSID;
                Log.d(TAG, "Wifi SSID is " + localwifiSSID);
            } else if (isEthernetConnected()) {
                ethernet = "true";
                ip = logLocalIpAddresses();
                ip = ip.substring(1);
            } else
                threeg = "true";

            Log.d(TAG, "WifiStatus is" + wifi + " Etnethernet status " + ethernet + " 3G " + threeg);
            String hdmi = "true";

            String rooted = "false";
            if (isRooted())
                rooted = "true";
            //String pkt = packet.getAddress().getHostAddress();
            Log.d(TAG, "Rooted Status is " + rooted);
            String date_installed = null; //server


            //String launcher_ver = getLauncherVersion();
            //String hdmiApp = ReadHdmiAppVersion();
            {
                Log.d(TAG,
                        "send Queured Reports");
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                // String currVersion =

                String app_ver = pInfo.versionName;
            /*try {
                if (hdmiApp != null)
                    app_ver = app_ver + "-" + hdmiApp;
            } catch (Exception obj) {

            }*/
                Log.d(TAG, "App Version is " + app_ver);
                String mac = getMacAddr();
                Log.d(TAG,"MAC Address is "+mac);
                //String  ip_addr = "192.168.1.20";
                //	String  ip = logLocalIpAddresses(); // GetDeviceipWiFiData();
                //	String ip = GetDeviceipWiFiData();
                Log.d(TAG, "IP Addr is " + ip);


                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long msTimeSys = System.currentTimeMillis() - SystemClock.elapsedRealtime();
                Date curDateTimeSys = new Date(msTimeSys);

                Log.d(TAG, "sendUpdatedData Sys UpTime: " + formatter.format(curDateTimeSys));
                String curDateSys = formatter.format(curDateTimeSys);
                Log.d(TAG, "SysUpTIme TIme is : " + curDateSys);

                date_installed = currentDateTime; //curDateSys; //- server
                String date_identified = currentDateTime; //it is mandatory for the Server to Generate Reports
                String last_boot = curDateSys;
                String uptime = String.valueOf((SystemClock.elapsedRealtime() / 1000) / 60 * 60);

                String online = "true";
                //String playlist=GetLayoutID();//"none";  //LayoutID #
                //Log.d(TAG,"PlayList is "+playlist);

                //String storage_available=getAvailableInternalMemorySize(); //can check external Storage
                String last_crash_camera = currentDateTime;  //must be not null for server
                String hotspot = "false";  //is hotspot enable
                if (GetWifiApStatus()) {
                    hotspot = "true";
                }
                Log.d(TAG, "Hotspot is " + hotspot);

                String streaming = "false"; //Check Camera Available
          /*  if (CheckCameraStatus() ) {
                streaming = "true";
            }*/
                Log.d(TAG, "Camera Status is " + streaming);
                //com.teamviewer.quicksupport.market
                String teamviewer = "false"; //Check Team Viewer Installed or not


                RequestQueue requestQueue = Volley.newRequestQueue(this);
                String URL = GlobalVariables.SSOIPAddress + SSO_SERVER_DEVICE_STATUS;
                Log.d(TAG,"Device Status URL is "+URL);
                JSONObject jsonBody = new JSONObject();
                // jsonBody.put("Title", "Android Volley Demo");
                // jsonBody.put("Author", "BNK");
                //jsonBody.put("screen_id", screen_id);
                //jsonBody.put("display_name", display_name);
                //jsonBody.put("display_type", display_type);
                //jsonBody.put("category", category);
                // jsonBody.put("orientation", orientation);
                jsonBody.put("lattitude", lattitude);
                ;
                jsonBody.put("longitude", longitude);
            /*jsonBody.put("area",area);
            jsonBody.put("city",city);
            jsonBody.put("state",state);
            jsonBody.put("country",country);
            jsonBody.put("zip_code",zip_code);
            jsonBody.put("age",age);
            report.put("footfall",footfall);
            report.put("income_group",income_group);

            report.put("dwell_time",dwell_time);
            report.put("ethnicity",ethnicity);
            report.put("impressions",impressions);
            */
                // jsonBody.put("model", model);
                jsonBody.put("model",mac);
                jsonBody.put("os", os);
                jsonBody.put("processor", processor);
                jsonBody.put("ram", ram);
                jsonBody.put("storage", storage);
/*
            jsonBody.put("height", height);
            jsonBody.put("width",width);
            report.put("rtc",rtc);*/
                jsonBody.put("wifi", wifi);
                // jsonBody.put("_3g", threeg);
                jsonBody.put("ethernet", ethernet);
                jsonBody.put("hdmi", hdmi);
                jsonBody.put("rooted", rooted);
                jsonBody.put("date_installed", date_installed);
                // jsonBody.put("date_identified", date_identified);
                jsonBody.put("date_identified", currts);
                //   jsonBody.put("launcher_ver", launcher_ver);
                jsonBody.put("app_ver", app_ver);
                jsonBody.put("mac", mac);
                jsonBody.put("ip", ip);
                jsonBody.put("last_boot", last_boot);
                jsonBody.put("uptime", uptime);
            /*jsonBody.put("online",online);
            report.put("playlist",playlist);
*/
                //Details
            /*
            IP Address, Mac Address, BioMetric, Version
            Mobile
            Ethernet or Wifi,
            Rooted
            Android Version
            ScreenID
            os
            processor
            ram
            storage
            storage_available
            date
            last_boot
            up_time
             */
                final String requestBody = jsonBody.toString();

                Log.d(TAG,"Sendign Device Status "+requestBody);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }

                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response.statusCode);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }
                };

                requestQueue.add(stringRequest);
            }
        } catch(JSONException e){
            e.printStackTrace();
        }
    }
    private long totalRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.totalMem / 1048576L;
        return availableMegs;
    }
    private long freeRamMemorySize() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem / 1048576L;

        return availableMegs;
    }
    private static boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }
    private boolean GetWifiApStatus() {
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        Method[] wmMethods = wifi.getClass().getDeclaredMethods();
        for (Method method : wmMethods) {
            if (method.getName().equals("isWifiApEnabled")) {

                try {
                    return (boolean) method.invoke(wifi);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private String logLocalIpAddresses() {
        Enumeration<NetworkInterface> nwis;
        String ipAddr = null;
        try {
            nwis = NetworkInterface.getNetworkInterfaces();
            while (nwis.hasMoreElements()) {

                NetworkInterface ni = nwis.nextElement();
                for (InterfaceAddress ia : ni.getInterfaceAddresses())  {
                    try {
                        if ((ia.getNetworkPrefixLength() != 64) && (ni.getDisplayName().contains("eth0") || ni.getDisplayName().contains("wlan"))) {

                            ipAddr = ia.getAddress().toString();
                        }
                        Log.i(TAG, String.format("%s: %s/%d",
                                ni.getDisplayName(), ia.getAddress(), ia.getNetworkPrefixLength()));
                    } catch (Exception obj) {

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ipAddr;
    }
    private static String getTotalExternalMemorySize() {
        try {
            if (externalMemoryAvailable()) {
                File path = Environment.
                        getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long BlockSize = stat.getBlockSize();
                long TotalBlocks = stat.getBlockCount();
                return formatSize(TotalBlocks * BlockSize);
            } else {
                return getAvailableInternalMemorySize();
            }
        } catch (Exception obj) {
            return null;
        }
    }
    private static String  getAvailableExternalMemorySize() {
        try {
            if (externalMemoryAvailable()) {
                File path = Environment.getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long blockSize = stat.getBlockSize();
                long availableBlocks = stat.getAvailableBlocks();
                return  formatSize(availableBlocks * blockSize);
            } else {
                return 	getTotalInternalMemorySize(); // + " "+getAvailableInternalMemorySize();

            }
        } catch (Exception obj) {
            return null;
        }
    }
    private static String getAvailableInternalMemorySize() {
        Log.d("Display Stats"," in getAvailableInternalMemorySize");
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        Log.d("DisplayStatus","getAvailableInternalMemorySize lockSize is "+blockSize+" Available Blocks"+availableBlocks);
        return formatSize(availableBlocks * blockSize);
    }

    private static String getTotalInternalMemorySize() {
        Log.d("Display Stats"," in getTotalInternalMemorySize");
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        Log.d("DisplayStatus","getTotalInternalMemorySize lockSize is "+blockSize+" Total Blocks"+totalBlocks);
        return formatSize(totalBlocks * blockSize);
    }

    private static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        Log.d("DisplayStats","Format Size is "+resultBuffer.toString());
        return resultBuffer.toString();
    }
    private static boolean isRooted() {
        //  boolean isEmulator = isEmulator(context);
        /*String buildTags = Build.TAGS;*/
    /*if(!isEmulator && buildTags != null && buildTags.contains("test-keys")) {
        return true;
    } else*/ {
            File file = new File("/system/app/Superuser.apk");
            if(file.exists()) {
                return true;
            } else {
                file = new File("/system/xbin/su");
                Log.d(TAG,"isRooted "+file.exists());
                return /*!isEmulator && */file.exists();
            }
        }
    }
    private String getMacAddr() {
        try {
            Log.d(TAG,"In getMacAddr");
            List<NetworkInterface> all =Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                Log.d(TAG,"getMacAddr interface "+nif.getName());
                if (GlobalVariables.MobileDevice)
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                    else
                    if (!nif.getName().equalsIgnoreCase("eth0")) continue;

                if (nif.getName().contains("ip6")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                Log.d(TAG,"Process mac");
                if (macBytes == null) {
                    continue;
                    //  return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG,"Return Mac is "+res1.toString());
                return res1.toString();
            }
        } catch (Exception ex) {
            Log.d("NewSplash","Not able to get mac address");
        }
        return "02:00:00:00:00:00";
    }
    private String ReadSSOServerAddressDetails() {

        String ipAddr = null;
        String filePath=Environment.getExternalStorageDirectory() +"/ssoserverip.txt";

        GlobalVariables.SSOIPAddress= Constants.SERVER_IPADDR;
        try {

            Log.d("Main2Activity","in ReadSSOServerAddressDetails "+filePath);
            InputStream is = new FileInputStream(filePath);

            int size = is.available();
            Log.d("Main2Activity","Size of the JSON is "+size);
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();

            ipAddr = new String(buffer, "UTF-8");

            Log.d(TAG,"IP Addr is "+ipAddr);
            if (ipAddr.contains(".")) {
                GlobalVariables.SSOIPAddress=ipAddr;

                Log.d(TAG,"It is Proper IP Address "+GlobalVariables.SSOIPAddress);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.d("Main2Activity","Coming out of ReadSSOServerAddressDetails "+GlobalVariables.SSOIPAddress);
        return ipAddr;
    }
    //Timer to Check Internet connectivy
    private  void  sendDeviceStatusTimerTask() {
        //Showing the Progress

        //PrefUtils.setKioskModeActive(true, getApplicationContext());

        if (sendDeviceStatusTimer == null ) {
            //    Toast.makeText(getApplicationContext(),"Timer Started - on kiosk ", Toast.LENGTH_SHORT).show();

            Log.d(TAG,"Starting the InternetCheckTimer ");

            sendDeviceStatusTimer = new Timer();
            sendDeviceStatusTimer.scheduleAtFixedRate(new TimerTask() {

                                                          @Override
                                                          public void run() {
                                                              //  if (!InternetFine)
                                                              {
                                                                  Log.d("MainActivity", "Timer expired ");
                                                                  SupervisiorIdentity2Activity.this.runOnUiThread(new Runnable() {
                                                                      @Override
                                                                      public void run() {
                                                                          // MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                                                                          Log.d("MainActivity", "Running ");
                                                                          // new VolleyService(Main2Activity.this, LaunchActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress +GET_ALL_USERS, null, "101");
                                                                          sendDeviceStatusInfo();
                                                                      }
                                                                  });
                                                              }
                                                          }
                                                      },
                    120000,
                    120000);
        } else
            Log.d(TAG,"Not Starting InternetCheck Timer ");

        // Toast.makeText(getApplication(), "Timer Once again called ", Toast.LENGTH_SHORT).show();
    }

}
