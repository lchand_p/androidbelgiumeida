package lumidigm.captureandmatch.activites;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxand.facerecognition.MainActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

import frost.EnrollTypeActivityNew;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static lumidigm.constants.Constants.CHECK_USER;
import static lumidigm.constants.Constants.SSO_CHECK_USER_ID_ACTIVE_IN_LDAP;


public class FaceEnroll extends AppCompatActivity implements View.OnClickListener, IResult {

    private static final String TAG = "FaceEnroll";
    TextView editText;
    private Toolbar toolbarTop;
    private TextView welcomeText;
    private ImageView home;
    Button cancel;

     private TextView buttonOk;


    private SharedPreferences prefs= null;

    private String userId = null;
    private String ldapuser = null;
TextView textView;
    AlertDialog alertDialog =null;
//    private Button settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_enroll);
        View overlay = findViewById(R.id.mylayout);

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
  //      settings = (Button) findViewById(R.id.settings_new);
       // prefs = getBaseContext().getSharedPreferences(
         //       "FaceDetection", MODE_PRIVATE);

        Log.d(TAG,"Enroll onCreate ");
        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

        editText = (TextView) findViewById(R.id.user_id);


        toolbarTop = (Toolbar) findViewById(R.id.toolbar_top);
        welcomeText = (TextView) findViewById(R.id.welcome_text);
        home = (ImageView) findViewById(R.id.home);
        welcomeText.setVisibility(View.GONE);
        home.setVisibility(View.GONE);

        textView=(TextView)findViewById(R.id.msg);
          cancel=(Button)findViewById(R.id.cancel);
       /* imageBack = (ImageView) findViewById(R.id.image_back);
        imageHome = (ImageView) findViewById(R.id.image_home);
        imageNext = (ImageView) findViewById(R.id.image_next);
        textBack = (TextView) findViewById(R.id.text_back);
        imageHome1 = (ImageView) findViewById(R.id.image_home1);
        textNext = (TextView) findViewById(R.id.text_next);*/

         buttonOk = (TextView) findViewById(R.id.continue2);

        buttonOk.setOnClickListener(this);


         cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }
    @Override
    public void onClick(View view) {
        String msg=editText.getText().toString();
        switch (view.getId()){

             case R.id.settings_new:
               /* check = false;
                Intent i1 = new Intent(this, UserAuthenticationActivity.class);
                startActivity(i1);*/
                startSettings();
                //TODO implement
                break;
            case R.id.continue2 :
                if(msg!=null && !msg.isEmpty()) {
                    MyProgressDialog.show(FaceEnroll.this, R.string.wait_message);

                    Log.d(TAG, "It is Continue2 entered ");
                    Log.d(TAG, " Enroll device" + GlobalVariables.SSOIPAddress + SSO_CHECK_USER_ID_ACTIVE_IN_LDAP + editText.getText().toString());
                    userId = editText.getText().toString();
                    Log.d(TAG, "Current User ID is  " + userId);
                    //CHeck the LDAP Service =
                    if (!GlobalVariables.SSOIPAddress.contains("serviceme.blynksystems.com"))
                    {
                        Log.d(TAG,"Going for SSO SSO_CHECK_USER_ID_ACTIVE_IN_LDAP ");
                       // new VolleyService(Enroll.this, Enroll.this).tokenBase(GET, GlobalVariables.SSOIPAddress + SSO_CHECK_USER_ID_ACTIVE_IN_LDAP + editText.getText().toString(), null, "105");
                    //Check is this User Available in the SecondService
                    //   new VolleyService(Enroll.this, Enroll.this).tokenBase(GET, GlobalVariables.SSOIPAddress + SSO_SERVER_CHECK_USER + editText.getText().toString(), null, "201");
                        new VolleyService(FaceEnroll.this, FaceEnroll.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + editText.getText().toString(), null, "101");

                    }// else
                 }
                break;
        }
        /*settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"Gonee On Click of Settings ");

                startSettings();
            }
        });*/
    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        Log.d(TAG, "notifySuccess: " + response.toString());
        Log.d(TAG,"Request Type is "+requestType);
        if (requestType.equals("105")) {
            //Store the LdapUsername
            try {
                JSONObject obj = new JSONObject(response.toString());

               // JSONObject jobject = new JSONObject(response.toString());
                //if (obj != null)
                {
                    ldapuser = obj.optString("uid");
                    Log.d(TAG,"user is ldap "+ldapuser);
                    String UIDNum = obj.optString("uidNumber");

                    Log.d(TAG,"LDAP User ID is "+ldapuser+"  UID is "+UIDNum);
                    if (ldapuser != null ) {
                        Log.d(TAG,"Going for CHECK_USER User "+userId+"  "+GlobalVariables.SSOIPAddress + CHECK_USER + userId);
                        new VolleyService(FaceEnroll.this, FaceEnroll.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + userId, null, "110");
                    } else
                        Log.d(TAG,"Enroll Failure as User is Not Available ");
                }
            } catch (Exception obj) {
                Log.d(TAG,"Got Exception obj"+obj.getMessage());
            }
         //   new VolleyService(Enroll.this, Enroll.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + editText.getText().toString(), null, "101");

            MyProgressDialog.dismiss();
        } else
        if (requestType.equals("201")) {
            new VolleyService(FaceEnroll.this, FaceEnroll.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + editText.getText().toString(), null, "101");
        } else {
            MyProgressDialog.dismiss();


            if (requestType.equals("110") || requestType.equals("101")) {

            Log.d(TAG,"Got the Request Type -101 "+response.toString());
                Gson gson = new Gson();
                Type listType = new TypeToken<User>() {
                }.getType();

                User user = gson.fromJson(response.toString(), listType);

                if (user.getTemplate1() != null && !user.getTemplate1().isEmpty() &&
                        user.getTemplate2() != null && !user.getTemplate2().isEmpty() &&
                        user.getTemplate3() != null && !user.getTemplate3().isEmpty()) {

/*
                    Intent intent = new Intent(this, AlreadyExituserActivity.class);

                    intent.putExtra("userid", editText.getText().toString());
                    if (ldapuser != null)
                    intent.putExtra("ldapUser", ldapuser);
                    Bundle bn = new Bundle();
                    bn.putBoolean("check", true);
                    bn.putParcelable("userData", user);
                    intent.putExtras(bn);
                    startActivity(intent);*/

                    //Chand Added
                   // Toast.makeText(this, "User Finger Prints Already Available", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"User is there, Chand Added - Finger Print Enroll Activity ");
                    //Intent intent = new Intent(this, FingerPrintEnrollActivity.class);
                    Intent intent = new Intent(this, EnrollTypeActivityNew.class);

//                    Intent intent= new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
                    Bundle mBundle = new Bundle();

                    mBundle.putBoolean("Enrolment",true);
                    intent.putExtra("userid", editText.getText().toString());
                    Bundle bn = new Bundle();
                    bn.putBoolean("check", true);
                    bn.putParcelable("userData", user);
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    finishAffinity();
                   // Log.d(TAG,"LDAP User ID is "+ldapuser);

                    //  Toast.makeText(this, "Already you have added finger prints,so you can go to login and update your profile", Toast.LENGTH_SHORT).show();

                } else {
                   // Toast.makeText(this, "New User, Finger prints not available", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"No User - so Going to AlreadyExitUserActivity");
                   /* Intent intent = new Intent(this, AlreadyExituserActivity.class);

                    intent.putExtra("userid", editText.getText().toString());
                    Bundle bn = new Bundle();
                    bn.putBoolean("check", false);

                    bn.putParcelable("userData", user);
                    if (ldapuser != null)
                        intent.putExtra("ldapUser", ldapuser);
                    intent.putExtras(bn);
                    startActivity(intent);
*/
                    Intent intent = new Intent(this, EnrollTypeActivityNew.class);

  //                  Intent intent= new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean("Enrolment",true);
                    intent.putExtra("userid", editText.getText().toString());
                    intent.putExtras(mBundle);
                    startActivity(intent);
                    finishAffinity();
                   //chand commented above
                    /*Intent intent = new Intent(this, FingerPrintEnrollActivity.class);

                    intent.putExtra("userid", editText.getText().toString());
                    // if (ldapUser != null)
                    intent.putExtra("ldapUser", ldapuser);
                    // else {
                    Bundle bn = new Bundle();
                    if (user != null)
                        bn.putParcelable("userData", user);
                    intent.putExtras(bn);
                    // }
                    startActivity(intent);
*/
                }
            }
        }
    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG,"Notify Error request Type is "+requestType);

        Log.d(TAG, "notifyError: " + error);



        MyProgressDialog.dismiss();

        String id=editText.getText().toString();

        if ((requestType.equals("110")) || (requestType.equals("101"))) {
//            Toast.makeText(this, "User Not Available", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "User Doesn't exit", Toast.LENGTH_SHORT).show();

            //Chand Added
            Log.d(TAG,"Chand Added - User Not Available ");
            Intent intent = new Intent(this, UserNotAvailable.class);

            intent.putExtra("userid", editText.getText().toString());

            //intent.putExtras(bn);
            // }
            startActivity(intent);

            /*Intent intent = new Intent(this, AlreadyExituserActivity.class);

            intent.putExtra("userid", editText.getText().toString());
            Bundle bn = new Bundle();
            bn.putBoolean("check", false);

            //bn.putParcelable("userData", user);
            if (ldapuser != null)
                intent.putExtra("ldapUser", ldapuser);

            intent.putExtras(bn);
            startActivity(intent);
*/
/*
            Log.d(TAG,"Chand Added - Finger Print Enroll Activity ");
            Intent intent = new Intent(this, FingerPrintEnrollActivity.class);

            intent.putExtra("userid", editText.getText().toString());
            // if (ldapUser != null)
            intent.putExtra("ldapUser", ldapuser);
            // else {
            Bundle bn = new Bundle();
           // if (user != null)
            //    bn.putParcelable("userData", user);
            intent.putExtras(bn);
            // }
            startActivity(intent);*/
        } else if (requestType.equals("105")) {
            Toast.makeText(this, "User Doesn't exit", Toast.LENGTH_SHORT).show();

            //Chand Added
            Log.d(TAG,"Chand Added - User Not Available ");
            Intent intent = new Intent(this, UserNotAvailable.class);

            intent.putExtra("userid", editText.getText().toString());

            //intent.putExtras(bn);
            // }
            startActivity(intent);

            ///String text = "Employee number " + "<font color=#d50c20>" + id + "</font>" + " not found Please re-enter......";
            //textView.setText(Html.fromHtml(text));
            //textView.setText("Employee number "+editText.getText().toString() +" not found Please re-enter......");
            //editText.setText("");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
       // Main2Activity.check=true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Main2Activity.check=false;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
    private boolean SendingMessageWeb(String status, String requestType) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;


        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }


        Log.d(TAG,"Getting IP Address "+status+"  ");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        //String url="http://"+remoteIP+":8080/?username="+status;
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+requestType;;
        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
                //finish();


            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                  //  finish();
                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

        return true;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.toString());
        }
        return false;
    }
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    private void startSettings() {

        Log.d(TAG,"In SHow Licence App");
        LayoutInflater inflater = LayoutInflater.from(FaceEnroll.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Android Settings");
        builder.setMessage("Password Verification");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordText = subEditText.getText().toString().trim();
                Log.d(TAG,"Password Text is "+passwordText);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                String settingPwd = getsettingspwd();
                Log.d(TAG,"App Pwd is "+settingPwd);
                if (passwordText.equals("argus@542")) {
                    Log.d(TAG,"Going for Shutdown");
                    //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
                //    Shutdown();
                } else
                if (passwordText.equals("argus@543")) {
                    Log.d(TAG,"Going for Reboot");
                  //  reboot();
                } else
                if (passwordText.equals(settingPwd)) {

                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    finish();

                } //else //Kept for Testing by Chand - remove it
                   // if (TestWithoutUSB(passwordText) == null)
                     //   Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

                    // finish();
                }
            }
        });



        builder.show();
    }
    private String getsettingspwd() {
        String verVal=null;

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "settingspwd.txt");

            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(
                    fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            verVal = aBuffer.toString().trim();

            Log.d(TAG,"Reading settings pwd from sdfile is "+verVal+" len s "+verVal.length());
            myReader.close();
            if ((verVal != null)  && (verVal != " ") && (verVal != "") && verVal.length() > 0)
                return verVal;
        } catch (Exception obj) {
            Log.d(TAG,"Got exception while reading settings pwd");

        }
        return "blynk@123";
    }

}
