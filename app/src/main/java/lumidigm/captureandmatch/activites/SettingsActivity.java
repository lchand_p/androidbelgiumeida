package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import database.AppDatabase;
import frost.UserIdActivity;
import lumidigm.captureandmatch.R;
import webserver.GlobalVariables;

public class SettingsActivity extends Activity {
    private static final String TAG = "Settings";
     AppDatabase appDatabase;

    private SharedPreferences prefs=null;

    private Button detaineeoutsideblock;
    private Button visitguardstation;
    private Button detaineecheckpoint;
    private Button visitortvsupport;
    private Button detaineeboothguard;
    private Button detaineedblockguard;
    private Button terminalstart;
    private Button  escortterminal;

    private Button  waitingareavisitor;
    private Button detaineeboothguardoutside;
    private Button detaineestartexit=null;
    private Button detaineedetection= null;
    private Button enroll= null;
    private int workFlowID = 0;

/*
            (Workflowname.equals("tv_support")) { currWorkFlow=6;
        (Workflowname.equals("terminal_startexit")) currWorkFlow=7;
        (Workflowname.equals("guardstation_waitingarea currWorkFlow=8;
        (Workflowname.equals("detainee_checkpoints"))  currWorkFlow=9;
        (Workflowname.equals("detainee_outside_block")) { currWorkFlow=10;
            (Workflowname.equals("guard_in_block")) {  currWorkFlow=11;
                (Workflowname.equals("escort_boothguard_station")) currWorkFlow=12;
                (Workflowname.equals("detainee_terminal_startexit") currWorkFlow=13;
                (Workflowname.equals("escort_task_select")) { currWorkFlow=14;
                  s entinel_userlogin_changeworkflow currWorkFlow = 15; is
*/

                    @Override
    protected void onCreate(Bundle savedInstanceState) {

                        super.onCreate(savedInstanceState);
                        setContentView(R.layout.sentinel_workflow_settings);

                        Log.d(TAG, "In Settings Page It is sentinel_workflow_settings");

                        if (GlobalVariables.ProductV4Support) {
                            Log.d(TAG,"It is Product V4");
                            enroll = findViewById(R.id.enroll);

                            visitguardstation = findViewById(R.id.visitguardstation);
                            detaineeboothguard = findViewById(R.id.detaineeboothguard);
                            detaineedblockguard = findViewById(R.id.detaineedblockguard);
/*
//                            visitguardstation
                                    detaineeblockguard
                            detaineeboothguard
                                    detaineedetection*/
                                            detaineedetection = findViewById(R.id.detaineedetection);
                        } else {

                            Log.d(TAG,"It is Product V2");
                           // waitingareavisitor
                          //          detaineeoutsideblock
                         //   detaineeboothguardoutside
                            //        terminalstart
                            detaineeoutsideblock = findViewById(R.id.detaineeoutsideblock);
                            //detaineecheckpoint = findViewById(R.id.detaineecheckpoint);
                            //visitortvsupport = findViewById(R.id.visitortvsupport);
                            terminalstart = findViewById(R.id.terminalstart);
                            //escortterminal = findViewById(R.id.escortterminal);
                            waitingareavisitor = findViewById(R.id.waitingareavisitor);
                            detaineeboothguardoutside = findViewById(R.id.detaineeboothguardoutside);
                        }



                       // detaineestartexit.setVisibility(View.VISIBLE);

                        /*
                        if (GlobalVariables.ProductV4Support == false) {
                            try {

                                detaineestartexit = findViewById(R.id.detaineestartexit);
                                detaineestartexit.setVisibility(View.VISIBLE);
                            } catch (Exception obj) {
                                Log.d(TAG, "Got the Exception of detainee start exit " + obj.getMessage());
                            }
                        }*/
                        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                                MODE_PRIVATE);


                        if (waitingareavisitor !=null) {
                            waitingareavisitor.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In waitingareavisitor  ");
                                    workFlowID = 13;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }
                        if (detaineeoutsideblock !=null) {

                            detaineeoutsideblock.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In detaineeoutsideblock  ");
                                    workFlowID = 10;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }

                        if (visitguardstation !=null) {

                            visitguardstation.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In visitguardstation  ");
                                    workFlowID = 8;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }

                        if (detaineecheckpoint != null) {
                            detaineecheckpoint.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In detaineecheckpoint  ");
                                    workFlowID = 9;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }

                        if (visitortvsupport != null) {
                            visitortvsupport.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In visitortvsupport  ");
                                    workFlowID = 6;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }

                        if (detaineeboothguard != null) {
                            detaineeboothguard.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In detaineeboothguard  ");
                                    workFlowID = 12;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }

                        if (detaineedblockguard != null) {
                            detaineedblockguard.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In detaineedblockguard  ");
                                    workFlowID = 11;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }

                        if (terminalstart != null) {
                            terminalstart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In terminalstart  ");
                                    workFlowID = 7;
                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }

                        if (escortterminal != null) {
                            escortterminal.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In Escort Terminal  ");
                                    workFlowID = 14;
                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }
                        if (detaineeboothguardoutside != null) {
                            detaineeboothguardoutside.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In detaineeboothguardoutside  ");
                                    workFlowID = 15;

                                    SaveWorkFlow(workFlowID);
                                }
                            });
                        }
                        if (detaineedetection != null) {
                            //                  if (GlobalVariables.ProductV4Support == true && detaineestartexit != null) {
                            detaineedetection.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In detaineedetection  ");
                                    workFlowID = 16;
                                    SaveWorkFlow(workFlowID);
                                }
                            });
                            //                     } */
                        }
                        if (enroll != null) {
                            enroll.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Log.d(TAG, "In Go to Enrollment  ");
                                    //    workFlowID = 16;
                                    //   SaveWorkFlow(workFlowID);
                                    Log.d(TAG, "Enrollment going ");
                                    //Intent intent = new Intent(FaceSuccessActivity.this, Enroll.class);
                                    Intent intent = new Intent(getApplicationContext(), UserIdActivity.class);

                                    intent.putExtra("adminVerification", true);
                                    intent.putExtra("userid", "test");
                                    intent.putExtra("fromsupervisor", true);

                                    //intent.putExtra("userid", personID);
                                    startActivity(intent);
                                    finishAffinity();
                                }
                            });
                        }
                    }
    private void SaveWorkFlow(int configworkFlowID) {
        Log.d(TAG,"In Save Work Flow  "+configworkFlowID);
        int currworkflowID = prefs.getInt(GlobalVariables.localworkflowStatus, -1);

        if (configworkFlowID != currworkflowID)
        {
            SharedPreferences.Editor editor1 = prefs.edit();
            editor1.putInt(GlobalVariables.localworkflowStatus, configworkFlowID);
            editor1.commit();
        }
        Intent intent = new Intent(this, LaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finishAffinity();
    }


     @Override
    protected void onDestroy() {
        super.onDestroy();
        //countDownTimer.cancel();
    }

}