package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.arch.lifecycle.GenericLifecycleObserver;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import audio.MainActivity;
//import artisecure.database.AppDataBase;
//import database.AppDatabase;
//import artisecure.entity.Fingerentity;
//import lumidigm.HomeActivity;
//import lumidigm.HomeEnrollActivity;
//import lumidigm.MyProgressDialog;
//import lumidigm.captureandmatch.R;
 //import artisecure.GlobalVariables;

import lumidigm.captureandmatch.R;
import lumidigm.network.IResult;
import lumidigm.vcomdroid.VCOM;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static java.lang.String.valueOf;
//import static database.AppDatabase.getAppDatabase;

/**
 * Created by colors on 8/10/18.
 */

public class CardReaderFingerVerify extends Activity implements IResult {

    private static final String TAG = "[VCOM]";
    private TextView msg;
    private TextView submsg;

    private ImageView imageCard, tick;
    // VCOM Java/JNI adapter.
    public VCOM mVCOM=null;
    // fingerprint images.
    private Bitmap mBitmapCapture;
    private Bitmap mBitmapMatch;
    // fingerprint ANSI 378 templates.
    private byte[] mTmplCapture;
    private byte[] mTmplMatch;
    //AppDataBase database;

    private int MaxFingerprintAttempts = 3;
    private int currFingerPrintCount = 1;
    String fingarprint1=null;
    String fingarprint2=null;
    String fingarprint3=null;
   // String chandlefthandmiddleTemplate="Rk1SACAyMAACxwA1ABcAAAEYAWAAxQDFAQAAAAAgQJoAMVUPgLUAM6kPgN4ATKcNQBsAiGUJgD0A\nh2cGgI4AiacPgDMAiw0BQGkAjK8PQN4AmaAPgI8Aqp8PQF0AtakPQIoAu5gPgPEAxZkPQHsAyZQP\nQGUA14APgFwA13APQDMA1hYOgHgA2Y0PgG8A5n8PQDkA5hQOgB8A7xoMQFgA8WwPgIkA8o4PQB4A\n+g0NQIMBCIAOQLMBBjgPQIkBEZMHQJUBKzkIQHYBLwIJgFUBNA0PQIgBPF0IQNEBPEcPAecBAQHn\nSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgLQEByRgsBQB9IQM4BgBOJP3//mcHAKAyEP9VwQgA\nmDOAQ8JzAwC5NRbCEAAyQPQ4RMH9TP9cEQAoUPA4RP5U/0rAEgAhYO0+/lT/Qi/C/hQAEnTtR8D9\nU/7/wv1KVxQAEoDrRP84PsD9XVMQACGE7cBA/sBE/kr/EABsiQP//f9T/0BS/wgAZIx9w8DCwWwQ\nAGuQCcD+PsD+QcB+AwDhmBb+BADdnB5KFgASn+JL/V5HPv/A/v//ew4AkKwcPf//Wn7BFgAXp+T/\nTP5BPT7///83DwBgswP8/v3//8H+wcHBwnEMAIW2k8DFicLDwlIEAFi3cI8HAI27IP82/wcAib4p\nwMBDDAB+yif+U3XBcAMAec0t/wUAZdCTycT/GQAUydYrNfz9aDX//zfCWwQAW9Jnwf8SADfY4MD8\n+vz6/v/+/f3///7+MQoAadg9+zD+/VUGAFnbVyL/DAB53TQs//z/wSoZABbfzP4uwP7/IvzA/ztc\nhcIKAHDrTC4uVAYAVPFtYMAJAIvzNGnBw3gHAFr0YjtZDAD59atWwcDCasIFEIILSfz8/wYQtgwr\nwsBvAxCaLh7BAxBxMXD/BRB5MgAjBBCGOYOW\n";
    String template1="158000010C4B0675";
   public String template_158000010C4B0675="Rk1SACAyMAACxwA1ABcAAAEYAWAAxQDFAQAAAAAfQKIAI1cPQMIAI6YPQCUAemsKQHIAfq8PQJQA\nfqkPQOkAiZ8PgJkAnKAPQGYAp6oPQJQArpkPgP0AtpcOQIUAu5YPgIMAyo0OgDQAzBgNQH0A2oAP\nQEEA2BMPQCYA3RsKQGQA4mwPgJcA4ZAOQCkA6g0MQL8A9TkPQIwBDQ0FgJYBGV4FQKQBG0MKQIEB\nHgEIgGABJAwPQJQBKl4MQN0BLUYOQDoBMlsNQHgBMQMPQLMBNE0PQF4BNQcOAe0BAQHtSUMCAwAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAgMAEBzxiHBABuFwZlBwCoJBD/Wv8IAKAmgP/+wsHCwAQAwyca\ndxEANjXwL//AQytlwBIALkXt/0BMPkTARhIAKlPr/sD/O8L9V1RWEwAfZes9S0ZMTFcUAB1y61T+\n/8H9wUDAQ8D//xIAPnb0R/9AwDcrVRAAKnftwf3/Rzf9V8EEADd8aX4MAJd8DP9DMcH//ggAbH56\ng8GEDwB0ggk9RP5AZAwAl4MQ/0Q1SgMA7IgX/gMA6o0gwA0AnJ4cwDvB/cP/cBYAHp3gMMIoOC84\nQcAPAGmlAPspTHLCwMEFAGGpbcChCwCQqZbBxcJ1w3cHAJitIDhVBwCUsSfARMEMAIi7J0RSwn4I\nAIO/KSmQBQBuwpPJxP8EAGfEa8P9CgBzyj0h/jDABgBmzFz+/VkMAITMNzT++1cmCgB+1brDtJTA\nCQB63Un+JzMKAIDdQP78/cD+LgYAYOFpQMIKAJjlNMCDif8aABrb1sP+/f/+///+/f7B+sH/wP7/\nwP/CwML/wQYAxPwpe8AGECMG9MDBgAQQhxBpTRgQ9Qqrwf9FwsDCbHOgwWzCBRCqIBx4BRB9IXH/\nZAQQhSH9IQUQkieAwsL+BRBtPfo7BRC6QY96\n";

    public String template_15800000FFCB0675="Rk1SACAyMAAC7gA1ABcAAAEYAWAAxQDFAQAAAAAZQKAAOFQPQJgARlgPgGQATQUPQF4AY2cPQIMA\nZbEPQMUAbqANgMMAd0sNgN4AgEYOQNgAkZcPQBoAmhcOgCQAnXgOQFsAsBEPQNgAu5APQHsA0QEE\nQD8A1CIPgMQA24wJQCoA5nwPQHwA6HwCgMcA6jIJQIsA9IYGQNYA8TIMgPIBFoYPgNIBGoIPQB8B\nHSIPgPUBMIgNAjgBAQI4SUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgLgECGhvUDQBnGvov/8D9\nwv1UCwCfNYyLwYSDCgCmOw9H///+ZQwAnjyJwv/DwYF7EgA5NfDAwCv+N0b/wDYMAJdEjJtZwmIK\nAJ9IBv/9wDtdBACXSYBzBwBeYXTDccAIAGZi98H+/v8yBgCGY/0qwAcAX2dtwv/CagwAfmd0wIPA\nwsFrBwCGav0oMAkAxnIiwcH9TP4HANuRJEBPGAATk9P9VDv9w/7//8D9S/7/wEoMANeUK/9ea/51\nFQBbrd79I/zA/f7//8D9wf7/wP9GEwBesucs/v0u/0tLUgMAV7VawhcA1LWwiMHDeYx8wv/D/v+F\nCwDbvDD/TMD/PwsA1r03wT5ETQsAi88a/P79/8BA/wQAd9NinRMAPdTMNf/+/f/9KsD8wP7A/gkA\njd4n/DD/QQsAgdppw4k1WQYAPtxQwGgXAC7hzEz//v///cD9/MD//v7+////wP8GACjmV4P/IQAW\n38w+wP//NcD9//41MUP/PsDAwjoFAC7pT8FhCAB46lfCwv7/PwgAhepPwkcrCQB+61PDwP7//zUH\nAIj2Q8Aw/wcAjvY6/v81BwDY+UBYMAgAXvpMNcIxBBDyGUBDBBDRHkM3HhAUPtxkwsL9w/3C/f7/\n/v38/vz//v7A/sD////A/hkQJkbX/113wP7//Pv9/Pv//v7B/v/A/xsQHULawMFm/n/9/f/8/Pz/\n/P7/wP7A///A\n";
  //"template_2":"Rk1SACAyMAACeQA1ABcAAAEYAWAAxQDFAQAAAAAQQLkAOVIPQK8ARVYPQH0AUAYPQHQAYmcPQJoA\nZbEPgPUAg0YPQO8Ak5YPQDEAmBgPgDoAnHkPQHAAsQ8PQO4AvZAPQFQA1CAOQD4A6H4NgOkA6jQP\ngOoBDYcPQDcBGyQPAfkBAQH5SUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgMAEB2xnMBQDPGBD/\nPgoAlxcD//7AwD7/DAC5NpDCeImEDQC3PYyAdcB+wggAvz0MODsNAK1CiY1Sfn4IALVICTg1BACt\nSYD/wxEASTfpQDU+RD4JAHhUdMDEZn4NAIBV9ztG/sD+PgcAdGF3k3gLAHxi9P80wf3BMAYAnGT9\n/jEHAHZmbXxxDQCUaHrDbm92wAcAnGkGwP5AAwDylCT/BQDtlivARgMAN59TwRUAcK7gI/z/KjXB\nK8D/MwQBAJekdhMAc7TnwPss/v/+OP7APf8DAGy2XMIXAOu4rXWRhoyEaVIFAPG+LS8KAOzANMD/\nVf9SCgCn0yL9KD3ACwCP02eZwcD9wF0YEEE/18DAYnP9/f37/fz9/cD//8H9wBMAUtTPQf///P/+\n/f8q//9VBgBS3FPCwEoKAIzhXJD+Qv8IAKzwOv82/v0dAELi0FfA/iv9Lv4zODjBwTMRAFzj00H+\nJP3/Iv82BAA86FCCCQCP6VeFPMEEAEHqSVkIAJrtTMHC/MAvGAAx09BKM0YxM/7A/f/AVwgAkPRM\nV/8zBwBU9VDBL8AHAJ72Q8Ex/gcApPY6/v87BRDrEkA7BRDTE0b/RhsQOD3XwMDAwv7Bwv7/Ixv8\n/kD+wv1H\n","template_3":"Rk1SACAyMAACqwA1ABcAAAEYAWAAxQDFAQAAAAAYQFMAS2kPQL8AUKkNQM8AZaUHQEMAZwwPgK0A\nbVUKQOAAcUsLQKcAf1YOQHQAiQQPgCQAkBMPQOgAkpwEQGsAm2UPQI8An7APgOkAvkYPQOMAz5UP\nQCMAzRYOgCwA0XUOQGMA6RAPQOEA+Y8PQKwA+kkPQMgBAowPgEEBCR4PQDEBGnsPgJQBI4IFQNUB\nJTEPAfsBAQH7SUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgMQEB3RfECwCOKwn/wTXAwEIEAMk2\nF2AQAFFB8DXA/MHAwf3/RMAIAFpK8CsyBQBTT2fAcwQA314XNQUA3GEewGALAMhikHODkBAARWvw\nwDtBwP/BNsEEAD1tZ5ELAKpticCLi3MEAOZ1HDgFALV2EP/B/g0ArXeMhIvBwf+DDQCmfYmCwMDB\nb3EHAK2CCf/+wD8EAKWDg3MJAG6Md4PBwIQOAHaO9////cH9wDv/TwgAa5h3icHAwAsAc5r3Pf3/\n/sL9/w0AkZwALsDB/cD+RAcAa59wd2wNAImheoN0w2XCCACRowMzwP7ABgDnzynAQAgA4tItwMD/\nTAMAK9VgwwoAY+bg/Sz+/i4OAGfr5//9/f3+/f3+RP8IAF/uWsH/wIcWAN/zsIDBwsT+xIZ3wXBS\nDgCu+KTEwsXCw4ZbwQYA5foxPsAMAKj7l8PDxYLBwToGAOD7N8BEBhCwADE1wAYQxgU6WP0DEMwF\nN8ATEEIH2sL+O/7+/f7/Kjj/GRA1Fcz+wDxB/v3+///9/zPAwP7B+gUQgxdkkgYQixcA9P00HBAd\nEdBES/5GLkH+MGBEBBA0HVBcBBCCIl6XAxCJJknBCxBnMcz9//z7/Pn9Lw8QsD/DgMXDxKnCwjo=\n"
   //,"template_3":"ZnNkawMAAAAQBAAA/sHgQkIpor1ey928tJ2aPY13Or0M3ja8Km+LvWQRoz2rvBS9neEIvQdYvDz0\nYfg898QqOoNr4bxB8Re81X/6PePHcz22uRg9LPPPvBdzAbs/q8e9bt6qvJ/BHr0mkFQ9Ht6ivSJ+\nWr1ThsG7K+TcvNc8BT0A2my8JIX4vJqVCzxZpE69Tkq2vXc+JzwlyY080e7evPCIH72ucIa9X9Ci\nvP9khT0P/629YFQBPXqKOL3HPgQ7VMnqvTCuizxAnDK82g3MvPJ6EL0JwVo8SH+BveXJLjzbjx+9\n02OhPdThAD2QGmy8YbojvaII3DxJLMS8oiNmPbsTWb2Xb9+7aiyBPG7aSD4sVh6+5wFzPXiO3r0n\nyuA9xGKYvDa3hj1LmA69Rq16vcjXJzxV3CC6lIGNPT8GED1RkyK94O+NPL8Qqrz5QAo+c3u5vBSW\nzD19KLi8CKr6PCN0ar2CPBE9NLepvU5Z9Tx92E297NtgPQKEMb2vYa09y/oHvqOMD716kUI8Hy8R\nPVsqQjt63go9xacMvXvnazz83se9ov1MPbTPlb1AUpW9Pnu+OziFXj0DXOG9bfm/vZCLsLz3dSQ6\nzUhoPaiuhD1YoWG8hiIfPSXcLD6vQZ29Q+A1PjT3kz3U7rY9iwHPPT7irTwhJ7S9i7jkPV1NKL12\nxY+9yGiAPTRzJj2H1ke9D/Iiu6tESb2P1TG+C9hwPdUUmz1Ay1o996+cvDrjOT7e0gk8O9SavZro\nubzsVGu8HUiLPKJB0b2a+8y9zTMKPq2InL3u7kg81uTJvN/dhrwoluc6BudQPQT8DL2qZVe9iiR3\nPZlqtjxMdPi7hkPOvG75IL0Pfti9u31WPRIqMb1W/rU84hBAPbFlXj0Mv+S8xEewPQWzH7xoYMO9\nVjKEvYLO5L1L5gK8ravwPT3Txz2MP/67HstLvVlqir0f6dC8nasqPW+CgD3CuOe8aLbDPWc1jT1J\n1II8G8p3PekRrr1CcTy8kxF5PTp2d70Ufl69UldNPWUsrLopk1G9Y+z2PFEvcT1+/as9N7ZxvY8o\nnz27S2q9HR1fvEoLhz3oR3287VWzvJyQKD2/A4A9PhjiO1WKMb3uSVm8QH/Lu+jcwj21d2m92TEr\nvN0cjL3i0028uLbrvQQMP7xwY2892CeRPW+Rkj1uWgs9woTvPTziNj2AFuy97G5aOsYnSr33rNy8\nRjJbPQmBi72R8Yq9qZKMPX0Z9Dx0nri9CGpjPezbj72P+AE95JxsvZkCDj2DXt89PFY8vOgozb0e\nNoG8yzeJPQ3H6LtV5W09O+s6PUyphbybuiA83GgJPVWYgT1ha4W9eTgLvV2OIjl6yGk9fP6evFAd\njL1k0++8Q0uavLaPkjw=\n";
       String btesv = "Rk1SACAyMAADbgA1ABcAAAEYAWAAxQDFAQAAAAApgKQAHrANgJIAIGINgEEALXAIQHQAKwgPgE0AMnEMQOYAMk8PgLMAO68CgOoAPVIPgIEAQGoPgHEAWg8PgJ4AY2UPQKIAaK4PgEYAdHsFQNEAhp0PgEUAkCQGQIEAnn4LgEkApIENQKcAoaELgKIAuZMNQFgAxYIHQM4AxZkPgLwA1ZkPQHgA5o0KQPkA8kEOQIkA+pgHQN0A+UAPgFcA/I0PgMsBCEMPQGIBDJUKgOYBC0YPgEcBEIcPQJgBET8LgHUBFpYEQJQBHEMMQK8BHEcPgPABJlEPgHMBLT4GgNoBL1APQFIBMHwKgIIBNEYHQNABNVQPAlgBAQJYSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgPAECOhx9BABeFG1tBgCnHwZUwAkAdy/0QUT/BwBvMGnBfMEDAEs1YMARAO8tlsHD/sPB/4aLwXsIADNB5FfALwYAf0FrwnQEAPxNl8P/DQBzWen/Lj7/LwQA/FeafgoApWYDKzVCDgCda3DCwonBwcL/wYUIEHMLIo79jgoApW0D/Ub+/0UEALNzgJsKAIOLXMN8wv+IBABFd1N1BgBHflBtwQUAzIOTw5gHANSFHC9bBwDSiiJEchUBAH2iwsCEwMKWwMTBwMRf/wsApJb3+vxE/20SAC6E18D/wTX//Uc1RwYAfp1PwsCDDwAzjtD/Q/47/zf9DwCfnleexP9wxP/CWwcAg59J/5LBBACsoRw4HAD+hqLC/5DAm8HBxP/Dal3DwGf/DwCIp9P//vz7/f3+wP7B/8LAFAEEmqeEwIfDwMXAmYnACwCurCA3ZcGLCQCnrTrDwV98EACJr0bCiGrBw3KZBwCrsSf/wcFVCQCCtUOIwXELAKO7MXR0wn8FANHFJFgFAFrIQ3sHAM3IKcHA/5YGAD7PRnT/BQBEz0NrCAC+2CnBhXoIAFTgQ8HAwsA4BQB86TeOGwED4qRXcsLCxMfDw8LBw8HCdMLC/sX+CgD78KRYWYsGAFf/N8HAwcgKEMsInv9ANP0DEGEPNMIDEEESPf8ZADPjw/3BU1L8wMD/KivANf8EEEgUPf/9BhB2GCTDwsDBBRCbHBeFERBzHrFe/1P//zXAIQQQtSAQwP8DEPkyff8=";
       //String btesv2 = "Rk1SACAyMAADbgA1ABcAAAEYAWAAxQDFAQAAAAApgKQAHrANgJIAIGINgEEALXAIQHQAKwgPgE0AMnEMQOYAMk8PgLMAO68CgOoAPVIPgIEAQGoPgHEAWg8PgJ4AY2UPQKIAaK4PgEYAdHsFQNEAhp0PgEUAkCQGQIEAnn4LgEkApIENQKcAoaELgKIAuZMNQFgAxYIHQM4AxZkPgLwA1ZkPQHgA5o0KQPkA8kEOQIkA+pgHQN0A+UAPgFcA/I0PgMsBCEMPQGIBDJUKgOYBC0YPgEcBEIcPQJgBET8LgHUBFpYEQJQBHEMMQK8BHEcPgPABJlEPgHMBLT4GgNoBL1APQFIBMHwKgIIBNEYHQNABNVQPAlgBAQJYSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgPAECOhx9BABeFG1tBgCnHwZUwAkAdy/0QUT/BwBvMGnBfMEDAEs1YMARAO8tlsHD/sPB/4aLwXsIADNB5FfALwYAf0FrwnQEAPxNl8P/DQBzWen/Lj7/LwQA/FeafgoApWYDKzVCDgCda3DCwonBwcL/wYUIEHMLIo79jgoApW0D/Ub+/0UEALNzgJsKAIOLXMN8wv+IBABFd1N1BgBHflBtwQUAzIOTw5gHANSFHC9bBwDSiiJEchUBAH2iwsCEwMKWwMTBwMRf/wsApJb3+vxE/20SAC6E18D/wTX//Uc1RwYAfp1PwsCDDwAzjtD/Q/47/zf9DwCfnleexP9wxP/CWwcAg59J/5LBBACsoRw4HAD+hqLC/5DAm8HBxP/Dal3DwGf/DwCIp9P//vz7/f3+wP7B/8LAFAEEmqeEwIfDwMXAmYnACwCurCA3ZcGLCQCnrTrDwV98EACJr0bCiGrBw3KZBwCrsSf/wcFVCQCCtUOIwXELAKO7MXR0wn8FANHFJFgFAFrIQ3sHAM3IKcHA/5YGAD7PRnT/BQBEz0NrCAC+2CnBhXoIAFTgQ8HAwsA4BQB86TeOGwED4qRXcsLCxMfDw8LBw8HCdMLC/sX+CgD78KRYWYsGAFf/N8HAwcgKEMsInv9ANP0DEGEPNMIDEEESPf8ZADPjw/3BU1L8wMD/KivANf8EEEgUPf/9BhB2GCTDwsDBBRCbHBeFERBzHrFe/1P//zXAIQQQtSAQwP8DEPkyff8=";
       String btesv3 = "Rk1SACAyMAADZAA1ABcAAAEYAWAAxQDFAQAAAAAtQKcAG6APQMoAHKAPQD0ALq0LQOoAMq4LgI8AMk8PgE8AR64GQGwASVUOQPQATDABQJEAU5kPQPUAVyQBQOYAYzMNQIUAZksNgDQAazMOQGUAcqEFgMgAdTEPQI0AdkMJgLQAejcPgGQAgZALQOsAfiAPgHAAhJkHQEcAjDMPgKMAiy4HQHcAk5sHQK8Alm0HQF0AlDEPQOwAlR0PQJIAnE8BgLMAniAJgJMApQ8BQDkAqDkPQLYArhMHQNYAsxYPgKMAtg4BQLcAul8HQIgAu68PQOUAwBEPQJEAzLMIQKsA2gcLgE0A6Z4PQLIA+QEPQCQBCJsOQJkBHlIPgGQBIKYPQGABKUYPQJoBN64PAjYBAQI2SUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgOwECGBlKCQCqEhbA/v9P/BAAgBIM/kDA//7+/v3//v/9BQDMEhZnDQBeEwP+QUwu/xEAShgGwD3+/sH//Tj9/P8IAKsaGsA+JgcAPh0MwkbBDgCoHxzA/zv+/D0kBQDLICDAwMAPADwkDHv+wP/+MP79/gwAPy0Jwf/AO8AqBwA+MhPDwP83BgAvNBbBw8H+GQA0SDT//cOFhMF4kMGJZwYAQ0gTwcMrBwBzSxP+MfwZACZVNFxVw8B8ccHDdf94BAEDWVdoBAD/cVNOBwCVUx7+/v4YBQCQViT/JgQAQVg9cwQAXVoJMwMA9F5QwAYAH18wUsEEADRpt8DDBABXajAzBwDmak/C/i4EACB+MHgFAGhwF0cEAIl4l2gMAE56PcCdwmeWBQEAkmDASgUAZYM0wJUGAOmEUP/+MBYAHY8twcH/wMJq/3+IwMJmBgB8lS3GwsjDCgECnWL/wf7APv4GAHeXNLDFFQAYmylm/4P/csHBwsHBagYAX5srwsDCwAMBAqhnwQ4AGq8xwnb/aozBEgA8rinBeGd5/Zl6BADRt2k0BAC2uHQtBwEBv2n//1MEALe+cP39FAAWwCdzYMN8wcDDwMDAlQUA4MRr/lcKABPNJP/C/8DAgQYA/udx/8E1CQAT2inAcGcHAFHtHmd8CBAoCCLB/8LDTgQQIwstiQYQ6Rh0wP1dBBBnIhPAwgQQZiwQUg=";
       String btsv4 = "Rk1SACAyMAADRgA1ABcAAAEYAWAAxQDFAQAAAAAsQNEAICsKQCsAJrINQEkAKFYPQMUALDkOgGoA\n" +
               "L6APgDMAOK0LgKoARTUPQEEASKoMQDIASpUHQM0ASCUPgHcAS0kGQJQATD8KQD0AVZwHQHQAVzIG\n" +
               "gI0AYDgJQNMAYCIPQJoAanAJQFEAb5kGQF4AbqkHgCwAcDYOQH0AdFAJQKIAciUPgEoAdzgGgIAA\n" +
               "exMJQMgAfBsPgGUAfqkIgGEAgz8IgNMAhhYPQJMAjRMPQHYAlAENgIIApQQPgJ8AqgoPQKoAywcP\n" +
               "gEQAz6cPQJkA7FYPgGQA+6wPQGEBA08PQJ0BB7MPQLYBDlkPgFMBEaoPgJABIlQOQLcBJQQNgJ4B\n" +
               "LQAKQG8BL64PAh4BAQIeSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgNQECABiHCQCZER7//Ck7\n" +
               "BQCMEhr+KQsAqRQi/f/7wv00wAkAexEWLsD+IgUA6Q9WeAcA9Q9XdGwJAGkUEP/+MSIGAPcdVnvA\n" +
               "CwAkFv1G/sAp/ggA+ypTwkBNCgAiHQBPQUIGAE8qDP3+Vw4A/T1QwSlT//7AKwkAFC4GwGT9OgkA\n" +
               "xTNDwf1G/P8YABtAPf/EfsHBwcHBwcHBw//BxP/C/sLBBgA2OBDD/v3CBAAjRECHBQBDRglDBQD/\n" +
               "V1pUBwBBTBDAwP/AywoAME0t///EwXzCCgDNTUn/wP79//4pDQApTzD/wJbBwcHBmAMAlVJD+wMA\n" +
               "PVgwwBcAFWUrwVLE/sP/wcHCwMLCwMHD/sFvBQCNXrrEiQkA/mZX/v/A/cJLCQD9dlz/Nk8EAFdg\n" +
               "DP3CBgCba1P8+igVABaTIsB3asDAd4trwgUAn3de/hsGAMSAYjj+CAD8oWL+/17ADAAkkiR+dsP+\n" +
               "iRQAGqkaYsP+csL+lsJYwhMAG7ccW8NOwsDDwMDAcMIHAPyybcH+TxMAG8UXUXxvdHSNBwAb0R5t\n" +
               "fgcASdETwMCLBwAb3hrAwf+JBwDs7WQpQhEAI/AadMNRwP/Ew//AfgQA5f5tRAQAZ/wJYRAAKP0X\n" +
               "wMH+wYnBwv7FPgcQJQEewsD//8MHEGcFDMBcxAUQVRIQaQQQcS0GWQ==";
       String btsv5="Rk1SACAyMAACcgA1ABcAAAEYAWAAxQDFAQAAAAAYQM8AOq8MgN8AO1YLQFgARBEPQJQARQQPQLsA\n" +
               "UasPQNwAXqIPgI4AbQQPgNEAfJ4PQPYAf0UPgJUAggAPQE8AkXkPQJoAkq0PQGQAlxoPQJ8AoZsO\n" +
               "gM8ArpAPgJEAs3oHQDQAuSIPgJ8AvYMKQOYA3YsPQDoA4CcNgF0A6iIPQD4A7SwNQNIBK3oOQOoB\n" +
               "NYIKAcIBAQHCSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgKQEBpBhgAwB6EHDCBgCIFHDAhA0A\n" +
               "xhmDwIBxfsELANYkicL+xGfBfAsA2yyJb8L/wsCDDwDJOYnCiP+LwnPBCACWQvrAKTwMAI9IcMJ8\n" +
               "wViEBQBWSWB+CQCXSf1L/8A2EgDmOZPDwIvBwcF4w2b+CgC0UICGw3HBBwC8UAb/MP8GALtVDDDA\n" +
               "BADfXhc1CwCSbff+/i8r/wcA0n0cQVMVAPR/nnXBep3CwMLCRcDCEwCagv0kPf9DQME/EQCbkAD8\n" +
               "/SPA//9EMMEEAEySVmoHAFGVUGWPEABildMrLDs4OwgAk5VpwoeDEQBol9r//v7+/v7+/jI+/xAA\n" +
               "m5cD/PszPsD///3D/QcAYZ1TwHD/DgCbn3GeYv9kWQ4Ao58W/P3//kIrSg4Ao6UkJ1Q2Pg0Aoa4w\n" +
               "M/84V/8MAIyyWn7/ezcNAJGzT1hiwf1UDQCXs0Y/Yj5GCwCbw0nA/2BXwAsAocNAQErAOxgA5dW3\n" +
               "woHBwqfDw3jBwWjAbggA4909VFMHAOndN0ZSHBA7Edd3/1M4/v/+/v78/UY4wD8KENYlxsFadcA=";

    // HandlerHelp mHandler;
    private String currState = null;
    private boolean fromWeb=false;
    private SharedPreferences prefs=null;
    private boolean comingfromFingerPrint = false;
    private MyCountDownTimer  myExitTimer=null;

    public boolean FirstUser=true;
    public static boolean check = true;
    public static boolean toastcheck = true;
    String userID=null;
    //database.AppDatabase appDatabase;
    private BroadcastReceiver mUpdateReceiver;
    private IntentFilter mUpdateFilter;
    private boolean getDynamicContent = true;
    private boolean ldapSupport=true;
    private String ldapuser = null;
    private boolean FaceDetectionCompleteStatus =false;
    public byte[] PrevCapture=new byte[1024*1024];
    String CardSerialNumber = null;
//    public  artisecure.entity.Userfingerprint fingerprintData=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerscann);
        // if the device is plugged in, this will attempt to obtain permission and/or open the device.
        msg = (TextView) findViewById(R.id.msg);
        submsg = (TextView) findViewById(R.id.submsg);
        tick = (ImageView) findViewById(R.id.tick);
        imageCard = (ImageView) findViewById(R.id.imageCard);
        // Log.d(TAG, "onCreate: "+getIntent().getStringExtra("finger"));

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = null; //prefs.getString(GlobalVariables.SP_ServerIP, SERVER_IPADDR);

       // appDatabase = getAppDatabase(FingerScannActivity.this);

        Log.d(TAG,"Finger Print IP is "+GlobalVariables.SSOIPAddress);
        check = true;

        try {
            Log.d(TAG, "Starting FingerScanActivity");
            if ((getIntent().getExtras()) != null) {
                CardSerialNumber = getIntent().getStringExtra("cardserialnumber");
                Log.d(TAG,"Serial Number is "+CardSerialNumber);
               // mTmplCapture = Base64.decode((getIntent().getStringExtra("finger")), Base64.DEFAULT);
               // comingfromFingerPrint = true;
            }  //Log.d(TAG, "mTmplCapture: "+ Arrays.toString(mTmplCapture));


        } catch (Exception tpexcep) {

        }
        /*try {
            fromWeb = getIntent().getBooleanExtra("FromWeb", false);
            currState = getIntent().getStringExtra("CurrState");
            userID = getIntent().getStringExtra("tokenID");
            Log.d(TAG, "Got from Web " + fromWeb+" Rxvd UserID "+userID);


        } catch (Exception obj) {
            currState = null;
            userID = null;
        }*/
        Log.d(TAG, "CurrState is " + currState);
        boolean test =true;
//        GlobalVariables.FaceDetectionCompleteStatus = false;
        //DownloadUserData();
      {
        currFingerPrintCount = 1;
        //GlobalVariables.personDescription = "not matching";
        //GlobalVariables.tokenID=0;
        //GlobalVariables.reqStatus = false;

       // StartRemoteApp();

          try {
              String filePath = Environment.getExternalStorageDirectory() + "/displayImages/insert_your_finger_to_authenticate.mp3";
              playAudioFile(2, filePath);
          } catch (Exception obj) {
              Log.d(TAG,"Got Exception while plyaing audio ");
          }
        mVCOM = new VCOM(this);
        // mHandler = new HandlerHelp();
        //new CaptureAndMatchTask().execute(0);

          Log.d(TAG,"Card Serial Number before DownloadUserData "+CardSerialNumber);
         boolean serverSupport=false;
          {
             // mVCOM = new VCOM(this);
         //    GlobalVariables.FingerType = 1; //It is for Validation
             new CaptureAndMatchTaskLocally1().execute(CardSerialNumber);
         }
          //ChandCommented forDownloading userdata from server
          //kept to test
          //  database = AppDataBase.getAppDatabase(this);
        ImageView home = (ImageView) findViewById(R.id.home);
        if (myExitTimer == null) {

            myExitTimer = new MyCountDownTimer(45000, 5000);
            myExitTimer.start();
        }


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

           // RegisterInterReceiver();
    }
    }

    public void fingerNotMatch() {
        Log.d(TAG,"It is Fingernotmatch");

     //   GlobalVariables.personDescription="not matching";
        //GlobalVariables.tokenID=0;
      //  GlobalVariables.reqStatus= true;




     //   SendingMessageWithState("finger","101","fingernotmatch");
        msg.setText("Finger not matched");
        // imageCard.setImageResource(R.drawable.finger_first_success);
        tick.setVisibility(View.GONE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //SendingMessageWithState("finger","101","fingersuccess");
                imageCard.setImageResource(R.drawable.finger_first_success);
                tick.setVisibility(View.GONE);
                String filePath = Environment.getExternalStorageDirectory() + "/displayImages/insert_your_finger_to_authenticate.mp3";
                playAudioFile(2, filePath);

                msg.setText("Insert your finger");
                submsg.setText("To authenticate with Emirates ID profile");
        //        new CaptureAndMatchTask().execute(0);

            }
        }, 4000);

    }



    public void ProcessfingerNotMatch() {
        Log.d(TAG,"It is fingerNotMatchProcess");

       // GlobalVariables.personDescription="not matching";
        //GlobalVariables.tokenID=0;

       // GlobalVariables.reqStatus= true;

        //Send event finger print not match only when Face detectionstatus if false
        //For Timebeing commentng sending Finger print not matched to remote party
        //if (GlobalVariables.FaceDetectionCompleteStatus == false)
        {
           // SendingMessageWithState("finger", "101", "fingernotmatch");
            msg.setText("Finger not matched");
            // imageCard.setImageResource(R.drawable.finger_first_success);
            tick.setVisibility(View.GONE);
        }

      //  GlobalVariables.FingerType = 1;
      //  GlobalVariables.FingerStatus="Biometric-Not-Matched";

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //SendingMessageWithState("finger","101","fingersuccess");
                //imageCard.setImageResource(R.drawable.finger_first_success);
                //tick.setVisibility(View.GONE);
                if (FaceDetectionCompleteStatus == false)
                {
                    String filePath = Environment.getExternalStorageDirectory() + "/displayImages/finger_not_matched.mp3";
                    playAudioFile(2, filePath);
                }

              //  msg.setText("Insert your finger");
             //   submsg.setText("To authenticate with Emirates ID profile");
              //  new CaptureAndMatchTask().execute(0);


              /*  Intent i=new Intent(FingerScannActivity.this, FirstActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);*/
                   // onBackPressed();
                TestBack();
            }
        }, 4000);

    }
    public void fingerNotMatchTest() {
        //SendingMessageWithState("finger","101","fingernotmatch");
        msg.setText("Finger not matched");
        // imageCard.setImageResource(R.drawable.finger_first_success);
        tick.setVisibility(View.GONE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                imageCard.setImageResource(R.drawable.finger_first_success);
                tick.setVisibility(View.GONE);
                msg.setText("Insert your finger");
                submsg.setText("To authenticate with Emirates ID profile");
          //      new CaptureAndMatchTask().execute(0);

            }
        }, 4000);

    }

    public void fingerMatched() {
        msg.setText("Finger matched");

        imageCard.setImageResource(R.drawable.card);
        tick.setVisibility(View.GONE);
        mVCOM.Close();
        String filePath = Environment.getExternalStorageDirectory() + "/displayImages/finger_matched_please_remove_your_card.mp3";
        playAudioFile(2, filePath);

        Toast.makeText(this, "Please remove your card", Toast.LENGTH_SHORT).show();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

              /*  Intent i=new Intent(FingerScannActivity.this, FirstActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);*/
                onBackPressed();
            }
        }, 4000);


    }

    public void fingerRemove(int Score) {

        final int Scoret = Score;
        //mBundle.putString("CurrState",currState);
        String filePath = Environment.getExternalStorageDirectory() + "/displayImages/remove_your_finger_and_please_wait.mp3";
        playAudioFile(2, filePath);

        Log.d(TAG,"It is FingerRemove"+Score+"  "+Scoret);
    //    SendingMessageWithState("finger","101","fingerremove");
        msg.setText("Remove your finger");
        submsg.setText("Please wait while we authenticate your biometric details.");
        imageCard.setImageResource(R.drawable.finger_remove1);
        tick.setVisibility(View.GONE);


        mVCOM.Close();
        mVCOM=null;
        //  Toast.makeText(this, "Please remove your card", Toast.LENGTH_SHORT).show();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Scoret > 50000) {

                    Log.d(TAG,"Score is success "+Scoret);
                    tick.setVisibility(View.VISIBLE);

                  /* String filePath= Environment.getExternalStorageDirectory() +"/displayImages/success.mp3";
                    playAudioFile(1,filePath);
*/
                    //GlobalVariables.personDescription="finger matching";
                    //GlobalVariables.tokenID=0;
                    //GlobalVariables.reqStatus= true;

             //       GlobalVariables.FingerType = 1;
               //     GlobalVariables.FingerStatus="Biometric-Matched";
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String filePath = Environment.getExternalStorageDirectory() + "/displayImages/congratulations_your_bio_metric_sample.mp3";
                            playAudioFile(1, filePath);
                        }
                    },1000);



                   // SendingMessageWithState("finger","101","fingersuccess");
                    msg.setText("Congratulations");
                    submsg.setText("Your biometric sample has been validated successfully..");
                    imageCard.setImageResource(R.drawable.finger_first_success);
                    tick.setVisibility(View.VISIBLE);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                //onBackPressed();
                                TestBack();
                            } catch (Exception tes) {
                                Log.d(TAG,"Got onBackPressed Exception ");
                                finishAffinity();
                            }
                        }
                    }, 6000);//4000

                } else {
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/success.mp3";
                 //   playAudioFile(2,filePath);
                    Log.d(TAG,"Current Finger Print Count "+currFingerPrintCount);
                    if (currFingerPrintCount < MaxFingerprintAttempts) {
                        currFingerPrintCount++;
                        fingerNotMatch();
                    } else  {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                            try {
                                onBackPressed();
                            } catch (Exception objd) {
                                Log.d(TAG,"Got OnBackException crash");
                                finish();
                            }
                            }
                        }, 1000);//4000
                    }

                }
            }
        }, 4000);


    }


    private Bitmap GetBitmap() {
        int sizeX = mVCOM.GetCompositeImageSizeX();
        int sizeY = mVCOM.GetCompositeImageSizeY();
        if (sizeX > 0 && sizeY > 0) {
            Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
            int[] colors = new int[sizeX * sizeY];
            if (mVCOM.GetCompositeImage(colors) == 0) {
                bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
            } else {
                Log.i(TAG, "Unable to get composite image.");
            }
            return bm;
        } else {
            Log.i(TAG, "Composite image is too small.");
            // ...so we create a blank one-pixel bitmap and return it
            Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            return bm;
        }
    }

    private byte[] GetTemplate() {
        int tmplSize = mVCOM.GetTemplateSize();
        if (tmplSize > 0) {
            byte[] tmpl = new byte[tmplSize];
            if (mVCOM.GetTemplate(tmpl) == 0) {
                return tmpl;
            } else {
                Log.i(TAG, "Unable to get template.");
            }
        } else {
            Log.i(TAG, "Template is too small.");
        }
        return new byte[0];
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myExitTimer != null) {
            myExitTimer.cancel();
            myExitTimer= null;
        }
        try {
            //GlobalVariables.SupportWebFaceDetectonInitated=false;
            if(mVCOM != null)
                mVCOM.Close();
        } catch (Exception obj) {

        }
        try {
//          UnRegisterReceiver();
        }catch (Exception obj) {

        }
    }
private void playAudioFileOld(int Request, String mediafile) {
        try {
            Log.d(TAG,"Play Audio File "+Request+" Mediafile"+mediafile);

            MediaPlayer mp = new MediaPlayer();
            switch (Request) {
                //For Success
                case 1:

                mp.setDataSource(mediafile);
                mp.prepare();
                mp.start();
                break;

                case 2:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    break;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while play Audio "+obj.toString());
        }
}
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d(TAG,"onBackPressed"+fromWeb);
        if (myExitTimer != null) {
            myExitTimer.cancel();
            myExitTimer= null;
        }


        super.onBackPressed();
    }





    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }


    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);
        }

        @Override
        public void onFinish() {

            Log.d(TAG,"Timer Got Expired ");
     //       GlobalVariables.personDescription="not matching";
            //GlobalVariables.tokenID=0;
       //     GlobalVariables.reqStatus= true;
            //finish();
            //finishAffinity();
            onBackPressed();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }

    private void playAudioFile(int Request, String mediafile) {
        try {
            Log.d(TAG, "Play Audio File " + Request + " Mediafile" + mediafile);

            final MediaPlayer mp = new MediaPlayer();
            switch (Request) {
                //For Success
                case 1:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mpl) {
                            mp.reset();
                            mp.release();
                        }
                    });
                    break;

                case 2:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mpl) {
                            mp.reset();
                            mp.release();
                        }
                    });
                    break;
            }
        } catch (Exception obj) {
            Log.d(TAG, "Got Exception while play Audio " + obj.toString());
        }
    }

    private void RegisterInterReceiver() {

        //registerReceiver(this.FingerScanActivity,
          //      this.mUpdateReceiver);

        Log.d(TAG,"In RegisterItnerReceiver");
        this.mUpdateFilter = new IntentFilter("lumidigm.captureandmatch.activites.processevent");

        try {
            this.mUpdateReceiver = new BroadcastReceiver() {
                public void onReceive(final Context context, final Intent intent) {
                    final String stringExtra = intent.getStringExtra("STATUS");

                    Log.e("Main", "com.global.aadnetwork.insideshop Received UI Event"+stringExtra);
                    //if (stringExtra.equals("Success"))
                    {
                        Log.d(TAG,"Got Success from the Webserver disconnecting ");
                        //onBackPressed();
                        TestBack();
                        //    Main.this.updateStatusMessage(stringExtra, intExtra);
                        //return;
                    }
                }
            };
        } catch (Exception obj) {
            Log.d(TAG,"Got Crashed WHile Intent FIlter "+obj.getMessage());
        }

     //   getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
    }
    private void UnRegisterReceiver() {
        Log.d(TAG,"Got UnRegisterReceiver");
        try {
            //if (this.mUpdateReceiver != null)
            unregisterReceiver(this.mUpdateReceiver);
            //this.mUpdateReceiver = null;
        } catch (Exception obj) {

        }
    }

    private void RegisterReceiver() {
        Log.d(TAG, "Got RegisterReceiver");

        try {
            //if (this.mUpdateReceiver != null)
            getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
        } catch (Exception obj) {

        }
    }

    private void sendBroadCast(final Context context) {
        Log.e("Main","SendBroadcast");
        try {
            final Intent intent = new Intent("lumidigm.captureandmatch.activites.processevent");
            intent.putExtra("Got the Message STATUS", "Close");
            context.sendBroadcast(intent);
        } catch (Exception oj) {
            Log.d(TAG,"Got Crashed while Sending Broadcast "+oj.getMessage());
        }
    }
    /*
    @Override
    public void onPause() {
        super.onPause();
        UnRegisterReceiver();
    }
    @Override
    public void onResume() {
        super.onResume();
        RegisterReceiver();
    } */
    private void TestBack() {
        Log.d(TAG,"onBackPressed TestBack"+fromWeb);
        if (myExitTimer != null) {
            myExitTimer.cancel();
            myExitTimer= null;
        }

    }

    @Override
    public void notifySuccess(String requestType, Object response) {

        Log.d(TAG, "SendGetAllUsers - notifySuccess: "+requestType+" Response is"+response);
        //GlobalVariables.FirstUser = FirstUser_Cap;
        //fingerRemove(60000);

   {
            try {
                JSONObject obj = new JSONObject(response.toString());

                // JSONObject jobject = new JSONObject(response.toString());
                //if (obj != null)
                {
                    ldapuser = obj.optString("uid");
                    Log.d(TAG, "user is ldap " + ldapuser);
                    String UIDNum = obj.optString("uidNumber");

                    Log.d(TAG, "LDAP User ID is " + ldapuser + "  UID is " + UIDNum);
                    if (ldapuser != null) {
                        Log.d(TAG, "LDAP User is matching  " + ldapuser);

                         fingerRemove(60000);
                    } else {
                        ProcessfingerNotMatch();
                        Log.d(TAG, "Enroll Failure as User is Not Available ");
                    }
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got Exception obj" + obj.getMessage());
            }
        }
  //      MyProgressDialog.dismiss();
    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getCause());
        Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getMessage());
        try {
            //String fingerprint = "testfingerprint";
            Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
            //  String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
            //           playAudioFile(2,filePath);
    //        MyProgressDialog.dismiss();
            ProcessfingerNotMatch();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void SendGetAllUsers() {

        String requestType = "202";
        String url=null;//GlobalVariables.SSOIPAddress + GET_ALL_USERS;
       // String url="http://192.168.1.10:8114/user_enroll/check_user/1002";
       // new VolleyService(getApplicationContext(), getApplicationContext()).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");

        Log.d(TAG, "send url: "+url);
       /* new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "SendGetAllUsers - notifySuccess: "+requestType+" response us"+response);
                //GlobalVariables.FirstUser = FirstUser_Cap;
                //fingerRemove(60000);

                Log.d(TAG, "Befor Gson");
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<artisecure.entity.User>>() {
                }.getType();
                Log.d(TAG, "Befor allTasks data");
                List<artisecure.entity.User> allTasks = gson.fromJson(response.toString(), listType);
                Log.d(TAG, "notifySuccess: " + allTasks.size());
               // appDatabase.userDao().deleteAll();
                Log.d(TAG, "After deleteAll");
                //appDatabase.userDao().insertAll(allTasks);
                Log.d(TAG, "Befor Intent");

            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "SendGetAllUsers - notifyError:  is "+error.getCause());
                Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                  //  String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, ur  l, null, requestType);
*/

    }


    private class CaptureAndMatchTaskLocally extends AsyncTask<Integer, Integer, Integer> {
        boolean FirstUser_Cap=true;
        @Override
        protected Integer doInBackground(Integer... i) {
            int rc;
            rc = mVCOM.Open();
            if (rc == 0) {
                Log.d(TAG,"It is VCOM Capture");
                rc = mVCOM.Capture();
                if (rc == 0) {
                    int tmplSize = mVCOM.GetTemplateSize();
                    if (tmplSize > 0) {
                        byte[] tmpl = new byte[tmplSize];
                        rc = mVCOM.GetTemplate(tmpl);
                        if (rc == 0) {
                            Log.d(TAG, "doInBackground:templ " + Base64.encodeToString(tmpl, Base64.DEFAULT));
                            //   Log.d(TAG, "doInBackground: finger" + Base64.encodeToString(mTmplCapture, Base64.DEFAULT));
                            //database.getDAO().insert(new Fingerentity(Base64.encodeToString(tmpl, Base64.DEFAULT)));

                            Log.d(TAG,"doInBackground saved Finger  is "+btsv5);
                            Log.d(TAG,"doInBackground  captured Finger"+Base64.decode(btsv5, Base64.DEFAULT));
                            try {
                                rc = mVCOM.Match(tmpl, Base64.decode(btsv5, Base64.DEFAULT));

                                int score = mVCOM.GetMatchScore();
                                Log.d(TAG, "doInBackground:score " + score);
                                if (score > 0)
                                    Log.d(TAG, "Matching Score is " + score);
                                if (score > 50000) {
                                    FirstUser_Cap = true;
                                } else {
                                    Log.d(TAG,"Checking with second user");
                                    rc = mVCOM.Match(tmpl, Base64.decode(btsv4, Base64.DEFAULT));
                                    score = mVCOM.GetMatchScore();
                                    FirstUser_Cap = false;
                                }
                            } catch (Exception obj) {
                                Log.d(TAG,"Got Exception while fingerprint Matching ");
                            }
                        }
                        Log.d(TAG, "doInBackground: " + rc);
                    }
                }
                mVCOM.Close();
            }
            return rc;
        }

        @Override
        protected void onPostExecute(Integer rc) {

            if (rc == 0) {
                int score = mVCOM.GetMatchScore();

                Log.d(TAG,"First User Status "+FirstUser_Cap);
                GlobalVariables.FirstUser = FirstUser_Cap;
                fingerRemove(score);

            } else {

                    Log.d(TAG,"Not Matching Finger Print ");
                    ProcessfingerNotMatch();
         /*       final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new CaptureAndMatchTask().execute(0);
                    }
                }, 4000);
*/


            }
           /* if (score > 50000) {

                Log.d(TAG, "onPostExecute: " + mVCOM.GetMatchScore());
                mBitmapMatch = GetBitmap();
                mTmplMatch = GetTemplate();
                DrawMinutiae(mBitmapMatch, mTmplMatch);
                //  ((ImageView) findViewById(R.id.imageViewMatch)).setImageBitmap(mBitmapMatch);
                fingerMatched();

            } else {
//mVCOM.JNI_LED();
                Log.d(TAG, "onPostExecute: score" + score);
                fingerNotMatch();

                Log.i(TAG, String.format("Match returned %d", rc));
                // SetErrorText("There was a USB communication issue.  Please try again.");
            }*/

        }
    }

    private class CaptureAndMatchTaskLocally1 extends AsyncTask<String, Integer, Boolean> {
        boolean FirstUser_Cap=true;
        @Override
        //protected artisecure.entity.User doInBackground(Integer... i) {
        protected Boolean doInBackground(String... i) {
            try {
                String serialNum=i[0];
                String templateName="template_"+serialNum;
                Log.d(TAG,"SerialNum is "+valueOf(templateName));
               // String templateVal = valueOf(templateName);
               // Log.d(TAG,"Serial Value is "+templateVal);
               // Log.d(TAG,"SerialNum is "+Base64.decode(templateVal, Base64.DEFAULT));

                //template_158000010C4B0675="test";
                if (check) {

                    int rc;
                    Log.d(TAG, "in CaputreAndMatchTask");
                    rc = mVCOM.Open();
                    if (rc == 0) {

                        rc = mVCOM.Capture();

                        if (rc == 0) {
                            int tmplSize = mVCOM.GetTemplateSize();
                            if (tmplSize > 0) {
                                byte[] tmpl = new byte[tmplSize];
                                rc = mVCOM.GetTemplate(tmpl);

                                if (rc == 0) {
                                    //Log.d(TAG, "doInBackground:templ " + Base64.encodeToString(tmpl, Base64.DEFAULT));
                                    //   Log.d(TAG, "doInBackground: finger" + Base64.encodeToString(mTmplCapture, Base64.DEFAULT));
                                    //database.getDAO().insert(new Fingerentity(Base64.encodeToString(tmpl, Base64.DEFAULT)));

                                    Log.d(TAG,"doInBackground saved Finger  is "+btsv5);
                                    Log.d(TAG,"doInBackground  captured Finger"+Base64.decode(btsv5, Base64.DEFAULT));


                                    try {
                                       // template_158000010C4B0675
                                     //   rc = mVCOM.Match(tmpl, Base64.decode(btsv5, Base64.DEFAULT));
                                       // rc = mVCOM.Match(tmpl, Base64.decode(chandlefthandmiddleTemplate, Base64.DEFAULT));
                                        try {
                                     /*       Map<String, String> headers = new HashMap<String, String>();
                                              headers.put("template_158000010C4B0675", template_15800000FFCB0675);
                                              Log.d(TAG,"Value of Template template_15800000FFCB0675 "+template_15800000FFCB0675);
                                            headers.put("  template_15800000FFCB0675", template_158000010C4B0675);
                                           Log.d(TAG,"Template Name is "+templateName);
                                            Log.d(TAG,"Received "+ headers.get(templateName));*/
                                            //return headers;

                                            if (templateName.equals("template_158000010C4B0675"))
                                                rc = mVCOM.Match(tmpl, Base64.decode(template_158000010C4B0675, Base64.DEFAULT));
                                            else  if (templateName.equals("template_15800000FFCB0675"))
                                            rc = mVCOM.Match(tmpl, Base64.decode(template_15800000FFCB0675, Base64.DEFAULT));
                                           // else
                                             //   rc = mVCOM.Match(tmpl, Base64.decode(null, Base64.DEFAULT));
                                                //  rc = mVCOM.Match(tmpl, Base64.decode(templateVal, Base64.DEFAULT));
                                        int score = mVCOM.GetMatchScore();
                                        Log.d(TAG, "doInBackground:score " + score);
                                        if (score > 0)
                                            Log.d(TAG, "Matching Score is " + score);
                                        if (score > 50000) {
                                            FirstUser_Cap = true;
                                            return true;
                                        } else {
                                            Log.d(TAG,"Checking with second user");
                                            rc = mVCOM.Match(tmpl, Base64.decode(btsv5, Base64.DEFAULT));

                                            // rc = mVCOM.Match(tmpl, Base64.decode(btsv4, Base64.DEFAULT));
                                            score = mVCOM.GetMatchScore();
                                            Log.d(TAG,"Got the SCore "+score);
                                            if(score > 50000)
                                                return true;
                                            FirstUser_Cap = false;
                                        }
                                        } catch (Exception obj) {

                                        }
                                    } catch (Exception obj) {
                                        Log.d(TAG,"Got Exception while fingerprint Matching ");
                                    }
                                } else
                                if (rc == 1) {
                                    //  playBeep();
                                    Log.d(TAG, "doInBackground: toastcheck" + toastcheck);
                                    toastcheck = true;

                                    //List<artisecure.entity.User> users = appDatabase.userDao().getAll();

                                    //Log.d(TAG,"Number of Users is "+users.size());

                                    int userCount=0;
/*
                                    if (fingerprintData != null )
                                    //for (artisecure.entity.User user : users)
                                    {
                                        userCount++;
                                        Log.d(TAG,"For User "+userCount);
                                        //  Log.d(TAG, "doInBackground: " + user.toString());
                                        fingarprint1 = fingerprintData.getTemplate1();
                                        byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                        fingarprint2 = fingerprintData.getTemplate2();
                                        byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

                                        fingarprint3 = fingerprintData.getTemplate3();
                                        byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);

                                        rc = mVCOM.Match(tmpl, mTmplCapture1);
                                        int score1 = mVCOM.GetMatchScore();
                                        rc = mVCOM.Match(tmpl, mTmplCapture2);
                                        int score2 = mVCOM.GetMatchScore();
                                        rc = mVCOM.Match(tmpl, mTmplCapture3);
                                        int score3 = mVCOM.GetMatchScore();
                                        fingarprint1=null;
                                        fingarprint2=null;
                                        fingarprint3=null;
                                        fingerprintData = null;
                                        Log.d(TAG,"Score Match is "+score1+"  "+score2+"  "+score3);

                                        if (((score1 + score2 + score3) / 3) > 50000) {
                                            //Chand Added 13Oct208 for fixing TrueID issue
                                            //	mVCOM.Close();
                                            //	mVCOM = null;
                                            //above Chand added freshly
                                            Log.d(TAG,"MainActivity return user CaptureAndMatchTask");
                                            //return user;

                                            return true;
                                        } //else if (PrevCapture == null) {


                                    }*/
                                    PrevCapture = tmpl;
                                    Log.d(TAG,"Prev Capture  dones "+PrevCapture.toString());
                                } else {
                                    toastcheck = false;
                                }
                            }
                            /*

                             */
                        } else {
                            toastcheck = false;
                        }
                        //	if (mVCOM !=null)
                        Log.d(TAG,"MainActivity on close VCOM CaptureAndMatchTask ");
                        mVCOM.Close();
                        mVCOM=null;
                    } else {
                        toastcheck = false;
                    }
                }
            } catch (Exception obj) {

                try {
                    Log.d(TAG,"Got Exception while capture and match task"+obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    mVCOM = null;
                } catch (Exception obj2) {
                    mVCOM = null;
                }

                //return null;
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean user) {
            //  new CaptureAndMatchTask().execute(0);
           /* getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
            Log.d(TAG,"Return Status is "+user);
            if (user) {

                //Main2Activity.this.user = user;
                //Log.d(TAG,"Main Activity onPostExecute"+user.getUserId()+"  Status is "+user.getStatus()+"test "+user.getUsername()) ;
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                // MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                //new VolleyService(Main2Activity.this, Main2Activity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id(), null, "105");
                //SendtoSSOServer(SSO_SERVER_CHECK_USER , user.getUserId(),"102");
                //IResult mResultCallback = null;
                //new VolleyService(mResultCallback, FingerScannActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + SSO_SERVER_CHECK_USER + user.getUserId(), null, "201");
                //mBitmapMatch = GetBitmap();

                //For LDAP Check whether this User Exists in LDAP, if not exists - it is failure case
                //if (!ldapSupport) {

                Log.d(TAG,"Matched Finger remove finger");
                //  Log.d(TAG, "First User Status " + FirstUser_Cap);
                fingerRemove(60000);
                // } else {
                //WHen the User is available in LDAP, Accept the FingerPrint
                //If not available, even it is success, discard and say reject

                // CheckWetherLDAPUser(user.getUserId());
                //}
            } else {
                Log.d(TAG,"Not Matching Finger Print ");
                ProcessfingerNotMatch();
                //finish();
                if (check) {

                    if (toastcheck) {


                        //textView.setText("Your fingerprint could not be read or User does not exist");

                        //    check=false;
                        check =true;

                        // Toast.makeText(Main2Activity.this, "Finger not matched", Toast.LENGTH_SHORT).show();
                    }else {



                        // Toast.makeText(Main2Activity.this, "Place your finger on the sensor", Toast.LENGTH_SHORT).show();
                    }

                    /*
                    When Finger Print is matching, there is a case that User might have enrolled from remote device.
                    Now download the content and check the whether it is matching
                     */
                   /* if (getDynamicContent) {
                        //Save the Current Finger Print & Download the Content Once again
                        Log.d(TAG,"Dynamic Content is true - so downloading the content");
                        MyProgressDialog.show(FingerScannActivity.this, R.string.wait_message);
                    //   new VolleyService(this, getBaseContext()).tokenBase(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS , null, "101");
                    //    new VolleyService(FingerScannActivity.this, FingerScannActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS , null, "501");
                  //      DownloadUserData();
//                        new VolleyService(getApplicationContext(), FingerScannActivity.class).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
                       // SendGetAllUsers();
                    } else
                    if (mVCOM != null) {
                        Handler handler = new Handler();
                        Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                new CaptureAndMatchTask().execute(0);
                            }
                        }, 1000);
                    } else {
                        ProcessfingerNotMatch();
                        //Play Voice Promt of fAILURE AND COMEE OUT

                    }*/

                }
            }
        }
    }

}

