package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import webserver.GlobalVariables;
import webserver.WebUtils;

public class FingerPrintEnrollSuccessActivityNew extends Activity implements View.OnClickListener {

    private static final String TAG = "VerifyEnrollSuccessNew";
    private TextView msg;
    private LinearLayout centerImage;
    private ImageView imageview;
    private TextView hint;
    String userid;

  //  private VCOM mVCOM;
    public static boolean check=true;
    public static boolean toastcheck=true;
    Button Continue1;
    User user;
    String ldapUser = null;
    SharedPreferences prefs=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fingerprintverifysuccessadded);

        try {
            userid = getIntent().getExtras().getString("userid");
            user = getIntent().getExtras().getParcelable("userData");
            //ldapUser = getIntent().getExtras().getString("ldapUser");

        } catch (Exception obj) {

        }

        try {
         //   userid = getIntent().getExtras().getString("userid");
           // user = getIntent().getExtras().getParcelable("userData");
            ldapUser = getIntent().getExtras().getString("ldapUser");

        } catch (Exception obj) {

        }
        Log.d(TAG,"User ID is "+userid+" ldapUser "+ldapUser);
        View overlay = findViewById(R.id.mylayout);


        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
       // appDatabase = AppDatabase.getAppDatabase(this);
        //
      //  Continue1=(Button)findViewById(R.id.continue3);
        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        //msg = (TextView) findViewById(R.id.msg);
      /*  if (ldapUser != null)
            msg.setText("Employee #" + userid + " "+ldapUser);
        else
        msg.setText("Employee #" + userid + " "+user.getFullname());
*/
       // centerImage = (LinearLayout) findViewById(R.id.center_image);
   //     imageview = (ImageView) findViewById(R.id.imageview);
        //hint = (TextView) findViewById(R.id.hint);
   //     findViewById(R.id.cancel).setOnClickListener(this);
 /*
            Continue1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                 //   Continue1.setVisibility(View.GONE);
                  //  imageview.setImageResource(R.drawable.enroll_succuess_ornot);

                    Log.d(TAG,"It is on Continue ");

                    new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"FingerPrint_VerifyContinue");
      //              new CaptureAndMatchTask().execute(0);
                    Intent i = new Intent(FingerPrintEnrollSuccessActivityNew.this, VerifyEnrolSuccuessorNotActivity.class);
                    //Intent i = new Intent(FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
                    i.putExtra("userid", userid);
                    if (ldapUser != null)
                        i.putExtra("ldapUser", ldapUser);
                    if (user != null ) {
                        Bundle bn = new Bundle();
                        bn.putParcelable("userData", user);
                        i.putExtras(bn);
                    }
                    // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            });
 */       //}


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getApplicationContext(), LaunchActivity.class);
               /* Intent i = new Intent(getApplicationContext(), Main2Activity.class);

                if (GlobalVariables.FaceandFinger == true)
                    i = new Intent(getApplicationContext(), FaceTabLaunchActivity.class);*/

                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                //finish();
                finishAffinity();
            }
        }, 2000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                //TODO implement
                Intent i = new Intent(this, Main2Activity.class);
                if (GlobalVariables.FaceandFinger == true)
                    i = new Intent(this, FaceTabLaunchActivity.class);

                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        check=true;
       /* if(check)
            new CaptureAndMatchTask().execute(0);*/
    }

    @Override
    protected void onPause() {
        super.onPause();
 //       if (mVCOM != null)
   //         mVCOM.Close();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
     //   if (mVCOM != null)
       //     mVCOM.Close();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
}
