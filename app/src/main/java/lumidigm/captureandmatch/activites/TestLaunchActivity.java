package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import database.AppDatabase;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.BuildConfig;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.utils.PermissionListener;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static lumidigm.constants.Constants.GET_ALL_USERS;

public class TestLaunchActivity extends AppCompatActivity implements IResult {
    private static final String TAG = "TestLaunchActivity";
    Runnable runnable;
    SharedPreferences prefs;
    Handler handler;
    AppDatabase appDatabase;
    PermissionListener mPermissionListener;
    private Timer InternetCheckTimer = null;
    private boolean InternetFine=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch_activity);


        View overlay = findViewById(R.id.mylayout);
        ImageView      launchImage = findViewById(R.id.launch_image);
        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);

        ReadSSOServerAddressDetails();
        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        appDatabase = AppDatabase.getAppDatabase(this);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // only for gingerbread and newer versions

            requestPermissions(this, new String[]{"Manifest.permission.WRITE_EXTERNAL_STORAGE"}, 100, new PermissionListener() {
                @Override
                public void onGranted(int statuscode) {
                    createfile();
                }

                @Override
                public void onDenied(List<String> deniedPermissions) {

                }
            });
        } else {
            createfile();
        }


        MyProgressDialog.show(this, R.string.wait_message);
        String url = GlobalVariables.SSOIPAddress + GET_ALL_USERS;
      //  new VolleyService(TestLaunchActivity.this, TestLaunchActivity.this).tokenBase(GET,  GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
      //  new VolleyService(TestLaunchActivity.this, TestLaunchActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
        DownloadUserData();
/*
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LaunchActivity.this, Main2Activity.class);

                startActivity(intent);
                finish();
            }
        };*/


        StartUSBDevice();

        //Starting Internet Check Timer
//        InternetCheckTimerTask();

        launchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Log.d(TAG,"Coming Out of the Argus Launch Activity");
                    finishAffinity();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //  handler.postDelayed(runnable,3000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // handler.removeCallbacks(runnable);
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {

        super.onStop();
        Log.d(TAG,"onStop Launch Activity Stopping Timer ");

        if (InternetCheckTimer != null)
            InternetCheckTimer.cancel();
        InternetCheckTimer=null;
    }


    public void createfile() {
        String fileName = "hdmiappversion.txt";//like 2016_01_12.txt


        try {
           /*// File root = new File(Environment.getExternalStorageDirectory()+File.separator+"Music_Folder", "Report Files");
            File root = new File(Environment.getExternalStorageDirectory(), "Argus");
            if (!root.exists())
            {
                root.mkdirs();
            }
          */
            File gpxfile = new File(Environment.getExternalStorageDirectory(), fileName);


            FileWriter writer = new FileWriter(gpxfile, false);


            String versionName = BuildConfig.VERSION_NAME;

            writer.write(versionName);
            writer.flush();
            writer.close();
            // Toast.makeText(this, "Data has been written to Report File", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void requestPermissions(Activity activity, String[] permissions, int status, PermissionListener listener) {

        if (null == activity) {
            return;
        }

        mPermissionListener = listener;
        List<String> permissionList = new ArrayList<>();
        for (String permission : permissions) {
            //Permission is not authorized
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(permission);
            }
        }

        if (!permissionList.isEmpty()) {
            ActivityCompat.requestPermissions(activity, permissionList.toArray(new String[permissionList.size()]), status);
        } else {
            mPermissionListener.onGranted(status);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            List<String> deniedPermissions = new ArrayList<>();
            for (int i = 0; i < grantResults.length; i++) {
                int result = grantResults[i];
                if (result != PackageManager.PERMISSION_GRANTED) {
                    String permission = permissions[i];
                    deniedPermissions.add(permission);
                }
            }

            if (deniedPermissions.isEmpty()) {
                if (mPermissionListener != null)
                    mPermissionListener.onGranted(requestCode);
            } else {
                if (mPermissionListener != null)
                    mPermissionListener.onDenied(deniedPermissions);
            }
        }

    }

    private void StartUSBDevice() {
        Intent intent = getIntent();
        String ACTION_USB_DEVICE_ATTACHED = "com.example.ACTION_USB_DEVICE_ATTACHED";

        try {

            Log.d(TAG, "in StartUSBDevice ");
            if (intent != null) {
                Log.d(TAG, "in StartUSBDevice intent is not null ");

                if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                    Parcelable usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    Log.d(TAG, "in StartUSBDevice USB Device Attached ");
                    // Create a new intent and put the usb device in as an extra
                    Intent broadcastIntent = new Intent(ACTION_USB_DEVICE_ATTACHED);
                    broadcastIntent.putExtra(UsbManager.EXTRA_DEVICE, usbDevice);

                    // Broadcast this event so we can receive it
                    sendBroadcast(broadcastIntent);
                }
            }
        } catch (Exception obj) {
            Log.d(TAG, "Got USB Device exception " + obj.getMessage());
        }
    }
    //Timer to Check Internet connectivy
    private  void  InternetCheckTimerTask() {
        //Showing the Progress

        //PrefUtils.setKioskModeActive(true, getApplicationContext());

        if (InternetCheckTimer == null ) {
            //    Toast.makeText(getApplicationContext(),"Timer Started - on kiosk ", Toast.LENGTH_SHORT).show();

            Log.d(TAG,"Starting the InternetCheckTimer ");

            InternetCheckTimer = new Timer();
            InternetCheckTimer.scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    if (!InternetFine)
                    {
                         Log.d("MainActivity", "Timer expired ");
                         TestLaunchActivity.this.runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                                MyProgressDialog.show(TestLaunchActivity.this, R.string.wait_message);
                                Log.d("MainActivity", "Running ");
                                new VolleyService(TestLaunchActivity.this, TestLaunchActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress +GET_ALL_USERS, null, "101");
                           }
                           });
                    }
                }
                },
                    40000,
                    30000);
        } else
            Log.d(TAG,"Not Starting InternetCheck Timer ");

        // Toast.makeText(getApplication(), "Timer Once again called ", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
    private String ReadSSOServerAddressDetails() {

        String ipAddr = null;
        String filePath=Environment.getExternalStorageDirectory() +"/ssoserverip.txt";

        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

       // GlobalVariables.SSOIPAddress=Constants.SERVER_IPADDR;
        try {

            Log.d("Main2Activity","in ReadSSOServerAddressDetails "+filePath);
            InputStream is = new FileInputStream(filePath);

            int size = is.available();
            Log.d("Main2Activity","Size of the JSON is "+size);
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();

            ipAddr = new String(buffer, "UTF-8");

            Log.d(TAG,"IP Addr is "+ipAddr);
            if (ipAddr.contains(".")) {
                if (!GlobalVariables.SSOIPAddress.equals(ipAddr)) {
                    GlobalVariables.SSOIPAddress = ipAddr;

                    SharedPreferences.Editor editor1 = prefs.edit();

                        editor1.putString(GlobalVariables.SP_ServerIP, GlobalVariables.SSOIPAddress);

                        Log.d(TAG,"SSO Server IP Changed is "+GlobalVariables.SSOIPAddress);

                    editor1.commit();

                }
            }
            Log.d(TAG, "It is Proper IP Address " + GlobalVariables.SSOIPAddress);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.d("Main2Activity","Coming out of ReadSSOServerAddressDetails "+GlobalVariables.SSOIPAddress);
        return ipAddr;
    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        Log.d(TAG, "notifySuccess: " + response.toString());

        InternetFine=true;

        try {
            Log.d(TAG,"Stopping the Timer ");
            if (InternetCheckTimer != null)
                InternetCheckTimer.cancel();
            InternetCheckTimer= null;
            MyProgressDialog.dismiss();
        } catch (Exception obj) {
            Log.d(TAG, "Got exception " + obj.getMessage());
        }
        Log.d(TAG, "Befor Gson");
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<User>>() {
        }.getType();
        Log.d(TAG, "Befor allTasks data");
        List<User> allTasks = gson.fromJson(response.toString(), listType);
        Log.d(TAG, "notifySuccess: " + allTasks.size());
        appDatabase.userDao().deleteAll();
        Log.d(TAG, "After deleteAll");
        appDatabase.userDao().insertAll(allTasks);
        Log.d(TAG, "Befor Intent");

        //Intent intent = new Intent(LaunchActivity.this, Main2Activity.class);
        Intent intent = new Intent(TestLaunchActivity.this, FingerScannActivity.class);

        startActivity(intent);
        finish();


    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        MyProgressDialog.dismiss();
        Log.d(TAG, "notifyError: " + error.toString());
        InternetFine=false;
        //Log.d(TAG,"Notify Error finishing Activity");
        //finishAffinity();
    }



    public void DownloadUserData() {
        Log.d(TAG,"It is DownloadUserData");
        MyProgressDialog.show(TestLaunchActivity.this, R.string.wait_message);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG,"Sending Get User Data ");
                //SendingMessageWithState("finger","101","fingersuccess");
                //String url = GlobalVariables.SSOIPAddress + GET_ALL_USERS;
                //Log.d(TAG,"Login url is "+url);
                //new VolleyService(TestLaunchActivity.this, TestLaunchActivity.this).tokenBase(GET, url , null, "501");
                new VolleyService(TestLaunchActivity.this, TestLaunchActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");

            }
        }, 500);

    }
}