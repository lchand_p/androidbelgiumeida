package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hidglobal.biosdk.BioDeviceStatus;
import com.hidglobal.biosdk.BioSDKAPI;
import com.hidglobal.biosdk.BioSDKDevice;
import com.hidglobal.biosdk.BioSDKFactory;
import com.hidglobal.biosdk.BioSDKVisitor;
import com.hidglobal.biosdk.ImageTools;
import com.hidglobal.biosdk.listener.IBioSDKDeviceListener;
import com.hidglobal.biosdk.listener.ICaptureListener;
import com.hidglobal.biosdk.listener.IWaitForFingerClearListener;
import com.luxand.facerecognition.FaceSuccessActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import Led.DeviceManager;
import database.AppDatabase;
import frost.AdminVerificationActivity;
import frost.EnrollTypeActivityNew;
import frost.FIngerVerifiedActivity;
import frost.FaceVerifiedActivity;
import frost.VoiceVerifiedActivity;
import frost.timeandattendance.TerminalFaceCheckInActivity;
import lumidigm.HomeEnrollActivity;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.captureandmatch.models.AllTasks;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.utils.Alertwithtwobuttons;
import lumidigm.vcomdroid.ANSI378TemplateHelper;
import lumidigm.vcomdroid.Minutiae;
import lumidigm.vcomdroid.VCOM;
import webserver.AndroidWebServer;
import webserver.DiscoverWebServer;
import webserver.GlobalVariables;
import webserver.USBCommands;
import webserver.USBService;
import webserver.WebUtils;
import webserver.WebserverBroadcastRxService;

import static com.android.volley.Request.Method.POST;
import static com.android.volley.Request.Method.PUT;
import static com.hidglobal.biosdk.BioDeviceStatus.BIOSDK_OK;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_DONE;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_FINGER_PRESENT;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_PROCESSING;
import static lumidigm.constants.Constants.GET_ASSIGNED_TASKS;
import static lumidigm.constants.Constants.LOGOUT;
import static lumidigm.constants.Constants.LOG_SERVER;
import static lumidigm.constants.Constants.SERVER_IPADDR;
import static lumidigm.constants.Constants.SSO_SERVER_DEVICE_STATUS;
import static webserver.GlobalVariables.EnrollCount;
import static webserver.GlobalVariables.M21Support;
import static webserver.GlobalVariables.NoFingerPrintSupport;
import static webserver.GlobalVariables.TimeandAttendence;
import static webserver.GlobalVariables.ledSupport;
import static webserver.GlobalVariables.onlyVerification;
import static webserver.GlobalVariables.productVersion;
import static webserver.GlobalVariables.selectedProduct;
import static webserver.GlobalVariables.supervisionEnrolmentStatus;
import static webserver.GlobalVariables.supervisorUser;
import static webserver.GlobalVariables.vcomdatabaseSupport;

/*
http://localhost:8114/user_enroll/logs
method:post
parameters:::::{
"log_type":"ss",
"log_time":"4444444",
"log_use_id":"4",
"log_ass_id":"34",
"log_location":"log_location",
"log_message":"log_message",
"log_status":"34",
"log_key_id":"2",
"log_dev_type":"3",
"log_use_id_2":"23",
"log_loc_id":"23"

 */

/*
                    GlobalVariables.comeOut=0;
    For first boot once Finger print is clicked, next time Figner Print Module is Going off
    - So after Sending to other Activity and finish, if still the CaptureTask is running -
    Then count the number of attemts and Exit the App (in OpentDoor / NotAthorized / TerminalCheckinActivity
*/
public class Main2Activity extends Activity implements View.OnClickListener, IResult, ICaptureListener, IWaitForFingerClearListener,IBioSDKDeviceListener,IFragmentListener, BioSDKVisitor {

//Button Match;Button cap;

    private static final String TAG = "Main2Activity";
    Button settings;
    Button enroll;


    // VCOM Java/JNI adapter.
    private VCOM mVCOM = null;
    Bitmap mBitmapMatch;
    User user;

    AppDatabase appDatabase;

    public static boolean check = true;
    public static boolean toastcheck = true;
    public static boolean notauthorized = false;
    //TextView textView;

    private Handler mHandler;
    //Button rescan, help;
    AlertDialog alertDialog = null;
    SharedPreferences prefs = null;
    private AndroidWebServer androidWebServer;
    private static boolean isStarted = false;
    private static final int DEFAULT_PORT = 8080;
    public static boolean comeOutofApp = false;
    public boolean inCapture = false;
    public static boolean inCaptureTest = false;
    private Timer sendDeviceStatusTimer = null;
    private int TimerCountTest = 0;
    // private boolean adminVerification=true; //In Second Admin Verification added
    private boolean adminVerificationInitiated = false; //true;
    private ImageView imageView;
    private ImageView imageView2;
    private ImageView imageView3;
    private TextView usernameDet;
    private BroadcastReceiver mUpdateReceiver;
    private IntentFilter mUpdateFilter;
    private DeviceManager ledStatus = null;
    private boolean OutofActivity = false;
    private static long lastUnAuthorizedTime = 0;
    public static int fingermoduleCount = 0;

    private int mPADResult;
    boolean mIsWaitForFingerClearRunning = false;
    private boolean mCancelCapture = false;
    boolean mWaitForFingerClear = false;
    boolean mCaptureInProgress = false;
    private IFragmentListener mListener;
    private Bitmap mFingerImage;
    private byte[] mTemplate;
    byte[] mProbeTemplate;

    //private int mPADResult;
    private int mTimeOut = 500;
    private  BioSDKDevice  mFPDevice = null;
    private String mMatchLevel = "MEDIUM";
    private String mPADLevel = "MEDIUM";
    BioDeviceStatus mStatus;
    private boolean goingForEnroll = true;
    private Button TestEnrollment=null;
    //ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_main2);
        Log.d(TAG, "on Create Main2Activity first");
        setContentView(R.layout.activity_enroll);
        Log.d(TAG, "on Create Main2Activity");
        appDatabase = AppDatabase.getAppDatabase(Main2Activity.this);
        //View overlay = findViewById(R.id.mylayout);

        if (M21Support) {
            try {
                Log.d(TAG, "M21 Initializing ");
                BioSDKFactory.initializeBioSDKAPI(getApplicationContext());
                Log.d(TAG, "Initializing BIOSDK successful ");
/*

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Log.d(TAG, "Calling initEngine ");
                        initEngine();
                    }
                }, 1000);
*/

                initEngine();

/*
                BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
                if (bAPI == null) {
                    Log.d(TAG, "BIO SDK No device connected ");
                       popupDialog("No device connected.", "Exit", false);

                } else
                    Log.d(TAG,"getBIOSDK is successfule");


                //The one-time open of the device
                mFPDevice = bAPI.openDevice(0);
                if (mFPDevice == null) {
                    Log.d(TAG, "No MX21 Device connected ");
                    popupDialog("No device connected.", "Exit", false);

                    // return false;
                } else
                    Log.d(TAG,"M21 device got opened ");

                Log.d(TAG,"setBioSDKDeviceLiseneter before ");
                mFPDevice.setBioSDKDeviceListener(this);
                Log.d(TAG,"setBioSDKDeviceLiseneter After ");
*/


            } catch (Exception obj) {
                Log.d(TAG, "Got exception while initializing M21 " + obj.getMessage());
            }
        }

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);
/*

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
*/

        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } catch (Exception obj) {
            Log.d(TAG, "Got Exception while setting Full Screen");
        }
        OutofActivity = false;
        mHandler = new Handler();
        //textView = (TextView) findViewById(R.id.welcome_text1);
        comeOutofApp = false;
        GlobalVariables.comeOut = 0;
        adminVerificationInitiated = false;
        //textView.setOnClickListener(this);
        //imageView=(ImageView)findViewById(R.id.image_hint);
        settings = (Button) findViewById(R.id.settings_new);
        //  enroll = (Button) findViewById(R.id.enroll2);
        enroll = (Button) findViewById(R.id.button1);
        //enroll.setVisibility(View.INVISIBLE);

              if (GlobalVariables.WorkFlowVal == 2 && supervisorUser == true)
            enroll.setVisibility(View.VISIBLE);

        //  ReadSSOServerAddressDetails();
        if (GlobalVariables.SSOIPAddress == null)
            GlobalVariables.SSOIPAddress = SERVER_IPADDR;

        // GlobalVariables.supervisorUser=false;
        //When OnlyVerifcation is enabled - disable Enroll button
        if (onlyVerification == true) {
            Log.d(TAG, "Only Verification is true - Disable Enroll");
            enroll.setEnabled(false);
        }
        settings.setOnClickListener(this);

        if (NoFingerPrintSupport) {
            Log.d(TAG,"No Finger Print Support is true ");
            enroll.setEnabled(true);
            enroll.setVisibility(View.VISIBLE);
            enroll.setText("Verify");
        }

        enroll.setOnClickListener(this);

        notauthorized = false;
        //rescan = findViewById(R.id.rescann);
        //rescan.setOnClickListener(this);
        //help = findViewById(R.id.help);
        //help.setOnClickListener(this);
        Log.d(TAG, "MainActivity oncreate VCOM Open");

        int TerminalId = prefs.getInt(GlobalVariables.SP_TerminalID, -1);

        Log.d(TAG, "Terminal ID is " + TerminalId + "  SupervisionEnrollmentStatus is " + supervisionEnrolmentStatus);
        if (TerminalId == -1)
            Log.d(TAG, "Terminal ID is not mapped ");
        else if (M21Support == true) {

        } else if (supervisionEnrolmentStatus == false) {
            Log.d(TAG, "It is Not superVisionEnrollmentStatus");
            if (GlobalVariables.MobileDevice == false)
                mVCOM = new VCOM(this);
        }
        //mVCOM = null;
        /*, new VCOM.PermissionSuccuess() {
            @Override
            public void permissionsSuccuess() {
                new CaptureAndMatchTask().execute(0);
            }

            @Override
            public void onPermissionAlradyDone() {
                new CaptureAndMatchTask().execute(0);
            }
        });*/
        check = true;
        //imageView.setImageResource(R.drawable.finger_background);
        //textView.setText(getResources().getString(R.string.welcometext));

        settings.setVisibility(View.VISIBLE);
        //enroll.setVisibility(View.VISIBLE);

        //  rescan.setVisibility(View.GONE);
        // help.setVisibility(View.GONE);
        Log.d(TAG, "MainActivity oncreate before Capture And Match Task Supervisio Enrollment Status " + supervisionEnrolmentStatus);
        Log.d(TAG, "Mobile Device " + GlobalVariables.MobileDevice);

        //inCapture=true;
        Log.d(TAG, "In Capture Status is " + inCapture + "  prev " + inCaptureTest + " Database support " + vcomdatabaseSupport);
        //Chand commented - this not needed
        if (TerminalId == -1)
            Log.d(TAG, "Next Terminal ID is not mapped ");
        else if (M21Support == true) {
            Log.d(TAG, "M21 Support is enabled so Waiting for the Listener ");
        } else if ((supervisionEnrolmentStatus == false) && (vcomdatabaseSupport == 1)) {
            Log.d(TAG, "Starting Database Capture and Match ");
            new DatabaseCaptureAndMatchTask().execute(0);
        } else if (supervisionEnrolmentStatus == false) {
            if (GlobalVariables.MobileDevice == false)
                new CaptureAndMatchTask().execute(0);
        } else
            Log.d(TAG, "Non SUpervision Capture and Match Task ");

       /* textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CaptureAndMatchTask().execute(0);
            }
        });*/
        // new CaptureAndMatchTask().execute(0);
        //  prefs = getBaseContext().getSharedPreferences(
        //        "FaceDetection", MODE_PRIVATE);


        RemoveRemoteDeviceStatus();
        if (GlobalVariables.WebServer) {
            startAndroidWebServer();
        }
        if (GlobalVariables.AppSupport == 1) {
            //if (isConnectedInWifi()) {
            if (GlobalVariables.MobileDevice) {
                Log.d(TAG, "It is Conected Wifi and starting broadcast receiver task");
                Intent it = new Intent(getApplicationContext(), DiscoverWebServer.class);

                getApplicationContext().startService(it);
            } else {
                Log.d(TAG, "It is for Ethernet");
                Intent it = new Intent(getApplicationContext(), WebserverBroadcastRxService.class);
                getApplicationContext().startService(it);
            }
        } else if (GlobalVariables.AppSupport == 2) { //For USB Support
            // if (isEthernetConnected()) {
            //   StartUSBService();
            //}
        }

        imageView = (ImageView) findViewById(R.id.sentinellogo);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // onImageClick();
                Log.d(TAG, "Coming out of App");

                if (M21Support) {
                    try {
/*

                        Log.d(TAG,"on Destroy before getBioSDK");
                        BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
                        if (bAPI != null)
                            BioSDKFactory.releaseBioSDKAPI();
*/

                        BioSDKFactory.releaseBioSDKAPI();
                    } catch (Exception obj) {
                        Log.d(TAG,"Going for the release sdk exeption "+obj.getMessage());
                    }

                    //finishAffinity();
                } else {
                    //mVCOM.Close();
                    mVCOM.Close();
                    mVCOM = null;
                    comeOutofApp = true;

                   // finishAffinity();
                }
                Log.d(TAG,"Got the Sentinel Logo exit");
                Log.d(TAG,"Coming out of the App");
                comeOutofApp=true;
                Intent intent= new Intent(getApplicationContext(), SettingsActivity.class);

                Log.d(TAG,"selected Product is "+selectedProduct);

                if (selectedProduct == 1)
                    intent = new Intent(getApplicationContext(), FaceSettings.class);

                startActivity(intent);
                //  GlobalVariables.comeOut=1;
                finishAffinity();

            }
        });
        if (GlobalVariables.MobileDevice == false)
            RegisterInterReceiver();

        //RegisterReceiver();
        sendDeviceStatusInfo();
        sendDeviceStatusTimerTask();

        //for Testing remove it
        //InitiateFingerPrintAdd("123");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        check = true;
        OutofActivity = false;
        Log.d(TAG, "onResume ComeoutofApp is false");
        if (GlobalVariables.MobileDevice == false)
            RegisterReceiver();
        comeOutofApp = false;
        GlobalVariables.comeOut = 0;
        Log.d(TAG, "On Resume Admin Verification initiated ");
     /*   setContentView(R.layout.activity_enroll);
        settings = (Button) findViewById(R.id.settings_new);
        enroll = (Button) findViewById(R.id.button1);*/
        Log.d(TAG, "After enroll");
        if (onlyVerification == true) {
            Log.d(TAG, "Only Verification is true - Disable Enroll");
            enroll.setEnabled(false);
        } else
            enroll.setEnabled(true);

        if (adminVerificationInitiated) {
            Log.d(TAG, "On Resume Admin Verification initiated ");
            setContentView(R.layout.activity_enroll);
            settings = (Button) findViewById(R.id.settings_new);
            enroll = (Button) findViewById(R.id.button1);

            if (GlobalVariables.WorkFlowVal == 2 && supervisorUser == true) //supervisorUser)
                enroll.setVisibility(View.VISIBLE);
            else
                enroll.setVisibility(View.INVISIBLE);

        }
        adminVerificationInitiated = false;
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if ((GlobalVariables.MobileDevice == false) && (check)) {
            Log.d(TAG, "MainActivity onResume  fore CaptureAndmMatchTask");

            if (M21Support == true) {
                Log.d(TAG, "M21Support Chand enabled so onResume nothing ");
            } else if (supervisionEnrolmentStatus == false) {
                if (mVCOM == null)
                    mVCOM = new VCOM(this);
            }
            Log.d(TAG, "In Capture Status when check false is " + inCapture + "  prev " + inCaptureTest);

            try {
                if (M21Support == true) {
                    Log.d(TAG, "M21Support Chand enabled so onResume nothing ");

                    try {
                       /* Log.d(TAG, "M21 Initializing ");
                        BioSDKFactory.initializeBioSDKAPI(getApplicationContext());
                        Log.d(TAG, "Initializing BIOSDK successful ");

                        initEngine();
*/
                        //initializeDevice();


                        //onEnroll();

                    } catch (Exception obj) {
                        Log.d(TAG,"Got exception while on Resume "+obj.getMessage());
                    }
                } else if (supervisionEnrolmentStatus == false) {
                    if (inCapture == false) {
                        Log.d(TAG, "inCaputre if false so handling request ");
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                Log.d(TAG, "Calling Capture and Match Task ");
                                new CaptureAndMatchTask().execute(0);
                            }
                        }, 1000);
                    }
                } else
                    Log.d(TAG, "In Non SUpervision enrollment Mode ");
            } catch (Exception obj) {
                Log.d(TAG, "OnResume got exception ");
            }
        }
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (GlobalVariables.MobileDevice == false)
            UnRegisterReceiver();

        Log.d(TAG, "MainActivity on Pause VCOM Close ");
        if (M21Support == true) {

        } else if (supervisionEnrolmentStatus == false && GlobalVariables.MobileDevice == false) {
            if (mVCOM != null) {
                mVCOM.Close();
            } else
                Log.d(TAG, "MainActivity on Pause VCOM not Close ");
        } else
            Log.d(TAG, "It is in Non SuperVisionernolemnt mode ");
        //Chand added below
        //mVCOM=null;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button1:
                if (NoFingerPrintSupport == true) {
                    NoFingerCaptureUserID();
                    Log.d(TAG,"It is Enrollment");
                    break;
                }
            case R.id.enroll2:
                if (GlobalVariables.supervisorUser) {
                    try {
                        if (GlobalVariables.adminVerification) {
                            setContentView(R.layout.activity_main2_supervisor);

                     /* imageView = (ImageView) findViewById(R.id.imageView1);
                      imageView.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              adminFaceVerified();
                          }
                      });

                      imageView2 = (ImageView) findViewById(R.id.imageView2);
                      imageView2.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              adminFingerVerified();
                          }
                      });

                      imageView3 = (ImageView) findViewById(R.id.imageView6);
                      imageView3.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              adminVoiceVerified();
                          }
                      });*/
                            adminVerificationInitiated = true;

                            settings = (Button) findViewById(R.id.settings_new);
                            Log.d(TAG, "Admin Verification Initiated  onClick");
                        } else {
                            mVCOM.Close();
                            comeOutofApp = true;
                            Log.d(TAG, "CLosed the VCOM ComeOutofApp true");
                            check = false;
                            EnrollCount = 0;
                            Log.d(TAG, "CHECK is false ");
                            Log.d(TAG, "Going for Supervision Identification ");
                            //Log.d(TAG, "Enrolment Status RoleID " + user.getRoleID() + " RoleName" + user.getRoleName());
                            Log.d(TAG, "It is SupervisionEnrolmentStatus is true Starting Enrollment ");

                            Intent i = new Intent(getApplicationContext(), SupervisiorIdentity2Activity.class);
                            startActivity(i);
                        }

                        // finish();
                    } catch (Exception obj) {
                        Log.d(TAG, "Got exception ");
                    }
                } else if (supervisionEnrolmentStatus == false) {
                    Log.d(TAG, "Starting Enroll ");
                    Intent intent = new Intent(this, Enroll.class);


                    //Intent intent = new Intent(this, HomeEnrollActivity.class);
                    intent.putExtra("userid", "test");
                    intent.putExtra("fromsupervisor", false);
                    // Intent i = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    comeOutofApp = true;
                    // startActivity(i);
                    //TODO implement
                } else {
                    Log.d(TAG, "It is Supervisionmode Enrolment");


                    if (!GlobalVariables.MobileDevice) {

                        //      enroll.setVisibility(View.GONE);
                        // Toast.makeText(Main2Activity.this, "Please Verify your finger print", Toast.LENGTH_SHORT).show();


                        Intent intent = new Intent(this, AdminVerificationActivity.class);
                        // Intent intent = new Intent(this, AuthenticationHomeEnrollActivity.class);

                        // Intent i = new Intent(this, MainActivity.class);

                        intent.putExtra("userid", "test");
                        intent.putExtra("fromsupervisor", true);
                        startActivity(intent);

 /*                       mVCOM = new VCOM(this);

                        Handler handler=new Handler();
                        check=true;
                        Log.d(TAG,"Starting CaptureNad match Task going for sleep");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {

                                Log.d(TAG,"SuperVision Calling Capture and Match Task ");
                                new CaptureAndMatchTask().execute(0);
                            }
                        },1000); */
                        //new CaptureAndMatchTask().execute(0);
                    }
                }
                break;
            case R.id.settings_new:
               /* check = false;
                Intent i1 = new Intent(this, UserAuthenticationActivity.class);
                startActivity(i1);*/
                //Toast.makeText(Main2Activity.this, "Super User Verification", Toast.LENGTH_SHORT).show();
                startSettings();
                // GlobalVariables.supervisorUser=true;
                //TODO implement
                break;

            case R.id.welcome_text:
                Log.d(TAG, "MainActivity on WelcomeTst CaptureAndMatchTask");
                //Chand Commented here
                // new CaptureAndMatchTask().execute(0);
                break;
            //case R.id.rescann:
            //  Log.d(TAG,"MainActivity on Rescan CaptureAndMatchTask");
            //   new CaptureAndMatchTask().execute(0);
            //  check=true;
            //   imageView.setImageResource(R.drawable.finger_background);
            //      textView.setText(getResources().getString(R.string.welcometext));
            //  settings.setVisibility(View.VISIBLE);
            //  enroll.setVisibility(View.VISIBLE);

            //       rescan.setVisibility(View.GONE);
            //     help.setVisibility(View.GONE);


            //   break;
            //case  R.id.help:

            //  break;
        }
    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        MyProgressDialog.dismiss();
        //check = false;
        Log.d(TAG, "CHECK is false now notifysuccess");
        Log.d(TAG, "notifySuccessdddd: " + response.toString());

        if (requestType.equals("210")) {

            Log.d(TAG, "Device Status Response is  " + response.toString());
            try {
                //last_config_update - 1/0
                //terminal_status - 1/0
                //terminal_id- -1/Valid value

                JSONObject deviceStatus = new JSONObject(response.toString());
                int TerminalID = deviceStatus.optInt("terminal_id");
                int last_config_update = deviceStatus.optInt("terminal_config_update");

                Log.d(TAG, "Rcvd Terminal ID is " + TerminalID);
                Log.d(TAG, "Last Config Update " + last_config_update);
                int last_users_config_update = deviceStatus.optInt("users_config_update");

                //FOr Test Remove it
             /*   TimerCountTest++;
                Log.d(TAG,"Timer COunt is "+TimerCountTest);
                if (TimerCountTest > 2)
                    last_users_config_update=1;
                //Above Testing remove it
*/
                // int last_users_config_update = deviceStatus.optInt("users_config_update");
                int pending_terminal_config_update = prefs.getInt(GlobalVariables.pending_terminal_config_Update, 0);

                int pending_usersconfig_update = prefs.getInt(GlobalVariables.pending_users_Config_Update, 0);
                Log.d(TAG, " Pending Config Update is " + last_config_update);


                if ((pending_terminal_config_update != last_config_update) || (last_users_config_update != pending_usersconfig_update)) {
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putInt(GlobalVariables.pending_terminal_config_Update, last_config_update);
                    editor1.putInt(GlobalVariables.pending_users_Config_Update, last_users_config_update);

                    Log.d(TAG, "Saving the last config update " + last_config_update);
                    editor1.commit();
                }

                if (TerminalID != -1) {
                    Log.d(TAG, "Terminal ID is " + TerminalID);

                    //prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                    int Global_TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);

                    if (TerminalID != Global_TerminalID) {

                        SharedPreferences.Editor editor1 = prefs.edit();

                        editor1.putInt(GlobalVariables.SP_TerminalID, TerminalID);
                        Log.d(TAG, "Storing TerminalID " + TerminalID);
                        GlobalVariables.TerminalID = TerminalID;
                        editor1.commit();
                    } else {
                        Log.d(TAG, "No Change in TermnalID " + Global_TerminalID);
                        GlobalVariables.TerminalID = Global_TerminalID;
                    }
                } else
                    Log.d(TAG, "Terminal is not mapped ");

                try {
                    Log.d(TAG, "Work flow  before parsing");
                    String workflowString = deviceStatus.optString("workflows");
                    //JSONObject workflowString2= deviceStatus.optJSONObject("workflows");

                    if (workflowString != null) {
                        Log.d(TAG, "Current Work flow is " + workflowString.toString());
                        String workflowstr = workflowString.toString();

                        Log.d(TAG, "Work flow Strng is " + workflowstr);
                        JSONObject deviceworkflow = new JSONObject(workflowstr.trim());

                        String Workflowname = deviceworkflow.optString("wor_name");
                        try {
                            if (Workflowname == null)
                                Workflowname = deviceworkflow.getString("wor_name");

                        } catch (Exception obj) {

                        }
                        Integer currWorkFlow = 0;

                        Log.d(TAG, "Current Workflow Name is " + Workflowname);
                        if (Workflowname.equals("access_control")) {
                            currWorkFlow = 1;
                            //GlobalVariables.WorkFlowVal = 1;
                        } else if (Workflowname.equals("supervision_enrollment")) {
                            currWorkFlow = 2;
                        } else if (Workflowname.equals("normal_enrollment")) {
                            currWorkFlow = 3;
                        } else if (Workflowname.equals("frost")) {
                            currWorkFlow = 4;
                        } else if (Workflowname.equals("restricted_access")) {
                            currWorkFlow = 5;
                        }
                        //Sentinel Work Flow starts
                        else if (Workflowname.equals("tv_support")) {
                            currWorkFlow = 6;
                        } else if (Workflowname.equals("terminal_startexit")) {
                            currWorkFlow = 7;
                        } else if (Workflowname.equals("guardstation_waitingarea")) {
                            currWorkFlow = 8;
                        } else if (Workflowname.equals("detainee_checkpoints")) {
                            currWorkFlow = 9;
                        } else if (Workflowname.equals("detainee_outside_block")) {
                            currWorkFlow = 10;
                        } else if (Workflowname.equals("guard_in_block")) {
                            currWorkFlow = 11;
                        } else if (Workflowname.equals("escort_boothguard_station")) {
                            currWorkFlow = 12;
                        } else if (Workflowname.equals("waiting_area_visitor")) { //detainee_terminal_startexit
                            currWorkFlow = 13;
                        } else if (Workflowname.equals("escort_task_select")) {
                            currWorkFlow = 14;
                        } else if (Workflowname.equals("boothguard_outside")) {
                            currWorkFlow = 15;

                        } else if (Workflowname.equals("sentinel_userlogin_changeworkflow")) {
                            currWorkFlow = 20;
                        } else
                            Log.d(TAG, "Flow work is null");
                        if (currWorkFlow != GlobalVariables.WorkFlowVal)
                            GlobalVariables.WorkFlowVal = currWorkFlow;

                        int workflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

                        if (currWorkFlow != workflowID) {
                            SharedPreferences.Editor editor1 = prefs.edit();
                            editor1.putInt(GlobalVariables.workflowStatus, currWorkFlow);

                            SetWorkFlow(currWorkFlow);
                            Log.d(TAG, "Saving the work flow status " + currWorkFlow);
                            editor1.commit();
                        }
                    } else
                        Log.d(TAG, "Workflow is null");
                } catch (Exception obj) {
                    Log.d(TAG, "Got Exception while reading the Workflow" + obj.getMessage());
                }


                if ((last_config_update != 0) || (last_users_config_update != 0)) {
                    Log.d(TAG, "Needs Configuration Download 123");
                   /* Intent intent = new Intent(Main2Activity.this, LaunchActivity.class);
                    // Intent intent = new Intent(LaunchActivity.this, TestLaunchActivity.class);
                    startActivity(intent);*/
                    if (mVCOM != null)
                        mVCOM.Close();
                    toastcheck = false;
                    this.finishAffinity();
                    System.exit(0);

                  /*  Handler handler=new Handler();
                    Log.d(TAG,"Starting Timers to go to Launcher activity");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            Intent intent = new Intent(Main2Activity.this, LaunchActivity.class);
                            // Intent intent = new Intent(LaunchActivity.this, TestLaunchActivity.class);
                            startActivity(intent);
                            finishAffinity();
                            Log.d(TAG,"Starting the LaunchActvity for Configuration update ");
                            //new CaptureAndMatchTask().execute(0);
                        }
                    },15000);*/
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got JSON Exception obj");
            }
        } else if (requestType.equals("105")) {

            check = false;
            try {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<AllTasks>>() {
                }.getType();

                Log.d(TAG, "Response " + response);

                if (response != null) {

                    Log.d(TAG, "Response is not null" + response.toString());
                    ArrayList<AllTasks> allTasks = gson.fromJson(response.toString(), listType);

                    Log.d(TAG, "Checking the response list typew");

                    ArrayList<AllTasks> allTasks1 = allTasksList(allTasks);
                    Log.d(TAG, "Checking the response list typew" + allTasks1.size());
                    Log.d(TAG, "User Status is " + user.getStatus());
                    if (allTasks1.size() > 0) {
                        JSONObject jsonObject2 = new JSONObject();
                        try {
                            jsonObject2.put("_id", user.get_id());
                            Date currentTime = Calendar.getInstance().getTime();
                            Log.d(TAG, "onPostExecute: " + currentTime.toString());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            String currentDateandTime = sdf.format(new Date());

                            jsonObject2.put("logged_in_time", currentDateandTime);
                            try {
                                Log.d(TAG, "Get Time of Day");
                                Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

                                //Added 4and Half Hours to show India Time on Serv er -- for testing
                                //Date currentTime2 = curCalendar.getInstance().getTime();
                                String currts = Long.toString(curCalendar.getTimeInMillis() / 1000L + 16200);
                                String curr1 = Long.toString(System.currentTimeMillis() / 1000L);
                                jsonObject2.put("logged_in_time_secs", currts);
                                Log.d(TAG, "Current Time stamp is GMT " + currts);
                                Log.d(TAG, "Current Time stamp is " + curr1);

                            } catch (Exception obj) {

                            }
                            Log.d(TAG, "onPostExecute: " + currentDateandTime);
                            Log.d(TAG, "onPostExecute: " + jsonObject2);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Log.d(TAG,"All Tasks "+allTasks1.get(0).getTagetAes_key().trim);
                        Log.d(TAG, "All Tasks  Terminal " + allTasks.get(0).getTags().get(0).getTerminalID());
                        Log.d(TAG, "All Tasks  Door " + allTasks.get(0).getTags().get(0).getDoor().trim());
                        // Log.d(TAG,"All Tasks  Role "+allTasks.get(0).getTags().get(0));
                        int locId = allTasks.get(0).getTags().get(0).getLocationID();

                        String location = allTasks.get(0).getTags().get(0).getLocation().trim();
                        //  Log.d(TAG,"All Tasks  Terminal "+allTasks.get(0).getTags().get(0).getRoleName().trim());


                        // prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                        GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                        Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);

                        sendLogInfo("Log", user.get_id(), locId, "Log In", "Success", location, GlobalVariables.TerminalID);
                /*
                Needs to Check Whether Terminal ID is matching with User assigned TerminalID,
                if yes, then only open the door

                 */
                        //  prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                        GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                        Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);


                        /// new VolleyService(Main2Activity.this, Main2Activity.this).tokenBase(PUT, GlobalVariables.SSOIPAddress + USER_LOGIN, jsonObject2, "101");

                        Log.d(TAG, "user.getStatus is " + user.getStatus());
                        //
                   /* if (user.getStatus().equals("offline") || user.getStatus().equals("")) {

                        Log.d(TAG,"User Status is Offline ");
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("_id", user.get_id());
                            Date currentTime = Calendar.getInstance().getTime();
                            Log.d(TAG, "onPostExecute: " + currentTime.toString());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            String currentDateandTime = sdf.format(new Date());
                            jsonObject.put("logged_in_time", currentDateandTime);
                            Log.d(TAG, "onPostExecute: " + currentDateandTime);
                            Log.d(TAG, "onPostExecute: " + jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        new VolleyService(Main2Activity.this, Main2Activity.this).tokenBase(PUT, GlobalVariables.SSOIPAddress + USER_LOGIN, jsonObject, "101");
                    } else*/
                        if (user.getStatus().equals("online") || user.getStatus().equals("on") || user.getStatus().equals("offline")) {
                            //  ArrayList<AllTasks> pendingtasks = allPendingTasks(allTasks1);

                            Log.d(TAG, "User Status is Online ");
                            if (allTasks1.size() > 0) {
                                check = false;
                                Log.d(TAG, "CHECK false before USER AUthentication");
                                Intent i1 = new Intent(Main2Activity.this, UserAuthenticationActivity.class);
                                Bundle bn = new Bundle();
                                bn.putBoolean("login", false);

                                bn.putParcelable("User", user);
                                bn.putParcelable("bitmap", mBitmapMatch);
                                i1.putExtras(bn);

                                //Chand added
                                if (inCapture == false) {
                                    Log.d(TAG, "Calling User Authentication ");
                                    finish();
                                }
                                startActivity(i1);
                            } else if (allTasks1.size() > 0) {

                                Intent i1 = new Intent(Main2Activity.this, UserAuthenticationActivity.class);
                                Bundle bn = new Bundle();
                                //  bn.putBoolean("login", true);
                                bn.putBoolean("login", false);
                                bn.putParcelable("User", user);
                                bn.putParcelable("bitmap", mBitmapMatch);
                                i1.putExtras(bn);

                                //Chand added
                                if (inCapture == false) {
                                    Log.d(TAG, "Calling User Authentication2 ");
                                    finish();
                                }
                                startActivity(i1);

                            } else {
                                Alertwithtwobuttons alertwithtwobuttons = new Alertwithtwobuttons(this, new Alertwithtwobuttons.OnClick() {
                                    @Override
                                    public void onClickOk() {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("_id", user.get_id());
                                            Date currentTime = Calendar.getInstance().getTime();

                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                            String currentDateandTime = sdf.format(new Date());
                                            jsonObject.put("time", currentDateandTime);
                                            jsonObject.put("status", 2);
                                            jsonObject.put("userId", user.getUserId());

                                            jsonObject.put("username", user.getUsername());
                                            jsonObject.put("status_name", "pending");
                                            //jsonObject.put("uid", allTasks.get(0).getUid());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Log.d(TAG, "onClickOk: " + jsonObject.toString());
                                        MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                                        new VolleyService(Main2Activity.this, Main2Activity.this).tokenBase(PUT, LOGOUT, jsonObject, "104");

                                    }

                                    @Override
                                    public void cancelButtonClick() {

                                    }
                                });

                                alertwithtwobuttons.show();


                            }
                        }
                    } else {
                        //check = true;

                        //  NotAuthorizedActivity

                        try {
                            if (GlobalVariables.USBExecute == 1)
                                new USBCommands().executeADBCommands(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                            else

                                new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                            //    WebUtils.("fingerfailure","101","fingerfailure");
                        } catch (Exception obj) {
                            Log.d(TAG, "It is exception");
                        }
                        Log.d(TAG, "Starting NotAUthorized ");
                        Intent i = new Intent(this, NotAuthorizedActivity.class);
                        finish();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("userData", user);
                        i.putExtras(bundle);
                        comeOutofApp = true;
                        startActivity(i);


                    }
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got Crashed and exception is" + obj.toString());
            }
        } else if (requestType.equals("104")) {
            user.setStatus("offline");
            appDatabase.userDao().update(user);

        } else if (requestType.equals("211")) {
            Log.d(TAG, "Got the 211 Response ");
        } else {
            check = false;
            Log.d(TAG, "CHECK false now notifySuccess: " + response.toString());
            Intent i1 = new Intent(Main2Activity.this, UserAuthenticationActivity.class);

            user.setStatus("online");
            appDatabase.userDao().update(user);

            Bundle bn = new Bundle();
            //bn.putBoolean("login", true);
            bn.putBoolean("login", false);
            bn.putParcelable("User", user);
            bn.putParcelable("bitmap", mBitmapMatch);
            i1.putExtras(bn);
            //Chand added
            if (inCapture == false) {
                Log.d(TAG, "Calling User Authentication3 ");
                finish();
            }


            startActivity(i1);
        }
    }

    public ArrayList<AllTasks> allTasksList(ArrayList<AllTasks> allTasks) {
        ArrayList<AllTasks> allTasks1 = new ArrayList<>();

        for (AllTasks allTasks2 : allTasks) {
            if (allTasks2.getStatus() == 0 || allTasks2.getStatus() == 1) {
                allTasks1.add(allTasks2);
            }
        }
        return allTasks1;
    }

    public ArrayList<AllTasks> allPendingTasks(ArrayList<AllTasks> allTasks) {
        ArrayList<AllTasks> allTasks1 = new ArrayList<>();

        for (AllTasks allTasks2 : allTasks) {
            if (allTasks2.getStatus() == 0) {
                allTasks1.add(allTasks2);
            }
        }
        return allTasks1;
    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        MyProgressDialog.dismiss();
        Log.d(TAG, "notifyError: " + error);
        if (requestType.equals("105")) {
            try {
                if (error.toString().contains("not authorized")) {
                    Log.d(TAG, "Self Starting MainActivity");
                    Intent i1 = new Intent(Main2Activity.this, Main2Activity.class);
                    i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i1);
                    finish();
                    Log.d(TAG, "Got Not Authorized Play Not AUthorized Tone ");
                    //Play Tone - User Not AUthorized
                }
            } catch (Exception obj) {

            }
        }
    }
    private void NoFingerCaptureUserID() {

        Log.d(TAG, "NoFingerCaptureUserID");
        LayoutInflater inflater = LayoutInflater.from(Main2Activity.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText) subView.findViewById(R.id.dialogEditText1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Authentication");
        builder.setMessage("Enter UserID");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userID = subEditText.getText().toString().trim();
                Log.d(TAG, "userID is " + userID);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                Log.d(TAG, "Check the User has Encoded data ");
                user = appDatabase.userDao().countUsersBasedonUserID(userID);

                try {
                    if (user != null) {
                        Log.d(TAG, "Processing the Success Message ");
                        lastUnAuthorizedTime = -1; //System.currentTimeMillis();

                        Intent intent = new Intent(getApplicationContext(), FaceSuccessActivity.class);//SuccessActivity
                        if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3)
                            intent = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);

                        intent.putExtra("case", "success");
                        //    intent.putExtra("FromWeb", true);

                        intent.putExtra("personName", user.getUsername());

                        String personID = userID; //user.getUserId();

                        //Log.d(TAG, "Add User " + user.getUsername());


                        Bundle bundle = new Bundle();
                        bundle.putParcelable("User", user);
                        intent.putExtras(bundle);


                        if (personID != null)
                            intent.putExtra("personID", personID);
                        Log.d(TAG, "Check IN status is " + personID);
                        //Log.d(TAG,"Person name is "+currState);
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */
                        alertDialog.cancel();
                        startActivity(intent);
                    } else {
                        Log.d(TAG, "User is Not Available");
                        Toast.makeText(Main2Activity.this, "Invalid User", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception obj) {
                    alertDialog.cancel();
                    Log.d(TAG,"Got the Exception ");
                    Toast.makeText(Main2Activity.this, "Invalid User Got exception", Toast.LENGTH_LONG).show();

                }
//                Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();
                    // finish();
                }
            }
        });


        builder.show();
    }

    private class CaptureAndMatchTask extends AsyncTask<Integer, Integer, User> {

        @Override
        protected User doInBackground(Integer... i) {
            int j = 1;
            try {
                fingermoduleCount++;
                inCapture = true;
                inCaptureTest = true;
                if (check) {

                    int rc = 10;
                    Log.d(TAG, "in CaputreAndMatchTask");
                    if (mVCOM != null)
                        rc = mVCOM.Open();

                    if (rc == 0) {

                        rc = mVCOM.Capture();

                        if (rc == 0) {
                            int tmplSize = mVCOM.GetTemplateSize();
                            if (tmplSize > 0) {
                                byte[] tmpl = new byte[tmplSize];
                                rc = mVCOM.GetTemplate(tmpl);
                                if (rc == 0) {
                                    playBeep();
                                    Log.d(TAG, "doInBackground: toastcheck" + toastcheck);
                                    toastcheck = true;

                                    List<User> users = appDatabase.userDao().getAll();

                                    long currTimemillisec = System.currentTimeMillis();
                                    long Timeinmillisec = 0;

                                    //Test Code Remove below
                             /*   for (int tmpVal=0;tmpVal < 100; tmpVal++) {
                                    for (User user : users) {
                                        try {

                                            Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                                            currTimemillisec = System.currentTimeMillis();
                                            Log.d(TAG, "Main Curr Milli sec " + currTimemillisec + "  diff  " + Timeinmillisec);

                                            j = 1;
                                            //Log.d(TAG, "doInBackground: " + user.toString());
                                            String fingarprint1 = user.getTemplate1();
                                            byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                            rc = mVCOM.Match(tmpl, mTmplCapture1);
                                            int score1 = mVCOM.GetMatchScore();
                                            int score2 = 0, score3 = 0;
                                            //if (score1 < 50000)
                                            {
                                                j++;
                                                String fingarprint2 = user.getTemplate2();
                                                byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);


                                                rc = mVCOM.Match(tmpl, mTmplCapture2);
                                                score2 = mVCOM.GetMatchScore();

                                                //if (score2 < 50000)
                                                {
                                                    j++;
                                                    String fingarprint3 = user.getTemplate3();
                                                    byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);

                                                    rc = mVCOM.Match(tmpl, mTmplCapture3);
                                                    score3 = mVCOM.GetMatchScore();
                                                }
                                            }
                                            Log.d(TAG, "Score 1" + score1 + " Score 2" + score2 + " Score 3" + score3 + " Count is " + j);

                                        } catch (Exception obj) {
                                           }
                                    }
                                }
                             */   //remove Above

                                    Log.d(TAG, "Users Count is " + users.size());
                                    int currCount = 0;
                                    for (User user : users) {
                                        try {
                                            currCount++;
                                            Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                                            currTimemillisec = System.currentTimeMillis();

                                            //Log.d(TAG,"Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);

                                            j = 1;
                                            //Log.d(TAG, "doInBackground: " + user.toString());
                                            String fingarprint1 = user.getTemplate1();
                                            byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);
                                            Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                                            currTimemillisec = System.currentTimeMillis();
                                            //Log.d(TAG,"decode Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);
                                            //Each Match Taking 66 milliseconds
                                            rc = mVCOM.Match(tmpl, mTmplCapture1);
                                            Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                                            currTimemillisec = System.currentTimeMillis();
                                            Log.d(TAG, "user Count is " + currCount + " after match Curr Milli sec " + currTimemillisec + "  diff  " + Timeinmillisec);

                                            int score1 = mVCOM.GetMatchScore();

                                            Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                                            currTimemillisec = System.currentTimeMillis();
                                            //Log.d(TAG,"after getscore 2Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);

                                            int score2 = 0, score3 = 0;
                                            //if (score1 < 50000)
                                            {
                                                j++;
                                                //String fingarprint2 = user.getTemplate2();
                                                fingarprint1 = user.getTemplate2();
                                                //byte[] mTmplCapture2 = Base64.decode(fingarprint1, Base64.DEFAULT);
                                                mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                                rc = mVCOM.Match(tmpl, mTmplCapture1);
                                                score2 = mVCOM.GetMatchScore();

                                                Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                                                currTimemillisec = System.currentTimeMillis();
                                                //    Log.d(TAG,"3Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);
                                                //if (score2 < 50000)
                                                {
                                                    j++;
                                                    // String fingarprint3 = user.getTemplate3();
                                                    fingarprint1 = user.getTemplate3();
                                                    //byte[] mTmplCapture3 = Base64.decode(fingarprint1, Base64.DEFAULT);
                                                    mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                                    rc = mVCOM.Match(tmpl, mTmplCapture1);
                                                    score3 = mVCOM.GetMatchScore();

                                                    Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                                                    currTimemillisec = System.currentTimeMillis();

                                                    //      Log.d(TAG,"4Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);
                                                }
                                            }
                                            //Log.d(TAG,"Score 1"+score1+" Score 2"+score2+" Score 3"+score3+" Count is "+j);

                                            Log.d(TAG, "Matching User ID is " + user.getUserId());

                                            if (((score1 + score2 + score3) / j) > GlobalVariables.MaxThresholdVal) {
                                                //Chand Added 13Oct208 for fixing TrueID issue
                                                mVCOM.Close();
                                                mVCOM = null;
                                                //above Chand added freshly
                                                notauthorized = false;
                                                Log.d(TAG, "MainActivity return user CaptureAndMatchTask");
                                                return user;
                                            } else {
                                                Log.d(TAG, "Not Authorized ");
                                                notauthorized = true;
                                            }
                                        } catch (Exception obj) {
                                            Log.d(TAG, "Got Exception while reading fingerprint");
                                            if (mVCOM != null)
                                                mVCOM.Close();
                                            notauthorized = true;
                                        }
                                    }


                                } else {
                                    toastcheck = false;
                                }

                            }
                            /*

                             */
                        } else {
                            toastcheck = false;
                        }
                        //	if (mVCOM !=null)
                        Log.d(TAG, "MainActivity on close VCOM CaptureAndMatchTask ");
                        mVCOM.Close();
                        //mVCOM=null;
                    } else {
                        toastcheck = false;
                        //comeOutofApp=true;
                    }
                }
            } catch (Exception obj) {

                try {
                    Log.d(TAG, "Got Exception while capture and match task" + obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    toastcheck = false;
                    notauthorized = true;
                    // mVCOM = null;
                } catch (Exception obj2) {

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            //  new CaptureAndMatchTask().execute(0);
            inCapture = false;
            inCaptureTest = false;
            fingermoduleCount--;
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            Log.d(TAG, "Got onPostExecute ");

            //notauthorized = false;
            //if (comeOutofApp) {
            /*if ((user == null || notauthorized==false) && comeOutofApp) {
                Log.d(TAG, "Come out App is true onPostExecute returning");
                comeOutofApp=false;

                GoBack();
                return;
            } else*/

            if ((user == null || notauthorized == false) && GlobalVariables.comeOut > 1) {
                Log.d(TAG, "Come out APp is onPostExecute" + GlobalVariables.comeOut);
                GlobalVariables.comeOut++;
                GoBack();
                return;
            } else if ((user == null || notauthorized == false) && comeOutofApp) {
                Log.d(TAG, "Come out App is true onPostExecute returning  " + GlobalVariables.comeOut);
                // comeOutofApp=false;
                //GoBack();
                GlobalVariables.comeOut++;
                return;
            } else if (GlobalVariables.Terminal_Support) {
                if (user != null) {
                    Log.d(TAG, "For Terminal_Start and Stop Successfull User ");

                    //sendLogInfo("Log",user.get_id(),locId,"Log In","Success",location,GlobalVariables.TerminalID);
                    GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                    Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);


                } else {
                    Log.d(TAG, "UnAUthorized Access ");


                    boolean goforEnrollment = false;
/*          if (GlobalVariables.mobileWorkFlowConfigure == true) {

            Log.d(TAG,"Mobileworkflow configure is true ");

            long currTime = System.currentTimeMillis();

            if (lastUnAuthorizedTime < 0)
              Log.d(TAG,"No Send unAUthorized ");
              else
              if (lastUnAuthorizedTime == 0)
              lastUnAuthorizedTime = System.currentTimeMillis();
            else if (lastUnAuthorizedTime > 0) {
              long currsecs = (currTime - lastUnAuthorizedTime) / 1000;
              Log.d(TAG, "Current Secs is " + currsecs);
              if (currsecs < 10) {
                goforEnrollment=true;
                InitiateFingerPrintAdd("126");
              }
            }
          }
*/

                    if (goforEnrollment == false) {
                        Log.d(TAG, "Going for NotAuthorized ");
                        Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                        i.putExtra("fromsupervisor", true);
                        finish();
                        comeOutofApp = true;

                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
            } else if (productVersion == 1 && user != null) {
                Log.d(TAG, "Check the User has Encoded data ");

                //Log.d(TAG,"All Tasks "+allTasks1.get(0).getTagetAes_key().trim);
                Log.d(TAG, "All Tasks  Terminal " + user.getTerminalID());
                Log.d(TAG, "All Tasks  Door " + user.getDoor().trim());
                // Log.d(TAG,"All Tasks  Role "+allTasks.get(0).getTags().get(0));
                int locId = user.getLocationID();

                String location = user.getLocation().trim();
                //  Log.d(TAG,"All Tasks  Terminal "+allTasks.get(0).getTags().get(0).getRoleName().trim());


                // prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);
                if (GlobalVariables.USBExecute == 1)
                    new USBCommands().executeADBCommands(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());
                else
                    new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());

                sendLogInfo("Log", user.get_id(), locId, "Log In", "Success", location, GlobalVariables.TerminalID);


                Intent i = new Intent(getApplicationContext(), OpentDoor.class);

                Log.d(TAG, "TimeAndATtendance is  " + TimeandAttendence);
                   /* if (TimeandAttendence == false ) {
                        i = new Intent(getApplicationContext(), TerminalCheckInActivity.class);
                    }*/

                finish();
                Bundle bundle = new Bundle();
                bundle.putParcelable("User", user);
                i.putExtras(bundle);
                comeOutofApp = true;

                Log.d(TAG, "Come out of APp set true");
                startActivity(i);

            } else if (adminVerificationInitiated) {
                Log.d(TAG, "Admin Verification Initiated onPostExecute " + supervisionEnrolmentStatus + " superVisiorUser " + supervisorUser);
                if ((supervisionEnrolmentStatus == true || supervisorUser == true) && (user != null)) {

                    GlobalVariables.comeOut = 0;
                    adminVerificationInitiated = false;
                    int RoleID = user.getRoleID();
                    String RoleName = user.getRoleName();
                    Log.d(TAG, "SuperVision Enrollment Status is " + supervisionEnrolmentStatus + " User RoleID " + RoleID +
                            "RoleName is " + RoleName);
                    //When User is Administraro or supervisor - start
                    if (RoleID == 3) {

                        //    showImage(3);
                        //   txtView.setText("Admin successfully Authenticated... ");
                        setContentView(R.layout.admin_success);
                        if (ledSupport)
                            StartLedAuthorized();

                        try {
                            usernameDet = findViewById(R.id.username);
                            GlobalVariables.supervisorUserID = Integer.valueOf(user.getUserId());
                            usernameDet.setText(user.getFullname());
                        } catch (Exception obj) {
                            GlobalVariables.supervisorUserID = -1;
                            Log.d(TAG, "Got Exception ");
                        }
                        Log.d(TAG, "SuperVisor UserID is " + GlobalVariables.supervisorUserID);

                        Log.d(TAG, "Successfully It is SupervisionEnrolmentStatus is true Starting Enrollment ");

                        EnrollCount = 0;

                        Log.d(TAG, "Before Play Beep");
                        //playBeep();
                        Log.d(TAG, "AFter Play Beep");


                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "AdminFingerSuccess", user.getUserId(), user.getFullname());

                        Handler handler = new Handler();
                        Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //OutofActivity=true;
                                Intent intent = new Intent(getApplicationContext(), Enroll.class);

                                intent.putExtra("userid", "test");
                                intent.putExtra("fromsupervisor", true);
                                //Intent i1 = new Intent(getApplicationContext(), HomeEnrollActivity.class);
                                // Intent i = new Intent(this, MainActivity.class);
                                startActivity(intent);
                                StopLedAuthorized();
                            }
                        }, 3000);
                    } else {
                        Log.d(TAG, "User authenticated is not supervisor - coming out ");
                        // startActivity(i);
                        check = false;
                        adminVerificationInitiated = false;
                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "AdminFingerFailure", null, "Sucess");
                        Log.d(TAG, "Starting NotAuthorized ");
                        Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                        i.putExtra("fromsupervisor", true);
                        finish();
                        comeOutofApp = true;
                        Log.d(TAG, "Come out of APp set true");
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }

                } else if (supervisionEnrolmentStatus == true) {
                    Log.d(TAG, "User authenticated is not authorized ");
                    // startActivity(i);
                    check = false;
                    adminVerificationInitiated = false;
                    new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "AdminFingerFailure", null, "Sucess");
                    Log.d(TAG, "Starting NotAuthorized ");
                    Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                    i.putExtra("fromsupervisor", true);
                    comeOutofApp = true;

                    GlobalVariables.comeOut = 0;
                    Log.d(TAG, "Come out of APp set true");
                    finish();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } else if (notauthorized) {
                    Log.d(TAG, "SuperUser Got User Not Authorized ");
                    check = false;
                    adminVerificationInitiated = false;
                    Log.d(TAG, "Starting NotAuthorized ");
                    try {

                        if (GlobalVariables.USBExecute == 1)
                            new USBCommands().executeADBCommands(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        else
                            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        //    WebUtils.("fingerfailure","101","fingerfailure");
                    } catch (Exception obj) {
                        Log.d(TAG, "It is exception");
                    }


                    boolean goforEnrollment = false;
                    if (selectedProduct == 1 && productVersion == 0 && GlobalVariables.mobileWorkFlowConfigure == true)

                    //
                    {

                        Log.d(TAG, "Mobileworkflow configure is true ");

                        long currTime = System.currentTimeMillis();

                        if (lastUnAuthorizedTime < 0)
                            Log.d(TAG, "No Send unAUthorized ");
                        else if (lastUnAuthorizedTime == 0)
                            lastUnAuthorizedTime = System.currentTimeMillis();
                        else if (lastUnAuthorizedTime > 0) {
                            long currsecs = (currTime - lastUnAuthorizedTime) / 1000;
                            Log.d(TAG, "Current Secs is " + currsecs);
                            if (currsecs < 20) {
                                goforEnrollment = true;
                                InitiateFingerPrintAdd("105");
                            }
                        }
                    }

                    if (goforEnrollment == false) {
                        Log.d(TAG, "It is unauthorized not going for enrollment ");
                        Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                        i.putExtra("fromsupervisor", true);
                        finish();

                        GlobalVariables.comeOut = 0;
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        comeOutofApp = true;

                        Log.d(TAG, "Come out of APp set true");
                        startActivity(i);
                    } else
                        Log.d(TAG, "Going for enrollment ");
                } else {
                    Log.d(TAG, "Timeout adminverificationInitiated");

                    Handler handler = new Handler();
                    //                 Log.d(TAG,"Starting CaptureNad match Task going for sleep");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (comeOutofApp) {
                                Log.d(TAG, "admin verification Coming out of PostExecute 2");
                                GlobalVariables.comeOut++;
                                GoBack();
                                return;
                            }
                            Log.d(TAG, "admin verificationCalling CaptureAndMatchTask and Match Task ");
                            new CaptureAndMatchTask().execute(0);
                        }
                    }, 1000);

                }
            } else if (user != null) {
                Log.d(TAG, "Processing the Success Message ");
                lastUnAuthorizedTime = -1; //System.currentTimeMillis();

                Intent intent = new Intent(getApplicationContext(), FaceSuccessActivity.class);//SuccessActivity
                if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3)
                    intent = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);

                intent.putExtra("case", "success");
                //    intent.putExtra("FromWeb", true);

                intent.putExtra("personName", user.getUsername());

                String personID = user.getUserId();

                Log.d(TAG, "Add User " + user.getUsername());


                Bundle bundle = new Bundle();
                bundle.putParcelable("User", user);
                intent.putExtras(bundle);


                if (personID != null)
                    intent.putExtra("personID", personID);
                Log.d(TAG, "Check IN status is " + personID);
                //Log.d(TAG,"Person name is "+currState);
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */

                startActivity(intent);
            } else if (notauthorized) {
                Log.d(TAG, "Got User Not Authorized new");
                //   Toast.makeText(Main2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();

                /*Handler handler=new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {*/
                try {

                    //  if (mVCOM != null)
                    //     mVCOM.Close();

                    check = false;
                    try {

                        if (GlobalVariables.USBExecute == 1)
                            new USBCommands().executeADBCommands(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        else
                            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        //    WebUtils.("fingerfailure","101","fingerfailure");
                    } catch (Exception obj) {
                        Log.d(TAG, "It is exception");
                    }
                    GlobalVariables.comeOut = 0;


                    boolean goforEnrollment = false;

                    //Allow for forst and Second Version
                    if (selectedProduct == 1 && productVersion == 0 && GlobalVariables.mobileWorkFlowConfigure == true) {
                        //      {

                        //   if (GlobalVariables.mobileWorkFlowConfigure == true) {

                        Log.d(TAG, "Mobileworkflow configure is true " + lastUnAuthorizedTime);

                        long currTime = System.currentTimeMillis();

                        if (lastUnAuthorizedTime < 0)
                            Log.d(TAG, "No Send unAUthorized ");
                        else if (lastUnAuthorizedTime == 0)
                            lastUnAuthorizedTime = System.currentTimeMillis();
                        else if (lastUnAuthorizedTime > 0) {
                            long currsecs = (currTime - lastUnAuthorizedTime) / 1000;
                            Log.d(TAG, "Current Secs is " + currsecs);
                            if (currsecs < 20) {
                                goforEnrollment = true;
                                InitiateFingerPrintAdd("105");
                            }
                        }
                    }

                    if (goforEnrollment == false) {

                        Log.d(TAG, "Starting NotAuthorized ");
                        Intent i = new Intent(Main2Activity.this, NotAuthorizedActivity.class);
                        finish();
                        comeOutofApp = true;

                        Log.d(TAG, "Come out of APp set true");
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("fromsupervisor", false);
                        startActivity(i);
                    } else
                        Log.d(TAG, "GoforEnrollment is true");
                } catch (Exception ojb) {
                    Log.d(TAG, "Got exception ");
                }
                  /*  }
                },2000);*/


            } /*else if (GlobalVariables.supervisorUser) {
                //if (fromSuperVisior) {
                Log.d(TAG, "Going for Supervision Identification ");
                Log.d(TAG,"Enrolment Status RoleID "+user.getRoleID()+" RoleName"+user.getRoleName());
                Log.d(TAG,"It is SupervisionEnrolmentStatus is true Starting Enrollment ");

                Intent i = new Intent(getApplicationContext(), SupervisiorIdentity2Activity.class);
                startActivity(i);
                finish();
            }*/ else if ((supervisionEnrolmentStatus == true) && (user != null)) {

                //  Log.d(TAG,"SuperVision Enrolment Status "+user.getRoleID()+"  "+user.getRoleName());
                //    sendLogInfo("Message",user.get_id(),"Test","Admin Verification","Success");
                Log.d(TAG, "Enrolment Status RoleID " + user.getRoleID() + " RoleName" + user.getRoleName());
                Log.d(TAG, "It is SupervisionEnrolmentStatus is true Starting Enrollment ");
                //Intent i = new Intent(this, Enroll.class);
                Intent i1 = new Intent(getApplicationContext(), HomeEnrollActivity.class);

                // Intent i = new Intent(this, MainActivity.class);
                startActivity(i1);

                GlobalVariables.comeOut = 0;
                // startActivity(i);
                //TODO implement
            } else if (user != null) {


                GlobalVariables.comeOut = 0;
                Log.d(TAG, "Enrolment Status RoleID " + user.getRoleID() + " RoleName" + user.getRoleName());
                Main2Activity.this.user = user;
                // prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                String URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id();
                int TerminalId = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                if (TerminalId != -1) {
                    URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id() + "/" + TerminalId;
                }
                Log.d(TAG, "Get Assigned Tasks URL is " + URL);
                //Log.d(TAG,"Main Activity onPostExecute "+user.getT) ;
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                // Log.d(TAG, "Before Please wiat" + GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id());
                try {
                    //Chand Commented - only with Single call Open the door, so keep aes,hask keys in get users itself
                    //MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                    //new VolleyService(Main2Activity.this, Main2Activity.this).tokenBaseB(GET, URL, null, "105");

                    Log.d(TAG, "Check the User has Encoded data ");

                    //Log.d(TAG,"All Tasks "+allTasks1.get(0).getTagetAes_key().trim);
                    Log.d(TAG, "All Tasks  Terminal " + user.getTerminalID());
                    Log.d(TAG, "All Tasks  Door " + user.getDoor().trim());
                    // Log.d(TAG,"All Tasks  Role "+allTasks.get(0).getTags().get(0));
                    int locId = user.getLocationID();

                    String location = user.getLocation().trim();
                    //  Log.d(TAG,"All Tasks  Terminal "+allTasks.get(0).getTags().get(0).getRoleName().trim());


                    // prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                    GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                    Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);
                    if (GlobalVariables.USBExecute == 1)
                        new USBCommands().executeADBCommands(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());
                    else
                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());

                    sendLogInfo("Log", user.get_id(), locId, "Log In", "Success", location, GlobalVariables.TerminalID);


                    Intent i = new Intent(getApplicationContext(), OpentDoor.class);

                    Log.d(TAG, "TimeAndATtendance is  " + TimeandAttendence);
                   /* if (TimeandAttendence == false ) {
                        i = new Intent(getApplicationContext(), TerminalCheckInActivity.class);
                    }*/

                    finish();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("User", user);
                    i.putExtras(bundle);
                    comeOutofApp = true;

                    Log.d(TAG, "Come out of APp set true");
                    startActivity(i);

                } catch (Exception obj) {
                    Log.d(TAG, "MyProgressDialog got exception obj - SO Restarting " + obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    finishAffinity();
                    System.exit(0);
                    return;
                }


                Log.d(TAG, "Before Bitmap");
                mBitmapMatch = GetBitmap();
                Log.d(TAG, "AFter Bitmap ");


            } else {
                Log.d(TAG, "onPost Execute not valid User " + check + "  " + toastcheck);
                if (check) {

                    if (toastcheck) {
                        //           imageView.setImageResource(R.drawable.finger_notvalid);

                        // textView.setText("Your fingerprint could not be read or User does not exist");
                        //    settings.setVisibility(View.GONE);
                        //    enroll.setVisibility(View.GONE);

                        // rescan.setVisibility(View.VISIBLE);
                        // help.setVisibility(View.VISIBLE);
                        //   check=false;
                        Log.d(TAG, "CHECK flase ");
                        Log.d(TAG, "CHECK flase ");
                        //Toast.makeText(Main2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();
                    } else {


                        // Toast.makeText(Main2Activity.this, "Place your finger on the sensor", Toast.LENGTH_SHORT).show();
                    }
                    if (comeOutofApp) {
                        Log.d(TAG, "Coming out of PostExecute 1");
                        GlobalVariables.comeOut++;
                        GoBack();
                        return;
                    }
                    Handler handler = new Handler();
                    Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (comeOutofApp) {
                                Log.d(TAG, "Coming out of PostExecute 2");
                                GlobalVariables.comeOut++;
                                GoBack();
                                return;
                            }
                            Log.d(TAG, "Calling CaptureAndMatchTask and Match Task ");
                            new CaptureAndMatchTask().execute(0);
                        }
                    }, 1000);

                } else {
                    Log.d(TAG, "CHECK is false, come out restart App ");
                  /*  Intent i = new Intent(Main2Activity.this, Main2Activity.class);
                    finishAffinity();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);*/
                }
            }
        }
    }

    private class DatabaseCaptureAndMatchTask extends AsyncTask<Integer, Integer, User> {

        @Override
        protected User doInBackground(Integer... i) {
            int j = 1;
            try {
                fingermoduleCount++;
                inCapture = true;
                inCaptureTest = true;
                if (check) {

                    int rc = 10;
                    Log.d(TAG, "in DatabaseCaptureAndMatchTask");
                    if (mVCOM != null)
                        rc = mVCOM.Open();

                    if (rc == 0) {

                        // rc = mVCOM.Capture();

                        Log.d(TAG, "DatabaseCaptureAndMatchTask Databse Support is enabled - Waiting for the VerifyUserRecord");
                        long timeVal = System.currentTimeMillis();
                        int retVal = mVCOM.VerifyUserRecordDatabase(6);
                        long diff = System.currentTimeMillis() - timeVal;
                        Log.d(TAG, "Difference time is " + diff);
                        if (retVal < 0) {

                            //playBeep();
                            notauthorized = false;
                            Log.d(TAG, "Timeout ");
                        } else if (retVal > 0) {
                            Log.d(TAG, "Successful User ID is " + retVal);

                            //playBeep();
                            User user1 = appDatabase.userDao().countUsersBasedonUserID(String.valueOf(retVal));

                            Log.d(TAG, "User Recvd " + user1.getUserId());
                            //  Log.d(TAG, "notifySuccess: " + user.toString());

                            //	if (mVCOM !=null)
                            Log.d(TAG, "MainActivity on close VCOM CaptureAndMatchTask ");
                            mVCOM.Close();
                            mVCOM=null;
                            notauthorized = false;
                            return user1;
                        } else {
                            Log.d(TAG, "Not successful ID is " + retVal);
                            //mVCOM.Close();

                            //playBeep();
                            notauthorized = true;
                        }
                        //mVCOM=null;
                    } else {
                        toastcheck = false;
                        //comeOutofApp=true;
                    }
                    //Chand closed
                    mVCOM.Close();
                    //mVCOM=null;
                }
            } catch (Exception obj) {

                try {
                    Log.d(TAG, "Got Exception while capture and match task" + obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    toastcheck = false;
                    notauthorized = true;
                    // mVCOM = null;
                } catch (Exception obj2) {

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            //  new CaptureAndMatchTask().execute(0);
            inCapture = false;
            inCaptureTest = false;
            fingermoduleCount--;
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            Log.d(TAG, "Got onPostExecute ");

            //notauthorized = false;
            //if (comeOutofApp) {
            /*if ((user == null || notauthorized==false) && comeOutofApp) {
                Log.d(TAG, "Come out App is true onPostExecute returning");
                comeOutofApp=false;

                GoBack();
                return;
            } else*/

            if ((user == null || notauthorized == false) && GlobalVariables.comeOut > 1) {
                Log.d(TAG, "Come out APp is onPostExecute" + GlobalVariables.comeOut);
                GlobalVariables.comeOut++;
                GoBack();
                return;
            } else if ((user == null || notauthorized == false) && comeOutofApp) {
                Log.d(TAG, "Come out App is true onPostExecute returning  " + GlobalVariables.comeOut);
                // comeOutofApp=false;
                //GoBack();
                GlobalVariables.comeOut++;
                return;
            } else if (GlobalVariables.Terminal_Support) {
                if (user != null) {
                    Log.d(TAG, "For Terminal_Start and Stop Successfull User ");

                    //sendLogInfo("Log",user.get_id(),locId,"Log In","Success",location,GlobalVariables.TerminalID);
                    GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                    Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);


                } else {
                    Log.d(TAG, "UnAUthorized Access ");


                    boolean goforEnrollment = false;
/*          if (GlobalVariables.mobileWorkFlowConfigure == true) {

            Log.d(TAG,"Mobileworkflow configure is true ");

            long currTime = System.currentTimeMillis();

            if (lastUnAuthorizedTime < 0)
              Log.d(TAG,"No Send unAUthorized ");
              else
              if (lastUnAuthorizedTime == 0)
              lastUnAuthorizedTime = System.currentTimeMillis();
            else if (lastUnAuthorizedTime > 0) {
              long currsecs = (currTime - lastUnAuthorizedTime) / 1000;
              Log.d(TAG, "Current Secs is " + currsecs);
              if (currsecs < 10) {
                goforEnrollment=true;
                InitiateFingerPrintAdd("126");
              }
            }
          }
*/

                    if (goforEnrollment == false) {
                        Log.d(TAG, "Going for NotAuthorized ");
                        Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                        i.putExtra("fromsupervisor", true);
                        finish();
                        comeOutofApp = true;

                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
            } else if (productVersion == 1 && user != null) {
                Log.d(TAG, "Check the User has Encoded data ");

                //Log.d(TAG,"All Tasks "+allTasks1.get(0).getTagetAes_key().trim);
                Log.d(TAG, "All Tasks  Terminal " + user.getTerminalID());
                Log.d(TAG, "All Tasks  Door " + user.getDoor().trim());
                // Log.d(TAG,"All Tasks  Role "+allTasks.get(0).getTags().get(0));
                int locId = user.getLocationID();

                String location = user.getLocation().trim();
                //  Log.d(TAG,"All Tasks  Terminal "+allTasks.get(0).getTags().get(0).getRoleName().trim());


                // prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);
                if (GlobalVariables.USBExecute == 1)
                    new USBCommands().executeADBCommands(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());
                else
                    new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());

                sendLogInfo("Log", user.get_id(), locId, "Log In", "Success", location, GlobalVariables.TerminalID);


                Intent i = new Intent(getApplicationContext(), OpentDoor.class);

                Log.d(TAG, "TimeAndATtendance is  " + TimeandAttendence);
                   /* if (TimeandAttendence == false ) {
                        i = new Intent(getApplicationContext(), TerminalCheckInActivity.class);
                    }*/

                finish();
                Bundle bundle = new Bundle();
                bundle.putParcelable("User", user);
                i.putExtras(bundle);
                comeOutofApp = true;

                Log.d(TAG, "Come out of APp set true");
                startActivity(i);

            } else if (adminVerificationInitiated) {
                Log.d(TAG, "Admin Verification Initiated onPostExecute " + supervisionEnrolmentStatus + " superVisiorUser " + supervisorUser);
                if ((supervisionEnrolmentStatus == true || supervisorUser == true) && (user != null)) {

                    GlobalVariables.comeOut = 0;
                    adminVerificationInitiated = false;
                    int RoleID = user.getRoleID();
                    String RoleName = user.getRoleName();
                    Log.d(TAG, "SuperVision Enrollment Status is " + supervisionEnrolmentStatus + " User RoleID " + RoleID +
                            "RoleName is " + RoleName);
                    //When User is Administraro or supervisor - start
                    if (RoleID == 3) {

                        //    showImage(3);
                        //   txtView.setText("Admin successfully Authenticated... ");
                        setContentView(R.layout.admin_success);
                        if (ledSupport)
                            StartLedAuthorized();

                        try {
                            usernameDet = findViewById(R.id.username);
                            GlobalVariables.supervisorUserID = Integer.valueOf(user.getUserId());
                            usernameDet.setText(user.getFullname());
                        } catch (Exception obj) {
                            GlobalVariables.supervisorUserID = -1;
                            Log.d(TAG, "Got Exception ");
                        }
                        Log.d(TAG, "SuperVisor UserID is " + GlobalVariables.supervisorUserID);

                        Log.d(TAG, "Successfully It is SupervisionEnrolmentStatus is true Starting Enrollment ");

                        EnrollCount = 0;

                        Log.d(TAG, "Before Play Beep");
                        //playBeep();
                        Log.d(TAG, "AFter Play Beep");


                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "AdminFingerSuccess", user.getUserId(), user.getFullname());

                        Handler handler = new Handler();
                        Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //OutofActivity=true;
                                Intent intent = new Intent(getApplicationContext(), Enroll.class);

                                intent.putExtra("userid", "test");
                                intent.putExtra("fromsupervisor", true);
                                //Intent i1 = new Intent(getApplicationContext(), HomeEnrollActivity.class);
                                // Intent i = new Intent(this, MainActivity.class);
                                startActivity(intent);
                                StopLedAuthorized();
                            }
                        }, 3000);
                    } else {
                        Log.d(TAG, "User authenticated is not supervisor - coming out ");
                        // startActivity(i);
                        check = false;
                        adminVerificationInitiated = false;
                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "AdminFingerFailure", null, "Sucess");
                        Log.d(TAG, "Starting NotAuthorized ");
                        Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                        i.putExtra("fromsupervisor", true);
                        finish();
                        comeOutofApp = true;
                        Log.d(TAG, "Come out of APp set true");
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }

                } else if (supervisionEnrolmentStatus == true) {
                    Log.d(TAG, "User authenticated is not authorized ");
                    // startActivity(i);
                    check = false;
                    adminVerificationInitiated = false;
                    new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "AdminFingerFailure", null, "Sucess");
                    Log.d(TAG, "Starting NotAuthorized ");
                    Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                    i.putExtra("fromsupervisor", true);
                    comeOutofApp = true;

                    GlobalVariables.comeOut = 0;
                    Log.d(TAG, "Come out of APp set true");
                    finish();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } else if (notauthorized) {
                    Log.d(TAG, "SuperUser Got User Not Authorized ");
                    check = false;
                    adminVerificationInitiated = false;
                    Log.d(TAG, "Starting NotAuthorized ");
                    try {

                        if (GlobalVariables.USBExecute == 1)
                            new USBCommands().executeADBCommands(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        else
                            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        //    WebUtils.("fingerfailure","101","fingerfailure");
                    } catch (Exception obj) {
                        Log.d(TAG, "It is exception");
                    }


                    boolean goforEnrollment = false;
                    if (selectedProduct == 1 && productVersion == 0 && GlobalVariables.mobileWorkFlowConfigure == true)

                    //
                    {

                        Log.d(TAG, "Mobileworkflow configure is true ");

                        long currTime = System.currentTimeMillis();

                        if (lastUnAuthorizedTime < 0)
                            Log.d(TAG, "No Send unAUthorized ");
                        else if (lastUnAuthorizedTime == 0)
                            lastUnAuthorizedTime = System.currentTimeMillis();
                        else if (lastUnAuthorizedTime > 0) {
                            long currsecs = (currTime - lastUnAuthorizedTime) / 1000;
                            Log.d(TAG, "Current Secs is " + currsecs);
                            if (currsecs < 20) {
                                goforEnrollment = true;
                                InitiateFingerPrintAdd("105");
                            }
                        }
                    }

                    if (goforEnrollment == false) {
                        Log.d(TAG, "It is unauthorized not going for enrollment ");
                        Intent i = new Intent(getApplicationContext(), NotAuthorizedActivity.class);
                        i.putExtra("fromsupervisor", true);
                        finish();

                        GlobalVariables.comeOut = 0;
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        comeOutofApp = true;

                        Log.d(TAG, "Come out of APp set true");
                        startActivity(i);
                    } else
                        Log.d(TAG, "Going for enrollment ");
                } else {
                    Log.d(TAG, "Timeout adminverificationInitiated");

                    Handler handler = new Handler();
                    //                 Log.d(TAG,"Starting CaptureNad match Task going for sleep");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (comeOutofApp) {
                                Log.d(TAG, "admin verification Coming out of PostExecute 2");
                                GlobalVariables.comeOut++;
                                GoBack();
                                return;
                            }
                            Log.d(TAG, "admin verificationCalling CaptureAndMatchTask and Match Task ");
                            new DatabaseCaptureAndMatchTask().execute(0);
                        }
                    }, 1000);

                }
            } else if (user != null) {
                Log.d(TAG, "Processing the Success Message ");
                lastUnAuthorizedTime = -1; //System.currentTimeMillis();

                Intent intent = new Intent(getApplicationContext(), FaceSuccessActivity.class);//SuccessActivity
                if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3)
                    intent = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);

                intent.putExtra("case", "success");
                //    intent.putExtra("FromWeb", true);

                intent.putExtra("personName", user.getUsername());

                String personID = user.getUserId();

                Log.d(TAG, "Add User " + user.getUsername());


                Bundle bundle = new Bundle();
                bundle.putParcelable("User", user);
                intent.putExtras(bundle);


                if (personID != null)
                    intent.putExtra("personID", personID);
                Log.d(TAG, "Check IN status is " + personID);
                //Log.d(TAG,"Person name is "+currState);
				/*Bundle mBundle = new Bundle();
				mBundle.putString("case","success");
				mBundle.putBoolean("FromWeb",true);

				intent.putExtras(mBundle); */

                startActivity(intent);
            } else if (notauthorized) {
                Log.d(TAG, "Got User Not Authorized new");
                //   Toast.makeText(Main2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();

                /*Handler handler=new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {*/
                try {

                    //  if (mVCOM != null)
                    //     mVCOM.Close();

                    check = false;
                    try {

                        if (GlobalVariables.USBExecute == 1)
                            new USBCommands().executeADBCommands(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        else
                            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingerfailure", null, "fingerfailure");
                        //    WebUtils.("fingerfailure","101","fingerfailure");
                    } catch (Exception obj) {
                        Log.d(TAG, "It is exception");
                    }
                    GlobalVariables.comeOut = 0;


                    boolean goforEnrollment = false;

                    //Allow for forst and Second Version
                    if (selectedProduct == 1 && productVersion == 0 && GlobalVariables.mobileWorkFlowConfigure == true) {
                        //      {

                        //   if (GlobalVariables.mobileWorkFlowConfigure == true) {

                        Log.d(TAG, "Mobileworkflow configure is true " + lastUnAuthorizedTime);

                        long currTime = System.currentTimeMillis();

                        if (lastUnAuthorizedTime < 0)
                            Log.d(TAG, "No Send unAUthorized ");
                        else if (lastUnAuthorizedTime == 0)
                            lastUnAuthorizedTime = System.currentTimeMillis();
                        else if (lastUnAuthorizedTime > 0) {
                            long currsecs = (currTime - lastUnAuthorizedTime) / 1000;
                            Log.d(TAG, "Current Secs is " + currsecs);
                            if (currsecs < 20) {
                                goforEnrollment = true;
                                InitiateFingerPrintAdd("105");
                            }
                        }
                    }

                    if (goforEnrollment == false) {

                        Log.d(TAG, "Starting NotAuthorized ");
                        Intent i = new Intent(Main2Activity.this, NotAuthorizedActivity.class);
                        finish();
                        comeOutofApp = true;

                        Log.d(TAG, "Come out of APp set true");
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("fromsupervisor", false);
                        startActivity(i);
                    } else
                        Log.d(TAG, "GoforEnrollment is true");
                } catch (Exception ojb) {
                    Log.d(TAG, "Got exception ");
                }
                  /*  }
                },2000);*/


            } /*else if (GlobalVariables.supervisorUser) {
                //if (fromSuperVisior) {
                Log.d(TAG, "Going for Supervision Identification ");
                Log.d(TAG,"Enrolment Status RoleID "+user.getRoleID()+" RoleName"+user.getRoleName());
                Log.d(TAG,"It is SupervisionEnrolmentStatus is true Starting Enrollment ");

                Intent i = new Intent(getApplicationContext(), SupervisiorIdentity2Activity.class);
                startActivity(i);
                finish();
            }*/ else if ((supervisionEnrolmentStatus == true) && (user != null)) {

                //  Log.d(TAG,"SuperVision Enrolment Status "+user.getRoleID()+"  "+user.getRoleName());
                //    sendLogInfo("Message",user.get_id(),"Test","Admin Verification","Success");
                Log.d(TAG, "Enrolment Status RoleID " + user.getRoleID() + " RoleName" + user.getRoleName());
                Log.d(TAG, "It is SupervisionEnrolmentStatus is true Starting Enrollment ");
                //Intent i = new Intent(this, Enroll.class);
                Intent i1 = new Intent(getApplicationContext(), HomeEnrollActivity.class);

                // Intent i = new Intent(this, MainActivity.class);
                startActivity(i1);

                GlobalVariables.comeOut = 0;
                // startActivity(i);
                //TODO implement
            } else if (user != null) {


                GlobalVariables.comeOut = 0;
                Log.d(TAG, "Enrolment Status RoleID " + user.getRoleID() + " RoleName" + user.getRoleName());
                Main2Activity.this.user = user;
                // prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                String URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id();
                int TerminalId = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                if (TerminalId != -1) {
                    URL = GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id() + "/" + TerminalId;
                }
                Log.d(TAG, "Get Assigned Tasks URL is " + URL);
                //Log.d(TAG,"Main Activity onPostExecute "+user.getT) ;
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                // Log.d(TAG, "Before Please wiat" + GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id());
                try {
                    //Chand Commented - only with Single call Open the door, so keep aes,hask keys in get users itself
                    //MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                    //new VolleyService(Main2Activity.this, Main2Activity.this).tokenBaseB(GET, URL, null, "105");

                    Log.d(TAG, "Check the User has Encoded data ");

                    //Log.d(TAG,"All Tasks "+allTasks1.get(0).getTagetAes_key().trim);
                    Log.d(TAG, "All Tasks  Terminal " + user.getTerminalID());
                    Log.d(TAG, "All Tasks  Door " + user.getDoor().trim());
                    // Log.d(TAG,"All Tasks  Role "+allTasks.get(0).getTags().get(0));
                    int locId = user.getLocationID();

                    String location = user.getLocation().trim();
                    //  Log.d(TAG,"All Tasks  Terminal "+allTasks.get(0).geftTags().get(0).getRoleName().trim());


                    // prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                    GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);
                    Log.d(TAG, " Device Terminal ID is " + GlobalVariables.TerminalID);
                    if (GlobalVariables.USBExecute == 1)
                        new USBCommands().executeADBCommands(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());
                    else
                        new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "fingersuccess", user.getUserId(), user.getFullname());

                    sendLogInfo("Log", user.get_id(), locId, "Log In", "Success", location, GlobalVariables.TerminalID);


                    Intent i = new Intent(getApplicationContext(), OpentDoor.class);

                    Log.d(TAG, "TimeAndATtendance is  " + TimeandAttendence);
                   /* if (TimeandAttendence == false ) {
                        i = new Intent(getApplicationContext(), TerminalCheckInActivity.class);
                    }*/

                    finish();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("User", user);
                    i.putExtras(bundle);
                    comeOutofApp = true;

                    Log.d(TAG, "Come out of APp set true");
                    startActivity(i);

                } catch (Exception obj) {
                    Log.d(TAG, "MyProgressDialog got exception obj - SO Restarting " + obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    finishAffinity();
                    System.exit(0);
                    return;
                }


                Log.d(TAG, "Before Bitmap");
                mBitmapMatch = GetBitmap();
                Log.d(TAG, "AFter Bitmap ");


            } else {
                Log.d(TAG, "onPost Execute not valid User " + check + "  " + toastcheck);
                if (check) {

                    if (toastcheck) {
                        //           imageView.setImageResource(R.drawable.finger_notvalid);

                        // textView.setText("Your fingerprint could not be read or User does not exist");
                        //    settings.setVisibility(View.GONE);
                        //    enroll.setVisibility(View.GONE);

                        // rescan.setVisibility(View.VISIBLE);
                        // help.setVisibility(View.VISIBLE);
                        //   check=false;
                        Log.d(TAG, "CHECK flase ");
                        Log.d(TAG, "CHECK flase ");
                        //Toast.makeText(Main2Activity.this, "User Not Authorized", Toast.LENGTH_SHORT).show();
                    } else {


                        // Toast.makeText(Main2Activity.this, "Place your finger on the sensor", Toast.LENGTH_SHORT).show();
                    }
                    if (comeOutofApp) {
                        Log.d(TAG, "Coming out of PostExecute 1");
                        GlobalVariables.comeOut++;
                        GoBack();
                        return;
                    }
                    Handler handler = new Handler();
                    Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (comeOutofApp) {
                                Log.d(TAG, "Coming out of PostExecute 2");
                                GlobalVariables.comeOut++;
                                GoBack();
                                return;
                            }
                            Log.d(TAG, "Calling CaptureAndMatchTask and Match Task ");
                            new DatabaseCaptureAndMatchTask().execute(0);
                        }
                    }, 1000);

                } else {
                    Log.d(TAG, "CHECK is false, come out restart App ");
                  /*  Intent i = new Intent(Main2Activity.this, Main2Activity.class);
                    finishAffinity();
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);*/
                }
            }
        }
    }

    private Bitmap GetBitmap() {
        try {
            Log.d(TAG, "in Get BitMap");
            int sizeX = mVCOM.GetCompositeImageSizeX();
            int sizeY = mVCOM.GetCompositeImageSizeY();
            if (sizeX > 0 && sizeY > 0) {
                Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
                int[] colors = new int[sizeX * sizeY];
                if (mVCOM.GetCompositeImage(colors) == 0) {
                    bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
                } else {
                    Log.i(TAG, "Unable to get composite image.");
                }
                return bm;
            } else {
                Log.i(TAG, "Composite image is too small.");
                // ...so we create a blank one-pixel bitmap and return it
                Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
                return bm;
            }
        } catch (Exception obj) {
            //Chand aadded
            Log.d(TAG, "GetBitMap is null got exception" + obj.getMessage());

            Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            return bm;

        }
    }

    private byte[] GetTemplate() {
        try {
            //if (mVCOM != null)
            {
                int tmplSize = mVCOM.GetTemplateSize();
                if (tmplSize > 0) {
                    byte[] tmpl = new byte[tmplSize];
                    if (mVCOM.GetTemplate(tmpl) == 0) {
                        return tmpl;
                    } else {
                        Log.i(TAG, "Unable to get template.");
                    }
                } else {
                    Log.i(TAG, "Template is too small.");
                }
                return new byte[0];
            } /*else {
//Chand added
                 // Toast.makeText(ActivityMain.this, "GetTemplate Invalid mCOM return null", Toast.LENGTH_LONG).show();
		  Log.d(TAG,"Invalid mCOM  - return null");
		return null;
	    } */
        } catch (Exception obj) {
            Log.d(TAG, "Got Exception Invalid mCOM  - return null");
            return null;
        }
    }

    private void DrawMinutiae(Bitmap bm, byte[] tmpl) {
        int tmplSize = tmpl.length;
        if (tmplSize < 1)
            return;
        if (bm.getWidth() < 2 || bm.getHeight() < 2)
            return;

        ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
        Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint();
        for (int i = 0; i < minutiaeList.length; i++) {
            if (minutiaeList[i].nType == 1)
                paint.setARGB(0xFF, 0xFF, 0x00, 0x00);    // line-ending
            else
                paint.setARGB(0xFF, 0x00, 0xFF, 0x00);    // bifurcation
            int nX = minutiaeList[i].nX;
            int nY = minutiaeList[i].nY;
            canvas.drawCircle(nX, nY, 4, paint);
            double nR = minutiaeList[i].nRotAngle;
            int nX1;
            int nY1;
            nX1 = (int) (nX + (10.0 * Math.cos(nR)));
            nY1 = (int) (nY - (10.0 * Math.sin(nR)));
            canvas.drawLine(nX, nY, nX1, nY1, paint);
            canvas.drawLine(nX + 1, nY, nX1 + 1, nY1, paint);
            canvas.drawLine(nX - 1, nY, nX1 - 1, nY1, paint);
            canvas.drawLine(nX, nY + 1, nX1, nY1 + 1, paint);
            canvas.drawLine(nX, nY - 1, nX1, nY1 - 1, paint);
        }
    }

    @Override
    public void onStop() {

        super.onStop();
        Log.d(TAG, "onStop Main2Activity Activity Stopping Timer ");

        if (sendDeviceStatusTimer != null)
            sendDeviceStatusTimer.cancel();
        sendDeviceStatusTimer = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainAtivity on Destroy before VCOM CLose");
        Log.d(TAG, "onDestroy inCapture " + inCapture + "  Test" + inCaptureTest);
        if (M21Support == true) {
            try {

 /*               Log.d(TAG,"on Destroy before getBioSDK");
                BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
                if (bAPI != null) {
                    Log.d(TAG,"Release Bio SDK");
                    BioSDKDevice fpDevice = getConnectedDevice();
                    Log.d(TAG, "Wait for Finger Clear ");
                    if (fpDevice == null) {
                        return;
                    }
                    //BioDeviceStatus status = fpDevice.waitForFingerClear_asynch(mTimeOut, this);
                    fpDevice.waitForFingerClear(mTimeOut,this);
                    Log.d(TAG,"Clear Finger ");
                    fpDevice.clearBioData();
                    Log.d(TAG,"After clear Bio Data ");
                    BioSDKFactory.releaseBioSDKAPI();
                } else
                    Log.d(TAG,"BIO SDK is null;");
                BioSDKFactory.releaseBioSDKAPI();
*/
                BioSDKFactory.releaseBioSDKAPI();
            } catch (Exception obj) {

            }

        } else if (inCapture == false) {
            if (mVCOM != null)
                mVCOM.Close();
            else
                Log.d(TAG, "MainAtivity on Destroy before already VCOM CLose");
        } else
            Log.d(TAG, "inCapture is true - so not VCOM CLose ");
        //mVCOM = null;
        //Chand added above
        //  comeOutofApp=true;
        if (sendDeviceStatusTimer != null)
            sendDeviceStatusTimer.cancel();
        sendDeviceStatusTimer = null;


        StopLedAuthorized();
    }

    public void playBeep() {
        MediaPlayer m = new MediaPlayer();
        try {
            if (m.isPlaying()) {
                m.stop();
                m.release();
                m = new MediaPlayer();
            }

            AssetFileDescriptor descriptor = getAssets().openFd("shutter.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            m.prepare();
            m.setVolume(1f, 1f);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startSettings() {

        Log.d(TAG, "Go to the Settings Page ");
        Intent intent = new Intent(getApplicationContext(), FaceSettings.class);

        startActivity(intent);
        GlobalVariables.comeOut = 1;

        //mVCOM.Close();
        try {
            if (mVCOM != null)
                mVCOM.Close();
        } catch (Exception obj) {
            Log.d(TAG,"Got the Exception ");
        }
        mVCOM = null;

        comeOutofApp = true;

        finishAffinity();

    }

    private void startSettingsOld() {

        Log.d(TAG, "In SHow Licence App");
        LayoutInflater inflater = LayoutInflater.from(Main2Activity.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText) subView.findViewById(R.id.dialogEditText1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Android Settings");
        builder.setMessage("Password Verification");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordText = subEditText.getText().toString().trim();
                Log.d(TAG, "Password Text is " + passwordText);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                String settingPwd = getsettingspwd();
                Log.d(TAG, "App Pwd is " + settingPwd);
                if (passwordText.equals("argus@542")) {
                    Log.d(TAG, "Going for Shutdown");
                    //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
                    Shutdown();
                } else if (passwordText.equals("argus@543")) {
                    Log.d(TAG, "Going for Reboot");
                    reboot();
                } else if (passwordText.equals(settingPwd)) {

                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    finish();

                } else //Kept for Testing by Chand - remove it
                    if (TestWithoutUSB(passwordText) == null)
                        Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

                    // finish();
                }
            }
        });


        builder.show();
    }

    private String getsettingspwd() {
        String verVal = null;

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "settingspwd.txt");

            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(
                    fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            verVal = aBuffer.toString().trim();

            Log.d(TAG, "Reading settings pwd from sdfile is " + verVal + " len s " + verVal.length());
            myReader.close();
            if ((verVal != null) && (verVal != " ") && (verVal != "") && verVal.length() > 0)
                return verVal;
        } catch (Exception obj) {
            Log.d(TAG, "Got exception while reading settings pwd");

        }
        return "digit9@123";
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

    private static void Shutdown() {
        Log.d(TAG, "Shutdown the system ");

        try {
            //
            Process proc = Runtime.getRuntime().exec(
                    //		new String[] { "su", "-c", "am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task"});
                    //adb shell su -c 'am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task'
                    new String[]{"su", "-c", "reboot -p"});
            proc.waitFor();
        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), "No rooted device", 3000).show();
            ex.printStackTrace();
        }
    }

    private static void reboot() {
        Log.d(TAG, "Reboot the system ");

        try {
            //
            Process proc = Runtime.getRuntime().exec(
                    //		new String[] { "su", "-c", "am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task"});
                    //adb shell su -c 'am start -a android.intent.action.ACTION_REQUEST_SHUTDOWN --ez KEY_CONFIRM true --activity-clear-task'
                    new String[]{"su", "-c", "reboot "});
            proc.waitFor();
        } catch (Exception ex) {
            //Toast.makeText(getApplicationContext(), "No rooted device", 3000).show();
            ex.printStackTrace();
        }
    }

    private String TestWithoutUSB(String userID) {
        List<User> users = appDatabase.userDao().getAll();
        Log.d(TAG, "testWithoutUSB - UserID is " + userID);

        for (User user : users) {

            Log.d(TAG, "User Strings Vals: " + user.toString());
            Log.d(TAG, "User Full Name Strings Vals: " + user.getFullname());
            Log.d(TAG, "User ID Strings Vals: " + user.get_id());
            Log.d(TAG, "User User ID Strings Vals: " + user.getUserId());
            Log.d(TAG, "User UserName Strings Vals: " + user.getUsername());
            Log.d(TAG, "User UserStatus Strings Vals: " + user.getStatus());

            if (userID.equals(user.getUserId())) {
                Log.d(TAG, "Matched User Get the Assigned Tasks");
                Main2Activity.this.user = user;

                //Chand commented here
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                //      MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                //       new VolleyService(Main2Activity.this, Main2Activity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id(), null, "105");


                mBitmapMatch = GetBitmap();
                return user.get_id();
            }
/*
        String fingarprint1 = user.getTemplate1();
        byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

        String fingarprint2 = user.getTemplate2();
        byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

        String fingarprint3 = user.getTemplate3();
        byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);


        rc = mVCOM.Match(tmpl, mTmplCapture1);
        int score1 = mVCOM.GetMatchScore();
        rc = mVCOM.Match(tmpl, mTmplCapture2);
        int score2 = mVCOM.GetMatchScore();
        rc = mVCOM.Match(tmpl, mTmplCapture3);
        int score3 = mVCOM.GetMatchScore();
        if (((score1 + score2 + score3) / 3) > 50000) {
            return user;
        }
        */

        }
        return null;
    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected() {
        if (isNetworkAvailable()) {
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected() {
        if (isNetworkAvailable()) {
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public String GetDeviceipWiFiData() {

        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        //    @SuppressWarnings("deprecation")
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());


        Log.d(TAG, "GetDeviceWifiData" + ip);
        return ip;

    }

    private String getIpAccess() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        Log.d(TAG, "Before Reading IP Address");
        @SuppressWarnings("deprecation")
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        final String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        final String serverIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (0 & 0xff));
        //return "http://" + formatedIpAddress + ":";
        return serverIpAddress;
    }

    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }

    private void StartUSBService() {

        if (!GlobalVariables.MobileDevice) {
            Log.d(TAG, "It is Start USB Service");
            stopService(new Intent(this,
                    USBService.class));

            startService(new Intent(this,
                    USBService.class));
        } else
            Log.d(TAG, "Not Staring USB Service as it is Mobile device");
    }

    private void executeADBCommands(String action, String state) {

        Process process = null;
        DataOutputStream os = null;

        //If not USB - Come out
        if (GlobalVariables.AppSupport != 2) {
            Log.d(TAG, "It is Not USB COming out");
            return;
        }
        try {
            Log.d(TAG, "Going to Excecute ADB Commands ");
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());


            if (action == null) {
                //os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell\n");
                os.writeBytes("adb -s AINBLJFI4PEQTOI7 pull /mnt/sdcard/status.txt /mnt/mystatus.txt \n");
            } else {
                Log.d(TAG, "ADB state is " + action + "  State is " + state);
                if (state != null) {
                    os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state + "  \n");
                    Log.d(TAG, "It is adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state);
                } else {
                    os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + "  \n");
                    //			Log.d(TAG,"adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action);
                }
            }


            os.writeBytes("exit\n");
            //os.flush();
            process.waitFor();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        } finally {
            try {
                //     if (os != null) {
                //           os.close();
                //   }
                process.destroy();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        // TODO Auto-generated method stub

    }

    private void SaveDeviceStatustoRemoteDevice(String action, String state) {

        Log.d(TAG, "In SaveDeviceStatustoRemoteDevice" + action + "  " + state);
        try {

            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "mydevicestatus.txt");

            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

            if (state != null)
                myOutWriter.append(action + ";" + state);
            else
                myOutWriter.append(action);

            myOutWriter.close();
            fOut.close();
        } catch (Exception obj) {

        }
    }

    private void RemoveRemoteDeviceStatus() {
        String verVal = null;
        String REMOTE_DEVICE_STATUS_FILE = "mydevicestatus.txt";

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + REMOTE_DEVICE_STATUS_FILE);
            //File myFile = new File("/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE); //Both are same

            Log.d(TAG, "File Checking is " + "/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE);

            if (myFile.exists()) {
                myFile.delete();
            }
        } catch (Exception obj) {

        }
    }

    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG, "Got exception " + obj.toString());
        }
        return false;
    }

    //region Start And Stop AndroidWebServer
    private boolean startAndroidWebServer() {
        Log.d("HomeActivity", "in Start Web Server");
        if (!isStarted) {
            int port = getPortFromEditText();
            try {
                if (port == 0) {
                    throw new Exception();
                }
                androidWebServer = new AndroidWebServer(port, this);
                androidWebServer.start();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                //Snackbar.make(coordinatorLayout, "The PORT " + port + " doesn't work, please change it between 1000 and 9999.", Snackbar.LENGTH_LONG).show();
            }
        }
        return false;
    }

    private boolean stopAndroidWebServer() {
        if (isStarted && androidWebServer != null) {
            androidWebServer.stop();
            return true;
        }
        return false;
    }

    private int getPortFromEditText() {
        //String valueEditText = "8080";
        //return (valueEditText.length() > 0) ? Integer.parseInt(valueEditText) : DEFAULT_PORT;
        return DEFAULT_PORT;
    }

    private void sendDeviceStatusInfo() {
        Log.d(TAG, "sendDeviceStatusInfo ");
        try {
            //   String screen_id = screen_Id;
            //  String display_name = screenNamee1;
            String display_type = "Digit9Terminal";  //DigitalSignage,Kisok,InteractiveSignage
            String category = "Finance"; //Hotel, HealthCare, Hospital
            String orientation = "horizontal"; //HoHoritonTal or Vertical
            /*try {
                orientation = getScreenOrientation(this);
            } catch (Exception obj) {
                Log.d(TAG, "Got Exception orientation ");
            }
            Log.d(TAG, "Orientation is " + orientation);
            if (GlobalVariables.orientation == 1) orientation = "Vertical"; //Portrait
            else if (GlobalVariables.orientation == 2)
                orientation = "Horizontal"; //Landscape
*/
            Log.d(TAG, "Orientation is " + orientation);
            String lattitude = "1.3222";
            String longitude = "1.52333";
/*

            if (longitude_g != 0)
                longitude = String.valueOf(longitude_g);
            if (latitude_g != 0)
                lattitude = String.valueOf(latitude_g);
*/

/*
            String area = "Marathahalli";
            String city = "Bangalore";
            String state = "Karnataka";
            String country = "India";
            String zip_code = "560076";
            String age = "5";
            String footfall = "0"; //server side
            String income_group = "high"; //server side
*/

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            String currentDateTime = sdf.format(cal.getTime());
            String currts = null;
            try {
                Log.d(TAG, "Get Time of Day");
                currts = Long.toString(System.currentTimeMillis() / 1000L);
                try {
                    Log.d(TAG, "Get Time of Day");
                    Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                    //Added 4 & half hours to show the Indian time on the Server - Testing - 25/07/2019
                    //Date currentTime2 = curCalendar.getInstance().getTime();
                    // currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                    currts = Long.toString(curCalendar.getTimeInMillis() / 1000L + 16200);

                    String curr1 = Long.toString(System.currentTimeMillis() / 1000L);
                    // jsonObject2.put("logged_in_time_secs", currts);
                    Log.d(TAG, "Current Time stamp is GMT " + currts);
                    Log.d(TAG, "Current Time stamp is " + curr1);

                } catch (Exception obj) {

                }
                Log.d(TAG, "Current Time stamp is " + currts);
            } catch (Exception obj) {

            }

            String dwell_time = "0"; //currentDateTime;  //seconds
            Log.d(TAG, "Current Dwell Time is " + dwell_time);
            String ethnicity = "India";
            String impressions = "8000"; //from reports server should calculate
            String model = Build.MODEL; //"MT955";
            String os = Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
            //"Android";
            String processor = Build.MANUFACTURER; //"MTK500";
            String ram = "1 GB";

            Log.d(TAG, "Total Ram Size is " + totalRamMemorySize() + " Free Memory is " + freeRamMemorySize());
            ram = String.valueOf(totalRamMemorySize()) + " MB";
            String androidVersion = Build.MANUFACTURER
                    + " " + Build.MODEL + " " + Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
            Log.d(TAG, "Android details " + androidVersion);


            String Memory = getTotalExternalMemorySize(); // + " "+getAvailableInternalMemorySize();
            String storage_available = getAvailableExternalMemorySize(); //can check external Storage

            Log.d(TAG, "Total Memory is " + Memory);
            Log.d(TAG, "Available Memory is " + storage_available);
            String storage = Memory;
            ;
            try {
                ActivityManager actManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
                actManager.getMemoryInfo(memInfo);
                long totalMemory = memInfo.totalMem;
                Log.d(TAG, "Total Memory is " + totalMemory);
            } catch (Exception obj) {

            }


            String height = "32"; //display resolution 1024x 720P
            String width = "32";
/*
            if (GlobalVariables.Width != 0)
                width = String.valueOf(GlobalVariables.Width);

            if (GlobalVariables.Height != 0)
                height = String.valueOf(GlobalVariables.Height);
*/

            Log.d(TAG, "WIndow Width is " + width + " Height is " + height);
            String rtc = "true"; //detect
            String wifi = "false";
            String ethernet = "false";
            String threeg = "false";
            String ip = null;
            String localwifiSSID = null;
            long lastConfigupdate = 0;

            if (isWifiConnected()) {
                wifi = "true";
                ip = GetDeviceipWiFiData();
                //   localwifiSSID = wifiSSID;
                Log.d(TAG, "Wifi SSID is " + localwifiSSID);
            } else if (isEthernetConnected()) {
                ethernet = "true";
                ip = logLocalIpAddresses();
                ip = ip.substring(1);
            } else
                threeg = "true";

            Log.d(TAG, "WifiStatus is" + wifi + " Etnethernet status " + ethernet + " 3G " + threeg);
            String hdmi = "true";

            String rooted = "false";
            if (isRooted())
                rooted = "true";
            //String pkt = packet.getAddress().getHostAddress();
            Log.d(TAG, "Rooted Status is " + rooted);
            String date_installed = null; //server


            //String launcher_ver = getLauncherVersion();
            //String hdmiApp = ReadHdmiAppVersion();
            {
                Log.d(TAG,
                        "send Queured Reports");
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                // String currVersion =

                String app_ver = pInfo.versionName;
            /*try {
                if (hdmiApp != null)
                    app_ver = app_ver + "-" + hdmiApp;
            } catch (Exception obj) {

            }*/
                Log.d(TAG, "App Version is " + app_ver);
                String mac = getMacAddr();
                Log.d(TAG, "MAC Address is " + mac);
                //String  ip_addr = "192.168.1.20";
                //	String  ip = logLocalIpAddresses(); // GetDeviceipWiFiData();
                //	String ip = GetDeviceipWiFiData();
                Log.d(TAG, "IP Addr is " + ip);


                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long msTimeSys = System.currentTimeMillis() - SystemClock.elapsedRealtime();
                Date curDateTimeSys = new Date(msTimeSys);

                Log.d(TAG, "sendUpdatedData Sys UpTime: " + formatter.format(curDateTimeSys));
                String curDateSys = formatter.format(curDateTimeSys);
                Log.d(TAG, "SysUpTIme TIme is : " + curDateSys);

                date_installed = currentDateTime; //curDateSys; //- server
                String date_identified = currentDateTime; //it is mandatory for the Server to Generate Reports
                String last_boot = curDateSys;
                String uptime = String.valueOf((SystemClock.elapsedRealtime() / 1000) / 60 * 60);

                String online = "true";
                //String playlist=GetLayoutID();//"none";  //LayoutID #
                //Log.d(TAG,"PlayList is "+playlist);

                //String storage_available=getAvailableInternalMemorySize(); //can check external Storage
                String last_crash_camera = currentDateTime;  //must be not null for server
                String hotspot = "false";  //is hotspot enable
                if (GetWifiApStatus()) {
                    hotspot = "true";
                }
                Log.d(TAG, "Hotspot is " + hotspot);

                String streaming = "false"; //Check Camera Available
          /*  if (CheckCameraStatus() ) {
                streaming = "true";
            }*/
                Log.d(TAG, "Camera Status is " + streaming);
                //com.teamviewer.quicksupport.market
                String teamviewer = "false"; //Check Team Viewer Installed or not

                try {
                    lastConfigupdate = prefs.getLong(GlobalVariables.last_Config_SP, 0);
                    Log.d(TAG, "last Config Update was " + lastConfigupdate);

                } catch (Exception obj) {
                    lastConfigupdate = 0;
                }

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                String URL = GlobalVariables.SSOIPAddress + SSO_SERVER_DEVICE_STATUS;
                Log.d(TAG, "Device Status URL is " + URL);
                JSONObject jsonBody = new JSONObject();
                // jsonBody.put("Title", "Android Volley Demo");
                // jsonBody.put("Author", "BNK");
                //jsonBody.put("screen_id", screen_id);
                //jsonBody.put("display_name", display_name);
                //jsonBody.put("display_type", display_type);
                //jsonBody.put("category", category);
                // jsonBody.put("orientation", orientation);
                jsonBody.put("lattitude", lattitude);
                ;
                jsonBody.put("longitude", longitude);
            /*jsonBody.put("area",area);
            jsonBody.put("city",city);
            jsonBody.put("state",state);
            jsonBody.put("country",country);
            jsonBody.put("zip_code",zip_code);
            jsonBody.put("age",age);
            report.put("footfall",footfall);
            report.put("income_group",income_group);

            report.put("dwell_time",dwell_time);
            report.put("ethnicity",ethnicity);
            report.put("impressions",impressions);
            */
                // jsonBody.put("model", model);
                jsonBody.put("model", mac);
                jsonBody.put("os", os);
                jsonBody.put("processor", processor);
                jsonBody.put("ram", ram);
                jsonBody.put("storage", storage);
/*
            jsonBody.put("height", height);
            jsonBody.put("width",width);
            report.put("rtc",rtc);*/
                jsonBody.put("wifi", wifi);
                // jsonBody.put("_3g", threeg);
                jsonBody.put("ethernet", ethernet);
                jsonBody.put("hdmi", hdmi);
                jsonBody.put("rooted", rooted);
                jsonBody.put("date_installed", date_installed);
                // jsonBody.put("date_identified", date_identified);
                jsonBody.put("date_identified", currts);
                //   jsonBody.put("launcher_ver", launcher_ver);
                jsonBody.put("app_ver", app_ver);
                jsonBody.put("mac", mac);
                jsonBody.put("ip", ip);
                jsonBody.put("last_boot", last_boot);
                jsonBody.put("uptime", uptime);

                if (lastConfigupdate != 0)
                    jsonBody.put("last_config_update", lastConfigupdate);

                final String requestBody = jsonBody.toString();


                Log.d(TAG, "Sendign Device Status " + requestBody + " URL is " + URL);

                new VolleyService(Main2Activity.this, Main2Activity.this).tokenBase(POST, URL, jsonBody, "210");
/*
                StringRequest stringRequest = new StringRequest(POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);


                        String RxTerminalId = null;
                        //Save the Terminal ID of the device;
                        prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                        GlobalVariables.TerminalID = prefs.getString(GlobalVariables.SP_TerminalID, Constants.SERVER_IPADDR);

                        if (RxTerminalId != null && !RxTerminalId.equals( GlobalVariables.TerminalID)) {

                            SharedPreferences.Editor editor1 = prefs.edit();

                            editor1.putString(GlobalVariables.SP_ServerIP, GlobalVariables.TerminalID);
                            Log.d(TAG,"Storing TerminalID "+GlobalVariables.TerminalID);

                            editor1.commit();
                        } else
                            Log.d(TAG,"No Change in TermnalID "+GlobalVariables.SP_TerminalID);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");

                            //og.d(TAG,"Request Body is "+requestBody.toString());

                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }

                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response.statusCode);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }
                };

                requestQueue.add(stringRequest);*/
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private long totalRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.totalMem / 1048576L;
        return availableMegs;
    }

    private long freeRamMemorySize() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem / 1048576L;

        return availableMegs;
    }

    private static boolean externalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    private boolean GetWifiApStatus() {
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        Method[] wmMethods = wifi.getClass().getDeclaredMethods();
        for (Method method : wmMethods) {
            if (method.getName().equals("isWifiApEnabled")) {

                try {
                    return (boolean) method.invoke(wifi);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private String logLocalIpAddresses() {
        Enumeration<NetworkInterface> nwis;
        String ipAddr = null;
        try {
            nwis = NetworkInterface.getNetworkInterfaces();
            while (nwis.hasMoreElements()) {

                NetworkInterface ni = nwis.nextElement();
                for (InterfaceAddress ia : ni.getInterfaceAddresses()) {
                    try {
                        if ((ia.getNetworkPrefixLength() != 64) && (ni.getDisplayName().contains("eth0") || ni.getDisplayName().contains("wlan"))) {

                            ipAddr = ia.getAddress().toString();
                        }
                        Log.i(TAG, String.format("%s: %s/%d",
                                ni.getDisplayName(), ia.getAddress(), ia.getNetworkPrefixLength()));
                    } catch (Exception obj) {

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ipAddr;
    }

    private static String getTotalExternalMemorySize() {
        try {
            if (externalMemoryAvailable()) {
                File path = Environment.
                        getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long BlockSize = stat.getBlockSize();
                long TotalBlocks = stat.getBlockCount();
                return formatSize(TotalBlocks * BlockSize);
            } else {
                return getAvailableInternalMemorySize();
            }
        } catch (Exception obj) {
            return null;
        }
    }

    private static String getAvailableExternalMemorySize() {
        try {
            if (externalMemoryAvailable()) {
                File path = Environment.getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long blockSize = stat.getBlockSize();
                long availableBlocks = stat.getAvailableBlocks();
                return formatSize(availableBlocks * blockSize);
            } else {
                return getTotalInternalMemorySize(); // + " "+getAvailableInternalMemorySize();

            }
        } catch (Exception obj) {
            return null;
        }
    }

    private static String getAvailableInternalMemorySize() {
        Log.d("Display Stats", " in getAvailableInternalMemorySize");
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        Log.d("DisplayStatus", "getAvailableInternalMemorySize lockSize is " + blockSize + " Available Blocks" + availableBlocks);
        return formatSize(availableBlocks * blockSize);
    }

    private static String getTotalInternalMemorySize() {
        Log.d("Display Stats", " in getTotalInternalMemorySize");
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        Log.d("DisplayStatus", "getTotalInternalMemorySize lockSize is " + blockSize + " Total Blocks" + totalBlocks);
        return formatSize(totalBlocks * blockSize);
    }

    private static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        Log.d("DisplayStats", "Format Size is " + resultBuffer.toString());
        return resultBuffer.toString();
    }

    private static boolean isRooted() {
        //  boolean isEmulator = isEmulator(context);
        /*String buildTags = Build.TAGS;*/
    /*if(!isEmulator && buildTags != null && buildTags.contains("test-keys")) {
        return true;
    } else*/
        {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            } else {
                file = new File("/system/xbin/su");
                Log.d(TAG, "isRooted " + file.exists());
                return /*!isEmulator && */file.exists();
            }
        }
    }

    private static String getMacAddr() {
        try {
            Log.d(TAG, "In getMacAddr Changed");
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());

            Log.d(TAG, "Get Mac Address is " + all.size());

            for (NetworkInterface nif : all) {
                Log.d(TAG, "Test check getMacAddr interface " + nif.getName());
            }
            //above for testing
            for (NetworkInterface nif : all) {
                Log.d(TAG, "getMacAddr interface " + nif.getName());
              /*  if (GlobalVariables.MobileDevice) //added ! chand for testing
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                    else
                    if (!nif.getName().equalsIgnoreCase("eth0")) continue;
*/
                if (GlobalVariables.MobileDevice) {

                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                } else {
                    Log.d(TAG, "CHecking the wlan0 and eth0");
                   /* if (!nif.getName().equalsIgnoreCase("wlan0") &&
                            !nif.getName().equalsIgnoreCase("eth0"))  {*/
                    if (!nif.getName().equalsIgnoreCase("wlan0")
                    ) {
                        Log.d(TAG, "It is not wlan or eth0");
                        continue;
                    }
                    //chand added above - for Mobile 11/11/2019

                    /*if (!nif.getName().equalsIgnoreCase("eth0")) {
                        Log.d(TAG, "Not matching with eth0" + nif.getName());
                        continue;
                    }*/
                    else
                        Log.d(TAG, "Matching interface is " + nif.getName());
                }
                if (nif.getName().contains("ip6")) continue;
                Log.d(TAG, "getMacAddr interface processing " + nif.getName());

                byte[] macBytes = nif.getHardwareAddress();
                Log.d(TAG, "Process mac");
                if (macBytes == null) {
                    continue;
                    //  return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG, "Return Mac is " + res1.toString());
                return res1.toString();
                //for testing commented
            }
        } catch (Exception ex) {
            Log.d("NewSplash", "Not able to get mac address");
        }
        String macAddr = getMacAddrEthernet();

        Log.d(TAG, "Got the mac Address " + macAddr);
        return macAddr;
    }


    private static String getMacAddrEthernet() {
        try {
            Log.d(TAG, "In getMacAddr Changed");
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());

            Log.d(TAG, "Get Mac Address is " + all.size());

            for (NetworkInterface nif : all) {
                Log.d(TAG, "Test check getMacAddr interface " + nif.getName());
            }
            //above for testing
            for (NetworkInterface nif : all) {
                Log.d(TAG, "getMacAddr interface " + nif.getName());
              /*  if (GlobalVariables.MobileDevice) //added ! chand for testing
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                    else
                    if (!nif.getName().equalsIgnoreCase("eth0")) continue;
*/
                if (GlobalVariables.MobileDevice) {

                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                } else {
                    Log.d(TAG, "CHecking the wlan0 and eth0");
                   /* if (!nif.getName().equalsIgnoreCase("wlan0") &&
                            !nif.getName().equalsIgnoreCase("eth0"))  {*/
                    //    if (!nif.getName().equalsIgnoreCase("wlan0")
                    if (!nif.getName().equalsIgnoreCase("eth0")
                    ) {
                        Log.d(TAG, "It is not wlan or eth0");
                        continue;
                    }
                    //chand added above - for Mobile 11/11/2019

                    /*if (!nif.getName().equalsIgnoreCase("eth0")) {
                        Log.d(TAG, "Not matching with eth0" + nif.getName());
                        continue;
                    }*/
                    else
                        Log.d(TAG, "Matching interface is " + nif.getName());
                }
                if (nif.getName().contains("ip6")) continue;
                Log.d(TAG, "getMacAddr interface processing " + nif.getName());

                byte[] macBytes = nif.getHardwareAddress();
                Log.d(TAG, "Process mac");
                if (macBytes == null) {
                    continue;
                    //  return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG, "Return Mac is " + res1.toString());
                return res1.toString();
                //for testing commented
            }
        } catch (Exception ex) {
            Log.d("NewSplash", "Not able to get mac address");
        }
        return "02:00:00:00:00:00";
    }

    private String getMacAddrOld() {
        try {
            Log.d(TAG, "In getMacAddr");
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                Log.d(TAG, "getMacAddr interface " + nif.getName());
               /* if (GlobalVariables.MobileDevice)
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                else
                if (!nif.getName().equalsIgnoreCase("eth0")) continue;
*/
                if (GlobalVariables.MobileDevice) {
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                } else {
                    if (!nif.getName().equalsIgnoreCase("eth0")) {
                        Log.d(TAG, "Not matching with eth0" + nif.getName());
                        continue;
                    }
                }
                if (nif.getName().contains("ip6")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                Log.d(TAG, "Process mac");
                if (macBytes == null) {
                    continue;
                    //  return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG, "Return Mac is " + res1.toString());
                return res1.toString();
            }
        } catch (Exception ex) {
            Log.d("NewSplash", "Not able to get mac address");
        }
        return "02:00:00:00:00:00";
    }

    private String ReadSSOServerAddressDetails() {

        String ipAddr = null;
        String filePath = Environment.getExternalStorageDirectory() + "/ssoserverip.txt";

        GlobalVariables.SSOIPAddress = Constants.SERVER_IPADDR;
        try {

            Log.d("Main2Activity", "in ReadSSOServerAddressDetails " + filePath);
            InputStream is = new FileInputStream(filePath);

            int size = is.available();
            Log.d("Main2Activity", "Size of the JSON is " + size);
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();

            ipAddr = new String(buffer, "UTF-8");

            Log.d(TAG, "IP Addr is " + ipAddr);
            if (ipAddr.contains(".")) {
                GlobalVariables.SSOIPAddress = ipAddr;

                Log.d(TAG, "It is Proper IP Address " + GlobalVariables.SSOIPAddress);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.d("Main2Activity", "Coming out of ReadSSOServerAddressDetails " + GlobalVariables.SSOIPAddress);
        return ipAddr;
    }

    //Timer to Check Internet connectivy
    private void sendDeviceStatusTimerTask() {
        //Showing the Progress

        //PrefUtils.setKioskModeActive(true, getApplicationContext());

        if (sendDeviceStatusTimer == null) {
            //    Toast.makeText(getApplicationContext(),"Timer Started - on kiosk ", Toast.LENGTH_SHORT).show();

            Log.d(TAG, "Starting the InternetCheckTimer ");

            sendDeviceStatusTimer = new Timer();
            sendDeviceStatusTimer.scheduleAtFixedRate(new TimerTask() {

                                                          @Override
                                                          public void run() {
                                                              //  if (!InternetFine)
                                                              {
                                                                  Log.d("MainActivity", "Timer expired ");
                                                                  Main2Activity.this.runOnUiThread(new Runnable() {
                                                                      @Override
                                                                      public void run() {
                                                                          // MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                                                                          Log.d("MainActivity", "Running ");
                                                                          // new VolleyService(Main2Activity.this, LaunchActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress +GET_ALL_USERS, null, "101");
                                                                          sendDeviceStatusInfo();
                                                                      }
                                                                  });
                                                              }
                                                          }
                                                      },
                    20000,
                    20000);
            // 120000,
            //   120000);
        } else
            Log.d(TAG, "Not Starting InternetCheck Timer ");

        // Toast.makeText(getApplication(), "Timer Once again called ", Toast.LENGTH_SHORT).show();
    }

    /*
http://localhost:8114/user_enroll/logs
method:post
parameters:::::{
"log_type":"ss",
"log_time":"4444444",
"log_use_id":"4",
"log_ass_id":"34",
"log_location":"log_location",
"log_message":"log_message",
"log_status":"34",
"log_key_id":"2",
"log_dev_type":"3",
"log_use_id_2":"23",
"log_loc_id":"23"

*/
///log_status,log_devtype, loc_id - integer
    private void sendLogInfo(String logtype, String userID, int locID, String logMessage, String logStatus, String location, int terminal_id) {
        long lastConfigupdate = 0;
        Log.d(TAG, "sendLogInfo " + logtype + " userID " + userID + "  " + locID + "  location " + location);
        try {
            String currts = null;
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = GlobalVariables.SSOIPAddress + LOG_SERVER;
            Log.d(TAG, "Device LOG URL is " + URL);
            JSONObject jsonBody = new JSONObject();

            try {
                Log.d(TAG, "Get Time of Day");
                currts = Long.toString(System.currentTimeMillis() / 1000L);
                try {
                    Log.d(TAG, "Get Time of Day");
                    Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                    //Added 4 & half hours to show the Indian time on the Server - Testing - 25/07/2019
                    //Date currentTime2 = curCalendar.getInstance().getTime();
                    // currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                    currts = Long.toString(curCalendar.getTimeInMillis() / 1000L + 16200);

                    String curr1 = Long.toString(System.currentTimeMillis() / 1000L);
                    // jsonObject2.put("logged_in_time_secs", currts);
                    Log.d(TAG, "Current Time stamp is GMT " + currts);
                    Log.d(TAG, "Current Time stamp is " + curr1);

                } catch (Exception obj) {

                }
                Log.d(TAG, "Current Time stamp is " + currts);


                // jsonBody.put("model", model);
                jsonBody.put("log_type", logtype);
                jsonBody.put("log_time", currts);
                jsonBody.put("log_use_id", userID);
                jsonBody.put("log_ass_id", "34");
                jsonBody.put("log_location", location);
                jsonBody.put("log_message", logMessage);
                // jsonBody.put("_3g", threeg);
                jsonBody.put("log_status", 34); //7, 34
                jsonBody.put("log_key_id", terminal_id);//terminal_id
                jsonBody.put("log_dev_type", 0); //0,3
                // jsonBody.put("log_use_id_2", "23");
                // jsonBody.put("date_identified", date_identified);
                jsonBody.put("log_loc_id", locID);

            } catch (Exception obj) {
                Log.d(TAG, "Got Exception during JSON Body");
            }

            final String requestBody = jsonBody.toString();
            Log.d(TAG, "Post " + requestBody);
            new VolleyService(Main2Activity.this, Main2Activity.this).tokenBase(POST, URL, jsonBody, "211");

/*
                Log.d(TAG,"Sendign Device Status "+requestBody+"Get URL is "+URL);
                StringRequest stringRequest = new StringRequest(POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("LOGStatus ", "sendInfo" + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("LogStatus", "SendInfo" + error.toString());
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }

                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response.statusCode);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }
                };

                requestQueue.add(stringRequest);
*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void adminVoiceVerified() {
        Intent intent = new Intent(this, VoiceVerifiedActivity.class);
        startActivity(intent);
    }

    public void adminFingerVerified() {
        Intent intent = new Intent(this, FIngerVerifiedActivity.class);
        startActivity(intent);
    }

    public void adminFaceVerified() {

        Intent intent = new Intent(this, FaceVerifiedActivity.class);
        startActivity(intent);
    }

    private void RegisterInterReceiver() {

        //registerReceiver(this.FingerScanActivity,
        //      this.mUpdateReceiver);

        if (GlobalVariables.AppSupport == 1) {
            Log.d(TAG, "In RegisterItnerReceiver");
            this.mUpdateFilter = new IntentFilter("lumidigm.captureandmatch.activites.processmain");

            try {
                this.mUpdateReceiver = new BroadcastReceiver() {
                    public void onReceive(final Context context, final Intent intent) {
                        final String state = intent.getStringExtra("STATE");
                        final String Status = intent.getStringExtra("EVENT");


                        Log.e("Main", "artisecure.activites Received UI Event" + Status + " " + state);

                        {
                            if (Status != null && Status.equals("quit")) {
                                Log.d(TAG, "It is Quit ");
                                GlobalVariables.comeOut++;
                                comeOutofApp = true;
                                finishAffinity();
                            } else if (Status != null && Status.equals("restart")) {
                                Log.d(TAG, "It is Restart ");
                                GlobalVariables.comeOut++;
                                comeOutofApp = true;
                                GoBack();
                            } else if (Status != null && Status.equals("AdminFingerVerification")) {

                                Log.d(TAG, "Got event for the  " + Status + "supervisorUser " + GlobalVariables.supervisorUser + " adminverification" + GlobalVariables.adminVerification);
                                if (GlobalVariables.supervisorUser) {
                                    try {

                                        if (GlobalVariables.adminVerification) {

                                            Log.d(TAG, "It is AdminVerification");
                                            setContentView(R.layout.activity_main2_supervisor);

                                            adminVerificationInitiated = true;

                                            settings = (Button) findViewById(R.id.settings_new);
                                            Log.d(TAG, "Admin Verification Initiated  onClick");
                                        }
                                    } catch (Exception obj) {

                                    }
                                } else {
                                    Log.d(TAG, "Here SupervisiorUser is false - Going for Normal Enrollment ");

                                    //For Normal Enrollment - Show AdminFigner Success and Show UserID screen
                                    new WebUtils().SendingMessageWithStateWeb(getApplicationContext(), "AdminFingerSuccess", null, null);

                                    Handler handler = new Handler();
                                    Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //OutofActivity=true;
                                            Intent intent = new Intent(getApplicationContext(), Enroll.class);

                                            intent.putExtra("userid", "test");
                                            intent.putExtra("fromsupervisor", true);
                                            //Intent i1 = new Intent(getApplicationContext(), HomeEnrollActivity.class);
                                            // Intent i = new Intent(this, MainActivity.class);
                                            finish();

                                            comeOutofApp = true;
                                            startActivity(intent);
                                            Log.d(TAG, "Going to the Enroll");
                                            // StopLedAuthorized();
                                        }
                                    }, 3000);

                                }
                            } else if (Status != null && Status.equals("EnrollTypeSelect")) {

                                Log.d(TAG, "Got event for the  " + Status);
                                //if (GlobalVariables.supervisorUser)
                                {
                                    try {

                                        //if (adminVerification)
                                        {

                                            //  Log.d(TAG,"It is AdminVerification");
                                            //    setContentView(R.layout.activity_main2_supervisor);

                                            //  adminVerificationInitiated = true;

                                            // settings = (Button) findViewById(R.id.settings_new);
                                            Log.d(TAG, "Check it is " + state);
                                            Log.d(TAG, "Starting Enroll ");
                                            Intent intent3 = new Intent(getApplicationContext(), FingerPrintEnrollActivity.class);

                                            //Intent intent = new Intent(this, HomeEnrollActivity.class);
                                            intent3.putExtra("userid", state);
                                            intent3.putExtra("fromsupervisor", false);
                                            // Intent i = new Intent(this, MainActivity.class);
                                            startActivity(intent3);
                                            finish();
                                            comeOutofApp = true;
                                        }
                                    } catch (Exception obj) {

                                    }
                                }
                            }
                            if (Status != null && Status.equals("GoToEnroll")) {

                                Log.d(TAG, "Got event for the  " + Status);
                                //if (GlobalVariables.supervisorUser)
                                {
                                    try {

                                        //if (adminVerification)
                                        {

                                            //  Log.d(TAG,"It is AdminVerification");
                                            //    setContentView(R.layout.activity_main2_supervisor);

                                            //  adminVerificationInitiated = true;

                                            // settings = (Button) findViewById(R.id.settings_new);
                                            Log.d(TAG, "Check it is " + state);
                                            Log.d(TAG, "Starting Enroll ");
                             /*                Intent intent3 = new Intent(getApplicationContext(), FingerPrintEnrollActivity.class);


                                             //Intent intent = new Intent(this, HomeEnrollActivity.class);
                                             intent3.putExtra("userid",state );
                                             intent3.putExtra("fromsupervisor",false );
                                             // Intent i = new Intent(this, MainActivity.class);
                                             startActivity(intent3);
*/
                                            Intent intent2 = new Intent(getApplicationContext(), Enroll.class);

                                            //Intent intent = new Intent(this, HomeEnrollActivity.class);
                                            intent2.putExtra("userid", "test");
                                            intent2.putExtra("fromsupervisor", false);
                                            // Intent i = new Intent(this, MainActivity.class);
                                            startActivity(intent2);
                                            comeOutofApp = true;
                                            finish();
                                        }
                                    } catch (Exception obj) {

                                    }
                                }
                            } else if (Status != null && Status.equals("Glow_Led")) {
                                Log.d(TAG, "Glow Led State is " + state);
                                if (state != null && state.equals("Success")) {
                                    if (ledSupport)
                                        StartLedAuthorized();
                                } else if (state != null && state.equals("Reject")) {
                                    if (ledSupport)
                                        StartLedUnAuthorized();
                                }
                            } else if (Status != null && Status.equals("Stop_Led")) {
                                Log.d(TAG, "Glow Led State is " + state);
                                if (state != null && state.equals("Success")) {
                                    if (ledSupport)
                                        StopLedAuthorized();
                                } else if (state != null && state.equals("Reject")) {
                                    if (ledSupport)
                                        StopLedUnAuthorized();
                                }
                            }

                        }
                    }
                };
            } catch (Exception obj) {
                Log.d(TAG, "Got Crashed WHile Intent FIlter " + obj.getMessage());
            }
        }
        //   getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
    }

    private void UnRegisterReceiver() {
        Log.d(TAG, "Got UnRegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG, "Getting unRegister");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.mUpdateReceiver);
                //this.mUpdateReceiver = null;
            } catch (Exception obj) {
                Log.d(TAG, "Got crashed while unregister " + obj.getMessage());
            }
        }
    }

    private void RegisterReceiver() {
        Log.d(TAG, "Got RegisterReceiver");

        if (GlobalVariables.AppSupport == 1) {
            try {
                Log.d(TAG, "Register Receiver ");
                //if (this.mUpdateReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mUpdateReceiver, mUpdateFilter);
            } catch (Exception obj) {
                Log.d(TAG, "RegisterReceiver crashed " + obj.getMessage());
            }
        }
    }

    private void SetWorkFlow(int workflow) {
        Log.d(TAG, "Current work flow is " + workflow);

        if (productVersion == 1) {
            GlobalVariables.WorkFlowVal = 1;
            Log.d(TAG, "Work Flow is only Access Control - V0 ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport = 0;
            GlobalVariables.WebServer = false; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = true;//true; //For Mobile - Admin Verification
            TimeandAttendence = false; //true;
            GlobalVariables.adminVerification = true;
            GlobalVariables.RestrictedAccess = false;

            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 1) {
            GlobalVariables.WorkFlowVal = 1;
            Log.d(TAG, "Work Flow is only Access Control ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport = 1;
            GlobalVariables.WebServer = true; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = true;//true; //For Mobile - Admin Verification
            TimeandAttendence = false; //true;
            GlobalVariables.adminVerification = true;
            GlobalVariables.RestrictedAccess = false;

            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 2) {

            GlobalVariables.WorkFlowVal = 2;
            Log.d(TAG, "Work Flow is Supervisiorory Enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport = 1;
            GlobalVariables.WebServer = true; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess = false;
            GlobalVariables.adminVerification = true;

            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 3) {

            GlobalVariables.WorkFlowVal = 3;
            Log.d(TAG, "Work Flow is Normal enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport = 1;
            GlobalVariables.WebServer = true; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = false;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess = false;
            GlobalVariables.adminVerification = false;

            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 4) {

            GlobalVariables.WorkFlowVal = 4;
            Log.d(TAG, "Work Flow is for Time and Attendence");

            GlobalVariables.AppSupport = 1;
            GlobalVariables.WebServer = true; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = true; //true;
            GlobalVariables.RestrictedAccess = false;

            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 5) {

            GlobalVariables.WorkFlowVal = 5;
            Log.d(TAG, "Work Flow is for Restricted Access");

            GlobalVariables.AppSupport = 1;
            GlobalVariables.WebServer = true; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = true;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = true; //true;
            GlobalVariables.TimeandAttendence = false;


            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 6) {

            GlobalVariables.WorkFlowVal = 6;
            Log.d(TAG, "Work Flow is for TV Support");

            GlobalVariables.AppSupport = 0;
            GlobalVariables.WebServer = false; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = true;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = false; //true;
            GlobalVariables.TimeandAttendence = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 7) {

            GlobalVariables.WorkFlowVal = 7;
            Log.d(TAG, "Work Flow is for Termianl Entry Exit  ");

            GlobalVariables.AppSupport = 0;
            GlobalVariables.WebServer = false; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = false;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = false; //true;
            GlobalVariables.TimeandAttendence = false;

            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = true;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 8) {

            GlobalVariables.WorkFlowVal = 8;
            Log.d(TAG, "Work Flow is for Guard Station Waiting ARea");

            GlobalVariables.AppSupport = 0;
            GlobalVariables.WebServer = false; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = false;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = false; //true;
            GlobalVariables.TimeandAttendence = false;


            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = true;
            GlobalVariables.Detainee_Checkpoint = false;
        } else if (workflow == 9) {

            GlobalVariables.WorkFlowVal = 9;
            Log.d(TAG, "Work Flow is for Guard Station Waiting ARea");

            GlobalVariables.AppSupport = 0;
            GlobalVariables.WebServer = false; //true;

            GlobalVariables.onlyVerification = false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus = false;
            GlobalVariables.supervisorUser = false;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = false; //true;
            GlobalVariables.TimeandAttendence = false;


            GlobalVariables.TV_Support = false;
            GlobalVariables.Terminal_Support = false;
            GlobalVariables.GuardStation_WaitingArea = false;
            GlobalVariables.Detainee_Checkpoint = true;

        } else


            Log.d(TAG, "Work Flow not supported " + workflow);
    }

    void StartLedAuthorized() {
        Log.d(TAG, "in startLedAuthorized " + productVersion);

        if (ledSupport == true) {
            try {
                if (GlobalVariables.MobileDevice == false) {
                    ledStatus = new DeviceManager();
                    /* set Pin Direction
                     * @param PIN pin number from GPIO map
                     * @param Direction in : for input , out : for output
                     */
                    // public static void setPinDir(@NonNull String PIN,@NonNull EnPinDirection Direction){

                    // EnPinDirection Direction= new ("out");
                    Log.d(TAG, "Set Pin DIrection");
                    ledStatus.setPinDir("13", "out");
                    Log.d(TAG, "Set Pin Output ");
                    ledStatus.setPinOn("13");
                    Log.d(TAG, "Set Pin Out complete");
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got Crashed while enabling ");
            }

        } else
            Log.d(TAG, "LED disabled ");
    }

    void StartLedUnAuthorized() {

        Log.d(TAG, "It is in startLedUnAuthorized " + productVersion);
        if (ledSupport == true) {
            try {
                if (GlobalVariables.MobileDevice == false) {
                    ledStatus = new DeviceManager();
                    /* set Pin Direction
                     * @param PIN pin number from GPIO map
                     * @param Direction in : for input , out : for output
                     */
                    // public static void setPinDir(@NonNull String PIN,@NonNull EnPinDirection Direction){

                    // EnPinDirection Direction= new ("out");
                    Log.d(TAG, "Set Pin DIrection");
                    ledStatus.setPinDir("18", "out");
                    Log.d(TAG, "Set Pin Output ");
                    ledStatus.setPinOn("18");
                    Log.d(TAG, "Set Pin Out complete");
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got Crashed while enabling ");
            }
        } else
            Log.d(TAG, "Start LED is disabled ");
    }

    private void StopLedAuthorized() {

        Log.d(TAG, "Product Version - stopLedAuthorized " + productVersion);
        if (ledSupport == true) {
            if (ledStatus != null) {
                ledStatus.setPinOff("13");
                ledStatus = null;
            }
        }
    }

    private void StopLedUnAuthorized() {

        Log.d(TAG, "Stop LedUnAuthorized " + productVersion);
        if (ledSupport == true) {

            if (ledStatus != null) {
                ledStatus.setPinOff("18");
                ledStatus = null;
            }
        }
    }

    private void GoBack() {
        Log.d(TAG, "Go Back ");

     /*   Intent i = new Intent(getApplicationContext(), LaunchActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK  );
        startActivity(i);*/
        finishAffinity();
    }

    private void InitiateFingerPrintAdd(String userId) {
        //String userId = "126";

        //Current User ID 's are 103 - 108 - 5 devices
        //Levae 103, 104
        //Next 104,105,106,107,108

        String curruserID = null;
        try {
            //      adduserID;

            int startUserID = 103;
            int reserveruserid = 2;
            int lastconfigureduserID = 108;


            String lastuserID = prefs.getString(GlobalVariables.ManuallyEnrollUsedID, null);

            Log.d(TAG, "InitiateFingerPrintAdd" + lastuserID);

            if (lastuserID == null)
                curruserID = String.valueOf(startUserID);
            else {
                int intuserID = Integer.parseInt(lastuserID);

                if (intuserID == startUserID) {
                    intuserID = intuserID + 1;
                    Log.d(TAG, "Matching int and start userid " + intuserID);
                } else if (intuserID > startUserID && intuserID < (startUserID + reserveruserid)) {
                    intuserID = intuserID + 1;

                    Log.d(TAG, "Greater than startuserid and less than reserved " + intuserID);
                } else if (intuserID < lastconfigureduserID) {
                    intuserID = intuserID + 1;

                    Log.d(TAG, "less than last user id " + intuserID);
                } else if (intuserID == lastconfigureduserID) {
                    intuserID = startUserID + reserveruserid;
                    Log.d(TAG, "This is last USER id so intiialing from public " + intuserID);
                }


                Log.d(TAG, "UserID is " + intuserID);
                curruserID = String.valueOf(intuserID);

            }
            Toast.makeText(getApplicationContext(), "Last UserID " + lastuserID + " current user id " + curruserID, Toast.LENGTH_LONG).show();

            SharedPreferences.Editor editor1 = prefs.edit();
            editor1.putString(GlobalVariables.ManuallyEnrollUsedID, curruserID);
            editor1.commit();

            userId = curruserID;

        } catch (Exception obj) {
            Log.d(TAG, "Got the exception o" + obj.getMessage());
        }

        Log.d(TAG, "InitiatedFingedPrint Add " + userId);

        if (GlobalVariables.mobileWorkFlowConfigure == true)
        //if (true)
        {
            //                if (GlobalVariables.supervisorUser==false) {
            try {
                lumidigm.captureandmatch.entity.User user = appDatabase.userDao().countUsersBasedonUserID(userId);
                if (user != null) {
                    Log.d(TAG, "Got the User details Needs to update" + user.getUsername());

/*
          Intent intent = new Intent(getApplicationContext(), EnrollTypeActivityNew.class);

          intent.putExtra("userid", userId);
          // if (ldapUser != null)

          intent.putExtra("updateUser", true);
          //intent.putExtra("ldapUser", ldapuser);
          // else {
          Bundle bn = new Bundle();
          if (user != null)
            bn.putParcelable("userData", user);
          intent.putExtras(bn);
          // }
          startActivity(intent);
*/

                    Log.d("EnrollmentType", "FingerPrintEnrollActivity User Finger Activity");
                    Intent intent = new Intent(this, FingerPrintEnrollActivity.class);
                    Bundle mBundle = new Bundle();

                    //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerComeOut","102","UserFingerComeOut");
                    mBundle.putBoolean("Enrolment", true);
                    intent.putExtra("userid", user.getUserId());
                    Bundle bn = new Bundle();
                    bn.putBoolean("check", true);
                    if (user != null)
                        bn.putParcelable("userData", user);
                    intent.putExtras(mBundle);


                    intent.putExtra("updateUser", true);

                    Log.d(TAG, "User ID is " + user.getUserId());
                    Log.d(TAG, "Uer Name is " + user.getUsername());

                    // Log.d(TAG,"Boolean Flash updateUser "+updateUser);
                    startActivity(intent);


                }
            } catch (Exception obj) {
                Log.d(TAG, "SuperVisor User got exception ");
            }
        } else
            Log.d(TAG, "Not Adding Finger Print ");
    }


    @Override
    public boolean onUpdateStatus(int acqStatus) {
        Log.d(TAG, "Chand:onUpdateStatus ");
        if (mCancelCapture) {
            mCancelCapture = false;
            return false;
        }

        String feedback = "";

        if (mIsWaitForFingerClearRunning == true && acqStatus == ACQ_FINGER_PRESENT) {
            feedback = "Lift Finger";
        } else if (mIsWaitForFingerClearRunning == false && (acqStatus != ACQ_PROCESSING || acqStatus != ACQ_DONE)) {
            feedback = "Finger Down";
        }
        class Runner implements Runnable {
            Runner(String str) {
                strFeedback = str;
            }

            @Override
            public void run() {
//                mFingerFeedbackTxtView.setText(strFeedback);
            }

            final String strFeedback;
        }
        new Handler(Looper.getMainLooper()).post(new Runner(feedback));

        return true;

    }

    @Override
    public void bioSDKCaptureComplete(BioDeviceStatus result, Bitmap capImage, byte[] capTemplate, int capPADResult) {
        Log.d(TAG, "Chand:bioSDKCaptureComplete ");
        if (result != BIOSDK_OK) {
            onEnrollmentFinished(null);
            handleBioDeviceStatus(result);
            resetGUI();
            //      mEnrollButton.setText("ENROLL");
            mCaptureInProgress = false;
            return;
        }
        mFingerImage = capImage;
        mTemplate = capTemplate;
        mPADResult = capPADResult;
        Log.d(TAG, "Got the PAD Result " + mPADResult);
        if (mWaitForFingerClear) {
            BioSDKDevice fpDevice = getConnectedDevice();
            Log.d(TAG, "Wait for Finger Clear ");
            if (fpDevice == null) {
                return;
            }
            BioDeviceStatus status = fpDevice.waitForFingerClear_asynch(mTimeOut, this);
            if (status == BIOSDK_OK) {
                mIsWaitForFingerClearRunning = true;
            }
        } else {
            mCaptureInProgress = false;
            if (goingForEnroll == true) {
                Log.d(TAG, "Goign for enrollment capture ");
                displayResults();
            } else {
                Log.d(TAG, "Going for Verify Capture");
                VerifyTemplate();

                //Check whether it matches with the current Template
                //ListofFingersVerify();
            }

        }
    }

    @Override
    public void bioSDKWaitForFingerClearComplete(BioDeviceStatus var1) {
        Log.d(TAG, "Chand:bioSDKWaitForFingerClearComplete checking ");
        mCaptureInProgress = false;
        displayResults();
    }

    private void displayResults() {
        Log.d(TAG, "Chand:displayResults");
        // mEnrollButton.setText("ENROLL");
        // mFingerFeedbackTxtView.setText("");
        mIsWaitForFingerClearRunning = false;
        //ImageTools.drawMinutiae(mFingerImage, mTemplate);
        //mFingerImageView.setImageBitmap(mFingerImage);
        int percent = 0;
        String realFinger = "";
        int textColor = 0;
        //  Resources res = getResources();
        //  Drawable dRed = res.getDrawable(R.drawable.curved_progress_bar_red);
        //  Drawable dGreen = res.getDrawable(R.drawable.curved_progress_bar_green);
        if (mPADResult == 1) {
            //    mRealFingerProgressBar.setProgressDrawable(dGreen);
            //   textColor = mColorGreen;
            realFinger = "Genuine";
            percent = 100;
            Log.d(TAG, "It is Geniuine ");
        } else {
            //   mRealFingerProgressBar.setProgressDrawable(dRed);
            //   textColor = mColorRed;
            realFinger = "Impostor";
            percent = 15;
            Log.d(TAG, "It is Impostor");
        }
        //  mRealFingerProgressBar.setProgress(percent);
        //  mRealFingerTxtView.setText(realFinger);
        //  mRealFingerTxtView.setTextColor(textColor);
        onEnrollmentFinished(mTemplate);
        Log.d(TAG,"Playing Beep");
        playBeep();

        Log.d(TAG, "Got the First Template - Now check the second template ");
        Log.d(TAG, "Going for the Verification ");

        User user= ListofFingersVerify();
        if (user == null) {
        //    clearBioData();
            Log.d(TAG,"User is null & it is not authorized");
            Intent i = new Intent(this, NotAuthorizedActivity.class);
            finish();
            Bundle bundle = new Bundle();
            bundle.putParcelable("userData", user);
            i.putExtras(bundle);
            comeOutofApp = true;
            startActivity(i);
            terminate();
            // finishAffinity();
        } else {
            Log.d(TAG,"User is Available and it is Authorized");

            Intent intent = new Intent(getApplicationContext(), FaceSuccessActivity.class);//SuccessActivity
            if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3)
                intent = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);

            intent.putExtra("case", "success");
            //    intent.putExtra("FromWeb", true);

            intent.putExtra("personName", user.getUsername());

            String personID = user.getUserId();

            Log.d(TAG, "Add User " + user.getUsername());


            Bundle bundle = new Bundle();
            bundle.putParcelable("User", user);
            intent.putExtras(bundle);


            if (personID != null)
                intent.putExtra("personID", personID);
            Log.d(TAG, "Check IN status is " + personID);

           // finishAffinity();
            startActivity(intent);

            comeOutofApp=true;
            notauthorized=false;
            terminate();
            //finish();
        }
        //GoforVerify();
    }

    public void resetGUI() {
        //  mFingerImageView.setImageDrawable(null);
        //  mRealFingerProgressBar.setProgress(0);
        //  mRealFingerTxtView.setText("");
        //  mFingerFeedbackTxtView.setText("");
    }

    /*
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof IFragmentListener) {
                mListener = (IFragmentListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }


        @Override
        public void onStart() {
            super.onStart();

        }


        @Override
        public void onHiddenChanged(boolean hidden) {
            if (hidden) {
                BioSDKDevice device = mListener.getConnectedDevice();
                if (device != null) {
                    //        mFingerFeedbackTxtView.setText("");
                    //      mEnrollButton.setText("ENROLL");
                    mCaptureInProgress = false;
                    device.cancel_async();
                }
            }
        }
    */
    @Override
    public void onUpdateProgress(int percent) {
        //  mInitProgressBar.setProgress(percent);
    }

    @Override
    public void onEnumerateFinished(BioDeviceStatus result) {
        Log.d(TAG, "ChandonEnumberatedFinished " + result);

        mStatus = result;
        switch (result) {
            case BIOSDK_OK:
            case BIOSDK_ERROR_ALREADY_INITIALIZED:
                finishFragment();
                break;
            case BIOSDK_ERROR_INTERNAL:
            case BIOSDK_ERROR_USER_DENIED_PERMISSIONS:
                popupDialog("App Needs USB Permissions", "Exit", true);
                break;
            case BIOSDK_ERROR_NO_DEVICE_PRESENT:
                Log.d(TAG, "ChandNo Device Present ");
                popupDialog("No Supported Device Found", "Exit", true);
                break;
            case BIOSDK_ERROR_SYSTEM_PERMISSIONS:
                popupDialog("Device open in another application", "Exit", true);
                break;
            default:
                popupDialog("Device open in another application", "Exit", true);
                break;
        }
    }

    private void finishFragment() {
        Log.d(TAG, "finishFragment ");
        // mStatus;
        onInitializationFinished(mStatus);
    }

//}

    private void terminate() {
        Log.d(TAG, "Terminate ");
        BioSDKFactory.releaseBioSDKAPI();
        finish();
        return;
    }

    @Override
    public void onInitializationFinished(BioDeviceStatus status) {
        Log.d(TAG, "Chand InitializedFinished ");
        if (status != BIOSDK_OK) {
            BioSDKFactory.releaseBioSDKAPI();
            finish();
            return;
        }
        if (false == initializeDevice()) {
            return;
        }
        // Default to the Enroll Fragment
//        mFragmentMgr.beginTransaction().hide(mActiveFragment).show(mEnrollFragment).commitAllowingStateLoss();
//        mActiveFragment = mEnrollFragment;
        return;
    }

    @Override
    public void onEnrollmentFinished(byte[] template) {
        // In a real integration, this enrollment template would be persisted in a database.  For
        // this source code example, we just send it to the Verify Fragment.
        //      mVerifyFragment.setProbeTemplate(template);
        Log.d(TAG, "Saving the Enrollment ");
        mProbeTemplate = template;
        goingForEnroll = false;
        Log.d(TAG, "Going for the Enroll ");
        Log.d(TAG, "Chand Enrollment finished Got the Template ");
    }

    @Override
    public void onTerminate(String msg) {
        Log.d(TAG, "Chand onTerminate ");
        popupDialog(msg, "Exit", true);
    }


    @Override
    public BioSDKDevice getConnectedDevice() {
        Log.d(TAG, "Get deviceConnectedDevice ");
        return mFPDevice;
    }

    @Override
    public Void device_connected() {
        Log.d(TAG, "ChandDevice_connected ");
        return null;
    }

    @Override
    public Void device_disconnected() {
        Log.d(TAG, "Chand device_disconnected ");
        mFPDevice = null;
        if (comeOutofApp == false)
            popupDialog("Device disconnected!", "Exit", true);
        return null;
    }

    public void handleBioDeviceStatus(BioDeviceStatus status) {

        Log.d(TAG, "Chand:handleBioDeviceStatus ");

        switch (status) {
            case BIOSDK_OK: {
                // Do nothing
            }
            break;
            case BIOSDK_TIMEOUT: {
                if (comeOutofApp == false)
                popupDialog("Capture Timed Out", "OK", false);
            }
            break;
            case BIOSDK_CANCELLED: {
                if (comeOutofApp == false)
                popupDialog("Capture Cancelled", "OK", false);
            }
            break;
            case BIOSDK_ERROR_ALREADY_INITIALIZED: {
                popupDialog("SDK Already Initialized", "OK", false);
            }
            break;
            case BIOSDK_ERROR_NOT_INITIALIZED: {
                popupDialog("SDK Not InitializedExiting: Exiting", "Exit", true);
            }
            break;
            case BIOSDK_ERROR_NO_DATA: {
                if (comeOutofApp == false)
                popupDialog("No Data", "OK", false);
            }
            break;
            case BIOSDK_ERROR_PARAMETER: {
                popupDialog("Bad Parameter", "OK", false);
            }
            break;
            case BIOSDK_ERROR_THREAD: {
                popupDialog("Thread Error", "OK", false);
            }
            break;
            case BIOSDK_ERROR_PROCESSING: {
                popupDialog("Processing Error", "OK", false);
            }
            break;
            case BIOSDK_ERROR_ASYNC_TASK_RUNNING: {
                popupDialog("Async Task Running", "OK", false);
            }
            break;
            case BIOSDK_ERROR_INTERNAL: {
                popupDialog("Internal Error: Exiting", "Exit", true);
            }
            break;
            case BIOSDK_ERROR_USER_DENIED_PERMISSIONS: {
                popupDialog("App Needs USB Permissions: Exiting", "Exit", true);
            }
            break;
            case BIOSDK_ERROR_SYSTEM_PERMISSIONS: {
                popupDialog("System Permission Error: Exiting", "Exit", true);
            }
            break;
            case BIOSDK_ERROR_NO_DEVICE_PRESENT: {
                if (comeOutofApp == false)
                popupDialog("No Supported Device Found: Exiting", "Exit", true);
            }
            break;
            case BIOSDK_ERROR_ENROLLMENTS_DO_NOT_MATCH: {
                popupDialog("Enrollments Do Not Match", "OK", false);
            }
            break;
            case BIOSDK_FINGER_PRESENT: {
                popupDialog("Finger is Present", "OK", false);
            }
            break;
            default: {
                popupDialog("Unknown Error", "OK", false);
            }
            break;
        }
    }

    private boolean initializeDevice() {

        Log.d(TAG, "Chand:initializeDevice ");
        try {
            Log.d(TAG, "in initializeDevice ");
            BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
            if (bAPI == null) {
                popupDialog("No device connected.", "Exit", false);
                return false;
            }
            //The one-time open of the device
            mFPDevice = bAPI.openDevice(0);
            if (mFPDevice == null) {
                popupDialog("For Open No device connected.", "Exit", false);
                return false;
            }
            Log.d(TAG, "Before set BioSDKDeviceListener ");
            mFPDevice.setBioSDKDeviceListener(this);
      //      Toast.makeText(Main2Activity.this, "Device Got connected ", Toast.LENGTH_SHORT).show();

            //popupDialog("Device connected.", "ComeOut", false);

            Log.d(TAG, "Going to start the Enroll ");
            /*Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Log.d(TAG, "Calling onEnroll ");
                    onEnroll();
                }
            }, 4000);
*/
            onEnroll();
            return true;
        } catch (Exception obj) {
            Log.d(TAG, "initializeDevice " + obj.getMessage());
            popupDialog("InitializeDevice No device connected.", "Exit", false);
        }
        return false;
    }

    public void popupDialog(String msg, String btn, final boolean terminal) {

        Log.d(TAG, "Chand:popupDialog");
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setText(btn);
        TextView tv = dialog.findViewById(R.id.text);
        tv.setText(msg);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (terminal) terminate();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                if (terminal) terminate();
            }
        });
        dialog.show();
    }

    private void onEnroll() {
        Log.d(TAG, "in onEnroll ");
        BioSDKDevice fpDevice = getConnectedDevice();
        //fpDevice.getLastImage()

        if (fpDevice == null) {

            Log.d(TAG, "in onEnroll Failure");
            popupDialog("Fingerprint Device is null", "OK", false);
            return;
        } else
            Log.d(TAG, "in onEnroll Success");

        if (mCaptureInProgress == true) {
            Log.d(TAG, "capture in progress come out");
            mCancelCapture = true;
            //   mFingerFeedbackTxtView.setText("");
            //   mEnrollButton.setText("ENROLL");
            mCaptureInProgress = false;
            return;
        }
        Log.d(TAG, "Going for setConfiguration State ");
        Map<String, String> strMapSecurityLevel = new HashMap<>();
        strMapSecurityLevel.put("matching_security_level", mMatchLevel);
        strMapSecurityLevel.put("pad_security_level", mPADLevel);
        BioDeviceStatus status = fpDevice.setConfigurationState(strMapSecurityLevel);
        if (status != BioDeviceStatus.BIOSDK_OK) {
            // mListener.popupDialog("SetConfigState returned " + status.toString() + "(" + mMatchLevel + " " + mPADLevel + ")", "Exit",true);
            return;
        }
        BioDeviceStatus mStatus = fpDevice.capture_async(mTimeOut, this);
        if (mStatus != BIOSDK_OK) {
            //  mFingerFeedbackTxtView.setText("");
            //  mEnrollButton.setText("ENROLL");
            mCaptureInProgress = false;
            Log.d(TAG, "It is not Success ");
            handleBioDeviceStatus(mStatus);
        } else
            Log.d(TAG, "Enroll is success ");
        resetGUI();
        // mFingerFeedbackTxtView.setText("");
        // mEnrollButton.setText("CANCEL");
        mCaptureInProgress = true;
    }

    public void setTimeOut(int timeOut) {
        mTimeOut = timeOut;
    }

    public void setMatchLevel(String matchLevel) {
        mMatchLevel = matchLevel.toUpperCase();
    }

    public void setPADLevel(String padLevel) {
        mPADLevel = padLevel.toUpperCase();
    }

    public void setWaitForFingerClear(boolean waitForFingerClear) {
        mWaitForFingerClear = waitForFingerClear;
    }

    private void VerifyTemplate() {
//        mVerifyButton.setText("VERIFY");
        //   mFingerFeedbackTxtView.setText("");
        //   mWaitForFingerClearRunning = false;
        //   mFingerFeedbackTxtView.setText("");
        //ImageTools.drawMinutiae(mFingerImage, mTemplate);
        //   mFingerImageView.setImageBitmap(mFingerImage);
        int percent = 0;

        String match = "";
        String fealFinger = "";

        int txtColorMatch = 0;
        int txtColorRealFinger = 0;

        // Resources res = getResources();
        // Drawable dRed = res.getDrawable(R.drawable.curved_progress_bar_red);
        // Drawable dGreen = res.getDrawable(R.drawable.curved_progress_bar_green);

        Log.d(TAG, "Going for Verify Template ");
        if (mPADResult == 1) {
            //   mRealFingerProgressBar.setProgressDrawable(dGreen);
            fealFinger = "Genuine";
            // txtColorRealFinger = mColorGreen;
            percent = 100;
            Log.d(TAG, "It is Genuine Verify ");
        } else {
            //  mRealFingerProgressBar.setProgressDrawable(dRed);
            fealFinger = "Impostor";
            //  txtColorRealFinger = mColorRed;
            percent = 15;

            Log.d(TAG, "It is Impostor Verify ");
        }
        //mRealFingerProgressBar.setProgress(percent);
        //mRealFingerTxtView.setText(fealFinger);
        //mRealFingerTxtView.setTextColor(txtColorRealFinger);
        int matchScore = 0;
        if (mProbeTemplate != null) {

            Log.d(TAG, "ProbleTemplate is not null");
            BioSDKDevice fpDevice = getConnectedDevice();
            if (fpDevice == null) {
                return;
            }
            Log.d(TAG, "Going for Match previous Template ");
            BioDeviceStatus mStatus = fpDevice.match(mProbeTemplate, mTemplate);

            if (mStatus == BIOSDK_OK) {
                Log.d(TAG, "Beforee matchScore ");
                matchScore = fpDevice.getLastMatchResult();

                Log.d(TAG, "MatchSCore value is " + matchScore);
                if (matchScore == 1) {
                    //      mMatchProgressBar.setProgressDrawable(dGreen);
                    //      mMatchProgressBar.setProgress(100);
                    match = "Match";
                    Log.d(TAG, "Match it is  ");

                    Toast.makeText(Main2Activity.this, "Matching Templates", Toast.LENGTH_SHORT).show();

                    //      txtColorMatch = mColorGreen;
                } else {
                    //    mMatchProgressBar.setProgressDrawable(dRed);
                    //    mMatchProgressBar.setProgress(15);
                    match = "No Match";

                    Log.d(TAG, "No Match it is ");
                    Toast.makeText(Main2Activity.this, "Not matching Templates", Toast.LENGTH_SHORT).show();

                    //    txtColorMatch = mColorRed;
                }
            }
        }
        //   mMatchTxtView.setText(match);
        //   mMatchTxtView.setTextColor(txtColorMatch);
    }

    private boolean initEngine() {
        Log.d(TAG, "ChandinitEngine");
        BioSDKAPI api = BioSDKFactory.getBioSDK();
        if (api == null) {
            Log.d(TAG, "API is null and coming out ");
            return false;
        } else
            Log.d(TAG, "ChandgetBioSDK is successful ");
        api.enumerateDevices(this);
        return true;
    }

    private void GoforVerify() {

        Log.d(TAG, "GoforVerify ");

        BioSDKDevice fpDevice = getConnectedDevice();
        if (fpDevice == null) {
            popupDialog("Fingerprint Device is null", "OK", false);
            return;
        }
        if (mCaptureInProgress == true) {
            Log.d(TAG, "For Verification inProgress is true ");
            mCancelCapture = true;
            //   mFingerFeedbackTxtView.setText("");
            //   mVerifyButton.setText("VERIFY");
            mCaptureInProgress = false;
            return;
        }
        Map<String, String> strMapSecurityLevel = new HashMap<>();
        strMapSecurityLevel.put("matching_security_level", mMatchLevel);
        strMapSecurityLevel.put("pad_security_level", mPADLevel);
        BioDeviceStatus status = fpDevice.setConfigurationState(strMapSecurityLevel);
        if (status != BioDeviceStatus.BIOSDK_OK) {
            popupDialog("SetConfigState returned " + status.toString() + "(" + mMatchLevel + " " + mPADLevel + ")", "Exit", true);
            return;
        }
        // In this example we call capture_async().  Note: we could call verify_async() and pass in
        // mProbeTemplate instead of calling match() in displayResults() method.
        mStatus = fpDevice.capture_async(mTimeOut, this);
        if (mStatus != BIOSDK_OK) {
            // mFingerFeedbackTxtView.setText("");
            // mVerifyButton.setText("VERIFY");
            mCaptureInProgress = false;
            handleBioDeviceStatus(mStatus);
        }
        resetGUI();
        //mFingerFeedbackTxtView.setText("");
        // mVerifyButton.setText("CANCEL");
        mCaptureInProgress = true;


    }

    private void GoforTemplateVerification() {

        Log.d(TAG, "GoforVerify ");

        BioSDKDevice fpDevice = getConnectedDevice();
        if (fpDevice == null) {
            popupDialog("Fingerprint Device is null", "OK", false);
            return;
        }
        if (mCaptureInProgress == true) {
            Log.d(TAG, "For Verification inProgress is true ");
            mCancelCapture = true;
            //   mFingerFeedbackTxtView.setText("");
            //   mVerifyButton.setText("VERIFY");
            mCaptureInProgress = false;
            return;
        }
        Map<String, String> strMapSecurityLevel = new HashMap<>();
        strMapSecurityLevel.put("matching_security_level", mMatchLevel);
        strMapSecurityLevel.put("pad_security_level", mPADLevel);
        BioDeviceStatus status = fpDevice.setConfigurationState(strMapSecurityLevel);
        if (status != BioDeviceStatus.BIOSDK_OK) {
            popupDialog("SetConfigState returned " + status.toString() + "(" + mMatchLevel + " " + mPADLevel + ")", "Exit", true);
            return;
        }
        // In this example we call capture_async().  Note: we could call verify_async() and pass in
        // mProbeTemplate instead of calling match() in displayResults() method.
        mStatus = fpDevice.capture_async(mTimeOut, this);
        if (mStatus != BIOSDK_OK) {
            // mFingerFeedbackTxtView.setText("");
            // mVerifyButton.setText("VERIFY");
            mCaptureInProgress = false;
            handleBioDeviceStatus(mStatus);
        }
        resetGUI();
        //mFingerFeedbackTxtView.setText("");
        // mVerifyButton.setText("CANCEL");
        mCaptureInProgress = true;

    }

    private User ListofFingersVerify() {

        int j;
        int score1=4,score2=4,score3=4;
        List<User> users = appDatabase.userDao().getAll();

        long currTimemillisec = System.currentTimeMillis();
        long Timeinmillisec = 0;

        Log.d(TAG, "Users Count is " + users.size());
        int currCount = 0;
        BioSDKDevice fpDevice = getConnectedDevice();
        Log.d(TAG, "Wait for Finger Clear ");
        if (fpDevice == null) {
            Log.d(TAG,"fpDevice is null coming out ");
            return null;
        }
        Log.d(TAG,"Going for users Verify Template "+mProbeTemplate.toString());

        for (User user : users) {
            try {
                currCount++;
                Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                currTimemillisec = System.currentTimeMillis();

                //Log.d(TAG,"Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);

                j = 1;
                //Log.d(TAG, "doInBackground: " + user.toString());
                String fingarprint1 = user.getTemplate1();
                byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);
                Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
                currTimemillisec = System.currentTimeMillis();
                //Log.d(TAG,"decode Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);
                //Each Match Taking 66 milliseconds
               // rc = mVCOM.Match(mProbeTemplate, mTmplCapture1);

                Log.d(TAG,"Template Capture is "+mTmplCapture1);

                BioDeviceStatus mStatus = fpDevice.match(mProbeTemplate, mTmplCapture1);
                Log.d(TAG,"First Template Status is "+mStatus);
                fingarprint1 = user.getTemplate2();
                //byte[] mTmplCapture2 = Base64.decode(fingarprint1, Base64.DEFAULT);
                mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);
                BioDeviceStatus mStatus2 = fpDevice.match(mProbeTemplate, mTmplCapture1);
                score2= fpDevice.getLastMatchResult();
                fingarprint1 = user.getTemplate3();
                //byte[] mTmplCapture3 = Base64.decode(fingarprint1, Base64.DEFAULT);
                mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);
                BioDeviceStatus mStatus3 = fpDevice.match(mProbeTemplate, mTmplCapture1);
                score3= fpDevice.getLastMatchResult();


                if (mStatus == BIOSDK_OK || mStatus2 == BIOSDK_OK || mStatus3 == BIOSDK_OK ) {
                    Log.d(TAG,"SDK is success ");
                     score1= fpDevice.getLastMatchResult();
                    if (score1 == 1 || score2 == 1 || score3 == 1) {
                        Log.d(TAG,"It is success matching the template ");


                   /*     Intent intent = new Intent(getApplicationContext(), FaceSuccessActivity.class);//SuccessActivity
                        if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3)
                            intent = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);

                        intent.putExtra("case", "success");
                        //    intent.putExtra("FromWeb", true);

                        intent.putExtra("personName", user.getUsername());

                        String personID = user.getUserId();

                        Log.d(TAG, "Add User " + user.getUsername());


                        Bundle bundle = new Bundle();
                        bundle.putParcelable("User", user);
                        intent.putExtras(bundle);


                        if (personID != null)
                            intent.putExtra("personID", personID);
                        Log.d(TAG, "Check IN status is " + personID);

                        startActivity(intent);

                        notauthorized=false;
                   */

                        Log.d(TAG,"going for Clearn Async ");
                     /*   BioDeviceStatus status = fpDevice.waitForFingerClear_asynch(mTimeOut, this);
                        if (status == BIOSDK_OK) {
                            Log.d(TAG,"Clear Asyn success");
                            mIsWaitForFingerClearRunning = true;
                        }
*/
                     Log.d(TAG,"Going for Wait Clear ");
                     fpDevice.waitForFingerClear(mTimeOut,this);
                    Log.d(TAG,"come out of wait for Finger clear ");
                        fpDevice.clearBioData();
                        BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
 /*
                        if (bAPI != null)
                            bAPI.closeDevices();
                        else
                            Log.d(TAG,"Device is already closed ");*/
                        return user;
                        //break;
                        //finish();
                    }
                } else
                    notauthorized=true;
            } catch (Exception obj) {
                Log.d(TAG, "Got Exception while reading fingerprint"+obj.getMessage());
                notauthorized = true;
            }
        }

      /*  Log.d(TAG,"going for Clearn Async Next ");
        BioDeviceStatus status = fpDevice.waitForFingerClear_asynch(mTimeOut, this);
        if (status == BIOSDK_OK) {
            Log.d(TAG,"Clear Asyn success");
            mIsWaitForFingerClearRunning = true;
        }

*/
 //       fpDevice.clearBioData();
        return null;
    }
}
