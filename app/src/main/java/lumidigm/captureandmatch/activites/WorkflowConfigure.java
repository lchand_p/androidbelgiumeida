package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import frost.AdminVerificationActivityMobile;
import frost.UserIdActivity;
import lumidigm.captureandmatch.R;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static webserver.GlobalVariables.ProductV4Support;
import static webserver.GlobalVariables.TimeandAttendence;
import static webserver.GlobalVariables.productVersion;
import static webserver.GlobalVariables.selectedProduct;

public class WorkflowConfigure extends Activity implements View.OnClickListener {

//public class FaceSettings extends Activity  {


    private LinearLayout mainlayut;
    private Button settings;
    private Button enrollbutton;
    private Button enrollbasic;
    private String TAG="Settings";
    private TextView enrollView=null;
    AlertDialog alertDialog =null;
    private Button enrollment;
    private Button accesscontrol;
    private Button timeandattendance;
    private Button restrictedaccess;
    private SharedPreferences prefs= null;
    private int currWorkFlow=-1;
    private Button back;

    private Button DetaineeIdentify=null;
    private TextView DetaineeIdentifyText=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workflowconfigure);

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
       //  enrollbutton=(Button)findViewById(R.id.enrolladmin);

        Log.d("FaceSettings","oncreate ");
       // mainlayut = (LinearLayout) findViewById(R.id.mainlayut);
        ///imageHint = (ImageView) findViewById(R.id.image_hint);
        enrollment = (Button) findViewById(R.id.enrollment);
      //  View overlay = findViewById(R.id.mylayout);

        accesscontrol=(Button)findViewById(R.id.accesscontrol);
        timeandattendance=(Button)findViewById(R.id.timeandattendance);
        restrictedaccess=(Button)findViewById(R.id.restrictedaccess);

        enrollment.setOnClickListener(this);
        accesscontrol.setOnClickListener(this);
        timeandattendance.setOnClickListener(this);
        restrictedaccess.setOnClickListener(this);
        back=(Button) findViewById(R.id.home3);
        back.setOnClickListener(this);

        //Only When Showing Frost - Show the Sentinel Detainee Identifcation
        if (ProductV4Support == true && selectedProduct == 1) {

            try {
                DetaineeIdentify = (Button) findViewById(R.id.detaineeidentify);
                DetaineeIdentifyText = (TextView) findViewById(R.id.detaineeidentifytext);

                DetaineeIdentify.setVisibility(View.VISIBLE);
                DetaineeIdentifyText.setVisibility(View.VISIBLE);

                DetaineeIdentify.setOnClickListener(this);
            } catch (Exception obj) {
                DetaineeIdentify = null;
                Log.d(TAG,"Got the Detainee Identifcation Failure "+obj.getMessage());
            }
            }
        /*Log.d(TAG,"Work Flow Status Val is  "+GlobalVariables.WorkFlowVal);
        if ( GlobalVariables.WorkFlowVal==2 || GlobalVariables.WorkFlowVal==3 ) {

            //enrollbutton.setOnClickListener(this);
            enrollbasic.setOnClickListener(this);
        } else {
            enrollbasic.setVisibility(View.INVISIBLE);
            enrollView.setVisibility(View.INVISIBLE);
        }*/
        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

    @Override
    public void onClick(View view) {
        Log.d(TAG,"onClick"+view.getId());
        switch (view.getId()) {
            case R.id.enrollment:
                currWorkFlow=2; //For Supervisory enrolment  //3 - normal enrolment

                break;
            case R.id.accesscontrol:
                currWorkFlow = 1;
                break;
            case R.id.timeandattendance:
                currWorkFlow=4;
                break;
            case R.id.restrictedaccess:
                currWorkFlow=5;
                break;
            case R.id.home3:
                //currWorkFlow=5;
                Log.d(TAG,"Go Back Home ");
/*


                //Intent intent = new Intent(getApplicationContext(), Face.class);
                //  finishAffinity();
                //startActivity(intent);
                Intent intent = new Intent(getApplicationContext(), LaunchActivity.class);

                //      Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                //startActivity(intent);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("Enrolment", false);
                mBundle.putBoolean("adminVerification", false);
                // intent.putExtra("userid", editText.getText().toString());
                intent.putExtras(mBundle);
                startActivity(intent);
                finishAffinity();
*/

                break;

            case R.id.detaineeidentify:
                Log.d(TAG,"It is Detainee Identification ");
                currWorkFlow=16;
                break;
            default:
                break;
        }

        Log.d(TAG,"Configured Work FLow is  "+currWorkFlow);

         int oldworkflowID = prefs.getInt(GlobalVariables.localworkflowStatus, -1);

         Log.d(TAG,"Old WOrk flow is "+oldworkflowID);

        //if (currWorkFlow != 1 && currWorkFlow != oldworkflowID) {
        if ( currWorkFlow != oldworkflowID) {

            SharedPreferences.Editor editor1 = prefs.edit();
            editor1.putInt(GlobalVariables.localworkflowStatus, currWorkFlow);

            SetWorkFlow(currWorkFlow);
            Log.d(TAG,"Saving the work flow status "+currWorkFlow);
            editor1.commit();

        } else {
            Log.d(TAG,"Same Work Flow :"+currWorkFlow);
        }

        Handler handler = new Handler();
            Log.d(TAG, "Starting CaptureNad match Task going for sleep");
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    //Intent intent = new Intent(getApplicationContext(), Face.class);
                  //  finishAffinity();
                     //startActivity(intent);

                    if (currWorkFlow == 2) {
                        Log.d(TAG, "Admin Verification is true ");
                        //Intent intent = new Intent(FaceSuccessActivity.this, Enroll.class);
                        Intent intent = new Intent(getApplicationContext(), UserIdActivity.class);

                        intent.putExtra("adminVerification", true);
                        intent.putExtra("userid", "test");
                        intent.putExtra("fromsupervisor", true);

                        //intent.putExtra("userid", personID);
                        startActivity(intent);
                        finishAffinity();
                    } else {
                        Intent intent = new Intent(getApplicationContext(), LaunchActivity.class);

                        //      Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        //Intent intent = new Intent(SuccessActivity.this, HomeActivity.class);
                        //startActivity(intent);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle mBundle = new Bundle();
                        mBundle.putBoolean("Enrolment", false);
                        mBundle.putBoolean("adminVerification", false);
                        // intent.putExtra("userid", editText.getText().toString());
                        intent.putExtras(mBundle);
                        startActivity(intent);
                        finishAffinity();
                    }
                }
            }, 100);
            }
    private void startSettings() {

        Log.d(TAG,"In SHow Licence App");
        LayoutInflater inflater = LayoutInflater.from(WorkflowConfigure.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Android Settings");
        builder.setMessage("Password Verification");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordText = subEditText.getText().toString().trim();
                Log.d(TAG,"Password Text is "+passwordText);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                String settingPwd = getsettingspwd();
                Log.d(TAG,"App Pwd is "+settingPwd);
                if (passwordText.equals("argus@542")) {
                    Log.d(TAG,"Going for Shutdown");
                    //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
                    //Shutdown();
                } else
                if (passwordText.equals("argus@543")) {
                    Log.d(TAG,"Going for Reboot");
                    //reboot();
                } else
                if (passwordText.equals(settingPwd)) {

                    startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                    finish();

                } /*else //Kept for Testing by Chand - remove it
                    if (TestWithoutUSB(passwordText) == null)
                        Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();*/
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

                    // finish();
                }
            }
        });



        builder.show();
    }
    private String getsettingspwd() {
        String verVal=null;

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "settingspwd.txt");

            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(
                    fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            verVal = aBuffer.toString().trim();

            Log.d(TAG,"Reading settings pwd from sdfile is "+verVal+" len s "+verVal.length());
            myReader.close();
            if ((verVal != null)  && (verVal != " ") && (verVal != "") && verVal.length() > 0)
                return verVal;
        } catch (Exception obj) {
            Log.d(TAG,"Got exception while reading settings pwd");

        }
        return "digit9@123";
    }
    private void SetWorkFlow(int workflow) {
        Log.d(TAG,"Current work flow is "+workflow);

        if (productVersion == 1) {
            GlobalVariables.WorkFlowVal=1;
            Log.d(TAG,"Work Flow is only Access Control - V0 ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=0;
            GlobalVariables.WebServer=false; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            TimeandAttendence = false; //true;
            GlobalVariables.adminVerification=true;
            GlobalVariables.RestrictedAccess = false;

        } else
        if (workflow==1) {
            GlobalVariables.WorkFlowVal=1;
            Log.d(TAG,"Work Flow is only Access Control ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.adminVerification=true;
            GlobalVariables.RestrictedAccess =false;
        } else
        if (workflow ==2) {

            GlobalVariables.WorkFlowVal=2;
            Log.d(TAG,"Work Flow is Supervisiorory Enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess =false;
            GlobalVariables.adminVerification=true;

        } else

        if (workflow ==3) {

            GlobalVariables.WorkFlowVal=3;
            Log.d(TAG,"Work Flow is Normal enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess =false;
            GlobalVariables.adminVerification=false;

        } else
        if (workflow ==4) {

            GlobalVariables.WorkFlowVal=4;
            Log.d(TAG,"Work Flow is for Time and Attendence");

            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = true; //true;
            GlobalVariables.RestrictedAccess =false;
        } else
        if (workflow ==5) {

            GlobalVariables.WorkFlowVal=5;
            Log.d(TAG,"Work Flow is for Restricted Access");

            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = true; //true;
            GlobalVariables.TimeandAttendence =false;
        }
        else
            Log.d(TAG,"Work Flow not supported "+workflow);
    }
}
