package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import database.AppDatabase;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
//import lumidigm.captureandmatch.adapters.RackAdapter;
//import lumidigm.captureandmatch.constants.Constant;
//import lumidigm.captureandmatch.database.AppDatabase;
import lumidigm.captureandmatch.entity.User;
//import lumidigm.captureandmatch.lisners.RecyclerItemClickListener;
import lumidigm.captureandmatch.models.AllTasks;
import lumidigm.network.HttpsTrustManager;
import lumidigm.network.IResult;
//import lumidigm.services.AlarmService;
//import lumidigm.services.MyBroadcastReceiver;
import lumidigm.services.VolleyService;
import lumidigm.utils.AlertPopUp;
import webserver.GlobalVariables;
//import lumidigm.view.CircularProgressIndicator;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.android.volley.Request.Method.PUT;
import static lumidigm.constants.Constants.ARGUS_CONTROLLER;
import static lumidigm.constants.Constants.ARGUS_CONTROLLER_STATUS;
import static lumidigm.constants.Constants.TASK_STATUS;
//import static lumidigm.captureandmatch.constants.Constant.LOGOUT;

public class SelectDoor  extends Activity implements IResult {
    private static final String TAG = "SelectDoor";
    private Toolbar toolbarTop;
    private TextView welcomeText;
    private ImageView home1;



    private TextView welcome;
    private TextView msg;
    private TextView proced;
    private ImageView home;

    User user;
    TextView textView;
    String encodedData;

    AppDatabase appDatabase;
    AllTasks allTasks;
    LinearLayoutManager linearLayoutManager;

    //AlarmService alarmService;
    private TextView ticketid;
    private TextView title;
    private TextView date;
    private TextView priority;
    private TextView assignedBy;
    private TextView description;
    private LinearLayout radioGroup;
    private ImageView remove;
    private TextView count;
    private ImageView add;

   public int i1 = 0;
    private CountDownTimer countDownTimer;

    TextView countDownTextView;
   // private CircularProgressIndicator circularProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_list_item2);
        appDatabase = AppDatabase.getAppDatabase(this);
      //  alarmService=new AlarmService();
        View overlay = findViewById(R.id.mylayout);

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//        Intent i = new Intent(this, AlarmService.class);

        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

  //      startService(i);
        Log.d(TAG,"In Select Rack ");
        int minutes = 2;
        int milliseconds = minutes * 60 * 1000;
        try {
            user = getIntent().getExtras().getParcelable("User");
            allTasks = getIntent().getExtras().getParcelable("task");
        } catch (Exception ob) {
            Log.d(TAG,"Got Exception while reading User ");
        }
        Log.d(TAG,"In SelectDoor All Tasks val is "+allTasks.toString());
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("con_hash_key", allTasks.getTags().get(0).getHash_key().trim());
            jsonObject.put("con_aes_key", allTasks.getTags().get(0).getAes_key().trim());
            jsonObject.put("door", allTasks.getTags().get(0).getDoor());
            jsonObject.put("relay", allTasks.getTags().get(0).getRelay());
            Log.d(TAG, "onClick: "+jsonObject);
            Log.d(TAG, "onCreate: "+ARGUS_CONTROLLER);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (ARGUS_CONTROLLER_STATUS.contains("serviceme"))
            HttpsTrustManager.allowAllSSL();

        MyProgressDialog.show(SelectDoor.this, R.string.wait_message);

        UpdateTaskStatus();
        Log.d(TAG,"Calling "+GlobalVariables.SSOIPAddress +ARGUS_CONTROLLER);
        new VolleyService(SelectDoor.this, SelectDoor.this).tokenBase(POST, GlobalVariables.SSOIPAddress +ARGUS_CONTROLLER, jsonObject, "102");
     //   new VolleyService(SelectDoor.this, SelectDoor.this).tokenBase(POST, GlobalVariables.SSOIPAddress +ARGUS_CONTROLLER_STATUS, jsonObject, "102");


        /*countDownTextView=(TextView)findViewById(R.id.countDownTextView) ;
        circularProgress = findViewById(R.id.circular_progress);
        circularProgress.setMaxProgress(milliseconds/1000);
        countDownTimer = new CountDownTimer(milliseconds, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

             int time= (int) ( millisUntilFinished/1000);
                circularProgress.setCurrentProgress(millisUntilFinished/1000);




            }

            @Override
            public void onFinish() {
                Intent i = new Intent(SeletDoor.this, Main2Activity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        };
        countDownTimer.start();*/
        ticketid = (TextView) findViewById(R.id.ticketid);
        title = (TextView) findViewById(R.id.title);
        date = (TextView) findViewById(R.id.date);
        priority = (TextView) findViewById(R.id.priority);
        assignedBy = (TextView) findViewById(R.id.assigned_by);
        description = (TextView) findViewById(R.id.description);
        radioGroup = (LinearLayout) findViewById(R.id.radioGroup);
        remove = (ImageView) findViewById(R.id.remove);
        count = (TextView) findViewById(R.id.count);
        add = (ImageView) findViewById(R.id.add);
          proced =(TextView)findViewById(R.id.proced);


          ImageView imageView=(ImageView)findViewById(R.id.home1);
          imageView.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  Intent i = new Intent(SelectDoor.this, Main2Activity.class);
                  i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                  startActivity(i);
                  finish();
              }
          });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i1 > 0) {
                    --i1;
                    count.setText(String.valueOf(i1));
                } else if (i1 == 0) {
                    count.setText(String.valueOf(i1));
                }


            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ++i1;
                count.setText(String.valueOf(i1));

            }
        });

        proced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject jsonObject = new JSONObject();

                try {
                    Date currentTime = Calendar.getInstance().getTime();
                    Log.d(TAG, "onPostExecute: " + currentTime.toString());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String currentDateandTime = sdf.format(new Date());

                    jsonObject.put("ticket_id", allTasks.get_id());
                    jsonObject.put("status", 1);
                    jsonObject.put("update_time", currentDateandTime);
                    jsonObject.put("userId", user.getUserId());
                    jsonObject.put("status_name", "open");
                    jsonObject.put("uid", allTasks.getUid());
                    jsonObject.put("username", user.getUsername());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                MyProgressDialog.show(SelectDoor.this, R.string.wait_message);
                Log.d(TAG,"Starting sending TaskStatus "+TASK_STATUS);
                Log.d(TAG,"Buffer is "+jsonObject);
                new VolleyService(SelectDoor.this, SelectDoor.this).tokenBase(PUT, GlobalVariables.SSOIPAddress +TASK_STATUS, jsonObject, "101");
                Log.d(TAG,"Send ");


            }
        });


      title.setText(allTasks.getSubject());

    ticketid.setText("Ticket #"+String.valueOf(allTasks.getUid()) +" ("+String.valueOf(allTasks.getTags().get(0).getName())+")");

       // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date1 = null;//You will get date object relative to server/client timezone wherever it is parsed
        try {
            date1 = dateFormat.parse(allTasks.getDate());

            DateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd hh:mm a"); //If you need time just put specific format for time like 'HH:mm:ss'
            String dateStr = formatter1.format(date1);


           // String  strDate = formatter.format(allTasks.getDate());

            date.setText(dateStr);


        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(allTasks.getPriority() == 1){
           priority.setText("Normal");

        }else if(allTasks.getPriority()==3){
           priority.setText("Critical");

        }else if(allTasks.getPriority() == 2){
            priority.setText("High");


        }


       assignedBy.setText(allTasks.getOwner().getUsername());
    description.setText(Html.fromHtml(allTasks.getIssue()));




    }

    @Override
    public void notifySuccess(String requestType, Object response) {
        Log.d(TAG, "notifySuccess: " + response.toString());

        // [{"deleted":false,"status":0,"tags":[],"_id":"5b4f2583ee0c1d1e1e884a29","subject":"ticket targus user","issue":"<p>ticket targus user<\/p>\n","group":"5b4df86b86596b6051cc3bc8","type":"5b4df86b86596b6051cc3bc7","priority":1,"date":"2018-07-18T11:33:23.439Z","comments":[],"notes":[],"attachments":[],"history":[{"action":"ticket:created","description":"Ticket was created.","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:33:23.445Z","_id":"5b4f2583ee0c1d1e1e884a2a"},{"action":"ticket:set:type","description":"Ticket type set to: Issue","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:34.762Z","_id":"5b4f2642ee0c1d1e1e884a2c"},{"action":"ticket:set:type","description":"Ticket type set to: Task","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:36:38.938Z","_id":"5b4f2646ee0c1d1e1e884a2d"},{"action":"ticket:set:assignee","description":"Assignee was cleared","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T11:48:17.863Z","_id":"5b4f2901ee0c1d1e1e884a2e"},{"action":"ticket:set:assignee","description":"roooop was set as assignee","owner":"5b4df86b86596b6051cc3bc9","date":"2018-07-18T12:00:46.705Z","_id":"5b4f2beeee0c1d1e1e884a30"}],"subscribers":["5b4df86b86596b6051cc3bc9","5b4f193a607f6f318db8f9b9"],"owner":"5b4df86b86596b6051cc3bc9","uid":1010,"__v":4,"assignee":"5b4f193a607f6f318db8f9b9"}]

        if (requestType.equals("101")) {
            Log.d(TAG,"Send 101 Main Activity ");
            MyProgressDialog.dismiss();

            //AlertPopUp alertPopUp = new AlertPopUp(this);
            //alertPopUp.show();

            startAlert(encodedData,allTasks.getTags().get(0).getIp_addr());


            Intent i = new Intent(this, Main2Activity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
            this.startActivity(i);

          /*  JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("con_hash_key", allTasks.getTags().get(0).getHash_key());
                jsonObject.put("con_aes_key", allTasks.getTags().get(0).getAes_key());
                jsonObject.put("door", allTasks.getTags().get(0).getDoor());
                jsonObject.put("relay", allTasks.getTags().get(0).getRelay());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            MyProgressDialog.show(SeletRack.this, R.string.wait_message);

            new VolleyService(SeletRack.this, SeletRack.this).tokenBase(GET, Constant.ARGUS_CONTROLLER, jsonObject, "102");
*/
        } else if(requestType.equals("102")){

            Log.d(TAG,"Got SelectAPI Respone");
            try {
                encodedData=((JSONObject)response).getString("encoded_data");
/*
             JSONObject jsonObject=new JSONObject();
             jsonObject.put("encoded_data",encodedData);
                new VolleyService(SeletRack.this, SeletRack.this).tokenBase(POST, Constant.WRITE_LOG, jsonObject, "105");
*/
               // allTasks.getTags().get(0).getIp_addr()
                if (encodedData != null ) {
                    MyProgressDialog.show(SelectDoor.this, R.string.wait_message);
                   String url = "http://" + allTasks.getTags().get(0).getIp_addr() + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                   // String url = "http://172.16.16.110" + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                   // String url = "http://192.168.0.98" + "/cgi-bin/arguscgi.fcgi/aus502/" + encodedData;
                    Log.d(TAG, "onReceive: " + url);
                    new VolleyService(this, SelectDoor.this).tokenBase(GET, url, null, "133");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            MyProgressDialog.dismiss();


        } else if (requestType.equals("133")) {
            Log.d(TAG,"Got response ");
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG,"133 Go to Main Activity");
                        Intent i = new Intent(SelectDoor.this, Main2Activity.class);
                       // Bundle bn = new Bundle();

                       // bn.putParcelable("User", user);
                       // i.putExtras(bn);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                   //     if (homeTimer != null)
                     //       homeTimer.cancel();

                     //   homeTimer = null;
                    finish();
                    //finishAffinity();
                }
            },5000);
        }
    }




    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "notifyError: " + error);
        MyProgressDialog.dismiss();
    }



    public void startAlert(String data,String ip) {






        int i = Integer.parseInt(count.getText().toString());


       // alarmService.schedule(System.currentTimeMillis()+60000*i,this,data,ip);



       // cal.add(Calendar.MINUTE, i);
       /* Intent intent = new Intent(this, MyBroadcastReceiver.class);
        intent.putExtra("name","ramesh"+i);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 55555, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (i * 10000), pendingIntent);
        Toast.makeText(this, "Alarm set in " + i + " seconds",Toast.LENGTH_LONG).show();

*/
       /* AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent(this, MyBroadcastReceiver.class);
        notificationIntent.putExtra("name","ramesh"+i);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, i);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                cal.getTimeInMillis(),
                broadcast);*/



    }

    @Override
    protected void onPause() {
        super.onPause();
        //countDownTimer.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //countDownTimer.cancel();
    }


   /* private SSLSocketFactory getSSLSocketFactory()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = getResources().openRawResource(R.raw.my_cert); // this cert file stored in \app\src\main\res\raw folder path

        Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, wrappedTrustManagers, null);

        return sslContext.getSocketFactory();
    }*/
   @Override
   public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);

       getWindow().getDecorView().setSystemUiVisibility(
               View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                       | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                       | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                       | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                       | View.SYSTEM_UI_FLAG_FULLSCREEN
                       | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

   }
   private void UpdateTaskStatus() {

       Log.d(TAG,"Update Task Status ");

       JSONObject jsonObject = new JSONObject();
       try {
         //  JSONObject jsonObject = new JSONObject();
           Date currentTime = Calendar.getInstance().getTime();
           Log.d(TAG, "onPostExecute: " + currentTime.toString());
           SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
           sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
           String currentDateandTime = sdf.format(new Date());

           jsonObject.put("ticket_id", allTasks.get_id());
           jsonObject.put("status", 1);
           jsonObject.put("update_time", currentDateandTime);
           jsonObject.put("userId", user.getUserId());
           jsonObject.put("status_name", "open");
           jsonObject.put("uid", allTasks.getUid());
           jsonObject.put("username", user.getUsername());
       } catch (JSONException e) {
           e.printStackTrace();
       }

       MyProgressDialog.show(SelectDoor.this, R.string.wait_message);
       Log.d(TAG,"Starting sending TaskStatus "+TASK_STATUS);
       Log.d(TAG,"Buffer is "+jsonObject);
       new VolleyService(SelectDoor.this, SelectDoor.this).tokenBase(PUT, GlobalVariables.SSOIPAddress +TASK_STATUS, jsonObject, "101");
       Log.d(TAG,"Send ");

   }

}