package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static lumidigm.constants.Constants.SSO_CHECK_USER_ID_ACTIVE_IN_LDAP;

public class AlreadyExituserActivity extends Activity implements View.OnClickListener {


    private TextView msg;
    private LinearLayout centerImage;
    private ImageView imageview, imageView1;
    private TextView hint;
    private TextView  text_msg1;
    private ScrollView scollView;
    private TextView textMsg1;
    Button continue1, startenrolment, reenter;
    String id;
    User user;
    boolean check;
    String TAG="AlreadyExitUser";
    SharedPreferences prefs= null;
    String ldapUser = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.already_exituser);
        try {
            id = getIntent().getExtras().getString("userid");

            check = getIntent().getBooleanExtra("check", false);
            ldapUser = getIntent().getExtras().getString("ldapUser");

            user = getIntent().getExtras().getParcelable("userData");
        } catch (Exception obj) {

        }
        msg = (TextView) findViewById(R.id.msg);
        if (user != null)
        msg.setText("Employee #" + id + " " + user.getFullname());
        else
            msg.setText("Employee #" + id);

        View overlay = findViewById(R.id.mylayout);

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

//        prefs = getBaseContext().getSharedPreferences(
  //              "FaceDetection", MODE_PRIVATE);

        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

        centerImage = (LinearLayout) findViewById(R.id.center_image);
        imageview = (ImageView) findViewById(R.id.imageview);
        imageView1 = (ImageView) findViewById(R.id.imageview1);
        hint = (TextView) findViewById(R.id.hint);
        //text_msg1 = (TextView) findViewById(R.id.text_msg1);
        scollView = (ScrollView) findViewById(R.id.scoll_view);
        textMsg1 = (TextView) findViewById(R.id.text_msg1);
        findViewById(R.id.cancel).setOnClickListener(this);
        reenter = findViewById(R.id.re_enter_finger);
        reenter.setOnClickListener(this);
        continue1 = findViewById(R.id.continue1);
        continue1.setOnClickListener(this);
        startenrolment = findViewById(R.id.start_enrolment);
        startenrolment.setOnClickListener(this);

        if (check) {
            imageview.setImageResource(R.drawable.already_user);
            hint.setText("Already enrolled for fingerprint recognition");


        } else {
            imageview.setImageResource(R.drawable.scan_error);


            String text = "<font color=#d50c20>" + "Not" + "</font>" + " currently enrolled for fingerprint recognition";
            hint.setText(Html.fromHtml(text));

            try {
                reenter.setText("Enroll Fingers");
            } catch (Exception obj) {

            }
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                onBackPressed();
                //TODO implement
                break;
            case R.id.re_enter_finger:
                Log.d(TAG,"Enter Finger");
                //TODO implement
                reenter.setVisibility(View.GONE);
                continue1.setVisibility(View.VISIBLE);
                scollView.setVisibility(View.VISIBLE);
                centerImage.setVisibility(View.GONE);
                startenrolment.setVisibility(View.GONE);
                /*Intent intent = new Intent(this, FingerPrintEnrollActivity.class);

                intent.putExtra("userid", id);

                startActivity(intent);*/
                break;
            case R.id.continue1:
                reenter.setVisibility(View.GONE);
                continue1.setVisibility(View.GONE);
                scollView.setVisibility(View.GONE);
                centerImage.setVisibility(View.VISIBLE);
                startenrolment.setVisibility(View.VISIBLE);
                imageView1.setVisibility(View.VISIBLE);
                imageView1.setImageResource(R.drawable.start_enrole_hint);
                imageview.setVisibility(View.GONE);
                String text = "<font color=#0000FF>" + "To enrol, you will be asked to scan and remove each index finger 3 times" + "</font>";
                //String text = "<font color=#d50c20>" + "To enrol, you will be asked to scan and remove each index finger 3 times" + "</font>";
                //text_msg1.setText(Html.fromHtml(text));
               hint.setText(Html.fromHtml(text));

               Log.d(TAG,"Continue");
                break;

            case R.id.start_enrolment:

                Log.d("AlreadyUser"," Start Enrolment "+GlobalVariables.MobileDevice);

              //  CheckUserIdInLDAP(id);
                if (GlobalVariables.MobileDevice) {
                    Log.d("Already User","Going for the Remote Enrolment");
                    SendingMessageWeb("fingerenroll","enrollment",id,ldapUser);
                } else {
                    Intent intent = new Intent(this, FingerPrintEnrollActivity.class);

                    intent.putExtra("userid", id);
                    if (ldapUser != null)
                    intent.putExtra("ldapUser", ldapUser);
                    else {
                        Bundle bn = new Bundle();
                        if (user != null)
                            bn.putParcelable("userData", user);
                        intent.putExtras(bn);
                    }
                    startActivity(intent);
                }
                break;
        }
    }
    private boolean SendingMessageWeb(String status, String requestType,String userID, String ldapuser) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;


        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }


        Log.d(TAG,"Already Exit Getting IP Address "+status+"  "+userID);
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        //String url="http://"+remoteIP+":8080/?username="+status;

        //String url="http://"+remoteIP+":8080/?username="+status+"&state="+requestType+"&tokenID="+userID+"&ldapUser="+ldapuser;
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+requestType+"&tokenID="+userID+"&ldapUser="+ldapuser;
        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: Already Exit User"+requestType+" Resoibse us"+response);
                Intent intent = new Intent(getApplicationContext(), FingerPrintEnrollmentInProgress.class);

                intent.putExtra("userid", id);
                ;

                if (ldapUser != null)
                intent.putExtra("ldapUser", ldapUser);

                if (user != null) {
                    Bundle bn = new Bundle();
                    bn.putParcelable("userData", user);
                    intent.putExtras(bn);
                }
                startActivity(intent);
                //finish();
            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "AlreadyExit User notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "Already Exit User notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //finish();
                    Intent intent = new Intent(getApplicationContext(), FingerPrintEnrollmentInProgress.class);

                    intent.putExtra("userid", id);

                    if (ldapUser != null)
                        intent.putExtra("ldapUser", ldapUser);

                    if (user != null) {
                        ;
                        Bundle bn = new Bundle();
                        bn.putParcelable("userData", user);
                        intent.putExtras(bn);
                    }
                    startActivity(intent);
                    finish();
                    //           playAudioFile(2,filePath);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

        return true;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.toString());
        }
        return false;
    }
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
    //}
}