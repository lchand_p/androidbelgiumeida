package lumidigm.captureandmatch.activites;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.luxand.facerecognition.MainActivity_FaceandFinger;

import Led.DeviceManager;
import belgiumeida.FacePhotoReadProgress;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static webserver.GlobalVariables.EnrollCount;
import static webserver.GlobalVariables.FaceandFinger;
import static webserver.GlobalVariables.ledSupport;
import static webserver.GlobalVariables.productVersion;

public class NotAuthorizedActivity extends Activity  {

    private LinearLayout mainlayut;
    private ImageView imageHint;
    private TextView welcomeText1;
    private boolean fromSuperVisior=false;
    private String TAG="NotAuthorized";

    private SharedPreferences prefs=null;

    private DeviceManager ledStatus=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG,"ID Num"+GlobalVariables.IDNumber+" Count is "+GlobalVariables.FaceCardReadCount);

        /*if (FaceICA == true && GlobalVariables.IDNumber == null && GlobalVariables.FaceCardReadCount == 1) {
            GotoFaceInProgress();
        } else */{
            setContentView(R.layout.not_authorized);
            User user = null;
            try {
                user = getIntent().getExtras().getParcelable("userData");

            } catch (Exception obj) {

            }

            try {
                Log.d("NotAuthorized", "Before PlayBeep");

                // playBeep();
                Log.d("NotAUth", "on Create ");
                View decorView = getWindow().getDecorView();
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

            } catch (Exception obj) {
                Log.d("NotAUth", "Got Exception while setting Full Screen");
            }
            try {
                fromSuperVisior = getIntent().getExtras().getBoolean("fromsupervisor");
                Log.d("NotAUthorized ", "SuperVisor status is " + fromSuperVisior);
            } catch (Exception obj) {
                fromSuperVisior = false;
            }

            TextView textView = (TextView) findViewById(R.id.welcome);
            mainlayut = (LinearLayout) findViewById(R.id.mainlayut);
            imageHint = (ImageView) findViewById(R.id.image_hint);
            welcomeText1 = (TextView) findViewById(R.id.welcome_text1);
            //View overlay = findViewById(R.id.mylayout);
/*
        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
            int attempts = 2 - EnrollCount;

            Log.d("NotAUthorized", "SUpervisior status is " + fromSuperVisior);

            if (!fromSuperVisior) {
                welcomeText1.setText("You do not have Authorization \n" +
                        "to access this door");
                try {
                    //Only SHow for the V0 Version
                    Log.d(TAG, "Checking productVersion " + productVersion);
                    if (ledSupport == true) {
                        if (GlobalVariables.MobileDevice == false) {
                            ledStatus = new DeviceManager();
                            /* set Pin Direction
                             * @param PIN pin number from GPIO map
                             * @param Direction in : for input , out : for output
                             */
                            // public static void setPinDir(@NonNull String PIN,@NonNull EnPinDirection Direction){

                            // EnPinDirection Direction= new ("out");
                            Log.d(TAG, "Set Pin DIrection");
                            ledStatus.setPinDir("18", "out");
                            Log.d(TAG, "Set Pin Output ");
                            ledStatus.setPinOn("18");
                            Log.d(TAG, "Set Pin Out complete");
                        }
                    } else
                        Log.d(TAG, "Product versio nut supported ");
                } catch (Exception obj) {
                    Log.d(TAG, "Got Crashed while enabling ");
                }

            } else
                welcomeText1.setText("You do not have Authorization\n" +
                        "to enroll any user.");

            //imageHint.setImageResource(R.drawable.notasks);

            Log.d("NotAUthorized", "Before User setting");
    /*    if (user != null)
            textView.setText("Welcome "+user.getFullname());
        else if (fromSuperVisior)
            textView.setText("UnAuthorized Access for enrollment");
        else
            textView.setText("UnAuthorized Access");
*/

            prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                    MODE_PRIVATE);

            Log.d("NotAuth", "Going for Handler");

        /*try {
            SendingMessageWithStateWeb("fingerfailure","101","fingerfailure");
        } catch (Exception obj) {
            Log.d(TAG,"It is exception");
        }*/

            Integer timeout = 1000;
            if (productVersion == 1) {
                Log.d(TAG, "It is V0 version ");
                timeout = 1500; //200;
            } else if (productVersion == 2) {
                // timeout=200;
                timeout = 3000;
            }
            {
                Handler mHandler = new Handler();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("NotAuthorized", "Enrollment Count is " + EnrollCount);
                /*if (fromSuperVisior && EnrollCount<4) {
                    Log.d("Not AUthroized ","Calling SuperVisior Activity");
                    Intent i = new Intent(NotAuthorizedActivity.this, SupervisiorIdentity2Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                } else*/
                        {
                            //Only SHow for the V0 Version
                            if (ledSupport == true) {

                                if (ledStatus != null) {
                                    ledStatus.setPinOff("18");
                                    Log.d(TAG, "RED LED is ON");
                                } else
                                    Log.d(TAG, "led status is null");
                            } else
                                Log.d(TAG, "Led is disabled ");
                            if (GlobalVariables.comeOut > 0) {
                                Log.d(TAG, "Comout is zero ");
                                finishAffinity();

                            } else {
                                // Intent i = new Intent(NotAuthorizedActivity.this, Main2Activity.class);
                                Intent i = new Intent(NotAuthorizedActivity.this, Main2Activity.class);

                                if (FaceandFinger == true)
                                    i = new Intent(NotAuthorizedActivity.this, MainActivity_FaceandFinger.class);


                                //  Intent i = new Intent(NotAuthorizedActivity.this, LaunchActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                                //finishAffinity();
                            }
                        }
                    }
                }, timeout); //5000
            }
        }
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }
    public void playBeep() {
        MediaPlayer m = new MediaPlayer();
        try {
            if (m.isPlaying()) {
                m.stop();
                m.release();
                m = new MediaPlayer();
            }

            AssetFileDescriptor descriptor = getAssets().openFd( "shutter.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            m.prepare();
            m.setVolume(1f, 1f);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean SendingMessageWithStateWeb(String status, String requestType, String state) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;

        if (GlobalVariables.MobileDevice)
            return false;

        if (GlobalVariables.AppSupport != 1)
            return false;

  /*      if (!isEthernetConnected()) {
            Log.d(TAG,"SendingMEssageWithStats Not sending as it is Wifi");
        }*/

        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
     /*   if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }
*/

        Log.d(TAG,"Getting IP Address");
        //   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+state;

        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: 123"+requestType+" Resoibse us"+response);

            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: 123 "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);
        return true;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    public void GotoFaceInProgress(){
        Intent intent = new Intent(this, FacePhotoReadProgress.class);
        startActivity(intent);
        finish();

//        finish();
    }
}
