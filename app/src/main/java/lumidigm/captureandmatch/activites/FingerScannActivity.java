package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import database.AppDataBase;
import database.AppDatabase;
import entity.Fingerentity;
import lumidigm.HomeActivity;
import lumidigm.HomeEnrollActivity;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.constants.Constants;

import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.vcomdroid.ANSI378TemplateHelper;
import lumidigm.vcomdroid.Minutiae;
import lumidigm.vcomdroid.VCOM;
import webserver.GlobalVariables;

import static com.android.volley.Request.Method.GET;
import static lumidigm.constants.Constants.CHECK_USER;
import static lumidigm.constants.Constants.GET_ALL_USERS;
import static lumidigm.constants.Constants.GET_ASSIGNED_TASKS;
import static lumidigm.constants.Constants.SERVER_IPADDR;
import static lumidigm.constants.Constants.SSO_CHECK_USER_ID_ACTIVE_IN_LDAP;
import static lumidigm.constants.Constants.SSO_SERVER_CHECK_USER;

/**
 * Created by colors on 8/10/18.
 */

public class FingerScannActivity extends Activity implements IResult  {

    private static final String TAG = "[VCOM]";
    private TextView msg;
    private TextView submsg;

    private ImageView imageCard, tick;
    // VCOM Java/JNI adapter.
    public VCOM mVCOM=null;
    // fingerprint images.
    private Bitmap mBitmapCapture;
    private Bitmap mBitmapMatch;
    // fingerprint ANSI 378 templates.
    private byte[] mTmplCapture;
    private byte[] mTmplMatch;
    // AppDataBase database;
    //
    private int MaxFingerprintAttempts = 3;
    private int currFingerPrintCount = 1;
    /*   String btesv = "Rk1SACAyMAADbgA1ABcAAAEYAWAAxQDFAQAAAAApgKQAHrANgJIAIGINgEEALXAIQHQAKwgPgE0AMnEMQOYAMk8PgLMAO68CgOoAPVIPgIEAQGoPgHEAWg8PgJ4AY2UPQKIAaK4PgEYAdHsFQNEAhp0PgEUAkCQGQIEAnn4LgEkApIENQKcAoaELgKIAuZMNQFgAxYIHQM4AxZkPgLwA1ZkPQHgA5o0KQPkA8kEOQIkA+pgHQN0A+UAPgFcA/I0PgMsBCEMPQGIBDJUKgOYBC0YPgEcBEIcPQJgBET8LgHUBFpYEQJQBHEMMQK8BHEcPgPABJlEPgHMBLT4GgNoBL1APQFIBMHwKgIIBNEYHQNABNVQPAlgBAQJYSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgPAECOhx9BABeFG1tBgCnHwZUwAkAdy/0QUT/BwBvMGnBfMEDAEs1YMARAO8tlsHD/sPB/4aLwXsIADNB5FfALwYAf0FrwnQEAPxNl8P/DQBzWen/Lj7/LwQA/FeafgoApWYDKzVCDgCda3DCwonBwcL/wYUIEHMLIo79jgoApW0D/Ub+/0UEALNzgJsKAIOLXMN8wv+IBABFd1N1BgBHflBtwQUAzIOTw5gHANSFHC9bBwDSiiJEchUBAH2iwsCEwMKWwMTBwMRf/wsApJb3+vxE/20SAC6E18D/wTX//Uc1RwYAfp1PwsCDDwAzjtD/Q/47/zf9DwCfnleexP9wxP/CWwcAg59J/5LBBACsoRw4HAD+hqLC/5DAm8HBxP/Dal3DwGf/DwCIp9P//vz7/f3+wP7B/8LAFAEEmqeEwIfDwMXAmYnACwCurCA3ZcGLCQCnrTrDwV98EACJr0bCiGrBw3KZBwCrsSf/wcFVCQCCtUOIwXELAKO7MXR0wn8FANHFJFgFAFrIQ3sHAM3IKcHA/5YGAD7PRnT/BQBEz0NrCAC+2CnBhXoIAFTgQ8HAwsA4BQB86TeOGwED4qRXcsLCxMfDw8LBw8HCdMLC/sX+CgD78KRYWYsGAFf/N8HAwcgKEMsInv9ANP0DEGEPNMIDEEESPf8ZADPjw/3BU1L8wMD/KivANf8EEEgUPf/9BhB2GCTDwsDBBRCbHBeFERBzHrFe/1P//zXAIQQQtSAQwP8DEPkyff8=";
       //String btesv2 = "Rk1SACAyMAADbgA1ABcAAAEYAWAAxQDFAQAAAAApgKQAHrANgJIAIGINgEEALXAIQHQAKwgPgE0AMnEMQOYAMk8PgLMAO68CgOoAPVIPgIEAQGoPgHEAWg8PgJ4AY2UPQKIAaK4PgEYAdHsFQNEAhp0PgEUAkCQGQIEAnn4LgEkApIENQKcAoaELgKIAuZMNQFgAxYIHQM4AxZkPgLwA1ZkPQHgA5o0KQPkA8kEOQIkA+pgHQN0A+UAPgFcA/I0PgMsBCEMPQGIBDJUKgOYBC0YPgEcBEIcPQJgBET8LgHUBFpYEQJQBHEMMQK8BHEcPgPABJlEPgHMBLT4GgNoBL1APQFIBMHwKgIIBNEYHQNABNVQPAlgBAQJYSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgPAECOhx9BABeFG1tBgCnHwZUwAkAdy/0QUT/BwBvMGnBfMEDAEs1YMARAO8tlsHD/sPB/4aLwXsIADNB5FfALwYAf0FrwnQEAPxNl8P/DQBzWen/Lj7/LwQA/FeafgoApWYDKzVCDgCda3DCwonBwcL/wYUIEHMLIo79jgoApW0D/Ub+/0UEALNzgJsKAIOLXMN8wv+IBABFd1N1BgBHflBtwQUAzIOTw5gHANSFHC9bBwDSiiJEchUBAH2iwsCEwMKWwMTBwMRf/wsApJb3+vxE/20SAC6E18D/wTX//Uc1RwYAfp1PwsCDDwAzjtD/Q/47/zf9DwCfnleexP9wxP/CWwcAg59J/5LBBACsoRw4HAD+hqLC/5DAm8HBxP/Dal3DwGf/DwCIp9P//vz7/f3+wP7B/8LAFAEEmqeEwIfDwMXAmYnACwCurCA3ZcGLCQCnrTrDwV98EACJr0bCiGrBw3KZBwCrsSf/wcFVCQCCtUOIwXELAKO7MXR0wn8FANHFJFgFAFrIQ3sHAM3IKcHA/5YGAD7PRnT/BQBEz0NrCAC+2CnBhXoIAFTgQ8HAwsA4BQB86TeOGwED4qRXcsLCxMfDw8LBw8HCdMLC/sX+CgD78KRYWYsGAFf/N8HAwcgKEMsInv9ANP0DEGEPNMIDEEESPf8ZADPjw/3BU1L8wMD/KivANf8EEEgUPf/9BhB2GCTDwsDBBRCbHBeFERBzHrFe/1P//zXAIQQQtSAQwP8DEPkyff8=";
       String btesv3 = "Rk1SACAyMAADZAA1ABcAAAEYAWAAxQDFAQAAAAAtQKcAG6APQMoAHKAPQD0ALq0LQOoAMq4LgI8AMk8PgE8AR64GQGwASVUOQPQATDABQJEAU5kPQPUAVyQBQOYAYzMNQIUAZksNgDQAazMOQGUAcqEFgMgAdTEPQI0AdkMJgLQAejcPgGQAgZALQOsAfiAPgHAAhJkHQEcAjDMPgKMAiy4HQHcAk5sHQK8Alm0HQF0AlDEPQOwAlR0PQJIAnE8BgLMAniAJgJMApQ8BQDkAqDkPQLYArhMHQNYAsxYPgKMAtg4BQLcAul8HQIgAu68PQOUAwBEPQJEAzLMIQKsA2gcLgE0A6Z4PQLIA+QEPQCQBCJsOQJkBHlIPgGQBIKYPQGABKUYPQJoBN64PAjYBAQI2SUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgOwECGBlKCQCqEhbA/v9P/BAAgBIM/kDA//7+/v3//v/9BQDMEhZnDQBeEwP+QUwu/xEAShgGwD3+/sH//Tj9/P8IAKsaGsA+JgcAPh0MwkbBDgCoHxzA/zv+/D0kBQDLICDAwMAPADwkDHv+wP/+MP79/gwAPy0Jwf/AO8AqBwA+MhPDwP83BgAvNBbBw8H+GQA0SDT//cOFhMF4kMGJZwYAQ0gTwcMrBwBzSxP+MfwZACZVNFxVw8B8ccHDdf94BAEDWVdoBAD/cVNOBwCVUx7+/v4YBQCQViT/JgQAQVg9cwQAXVoJMwMA9F5QwAYAH18wUsEEADRpt8DDBABXajAzBwDmak/C/i4EACB+MHgFAGhwF0cEAIl4l2gMAE56PcCdwmeWBQEAkmDASgUAZYM0wJUGAOmEUP/+MBYAHY8twcH/wMJq/3+IwMJmBgB8lS3GwsjDCgECnWL/wf7APv4GAHeXNLDFFQAYmylm/4P/csHBwsHBagYAX5srwsDCwAMBAqhnwQ4AGq8xwnb/aozBEgA8rinBeGd5/Zl6BADRt2k0BAC2uHQtBwEBv2n//1MEALe+cP39FAAWwCdzYMN8wcDDwMDAlQUA4MRr/lcKABPNJP/C/8DAgQYA/udx/8E1CQAT2inAcGcHAFHtHmd8CBAoCCLB/8LDTgQQIwstiQYQ6Rh0wP1dBBBnIhPAwgQQZiwQUg=";
       String btsv4 = "Rk1SACAyMAADRgA1ABcAAAEYAWAAxQDFAQAAAAAsQNEAICsKQCsAJrINQEkAKFYPQMUALDkOgGoA\n" +
               "L6APgDMAOK0LgKoARTUPQEEASKoMQDIASpUHQM0ASCUPgHcAS0kGQJQATD8KQD0AVZwHQHQAVzIG\n" +
               "gI0AYDgJQNMAYCIPQJoAanAJQFEAb5kGQF4AbqkHgCwAcDYOQH0AdFAJQKIAciUPgEoAdzgGgIAA\n" +
               "exMJQMgAfBsPgGUAfqkIgGEAgz8IgNMAhhYPQJMAjRMPQHYAlAENgIIApQQPgJ8AqgoPQKoAywcP\n" +
               "gEQAz6cPQJkA7FYPgGQA+6wPQGEBA08PQJ0BB7MPQLYBDlkPgFMBEaoPgJABIlQOQLcBJQQNgJ4B\n" +
               "LQAKQG8BL64PAh4BAQIeSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgNQECABiHCQCZER7//Ck7\n" +
               "BQCMEhr+KQsAqRQi/f/7wv00wAkAexEWLsD+IgUA6Q9WeAcA9Q9XdGwJAGkUEP/+MSIGAPcdVnvA\n" +
               "CwAkFv1G/sAp/ggA+ypTwkBNCgAiHQBPQUIGAE8qDP3+Vw4A/T1QwSlT//7AKwkAFC4GwGT9OgkA\n" +
               "xTNDwf1G/P8YABtAPf/EfsHBwcHBwcHBw//BxP/C/sLBBgA2OBDD/v3CBAAjRECHBQBDRglDBQD/\n" +
               "V1pUBwBBTBDAwP/AywoAME0t///EwXzCCgDNTUn/wP79//4pDQApTzD/wJbBwcHBmAMAlVJD+wMA\n" +
               "PVgwwBcAFWUrwVLE/sP/wcHCwMLCwMHD/sFvBQCNXrrEiQkA/mZX/v/A/cJLCQD9dlz/Nk8EAFdg\n" +
               "DP3CBgCba1P8+igVABaTIsB3asDAd4trwgUAn3de/hsGAMSAYjj+CAD8oWL+/17ADAAkkiR+dsP+\n" +
               "iRQAGqkaYsP+csL+lsJYwhMAG7ccW8NOwsDDwMDAcMIHAPyybcH+TxMAG8UXUXxvdHSNBwAb0R5t\n" +
               "fgcASdETwMCLBwAb3hrAwf+JBwDs7WQpQhEAI/AadMNRwP/Ew//AfgQA5f5tRAQAZ/wJYRAAKP0X\n" +
               "wMH+wYnBwv7FPgcQJQEewsD//8MHEGcFDMBcxAUQVRIQaQQQcS0GWQ==";
       String btsv5="Rk1SACAyMAACcgA1ABcAAAEYAWAAxQDFAQAAAAAYQM8AOq8MgN8AO1YLQFgARBEPQJQARQQPQLsA\n" +
               "UasPQNwAXqIPgI4AbQQPgNEAfJ4PQPYAf0UPgJUAggAPQE8AkXkPQJoAkq0PQGQAlxoPQJ8AoZsO\n" +
               "gM8ArpAPgJEAs3oHQDQAuSIPgJ8AvYMKQOYA3YsPQDoA4CcNgF0A6iIPQD4A7SwNQNIBK3oOQOoB\n" +
               "NYIKAcIBAQHCSUMCAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgKQEBpBhgAwB6EHDCBgCIFHDAhA0A\n" +
               "xhmDwIBxfsELANYkicL+xGfBfAsA2yyJb8L/wsCDDwDJOYnCiP+LwnPBCACWQvrAKTwMAI9IcMJ8\n" +
               "wViEBQBWSWB+CQCXSf1L/8A2EgDmOZPDwIvBwcF4w2b+CgC0UICGw3HBBwC8UAb/MP8GALtVDDDA\n" +
               "BADfXhc1CwCSbff+/i8r/wcA0n0cQVMVAPR/nnXBep3CwMLCRcDCEwCagv0kPf9DQME/EQCbkAD8\n" +
               "/SPA//9EMMEEAEySVmoHAFGVUGWPEABildMrLDs4OwgAk5VpwoeDEQBol9r//v7+/v7+/jI+/xAA\n" +
               "m5cD/PszPsD///3D/QcAYZ1TwHD/DgCbn3GeYv9kWQ4Ao58W/P3//kIrSg4Ao6UkJ1Q2Pg0Aoa4w\n" +
               "M/84V/8MAIyyWn7/ezcNAJGzT1hiwf1UDQCXs0Y/Yj5GCwCbw0nA/2BXwAsAocNAQErAOxgA5dW3\n" +
               "woHBwqfDw3jBwWjAbggA4909VFMHAOndN0ZSHBA7Edd3/1M4/v/+/v78/UY4wD8KENYlxsFadcA=";
       */
    // HandlerHelp mHandler;
    private String currState = null;
    private boolean fromWeb=false;
    private SharedPreferences prefs=null;
    private boolean comingfromFingerPrint = false;
    private MyCountDownTimer  myExitTimer=null;

    public boolean FirstUser=true;
    public static boolean check = true;
    public static boolean toastcheck = true;
    String userID=null;
    AppDatabase appDatabase;
    private BroadcastReceiver mUpdateReceiver;
    private IntentFilter mUpdateFilter;
    private boolean getDynamicContent = true;
    private boolean ldapSupport=true;
    private String ldapuser = null;

    public byte[] PrevCapture=new byte[1024*1024];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerscann);
        // if the device is plugged in, this will attempt to obtain permission and/or open the device.
        msg = (TextView) findViewById(R.id.msg);
        submsg = (TextView) findViewById(R.id.submsg);
        tick = (ImageView) findViewById(R.id.tick);
        imageCard = (ImageView) findViewById(R.id.imageCard);
        // Log.d(TAG, "onCreate: "+getIntent().getStringExtra("finger"));

        prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
                MODE_PRIVATE);
        GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

        appDatabase = AppDatabase.getAppDatabase(FingerScannActivity.this);

        Log.d(TAG,"Finger Print IP is "+SERVER_IPADDR);
        check = true;
        try {
            Log.d(TAG, "on Create ");
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while setting Full Screen");
        }

        try {
            Log.d(TAG, "Starting FingerScanActivity");
            if ((getIntent().getExtras()) != null) {
                mTmplCapture = Base64.decode((getIntent().getStringExtra("finger")), Base64.DEFAULT);
                comingfromFingerPrint = true;
            }  //Log.d(TAG, "mTmplCapture: "+ Arrays.toString(mTmplCapture));


        } catch (Exception tpexcep) {

        }
        try {
            fromWeb = getIntent().getBooleanExtra("FromWeb", false);
            currState = getIntent().getStringExtra("CurrState");
            userID = getIntent().getStringExtra("tokenID");
            Log.d(TAG, "Got from Web " + fromWeb+" Rxvd UserID "+userID);


        } catch (Exception obj) {
            currState = null;
            userID = null;
        }
        Log.d(TAG, "CurrState is " + currState);
        boolean test =true;
        GlobalVariables.FaceDetectionCompleteStatus = false;
        //DownloadUserData();
        if (currState != null && currState.equals("comeOut")) {
            GlobalVariables.SupportWebFaceDetectonInitated=false;
            Log.d(TAG,"Got ComingOut - GoBack to Home Activity");
            //Intent intent= new Intent(this, HomeActivity.class);
            Intent intent= new Intent(this, Main2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            this.startActivity(intent);
            finish();
        } else {
            currFingerPrintCount = 1;
            //GlobalVariables.personDescription = "not matching";
            //GlobalVariables.tokenID=0;
            //GlobalVariables.reqStatus = false;

            StartRemoteApp();

            String filePath = Environment.getExternalStorageDirectory() + "/displayImages/insert_your_finger_to_authenticate.mp3";
            playAudioFile(2, filePath);

            mVCOM = new VCOM(this);
            // mHandler = new HandlerHelp();
            new CaptureAndMatchTask().execute(0);
            //  database = AppDataBase.getAppDatabase(this);
            ImageView home = (ImageView) findViewById(R.id.home);
            if (myExitTimer == null) {

                myExitTimer = new MyCountDownTimer(45000, 5000);
                myExitTimer.start();
            }


            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
            try {
                if (currState != null) {
                    if (currState != null && currState.equals("fingernotmatch")) {
                        Log.d(TAG, "FingerNot Match");
                        //            fingerNotMatch();
                        // SendingMessageWithState("finger","101","fingernotmatch");
                        msg.setText("Finger not matched & Insert your finger");
                        submsg.setText("To authenticate with Emirates ID profile");

                        // imageCard.setImageResource(R.drawable.finger_first_success);
                        tick.setVisibility(View.GONE);

                    } else if (currState != null && currState.equals("fingerremove")) {
                        Log.d(TAG, "It is FInger Remove");
                        msg.setText("Remove your finger");
                        submsg.setText("Please wait while we authenticate your biometric details.");
                        imageCard.setImageResource(R.drawable.finger_remove1);
                        tick.setVisibility(View.GONE);

                        //mVCOM.Close();
                        //fingerNotMatchTest();
                    } else if (currState != null && currState.equals("fingersuccess")) {
                        Log.d(TAG, "Finger Success");
                        msg.setText("Congratulations");
                        submsg.setText("Your biometric sample has been validated successfully..");
                        imageCard.setImageResource(R.drawable.finger_first_success);
                        tick.setVisibility(View.VISIBLE);

                    }
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got Exception while Processing State");
            }
            // RegisterInterReceiver();
        }
    }

    public void fingerNotMatch() {
        Log.d(TAG,"It is Fingernotmatch");

        GlobalVariables.personDescription="not matching";
        //GlobalVariables.tokenID=0;
        GlobalVariables.reqStatus= true;




        SendingMessageWithState("finger","101","fingernotmatch");
        msg.setText("Finger not matched");
        // imageCard.setImageResource(R.drawable.finger_first_success);
        tick.setVisibility(View.GONE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //SendingMessageWithState("finger","101","fingersuccess");
                imageCard.setImageResource(R.drawable.finger_first_success);
                tick.setVisibility(View.GONE);
                String filePath = Environment.getExternalStorageDirectory() + "/displayImages/insert_your_finger_to_authenticate.mp3";
                playAudioFile(2, filePath);

                msg.setText("Insert your finger");
                submsg.setText("To authenticate with Emirates ID profile");
                new CaptureAndMatchTask().execute(0);

            }
        }, 4000);

    }



    public void ProcessfingerNotMatch() {
        Log.d(TAG,"It is fingerNotMatchProcess");

        GlobalVariables.personDescription="not matching";
        //GlobalVariables.tokenID=0;

        GlobalVariables.reqStatus= true;

        //Send event finger print not match only when Face detectionstatus if false
        //For Timebeing commentng sending Finger print not matched to remote party
        //if (GlobalVariables.FaceDetectionCompleteStatus == false)
        {
            // SendingMessageWithState("finger", "101", "fingernotmatch");
            msg.setText("Finger not matched");
            // imageCard.setImageResource(R.drawable.finger_first_success);
            tick.setVisibility(View.GONE);
        }
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //SendingMessageWithState("finger","101","fingersuccess");
                //imageCard.setImageResource(R.drawable.finger_first_success);
                //tick.setVisibility(View.GONE);
                if (GlobalVariables.FaceDetectionCompleteStatus == false)
                {
                    String filePath = Environment.getExternalStorageDirectory() + "/displayImages/finger_not_matched.mp3";
                    playAudioFile(2, filePath);
                }

                //  msg.setText("Insert your finger");
                //   submsg.setText("To authenticate with Emirates ID profile");
                //  new CaptureAndMatchTask().execute(0);


              /*  Intent i=new Intent(FingerScannActivity.this, FirstActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);*/
                onBackPressed();

            }
        }, 4000);

    }
    public void fingerNotMatchTest() {
        //SendingMessageWithState("finger","101","fingernotmatch");
        msg.setText("Finger not matched");
        // imageCard.setImageResource(R.drawable.finger_first_success);
        tick.setVisibility(View.GONE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                imageCard.setImageResource(R.drawable.finger_first_success);
                tick.setVisibility(View.GONE);
                msg.setText("Insert your finger");
                submsg.setText("To authenticate with Emirates ID profile");
                //      new CaptureAndMatchTask().execute(0);

            }
        }, 4000);

    }

    public void fingerMatched() {
        msg.setText("Finger matched");

        imageCard.setImageResource(R.drawable.card);
        tick.setVisibility(View.GONE);
        mVCOM.Close();
        String filePath = Environment.getExternalStorageDirectory() + "/displayImages/finger_matched_please_remove_your_card.mp3";
        playAudioFile(2, filePath);

        Toast.makeText(this, "Please remove your card", Toast.LENGTH_SHORT).show();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

              /*  Intent i=new Intent(FingerScannActivity.this, FirstActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);*/
                onBackPressed();
            }
        }, 4000);


    }

    public void fingerRemove(int Score) {

        //mBundle.putString("CurrState",currState);
        String filePath = Environment.getExternalStorageDirectory() + "/displayImages/remove_your_finger_and_please_wait.mp3";
        playAudioFile(2, filePath);

        Log.d(TAG,"It is FingerRemove"+Score);
        SendingMessageWithState("finger","101","fingerremove");
        msg.setText("Remove your finger");
        submsg.setText("Please wait while we authenticate your biometric details.");
        imageCard.setImageResource(R.drawable.finger_remove1);
        tick.setVisibility(View.GONE);

        if (Score > 50000 ) {
            GlobalVariables.personDescription="finger matching";
            GlobalVariables.reqStatus= true;
            Log.d(TAG,"Finger LOgged In User is "+GlobalVariables.loggedInUser);
        }
        mVCOM.Close();
        mVCOM=null;
        //  Toast.makeText(this, "Please remove your card", Toast.LENGTH_SHORT).show();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Score > 50000) {

                    tick.setVisibility(View.VISIBLE);

                  /* String filePath= Environment.getExternalStorageDirectory() +"/displayImages/success.mp3";
                    playAudioFile(1,filePath);
*/
                    //GlobalVariables.personDescription="finger matching";
                    //GlobalVariables.tokenID=0;
                    //GlobalVariables.reqStatus= true;

                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String filePath = Environment.getExternalStorageDirectory() + "/displayImages/congratulations_your_bio_metric_sample.mp3";
                            playAudioFile(1, filePath);
                        }
                    },1000);



                    SendingMessageWithState("finger","101","fingersuccess");
                    msg.setText("Congratulations");
                    submsg.setText("Your biometric sample has been validated successfully..");
                    imageCard.setImageResource(R.drawable.finger_first_success);
                    tick.setVisibility(View.VISIBLE);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                //onBackPressed();
                                TestBack();
                            } catch (Exception tes) {
                                Log.d(TAG,"Got onBackPressed Exception ");
                                finish();
                            }
                        }
                    }, 6000);//4000

                } else {
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/success.mp3";
                    //   playAudioFile(2,filePath);
                    Log.d(TAG,"Current Finger Print Count "+currFingerPrintCount);
                    if (currFingerPrintCount < MaxFingerprintAttempts) {
                        currFingerPrintCount++;
                        fingerNotMatch();
                    } else  {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    onBackPressed();
                                } catch (Exception objd) {
                                    Log.d(TAG,"Got OnBackException crash");
                                    finish();
                                }

                            }
                        }, 1000);//4000
                    }

                }
            }
        }, 4000);


    }


    private class CaptureAndMatchTask extends AsyncTask<Integer, Integer, lumidigm.captureandmatch.entity.User> {
        boolean FirstUser_Cap=true;
        @Override
        protected lumidigm.captureandmatch.entity.User doInBackground(Integer... i) {
            try {
                if (check) {

                    int rc;
                    Log.d(TAG, "in CaputreAndMatchTask");
                    rc = mVCOM.Open();
                    if (rc == 0) {

                        rc = mVCOM.Capture();

                        if (rc == 0) {
                            int tmplSize = mVCOM.GetTemplateSize();
                            if (tmplSize > 0) {
                                byte[] tmpl = new byte[tmplSize];
                                rc = mVCOM.GetTemplate(tmpl);
                                if (rc == 0) {
                                    //  playBeep();
                                    Log.d(TAG, "doInBackground: toastcheck" + toastcheck);
                                    toastcheck = true;

                                    List<lumidigm.captureandmatch.entity.User> users = appDatabase.userDao().getAll();

                                    Log.d(TAG,"Number of Users is "+users.size());

                                    int userCount=0;
                                    for (lumidigm.captureandmatch.entity.User user : users) {
                                        userCount++;
                                        Log.d(TAG,"For User "+userCount);
                                        Log.d(TAG, "doInBackground: " + user.toString());
                                        String fingarprint1 = user.getTemplate1();
                                        byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                        String fingarprint2 = user.getTemplate2();
                                        byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

                                        String fingarprint3 = user.getTemplate3();
                                        byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);


                                        rc = mVCOM.Match(tmpl, mTmplCapture1);
                                        int score1 = mVCOM.GetMatchScore();
                                        rc = mVCOM.Match(tmpl, mTmplCapture2);
                                        int score2 = mVCOM.GetMatchScore();
                                        rc = mVCOM.Match(tmpl, mTmplCapture3);
                                        int score3 = mVCOM.GetMatchScore();
                                        if (((score1 + score2 + score3) / 3) > GlobalVariables.MaxThresholdVal) {
                                            //Chand Added 13Oct208 for fixing TrueID issue
                                            //	mVCOM.Close();
                                            //	mVCOM = null;
                                            //above Chand added freshly
                                            Log.d(TAG,"MainActivity return user CaptureAndMatchTask");
                                            return user;
                                        } //else if (PrevCapture == null) {


                                    }
                                    PrevCapture = tmpl;
                                    Log.d(TAG,"Prev Capture  dones "+PrevCapture.toString());

                                } else {
                                    toastcheck = false;
                                }

                            }
                            /*

                             */
                        } else {
                            toastcheck = false;
                        }
                        //	if (mVCOM !=null)
                        Log.d(TAG,"MainActivity on close VCOM CaptureAndMatchTask ");
                        mVCOM.Close();

                        mVCOM=null;
                    } else {
                        toastcheck = false;
                    }
                }
            } catch (Exception obj) {

                try {
                    Log.d(TAG,"Got Exception while capture and match task"+obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    mVCOM = null;
                } catch (Exception obj2) {
                    mVCOM = null;
                }

                //return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(lumidigm.captureandmatch.entity.User user) {
            //  new CaptureAndMatchTask().execute(0);
           /* getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
            if (user != null) {

                //Main2Activity.this.user = user;
                Log.d(TAG,"Main Activity onPostExecute"+user.getUserId()+"  Status is "+user.getStatus()+"test "+user.getUsername()) ;
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                // MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                //new VolleyService(Main2Activity.this, Main2Activity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id(), null, "105");
                //SendtoSSOServer(SSO_SERVER_CHECK_USER , user.getUserId(),"102");
                //IResult mResultCallback = null;
                //new VolleyService(mResultCallback, FingerScannActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + SSO_SERVER_CHECK_USER + user.getUserId(), null, "201");
                //mBitmapMatch = GetBitmap();

                //For LDAP Check whether this User Exists in LDAP, if not exists - it is failure case
                if (!ldapSupport) {

                    Log.d(TAG, "First User Status " + FirstUser_Cap);
                    GlobalVariables.FirstUser = FirstUser_Cap;
                    GlobalVariables.loggedInUser = user.getUsername();
                    Log.d(TAG, "First User is " + GlobalVariables.loggedInUser + "  " + user.getFullname());
                    fingerRemove(60000);
                } else {
                    //WHen the User is available in LDAP, Accept the FingerPrint
                    //If not available, even it is success, discard and say reject

                    CheckWetherLDAPUser(user.getUserId());
                }
            } else {
                if (check) {

                    if (toastcheck) {


                        //textView.setText("Your fingerprint could not be read or User does not exist");

                        //    check=false;
                        check =true;

                        // Toast.makeText(Main2Activity.this, "Finger not matched", Toast.LENGTH_SHORT).show();
                    }else {



                        // Toast.makeText(Main2Activity.this, "Place your finger on the sensor", Toast.LENGTH_SHORT).show();
                    }

                    /*
                    When Finger Print is matching, there is a case that User might have enrolled from remote device.
                    Now download the content and check the whether it is matching
                     */
                    if (getDynamicContent) {
                        //Save the Current Finger Print & Download the Content Once again
                        Log.d(TAG,"Dynamic Content is true - so downloading the content");
                        MyProgressDialog.show(FingerScannActivity.this, R.string.wait_message);
                        //   new VolleyService(this, getBaseContext()).tokenBase(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS , null, "101");
                        //    new VolleyService(FingerScannActivity.this, FingerScannActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS , null, "501");
                        DownloadUserData();
//                        new VolleyService(getApplicationContext(), FingerScannActivity.class).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
                        // SendGetAllUsers();
                    } else
                    if (mVCOM != null) {
                        Handler handler = new Handler();
                        Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                new CaptureAndMatchTask().execute(0);
                            }
                        }, 1000);
                    } else {
                        ProcessfingerNotMatch();
                        //Play Voice Promt of fAILURE AND COMEE OUT

                    }

                }
            }
        }
    }

    private class CheckPreviousCaptureFingerMatchTask extends AsyncTask<Integer, Integer, lumidigm.captureandmatch.entity.User> {
        boolean FirstUser_Cap=true;
        @Override
        protected lumidigm.captureandmatch.entity.User doInBackground(Integer... i) {
            try {
                if (check) {

                    int rc;
                    Log.d(TAG, "in CheckPreviousCaptureFingerMatchTask");
                    rc = mVCOM.Open();
                    if (rc == 0) {
                        // rc = mVCOM.Capture();
                        //if (rc == 0)
                        {
                            int tmplSize = 10 ;//mVCOM.GetTemplateSize();
                            //if (PrevCapture != null)
                            {
                                Log.d(TAG,"Prevous capture ");
                                byte[] tmpl = PrevCapture; //new byte[tmplSize];
                                Log.d(TAG,"Prev Capture comapre is "+tmpl.toString());
                                //rc = mVCOM.GetTemplate(tmpl);
                                if (rc == 0) {
                                    //  playBeep();
                                    Log.d(TAG, "doInBackground: toastcheck" + toastcheck);
                                    toastcheck = true;

                                    List<lumidigm.captureandmatch.entity.User> users = appDatabase.userDao().getAll();

                                    Log.d(TAG,"Number of Users is "+users.size());

                                    int userCount=0;
                                    for (lumidigm.captureandmatch.entity.User user : users) {
                                        userCount++;
                                        Log.d(TAG,"For User "+userCount);
                                        Log.d(TAG, "doInBackground: " + user.toString());
                                        String fingarprint1 = user.getTemplate1();
                                        byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);

                                        String fingarprint2 = user.getTemplate2();
                                        byte[] mTmplCapture2 = Base64.decode(fingarprint2, Base64.DEFAULT);

                                        String fingarprint3 = user.getTemplate3();
                                        byte[] mTmplCapture3 = Base64.decode(fingarprint3, Base64.DEFAULT);


                                        rc = mVCOM.Match(tmpl, mTmplCapture1);
                                        int score1 = mVCOM.GetMatchScore();
                                        rc = mVCOM.Match(tmpl, mTmplCapture2);
                                        int score2 = mVCOM.GetMatchScore();
                                        rc = mVCOM.Match(tmpl, mTmplCapture3);
                                        int score3 = mVCOM.GetMatchScore();
                                        if (((score1 + score2 + score3) / 3) > GlobalVariables.MaxThresholdVal) {
                                            //Chand Added 13Oct208 for fixing TrueID issue
                                            //	mVCOM.Close();
                                            //	mVCOM = null;
                                            //above Chand added freshly
                                            Log.d(TAG,"MainActivity return user CaptureAndMatchTask");
                                            return user;
                                        }

                                    }


                                } else {
                                    toastcheck = false;
                                }

                            }
                            /*

                             */
                        }
                        //	if (mVCOM !=null)
                        Log.d(TAG,"MainActivity on close VCOM CaptureAndMatchTask ");
                        mVCOM.Close();

                        mVCOM=null;
                    } else {
                        toastcheck = false;
                    }
                }
            } catch (Exception obj) {

                try {
                    Log.d(TAG,"Got Exception while capture and match task"+obj.getMessage());
                    if (mVCOM != null)
                        mVCOM.Close();
                    mVCOM = null;
                } catch (Exception obj2) {
                    mVCOM = null;
                }

                //return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(lumidigm.captureandmatch.entity.User user) {
            //  new CaptureAndMatchTask().execute(0);
           /* getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
            if (user != null) {

                //Main2Activity.this.user = user;
                Log.d(TAG,"Main Activity onPostExecute"+user.getUserId()+"  Status is "+user.getStatus()+"test "+user.getUsername()) ;
                // if (user.getStatus().equals("offline") || user.getStatus().equals("")) {
                // MyProgressDialog.show(Main2Activity.this, R.string.wait_message);
                //new VolleyService(Main2Activity.this, Main2Activity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ASSIGNED_TASKS + user.get_id(), null, "105");
                //SendtoSSOServer(SSO_SERVER_CHECK_USER , user.getUserId(),"102");
                //IResult mResultCallback = null;
                //new VolleyService(mResultCallback, FingerScannActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + SSO_SERVER_CHECK_USER + user.getUserId(), null, "201");
                //mBitmapMatch = GetBitmap();

                //For LDAP Check whether this User Exists in LDAP, if not exists - it is failure case
                if (!ldapSupport) {

                    Log.d(TAG, "First User Status " + FirstUser_Cap);
                    GlobalVariables.FirstUser = FirstUser_Cap;
                    GlobalVariables.loggedInUser = user.getUsername();
                    Log.d(TAG, "First User is " + GlobalVariables.loggedInUser + "  " + user.getFullname());
                    fingerRemove(60000);
                } else {
                    CheckWetherLDAPUser(user.getUserId());
                }
            } else {
                if (check) {

                    if (toastcheck) {


                        //textView.setText("Your fingerprint could not be read or User does not exist");

                        //    check=false;
                        check =true;

                        // Toast.makeText(Main2Activity.this, "Finger not matched", Toast.LENGTH_SHORT).show();
                    }else {



                        // Toast.makeText(Main2Activity.this, "Place your finger on the sensor", Toast.LENGTH_SHORT).show();
                    }

                    /*
                    When Finger Print is matching, there is a case that User might have enrolled from remote device.
                    Now download the content and check the whether it is matching
                     */

                    if (mVCOM != null) {
                        Handler handler = new Handler();
                        Log.d(TAG, "Starting CaptureNad match Task going for sleep");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                new CaptureAndMatchTask().execute(0);
                            }
                        }, 1000);
                    } else {
                        ProcessfingerNotMatch();
                        //Play Voice Promt of fAILURE AND COMEE OUT

                    }

                }
            }
        }
    }
    /*
    private class CaptureAndMatchTaskOrig extends AsyncTask<Integer, Integer, Integer> {
        boolean FirstUser_Cap=true;
        @Override
        protected Integer doInBackground(Integer... i) {
            int rc;
            rc = mVCOM.Open();
            if (rc == 0) {
                Log.d(TAG,"It is VCOM Capture");
                rc = mVCOM.Capture();
                if (rc == 0) {
                    int tmplSize = mVCOM.GetTemplateSize();
                    if (tmplSize > 0) {
                        byte[] tmpl = new byte[tmplSize];
                        rc = mVCOM.GetTemplate(tmpl);
                        if (rc == 0) {
                            Log.d(TAG, "doInBackground:templ " + Base64.encodeToString(tmpl, Base64.DEFAULT));
                         //   Log.d(TAG, "doInBackground: finger" + Base64.encodeToString(mTmplCapture, Base64.DEFAULT));
                      //      database.getDAO().insert(new Fingerentity(Base64.encodeToString(tmpl, Base64.DEFAULT)));

                            Log.d(TAG,"doInBackground saved Finger  is "+btsv5);
                            Log.d(TAG,"doInBackground  captured Finger"+Base64.decode(btsv5, Base64.DEFAULT));
                        try {
                            rc = mVCOM.Match(tmpl, Base64.decode(btsv5, Base64.DEFAULT));

                            int score = mVCOM.GetMatchScore();
                            Log.d(TAG, "doInBackground:score " + score);
                            if (score > 0)
                                Log.d(TAG, "Matching Score is " + score);
                            if (score > 50000) {
                                FirstUser_Cap = true;
                            } else {
                                Log.d(TAG,"Checking with second user");
                                rc = mVCOM.Match(tmpl, Base64.decode(btsv4, Base64.DEFAULT));
                                score = mVCOM.GetMatchScore();
                                FirstUser_Cap = false;
                            }
                        } catch (Exception obj) {
                            Log.d(TAG,"Got Exception while fingerprint Matching ");
                        }
                        }
                        Log.d(TAG,
                                "doInBackground: " + rc);
                    }
                }
                mVCOM.Close();
            }
            return rc;
        }

        @Override
        protected void onPostExecute(Integer rc) {

            if (rc == 0) {
                int score = mVCOM.GetMatchScore();

                Log.d(TAG,"First User Status "+FirstUser_Cap);
                GlobalVariables.FirstUser = FirstUser_Cap;
                fingerRemove(score);

            } else {


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new CaptureAndMatchTask().execute(0);
                    }
                }, 4000);



            }

        }
    }
*/
    private Bitmap GetBitmap() {
        int sizeX = mVCOM.GetCompositeImageSizeX();
        int sizeY = mVCOM.GetCompositeImageSizeY();
        if (sizeX > 0 && sizeY > 0) {
            Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
            int[] colors = new int[sizeX * sizeY];
            if (mVCOM.GetCompositeImage(colors) == 0) {
                bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
            } else {
                Log.i(TAG, "Unable to get composite image.");
            }
            return bm;
        } else {
            Log.i(TAG, "Composite image is too small.");
            // ...so we create a blank one-pixel bitmap and return it
            Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            return bm;
        }
    }

    private byte[] GetTemplate() {
        int tmplSize = mVCOM.GetTemplateSize();
        if (tmplSize > 0) {
            byte[] tmpl = new byte[tmplSize];
            if (mVCOM.GetTemplate(tmpl) == 0) {
                return tmpl;
            } else {
                Log.i(TAG, "Unable to get template.");
            }
        } else {
            Log.i(TAG, "Template is too small.");
        }
        return new byte[0];
    }

    private void DrawMinutiae(Bitmap bm, byte[] tmpl) {
        int tmplSize = tmpl.length;
        if (tmplSize < 1)
            return;
        if (bm.getWidth() < 2 || bm.getHeight() < 2)
            return;

        ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
        Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint();
        for (int i = 0; i < minutiaeList.length; i++) {
            if (minutiaeList[i].nType == 1)
                paint.setARGB(0xFF, 0xFF, 0x00, 0x00);    // line-ending
            else
                paint.setARGB(0xFF, 0x00, 0xFF, 0x00);    // bifurcation
            int nX = minutiaeList[i].nX;
            int nY = minutiaeList[i].nY;
            canvas.drawCircle(nX, nY, 4, paint);
            double nR = minutiaeList[i].nRotAngle;
            int nX1;
            int nY1;
            nX1 = (int) (nX + (10.0 * Math.cos(nR)));
            nY1 = (int) (nY - (10.0 * Math.sin(nR)));
            canvas.drawLine(nX, nY, nX1, nY1, paint);
            canvas.drawLine(nX + 1, nY, nX1 + 1, nY1, paint);
            canvas.drawLine(nX - 1, nY, nX1 - 1, nY1, paint);
            canvas.drawLine(nX, nY + 1, nX1, nY1 + 1, paint);
            canvas.drawLine(nX, nY - 1, nX1, nY1 - 1, paint);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myExitTimer != null) {
            myExitTimer.cancel();
            myExitTimer= null;
        }
        try {
            GlobalVariables.SupportWebFaceDetectonInitated=false;
            if(mVCOM != null)
                mVCOM.Close();
        } catch (Exception obj) {

        }
        try {
//          UnRegisterReceiver();
        }catch (Exception obj) {

        }
    }
    private void playAudioFileOld(int Request, String mediafile) {
        try {
            Log.d(TAG,"Play Audio File "+Request+" Mediafile"+mediafile);

            MediaPlayer mp = new MediaPlayer();
            switch (Request) {
                //For Success
                case 1:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    break;

                case 2:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    break;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while play Audio "+obj.toString());
        }
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        Log.d(TAG,"onBackPressed"+fromWeb);
        if (myExitTimer != null) {
            myExitTimer.cancel();
            myExitTimer= null;
        }
        if (fromWeb) {
            Log.d(TAG,"GoBack to Home Activity");
            //Intent intent= new Intent(this, HomeActivity.class);
            //Intent intent= new Intent(this, HomeEnrollActivity.class);
            Intent intent= new Intent(this, Main2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            this.startActivity(intent);
            finish();
            //mVCOM.Close();
        }  else
        if (!SendingMessage("home","104")) {
            Log.d(TAG,"Sending   MEssage Failure in MainActivity");
            finish();
        }
        super.onBackPressed();
    }
    private boolean SendingMessage(String status, String requestType) {
        SendingMessageWithState(status,"11",requestType);
        return false;
    }
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }



    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }
    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.toString());
        }
        return false;
    }
    private boolean SendingMessageWithState(String status, String requestType, String state) {
        Log.d(TAG, "SendMessage " + status);
        //String url="http://192.168.43.24:8080/?username="+status;
        GlobalVariables.currAction=status;
        GlobalVariables.curState=state;

//For Web Server
        if (GlobalVariables.AppSupport ==1) {
            SendingMessageWithStateWeb(status, requestType, state);
        }
        return true;
    }
    private boolean SendingMessageWithStateWeb(String status, String requestType, String state) {
        Log.d(TAG,"SendMessage "+status);
        //String url="http://192.168.43.24:8080/?username="+status;

        if (!isEthernetConnected()) {
            Log.d(TAG,"SendingMEssageWithStats Not sending as it is Wifi");
        }

        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }


        Log.d(TAG,"Getting IP Address");
        //   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        String url="http://"+remoteIP+":8080/?username="+status+"&state="+state;

        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: 123"+requestType+" Resoibse us"+response);

            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: 123 "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);
        return true;
    }
    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);
        }

        @Override
        public void onFinish() {

            Log.d(TAG,"Timer Got Expired ");
            GlobalVariables.personDescription="not matching";
            //GlobalVariables.tokenID=0;
            GlobalVariables.reqStatus= true;
            //finish();
            //finishAffinity();
            onBackPressed();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }

    private void playAudioFile(int Request, String mediafile) {
        try {
            Log.d(TAG, "Play Audio File " + Request + " Mediafile" + mediafile);

            MediaPlayer mp = new MediaPlayer();
            switch (Request) {
                //For Success
                case 1:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mpl) {
                            mp.reset();
                            mp.release();
                        }
                    });
                    break;

                case 2:

                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mpl) {
                            mp.reset();
                            mp.release();
                        }
                    });
                    break;
            }
        } catch (Exception obj) {
            Log.d(TAG, "Got Exception while play Audio " + obj.toString());
        }
    }
    private void StartRemoteApp() {

        Log.d(TAG,"in StartRemoteApp");
        if (GlobalVariables.SupportWebFaceDetection && !GlobalVariables.MobileDevice
                && fromWeb) {
            Log.d(TAG, "Sending the Message ");
            GlobalVariables.SupportWebFaceDetectonInitated=true;
            SendingMessageWithStateWeb("face","101",null);
        }
    }

    private void RegisterInterReceiver() {

        //registerReceiver(this.FingerScanActivity,
        //      this.mUpdateReceiver);

        Log.d(TAG,"In RegisterItnerReceiver");
        this.mUpdateFilter = new IntentFilter("lumidigm.captureandmatch.activites.processevent");

        try {
            this.mUpdateReceiver = new BroadcastReceiver() {
                public void onReceive(final Context context, final Intent intent) {
                    final String stringExtra = intent.getStringExtra("STATUS");

                    Log.e("Main", "com.global.aadnetwork.insideshop Received UI Event"+stringExtra);
                    //if (stringExtra.equals("Success"))
                    {
                        Log.d(TAG,"Got Success from the Webserver disconnecting ");
                        //onBackPressed();
                        TestBack();
                        //    Main.this.updateStatusMessage(stringExtra, intExtra);
                        //return;
                    }
                }
            };
        } catch (Exception obj) {
            Log.d(TAG,"Got Crashed WHile Intent FIlter "+obj.getMessage());
        }

        //   getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
    }
    private void UnRegisterReceiver() {
        Log.d(TAG,"Got UnRegisterReceiver");
        try {
            //if (this.mUpdateReceiver != null)
            unregisterReceiver(this.mUpdateReceiver);
            //this.mUpdateReceiver = null;
        } catch (Exception obj) {

        }
    }

    private void RegisterReceiver() {
        Log.d(TAG, "Got RegisterReceiver");

        try {
            //if (this.mUpdateReceiver != null)
            getApplicationContext().registerReceiver(mUpdateReceiver, mUpdateFilter);
        } catch (Exception obj) {

        }
    }

    private void sendBroadCast(final Context context) {
        Log.e("Main","SendBroadcast");
        try {
            final Intent intent = new Intent("lumidigm.captureandmatch.activites.processevent");
            intent.putExtra("Got the Message STATUS", "Close");
            context.sendBroadcast(intent);
        } catch (Exception oj) {
            Log.d(TAG,"Got Crashed while Sending Broadcast "+oj.getMessage());
        }
    }
    /*
    @Override
    public void onPause() {
        super.onPause();
        UnRegisterReceiver();
    }
    @Override
    public void onResume() {
        super.onResume();
        RegisterReceiver();
    } */
    private void TestBack() {
        Log.d(TAG,"onBackPressed"+fromWeb);
        if (myExitTimer != null) {
            myExitTimer.cancel();
            myExitTimer= null;
        }
        if (!SendingMessage("home","124")) {
            Log.d(TAG,"Sending   MEssage Failure in MainActivity");
            finish();
        }
        {
            Log.d(TAG,"GoBack to Home Activity");
            //Intent intent= new Intent(this, HomeActivity.class);
            //Intent intent= new Intent(this, HomeEnrollActivity.class);
            Intent intent= new Intent(this, Main2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            this.startActivity(intent);
            //  finish();
            //mVCOM.Close();
        }

    }

    @Override
    public void notifySuccess(String requestType, Object response) {

        Log.d(TAG, "SendGetAllUsers - notifySuccess: "+requestType+" Response is"+response);
        //GlobalVariables.FirstUser = FirstUser_Cap;
        //fingerRemove(60000);

        if (requestType.equals("501")) {
            //Process the
            try {
                Log.d(TAG, "Got all the Users data ");
                Log.d(TAG, "Befor Gson");
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<lumidigm.captureandmatch.entity.User>>() {
                }.getType();
                Log.d(TAG, "Befor allTasks data");
                List<lumidigm.captureandmatch.entity.User> allTasks = gson.fromJson(response.toString(), listType);
                Log.d(TAG, "notifySuccess: " + allTasks.size());
                appDatabase.userDao().deleteAll();
                Log.d(TAG, "After deleteAll");
                appDatabase.userDao().insertAll(allTasks);

                //Now needs to check whether that Finger Print Matches the User Enrolled
                mVCOM = new VCOM(this);
                new CheckPreviousCaptureFingerMatchTask().execute(0);
            } catch (Exception obj) {

            }
        } else {
            try {
                JSONObject obj = new JSONObject(response.toString());

                // JSONObject jobject = new JSONObject(response.toString());
                //if (obj != null)
                {
                    ldapuser = obj.optString("uid");
                    Log.d(TAG, "user is ldap " + ldapuser);
                    String UIDNum = obj.optString("uidNumber");

                    Log.d(TAG, "LDAP User ID is " + ldapuser + "  UID is " + UIDNum);
                    if (ldapuser != null) {
                        Log.d(TAG, "LDAP User is matching  " + ldapuser);

                        GlobalVariables.FirstUser = true; //FirstUser_Cap;
                        GlobalVariables.loggedInUser = ldapuser;
                        Log.d(TAG, "First User is " + GlobalVariables.loggedInUser);
                        fingerRemove(60000);
                    } else {
                        ProcessfingerNotMatch();
                        Log.d(TAG, "Enroll Failure as User is Not Available ");
                    }
                }
            } catch (Exception obj) {
                Log.d(TAG, "Got Exception obj" + obj.getMessage());
            }
        }
        MyProgressDialog.dismiss();
    }

    @Override
    public void notifyError(String requestType, VolleyError error) {
        Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getCause());
        Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getMessage());
        try {
            //String fingerprint = "testfingerprint";
            Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
            //  String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
            //           playAudioFile(2,filePath);
            MyProgressDialog.dismiss();
            ProcessfingerNotMatch();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private boolean SendtoSSOServer(String serverName, String Val ,String requestType) {
        Log.d(TAG,"SendMessage "+serverName);

        //return false
        if (serverName != null)
            return false;

        String remoteIP = getRemoteIP();
        if (remoteIP == null || remoteIP == "") {
            Log.d(TAG," No IP ");
            return false;
        }
        if (!isEthernetConnected()) {
            if (!isConnectedInWifi()) {
                Log.d(TAG,"Network not connected");
                return false;
            }
        }


        Log.d(TAG,"Getting IP Address");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
        String url=serverName+"/?"+Val;

        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
                //GlobalVariables.FirstUser = FirstUser_Cap;
                fingerRemove(60000);
            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "notifyError: 567 "+error.getCause());
                Log.d(TAG, "notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

        return true;
    }
    private void SendGetAllUsers() {

        String requestType = "202";
        String url=GlobalVariables.SSOIPAddress + GET_ALL_USERS;
        // String url="http://192.168.1.10:8114/user_enroll/check_user/1002";
        // new VolleyService(getApplicationContext(), getApplicationContext()).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");

        Log.d(TAG, "send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "SendGetAllUsers - notifySuccess: "+requestType+" response us"+response);
                //GlobalVariables.FirstUser = FirstUser_Cap;
                //fingerRemove(60000);

                Log.d(TAG, "Befor Gson");
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<lumidigm.captureandmatch.entity.User>>() {
                }.getType();
                Log.d(TAG, "Befor allTasks data");
                List<lumidigm.captureandmatch.entity.User> allTasks = gson.fromJson(response.toString(), listType);
                Log.d(TAG, "notifySuccess: " + allTasks.size());
                appDatabase.userDao().deleteAll();
                Log.d(TAG, "After deleteAll");
                appDatabase.userDao().insertAll(allTasks);
                Log.d(TAG, "Befor Intent");

            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "SendGetAllUsers - notifyError:  is "+error.getCause());
                Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    //  String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

    }

    private void CheckWetherLDAPUser(String userID) {

        String requestType = "202";
        String url=GlobalVariables.SSOIPAddress + SSO_CHECK_USER_ID_ACTIVE_IN_LDAP +userID;
        //   new VolleyService(getApplicationContext(), getApplicationContext()).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
        // Log.d(TAG," Enroll device"+GlobalVariables.SSOIPAddress + SSO_CHECK_USER_ID_ACTIVE_IN_LDAP + editText.getText().toString());

        Log.d(TAG, "CheckWetherLDAPUser send url: "+url);
        new VolleyService(new IResult() {
            @Override
            public void notifySuccess(String requestType, Object response) {

                Log.d(TAG, "SendGetAllUsers - notifySuccess: "+requestType+" Resoibse us"+response);
                //GlobalVariables.FirstUser = FirstUser_Cap;
                //fingerRemove(60000);

                try {
                    JSONObject obj = new JSONObject(response.toString());

                    // JSONObject jobject = new JSONObject(response.toString());
                    //if (obj != null)
                    {
                        ldapuser = obj.optString("uid");
                        Log.d(TAG,"user is ldap "+ldapuser);
                        String UIDNum = obj.optString("uidNumber");

                        Log.d(TAG,"LDAP User ID is "+ldapuser+"  UID is "+UIDNum);
                        if (ldapuser != null ) {
                            Log.d(TAG, "LDAP User is matching  "+ldapuser);

                            GlobalVariables.FirstUser = true; //FirstUser_Cap;
                            GlobalVariables.loggedInUser = ldapuser;
                            Log.d(TAG, "First User is " + GlobalVariables.loggedInUser );
                            fingerRemove(60000);
                        } else {
                            ProcessfingerNotMatch();
                            Log.d(TAG, "Enroll Failure as User is Not Available ");
                        }
                    }
                } catch (Exception obj) {
                    Log.d(TAG,"Got Exception obj"+obj.getMessage());
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "SendGetAllUsers - notifyError: "+error.getMessage());
                try {
                    //String fingerprint = "testfingerprint";
                    Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                    //  String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
                    //           playAudioFile(2,filePath);

                    ProcessfingerNotMatch();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }, this).tokenBase(GET, url, null, requestType);

    }

    public void DownloadUserData() {
        Log.d(TAG,"It is DownloadUserData");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG,"Sending Get User Data ");
                //SendingMessageWithState("finger","101","fingersuccess");
                //new VolleyService(FingerScannActivity.this, FingerScannActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS , null, "501");
                new VolleyService(FingerScannActivity.this, FingerScannActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "501");


            }
        }, 500);

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

}

