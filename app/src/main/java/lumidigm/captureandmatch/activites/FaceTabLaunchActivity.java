package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StatFs;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxand.facerecognition.MainActivity;
import com.luxand.facerecognition.MainActivity_FaceandFinger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.BuildConfig;
import lumidigm.captureandmatch.R;
import lumidigm.constants.Constants;
import database.AppDatabase;
import lumidigm.captureandmatch.entity.User;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.utils.PermissionListener;
import lumidigm.vcomdroid.VCOM;
import webserver.AndroidWebServer;
import webserver.DiscoverWebServer;
import webserver.GlobalVariables;
import webserver.WebserverBroadcastRxService;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static lumidigm.constants.Constants.GET_ALL_ACTIVE_VISITORS_AND_OTHERS;
import static lumidigm.constants.Constants.GET_ALL_NON_VISITOR_USERS;
import static lumidigm.constants.Constants.GET_ALL_USERS;
import static lumidigm.constants.Constants.SSO_SERVER_DEVICE_STATUS;
import static webserver.GlobalVariables.EnrollCount;
import static webserver.GlobalVariables.FaceDetectionDetaineeIdentificationforV4;
import static webserver.GlobalVariables.FaceandFinger;
import static webserver.GlobalVariables.GuardStation_WaitingArea;
import static webserver.GlobalVariables.MobileDevice;
import static webserver.GlobalVariables.TimeandAttendence;
import static webserver.GlobalVariables.faceTab;
import static webserver.GlobalVariables.mobileWorkFlowConfigure;
import static webserver.GlobalVariables.productVersion;
import static webserver.GlobalVariables.selectedProduct;
import static webserver.GlobalVariables.vcomdatabaseSupport;

public class FaceTabLaunchActivity extends AppCompatActivity implements IResult {
  private static final String TAG = "FaceTabLaunchActivity";
  Runnable runnable;
  SharedPreferences prefs;
  Handler handler;
  AppDatabase appDatabase;
  PermissionListener mPermissionListener;
  private Timer InternetCheckTimer = null;
  private boolean InternetFine=false;
  private int TimerCount =0;
  private int MAXTIMERCOUNT=3;
  private int UserCount = 0;
  private VCOM mVCOM=null;
  private int UserID=100;

  private AndroidWebServer androidWebServer;
  private static boolean isStarted = false;
  private static final int DEFAULT_PORT = 8080;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (GlobalVariables.selectedProduct == 1)
      setContentView(R.layout.launch_activity_frost);
    else
      setContentView(R.layout.launch_activity);

    //      setContentView(R.layout.face_homeactivity);

    //   View overlay = findViewById(R.id.mylayout);
    //   ImageView      launchImage = findViewById(R.id.launch_image);
    prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
            MODE_PRIVATE);
    EnrollCount=0;


    ReadSSOServerAddressDetails();
    //ReadCallFlowNumber();

    int workflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

    Log.d(TAG,"Work Flow Status is  "+workflowID);
    // SetWorkFlow(workflowID);
    // workflowID  =0;  // Chand - To take from local callflow file - for testing
    if (workflowID < 1)
    {
      ReadCallFlowNumber();
      Log.d(TAG, "Reading from file");
    }  else {
      Log.d(TAG, "Not Reading Call Flow Number ");
      SetWorkFlow(workflowID);
    }
    GlobalVariables.WorkFlowVal = workflowID;

       /* overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
*/
    try {
      Log.d(TAG, "on Create ");
      View decorView = getWindow().getDecorView();
      decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
              | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
              | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_FULLSCREEN
              | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    } catch (Exception obj) {
      Log.d(TAG,"Got Exception while setting Full Screen");
    }


    appDatabase = AppDatabase.getAppDatabase(FaceTabLaunchActivity.this);

    List<User> users = appDatabase.userDao().getAll();

    UserCount = users.size();
    Log.d(TAG,"UserCount is "+UserCount);
    UserCount =1;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      // only for gingerbread and newer versions

      requestPermissions(this, new String[]{"Manifest.permission.WRITE_EXTERNAL_STORAGE"}, 100, new PermissionListener() {
        @Override
        public void onGranted(int statuscode) {
          createfile();
        }

        @Override
        public void onDenied(List<String> deniedPermissions) {

        }
      });
    } else {
      createfile();
    }


    Log.d(TAG,"Progress Dialog Wait");
//        MyProgressDialog.show(this, R.string.wait_message);

    //sendDeviceStatusInfo();

    if (GlobalVariables.WebServer) {
      startAndroidWebServer();
    }

    // mVCOM mVCOM

    GlobalVariables.TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);

    int pending_config_update = prefs.getInt(GlobalVariables.pending_terminal_config_Update, 0);
    int pending_users_config_update = prefs.getInt(GlobalVariables.pending_users_Config_Update,0);

    Log.d(TAG,"Test Pending Config Update is "+pending_config_update+"  "+pending_users_config_update);
    //Pending for Config update
    Log.d(TAG,"FaceTab is "+faceTab);


      if (faceTab && UserCount > 0) {

          Log.d(TAG,"Starting Face Authentication");
          //      Intent intent= new Intent(this, MainActivity.class);
          Intent intent= new Intent(getApplicationContext(), MainActivity_FaceandFinger.class);
          //    Intent intent= new Intent(this, MainActivity_Sentinel.class);
          Bundle mBundle = new Bundle();
          mBundle.putBoolean("Enrolment",false);
          intent.putExtras(mBundle);
          startActivity(intent);
          finish();

  /*      }
      }, 100);
*/

    }
    StartUSBDevice();


  }
  @Override
  protected void onResume() {
    super.onResume();

    //  handler.postDelayed(runnable,3000);
  }

  @Override
  protected void onPause() {
    super.onPause();
    // handler.removeCallbacks(runnable);
  }


  @Override
  public void onStart() {
    super.onStart();

  }

  @Override
  public void onStop() {

    super.onStop();
    Log.d(TAG,"onStop Launch Activity Stopping Timer ");

    if (InternetCheckTimer != null)
      InternetCheckTimer.cancel();
    InternetCheckTimer=null;
  }

  @Override
  public void notifySuccess(String requestType, Object response)  {

  }


  @Override
  public void notifyError(String requestType, VolleyError error) {

  }

  public void createfile() {
    String fileName = "hdmiappversion.txt";//like 2016_01_12.txt


    try {


      File gpxfile = new File(Environment.getExternalStorageDirectory(), fileName);


      FileWriter writer = new FileWriter(gpxfile, false);


      String versionName = BuildConfig.VERSION_NAME;

      writer.write(versionName);
      writer.flush();
      writer.close();
      // Toast.makeText(this, "Data has been written to Report File", Toast.LENGTH_SHORT).show();
    } catch (IOException e) {
      e.printStackTrace();

    }
  }

  public void requestPermissions(Activity activity, String[] permissions, int status, PermissionListener listener) {

    if (null == activity) {
      return;
    }

    mPermissionListener = listener;
    List<String> permissionList = new ArrayList<>();
    for (String permission : permissions) {
      //Permission is not authorized
      if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
        permissionList.add(permission);
      }
    }

    if (!permissionList.isEmpty()) {
      ActivityCompat.requestPermissions(activity, permissionList.toArray(new String[permissionList.size()]), status);
    } else {
      mPermissionListener.onGranted(status);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if (grantResults.length > 0) {
      List<String> deniedPermissions = new ArrayList<>();
      for (int i = 0; i < grantResults.length; i++) {
        int result = grantResults[i];
        if (result != PackageManager.PERMISSION_GRANTED) {
          String permission = permissions[i];
          deniedPermissions.add(permission);
        }
      }

      if (deniedPermissions.isEmpty()) {
        if (mPermissionListener != null)
          mPermissionListener.onGranted(requestCode);
      } else {
        if (mPermissionListener != null)
          mPermissionListener.onDenied(deniedPermissions);
      }
    }

  }

  private void StartUSBDevice() {
    Intent intent = getIntent();
    String ACTION_USB_DEVICE_ATTACHED = "com.example.ACTION_USB_DEVICE_ATTACHED";

    try {

      Log.d(TAG, "in StartUSBDevice ");
      if (intent != null) {
        Log.d(TAG, "in StartUSBDevice intent is not null ");

        if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
          Parcelable usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
          Log.d(TAG, "in StartUSBDevice USB Device Attached ");
          // Create a new intent and put the usb device in as an extra
          Intent broadcastIntent = new Intent(ACTION_USB_DEVICE_ATTACHED);
          broadcastIntent.putExtra(UsbManager.EXTRA_DEVICE, usbDevice);

          // Broadcast this event so we can receive it
          sendBroadcast(broadcastIntent);
        }
      }
    } catch (Exception obj) {
      Log.d(TAG, "Got USB Device exception " + obj.getMessage());
    }
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

  }
  private String ReadSSOServerAddressDetails() {

    String ipAddr = null;
    String filePath=Environment.getExternalStorageDirectory() +"/ssoserverip.txt";

    GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

    // GlobalVariables.SSOIPAddress=Constants.SERVER_IPADDR;
    try {

      Log.d("Main2Activity","in ReadSSOServerAddressDetails "+filePath);
      InputStream is = new FileInputStream(filePath);

      int size = is.available();
      Log.d("Main2Activity","Size of the JSON is "+size);
      byte[] buffer = new byte[size];

      is.read(buffer);
      is.close();

      ipAddr = new String(buffer, "UTF-8");

      Log.d(TAG,"IP Addr is "+ipAddr);
      if (ipAddr.contains(".")) {
        if (!GlobalVariables.SSOIPAddress.equals(ipAddr)) {
          GlobalVariables.SSOIPAddress = ipAddr;

          SharedPreferences.Editor editor1 = prefs.edit();

          editor1.putString(GlobalVariables.SP_ServerIP, GlobalVariables.SSOIPAddress);

          Log.d(TAG,"SSO Server IP Changed is "+GlobalVariables.SSOIPAddress);

          editor1.commit();

        }
      }
      Log.d(TAG, "It is Proper IP Address " + GlobalVariables.SSOIPAddress);
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
    Log.d("Main2Activity","Coming out of ReadSSOServerAddressDetails "+GlobalVariables.SSOIPAddress);
    return ipAddr;
  }
  private String ReadCallFlowNumber() {

    String callflowstr = null;
    String filePath=Environment.getExternalStorageDirectory() +"/callflow.txt";

    // GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

    // GlobalVariables.SSOIPAddress=Constants.SERVER_IPADDR;
    try {

      Log.d("Main2Activity","in ReadCallFlowNumber "+filePath);
      InputStream is = new FileInputStream(filePath);

      int size = is.available();
      Log.d("Main2Activity","Size of the JSON is "+size);
      byte[] buffer = new byte[size];

      is.read(buffer);
      is.close();

      callflowstr = new String(buffer, "UTF-8");
      int callflow = Integer.valueOf(callflowstr);

      Log.d(TAG,"Call flwo val "+callflow);
      Log.d(TAG,"Call Flow is "+callflow+" 0 for Verification 1 for Normal enroll - 2 for Supervisor enroll");

      if (callflow == 0) {
        GlobalVariables.onlyVerification=true;
        GlobalVariables.supervisionEnrolmentStatus=false;
      } else if (callflow ==2 ) {
        GlobalVariables.onlyVerification=false;
        GlobalVariables.supervisionEnrolmentStatus=true;
      } else  {
        GlobalVariables.onlyVerification=false;
        GlobalVariables.supervisionEnrolmentStatus=false;
      }
      Log.d(TAG,"Only Verificationis  "+GlobalVariables.onlyVerification+"  "+GlobalVariables.supervisionEnrolmentStatus);

    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
    Log.d("Main2Activity","Coming out of ReadSSOServerAddressDetails "+GlobalVariables.SSOIPAddress);
    return callflowstr;
  }

  private boolean startAndroidWebServer() {
    Log.d("HomeActivity","in Start Web Server");
    if (!isStarted) {
      int port = getPortFromEditText();
      try {
        if (port == 0) {
          throw new Exception();
        }
        androidWebServer = new AndroidWebServer(port, this);
        androidWebServer.start();
        return true;
      } catch (Exception e) {
        e.printStackTrace();
        //Snackbar.make(coordinatorLayout, "The PORT " + port + " doesn't work, please change it between 1000 and 9999.", Snackbar.LENGTH_LONG).show();
      }
    }
    return false;
  }
  private boolean stopAndroidWebServer() {
    if (isStarted && androidWebServer != null) {
      androidWebServer.stop();
      return true;
    }
    return false;
  }
  private int getPortFromEditText() {
    //String valueEditText = "8080";
    //return (valueEditText.length() > 0) ? Integer.parseInt(valueEditText) : DEFAULT_PORT;
    return DEFAULT_PORT;
  }
  private void SetWorkFlow(int workflow) {
    Log.d(TAG,"Current work flow is "+workflow);

    //int workflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

    int localWorkFlowID = prefs.getInt(GlobalVariables.localworkflowStatus, -1);
    Log.d(TAG,"Main Work FLow is "+workflow+"  LocalWorlFlow"+localWorkFlowID);

    if (workflow == 20) {
      Log.d(TAG,"local   Work Flow is Got precedence "+localWorkFlowID);
      if (localWorkFlowID != -1)
        workflow = localWorkFlowID;
      else if (selectedProduct == 1)
        workflow = 1;
      else
        workflow = 7;

      Log.d(TAG,"Going with local work flow "+workflow);
    } else
      Log.d(TAG,"Going with Normal work flow "+workflow);

    GlobalVariables.ledSupport=false;
    GlobalVariables.BoothGuardStation_Outside=false;
    FaceDetectionDetaineeIdentificationforV4=false;

    if (productVersion == 1) {
      GlobalVariables.WorkFlowVal=1;
      GlobalVariables.ledSupport=true;
      Log.d(TAG,"Work Flow is only Access Control - V0 ");
      // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //  GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=0;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=0;
      GlobalVariables.WebServer=false; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      TimeandAttendence = false; //true;
      GlobalVariables.adminVerification=true;
      GlobalVariables.RestrictedAccess = false;


      GlobalVariables.TV_Support =false;
      GlobalVariables.Terminal_Support =false;
      GuardStation_WaitingArea =false;
      GlobalVariables.Detainee_Checkpoint = false;
      GlobalVariables.Guard_In_Block=false;

      GlobalVariables.Escort_BoothGuardStation=false;
      GlobalVariables.Detainee_Terminal_StartExit=false;
      GlobalVariables.Escort_TaskList=false;

      GlobalVariables.WaitingAreaVisitorVerification=false;
      GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
    } else
    if (workflow==1) {
      GlobalVariables.WorkFlowVal=1;
      Log.d(TAG,"Work Flow is only Access Control ");
      // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //  GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=0;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      GlobalVariables.TimeandAttendence = false; //true;
      GlobalVariables.adminVerification=true;
      GlobalVariables.RestrictedAccess =false;

      GlobalVariables.TV_Support =false;
      GlobalVariables.Terminal_Support =false;
      GuardStation_WaitingArea =false;
      GlobalVariables.Detainee_Checkpoint = false;
      GlobalVariables.Detainee_outsideBlock=false;
      GlobalVariables.Guard_In_Block=false;

      GlobalVariables.WaitingAreaVisitorVerification=false;
      GlobalVariables.Escort_BoothGuardStation=false;

      GlobalVariables.Detainee_Terminal_StartExit=false;
      GlobalVariables.Escort_TaskList=false;

      GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
    } else
    if (workflow ==2) {

      GlobalVariables.WorkFlowVal=2;
      Log.d(TAG,"Work Flow is Supervisiorory Enrollment ");

      //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //   GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=1;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      GlobalVariables.TimeandAttendence = false; //true;
      GlobalVariables.RestrictedAccess =false;
      GlobalVariables.adminVerification=true;


      GlobalVariables.TV_Support =false;
      GlobalVariables.Terminal_Support =false;
      GuardStation_WaitingArea =false;
      GlobalVariables.Detainee_Checkpoint = false;
      GlobalVariables.Detainee_outsideBlock=false;
      GlobalVariables.Guard_In_Block=false;

      GlobalVariables.Escort_BoothGuardStation=false;
      GlobalVariables.Detainee_Terminal_StartExit=false;
      GlobalVariables.Escort_TaskList=false;

      GlobalVariables.WaitingAreaVisitorVerification=false;
      GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
    } else

    if (workflow ==3) {

      GlobalVariables.WorkFlowVal=3;
      Log.d(TAG,"Work Flow is Normal enrollment ");

      //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //   GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=1;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
      GlobalVariables.TimeandAttendence = false; //true;
      GlobalVariables.RestrictedAccess =false;
      GlobalVariables.adminVerification=false;


      GlobalVariables.TV_Support =false;
      GlobalVariables.Terminal_Support =false;
      GuardStation_WaitingArea =false;
      GlobalVariables.Detainee_Checkpoint = false;
      GlobalVariables.Detainee_outsideBlock=false;
      GlobalVariables.Guard_In_Block=false;

      GlobalVariables.Escort_BoothGuardStation=false;
      GlobalVariables.Detainee_Terminal_StartExit=false;
      GlobalVariables.Escort_TaskList=false;

      GlobalVariables.WaitingAreaVisitorVerification=false;
      GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
    } else
    if (workflow ==4) {

      GlobalVariables.WorkFlowVal=4;
      Log.d(TAG,"Work Flow is for Time and Attendence");

      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      GlobalVariables.TimeandAttendence = true; //true;
      GlobalVariables.RestrictedAccess =false;


      GlobalVariables.TV_Support =false;
      GlobalVariables.Terminal_Support =false;
      GuardStation_WaitingArea =false;
      GlobalVariables.Detainee_Checkpoint = false;
      GlobalVariables.Detainee_outsideBlock=false;
      GlobalVariables.Guard_In_Block=false;

      GlobalVariables.Escort_BoothGuardStation=false;
      GlobalVariables.Detainee_Terminal_StartExit=false;
      GlobalVariables.Escort_TaskList=false;

      GlobalVariables.WaitingAreaVisitorVerification=false;
      GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
    } else
    if (workflow ==5) {

      GlobalVariables.WorkFlowVal=5;
      Log.d(TAG,"Work Flow is for Restricted Access");

      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      GlobalVariables.RestrictedAccess = true; //true;
      GlobalVariables.TimeandAttendence =false;

      GlobalVariables.TV_Support =false;
      GlobalVariables.Terminal_Support =false;
      GuardStation_WaitingArea =false;
      GlobalVariables.Detainee_Checkpoint = false;
      GlobalVariables.Detainee_outsideBlock=false;
      GlobalVariables.Guard_In_Block=false;

      GlobalVariables.Escort_BoothGuardStation=false;

      GlobalVariables.Detainee_Terminal_StartExit=false;
      GlobalVariables.Escort_TaskList=false;

      GlobalVariables.WaitingAreaVisitorVerification=false;
      GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
    }
    else //Starts from Sentinel
      if (workflow ==6) {
        GlobalVariables.WorkFlowVal=6;
        Log.d(TAG,"Work Flow is for TV Support");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;

        GlobalVariables.TV_Support =true;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;

        GlobalVariables.Escort_BoothGuardStation=false;

        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else
      if (workflow ==7) {

        GlobalVariables.WorkFlowVal=7;
        Log.d(TAG,"Work Flow is for Termianl Entry Exit  ");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;

        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =true;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;

        GlobalVariables.Escort_BoothGuardStation=false;

        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else
      if (workflow ==8) {

        GlobalVariables.WorkFlowVal=8;
        Log.d(TAG,"Work Flow is for Guard Station Waiting ARea");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;

        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =true;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;

        GlobalVariables.Escort_BoothGuardStation=false;

        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else
      if (workflow ==9) {

        GlobalVariables.WorkFlowVal=9;
        Log.d(TAG,"Work Flow is for Detainee CheckPoint");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = true;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.Escort_BoothGuardStation=false;

        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;

        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else if (workflow ==10) {

        GlobalVariables.WorkFlowVal=10;
        Log.d(TAG,"Work Flow is for Guard Station Waiting ARea");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=true;
        GlobalVariables.Guard_In_Block=false;

        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;
        GlobalVariables.Escort_BoothGuardStation=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      }
      else
      if (workflow ==11) {

        GlobalVariables.WorkFlowVal=11;
        Log.d(TAG,"Work Flow is for Guard Station BLock IN ");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=true;

        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;
        GlobalVariables.Escort_BoothGuardStation=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else
      if (workflow ==12) {

        GlobalVariables.WorkFlowVal=12;
        Log.d(TAG,"Work Flow is for Escort BoothGuardStation");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;
        GlobalVariables.Escort_BoothGuardStation=true;
        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else
      if (workflow ==13) {

        GlobalVariables.WorkFlowVal=13;
        Log.d(TAG,"Work Flow is for Escort BoothGuardStation");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;
        GlobalVariables.Escort_BoothGuardStation=false;
        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.WaitingAreaVisitorVerification=true;
        GlobalVariables.Escort_TaskList=false;

        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else
      if (workflow ==14) {

        GlobalVariables.WorkFlowVal=14;
        Log.d(TAG,"Work Flow is for Escort BoothGuardStation");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;
        GlobalVariables.Escort_BoothGuardStation=false;
        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=true;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
      } else
      if (workflow ==15) {

        GlobalVariables.WorkFlowVal=15;
        Log.d(TAG,"Work Flow is for Single Work Flow");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;
        GlobalVariables.Escort_BoothGuardStation=false;
        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;
        GlobalVariables.BoothGuardStation_Outside=true;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;

      } else if (workflow ==16) {

        GlobalVariables.WorkFlowVal=16;
        Log.d(TAG,"Work Flow is for Detainee Exit Work Flow ");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;
        GlobalVariables.Escort_BoothGuardStation=false;
        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;
        GlobalVariables.BoothGuardStation_Outside=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        FaceDetectionDetaineeIdentificationforV4=true;
        Log.d(TAG,"Detainee Start Exit is true ");
      } else if (workflow ==20) {

        GlobalVariables.WorkFlowVal=20;
        Log.d(TAG,"Work Flow is for Single Work Flow");

        GlobalVariables.AppSupport=0;
        GlobalVariables.WebServer=false; //true;

        GlobalVariables.onlyVerification=false;
        //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
        //above true means - no verification and enrollment only with password
        GlobalVariables.supervisionEnrolmentStatus=false;
        GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
        GlobalVariables.RestrictedAccess = false; //true;
        GlobalVariables.TimeandAttendence = false;


        GlobalVariables.TV_Support =false;
        GlobalVariables.Terminal_Support =false;
        GuardStation_WaitingArea =false;
        GlobalVariables.Detainee_Checkpoint = false;
        GlobalVariables.Detainee_outsideBlock=false;
        GlobalVariables.Guard_In_Block=false;
        GlobalVariables.Escort_BoothGuardStation=false;
        GlobalVariables.Detainee_Terminal_StartExit=false;
        GlobalVariables.Escort_TaskList=false;

        GlobalVariables.WaitingAreaVisitorVerification=false;
        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = true;
      }

    Log.d(TAG,"Work Flow not supported "+workflow);
  }

  private void removeSetWorkFlow(int workflow) {
    Log.d(TAG,"Current work flow is "+workflow);

    if (productVersion == 1) {
      GlobalVariables.WorkFlowVal=1;
      Log.d(TAG,"Work Flow is only Access Control - V0 ");
      // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //  GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=0;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=0;
      GlobalVariables.WebServer=false; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      TimeandAttendence = false; //true;
      GlobalVariables.adminVerification=true;
      GlobalVariables.RestrictedAccess = false;

    } else

    if (workflow==1) {
      GlobalVariables.WorkFlowVal=1;
      Log.d(TAG,"Work Flow is only Access Control ");
      // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //  GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=0;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      GlobalVariables.TimeandAttendence = false; //true;
      GlobalVariables.RestrictedAccess =false;
      GlobalVariables.adminVerification=true;
    } else
    if (workflow ==2) {

      GlobalVariables.WorkFlowVal=2;
      Log.d(TAG,"Work Flow is Supervisiorory Enrollment ");

      //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //   GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=1;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      TimeandAttendence = false; //true;

      GlobalVariables.adminVerification=true;
      GlobalVariables.RestrictedAccess =false;
    } else

    if (workflow ==3) {

      GlobalVariables.WorkFlowVal=3;
      Log.d(TAG,"Work Flow is Normal enrollment ");

      //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
      //   GlobalVariables.faceTab=false;//For Mobile
      //  GlobalVariables.vcomdatabaseSupport=1;
      //For Web Server
      //0 for Normal, 1 for WebSupport, 2 for USB
      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=true;
      GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
      TimeandAttendence = false; //true;

      GlobalVariables.adminVerification=false;
      GlobalVariables.RestrictedAccess =false;
    } else
    if (workflow ==4) {

      GlobalVariables.WorkFlowVal=4;
      Log.d(TAG,"Work Flow is for Time and Attendence");

      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      TimeandAttendence = true; //true;
      GlobalVariables.RestrictedAccess =false;
    } else
    if (workflow ==5) {

      GlobalVariables.WorkFlowVal=5;
      Log.d(TAG,"Work Flow is for Time and Attendence");

      GlobalVariables.AppSupport=1;
      GlobalVariables.WebServer=true; //true;

      GlobalVariables.onlyVerification=false;
      //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
      //above true means - no verification and enrollment only with password
      GlobalVariables.supervisionEnrolmentStatus=false;
      GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
      GlobalVariables.TimeandAttendence = false; //true;
      GlobalVariables.RestrictedAccess = true;
    } else
      Log.d(TAG,"Work Flow not supported "+workflow);
  }
}