package lumidigm.captureandmatch.activites;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hidglobal.biosdk.BioDeviceStatus;
import com.hidglobal.biosdk.BioSDKAPI;
import com.hidglobal.biosdk.BioSDKDevice;
import com.hidglobal.biosdk.BioSDKFactory;
import com.hidglobal.biosdk.BioSDKVisitor;
import com.hidglobal.biosdk.listener.IBioSDKDeviceListener;
import com.hidglobal.biosdk.listener.ICaptureListener;
import com.hidglobal.biosdk.listener.IWaitForFingerClearListener;
import com.luxand.facerecognition.FaceSuccessActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import database.AppDatabase;
import frost.timeandattendance.TerminalFaceCheckInActivity;
import lumidigm.MyProgressDialog;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.entity.User;
import lumidigm.constants.Constants;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import lumidigm.vcomdroid.ANSI378TemplateHelper;
import lumidigm.vcomdroid.Minutiae;
import lumidigm.vcomdroid.VCOM;
import webserver.GlobalVariables;
import webserver.WebUtils;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.hidglobal.biosdk.BioDeviceStatus.BIOSDK_OK;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_DONE;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_FINGER_PRESENT;
import static com.hidglobal.biosdk.V100_CmdJNI.ACQ_PROCESSING;
import static lumidigm.constants.Constants.CHECK_USER;
import static webserver.GlobalVariables.M21Support;

public class M21FingerPrintEnrollActivity extends Activity implements View.OnClickListener, IResult , ICaptureListener, IWaitForFingerClearListener, IBioSDKDeviceListener,IFragmentListener, BioSDKVisitor {

  private static final String TAG = "M21FingPriEnrlActivi";
  private Toolbar toolbarTop;
  private TextView welcomeText;
  private ImageView home;
  private ImageView imageBack;
  private ImageView imageHome;
  private ImageView imageNext;
  private TextView textBack;
  private ImageView imageHome1;
  private TextView textNext;


  private TextView enrollUser;
  private LinearLayout centerImage;
  private TextView hint;
  private ImageView imageview1;

  // VCOM Java/JNI adapter.
  private VCOM mVCOM;
  // fingerprint images.
  private Bitmap mBitmapCapture;

  // fingerprint ANSI 378 templates.
  private byte[] mTmplCapture;
  private byte[] mTmplCapture1;
  private byte[] mTmplCapture2;


  String userid;
  String ldapUser=null;
  static int value = 0;

  private boolean fromWeb=false;
  private String currState= null;
  User user;
  SharedPreferences prefs= null;
  AlertDialog alertDialog =null;
  private ImageSwitcher imageSwitcher;
  private int currentIndex=0;
  private final String[] imageNames={"enroll", "enroll_one", "enroll_two","enroll_complete"};
  private final int[] imageNamesnew={R.drawable.enroll, R.drawable.enroll_one,R.drawable.enroll_two,R.drawable.enroll_complete};
  private boolean updateUser =false;
  AppDatabase appDatabase;


  private int mPADResult;
  boolean mIsWaitForFingerClearRunning = false;
  private boolean mCancelCapture = false;
  boolean mWaitForFingerClear = false;
  boolean mCaptureInProgress = false;
  private IFragmentListener mListener;
  private Bitmap mFingerImage;
  private byte[] mTemplate;
  byte[] mProbeTemplate;

  //private int mPADResult;
  private int mTimeOut = 500;
  private  BioSDKDevice  mFPDevice = null;
  private String mMatchLevel = "MEDIUM";
  private String mPADLevel = "MEDIUM";
  BioDeviceStatus mStatus;
  private boolean goingForEnroll = true;
  private boolean comeOutofApp=false;
  private boolean notauthorized=false;
  private int TemplateCount=0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.finger_print_enroll);
    View overlay = findViewById(R.id.mylayout);

    overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    try {

      Log.d(TAG,"Reading User ID  Finger printenroll ");
      ldapUser = getIntent().getExtras().getString("ldapUser");

      updateUser = getIntent().getExtras().getBoolean("updateUser");
      userid = getIntent().getExtras().getString("userid");
      user = getIntent().getExtras().getParcelable("userData");
      Log.d(TAG,"Got the update User "+updateUser);
    } catch (Exception obj) {
      Log.d(TAG,"Got Exception while gtting user");
      user=null;
      ldapUser= null;
      updateUser = false;
    }
    Log.d(TAG,"ldapUser is "+ldapUser);

    try {
      fromWeb = getIntent().getBooleanExtra("FromWeb", false);
      currState = getIntent().getStringExtra("CurrState");
      userid = getIntent().getStringExtra("userid");
      Log.d(TAG, "Got from Web " + fromWeb+" Rxvd UserID "+userid);

    } catch (Exception obj) {
      currState = null;
      //userID = null;
    }


    try {
      updateUser = getIntent().getExtras().getBoolean("updateUser");
      Log.d(TAG,"Update User status is "+updateUser);
      //   user = getIntent().getExtras().getParcelable("userData");
      //ldapUser = getIntent().getExtras().getString("ldapUser");
      //  userid = getIntent().getExtras().getString("userid");
    } catch (Exception obj) {
      Log.d(TAG,"Got Exception while gtting user");
      //user=null;
      //ldapUser= null;
      updateUser= false;
    }
    appDatabase = AppDatabase.getAppDatabase(M21FingerPrintEnrollActivity.this);

    prefs = getSharedPreferences(GlobalVariables.SP_FingerPrint,
            MODE_PRIVATE);
    GlobalVariables.SSOIPAddress = prefs.getString(GlobalVariables.SP_ServerIP, Constants.SERVER_IPADDR);

    Log.d(TAG,"LdapUser is "+ldapUser);

    if (user == null && userid != null) {
      try {
        User user2 = appDatabase.userDao().countUsersBasedonUserID(userid);
        if (user2 != null) {
          user = user2;
          Log.d(TAG, "Got the User details Needs to update" + user2.getUsername());
        } else
          Log.d(TAG, "No User ");
      } catch (Exception obj) {
        Log.d(TAG,"Got the Exception while reading user ");
      }
    }
    if (user == null && userid != null) {
      //Get the UserData based on the UserID
      Log.d(TAG,"Chand Fetch User Data for UserId"+userid);

      GetUserData(userid);
    } else
    if ((fromWeb) && (ldapUser == null))  {
      if (userid != null) {
        //Get the UserData based on the UserID
        Log.d(TAG,"Fetch User Data for UserId"+userid);

        GetUserData(userid);
      }
    } else {

      mVCOM = new VCOM(this);

      toolbarTop = (Toolbar) findViewById(R.id.toolbar_top);
      welcomeText = (TextView) findViewById(R.id.welcome_text);
      home = (ImageView) findViewById(R.id.home);
      welcomeText.setVisibility(View.GONE);
      home.setVisibility(View.GONE);


      //enrollUser = (TextView) findViewById(R.id.enroll_user);
      centerImage = (LinearLayout) findViewById(R.id.center_image);
      hint = (TextView) findViewById(R.id.hint);
      // imageview1 = (ImageView) findViewById(R.id.imageview1);
      //imageSwitcher
      imageSwitcher = (ImageSwitcher) findViewById(R.id.imageSwitcher);

      Animation out= AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
      Animation in= AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
      // Set animation when switching images.
      imageSwitcher.setInAnimation(in);
      imageSwitcher.setOutAnimation(out);
      try {
        Log.d(TAG, "on Create ");
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

      } catch (Exception obj) {
        Log.d(TAG,"Got Exception while setting Full Screen");
      }

      imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

        // Returns the view to show Image
        // (Usually should use ImageView)
        @Override
        public View makeView() {
          ImageView imageView = new ImageView(getApplicationContext());

          //imageView.setBackgroundColor(Color.LTGRAY);
          imageView.setScaleType(ImageView.ScaleType.CENTER);

          //ImageSwitcher.LayoutParams params= new ImageSwitcher.LayoutParams(
          //      Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.MATCH_PARENT);
          //imageView.setLayoutParams(params);
          return imageView;
        }
      });
      this.currentIndex=0;
      this.showImage(this.currentIndex);
      //findViewById(R.id.cancel).setOnClickListener(this);
      userid = getIntent().getExtras().getString("userid");
      //   if (user != null )
      //   enrollUser.setText("Employee #" + userid + " " + user.getFullname());
      //  else
      //    enrollUser.setText("Employee #" + userid);
      // hint.setText("Please scan your right finger");
      //new CaptureTask(0).execute();


      if (M21Support) {
        try {
          Log.d(TAG, "M21 Initializing ");
          BioSDKFactory.initializeBioSDKAPI(getApplicationContext());
          Log.d(TAG, "Initializing BIOSDK successful ");
/*

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Log.d(TAG, "Calling initEngine ");
                        initEngine();
                    }
                }, 1000);
*/

          initEngine();

/*
                BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
                if (bAPI == null) {
                    Log.d(TAG, "BIO SDK No device connected ");
                       popupDialog("No device connected.", "Exit", false);

                } else
                    Log.d(TAG,"getBIOSDK is successfule");


                //The one-time open of the device
                mFPDevice = bAPI.openDevice(0);
                if (mFPDevice == null) {
                    Log.d(TAG, "No MX21 Device connected ");
                    popupDialog("No device connected.", "Exit", false);

                    // return false;
                } else
                    Log.d(TAG,"M21 device got opened ");

                Log.d(TAG,"setBioSDKDeviceLiseneter before ");
                mFPDevice.setBioSDKDeviceListener(this);
                Log.d(TAG,"setBioSDKDeviceLiseneter After ");
*/


        } catch (Exception obj) {
          Log.d(TAG, "Got exception while initializing M21 " + obj.getMessage());
        }
      } else {
        new CaptureTask(0).execute();
      }
    }
  }
  private void showImage(int imgIndex) {
    Log.d(TAG,"Show Image index is "+imgIndex);
        /*String imageName= this.imageNames[imgIndex];
        int resId= getDrawableResIdByName(imageName);
        if(resId!= 0) {
            this.imageSwitcher.setImageResource(resId);
        }*/
    this.imageSwitcher.setBackgroundResource(imageNamesnew[imgIndex]);
  }
  // Find Image ID corresponding to the name of the image (in the drawable folder).

  public int getDrawableResIdByName(String resName) {
    String pkgName = this.getPackageName();
    Log.d(TAG,"PKG Name is "+pkgName);
    // Return 0 if not found.
    int resID = this.getResources().getIdentifier(resName , "drawable", pkgName);
    Log.i(TAG, "Res Name: " + resName + "==> Res ID = " + resID);
    return resID;
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.cancel:
        //TODO implement
        onBackPressed();
        break;
      case R.id.settings_new:
        Log.d(TAG,"On FingerPrintEnrollActivity  Setting");
               /* check = false;
                Intent i1 = new Intent(this, UserAuthenticationActivity.class);
                startActivity(i1);*/
        startSettings();
        //TODO implement
        break;
    }
  }

  @Override
  public void notifySuccess(String requestType, Object response) {
//{"role":"support","deleted":false,"_id":"5b5067d195a76a4d582a3bbc","username":"ram","password":"ram","fullname":"ram","email":"ram","status":"","lastlogin":"2018-07-19T10:28:33.416Z","lastOnline":"2018-07-19T10:28:33.416Z"}
    Log.d(TAG, "notifySuccess: " + response.toString());

    Log.d(TAG,"Got the Request Type "+requestType);
    if (requestType.equals("101")) {

      Log.d(TAG,"Got the Request Type 101 - UserData Fetch");
      Gson gson = new Gson();
      Type listType = new TypeToken<User>() {
      }.getType();

      User user = gson.fromJson(response.toString(), listType);

      if (user.getTemplate1() != null && !user.getTemplate1().isEmpty() &&
              user.getTemplate2() != null && !user.getTemplate2().isEmpty() &&
              user.getTemplate3() != null && !user.getTemplate3().isEmpty()) {


        Intent intent = new Intent(this, M21FingerPrintEnrollActivity.class);

        intent.putExtra("userid", userid);
        if (ldapUser != null)
          intent.putExtra("ldapUser", ldapUser);

        if (user != null) {
          Bundle bn = new Bundle();
          bn.putParcelable("userData", user);
          intent.putExtras(bn);
        }
        startActivity(intent);

        Log.d(TAG,"NotifySucces Sending Self Activity ");
        //Toast.makeText(this, "Already you have added finger prints,so you can go to login and update your profile", Toast.LENGTH_SHORT).show();

      } else {
        Log.d(TAG,"User Already Exists");
                /*Intent intent = new Intent(this, AlreadyExituserActivity.class);

                intent.putExtra("userid", editText.getText().toString());
                Bundle bn=new Bundle();
                bn.putBoolean("check",false);
                bn.putParcelable("userData",user);
                intent.putExtras(bn);
                startActivity(intent);*/



        Intent intent = new Intent(this, M21FingerPrintEnrollActivity.class);

        intent.putExtra("userid", userid);
        if (ldapUser != null)
          intent.putExtra("ldapUser", ldapUser);

        if (user != null) {
          Bundle bn = new Bundle();
          bn.putParcelable("userData", user);
          intent.putExtras(bn);
        }
        startActivity(intent);

      }


    } else {
      Gson gson = new Gson();

      User user1 = gson.fromJson(response.toString(), User.class);
      //  Log.d(TAG, "notifySuccess: " + user.toString());
      AppDatabase appDatabase = AppDatabase.getAppDatabase(M21FingerPrintEnrollActivity.this);
      if (appDatabase.userDao().countUsers(user1.get_id()) != null) {
        appDatabase.userDao().update(user1);
        Log.d(TAG, "notifySuccess: " + appDatabase.userDao().countUsers(user1.get_id()).toString());


      } else {

        appDatabase.userDao().insertAll(user);
      }


      Log.d(TAG,"Sending UserFingerTeamplateResponse start verify");
      new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"FingerPrint_Verify");

      MyProgressDialog.dismiss();
      Log.d(TAG,"Staerting Verifysuccessenroll "+ldapUser);
       Intent i = new Intent(M21FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivityNew.class);
  //    Intent i = new Intent(M21FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
    //  Intent i = new Intent(M21FingerPrintEnrollActivity.this, Main2Activity.class);

      i.putExtra("userid", userid);
      if (ldapUser != null)
        i.putExtra("ldapUser", ldapUser);
      if (user1 != null ) {
        Bundle bn = new Bundle();
        bn.putParcelable("userData", user1);
        i.putExtras(bn);
      }
      // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(i);
      // finish();
      terminate();
    }
  }

  @Override
  public void notifyError(String requestType, VolleyError error) {
    MyProgressDialog.dismiss();
    Log.d(TAG,"Got Notify Error "+error);
    //  Toast.makeText(this, "UseId already exit,please try again", Toast.LENGTH_SHORT).show();
    /*    if (updateUser == true ) {
            Log.d(TAG,"Sending Update Template Response ");
            new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"FingerPrint_Verify");

            MyProgressDialog.dismiss();
            Log.d(TAG,"Staerting Verifysuccessenroll "+ldapUser);
            // Intent i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivity.class);
            Intent i = new Intent(FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
            i.putExtra("userid", userid);
            if (ldapUser != null)
                i.putExtra("ldapUser", ldapUser);
            if (user != null ) {
                Bundle bn = new Bundle();
                bn.putParcelable("userData", user);
                i.putExtras(bn);
            }
            // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }*/
  }


  private class CaptureTask extends AsyncTask<Integer, Integer, Integer> {
    int i = -1;

    public CaptureTask(int i) {
      this.i = i;
    }


    @Override
    protected Integer doInBackground(Integer... i) {
      int rc;

      rc = mVCOM.Open();
      if (rc == 0) {
        rc = mVCOM.Capture();
        if (rc == 0) {

        }
        mVCOM.Close();
      }

      return rc;
    }

    @Override
    protected void onPostExecute(Integer rc) {
      Log.d(TAG, "onPostExecute: " + rc);
      Handler handler = new Handler();
      if (rc == 0) {

        Log.d(TAG,"on PostExecute FingerPrintScan "+i);

        Log.d(TAG,"New FingerPrintSuccess first scucess"+i);

        if (i == 0) {
          new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"First_Success");
          playBeep();
          //   imageview1.setImageResource(R.drawable.finger_remove_first);
          showImage(1);
        } else if (i == 1) {
          new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"Second_Success");

          playBeep();
          showImage(2);
          // imageview1.setImageResource(R.drawable.finger_remove_second);
        } else if (i == 2) {
          new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"Third_Success");

          playBeep();
          showImage(3);
          // imageview1.setImageResource(R.drawable.finger_remove_thired);
        }

        handler.postDelayed(new Runnable() {
          public void run() {


            //DrawMinutiae(mBitmapCapture, mTmplCapture);
            if (i == 0) {
              mBitmapCapture = GetBitmap();
              mTmplCapture = GetTemplate();

              // tickTemplate1.setVisibility(View.VISIBLE);
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                  //  new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse","102","First_Success");

                  Log.d(TAG,"FingerPrintSuccess first scucess"+i);
                  //imageview1.setImageResource(R.drawable.finger_first_success);
                  showImage(1);
                  new M21FingerPrintEnrollActivity.CaptureTask(1).execute();
                }
              },3000);

              value++;
            } else if (i == 1) {
              mBitmapCapture = GetBitmap();
              mTmplCapture1 = GetTemplate();
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse","102","Second_Success");

                  Log.d(TAG,"FingerPrintSuccess finger_success_Sencond "+i);
                  //  mVCOM.Match(mTmplCapture1, mTmplCapture);
                  //imageview1.setImageResource(R.drawable.finger_success_second);
                  showImage(2);
                  new M21FingerPrintEnrollActivity.CaptureTask(2).execute();
                }
              },3000);
                   /* if (mVCOM.GetMatchScore() > 50000) {

                        imageTemplate2.setImageBitmap(mBitmapCapture);
                        tickTemplate2.setVisibility(View.VISIBLE);

                    }else{
                        new CaptureTask(1).execute(0);
                    }*/

            } else if (i == 2) {
              //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse","102","Third_Success");

              Log.d(TAG,"FingerPrintSucce  is is 2 "+i);
              mBitmapCapture = GetBitmap();
              mTmplCapture2 = GetTemplate();
              //imageview1.setImageResource(R.drawable.finger_first_success);
              //new CaptureTask(2).execute(0);;
              // tickTemplate3.setVisibility(View.VISIBLE);
              //value++;
              showImage(3);

            }


            if (mTmplCapture1 != null && mTmplCapture1.length > 0 && mTmplCapture != null && mTmplCapture.length > 0 && mTmplCapture2 != null && mTmplCapture2.length > 0) {


              JSONObject jsonObject = new JSONObject();


              try {

                Log.d(TAG,"Added User template timestamp ");
                String tem1 = Base64.encodeToString(mTmplCapture, Base64.DEFAULT);

                String tem2 = Base64.encodeToString(mTmplCapture1, Base64.DEFAULT);

                String tem3 = Base64.encodeToString(mTmplCapture2, Base64.DEFAULT);

                jsonObject.put("uid", userid);
//need username  capture from ldap api(uid), this is needed for logging in
                //check_userid_ldap

                if (ldapUser != null)
                  jsonObject.put("username", ldapUser);


                Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                long currts = (curCalendar.getTimeInMillis() / 1000L);
                jsonObject.put("user_template_timestamp", currts);
                Log.d(TAG,"Timestamp to be updated for timestamp "+currts);

                jsonObject.put("template_1", tem1);
                jsonObject.put("template_2", tem2);
                jsonObject.put("template_3", tem3);

                // if (user.getTemplateFace() !=null)
                //   jsonObject.put("templateface", user.getTemplateFace());

                if (GlobalVariables.supervisorUserID != -1)
                  jsonObject.put("supervisor_userid", GlobalVariables.supervisorUserID);

                GlobalVariables.supervisorUserID= -1;
                Log.d(TAG, "onPostExecute: " + jsonObject);

                if (updateUser == true) {
                  Log.d(TAG,"Going for Update User Templates ");
                  user.setTemplate1(tem1);
                  user.setTemplate2(tem2);
                  user.setTemplate3(tem3);
                  //   AppDatabase appDatabase = AppDatabase.getAppDatabase(FingerPrintEnrollActivity.this);
                  // if (appDatabase.userDao().countUsers(user1.get_id()) != null) {
                  appDatabase.userDao().update(user);

                  Log.d(TAG,"Sending Update Template Response ");
                  new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"FingerPrint_Verify");

                  MyProgressDialog.dismiss();
                  Log.d(TAG,"Staerting Verifysuccessenroll "+ldapUser);
                  // Intent i = new Intent(FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivity.class);
                //  Intent i = new Intent(M21FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
                  Intent i = new Intent(M21FingerPrintEnrollActivity.this, MainActivity.class);

                  i.putExtra("userid", userid);
                  if (ldapUser != null)
                    i.putExtra("ldapUser", ldapUser);
                  if (user != null ) {
                    Bundle bn = new Bundle();
                    bn.putParcelable("userData", user);
                    i.putExtras(bn);
                  }
                  // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                  startActivity(i);
                  terminate();

                } else {


                  MyProgressDialog.show(M21FingerPrintEnrollActivity.this, R.string.wait_message);
                  String url = GlobalVariables.SSOIPAddress + Constants.UPDATER_USERFINGERPRINT;

                    Log.d(TAG,"Please Wait "+url);

                  new VolleyService(M21FingerPrintEnrollActivity.this, M21FingerPrintEnrollActivity.this).tokenBase(POST, url, jsonObject, "100");

                }
              } catch (JSONException e) {
                e.printStackTrace();
              }


            }
          }
        }, 1000);

        // hintLayout.setVisibility(View.VISIBLE);
        // v.setVisibility(View.VISIBLE);

      } else {
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {

            //  mVCOM.Match(mTmplCapture1, mTmplCapture);
            // imageview1.setImageResource(R.drawable.finger_success_second);

            Log.d(TAG,"Wait Capture Task");
            new M21FingerPrintEnrollActivity.CaptureTask(i).execute();
          }
        },1000); //3000
        Log.i(TAG, String.format("Capture returned %d", rc));
      }

    }
  }

  private Bitmap GetBitmap() {
    int sizeX = mVCOM.GetCompositeImageSizeX();
    int sizeY = mVCOM.GetCompositeImageSizeY();
    if (sizeX > 0 && sizeY > 0) {
      Bitmap bm = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
      int[] colors = new int[sizeX * sizeY];
      if (mVCOM.GetCompositeImage(colors) == 0) {
        bm.setPixels(colors, 0, sizeX, 0, 0, sizeX, sizeY);
      } else {
        Log.i(TAG, "Unable to get composite image.");
      }
      return bm;
    } else {
      Log.i(TAG, "Composite image is too small.");
      // ...so we create a blank one-pixel bitmap and return it
      Bitmap bm = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
      return bm;
    }
  }

  private byte[] GetTemplate() {
    int tmplSize = mVCOM.GetTemplateSize();
    if (tmplSize > 0) {
      byte[] tmpl = new byte[tmplSize];
      if (mVCOM.GetTemplate(tmpl) == 0) {
        return tmpl;
      } else {
        Log.i(TAG, "Unable to get template.");
      }
    } else {
      Log.i(TAG, "Template is too small.");
    }
    return new byte[0];
  }

  private void DrawMinutiae(Bitmap bm, byte[] tmpl) {
    int tmplSize = tmpl.length;
    if (tmplSize < 1)
      return;
    if (bm.getWidth() < 2 || bm.getHeight() < 2)
      return;

    ANSI378TemplateHelper templateHelper = new ANSI378TemplateHelper(tmpl, tmplSize);
    Minutiae[] minutiaeList = templateHelper.GetMinutiaeList();
    Canvas canvas = new Canvas(bm);
    Paint paint = new Paint();
    for (int i = 0; i < minutiaeList.length; i++) {
      if (minutiaeList[i].nType == 1)
        paint.setARGB(0xFF, 0xFF, 0x00, 0x00);    // line-ending
      else
        paint.setARGB(0xFF, 0x00, 0xFF, 0x00);    // bifurcation
      int nX = minutiaeList[i].nX;
      int nY = minutiaeList[i].nY;
      canvas.drawCircle(nX, nY, 4, paint);
      double nR = minutiaeList[i].nRotAngle;
      int nX1;
      int nY1;
      nX1 = (int) (nX + (10.0 * Math.cos(nR)));
      nY1 = (int) (nY - (10.0 * Math.sin(nR)));
      canvas.drawLine(nX, nY, nX1, nY1, paint);
      canvas.drawLine(nX + 1, nY, nX1 + 1, nY1, paint);
      canvas.drawLine(nX - 1, nY, nX1 - 1, nY1, paint);
      canvas.drawLine(nX, nY + 1, nX1, nY1 + 1, paint);
      canvas.drawLine(nX, nY - 1, nX1, nY1 - 1, paint);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (M21Support == true) {
      BioSDKFactory.releaseBioSDKAPI();
    } else
    if (mVCOM != null)
      mVCOM.Close();
  }
  public void playBeep() {
    MediaPlayer m = new MediaPlayer();
    try {
      if (m.isPlaying()) {
        m.stop();
        m.release();
        m = new MediaPlayer();
      }

      AssetFileDescriptor descriptor = getAssets().openFd( "shutter.mp3");
      m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
      descriptor.close();

      m.prepare();
      m.setVolume(1f, 1f);
      m.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

  }
  private void GetUserData(String userId) {

    if(userId != null) {
      Log.d(TAG,"GetUserData before CHECK USER"+CHECK_USER);
      MyProgressDialog.show(M21FingerPrintEnrollActivity.this, R.string.wait_message);
      new VolleyService(M21FingerPrintEnrollActivity.this, M21FingerPrintEnrollActivity.this).tokenBase(GET, GlobalVariables.SSOIPAddress + CHECK_USER + userId.toString(), null, "101");
    }
  }
  private void startSettings() {

    Log.d(TAG,"In SHow Licence App");
    LayoutInflater inflater = LayoutInflater.from(M21FingerPrintEnrollActivity.this);
    View subView = inflater.inflate(R.layout.settings_dialog, null);
    final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText1);

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Android Settings");
    builder.setMessage("Password Verification");
    builder.setView(subView);
    builder.setCancelable(false);
    alertDialog = builder.create();


    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        String passwordText = subEditText.getText().toString().trim();
        Log.d(TAG,"Password Text is "+passwordText);
        //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

        String settingPwd = getsettingspwd();
        Log.d(TAG,"App Pwd is "+settingPwd);
        if (passwordText.equals("argus@542")) {
          Log.d(TAG,"Going for Shutdown");
          //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
          //    Shutdown();
        } else
        if (passwordText.equals("argus@543")) {
          Log.d(TAG,"Going for Reboot");
          //  reboot();
        } else
        if (passwordText.equals(settingPwd)) {

          startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
          finish();

        } //else //Kept for Testing by Chand - remove it
        // if (TestWithoutUSB(passwordText) == null)
        //   Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();
      }
    });

    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        {
          alertDialog.cancel();
          // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

          // finish();
        }
      }
    });



    builder.show();
  }
  private String getsettingspwd() {
    String verVal=null;

    try {
      File myFile = new File(Environment.getExternalStorageDirectory()
              + File.separator + "settingspwd.txt");

      FileInputStream fIn = new FileInputStream(myFile);
      BufferedReader myReader = new BufferedReader(new InputStreamReader(
              fIn));
      String aDataRow = "";
      String aBuffer = "";
      while ((aDataRow = myReader.readLine()) != null) {
        aBuffer += aDataRow + "\n";
      }
      verVal = aBuffer.toString().trim();

      Log.d(TAG,"Reading settings pwd from sdfile is "+verVal+" len s "+verVal.length());
      myReader.close();
      if ((verVal != null)  && (verVal != " ") && (verVal != "") && verVal.length() > 0)
        return verVal;
    } catch (Exception obj) {
      Log.d(TAG,"Got exception while reading settings pwd");

    }
    return "blynk@123";
  }

  @Override
  public boolean onUpdateStatus(int acqStatus) {
    Log.d(TAG, "Chand:onUpdateStatus ");
    if (mCancelCapture) {
      mCancelCapture = false;
      return false;
    }

    String feedback = "";

    if (mIsWaitForFingerClearRunning == true && acqStatus == ACQ_FINGER_PRESENT) {
      feedback = "Lift Finger";
    } else if (mIsWaitForFingerClearRunning == false && (acqStatus != ACQ_PROCESSING || acqStatus != ACQ_DONE)) {
      feedback = "Finger Down";
    }
    class Runner implements Runnable {
      Runner(String str) {
        strFeedback = str;
      }

      @Override
      public void run() {
//                mFingerFeedbackTxtView.setText(strFeedback);
      }

      final String strFeedback;
    }
    new Handler(Looper.getMainLooper()).post(new Runner(feedback));

    return true;

  }

  @Override
  public void bioSDKCaptureComplete(BioDeviceStatus result, Bitmap capImage, byte[] capTemplate, int capPADResult) {
    Log.d(TAG, "Chand:bioSDKCaptureComplete ");
    if (result != BIOSDK_OK) {
      onEnrollmentFinished(null);
      handleBioDeviceStatus(result);
      resetGUI();
      //      mEnrollButton.setText("ENROLL");
      mCaptureInProgress = false;
      return;
    }
    mFingerImage = capImage;
    mTemplate = capTemplate;
    mPADResult = capPADResult;
    Log.d(TAG, "Got the PAD Result " + mPADResult);
    if (mWaitForFingerClear) {
      BioSDKDevice fpDevice = getConnectedDevice();
      Log.d(TAG, "Wait for Finger Clear ");
      if (fpDevice == null) {
        return;
      }
      BioDeviceStatus status = fpDevice.waitForFingerClear_asynch(mTimeOut, this);
      if (status == BIOSDK_OK) {
        mIsWaitForFingerClearRunning = true;
      }
    } else {
      mCaptureInProgress = false;
      if (goingForEnroll == true) {
        Log.d(TAG, "Goign for enrollment capture ");
        displayResults();
      } else {
        Log.d(TAG, "Going for Verify Capture");
        VerifyTemplate();

        //Check whether it matches with the current Template
        //ListofFingersVerify();
      }

    }
  }

  @Override
  public void bioSDKWaitForFingerClearComplete(BioDeviceStatus var1) {
    Log.d(TAG, "Chand:bioSDKWaitForFingerClearComplete checking ");
    mCaptureInProgress = false;
    displayResults();
  }

  private void displayResults() {
    Log.d(TAG, "Chand:displayResults");
    // mEnrollButton.setText("ENROLL");
    // mFingerFeedbackTxtView.setText("");
    mIsWaitForFingerClearRunning = false;
    //ImageTools.drawMinutiae(mFingerImage, mTemplate);
    //mFingerImageView.setImageBitmap(mFingerImage);
    int percent = 0;
    String realFinger = "";
    int textColor = 0;
    //  Resources res = getResources();
    //  Drawable dRed = res.getDrawable(R.drawable.curved_progress_bar_red);
    //  Drawable dGreen = res.getDrawable(R.drawable.curved_progress_bar_green);
    if (mPADResult == 1) {
      //    mRealFingerProgressBar.setProgressDrawable(dGreen);
      //   textColor = mColorGreen;
      realFinger = "Genuine";
      percent = 100;
      Log.d(TAG, "It is Geniuine ");
    } else {
      //   mRealFingerProgressBar.setProgressDrawable(dRed);
      //   textColor = mColorRed;
      realFinger = "Impostor";
      percent = 15;
      Log.d(TAG, "It is Impostor");
    }
    //  mRealFingerProgressBar.setProgress(percent);
    //  mRealFingerTxtView.setText(realFinger);
    //  mRealFingerTxtView.setTextColor(textColor);
    onEnrollmentFinished(mTemplate);

    Log.d(TAG, "Got the First Template - Now check the second template ");
    Log.d(TAG, "Going for the Verification ");

    Log.d(TAG,"Current Template Count is "+TemplateCount);
    if (TemplateCount >= 3)
    {
        Log.d(TAG,"Completed Enrolling 3 Templates ");
        Log.d(TAG,"Template Count is "+TemplateCount);
  //      Log.d(TAG, "No Match it is ");

       UpdateFingerPrints();

    } else {
      Log.d(TAG,"Now Going for the new Enrollment");

      Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
        @Override
        public void run() {
          Log.d(TAG, "Calling onEnroll ");
          onEnroll();
        }
      }, 3000);
    }
    /*User user= ListofFingersVerify();
    if (user == null) {
      //    clearBioData();
      Log.d(TAG,"User is null & it is not authorized");
      Intent i = new Intent(this, NotAuthorizedActivity.class);
      finish();
      Bundle bundle = new Bundle();
      bundle.putParcelable("userData", user);
      i.putExtras(bundle);
      comeOutofApp = true;
      startActivity(i);
      terminate();
      // finishAffinity();
    } else {
      Log.d(TAG,"User is Available and it is Authorized");

      Intent intent = new Intent(getApplicationContext(), FaceSuccessActivity.class);//SuccessActivity
      if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3)
        intent = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);

      intent.putExtra("case", "success");
      //    intent.putExtra("FromWeb", true);

      intent.putExtra("personName", user.getUsername());

      String personID = user.getUserId();

      Log.d(TAG, "Add User " + user.getUsername());

      Bundle bundle = new Bundle();
      bundle.putParcelable("User", user);
      intent.putExtras(bundle);


      if (personID != null)
        intent.putExtra("personID", personID);
      Log.d(TAG, "Check IN status is " + personID);

      // finishAffinity();
      startActivity(intent);

      notauthorized=false;
      terminate();
      //finish();
    }*/
    //GoforVerify();
  }

  public void resetGUI() {
    //  mFingerImageView.setImageDrawable(null);
    //  mRealFingerProgressBar.setProgress(0);
    //  mRealFingerTxtView.setText("");
    //  mFingerFeedbackTxtView.setText("");
  }

  /*
      @Override
      public void onAttach(Context context) {
          super.onAttach(context);
          if (context instanceof IFragmentListener) {
              mListener = (IFragmentListener) context;
          } else {
              throw new RuntimeException(context.toString()
                      + " must implement OnFragmentInteractionListener");
          }
      }

      @Override
      public void onDetach() {
          super.onDetach();
          mListener = null;
      }


      @Override
      public void onStart() {
          super.onStart();

      }


      @Override
      public void onHiddenChanged(boolean hidden) {
          if (hidden) {
              BioSDKDevice device = mListener.getConnectedDevice();
              if (device != null) {
                  //        mFingerFeedbackTxtView.setText("");
                  //      mEnrollButton.setText("ENROLL");
                  mCaptureInProgress = false;
                  device.cancel_async();
              }
          }
      }
  */
  @Override
  public void onUpdateProgress(int percent) {
    //  mInitProgressBar.setProgress(percent);
  }

  @Override
  public void onEnumerateFinished(BioDeviceStatus result) {
    Log.d(TAG, "ChandonEnumberatedFinished " + result);

    mStatus = result;
    switch (result) {
      case BIOSDK_OK:
      case BIOSDK_ERROR_ALREADY_INITIALIZED:
        finishFragment();
        break;
      case BIOSDK_ERROR_INTERNAL:
      case BIOSDK_ERROR_USER_DENIED_PERMISSIONS:
        popupDialog("App Needs USB Permissions", "Exit", true);
        break;
      case BIOSDK_ERROR_NO_DEVICE_PRESENT:
        Log.d(TAG, "ChandNo Device Present ");
        popupDialog("No Supported Device Found", "Exit", true);
        break;
      case BIOSDK_ERROR_SYSTEM_PERMISSIONS:
        popupDialog("Device open in another application", "Exit", true);
        break;
      default:
        popupDialog("Device open in another application", "Exit", true);
        break;
    }
  }

  private void finishFragment() {
    Log.d(TAG, "finishFragment ");
    // mStatus;
    onInitializationFinished(mStatus);
  }

//}

  private void terminate() {
    Log.d(TAG, "Terminate ");
    BioSDKFactory.releaseBioSDKAPI();
    finish();
    return;
  }

  @Override
  public void onInitializationFinished(BioDeviceStatus status) {
    Log.d(TAG, "Chand InitializedFinished ");
    if (status != BIOSDK_OK) {
      BioSDKFactory.releaseBioSDKAPI();
      finish();
      return;
    }
    if (false == initializeDevice()) {
      return;
    }
    // Default to the Enroll Fragment
//        mFragmentMgr.beginTransaction().hide(mActiveFragment).show(mEnrollFragment).commitAllowingStateLoss();
//        mActiveFragment = mEnrollFragment;
    return;
  }

  @Override
  public void onEnrollmentFinished(byte[] template) {
    // In a real integration, this enrollment template would be persisted in a database.  For
    // this source code example, we just send it to the Verify Fragment.
    //      mVerifyFragment.setProbeTemplate(template);
    Log.d(TAG, "Saving the Enrollment ");

    if (TemplateCount == 0) {
      mTmplCapture=template;

    } else
    if (TemplateCount ==1)
      mTmplCapture1=template;
    else
      mTmplCapture2=template;
 // }

    TemplateCount++;
    showImage(TemplateCount);

    playBeep();

    Log.d(TAG,"TemplateCount is "+TemplateCount);
    mProbeTemplate = template;
   // goingForEnroll = false;
    Log.d(TAG, "Going for the Enroll ");
    Log.d(TAG, "Chand Enrollment finished Got the Template ");
  }

  @Override
  public void onTerminate(String msg) {
    Log.d(TAG, "Chand onTerminate ");
    popupDialog(msg, "Exit", true);
  }


  @Override
  public BioSDKDevice getConnectedDevice() {
    Log.d(TAG, "Get deviceConnectedDevice ");
    return mFPDevice;
  }

  @Override
  public Void device_connected() {
    Log.d(TAG, "ChandDevice_connected ");
    return null;
  }

  @Override
  public Void device_disconnected() {
    Log.d(TAG, "Chand device_disconnected ");
    mFPDevice = null;
    popupDialog("Device disconnected!", "Exit", true);
    return null;
  }

  public void handleBioDeviceStatus(BioDeviceStatus status) {

    Log.d(TAG, "Chand:handleBioDeviceStatus ");

    switch (status) {
      case BIOSDK_OK: {
        // Do nothing
      }
      break;
      case BIOSDK_TIMEOUT: {
        popupDialog("Capture Timed Out", "OK", false);
      }
      break;
      case BIOSDK_CANCELLED: {
        popupDialog("Capture Cancelled", "OK", false);
      }
      break;
      case BIOSDK_ERROR_ALREADY_INITIALIZED: {
        popupDialog("SDK Already Initialized", "OK", false);
      }
      break;
      case BIOSDK_ERROR_NOT_INITIALIZED: {
        popupDialog("SDK Not InitializedExiting: Exiting", "Exit", true);
      }
      break;
      case BIOSDK_ERROR_NO_DATA: {
        popupDialog("No Data", "OK", false);
      }
      break;
      case BIOSDK_ERROR_PARAMETER: {
        popupDialog("Bad Parameter", "OK", false);
      }
      break;
      case BIOSDK_ERROR_THREAD: {
        popupDialog("Thread Error", "OK", false);
      }
      break;
      case BIOSDK_ERROR_PROCESSING: {
        popupDialog("Processing Error", "OK", false);
      }
      break;
      case BIOSDK_ERROR_ASYNC_TASK_RUNNING: {
        popupDialog("Async Task Running", "OK", false);
      }
      break;
      case BIOSDK_ERROR_INTERNAL: {
        popupDialog("Internal Error: Exiting", "Exit", true);
      }
      break;
      case BIOSDK_ERROR_USER_DENIED_PERMISSIONS: {
        popupDialog("App Needs USB Permissions: Exiting", "Exit", true);
      }
      break;
      case BIOSDK_ERROR_SYSTEM_PERMISSIONS: {
        popupDialog("System Permission Error: Exiting", "Exit", true);
      }
      break;
      case BIOSDK_ERROR_NO_DEVICE_PRESENT: {
        popupDialog("No Supported Device Found: Exiting", "Exit", true);
      }
      break;
      case BIOSDK_ERROR_ENROLLMENTS_DO_NOT_MATCH: {
        popupDialog("Enrollments Do Not Match", "OK", false);
      }
      break;
      case BIOSDK_FINGER_PRESENT: {
        popupDialog("Finger is Present", "OK", false);
      }
      break;
      default: {
        popupDialog("Unknown Error", "OK", false);
      }
      break;
    }
  }

  private boolean initializeDevice() {

    Log.d(TAG, "Chand:initializeDevice ");
    try {
      Log.d(TAG, "in initializeDevice ");
      BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
      if (bAPI == null) {
        popupDialog("No device connected.", "Exit", false);
        return false;
      }
      //The one-time open of the device
      mFPDevice = bAPI.openDevice(0);
      if (mFPDevice == null) {
        popupDialog("For Open No device connected.", "Exit", false);
        return false;
      }
      Log.d(TAG, "Before set BioSDKDeviceListener ");
      mFPDevice.setBioSDKDeviceListener(this);
    //  Toast.makeText( this, "Device Got connected ", Toast.LENGTH_SHORT).show();

      //popupDialog("Device connected.", "ComeOut", false);

      Log.d(TAG, "Going to start the Enroll ");
            /*Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Log.d(TAG, "Calling onEnroll ");
                    onEnroll();
                }
            }, 4000);
*/
      onEnroll();
      return true;
    } catch (Exception obj) {
      Log.d(TAG, "initializeDevice " + obj.getMessage());
      popupDialog("InitializeDevice No device connected.", "Exit", false);
    }
    return false;
  }

  public void popupDialog(String msg, String btn, final boolean terminal) {

    Log.d(TAG, "Chand:popupDialog");
    // custom dialog
    final Dialog dialog = new Dialog(this);
    dialog.setContentView(R.layout.alert_dialog);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
    dialogButton.setText(btn);
    TextView tv = dialog.findViewById(R.id.text);
    tv.setText(msg);
    // if button is clicked, close the custom dialog
    dialogButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
        if (terminal) terminate();
      }
    });
    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        dialog.dismiss();
        if (terminal) terminate();
      }
    });
  //  dialog.show();
  }

  private void onEnroll() {
    Log.d(TAG, "in onEnroll ");
    BioSDKDevice fpDevice = getConnectedDevice();
    //fpDevice.getLastImage()

    if (fpDevice == null) {

      Log.d(TAG, "in onEnroll Failure");
      popupDialog("Fingerprint Device is null", "OK", false);
      return;
    } else
      Log.d(TAG, "in onEnroll Success");

    if (mCaptureInProgress == true) {
      Log.d(TAG, "capture in progress come out");
      mCancelCapture = true;
      //   mFingerFeedbackTxtView.setText("");
      //   mEnrollButton.setText("ENROLL");
      mCaptureInProgress = false;
      return;
    }
    Log.d(TAG, "Going for setConfiguration State ");
    Map<String, String> strMapSecurityLevel = new HashMap<>();
    strMapSecurityLevel.put("matching_security_level", mMatchLevel);
    strMapSecurityLevel.put("pad_security_level", mPADLevel);
    BioDeviceStatus status = fpDevice.setConfigurationState(strMapSecurityLevel);
    if (status != BioDeviceStatus.BIOSDK_OK) {
      // mListener.popupDialog("SetConfigState returned " + status.toString() + "(" + mMatchLevel + " " + mPADLevel + ")", "Exit",true);
      return;
    }
    BioDeviceStatus mStatus = fpDevice.capture_async(mTimeOut, this);
    if (mStatus != BIOSDK_OK) {
      //  mFingerFeedbackTxtView.setText("");
      //  mEnrollButton.setText("ENROLL");
      mCaptureInProgress = false;
      Log.d(TAG, "It is not Success ");
      handleBioDeviceStatus(mStatus);
    } else
      Log.d(TAG, "Enroll is success ");
    resetGUI();
    // mFingerFeedbackTxtView.setText("");
    // mEnrollButton.setText("CANCEL");
    mCaptureInProgress = true;
  }

  public void setTimeOut(int timeOut) {
    mTimeOut = timeOut;
  }

  public void setMatchLevel(String matchLevel) {
    mMatchLevel = matchLevel.toUpperCase();
  }

  public void setPADLevel(String padLevel) {
    mPADLevel = padLevel.toUpperCase();
  }

  public void setWaitForFingerClear(boolean waitForFingerClear) {
    mWaitForFingerClear = waitForFingerClear;
  }

  private void VerifyTemplate() {
//        mVerifyButton.setText("VERIFY");
    //   mFingerFeedbackTxtView.setText("");
    //   mWaitForFingerClearRunning = false;
    //   mFingerFeedbackTxtView.setText("");
    //ImageTools.drawMinutiae(mFingerImage, mTemplate);
    //   mFingerImageView.setImageBitmap(mFingerImage);
    int percent = 0;

    String match = "";
    String fealFinger = "";

    int txtColorMatch = 0;
    int txtColorRealFinger = 0;

    // Resources res = getResources();
    // Drawable dRed = res.getDrawable(R.drawable.curved_progress_bar_red);
    // Drawable dGreen = res.getDrawable(R.drawable.curved_progress_bar_green);

    Log.d(TAG, "Going for Verify Template ");
    if (mPADResult == 1) {
      //   mRealFingerProgressBar.setProgressDrawable(dGreen);
      fealFinger = "Genuine";
      // txtColorRealFinger = mColorGreen;
      percent = 100;
      Log.d(TAG, "It is Genuine Verify ");
    } else {
      //  mRealFingerProgressBar.setProgressDrawable(dRed);
      fealFinger = "Impostor";
      //  txtColorRealFinger = mColorRed;
      percent = 15;

      Log.d(TAG, "It is Impostor Verify ");
    }
    //mRealFingerProgressBar.setProgress(percent);
    //mRealFingerTxtView.setText(fealFinger);
    //mRealFingerTxtView.setTextColor(txtColorRealFinger);
    int matchScore = 0;
    if (mProbeTemplate != null) {

      Log.d(TAG, "ProbleTemplate is not null");
      BioSDKDevice fpDevice = getConnectedDevice();
      if (fpDevice == null) {
        return;
      }
      Log.d(TAG, "Going for Match previous Template ");
      BioDeviceStatus mStatus = fpDevice.match(mProbeTemplate, mTemplate);

      if (mStatus == BIOSDK_OK) {
        Log.d(TAG, "Beforee matchScore ");
        matchScore = fpDevice.getLastMatchResult();

        Log.d(TAG, "MatchSCore value is " + matchScore);
        if (matchScore == 1) {
          //      mMatchProgressBar.setProgressDrawable(dGreen);
          //      mMatchProgressBar.setProgress(100);
          match = "Match";
          Log.d(TAG, "Match it is  ");

          Toast.makeText(getApplicationContext(), "Matching Templates", Toast.LENGTH_SHORT).show();

          //      txtColorMatch = mColorGreen;
        } else {
          //    mMatchProgressBar.setProgressDrawable(dRed);
          //    mMatchProgressBar.setProgress(15);
          match = "No Match";

          Log.d(TAG, "No Match it is ");
          Toast.makeText(getApplicationContext(), "Not matching Templates", Toast.LENGTH_SHORT).show();

          //    txtColorMatch = mColorRed;
        }
      }
    }
    //   mMatchTxtView.setText(match);
    //   mMatchTxtView.setTextColor(txtColorMatch);
  }

  private boolean initEngine() {
    Log.d(TAG, "ChandinitEngine");
    BioSDKAPI api = BioSDKFactory.getBioSDK();
    if (api == null) {
      Log.d(TAG, "API is null and coming out ");
      return false;
    } else
      Log.d(TAG, "ChandgetBioSDK is successful ");
    api.enumerateDevices(this);
    return true;
  }

  private void GoforVerify() {

    Log.d(TAG, "GoforVerify ");

    BioSDKDevice fpDevice = getConnectedDevice();
    if (fpDevice == null) {
      popupDialog("Fingerprint Device is null", "OK", false);
      return;
    }
    if (mCaptureInProgress == true) {
      Log.d(TAG, "For Verification inProgress is true ");
      mCancelCapture = true;
      //   mFingerFeedbackTxtView.setText("");
      //   mVerifyButton.setText("VERIFY");
      mCaptureInProgress = false;
      return;
    }
    Map<String, String> strMapSecurityLevel = new HashMap<>();
    strMapSecurityLevel.put("matching_security_level", mMatchLevel);
    strMapSecurityLevel.put("pad_security_level", mPADLevel);
    BioDeviceStatus status = fpDevice.setConfigurationState(strMapSecurityLevel);
    if (status != BioDeviceStatus.BIOSDK_OK) {
      popupDialog("SetConfigState returned " + status.toString() + "(" + mMatchLevel + " " + mPADLevel + ")", "Exit", true);
      return;
    }
    // In this example we call capture_async().  Note: we could call verify_async() and pass in
    // mProbeTemplate instead of calling match() in displayResults() method.
    mStatus = fpDevice.capture_async(mTimeOut, this);
    if (mStatus != BIOSDK_OK) {
      // mFingerFeedbackTxtView.setText("");
      // mVerifyButton.setText("VERIFY");
      mCaptureInProgress = false;
      handleBioDeviceStatus(mStatus);
    }
    resetGUI();
    //mFingerFeedbackTxtView.setText("");
    // mVerifyButton.setText("CANCEL");
    mCaptureInProgress = true;


  }

  private void GoforTemplateVerification() {

    Log.d(TAG, "GoforVerify ");

    BioSDKDevice fpDevice = getConnectedDevice();
    if (fpDevice == null) {
      popupDialog("Fingerprint Device is null", "OK", false);
      return;
    }
    if (mCaptureInProgress == true) {
      Log.d(TAG, "For Verification inProgress is true ");
      mCancelCapture = true;
      //   mFingerFeedbackTxtView.setText("");
      //   mVerifyButton.setText("VERIFY");
      mCaptureInProgress = false;
      return;
    }
    Map<String, String> strMapSecurityLevel = new HashMap<>();
    strMapSecurityLevel.put("matching_security_level", mMatchLevel);
    strMapSecurityLevel.put("pad_security_level", mPADLevel);
    BioDeviceStatus status = fpDevice.setConfigurationState(strMapSecurityLevel);
    if (status != BioDeviceStatus.BIOSDK_OK) {
      popupDialog("SetConfigState returned " + status.toString() + "(" + mMatchLevel + " " + mPADLevel + ")", "Exit", true);
      return;
    }
    // In this example we call capture_async().  Note: we could call verify_async() and pass in
    // mProbeTemplate instead of calling match() in displayResults() method.
    mStatus = fpDevice.capture_async(mTimeOut, this);
    if (mStatus != BIOSDK_OK) {
      // mFingerFeedbackTxtView.setText("");
      // mVerifyButton.setText("VERIFY");
      mCaptureInProgress = false;
      handleBioDeviceStatus(mStatus);
    }
    resetGUI();
    //mFingerFeedbackTxtView.setText("");
    // mVerifyButton.setText("CANCEL");
    mCaptureInProgress = true;

  }

  private User ListofFingersVerify() {

    int j;
    int score1=4,score2=4,score3=4;
    List<User> users = appDatabase.userDao().getAll();

    long currTimemillisec = System.currentTimeMillis();
    long Timeinmillisec = 0;

    Log.d(TAG, "Users Count is " + users.size());
    int currCount = 0;
    BioSDKDevice fpDevice = getConnectedDevice();
    Log.d(TAG, "Wait for Finger Clear ");
    if (fpDevice == null) {
      Log.d(TAG,"fpDevice is null coming out ");
      return null;
    }
    Log.d(TAG,"Going for users Verify Template "+mProbeTemplate.toString());

    for (User user : users) {
      try {
        currCount++;
        Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
        currTimemillisec = System.currentTimeMillis();

        //Log.d(TAG,"Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);

        j = 1;
        //Log.d(TAG, "doInBackground: " + user.toString());
        String fingarprint1 = user.getTemplate1();
        byte[] mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);
        Timeinmillisec = System.currentTimeMillis() - currTimemillisec;
        currTimemillisec = System.currentTimeMillis();
        //Log.d(TAG,"decode Curr Milli sec "+currTimemillisec+ "  diff  "+ Timeinmillisec);
        //Each Match Taking 66 milliseconds
        // rc = mVCOM.Match(mProbeTemplate, mTmplCapture1);

        Log.d(TAG,"Template Capture is "+mTmplCapture1);

        BioDeviceStatus mStatus = fpDevice.match(mProbeTemplate, mTmplCapture1);
        Log.d(TAG,"First Template Status is "+mStatus);
        fingarprint1 = user.getTemplate2();
        //byte[] mTmplCapture2 = Base64.decode(fingarprint1, Base64.DEFAULT);
        mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);
        BioDeviceStatus mStatus2 = fpDevice.match(mProbeTemplate, mTmplCapture1);
        score2= fpDevice.getLastMatchResult();
        fingarprint1 = user.getTemplate3();
        //byte[] mTmplCapture3 = Base64.decode(fingarprint1, Base64.DEFAULT);
        mTmplCapture1 = Base64.decode(fingarprint1, Base64.DEFAULT);
        BioDeviceStatus mStatus3 = fpDevice.match(mProbeTemplate, mTmplCapture1);
        score3= fpDevice.getLastMatchResult();


        if (mStatus == BIOSDK_OK || mStatus2 == BIOSDK_OK || mStatus3 == BIOSDK_OK ) {
          Log.d(TAG,"SDK is success ");
          score1= fpDevice.getLastMatchResult();
          if (score1 == 1 || score2 == 1 || score3 == 1) {
            Log.d(TAG,"It is success matching the template ");


                   /*     Intent intent = new Intent(getApplicationContext(), FaceSuccessActivity.class);//SuccessActivity
                        if (GlobalVariables.WorkFlowVal == 1 || GlobalVariables.WorkFlowVal == 2 || GlobalVariables.WorkFlowVal == 3)
                            intent = new Intent(getApplicationContext(), TerminalFaceCheckInActivity.class);

                        intent.putExtra("case", "success");
                        //    intent.putExtra("FromWeb", true);

                        intent.putExtra("personName", user.getUsername());

                        String personID = user.getUserId();

                        Log.d(TAG, "Add User " + user.getUsername());


                        Bundle bundle = new Bundle();
                        bundle.putParcelable("User", user);
                        intent.putExtras(bundle);


                        if (personID != null)
                            intent.putExtra("personID", personID);
                        Log.d(TAG, "Check IN status is " + personID);

                        startActivity(intent);

                        notauthorized=false;
                   */

            Log.d(TAG,"going for Clearn Async ");
                     /*   BioDeviceStatus status = fpDevice.waitForFingerClear_asynch(mTimeOut, this);
                        if (status == BIOSDK_OK) {
                            Log.d(TAG,"Clear Asyn success");
                            mIsWaitForFingerClearRunning = true;
                        }
*/
            Log.d(TAG,"Going for Wait Clear ");
            fpDevice.waitForFingerClear(mTimeOut,this);
            Log.d(TAG,"come out of wait for Finger clear ");
            fpDevice.clearBioData();
           // BioSDKAPI bAPI = BioSDKFactory.getBioSDK();
 /*
                        if (bAPI != null)
                            bAPI.closeDevices();
                        else
                            Log.d(TAG,"Device is already closed ");*/
            return user;
            //break;
            //finish();
          }
        } else
          notauthorized=true;
      } catch (Exception obj) {
        Log.d(TAG, "Got Exception while reading fingerprint"+obj.getMessage());
        notauthorized = true;
      }
    }

      /*  Log.d(TAG,"going for Clearn Async Next ");
        BioDeviceStatus status = fpDevice.waitForFingerClear_asynch(mTimeOut, this);
        if (status == BIOSDK_OK) {
            Log.d(TAG,"Clear Asyn success");
            mIsWaitForFingerClearRunning = true;
        }

*/
    //       fpDevice.clearBioData();
    return null;
  }
  private void UpdateFingerPrints() {

    Log.d(TAG,"Update Finger Prints ");
    if (mTmplCapture1 != null && mTmplCapture1.length > 0 && mTmplCapture != null && mTmplCapture.length > 0 && mTmplCapture2 != null && mTmplCapture2.length > 0) {


      JSONObject jsonObject = new JSONObject();


      try {

        Log.d(TAG,"Added User template timestamp ");
        String tem1 = Base64.encodeToString(mTmplCapture, Base64.DEFAULT);

        String tem2 = Base64.encodeToString(mTmplCapture1, Base64.DEFAULT);

        String tem3 = Base64.encodeToString(mTmplCapture2, Base64.DEFAULT);

        jsonObject.put("uid", userid);
//need username  capture from ldap api(uid), this is needed for logging in
        //check_userid_ldap

        if (ldapUser != null)
          jsonObject.put("username", ldapUser);


        Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long currts = (curCalendar.getTimeInMillis() / 1000L);
        jsonObject.put("user_template_timestamp", currts);
        Log.d(TAG,"Timestamp to be updated for timestamp "+currts);

        jsonObject.put("template_1", tem1);
        jsonObject.put("template_2", tem2);
        jsonObject.put("template_3", tem3);

        // if (user.getTemplateFace() !=null)
        //   jsonObject.put("templateface", user.getTemplateFace());

        if (GlobalVariables.supervisorUserID != -1)
          jsonObject.put("supervisor_userid", GlobalVariables.supervisorUserID);

        GlobalVariables.supervisorUserID= -1;
        Log.d(TAG, "onPostExecute: " + jsonObject);

        if (updateUser == true) {
          Log.d(TAG,"Going for Update User Templates ");
          user.setTemplate1(tem1);
          user.setTemplate2(tem2);
          user.setTemplate3(tem3);
          //   AppDatabase appDatabase = AppDatabase.getAppDatabase(FingerPrintEnrollActivity.this);
          // if (appDatabase.userDao().countUsers(user1.get_id()) != null) {
          appDatabase.userDao().update(user);

          Log.d(TAG,"Sending Update Template Response ");
          //new WebUtils().SendingMessageWithStateWeb(getApplicationContext(),"UserFingerTemplateResponse",null,"FingerPrint_Verify");

          MyProgressDialog.dismiss();
          Log.d(TAG,"Staerting Verifysuccessenroll "+ldapUser);
           Intent i = new Intent(M21FingerPrintEnrollActivity.this, FingerPrintEnrollSuccessActivityNew.class);
        //  Intent i = new Intent(M21FingerPrintEnrollActivity.this, VerifyEnrolSuccuessorNotActivity.class);
      //    Intent i = new Intent(M21FingerPrintEnrollActivity.this, Main2Activity.class);

          i.putExtra("userid", userid);
          if (ldapUser != null)
            i.putExtra("ldapUser", ldapUser);
          if (user != null ) {
            Bundle bn = new Bundle();
            bn.putParcelable("userData", user);
            i.putExtras(bn);
          }
          // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
          startActivity(i);

        } else {


          Log.d(TAG,"Updating to the Server ");
          MyProgressDialog.show(M21FingerPrintEnrollActivity.this, R.string.wait_message);
          String url = GlobalVariables.SSOIPAddress + Constants.UPDATER_USERFINGERPRINT;

          Log.d(TAG,"Please Wait "+url);

          new VolleyService(M21FingerPrintEnrollActivity.this, M21FingerPrintEnrollActivity.this).tokenBase(POST, url, jsonObject, "100");

        }
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
  }
  }

