package lumidigm.captureandmatch.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import java.util.List;

import lumidigm.captureandmatch.entity.User;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAll();


    @Query("SELECT * from user _id Where :id1")
    User countUsers(String id1);


    @Query("SELECT * from user Where userId=:userId1")
    User countUsersBasedonUserID(String userId1);


    @Insert
    void insertAll(User... users);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<User> users);

    @Update
    void update(User users);

    @Delete
    void delete(User user);

    @Query("DELETE FROM user")
    void deleteAll();


}
