/*
 *
 * You are free to use this example code to generate similar functionality
 * tailored to your own specific needs.
 *
 * This example code is provided by Lumidigm Inc. for illustrative purposes only.
 * This example has not been thoroughly tested under all conditions.  Therefore
 * Lumidigm Inc. cannot guarantee or imply reliability, serviceability, or
 * functionality of this program.
 *
 * This example code is provided by Lumidigm Inc. â€œAS ISâ€ and without any express
 * or implied warranties, including, but not limited to the implied warranties of
 * merchantability and fitness for a particular purpose.
 *
 */

package lumidigm.vcomdroid;

/**
 *
 * @author rmckee
 */
public class Minutiae {
    public Minutiae() {}
    public int nX;
    public int nY;
    public int nType;
    public int nDPI;
    public double nRotAngle;
    public int nCoreX;
    public int nCoreY;
}
