package lumidigm.vcomdroid;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class VCOM
{
    static
    {
        System.loadLibrary("VCOMDroid");
    }

    private static final String TAG = "[VCOM]";
    private static final String ACTION_USB_PERMISSION = "com.lumidigm.vcom.USB_PERMISSION";
    private Context mContext;
    private UsbDeviceConnection mConn = null;
    PermissionSuccuess permissionSuccuess;
    public VCOM(Context context,PermissionSuccuess permissionSuccuess)
    {
        mContext = context;
        this.permissionSuccuess=permissionSuccuess;
        Log.d(TAG,"Called FROM VCOM");
        FindDevice();

    }
    public VCOM(Context context)
    {
        mContext = context;
        FindDevice();
        Log.d(TAG,"Called FROM VCOM simple");
    }

    // Open, Close and Ping
    // Open  opens the device behind the JNI layer.
    // Close closes the device behind the JNI Layer.
    // Ping  attempts to do a quick round-trip I/O to the device and will Close/Open as necessary.
    //       this should be called first by any higher-layer abstraction (such as Capture()).
    //       doing this will obviate the need for dealing with power management / sketchy USB ports, etc.
    public int Open()
    {
        if (mConn == null || mConn.getFileDescriptor() == -1)
        {
            Log.d(TAG,"Called from Open");
            FindDevice();
            if (mConn == null || mConn.getFileDescriptor() == -1)
            {
                return -1;
            }
        }
        int rc;
        rc = JNI_Open(mConn.getFileDescriptor());
        if (rc != 0)
        {
            mConn.close();
            Log.i(TAG, "Unable to open device.");
        }
        else
        {
            Log.i(TAG, "Device opened.");
        }
        return rc;
    }
    public void Close()
    {
        JNI_Close();
        if (mConn != null)
        {
            mConn.close();
            mConn = null;
        }
        Log.i(TAG, "Device closed.");
    }
    public int Ping()
    {
        int rc;
        rc = JNI_Ping();
        if (rc == 0)
            return 0;
        Log.i(TAG, "Ping failed.");
        Close();
        Log.d(TAG,"Calleed from Ping ");
        FindDevice();
        if (mConn == null || mConn.getFileDescriptor() == -1)
            return -1;
        rc = Open();
        if (rc != 0)
            return -2;
        return 0;
    }

    // Capture - capture a composite image and its derived template. 
    public int Capture()
    {
        int rc;
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_Capture();
        return rc;
    }

    // Match - perform the matching algorithm on two templates.
    public int Match(byte [] tmpl1, byte [] tmpl2)
    {
        int rc;
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_Match(tmpl1, tmpl2);
        return rc;
    }

    // Match - perform the matching algorithm on two templates.
    public int AddDatabase(int userID, byte [] tmpl1, byte [] tmpl2,byte [] tmpl3)
    {
        int rc;
        Log.d(TAG,"in AddDatabase ");
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_AddDatabase(userID, tmpl1, tmpl2,tmpl3);
        return rc;
    }
    // Match - perform the matching algorithm on two templates.
    public int DeleteRecordDatabase(int userID)
    {
        int rc;
        Log.d(TAG,"in DeleteRecordDatabase ");
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_DeleteRecordDatabase(userID);
        return rc;
    }
    public int CreateDatabase(int groupID, int numInstancesperFinger,int nFingerPerUser,int numUsers)
    {
        int rc;
        Log.d(TAG,"in CreateDatabase ");
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_CreateDatabase(groupID,numInstancesperFinger,nFingerPerUser,numUsers);
        return rc;
    }
    public int DeleteDatabase(int groupID)
    {
        int rc;
        Log.d(TAG,"in DeleteDatabase ");
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_DeleteDatabase(groupID);
        return rc;
    }
    public int SetWorkingDB(int groupID)
    {
        int rc;
        Log.d(TAG,"in SetWorkingDB ");
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_SetWorkingDatabase(groupID);
        return rc;
    }

    public int EnrollUserRecordDatabase(int groupID)
    {
        int rc;
        Log.d(TAG,"in EnrollUserRecordDatabase ");
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_EnrollUserRecordDatabase(groupID);
        return rc;
    }

    public int VerifyUserRecordDatabase(int groupID)
    {
        int rc;
        Log.d(TAG,"in SetWorkingDB ");
        rc = Ping();
        if (rc != 0)
            return -1;
        rc = JNI_VerifyUserRecordDatabase(groupID);
        return rc;
    }


    // Size of composite image - may be called before Capture()
    // if any of these return 0, we don't have a connected device.
    public int GetCompositeImageSizeX()
    {
        return JNI_GetCompositeImageSizeX();
    }
    public int GetCompositeImageSizeY()
    {
        return JNI_GetCompositeImageSizeY();
    }
    public int GetCompositeImageSize()
    {
        return GetCompositeImageSizeX() * GetCompositeImageSizeY();
    }

    // Spoof score for current composite image - only valid after a Capture()
    public int GetSpoofScore()
    {
        return JNI_GetSpoofScore();
    }

    // Match score from last match operation
    public int GetMatchScore()
    {
        return JNI_GetMatchScore();
    }

    // Return the composite image as an integer array of Color.
    // i.e. alpha << 24 | red << 16 | green << 16 | blue
    public int GetCompositeImage(int [] colors)
    {
        if (colors.length != GetCompositeImageSize())
        {
            Log.i(TAG, "colors array size mismatch.");
            return -1;
        }
        int rc = JNI_GetCompositeImage(colors);
        if (rc != 0)
        {
            Log.i(TAG, String.format("GetCompositeImage failed rc %d.", rc));
            return -1;
        }
        return 0;
    }

    // Return the template - only valid after a Capture()
    public int GetTemplate(byte [] template)
    {
        if (template.length < GetTemplateSize())
            return -1;
        return JNI_GetTemplate(template);
    }
    public int GetTemplateSize()
    {
        return JNI_GetTemplateSize();
    }

    private void FindDevice()
    {
        UsbManager usbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);

        HashMap<String, UsbDevice> map = usbManager.getDeviceList();
        Collection<UsbDevice> coll = map.values();
        Iterator<UsbDevice> iter = coll.iterator();
        UsbDevice devFound = null;
        int count = 0;

        Log.d(TAG,"It is FindDevice");
        while (iter.hasNext())
        {
            UsbDevice dev = iter.next();
            int vid = dev.getVendorId();
            int pid = dev.getProductId();
            if ((vid == 0x0525 && pid == 0x3424) ||
                    (vid == 0x1fae && pid == 0x212c) ||
                    (vid == 0x1fae && pid == 0x0021) ||
                    (vid == 0x1fae && pid == 0x0020))
            {
                if (count == 0)
                    devFound = dev;
                count++;
            }
        }

        if (count == 0)
        {
            Log.i(TAG, "No device found.");
        }
        else if (count == 1)
        {
            Log.i(TAG, "Device found.");
            if (!usbManager.hasPermission(devFound))
            {
                Intent intent = new Intent(ACTION_USB_PERMISSION);
                PendingIntent permissionIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
                IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                mContext.registerReceiver(mUsbReceiver, filter);
                usbManager.requestPermission(devFound, permissionIntent);
            }
            else
            {
                mConn = usbManager.openDevice(devFound);
                if (mConn != null) {
                    Log.i(TAG, "Device FD: " + mConn.getFileDescriptor());
                    if(permissionSuccuess!=null)
                        permissionSuccuess.onPermissionAlradyDone();
                }
            }
        }
        else
        {
            Log.i(TAG, "Multiple devices not supported.");
        }
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action))
            {
                synchronized (this)
                {
                    UsbDevice dev = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (dev != null)
                    {
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false))
                        {
                            Log.i(TAG, "Permission obtained to use device " + dev + ".");
                            UsbManager usbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
                            mConn = usbManager.openDevice(dev);
                            if (mConn != null)
                            {
                                Log.i(TAG, "Device FD: " + mConn.getFileDescriptor());
                                if(permissionSuccuess!=null)
                                    permissionSuccuess.permissionsSuccuess();


                            }
                            else
                            {
                                Log.i(TAG, "USBManager failed to open device.");
                            }
                        }
                        else
                        {
                            Log.i(TAG, "Permission denied for device " + dev);
                        }
                    }
                    else
                    {
                        Log.i(TAG, "NULL USB device.");
                    }
                    context.unregisterReceiver(this);
                }
            }
        }
    };

    /*
     * native function prototypes
     */

    private native int JNI_Open(int devFD);
    private native int JNI_Close();

    private native int JNI_Ping();
    private native int JNI_Capture();
    private native int JNI_GetSpoofScore();
    private native int JNI_GetMatchScore();
    private native int JNI_GetCompositeImageSizeX();
    private native int JNI_GetCompositeImageSizeY();
    private native int JNI_GetCompositeImage(int [] image);
    private native int JNI_GetTemplateSize();
    private native int JNI_GetTemplate(byte [] tmpl);
    private native int JNI_Match(byte [] tmpl1, byte [] tmpl2);

    private native int JNI_AddDatabase(int userID, byte [] tmpl1, byte [] tmpl2, byte [] tmpl3);

    private native int JNI_CreateDatabase(int groupID,int numInstancesperFinger,int nFingerPerUser,int numUsers);
    private native int JNI_DeleteDatabase(int groupID);
    private native int JNI_SetWorkingDatabase(int groupID);

    private native int JNI_EnrollUserRecordDatabase(int groupID);
    private native int JNI_VerifyUserRecordDatabase(int groupID);

    private native int JNI_DeleteRecordDatabase(int userID);

    public interface PermissionSuccuess{
        public void permissionsSuccuess();
        public  void onPermissionAlradyDone();
    }


}
