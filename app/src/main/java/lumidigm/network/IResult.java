package lumidigm.network;

import com.android.volley.VolleyError;

public interface IResult {
    public void notifySuccess(String requestType, Object response);
    public void notifyError(String requestType, VolleyError error);


    // public void notifyError(String requestType, VolleyError error);
}
