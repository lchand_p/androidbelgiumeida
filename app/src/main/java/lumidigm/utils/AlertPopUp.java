package lumidigm.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import io.fabric.sdk.android.services.concurrency.Task;
import lumidigm.captureandmatch.activites.Main2Activity;
import lumidigm.captureandmatch.R;

public class AlertPopUp extends Dialog {

    private TextView headertext;
    public TextView txview;
    private Button ok;
//    private Integer TaskNumber=0;
    String key;
    private boolean check;
    Context context;

    public AlertPopUp(@NonNull Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datalostpopup);
        Window window;
        // dialog.setTitle("Delete Confirmation");  Window window = getWindow();

        getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);


        // getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        txview = findViewById(R.id.text);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dismiss();
                    Intent i = new Intent(context, Main2Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);

                //handler.postDelayed(this, 120000); //now is every 2 minutes
            }
        }, 100);
    }

public void setMsg(String msg){
        //txview = findViewById(R.id.text);
    try {
        Log.d("ALertPop", "Alert message is " + msg);
        txview.setText(msg);
    } catch (Exception obd) {
        Log.d("AlertPop","got Exception "+obd.getMessage());
    }
}

}