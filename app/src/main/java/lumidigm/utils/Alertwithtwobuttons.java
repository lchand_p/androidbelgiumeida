package lumidigm.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import lumidigm.captureandmatch.R;


public class Alertwithtwobuttons  extends Dialog implements View.OnClickListener {
    public OnClick onClick;
    private TextView headertext;
    private TextView txview;
    public Button ok;
public Button cencel;
    public boolean check;
    Context context;

    public Alertwithtwobuttons(@NonNull Context context, OnClick onClick) {
        super(context);
        this.onClick = onClick;
        this.context = context;
    }

    public Alertwithtwobuttons(@NonNull Context context, OnClick onClick, boolean check) {
        super(context);
        this.onClick = onClick;
        this.check = check;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two_buttondialog);
        Window window;
        // dialog.setTitle("Delete Confirmation");  Window window = getWindow();

        getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);


       // getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
       setCancelable(false);
        ok = (Button) findViewById(R.id.buttonok);
        cencel=(Button)findViewById(R.id.cancel);
        cencel.setOnClickListener(this);
        ok.setOnClickListener(this);
        headertext = (TextView) findViewById(R.id.headertext);
        txview = (TextView) findViewById(R.id.txview);
    }

    public void setHeadeandMsg(String header, String msg) {
        headertext.setText(header);
        txview.setText(msg);
    }
    public void hideHeder() {
        headertext.setVisibility(View.GONE);

    }
    public void changeButtonTexts(String okButton,String cancelButton){

        ok.setText(okButton);
        cencel.setText(cancelButton);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonok:

                    onClick.onClickOk();
                dismissDilog();
                //TODO implement
                break;
            case R.id.cancel:
                onClick.cancelButtonClick();
                dismissDilog();
                break;
        }
    }

    public void dismissDilog() {
        dismiss();

    }

    public interface OnClick {
        public void onClickOk();
        public void cancelButtonClick();

    }

    public void hideHeader(boolean check){
        if(check) {
            headertext.setVisibility(View.GONE);
            txview.setTextSize(19);
        } else {
            headertext.setVisibility(View.VISIBLE);
            txview.setTextSize(17);
        }
    }

}