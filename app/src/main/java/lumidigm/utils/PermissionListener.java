package lumidigm.utils;

import java.util.List;

public interface PermissionListener {
  void onGranted(int statuscode);

  void onDenied(List<String> deniedPermissions);
}
