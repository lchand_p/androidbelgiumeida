package lumidigm.utils;


import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.text.format.Formatter;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;

//import lumidigm.captureandmatch.activites.Main2Activity;
import database.AppDatabase;
import lumidigm.MyProgressDialog;
import webserver.GetUsersTimerService;
import webserver.GlobalVariables;

import static android.content.Context.ACTIVITY_SERVICE;
import static android.content.Context.WIFI_SERVICE;
import static lumidigm.constants.Constants.GET_ACTIVE_DETAINEE_USERS;
import static lumidigm.constants.Constants.GET_ALL_ACTIVE_VISITORS_AND_OTHERS;
import static lumidigm.constants.Constants.GET_ALL_NON_VISITOR_USERS;
import static lumidigm.constants.Constants.GET_ALL_USERS;
import static lumidigm.constants.Constants.GET_PENDING_USERS;
import static lumidigm.constants.Constants.GET_PENDING_VISITORS_AND_OTHERS;
import static lumidigm.constants.Constants.SSO_SERVER_DEVICE_STATUS;
import static webserver.GlobalVariables.FaceDetectionDetaineeIdentificationforV4;
import static webserver.GlobalVariables.GuardStation_WaitingArea;
import static webserver.GlobalVariables.TimeandAttendence;
import static webserver.GlobalVariables.mobileWorkFlowConfigure;
import static webserver.GlobalVariables.productVersion;
import static webserver.GlobalVariables.selectedProduct;
import static webserver.GlobalVariables.vcomdatabaseSupport;


/**
 * Created by Administrator on 2017/10/20 0020.
 */
public class DisplayStats {
    private static String TAG="DisplayStats";
    private static Context cntx;
    private static SharedPreferences prefs;
    private static AppDatabase appDatabase;
    public static void sendDeviceStatusInfo(Context curCtx,long lastConfigupdate,String lastConfigTimeupdate, SharedPreferences cur_prefs,AppDatabase appdb) {
        Log.d(TAG,"sendDeviceStatusInfo ");
        try {
            appDatabase = appdb;
            //   String screen_id = screen_Id;
            //  String display_name = screenNamee1;
            String display_type = "Digit9Terminal";  //DigitalSignage,Kisok,InteractiveSignage
            String category = "Finance"; //Hotel, HealthCare, Hospital
            String orientation = "horizontal"; //HoHoritonTal or Vertical

            Log.d(TAG, "Orientation is " + orientation);
            String lattitude = "1.3222";
            String longitude = "1.52333";
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            String currentDateTime = sdf.format(cal.getTime());
            String currts=null;
            cntx = curCtx;
            prefs =cur_prefs;
            try {
                Log.d(TAG,"Get Time of Day");
                currts =  Long.toString(System.currentTimeMillis() / 1000L);
                try {
                    Log.d(TAG,"Get Time of Day");
                    Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                    //Added 4 & half hours to show the Indian time on the Server - Testing - 25/07/2019
                    //Date currentTime2 = curCalendar.getInstance().getTime();
                    // currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                    currts = Long.toString(curCalendar.getTimeInMillis() /  1000L+16200);

                    String curr1      = Long.toString(System.currentTimeMillis() / 1000L);
                    // jsonObject2.put("logged_in_time_secs", currts);
                    Log.d(TAG,"Current Time stamp is GMT "+currts);
                    Log.d(TAG,"Current Time stamp is "+curr1);

                } catch (Exception obj) {

                }
                Log.d(TAG,"Current Time stamp is "+currts);
            } catch (Exception obj) {

            }

            String dwell_time = "0"; //currentDateTime;  //seconds
            Log.d(TAG, "Current Dwell Time is " + dwell_time);
            String ethnicity = "India";
            String impressions = "8000"; //from reports server should calculate
            String model = Build.MODEL; //"MT955";
            String os = Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
            //"Android";
            String processor = Build.MANUFACTURER; //"MTK500";
            String ram = "1 GB";

            Log.d(TAG, "Total Ram Size is " + totalRamMemorySize() + " Free Memory is " + freeRamMemorySize());
            ram = String.valueOf(totalRamMemorySize()) + " MB";
            String androidVersion = Build.MANUFACTURER
                    + " " + Build.MODEL + " " + Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
            Log.d(TAG, "Android details " + androidVersion);


            String Memory = getTotalExternalMemorySize(); // + " "+getAvailableInternalMemorySize();
            String storage_available = getAvailableExternalMemorySize(); //can check external Storage

            Log.d(TAG, "Total Memory is " + Memory);
            Log.d(TAG, "Available Memory is " + storage_available);
            String storage = Memory;
            ;
            try {
                ActivityManager actManager = (ActivityManager) cntx.getSystemService(ACTIVITY_SERVICE);
                ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
                actManager.getMemoryInfo(memInfo);
                long totalMemory = memInfo.totalMem;
                Log.d(TAG, "Total Memory is " + totalMemory);
            } catch (Exception obj) {

            }


            String height = "32"; //display resolution 1024x 720P
            String width = "32";
/*
            if (GlobalVariables.Width != 0)
                width = String.valueOf(GlobalVariables.Width);

            if (GlobalVariables.Height != 0)
                height = String.valueOf(GlobalVariables.Height);
*/

            Log.d(TAG, "WIndow Width is " + width + " Height is " + height);
            String rtc = "true"; //detect
            String wifi = "false";
            String ethernet = "false";
            String threeg = "false";
            String ip = null;
            String localwifiSSID = null;
          //  long lastConfigupdate=0;

            if (isWifiConnected()) {
                wifi = "true";
                ip = GetDeviceipWiFiData();
                //   localwifiSSID = wifiSSID;
                Log.d(TAG, "Wifi SSID is " + localwifiSSID);
            } else if (isEthernetConnected()) {
                ethernet = "true";
                ip = logLocalIpAddresses();
                ip = ip.substring(1);
            } else
                threeg = "true";

            Log.d(TAG, "WifiStatus is" + wifi + " Etnethernet status " + ethernet + " 3G " + threeg);
            String hdmi = "true";

            String rooted = "false";
            if (isRooted())
                rooted = "true";
            //String pkt = packet.getAddress().getHostAddress();
            Log.d(TAG, "Rooted Status is " + rooted);
            String date_installed = null; //server


            //String launcher_ver = getLauncherVersion();
            //String hdmiApp = ReadHdmiAppVersion();
            {
                Log.d(TAG,
                        "send Queured Reports");
                PackageInfo pInfo = null;
                try {
                    pInfo = cntx.getPackageManager().getPackageInfo(cntx.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                // String currVersion =

                String app_ver = pInfo.versionName;
            /*try {
                if (hdmiApp != null)
                    app_ver = app_ver + "-" + hdmiApp;
            } catch (Exception obj) {

            }*/
                Log.d(TAG, "App Version is " + app_ver);
                String mac = getMacAddr();
                Log.d(TAG,"MAC Address is "+mac);
                //String  ip_addr = "192.168.1.20";
                //	String  ip = logLocalIpAddresses(); // GetDeviceipWiFiData();
                //	String ip = GetDeviceipWiFiData();
                Log.d(TAG, "IP Addr is " + ip);


                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long msTimeSys = System.currentTimeMillis() - SystemClock.elapsedRealtime();
                Date curDateTimeSys = new Date(msTimeSys);

                Log.d(TAG, "sendUpdatedData Sys UpTime: " + formatter.format(curDateTimeSys));
                String curDateSys = formatter.format(curDateTimeSys);
                Log.d(TAG, "SysUpTIme TIme is : " + curDateSys);

                date_installed = currentDateTime; //curDateSys; //- server
                String date_identified = currentDateTime; //it is mandatory for the Server to Generate Reports
                String last_boot = curDateSys;
                String uptime = String.valueOf((SystemClock.elapsedRealtime() / 1000) / 60 * 60);

                String online = "true";
                //String playlist=GetLayoutID();//"none";  //LayoutID #
                //Log.d(TAG,"PlayList is "+playlist);

                //String storage_available=getAvailableInternalMemorySize(); //can check external Storage
                String last_crash_camera = currentDateTime;  //must be not null for server
                String hotspot = "false";  //is hotspot enable
                if (GetWifiApStatus()) {
                    hotspot = "true";
                }
                Log.d(TAG, "Hotspot is " + hotspot);

                String streaming = "false"; //Check Camera Available
          /*  if (CheckCameraStatus() ) {
                streaming = "true";
            }*/
                Log.d(TAG, "Camera Status is " + streaming);
                //com.teamviewer.quicksupport.market
                String teamviewer = "false"; //Check Team Viewer Installed or not

                try {
                 //   lastConfigupdate = cntx.prefs.getLong(GlobalVariables.last_Config_SP,0);
                    Log.d(TAG,"last Config Update was "+lastConfigupdate);

                }catch (Exception obj) {
                    lastConfigupdate=0;
                }

               // RequestQueue requestQueue = Volley.newRequestQueue(cntx);
                String URL = GlobalVariables.SSOIPAddress + SSO_SERVER_DEVICE_STATUS;
                Log.d(TAG,"Device Status URL is "+URL);
                JSONObject jsonBody = new JSONObject();
                // jsonBody.put("Title", "Android Volley Demo");
                // jsonBody.put("Author", "BNK");
                //jsonBody.put("screen_id", screen_id);
                //jsonBody.put("display_name", display_name);
                //jsonBody.put("display_type", display_type);
                //jsonBody.put("category", category);
                // jsonBody.put("orientation", orientation);
                jsonBody.put("lattitude", lattitude);
                ;
                jsonBody.put("longitude", longitude);
            /*jsonBody.put("area",area);
            jsonBody.put("city",city);
            jsonBody.put("state",state);
            jsonBody.put("country",country);
            jsonBody.put("zip_code",zip_code);
            jsonBody.put("age",age);
            report.put("footfall",footfall);
            report.put("income_group",income_group);

            report.put("dwell_time",dwell_time);
            report.put("ethnicity",ethnicity);
            report.put("impressions",impressions);
            */
                // jsonBody.put("model", model);
                jsonBody.put("model",mac);
                jsonBody.put("os", os);
                jsonBody.put("processor", processor);
                jsonBody.put("ram", ram);
                jsonBody.put("storage", storage);
/*
            jsonBody.put("height", height);
            jsonBody.put("width",width);
            report.put("rtc",rtc);*/
                jsonBody.put("wifi", wifi);
                // jsonBody.put("_3g", threeg);
                jsonBody.put("ethernet", ethernet);
                jsonBody.put("hdmi", hdmi);
                jsonBody.put("rooted", rooted);
                jsonBody.put("date_installed", date_installed);
                // jsonBody.put("date_identified", date_identified);
                jsonBody.put("date_identified", currts);
                //   jsonBody.put("launcher_ver", launcher_ver);
                jsonBody.put("app_ver", app_ver);
                jsonBody.put("mac", mac);
                jsonBody.put("ip", ip);
                jsonBody.put("last_boot", last_boot);
                jsonBody.put("uptime", uptime);

                if (lastConfigupdate !=0)
                    jsonBody.put("last_config_update", lastConfigupdate);

                if (lastConfigTimeupdate !=null)
                    jsonBody.put("last_config_timeupdate", lastConfigTimeupdate);

                final String requestBody = jsonBody.toString();


                Log.d(TAG,"Sendign Device Status "+requestBody+" URL is "+URL);

                SendDeviceStatusUpdate(jsonBody,URL);
  //              new VolleyService(cntx, cntx).tokenBase(POST, URL, jsonBody, "210");
/*
                StringRequest stringRequest = new StringRequest(POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("VOLLEY", response);


                        String RxTerminalId = null;
                        //Save the Terminal ID of the device;
                        prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                        GlobalVariables.TerminalID = prefs.getString(GlobalVariables.SP_TerminalID, Constants.SERVER_IPADDR);

                        if (RxTerminalId != null && !RxTerminalId.equals( GlobalVariables.TerminalID)) {

                            SharedPreferences.Editor editor1 = prefs.edit();

                            editor1.putString(GlobalVariables.SP_ServerIP, GlobalVariables.TerminalID);
                            Log.d(TAG,"Storing TerminalID "+GlobalVariables.TerminalID);

                            editor1.commit();
                        } else
                            Log.d(TAG,"No Change in TermnalID "+GlobalVariables.SP_TerminalID);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");

                            //og.d(TAG,"Request Body is "+requestBody.toString());

                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }

                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response.statusCode);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }
                };

                requestQueue.add(stringRequest);*/
            }
        } catch(JSONException e){
            e.printStackTrace();
        }
    }
    private static long totalRamMemorySize() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) cntx.getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.totalMem / 1048576L;
        return availableMegs;
    }
    private static long freeRamMemorySize() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) cntx.getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem / 1048576L;

        return availableMegs;
    }
    private static boolean externalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }
    private static boolean GetWifiApStatus() {
        WifiManager wifi = (WifiManager) cntx.getSystemService(WIFI_SERVICE);
        Method[] wmMethods = wifi.getClass().getDeclaredMethods();
        for (Method method : wmMethods) {
            if (method.getName().equals("isWifiApEnabled")) {

                try {
                    return (boolean) method.invoke(wifi);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private static String logLocalIpAddresses() {
        Enumeration<NetworkInterface> nwis;
        String ipAddr = null;
        try {
            nwis = NetworkInterface.getNetworkInterfaces();
            while (nwis.hasMoreElements()) {

                NetworkInterface ni = nwis.nextElement();
                for (InterfaceAddress ia : ni.getInterfaceAddresses())  {
                    try {
                        if ((ia.getNetworkPrefixLength() != 64) && (ni.getDisplayName().contains("eth0") || ni.getDisplayName().contains("wlan"))) {

                            ipAddr = ia.getAddress().toString();
                        }
                        Log.i(TAG, String.format("%s: %s/%d",
                                ni.getDisplayName(), ia.getAddress(), ia.getNetworkPrefixLength()));
                    } catch (Exception obj) {

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ipAddr;
    }
    private static String getTotalExternalMemorySize() {
        try {
            if (externalMemoryAvailable()) {
                File path = Environment.
                        getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long BlockSize = stat.getBlockSize();
                long TotalBlocks = stat.getBlockCount();
                return formatSize(TotalBlocks * BlockSize);
            } else {
                return getAvailableInternalMemorySize();
            }
        } catch (Exception obj) {
            return null;
        }
    }
    private static String  getAvailableExternalMemorySize() {
        try {
            if (externalMemoryAvailable()) {
                File path = Environment.getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long blockSize = stat.getBlockSize();
                long availableBlocks = stat.getAvailableBlocks();
                return  formatSize(availableBlocks * blockSize);
            } else {
                return 	getTotalInternalMemorySize(); // + " "+getAvailableInternalMemorySize();

            }
        } catch (Exception obj) {
            return null;
        }
    }
    private static String getAvailableInternalMemorySize() {
        Log.d("Display Stats"," in getAvailableInternalMemorySize");
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        Log.d("DisplayStatus","getAvailableInternalMemorySize lockSize is "+blockSize+" Available Blocks"+availableBlocks);
        return formatSize(availableBlocks * blockSize);
    }

    private static String getTotalInternalMemorySize() {
        Log.d("Display Stats"," in getTotalInternalMemorySize");
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        Log.d("DisplayStatus","getTotalInternalMemorySize lockSize is "+blockSize+" Total Blocks"+totalBlocks);
        return formatSize(totalBlocks * blockSize);
    }

    private static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        Log.d("DisplayStats","Format Size is "+resultBuffer.toString());
        return resultBuffer.toString();
    }
    private static boolean isRooted() {
        //  boolean isEmulator = isEmulator(context);
        /*String buildTags = Build.TAGS;*/
    /*if(!isEmulator && buildTags != null && buildTags.contains("test-keys")) {
        return true;
    } else*/ {
            File file = new File("/system/app/Superuser.apk");
            if(file.exists()) {
                return true;
            } else {
                file = new File("/system/xbin/su");
                Log.d("Displaystats","isRooted "+file.exists());
                return /*!isEmulator && */file.exists();
            }
        }
    }
    private static String getMacAddrTest() {
        try {
            Log.d(TAG,"In getMacAddr Changed");
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());

            Log.d(TAG,"Get Mac Address is "+all.size());

            for (NetworkInterface nif : all) {
                Log.d(TAG, "Test check getMacAddr interface " + nif.getName());
            }
            //above for testing
            for (NetworkInterface nif : all) {
                Log.d(TAG,"getMacAddr interface "+nif.getName());
              /*  if (GlobalVariables.MobileDevice) //added ! chand for testing
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                    else
                    if (!nif.getName().equalsIgnoreCase("eth0")) continue;
*/
                if (GlobalVariables.MobileDevice) {

                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                } else {
                    Log.d(TAG,"CHecking the wlan0 and eth0");
                  /*  if (!nif.getName().equalsIgnoreCase("wlan0") &&
                            !nif.getName().equalsIgnoreCase("eth0"))  {
                        Log.d(TAG,"It is not wlan or eth0");
                        continue;
                    }*/
                    /* if (!nif.getName().equalsIgnoreCase("wlan0") &&
                            !nif.getName().equalsIgnoreCase("eth0"))  {*/

                    //chand added above - for Mobile 11/11/2019

                    if (!nif.getName().equalsIgnoreCase("wlan0")) {
                        Log.d(TAG, "Not matching with eth0" + nif.getName());
                        continue;
                    } else
                        Log.d(TAG,"Matching interface is "+nif.getName());
                }
                if (nif.getName().contains("ip6")) continue;
                Log.d(TAG,"getMacAddr interface processing "+nif.getName());

                byte[] macBytes = nif.getHardwareAddress();
                Log.d(TAG,"Process mac for "+nif.getName());

                if (macBytes == null) {
                    continue;
                    //  return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG,"Return Mac is "+res1.toString());
                return res1.toString();
            }
        } catch (Exception ex) {
            Log.d("NewSplash","Not able to get mac address");
        }
        return "02:00:00:00:00:00";
    }
    private static String getMacAddr() {
        try {
            Log.d(TAG,"In getMacAddr Changed");
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());

            Log.d(TAG,"Get Mac Address is "+all.size());

            for (NetworkInterface nif : all) {
                Log.d(TAG, "Test check getMacAddr interface " + nif.getName());
            }
            //above for testing
            for (NetworkInterface nif : all) {
                Log.d(TAG,"getMacAddr interface "+nif.getName());
              /*  if (GlobalVariables.MobileDevice) //added ! chand for testing
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                    else
                    if (!nif.getName().equalsIgnoreCase("eth0")) continue;
*/
                if (GlobalVariables.MobileDevice) {

                    Log.d(TAG,"Before checking the wlan0 ");

                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                        Log.d(TAG,"Going for processing "+nif.getName());

                } else {
                    Log.d(TAG,"CHecking the wlan0 and eth0");
                   /* if (!nif.getName().equalsIgnoreCase("wlan0") &&
                            !nif.getName().equalsIgnoreCase("eth0"))  {*/
                    if (!nif.getName().equalsIgnoreCase("wlan0")
                    )  {
                        Log.d(TAG,"It is not wlan or eth0");
                        continue;
                    }
                    //chand added above - for Mobile 11/11/2019

                    /*if (!nif.getName().equalsIgnoreCase("eth0")) {
                        Log.d(TAG, "Not matching with eth0" + nif.getName());
                        continue;
                    }*/ else
                        Log.d(TAG,"Matching interface is "+nif.getName());
                }
                if (nif.getName().contains("ip6")) continue;
                Log.d(TAG,"getMacAddr interface processing "+nif.getName());

                byte[] macBytes = nif.getHardwareAddress();
                Log.d(TAG,"Process mac");

                Log.d(TAG,"Process mac for "+nif.getName());
                if (macBytes == null) {
                    continue;
                    //  return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG,"Return Mac is "+res1.toString());
                return res1.toString();
                //for testing commented
            }
        } catch (Exception ex) {
            Log.d("NewSplash","Not able to get mac address");
        }
        String newMac= getMacAddrEthernet();
        Log.d(TAG,"New Eth mac is "+newMac);
        return newMac;
        //return "02:00:00:00:00:00";
    }
    private static String getMacAddrEthernet() {
        try {
            Log.d(TAG,"In getMacAddr Changed");
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());

            Log.d(TAG,"Get Mac Address is "+all.size());

            for (NetworkInterface nif : all) {
                Log.d(TAG, "Test check getMacAddr interface " + nif.getName());
            }
            //above for testing
            for (NetworkInterface nif : all) {
                Log.d(TAG,"getMacAddr interface "+nif.getName());
              /*  if (GlobalVariables.MobileDevice) //added ! chand for testing
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                    else
                    if (!nif.getName().equalsIgnoreCase("eth0")) continue;
*/
                if (GlobalVariables.MobileDevice) {

                    Log.d(TAG,"Before checking the wlan0 ");

                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;
                    Log.d(TAG,"Going for processing "+nif.getName());

                } else {
                    Log.d(TAG,"CHecking the wlan0 and eth0");
                   /* if (!nif.getName().equalsIgnoreCase("wlan0") &&
                            !nif.getName().equalsIgnoreCase("eth0"))  {*/
                   // if (!nif.getName().equalsIgnoreCase("wlan0")
                    if (!nif.getName().equalsIgnoreCase("eth0")
                    )  {
                        Log.d(TAG,"It is not wlan or eth0");
                        continue;
                    }
                    //chand added above - for Mobile 11/11/2019

                    /*if (!nif.getName().equalsIgnoreCase("eth0")) {
                        Log.d(TAG, "Not matching with eth0" + nif.getName());
                        continue;
                    }*/ else
                        Log.d(TAG,"Matching interface is "+nif.getName());
                }
                if (nif.getName().contains("ip6")) continue;
                Log.d(TAG,"getMacAddr interface processing "+nif.getName());

                byte[] macBytes = nif.getHardwareAddress();
                Log.d(TAG,"Process mac");

                Log.d(TAG,"Process mac for "+nif.getName());
                if (macBytes == null) {
                    continue;
                    //  return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                Log.d(TAG,"Return Mac is "+res1.toString());
                return res1.toString();
                //for testing commented
            }
        } catch (Exception ex) {
            Log.d("NewSplash","Not able to get mac address");
        }
        return "02:00:00:00:00:00";
    }

    private static Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) cntx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) cntx.getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public static Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) cntx.getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public static String GetDeviceipWiFiData()
    {
        WifiManager wm = (WifiManager) cntx.getSystemService(WIFI_SERVICE);

        //    @SuppressWarnings("deprecation")
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        Log.d(TAG,"GetDeviceWifiData"+ip);
        return ip;

    }
    private static void SendDeviceStatusUpdate(JSONObject val,String url) {

       // RequestQueue mQueue = Volley.newRequestQueue(cntx);

        //InternetFine=true;
                Log.d("h"," inside http call");

//        String url = "http://10.0.2.2:3000/";
//        String url = "http://139.59.10.127:3000/";
      //  String url = "http://139.59.10.127:3001/api/escort_task";
//        String url = "https://jsonplaceholder.typicode.com/todos/1";

        Log.d(TAG,"SendDevice Status URL is "+url);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, val,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            {
                                boolean ChangeInWorkFlow =false;
                                Log.d(TAG,"Got the Get Device Status Response ");
                                Log.d(TAG,"Device Status Response is  "+response.toString());
                                try {
                                    //last_config_update - 1/0
                                    //terminal_status - 1/0
                                    //terminal_id- -1/Valid value

                                    int changeInTerminal = 0;

                                    mobileWorkFlowConfigure=false;
                                    Log.d(TAG,"5 Mobile Work Flow is "+mobileWorkFlowConfigure);

                                    JSONObject deviceStatus = new JSONObject(response.toString());
                                    int TerminalID = deviceStatus.optInt("terminal_id");

                                    int last_termianl_config_update = deviceStatus.optInt("terminal_config_update");
                                    int pending_termianl_config_update = prefs.getInt(GlobalVariables.pending_terminal_config_Update, 0);

                                    int last_users_config_update = deviceStatus.optInt("users_config_update");
                                    int pending_usersconfig_update = prefs.getInt(GlobalVariables.pending_users_Config_Update, 0);


                                    if ((pending_termianl_config_update !=  last_termianl_config_update) || (pending_usersconfig_update != last_users_config_update)) {
                                        SharedPreferences.Editor editor1 = prefs.edit();
                                        editor1.putInt(GlobalVariables.pending_terminal_config_Update, last_termianl_config_update);
                                        editor1.putInt(GlobalVariables.pending_users_Config_Update,last_users_config_update);
                                        editor1.commit();
                                        Log.d(TAG,"Saving last config update ");
                                    }

                                    Log.d(TAG,"Rcvd Terminal ID is "+TerminalID);
                                    Log.d(TAG,"Last Config Update "+last_termianl_config_update);

                                    //chand for testing
                                    if (TerminalID == -1)
                                        TerminalID = 49;

                                    Log.d(TAG,"It is Default Termianl ID ");
                                    if (TerminalID != -1) {
                                        Log.d(TAG, "Terminal ID is " + TerminalID);

                                        //prefs = getSharedPreferences(GlobalVariables.SP_TerminalID, MODE_PRIVATE);
                                        int Global_TerminalID = prefs.getInt(GlobalVariables.SP_TerminalID, -1);

                                        if (TerminalID != Global_TerminalID) {

                                            SharedPreferences.Editor editor1 = prefs.edit();

                                            editor1.putInt(GlobalVariables.SP_TerminalID, TerminalID);
                                            Log.d(TAG, "Storing TerminalID " + TerminalID);
                                            GlobalVariables.TerminalID = TerminalID;
                                            Global_TerminalID = TerminalID;
                                            changeInTerminal=1;
                                            editor1.commit();
                                        } else {
                                            Log.d(TAG, "No Change in TermnalID " + Global_TerminalID);
                                            GlobalVariables.TerminalID = Global_TerminalID;
                                        }

                                        //   if ((TerminalID != -1)) {
                                        //int last_config_update = deviceStatus.optInt("terminal_config_update");

                                        try {
                                            Log.d(TAG,"Work flow  before parsing");
                                            String workflowString= deviceStatus.optString("workflows");
                                            //JSONObject workflowString2= deviceStatus.optJSONObject("workflows");

                                            if (workflowString != null) {
                                                Log.d(TAG, "Current Work flow is " + workflowString.toString());
                                                String workflowstr = workflowString.toString();

                                                Log.d(TAG,"Work flow Strng is "+workflowstr);
                                                JSONObject deviceworkflow = new JSONObject(workflowstr.trim());

                                                String Workflowname = deviceworkflow.optString("wor_name");
                                                try {
                                                    if (Workflowname == null)
                                                        Workflowname =  deviceworkflow.getString("wor_name");

                                                } catch (Exception obj) {

                                                }
                                                Integer currWorkFlow=0;

                                                Log.d(TAG, "Current Workflow Name is " + Workflowname);
                                                if (Workflowname.equals("access_control")) {
                                                    currWorkFlow = 1;
                                                    //GlobalVariables.WorkFlowVal = 1;

                                                } else if (Workflowname.equals("supervision_enrollment")) {
                                                    currWorkFlow=2;
                                                } else if (Workflowname.equals("normal_enrollment")) {
                                                    currWorkFlow=3;
                                                } else if (Workflowname.equals("frost")) {
                                                    currWorkFlow=4;
                                                } else if (Workflowname.equals("restricted_access")) {
                                                    currWorkFlow=5;

                                                } //Sentinel Work Flow starts
                                                else if (Workflowname.equals("tv_support")) {
                                                    currWorkFlow=6;
                                                }
                                                else if (Workflowname.equals("terminal_startexit")) {
                                                    currWorkFlow=7;
                                                }
                                                else if (Workflowname.equals("guardstation_waitingarea")) {
                                                    currWorkFlow=8;
                                                }
                                                else if (Workflowname.equals("detainee_checkpoints")) {
                                                    currWorkFlow=9;
                                                }  else if (Workflowname.equals("detainee_outside_block")) {
                                                    currWorkFlow=10;
                                                }  else if (Workflowname.equals("guard_in_block")) {
                                                    currWorkFlow=11;
                                                }
                                                else if (Workflowname.equals("escort_boothguard_station")) {
                                                    currWorkFlow=12;
                                                } else if (Workflowname.equals("waiting_area_visitor")) { //detainee_terminal_startexit")) {
                                                    currWorkFlow=13;
                                                } else if (Workflowname.equals("escort_task_select")) {
                                                    currWorkFlow = 14;
                                                } else if (Workflowname.equals("boothguard_outside")) {
                                                        currWorkFlow=15;

                                                    } else if (Workflowname.equals("sentinel_userlogin_changeworkflow")) {
                                                        currWorkFlow=20;
                                                    }
                                                else
                                                    Log.d(TAG, "Flow work is null");
                                                if (currWorkFlow != GlobalVariables.WorkFlowVal)
                                                    GlobalVariables.WorkFlowVal = currWorkFlow;

                                                int workflowID = prefs.getInt(GlobalVariables.workflowStatus, -1);

                                                if (currWorkFlow != workflowID) {
                                                    SharedPreferences.Editor editor1 = prefs.edit();
                                                    editor1.putInt(GlobalVariables.workflowStatus, currWorkFlow);
                                                    ChangeInWorkFlow=true;
                                                    SetWorkFlow(currWorkFlow);
                                                    Log.d(TAG,"Saving the work flow status "+currWorkFlow);
                                                    editor1.commit();
                                                }
                                            } else
                                                Log.d(TAG,"Workflow is null");
                                        } catch (Exception obj) {
                                            Log.d(TAG,"Got Exception while reading the Workflow"+obj.getMessage());
                                        }

                                        Log.d(TAG,"last terminal config update "+last_termianl_config_update+" change in termianl "+changeInTerminal+" last users config "+last_users_config_update);
                                        if ((TerminalID != -1) && ((last_termianl_config_update != 0) || (last_users_config_update != 0) || (changeInTerminal ==1))) {
                                            Log.d(TAG,"Needs Config UPdate ");



                                            String url = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;

//        String URL = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;

                                            int workflowID1 = prefs.getInt(GlobalVariables.workflowStatus, -1);

                                            Log.d(TAG,"Workd Flow ID is "+workflowID1);
                                            if (FaceDetectionDetaineeIdentificationforV4 == true) {
                                                url = GlobalVariables.SSOIPAddress + GET_ACTIVE_DETAINEE_USERS;
                                                Log.d(TAG,"Got the Detainee Identification ");
                                            }
                                            else
                                            if (selectedProduct ==2 )
                                                url = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS +"/"+Global_TerminalID;
                                            else
                                            if (selectedProduct ==1 )
                                                url = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;
                                            else
                                            if (workflowID1 == 20)
                                                url = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS + "/" + Global_TerminalID;
                                            else
                                            if (workflowID1 >=9 && workflowID1 <=19)
                                                url = GlobalVariables.SSOIPAddress + GET_ALL_NON_VISITOR_USERS + "/" + Global_TerminalID;


                                            Log.d(TAG,"Launch Activity URL is "+url);
                                            //MyProgressDialog.show(LaunchActivity.this, R.string.wait_message);
                                            Log.d("MainActivity", "Running ");
                                            //  new VolleyService(LaunchActivity.this, LaunchActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
                                           // new VolleyService(LaunchActivity.this, LaunchActivity.this).tokenBaseB(GET, URL, null, "101");


                                            if (vcomdatabaseSupport == 1) {

                                                Log.d(TAG,"For Database Restarting the App");
                                                System.exit(0);
                                            } else
                                            setGetAllUsers(Global_TerminalID);

                                            //();
                                        } else {
                                            Log.d(TAG,"termain ID is "+Global_TerminalID);

                                            if (Global_TerminalID != -1) { // && ChangeInWorkFlow == true) {
                                                Log.d(TAG,"Change in Work Flow Download COnfiguration s");

                                                if (vcomdatabaseSupport == 1) {
                                         /*           Log.d(TAG,"For Database Restarting the App");
                                                    //stopSelf();
                                                    System.exit(0);
                                        */        } else
                                                setGetAllUsers(Global_TerminalID);
                                            } else
                                                Log.d(TAG,"No need of configuations ");

                                            Log.d(TAG,"Starting Main Activity ");
                                          }

                                    } else
                                        Log.d(TAG,"Terminal is not mapped ");

                                } catch (Exception obj) {
                                    Log.d(TAG,"Got JSON Exception obj");
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d("h"," inside http call error" + error);
                mobileWorkFlowConfigure=true;
                Log.d(TAG,"6 Mobile Work Flow is "+mobileWorkFlowConfigure);

            }
        });
       // mQueue.add(request);
        if (GetUsersTimerService.requestQueue != null)
            GetUsersTimerService.requestQueue.add(request);
        else
            Log.d(TAG,"requestQueue Failure ");
    }


    private static void setGetAllUsers1(int Global_TerminalID) {

        RequestQueue mQueue = Volley.newRequestQueue(cntx);

        //InternetFine=true;
        Log.d("h"," inside http call");

//        String url = "http://10.0.2.2:3000/";
//        String url = "http://139.59.10.127:3000/";
        //  String url = "http://139.59.10.127:3001/api/escort_task";
//        String url = "https://jsonplaceholder.typicode.com/todos/1";

        String url = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;

//        String URL = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;

        int workflowID1 = prefs.getInt(GlobalVariables.workflowStatus, -1);

        if (FaceDetectionDetaineeIdentificationforV4 == true)
            url = GlobalVariables.SSOIPAddress + GET_ACTIVE_DETAINEE_USERS;
            else
        if (selectedProduct ==2 )
            url = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS +"/"+Global_TerminalID;
        else
        if (selectedProduct ==1 )
            url = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;
        else
        if (workflowID1 == 20)
            url = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS + "/" + Global_TerminalID;
        else
        if (workflowID1 >=9 && workflowID1 <=19)
            url = GlobalVariables.SSOIPAddress + GET_ALL_NON_VISITOR_USERS + "/" + Global_TerminalID;


        Log.d(TAG,"Launch Activity URL is "+url);

        Log.d(TAG,"SendDevice Status URL is "+url);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                          //  {
                                Log.d(TAG, "Got the Get Device Status Response ");
                                Log.d(TAG, "Device Status Response is  " + response.toString());
                                try {


                                    mobileWorkFlowConfigure=false;
                                    Log.d(TAG,"8 Mobile Work Flow is "+mobileWorkFlowConfigure);

                                    Log.d(TAG, "Stopping the Timer Saving the time ");
                                    Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                                    //Added 4 & half hours to show the Indian time on the Server - Testing - 25/07/2019
                                    //Date currentTime2 = curCalendar.getInstance().getTime();
                                    // currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                                    //  long currts = (curCalendar.getTimeInMillis() / 1000L + 16200);
                                    long currts = (curCalendar.getTimeInMillis() / 1000L);

                                    //UserID = prefs.getInt(GlobalVariables.UserID_Test, -1);
                                    SharedPreferences.Editor editor1 = prefs.edit();
                                    editor1.putLong(GlobalVariables.last_Config_SP, currts);

                                    editor1.commit();
                                    Log.d(TAG, "Last Config success was " + currts);

                                    SharedPreferences.Editor editor2 = prefs.edit();
                                    editor2.putInt(GlobalVariables.pending_terminal_config_Update, 0);
                                    editor2.putInt(GlobalVariables.pending_users_Config_Update, 0);
                                    editor2.commit();

                                    Log.d(TAG, "SHow Progress Dialog dismiss");
                                    MyProgressDialog.dismiss();
                                } catch (Exception obj) {
                                    Log.d(TAG, "Got exception " + obj.getMessage());
                                }
                                Log.d(TAG, "Befor Gson");
                                Gson gson = new Gson();
                                Type listType = new TypeToken<ArrayList<lumidigm.captureandmatch.entity.User>>() {
                                }.getType();
                                Log.d(TAG, "Befor allTasks data");
                                List<lumidigm.captureandmatch.entity.User> allTasks = gson.fromJson(response.toString(), listType);
                                Log.d(TAG, "notifySuccess: " + allTasks.size());
                                appDatabase.userDao().deleteAll();
                                Log.d(TAG, "After deleteAll");
                                appDatabase.userDao().insertAll(allTasks);
                                Log.d(TAG, "Befor Intent");
                         //   }

                         } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.d(TAG,"9 Mobile Work Flow is "+mobileWorkFlowConfigure);

                mobileWorkFlowConfigure=true;
                Log.d("h"," inside http call error" + error);
            }
        });
        mQueue.add(request);
    }
    private static void SetWorkFlow(int workflow) {
        Log.d(TAG,"Current work flow is "+workflow);

        GlobalVariables.BoothGuardStation_Outside=false;

        GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        if (productVersion == 1) {
            GlobalVariables.WorkFlowVal=1;
            Log.d(TAG,"Work Flow is only Access Control - V0 ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=0;
            GlobalVariables.WebServer=false; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            TimeandAttendence = false; //true;
            GlobalVariables.adminVerification=true;
            GlobalVariables.RestrictedAccess = false;


            GlobalVariables.TV_Support =false;
            GlobalVariables.Terminal_Support =false;
            GuardStation_WaitingArea =false;
            GlobalVariables.Detainee_Checkpoint = false;
            GlobalVariables.Guard_In_Block=false;

            GlobalVariables.Escort_BoothGuardStation=false;
            GlobalVariables.Detainee_Terminal_StartExit=false;
            GlobalVariables.Escort_TaskList=false;

            GlobalVariables.WaitingAreaVisitorVerification=false;
            GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        } else
        if (workflow==1) {
            GlobalVariables.WorkFlowVal=1;
            Log.d(TAG,"Work Flow is only Access Control ");
            // GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //  GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=0;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.adminVerification=true;
            GlobalVariables.RestrictedAccess =false;

            GlobalVariables.TV_Support =false;
            GlobalVariables.Terminal_Support =false;
            GuardStation_WaitingArea =false;
            GlobalVariables.Detainee_Checkpoint = false;
            GlobalVariables.Detainee_outsideBlock=false;
            GlobalVariables.Guard_In_Block=false;

            GlobalVariables.WaitingAreaVisitorVerification=false;
            GlobalVariables.Escort_BoothGuardStation=false;

            GlobalVariables.Detainee_Terminal_StartExit=false;
            GlobalVariables.Escort_TaskList=false;

            GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        } else
        if (workflow ==2) {

            GlobalVariables.WorkFlowVal=2;
            Log.d(TAG,"Work Flow is Supervisiorory Enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess =false;
            GlobalVariables.adminVerification=true;


            GlobalVariables.TV_Support =false;
            GlobalVariables.Terminal_Support =false;
            GuardStation_WaitingArea =false;
            GlobalVariables.Detainee_Checkpoint = false;
            GlobalVariables.Detainee_outsideBlock=false;
            GlobalVariables.Guard_In_Block=false;

            GlobalVariables.Escort_BoothGuardStation=false;
            GlobalVariables.Detainee_Terminal_StartExit=false;
            GlobalVariables.Escort_TaskList=false;

            GlobalVariables.WaitingAreaVisitorVerification=false;
            GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        } else

        if (workflow ==3) {

            GlobalVariables.WorkFlowVal=3;
            Log.d(TAG,"Work Flow is Normal enrollment ");

            //   GlobalVariables.MobileDevice = false; //true; //false;//it has no Network
            //   GlobalVariables.faceTab=false;//For Mobile
            //  GlobalVariables.vcomdatabaseSupport=1;
            //For Web Server
            //0 for Normal, 1 for WebSupport, 2 for USB
            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = false; //true;
            GlobalVariables.RestrictedAccess =false;
            GlobalVariables.adminVerification=false;


            GlobalVariables.TV_Support =false;
            GlobalVariables.Terminal_Support =false;
            GuardStation_WaitingArea =false;
            GlobalVariables.Detainee_Checkpoint = false;
            GlobalVariables.Detainee_outsideBlock=false;
            GlobalVariables.Guard_In_Block=false;

            GlobalVariables.Escort_BoothGuardStation=false;
            GlobalVariables.Detainee_Terminal_StartExit=false;
            GlobalVariables.Escort_TaskList=false;

            GlobalVariables.WaitingAreaVisitorVerification=false;
            GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        } else
        if (workflow ==4) {

            GlobalVariables.WorkFlowVal=4;
            Log.d(TAG,"Work Flow is for Time and Attendence");

            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.TimeandAttendence = true; //true;
            GlobalVariables.RestrictedAccess =false;


            GlobalVariables.TV_Support =false;
            GlobalVariables.Terminal_Support =false;
            GuardStation_WaitingArea =false;
            GlobalVariables.Detainee_Checkpoint = false;
            GlobalVariables.Detainee_outsideBlock=false;
            GlobalVariables.Guard_In_Block=false;

            GlobalVariables.Escort_BoothGuardStation=false;
            GlobalVariables.Detainee_Terminal_StartExit=false;
            GlobalVariables.Escort_TaskList=false;

            GlobalVariables.WaitingAreaVisitorVerification=false;
            GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        } else
        if (workflow ==5) {

            GlobalVariables.WorkFlowVal=5;
            Log.d(TAG,"Work Flow is for Restricted Access");

            GlobalVariables.AppSupport=1;
            GlobalVariables.WebServer=true; //true;

            GlobalVariables.onlyVerification=false;
            //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
            //above true means - no verification and enrollment only with password
            GlobalVariables.supervisionEnrolmentStatus=false;
            GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
            GlobalVariables.RestrictedAccess = true; //true;
            GlobalVariables.TimeandAttendence =false;

            GlobalVariables.TV_Support =false;
            GlobalVariables.Terminal_Support =false;
            GuardStation_WaitingArea =false;
            GlobalVariables.Detainee_Checkpoint = false;
            GlobalVariables.Detainee_outsideBlock=false;
            GlobalVariables.Guard_In_Block=false;

            GlobalVariables.Escort_BoothGuardStation=false;

            GlobalVariables.Detainee_Terminal_StartExit=false;
            GlobalVariables.Escort_TaskList=false;

            GlobalVariables.WaitingAreaVisitorVerification=false;
            GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
        }
        else //Starts from Sentinel
            if (workflow ==6) {
                GlobalVariables.WorkFlowVal=6;
                Log.d(TAG,"Work Flow is for TV Support");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=true;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;

                GlobalVariables.TV_Support =true;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;

                GlobalVariables.Escort_BoothGuardStation=false;

                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==7) {

                GlobalVariables.WorkFlowVal=7;
                Log.d(TAG,"Work Flow is for Termianl Entry Exit  ");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;

                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =true;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;

                GlobalVariables.Escort_BoothGuardStation=false;

                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==8) {

                GlobalVariables.WorkFlowVal=8;
                Log.d(TAG,"Work Flow is for Guard Station Waiting ARea");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;

                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =true;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;

                GlobalVariables.Escort_BoothGuardStation=false;

                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==9) {

                GlobalVariables.WorkFlowVal=9;
                Log.d(TAG,"Work Flow is for Detainee CheckPoint");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = true;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.Escort_BoothGuardStation=false;

                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;

                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else if (workflow ==10) {

                GlobalVariables.WorkFlowVal=10;
                Log.d(TAG,"Work Flow is for Guard Station Waiting ARea");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=true;
                GlobalVariables.Guard_In_Block=false;

                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;
                GlobalVariables.Escort_BoothGuardStation=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            }
            else
            if (workflow ==11) {

                GlobalVariables.WorkFlowVal=11;
                Log.d(TAG,"Work Flow is for Guard Station BLock IN ");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=true;

                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;
                GlobalVariables.Escort_BoothGuardStation=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==12) {

                GlobalVariables.WorkFlowVal=12;
                Log.d(TAG,"Work Flow is for Escort BoothGuardStation");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;
                GlobalVariables.Escort_BoothGuardStation=true;
                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==13) {

                GlobalVariables.WorkFlowVal=13;
                Log.d(TAG,"Work Flow is for Escort BoothGuardStation");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;
                GlobalVariables.Escort_BoothGuardStation=false;
                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.WaitingAreaVisitorVerification=true;
                GlobalVariables.Escort_TaskList=false;

                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==14) {

                GlobalVariables.WorkFlowVal=14;
                Log.d(TAG,"Work Flow is for Escort BoothGuardStation");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;
                GlobalVariables.Escort_BoothGuardStation=false;
                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=true;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==15) {

                GlobalVariables.WorkFlowVal=15;
                Log.d(TAG,"Work Flow is for Single Work Flow");

                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;
                GlobalVariables.Escort_BoothGuardStation=false;
                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;
                GlobalVariables.BoothGuardStation_Outside=true;
                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = false;
            } else
            if (workflow ==20) {

                GlobalVariables.WorkFlowVal=20;
                Log.d(TAG,"Work Flow is for Single Work Flow");

/*                GlobalVariables.AppSupport=0;
                GlobalVariables.WebServer=false; //true;

                GlobalVariables.onlyVerification=false;
                //supervisionEnrolmentStatus - false - Means Verification and Enroll without Password
                //above true means - no verification and enrollment only with password
                GlobalVariables.supervisionEnrolmentStatus=false;
                GlobalVariables.supervisorUser=false;//true; //For Mobile - Admin Verification
                GlobalVariables.RestrictedAccess = false; //true;
                GlobalVariables.TimeandAttendence = false;


                GlobalVariables.TV_Support =false;
                GlobalVariables.Terminal_Support =false;
                GuardStation_WaitingArea =false;
                GlobalVariables.Detainee_Checkpoint = false;
                GlobalVariables.Detainee_outsideBlock=false;
                GlobalVariables.Guard_In_Block=false;
                GlobalVariables.Escort_BoothGuardStation=false;
                GlobalVariables.Detainee_Terminal_StartExit=false;
                GlobalVariables.Escort_TaskList=false;

                GlobalVariables.WaitingAreaVisitorVerification=false;
                GlobalVariables.sentinelWorkFlowChangeOnUserLogon = true;*/
            }

        Log.d(TAG,"Work Flow not supported "+workflow);


        Log.d(TAG,"Work Flow not supported "+workflow);
    }


    private static void setGetAllUsers(int Global_TerminalID)
   // public void fetchData()
    {

       // RequestQueue mQueue = Volley.newRequestQueue(cntx);

        Log.d("h"," inside fetch data");

//        String url = "http://139.59.10.127:8114/user_enroll/getallescortDetails";

        String url = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;

//        String URL = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;

        int workflowID1 = prefs.getInt(GlobalVariables.workflowStatus, -1);

        if (FaceDetectionDetaineeIdentificationforV4 == true) {
            url = GlobalVariables.SSOIPAddress + GET_ACTIVE_DETAINEE_USERS;
            Log.d(TAG,"Got the Detainee Identification ");
        }
        else
        if (selectedProduct == 2)
            url = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS +"/"+Global_TerminalID;
        else

        if (selectedProduct == 1)
        url = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;
        else
        if (workflowID1 == 20)
            url = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS + "/" + Global_TerminalID;
        else
        if (workflowID1 >=9 && workflowID1 <=19)
            url = GlobalVariables.SSOIPAddress + GET_ALL_NON_VISITOR_USERS + "/" + Global_TerminalID;


        Log.d(TAG,"Launch Activity URL is "+url);

        Log.d(TAG,"SendDevice Status URL is "+url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                /*for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        String block = jsonObject.getString("esc_block");
                        String msg = jsonObject.getString("esc_task_msg");

                        Log.d("data","block data " + block);
                        Log.d("data","block msg " + msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }*/
                Log.d(TAG, "Got the Get Device Status Response ");
                Log.d(TAG, "Device Status Response is  " + response.toString());
                try {

                    Log.d(TAG, "Stopping the Timer Saving the time ");
                   // Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                    Calendar curCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    //Added 4 & half hours to show the Indian time on the Server - Testing - 25/07/2019
                    //Date currentTime2 = curCalendar.getInstance().getTime();
                    // currts = Long.toString(curCalendar.getTimeInMillis() /  1000L);
                    //  long currts = (curCalendar.getTimeInMillis() / 1000L + 16200);
                    long currts = (curCalendar.getTimeInMillis() / 1000L);

                    final String DATE_FORMAT="yyyy-MM-dd HH:mm:ss";
                    final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                    final String utcTime = sdf.format(new Date());
                    Log.d(TAG,"TIME is "+utcTime);

                    //UserID = prefs.getInt(GlobalVariables.UserID_Test, -1);
                    SharedPreferences.Editor editor1 = prefs.edit();
                    editor1.putLong(GlobalVariables.last_Config_SP, currts);
                    editor1.putString(GlobalVariables.last_Config_DATE_SP, utcTime);

                    editor1.commit();



                    //remove



                    //UserID = prefs.getInt(GlobalVariables.UserID_Test, -1);
//                    SharedPreferences.Editor editor1 = prefs.edit();
    //                editor1.putLong(GlobalVariables.last_Config_SP, currts);
      //              editor1.putString(GlobalVariables.last_Config_DATE_SP, utcTime);

  //                  editor1.commit();


                    //test

                    Log.d(TAG, "Last Config success was " + currts);

                    SharedPreferences.Editor editor2 = prefs.edit();
                    editor2.putInt(GlobalVariables.pending_terminal_config_Update, 0);
                    editor2.putInt(GlobalVariables.pending_users_Config_Update, 0);
                    editor2.commit();





                    Log.d(TAG, "SHow Progress Dialog dismiss");
                    MyProgressDialog.dismiss();
                } catch (Exception obj) {
                    Log.d(TAG, "Got exception " + obj.getMessage());
                }
                Log.d(TAG, "Befor Gson");

                try {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<lumidigm.captureandmatch.entity.User>>() {
                }.getType();
                Log.d(TAG, "Befor allTasks data");
                List<lumidigm.captureandmatch.entity.User> allTasks = gson.fromJson(response.toString(), listType);
                Log.d(TAG, "notifySuccess: " + allTasks.size());
                appDatabase.userDao().deleteAll();
                Log.d(TAG, "After deleteAll");
                appDatabase.userDao().insertAll(allTasks);
                Log.d(TAG, "Befor Intent");
                   //}

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
            }
        });

        //mQueue.add(jsonArrayRequest);
        if (GetUsersTimerService.requestQueue != null)
            GetUsersTimerService.requestQueue.add(jsonArrayRequest);
        else
            Log.d(TAG,"Not Sending request queue");
    }

    public static String GetUTCdatetimeAsString()
    {
         final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        Log.d(TAG,"UTC Time is "+utcTime);
        return utcTime;
    }
    private void RequestPendingConfigurationUsers(int Global_TerminalID) {
        Log.d(TAG,"in RequestPendingConfigurationUsers");

        String URL = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;

        int workflowID1 = prefs.getInt(GlobalVariables.workflowStatus, -1);

        if (vcomdatabaseSupport == 1 && selectedProduct == 1)
            URL =  GlobalVariables.SSOIPAddress + GET_PENDING_USERS +"/"+Global_TerminalID;
        else
        if (vcomdatabaseSupport == 1 && selectedProduct == 2)
            URL =  GlobalVariables.SSOIPAddress + GET_PENDING_VISITORS_AND_OTHERS +"/"+Global_TerminalID;
        else
        if (FaceDetectionDetaineeIdentificationforV4 == true) {
            URL = GlobalVariables.SSOIPAddress + GET_ACTIVE_DETAINEE_USERS;
            Log.d(TAG,"Got the Detainee Identification ");
        }
        else
        if (selectedProduct == 2) {
            URL = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS + "/" + Global_TerminalID;
            //For Frost getallusers data
        } else
        if (selectedProduct == 1)
            URL = GlobalVariables.SSOIPAddress + GET_ALL_USERS +"/"+Global_TerminalID;
        else
        if (workflowID1 == 20)
            URL = GlobalVariables.SSOIPAddress + GET_ALL_ACTIVE_VISITORS_AND_OTHERS + "/" + Global_TerminalID;
        else
        if (workflowID1 >=9 && workflowID1 <20)
            URL = GlobalVariables.SSOIPAddress + GET_ALL_NON_VISITOR_USERS + "/" + Global_TerminalID;



        Log.d(TAG,"Launch Activity URL is "+URL);
        //MyProgressDialog.show(LaunchActivity.this, R.string.wait_message);
        Log.d("MainActivity", "Running ");

        //   String URL = GlobalVariables.SSOIPAddress + SSO_SERVER_DEVICE_STATUS;
        Log.d(TAG,"Device Status URL is "+URL);
        JSONObject jsonBody = new JSONObject();

        long lastConfigupdate=0;
        String lastConfigUpdateTime= null;
        try {
            lastConfigupdate = prefs.getLong(GlobalVariables.last_Config_SP,0);
            Log.d(TAG,"last Config Update was "+lastConfigupdate);

            if (lastConfigupdate !=0)
                jsonBody.put("lastupdatedtime", lastConfigupdate);

        }catch (Exception obj) {
            Log.d(TAG,"GOt Exception while reading lastConfigUpdate ");
            lastConfigupdate=0;
        }

        try {
            lastConfigUpdateTime = prefs.getString(GlobalVariables.last_Config_DATE_SP,null);
            Log.d(TAG,"last Config Update was "+lastConfigUpdateTime);

            if (lastConfigUpdateTime != null)
                jsonBody.put("updateson", lastConfigUpdateTime);

        }catch (Exception obj) {
            Log.d(TAG,"GOt Exception while reading lastConfigUpdate ");
            lastConfigUpdateTime=null;
        }

        final String requestBody = jsonBody.toString();

        Log.d(TAG,"Sendign Device Status "+requestBody+" URL is "+URL);

      //  new VolleyService(LaunchActivity.this, LaunchActivity.this).tokenBase(POST, URL, jsonBody, "310");

        //  new VolleyService(LaunchActivity.this, LaunchActivity.this).tokenBaseB(GET, GlobalVariables.SSOIPAddress + GET_ALL_USERS, null, "101");
        //  new VolleyService(LaunchActivity.this, LaunchActivity.this).tokenBaseB(POST, URL, null, "101");

    }
}