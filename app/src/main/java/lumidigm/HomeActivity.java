package lumidigm;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.luxand.facerecognition.MainActivity;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;

//import facerecognition.MainActivity;
import lumidigm.captureandmatch.R;
import lumidigm.captureandmatch.activites.Enroll;
import lumidigm.captureandmatch.activites.FingerScannActivity;
import lumidigm.network.IResult;
import lumidigm.services.VolleyService;
import webserver.AndroidWebServer;
import webserver.DiscoverWebServer;
import webserver.GlobalVariables;
import webserver.USBService;
import webserver.WebserverBroadcastRxService;

import static com.android.volley.Request.Method.GET;

/**
 * Created by colors on 12/10/18.
 */

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    private AndroidWebServer androidWebServer;
    private static boolean isStarted = false;
    private static final int DEFAULT_PORT = 8080;
    private static String  TAG="HomeActivity";
    SharedPreferences prefs;
    AlertDialog alertDialog =null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        prefs = getBaseContext().getSharedPreferences(
                GlobalVariables.SP_FingerPrint, MODE_PRIVATE);


        /*RemoveRemoteDeviceStatus();
        if (GlobalVariables.WebServer) {
            startAndroidWebServer();
        }*/

        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);
/*
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("SP_REMOTE_IP", alarmTime);
                editor.commit();*/

Log.d(TAG,"App Support is "+GlobalVariables.AppSupport);

//For WEb Mirror
/*
if (GlobalVariables.AppSupport == 1) {
    if (isConnectedInWifi()) {
        Log.d(TAG, "It is Conected Wifi and starting broadcast receiver task");
        Intent it = new Intent(getApplicationContext(), DiscoverWebServer.class);

        getApplicationContext().startService(it);
    } else {
        Log.d(TAG, "It is for Ethernet");
        Intent it = new Intent(getApplicationContext(), WebserverBroadcastRxService.class);
        getApplicationContext().startService(it);
    }
} else if (GlobalVariables.AppSupport ==2) { //For USB Support
    if (isEthernetConnected()) {
        StartUSBService();
    }fingerAuth
}
*/
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
             case R.id.settings_new:
               /* check = false;
                Intent i1 = new Intent(this, UserAuthenticationActivity.class);
                startActivity(i1);*/
               Log.d(TAG,"It is start Settings ");
                startSettings();
                //TODO implement
                break;
        }
    }
    public void faceAuth(View v){

        Log.d("HomeActivity","Starting FaceAuthentication");

        //Intent i= new Intent(this, MainActivity.class);
        //startActivity(i);
        GlobalVariables.currAction="face";
        GlobalVariables.curState = null;

        //For USBSupport
        if (GlobalVariables.AppSupport==2)
        SaveDeviceStatustoRemoteDevice(GlobalVariables.currAction, GlobalVariables.curState);

        if (!SendingMessage("face","100")) {
            Log.d("HomeActivity", "Starting Audio Main Activity");
            //Start This for Ethernet Service
            //Intent it = new Intent(getApplicationContext(), WebserverBroadcastRxService.class);
            //getApplicationContext().startService(it);
            Intent i= new Intent(this, MainActivity.class);
            startActivity(i);
        }



    }
    public void fingerAuth(View v){

        Log.d(TAG,"FingerAuth ");
        //GlobalVariables.currAction="finger";
        //GlobalVariables.curState = null;
        if (GlobalVariables.AppSupport==2) {
            SaveDeviceStatustoRemoteDevice("finger", null);

            if (!GlobalVariables.MobileDevice) {
                executeADBCommands("finger", null);
            }
        }
        if (!SendingMessage("finger","101")) {
            Intent i= new Intent(this, FingerScannActivity.class);
            startActivity(i);
        }
    }
    /*public void cardAuth(View v){

        GlobalVariables.currAction="card";
        GlobalVariables.curState = null;

if (GlobalVariables.AppSupport==2)
        SaveDeviceStatustoRemoteDevice(GlobalVariables.currAction, GlobalVariables.curState);

        if (!SendingMessage("card","102")) {
            Intent i= new Intent(this, FirstActivity.class);
            startActivity(i);
        }



    }*/
    public void audioAuth(View v) {


        Log.d(TAG,"AUdioAUth  ");
        GlobalVariables.currAction = "audio";
        GlobalVariables.curState = null;

        if (GlobalVariables.AppSupport == 2)
            SaveDeviceStatustoRemoteDevice(GlobalVariables.currAction, GlobalVariables.curState);

        if (!SendingMessage("audio","103")) {
            Log.d("HomeActivity", "Starting Audio Main Activity");
        //    Intent i= new Intent(this, audio.MainActivity.class);
         //   startActivity(i);
        }
        //Intent i= new Intent(this, audio.MainActivity.class);
        //startActivity(i);


    }
    //region Start And Stop AndroidWebServer
    private boolean startAndroidWebServer() {
        Log.d("HomeActivity","in Start Web Server");
        if (!isStarted) {
            int port = getPortFromEditText();
            try {
                if (port == 0) {
                    throw new Exception();
                }
                androidWebServer = new AndroidWebServer(port, this);
                androidWebServer.start();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                //Snackbar.make(coordinatorLayout, "The PORT " + port + " doesn't work, please change it between 1000 and 9999.", Snackbar.LENGTH_LONG).show();
            }
        }
        return false;
    }

    private boolean stopAndroidWebServer() {
        if (isStarted && androidWebServer != null) {
            androidWebServer.stop();
            return true;
        }
        return false;
    }
    private int getPortFromEditText() {
        //String valueEditText = "8080";
        //return (valueEditText.length() > 0) ? Integer.parseInt(valueEditText) : DEFAULT_PORT;
        return DEFAULT_PORT;
    }

    public boolean isConnectedInWifi() {

        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()
                    && wifiManager.isWifiEnabled() && networkInfo.getTypeName().equals("WIFI")) {
                return true;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got exception "+obj.toString());
        }
        return false;
    }

private boolean SendingMessage(String status, String requestType) {
    Log.d(TAG,"SendMessage "+status);
    //String url="http://192.168.43.24:8080/?username="+status;

    //to come out of SendMessage - Only only for Web Support otherwise quit
    if (GlobalVariables.AppSupport !=1)
        return false;


    String remoteIP = getRemoteIP();
    if (remoteIP == null || remoteIP == "") {
        Log.d(TAG," No IP ");
        return false;
    }
    if (!isEthernetConnected()) {
        if (!isConnectedInWifi()) {
            Log.d(TAG,"Network not connected");
            return false;
        }
    }


    Log.d(TAG,"Getting IP Address");
//   String ipAddr = getIpAccess(); //GetDeviceipWiFiData();
    String url="http://"+remoteIP+":8080/?username="+status;

    Log.d(TAG, "send url: "+url);
    new VolleyService(new IResult() {
        @Override
        public void notifySuccess(String requestType, Object response) {

            Log.d(TAG, "notifySuccess: "+requestType+" Resoibse us"+response);
            if (requestType.equals("100")) {
                //Intent i= new Intent(this, audio.MainActivity.class);

                Intent i = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(i);
            } else if (requestType.equals("101")) {
                Intent i = new Intent(HomeActivity.this, FingerScannActivity.class);
                startActivity(i);

            } else if (requestType.equals("103")) {
           //     Intent i = new Intent(HomeActivity.this, audio.MainActivity.class);
             //   startActivity(i);
            }
            }


        @Override
        public void notifyError(String requestType, VolleyError error) {
            Log.d(TAG, "notifyError: "+error.getMessage());
            try {
                //String fingerprint = "testfingerprint";
                Log.d(TAG, "notifyFailure:@@@@@@@@@@@@@@@@ ");
                String filePath= Environment.getExternalStorageDirectory() +"/displayImages/error.mp3";
     //           playAudioFile(2,filePath);
                if (requestType.equals("100")) {
                    //Intent i= new Intent(this, audio.MainActivity.class);

                    Intent i = new Intent(HomeActivity.this, MainActivity.class);

                    startActivity(i);
                } else if (requestType.equals("101")) {
                    Intent i = new Intent(HomeActivity.this, FingerScannActivity.class);
                    startActivity(i);

                } else if (requestType.equals("103")) {
                //    Intent i = new Intent(HomeActivity.this, audio.MainActivity.class);
                 //   startActivity(i);
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }, this).tokenBase(GET, url, null, requestType);

    return true;
}
    private void playAudioFile(int Request, String mediafile) {
        try {
            Log.d(TAG,"Play Audio File "+Request+" Mediafile"+mediafile);

            MediaPlayer mp = new MediaPlayer();
            switch (Request) {
                //For Success
                case 1:
                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    break;

                case 2:
                    mp.setDataSource(mediafile);
                    mp.prepare();
                    mp.start();
                    break;
            }
        } catch (Exception obj) {
            Log.d(TAG,"Got Exception while play Audio "+obj.toString());
        }
    }
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public Boolean isWifiConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI);
        }
        return false;
    }

    public Boolean isEthernetConnected(){
        if(isNetworkAvailable()){
            ConnectivityManager cm
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET);
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public String GetDeviceipWiFiData()
    {

        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

    //    @SuppressWarnings("deprecation")
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());


        Log.d(TAG,"GetDeviceWifiData"+ip);
        return ip;

    }
    private String getIpAccess() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        Log.d(TAG,"Before Reading IP Address");
        @SuppressWarnings("deprecation")
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        final String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        final String serverIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (0 & 0xff));
        //return "http://" + formatedIpAddress + ":";
        return serverIpAddress;
    }
    private String getRemoteIP() {
        String remoteIPAddr = prefs.getString("SP_REMOTE_IP",
                "");

        Log.d(TAG, "Remote IP Addr is " + remoteIPAddr);

        return remoteIPAddr;
    }

    private void StartUSBService() {

        if (!GlobalVariables.MobileDevice) {
            Log.d(TAG, "It is Start USB Service");
            stopService(new Intent(this,
                    USBService.class));

            startService(new Intent(this,
                    USBService.class));
        } else
            Log.d(TAG,"Not Staring USB Service as it is Mobile device");
    }
    private void executeADBCommands(String action, String state ) {

        Process process = null;
        DataOutputStream os = null;

        //If not USB - Come out
        if (GlobalVariables.AppSupport !=2) {
            Log.d(TAG,"It is Not USB COming out");
            return;
        }
        try {
            Log.d(TAG,"Going to Excecute ADB Commands ");
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());


            if (action == null) {
                //os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell\n");
                os.writeBytes("adb -s AINBLJFI4PEQTOI7 pull /mnt/sdcard/status.txt /mnt/mystatus.txt \n");
            } else {
                Log.d(TAG,"ADB state is "+action+"  State is "+state);
                if (state != null) {
                    os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state+"  \n");
                    Log.d(TAG, "It is adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action + " --es state " + state);
                } else {
                    os.writeBytes("adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action+"  \n");
                    //			Log.d(TAG,"adb -s AINBLJFI4PEQTOI7 shell am broadcast -n  lumidigm.captureandmatch/webserver.RemoteDeviceStatus_Receiver --es action " + action);
                }
            }


            os.writeBytes("exit\n");
            //os.flush();
            process.waitFor();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        } finally {
            try {
           //     if (os != null) {
         //           os.close();
             //   }
                process.destroy();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        // TODO Auto-generated method stub

    }
    private void SaveDeviceStatustoRemoteDevice(String action, String state) {

        Log.d(TAG,"In SaveDeviceStatustoRemoteDevice"+action +"  "+state);
        try {

        File myFile = new File(Environment.getExternalStorageDirectory()
                + File.separator + "mydevicestatus.txt");

        myFile.createNewFile();
        FileOutputStream fOut = new FileOutputStream(myFile);
        OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

        if (state != null)
            myOutWriter.append(action+";"+state);
        else
            myOutWriter.append(action);

        myOutWriter.close();
        fOut.close();
        } catch (Exception obj ) {

        }
    }
    private void RemoveRemoteDeviceStatus() {
        String verVal = null;
        String REMOTE_DEVICE_STATUS_FILE = "mydevicestatus.txt";

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + REMOTE_DEVICE_STATUS_FILE);
            //File myFile = new File("/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE); //Both are same

            Log.d(TAG, "File Checking is " + "/mnt/sdcard/" + REMOTE_DEVICE_STATUS_FILE);

            if (myFile.exists()) {
                myFile.delete();
            }
        } catch (Exception obj) {

        }
    }
    private void startSettings() {

        Log.d(TAG,"In SHow Licence App");
        LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
        View subView = inflater.inflate(R.layout.settings_dialog, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText1);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Android Settings");
        builder.setMessage("Password Verification");
        builder.setView(subView);
        builder.setCancelable(false);
        alertDialog = builder.create();


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordText = subEditText.getText().toString().trim();
                Log.d(TAG,"Password Text is "+passwordText);
                //  Toast.makeText(ActivityMain.this, "Pasword is "+passwordText, Toast.LENGTH_LONG).show();

                String settingPwd = getsettingspwd();
                Log.d(TAG,"App Pwd is "+settingPwd);
                if (passwordText.equals("argus@542")) {
                    Log.d(TAG,"Going for Shutdown");
                    //startActivityForResult(new Intent(android.intent.action.ACTION_SHUTDOWN), 0);
                    //    Shutdown();
                } else
                if (passwordText.equals("argus@543")) {
                    Log.d(TAG,"Going for Reboot");
                    //  reboot();
                } else
                if (passwordText.equals(settingPwd)) {

                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    finish();

                } //else //Kept for Testing by Chand - remove it
                // if (TestWithoutUSB(passwordText) == null)
                //   Toast.makeText(Main2Activity.this, "Invalid Password", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                {
                    alertDialog.cancel();
                    // Toast.makeText(ActivityMain.this, "Cancel", Toast.LENGTH_LONG).show();

                    // finish();
                }
            }
        });



        builder.show();
    }
    private String getsettingspwd() {
        String verVal=null;

        try {
            File myFile = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "settingspwd.txt");

            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(
                    fIn));
            String aDataRow = "";
            String aBuffer = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            verVal = aBuffer.toString().trim();

            Log.d(TAG,"Reading settings pwd from sdfile is "+verVal+" len s "+verVal.length());
            myReader.close();
            if ((verVal != null)  && (verVal != " ") && (verVal != "") && verVal.length() > 0)
                return verVal;
        } catch (Exception obj) {
            Log.d(TAG,"Got exception while reading settings pwd");

        }
        return "blynk@123";
    }
}

