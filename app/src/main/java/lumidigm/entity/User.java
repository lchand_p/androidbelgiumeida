package lumidigm.captureandmatch.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "user")
public class User implements Parcelable {



  public String get_id() {
    return _id;
  }

  public void set_id(String _id) {
    this._id = _id;
  }
  @PrimaryKey
  @NonNull
  @SerializedName("_id")
  @ColumnInfo(name = "_id")
  private  String _id;

  @Override
  public String toString() {
    return "User{" +
            "_id='" + _id + '\'' +
            ", userId='" + userId + '\'' +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", fullname='" + fullname + '\'' +
            ", email='" + email + '\'' +
            ", accessToken='" + accessToken + '\'' +
            ", status='" + status + '\'' +
            ", template1='" + template1 + '\'' +
            ", template2='" + template2 + '\'' +
            ", template3='" + template3 + '\'' +
            ", RoleID='" + RoleID + '\'' +
            ", RoleName=" + RoleName +
            // ", Rolename=" + Rolename +
            ", relay='" + relay + '\'' +
            ", door='" + door + '\'' +
            ", ip_addr='" + ip_addr + '\'' +
            ", aes_key='" + aes_key + '\'' +
            ", hash_key='" + hash_key + '\'' +
            // ", zone='" + zone + '\'' +
            // ", name='" + name + '\'' +
            // ", terminal_id=" + terminal_id +
            // ", controller_id=" + controller_id +
            // ", group_id=" + group_id +
            ", Encoded_data=" + Encoded_data +
            ", location=" + location +
            ", location_id=" + LocationID +
            ", terminal_id=" + TerminalID +
            ", checkinout="+ CheckInOut+
            // ", location_id=" + location_id +
            //   terminal_id, controller_id, group_id,location_id, Role_id,role_name,
            '}';
  }

  @NonNull
  public String getUserId() {
    return userId;
  }

  public void setUserId(@NonNull String userId) {
    this.userId = userId;
  }


  @NonNull
  @SerializedName("userId")
  @ColumnInfo(name = "userId")
  private  String userId;

  @SerializedName("username")
  @ColumnInfo(name = "username")
  private String username;
  @SerializedName("password")
  @ColumnInfo(name = "password")
  private String password;
  @SerializedName("fullname")
  @ColumnInfo(name = "fullname")
  private String fullname;
  @SerializedName("email")
  @ColumnInfo(name = "email")
  private String email;
  @SerializedName("accessToken")
  @ColumnInfo(name = "accessToken")
  private String accessToken;
  @SerializedName("status")
  @ColumnInfo(name = "status")
  private String status;

  @SerializedName("template_1")
  @ColumnInfo(name = "template1")
  private String template1;
  @SerializedName("template_2")
  @ColumnInfo(name = "template2")
  private String template2;
  @SerializedName("template_3")
  @ColumnInfo(name = "template3")
  private String template3;

  @SerializedName("RoleID")
  @ColumnInfo(name = "RoleID")
  private int RoleID;


  @SerializedName("RoleName")
  @ColumnInfo(name = "RoleName")
  private String RoleName;

  @Expose
  @SerializedName("relay")
  private String relay;
  @Expose
  @SerializedName("door")
  private String door;
  @Expose
  @SerializedName("ip_addr")
  private String ip_addr;
  @Expose
  @SerializedName("aes_key")
  private String aes_key;
  @Expose
  @SerializedName("hash_key")
  private String hash_key;
  /*@Expose
  @SerializedName("zone")
  private String zone;*/
   /* @Expose
    @SerializedName("name")
    private String name;*/
    /*@Expose
    @SerializedName("_id")
    private String _id; //next*/
    /*@Expose
    @SerializedName("terminal_id")
    private int terminal_id;*/
   /* @Expose
    @SerializedName("controller_id")
    private String controller_id;*/
  /*  @Expose
    @SerializedName("group_id")
    private String group_id;*/
  /*  @Expose
    @SerializedName("location_id")
    private int location_id;*/
  @Expose
  @SerializedName("Encoded_data")
  private String Encoded_data;
        /*@Expose
        @SerializedName("Role_id")
        private String Role_id;
        @Expose
        @SerializedName("role_name")
        private String role_name;
*/

//terminal_id, controller_id, group_id,location_id, Role_id,role_name,

  /*
      @SerializedName("Roleid_1")
      @ColumnInfo(name = "Roleid")
      private int Roleid;
      @SerializedName("Rolename_1")
      @ColumnInfo(name = "Rolename")
      private String Rolename;
  */
  @Expose
  @SerializedName("TerminalID")
  private int TerminalID;

  @Expose
  @SerializedName("LocationID")
  private int LocationID;
  @Expose
  @SerializedName("location")
  private String location;



  @Expose
  @SerializedName("CheckInOut")
  private int CheckInOut;


  @SerializedName("userImage")
  @ColumnInfo(name = "userImage")
  private String userImage;

  @SerializedName("{UserPHoto")
  @ColumnInfo(name = "UserPHoto")
  private String UserPHoto;

//
  @SerializedName("EmiratesDocumentID")
  @ColumnInfo(name = "EmiratesDocumentID")
  private String EmiratesDocumentID;

  @SerializedName("NationalityEnglish")
  @ColumnInfo(name = "NationalityEnglish")
  private String NationalityEnglish;

  @SerializedName("NationalityLocalLanguage")
  @ColumnInfo(name = "NationalityLocalLanguage")
  private String NationalityLocalLanguage;

  @SerializedName("NameLocalLanguage")
  @ColumnInfo(name = "NameLocalLanguage")
  private String NameLocalLanguage;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getTemplate1() {
    return template1;
  }

  public void setTemplate1(String template1) {
    this.template1 = template1;
  }

  public String getTemplate2() {
    return template2;
  }

  public void setTemplate2(String template2) {
    this.template2 = template2;
  }

  public String getTemplate3() {
    return template3;
  }

  public void setTemplate3(String template3) {
    this.template3 = template3;
  }
  public int getRoleID() {
    return RoleID;
  }

  public void setRoleID(int RoleID) {
    this.RoleID = RoleID;
  }


  /*public int getRoleID() {
       return Roleid;
   }
   public void setRoleID(int Role_id) {
       this.Roleid = Role_id;
   }*/
  public String getRoleName() {
    return RoleName;
  }
  public void setRoleName(String RoleName) {
    this.RoleName = RoleName;
  }

  public String getRelay() {
    return relay;
  }

  public void setRelay(String relay) {
    this.relay = relay;
  }

  public String getDoor() {
    return door;
  }

  public void setDoor(String door) {
    this.door = door;
  }

  public String getIp_addr() {
    return ip_addr;
  }

  public void setIp_addr(String ip_addr) {
    this.ip_addr = ip_addr;
  }

  public String getAes_key() {
    return aes_key;
  }

  public void setAes_key(String aes_key) {
    this.aes_key = aes_key;
  }

  public String getHash_key() {
    return hash_key;
  }

  public void setHash_key(String hash_key) {
    this.hash_key = hash_key;
  }

/*
    public int getTerminalID() {
        return terminal_id;
    }

    public void setTerminalID(int terminal_id) {
        this.terminal_id = terminal_id;
    }
*/

/*
    public String getControllerID() {
        return controller_id;
    }

    public void setControllerID(String controller_id) {
        this.controller_id = controller_id;
    }
*/

  //public String getGroupID() {
  //  return group_id;
  //}

  //public void setGroupID(String  group_id) {
  //  this.group_id = group_id;
  //}

  public String getEncoded_data() {
    return Encoded_data;
  }

  public void setEncoded_data(String Encoded_data) {
    this.Encoded_data = Encoded_data;
  }
  /*

      public int getLocationID() {
          return location_id;
      }

      public void setLocationID(int location_id) {
          this.location_id = location_id;
      }
  */
  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public int getLocationID() {
    return LocationID;
  }

  public void setLocationID(int location_id) {
    this.LocationID = location_id;
  }

  public int getTerminalID() {
    return TerminalID;
  }

  public void setTerminalID(int terminal_id) {
    this.TerminalID = terminal_id;
  }


  public int getCheckInOut() {
    return CheckInOut;
  }

  public void setCheckInOut(int checkinout) {
    this.CheckInOut = checkinout;
  }


  public String getUserImage() {
    return userImage;
  }

  public void setUserImage(String templateface) {
    this.userImage = templateface;
  }


  public String getUserPHoto() {
    return UserPHoto;
  }

  public void setUserPHoto(String photo_face) {
    this.UserPHoto = photo_face;
  }

  public String getEmiratesDocumentID() {
    return EmiratesDocumentID;
  }

  public  void setEmiratesDocumentID(String emiatesdocid) {
    EmiratesDocumentID = emiatesdocid;
  }

  public String getNationalityEnglish() {
    return NationalityEnglish;
  }

  public  void setNationalityEnglish(String arabicName) {
    NationalityEnglish = arabicName;
  }

  public String getNationalityLocalLanguage() {
    return NationalityLocalLanguage;
  }

  public  void setNationalityLocalLanguage(String arabicName) {
    NationalityLocalLanguage = arabicName;
  }

  public String getNameLocalLanguage() {
    return NameLocalLanguage;
  }

  public  void setNameLocalLanguage(String arabicName) {
    NameLocalLanguage = arabicName;
  }

  public User() {
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this._id);
    dest.writeString(this.userId);
    dest.writeString(this.username);
    dest.writeString(this.password);
    dest.writeString(this.fullname);
    dest.writeString(this.email);
    dest.writeString(this.accessToken);
    dest.writeString(this.status);
    dest.writeString(this.template1);
    dest.writeString(this.template2);
    dest.writeString(this.template3);
    dest.writeInt(this.RoleID);


    //   dest.writeInt(this.Roleid);
    dest.writeString(this.RoleName);


    dest.writeString(this.relay);
    dest.writeString(this.door);
    dest.writeString(this.ip_addr);
    dest.writeString(this.aes_key);
    dest.writeString(this.hash_key);
    //dest.writeString(this.zone);
    //dest.writeString(this.name);
    //dest.writeString(this._id);
    //dest.writeInt(this.terminal_id);
    //dest.writeString(this.controller_id);
//        dest.writeString(this.group_id);
    //dest.writeInt(this.location_id);
    dest.writeInt(this.LocationID);
    dest.writeString(this.location);
    dest.writeString(this.Encoded_data);
    dest.writeInt(this.TerminalID);
    dest.writeInt(this.CheckInOut);
    dest.writeString(this.EmiratesDocumentID);
    dest.writeString(this.NationalityEnglish);
    dest.writeString(this.NationalityLocalLanguage);
    dest.writeString(this.NameLocalLanguage);

    dest.writeString(this.userImage);
    dest.writeString(this.UserPHoto);
  }

  protected User(Parcel in) {
    this._id = in.readString();
    this.userId = in.readString();
    this.username = in.readString();
    this.password = in.readString();
    this.fullname = in.readString();
    this.email = in.readString();
    this.accessToken = in.readString();
    this.status = in.readString();
    this.template1 = in.readString();
    this.template2 = in.readString();
    this.template3 = in.readString();

    this.RoleID = in.readInt();

    //this.Roleid= in.readInt();
    this.RoleName= in.readString();


    this.relay = in.readString();
    this.door = in.readString();
    this.ip_addr = in.readString();
    this.aes_key = in.readString();
    this.hash_key = in.readString();
    //this.zone = in.readString();
    // this.name = in.readString();
    //  this._id = in.readString();
    // this.terminal_id = in.readInt();
    // this.controller_id= in.readString();
    //      this.group_id= in.readString();
    // this.location_id= in.readInt();
    this.LocationID= in.readInt();

    this.location= in.readString();
    this.Encoded_data= in.readString();
    this.TerminalID = in.readInt();
    this.CheckInOut = in.readInt();
     this.EmiratesDocumentID=in.readString();
     this.NationalityEnglish=in.readString();
     this.NationalityLocalLanguage=in.readString();
    this.NameLocalLanguage=in.readString();
    this.userImage = in.readString();
    this.UserPHoto = in.readString();
  }

  public static final Creator<User> CREATOR = new Creator<User>() {
    @Override
    public User createFromParcel(Parcel source) {
      return new User(source);
    }

    @Override
    public User[] newArray(int size) {
      return new User[size];
    }
  };
}
