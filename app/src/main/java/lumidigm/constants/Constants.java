package lumidigm.constants;

/**
 * Created by colors on 8/10/18.
 */

public class Constants {
    public static final String BASE_URL="http://digit9.sismatik.com/emirates_api/saveandroid.php?";
    public static final String DEVICE_INFO=BASE_URL+"devmodel=Philips&serialnum=32568&devversion=12&devuid=6&timestamp=12032018";

    //For Global Server - Secure
     //public static final String SERVER_IPADDR="http://serviceme.blynksystems.com";
    public static final String SERVER_IPADDR="http://139.59.10.127"; //"http://13.55.135.106";//"http://139.59.10.127"; //"http://192.168.43.161"; //"http://139.59.10.127"; //192.168.3.7";

    public static final String SERVER_IPADDR_SECURE="https://serviceme.blynksystems.com";

    //For Chinmay
 //   public static final String SERVER_IPADDR="http://192.168.2.2";
 //   public static final String SERVER_IPADDR_SECURE="https://192.168.2.2";

//For SriRam
   // public static final String SERVER_IPADDR="http://192.168.0.7";
    //public static final String SERVER_IPADDR_SECURE="https://192.168.0.7";
    //public static final String SERVER_IPADDR="http://192.168.1.21";
    //public static final String SERVER_IPADDR_SECURE="https://192.168.1.21";

    //public static final String SERVER_IPADDR="http://192.168.2.2";
    //public static final String SERVER_IPADDR_SECURE="https://192.168.2.2";

    //bilal
    //public static final String SERVER_IPADDR="http://192.168.2.144";
   // public static final String SERVER_IPADDR_SECURE="https://192.168.2.144";

    //public static final String SERVER_IPADDR="http://192.168.0.15";
    //public static final String SERVER_IPADDR_SECURE="https://192.168.0.15";

    //For Local Server
    //public static final String SERVER_IPADDR="http://161.44.100.22";
    //public static final String SERVER_IPADDR_SECURE=SERVER_IPADDR;

    public static final String   CREATE_USER=":8114/user_enroll/create_user";
    public static final String  USER_LOGIN =":8114/user_enroll/login_user";
    public static final String   ALL_TASKS   =":8114/user_enroll/get_assigned_tasks/";
    public static final String    TASK_STATUS =":8114/user_enroll/task_status";

    public static final String LOGOUT=SERVER_IPADDR+":8114/user_enroll/logout_user";
    public static final String GET_ASSIGNED_TASKS   =":8114/user_enroll/get_assigned_tasks/";

   // public static final String GET_ALL_USERS=":8114/user_enroll/get_users_frost5";

    public static final String GET_ALL_USERS=":8114/user_enroll/get_allusers";


    public static final String GET_PENDING_USERS=":8114/user_enroll/get_users_frost_pendingusers";
    public static final String GET_PENDING_VISITORS_AND_OTHERS=":8114/user_enroll/get_users_frost_pendingusers";


    //  public static final String GET_ALL_NON_VISITOR_USERS=":8114/user_enroll/get_allusers"; //get_allnonvisitorusers";
    public static final String CHECK_USER = ":8114/user_enroll/check_user/";
    public static final String UPDATER_USERFINGERPRINT= ":8114/user_enroll/update_user_template";
    public static final String GET_ALL_ACTIVE_VISITORS_AND_OTHERS=":8114/user_enroll/get_allactiveusersandothers"; //get_allnonvisitorusers";
    public static final String GET_ALL_NON_VISITOR_USERS=GET_ALL_ACTIVE_VISITORS_AND_OTHERS; //get_allnonvisitorusers";
    public static final String GET_ACTIVE_DETAINEE_USERS=":8114/user_enroll/get_allactiveusersandothers";
    //public static final String   ARGUS_CONTROLLER   =SERVER_IPADDR_SECURE+"/nodejs/argus_controller.php";

    //public static final String   ARGUS_CONTROLLER_STATUS   =SERVER_IPADDR_SECURE+"/nodejs/argus_controller_status.php";

    public static final String UPDATE_PAXTON_ENROLLMENT= ":8114/user_enroll/inserttoken_enroll";
    //This API to be called before update insert token
    public static final String PAXTON_ACCESS_TOKEN= ":8114/user_enroll/paxget_accesstoken1";
    //public static final String SERVER_IPADDR_TEST="http://172.16.16.8:8077";

   // public static final String SERVER_IPADDR_TEST="http://192.168.0.8:8077";

    public static final String   ARGUS_CONTROLLER  =":8077/argus_controller.php";

    public static final String   ARGUS_CONTROLLER_STATUS  =":8077/argus_controller_status.php";

    public static final String   ARGUS_CONTROLLER_STATUS_DECODE  =":8077/argus_controller_status_decode.php";

    //For Encoding
//    public static final String   ARGUS_CONTROLLER   ="/argus_controller.php";

  //  public static final String   ARGUS_CONTROLLER_STATUS   ="/argus_controller_status.php";

  //  public static final String   ARGUS_CONTROLLER_STATUS_DECODE   ="/argus_controller_status_decode.php";

    //For Logging
    public static final String WRITE_LOG=":8114/user_enroll/write_log";
    public static final String SSO_SERVER_CHECK_USER = ":8114/user_enroll/check_user/";
    public static final String SSO_SERVER_DEVICE_STATUS = ":8114/user_enroll/devicestatus1/";
    public static final String SSO_CHECK_USER_ID_ACTIVE_IN_LDAP = ":8114/user_enroll/check_userid_ldap/";
    public static final String LOG_SERVER = ":8114/user_enroll/logs";

    //Visitor API
    public static final String GET_VISITOR_USER_DETAILS=":8114/user_enroll/GetAllUsersVisitor";
    public static final String GET_VISITOR_DETAILS=":8114/user_enroll/GetVisitorDetails";
    public static final String GET_VISITORBOOTHS_DETAILS=":8114/user_enroll/GetVisitorBoothDetails";
    public static final String GET_ALLVISITORBOOTHS_DETAILS=":8114/user_enroll/GetAllVisitorBoothDetails";
    public static final String UPDATE_VISITORBOOTHSTATUS=":8114/user_enroll/updatevisitorstatus";
    public static final String UPDATE_VISITORSTARTEXIT=":8114/user_enroll/updatevisitorstartexit";

    //Detainee Details
    public static final String GET_DETAINEE_DETAILS=":8114/user_enroll/getdetaineedetails";

    public static final String GET_DETAINEE_BOOTH_DETAILS=":8114/user_enroll/getdetaineeboothdetails";
    public static final String GET_ALLDETAINEE_DETAILS=":8114/user_enroll/getalldetaineedetails";
    public static final String GET_ALLDETAINEEBOOTH_DETAILS=":8114/user_enroll/getalldetaineeboothdetails";
    public static final String GET_ALLDETAINEEBOOTH_DETAILS_WITHESCORTTASK=":8114/user_enroll/getalldetaineeboothdetails";
    public static final String GET_ALLDETAINEEBOOTH_DETAILS_WITHESCORTTASKASSIGNEDREVERSE=":8114/user_enroll/getallassigneddetaineeboothdetailsreverse";
    public static final String GET_ALLDETAINEEBOOTH_DETAILS_FOR_BOOTH=":8114/user_enroll/getalldetaineeboothdetailsforbooth";
    public static final String GET_ALLASSIGNEDDETAINEEBOOTHDETAILS=":8114/user_enroll/getallassigneddetaineeboothdetails";



    public static final String UPDATE_SELECT_DETAINEE_TASK=":8114/user_enroll/updateselectdetaineetask";
    public static final String UPDATE_VISIT_CONFIRMATION=":8114/user_enroll/updatevisitconformation";

    public static final String UPDATE_DETAINEEBOOTHSTARTEXIT=":8114/user_enroll/updatedetaineestartexit";


    public static final String UPDATE_REVERSE_PATH_AT_BOOT_ACCEPTED=":8114/user_enroll/updatereversepathaccepted";

    public static final String UPDATE_REVERSE_PATH_AT_BLOCKLEVEL=":8114/user_enroll/updatereversepathblocklevel";

    public static final String UPDATE_REVERSE_PATH_STATUS_AT_BLOCKLEVEL=":8114/user_enroll/updatereversepathstatusblocklevel";



    public static final String GET_ALLUSERSOTHERTHANVISITORS=":8114/user_enroll/getallusersotherthanvisitors";
    //Detainee Check Points
    public static final String GET_ESCORT_TASK_LIST=":8114/user_enroll/getescortdetails";
    public static final String GET_ALLESCORT_TASK_LIST=":8114/user_enroll/getallescortdetails";
    public static final String UPDATE_ESCORT_TASK=":8114/user_enroll/updatescortdetails";

    public static final String UPDATE_ENROLL_USER_GROUP=":8114/user_enroll/updateenrollusergroup";
    public static final String UPDATE_BOOTH_STATUS=":8114/user_enroll/updatevisitconformation";
    public static final String UPDATE_ENROLLREGISTERUSERS=":8114/user_enroll/updateenrollregisterusers";

    public static final String GET_BLOCK_GUARD_TASK_LIST=":8114/user_enroll/getguarddetails";
    public static final String GET_ALLBLOCK_GUARD_TASK_LIST=":8114/user_enroll/getgalluarddetails";
    public static final String UPDATE_BLOCK_GUARD_TASK=":8114/user_enroll/updategetguarddetails";

    public static final String UPDATE_VISITOR_STATUS_ON_BOOTH_LEVEL=":8114/user_enroll/updatevisitorstatusonboothlevel";





    public static final String GET_DETAINNE_CHECKPOINTS=":8114/user_enroll/get_detaineecheckpointdetails";



    public static final String CHECKINSTATUS = ":8114/user_enroll/checkinout";

    public static final String GETBOOTHSTATUSDETAILS=":8114/user_enroll/getVisitorBoothStatusDetails";

    public static final String GET_VISITORSTATUS=":8114/user_enroll/GetVisitorStatus";
    public static final String SETVISITORSTATUS=":8114/user_enroll/SetVisitorStatus";

    public static final String GET_OUTSIDEGUARD_TERMINALID=":8114/user_enroll/GetOutSideGuardTerminalID";
    public static final String GET_INSIDEGUARD_TERMINALID=":8114/user_enroll/GetInSideGuardTerminalID";
    public static final String UPDATE_INSIDEGUARD_FINGERSTATUS=":8114/user_enroll/UpdateInSideGuardFingerStatus";
    public static final String GET_OUTSIDEGUARDFINGERSTATUS=":8114/user_enroll/GetOutSideGuardFingerStatus";

    public static final String UPDATE_INSIDEGUARDFINGERSTATUS1=":8114/user_enroll/UpdateInsideGuardFingerStatus1";
    public static final String UPDATE_INSIDEGUARDFINGERSTATUS2=":8114/user_enroll/UpdateInsideGuardFingerStatus2";

    public static final String GET_INSIDEGUARDFINGERSTATUS1=":8114/user_enroll/GetInsideGuardFingerStatus1";
    public static final String GET_INSIDEGUARDFINGERSTATUS2=":8114/user_enroll/GetInsideGuardFingerStatus2";

    public static final String GET_OUTSIDE_ESCORTFINGERSTATUS=":8114/user_enroll/GetOutSide_EscortFingerStatus";
    public static final String UPDATE_OUTSIDE_ESCORTFINGERSTATUS=":8114/user_enroll/UpdateOutSide_EscortFingerStatus";

    public static final String UPDATE_INSIDE_DETAINEEFINGERSTATUS=":8114/user_enroll/UpdateInside_Detainee1FingerStatus";
    public static final String GET_INSIDE_DETAINEEFINGERSTATUS=":8114/user_enroll/GetInside_Detinee1FingerStatus";

    public static final String UPDATE_INSIDE_DETAINEEFINGERSTATUS2=":8114/user_enroll/UpdateInside_Detainee2FingerStatus";
    public static final String GET_INSIDE_DETAINEEFINGERSTATUS2=":8114/user_enroll/GetInside_Detinee2FingerStatus";

    public static final String UPDATE_INSIDE_DETAINEEFINGERSTATUS3=":8114/user_enroll/UpdateInside_Detainee3FingerStatus";
    public static final String GET_INSIDE_DETAINEEFINGERSTATUS3=":8114/user_enroll/GetInside_Detinee3FingerStatus";

    public static final String CLEAR_OUTSIDEGAURD=":8114/user_enroll/Clear_OutsideStatus";
    public static final String CLEAR_INSIDEGAURD=":8114/user_enroll/Clear_InsideStatus";

    public static final String GET_INSIDE_GUARDSTATUS=":8114/user_enroll/GetInsideDeviceStatus";
    //This has combination of Outside and Inside devices status mainted by the Inside Device
    //Outside device shall get the TerminalID of the Inside Device and update the Status to the Inside Device

    //OutSideEscortInitStatus, InsideGuardStatus1, InsideGuardStatus2, OutsideEscortSecondStatus,
    //InsideDetainee1Status, InsideDetainee2Status, InsideDetainee3Status, InsideDetaineeNumberAvailable

            /*
    http://192.168.3.7:8114/user_enroll/get_detaineecheckpoint
    String DetaineeVisitorGuard, String EscortUserID1, String EscortUserID2, integer DetaineeIDCount, Array of String DetaineeUserID, String Date


    Combination I:
    DetaineeVisitorGuard - UserID
            EscortUserID1
    DetaineeIDCount
            DetaineeUserID


    Combination 2:
    EscortUserID1
            EscortUserID2
    DetaineeIDCount
            DetaineeUserID


    http://192.168.3.7:8114/user_enroll/get_detaineedetails/3
    String DetaineeUserID, String DetaineeUserName,String  Block,String Room, String Bed, String Status

    http://192.168.3.7:8114/user_enroll/getBoothStatusDetails
    _id, tokenID, boothNo , userID, userName, message , userImage,datetime


    http://192.168.3.7:8114/user_enroll/updateBoothStatus
    Post - tokenID, status (allow, deny)


    http://192.168.3.7:8114/user_enroll/GetVisitorStatus
            - userID, TokenID, userName, status(in / out(default))

    http://192.168.3.7:8114/user_enroll/SetVisitorStatus
    Post userID, status (in/out)
*/
}
