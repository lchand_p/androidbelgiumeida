package lumidigm.services;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import lumidigm.network.AppController;
import lumidigm.network.IResult;
import lumidigm.network.NukeSSLCerts;

public class VolleyService {

    IResult mResultCallback = null;


    Context mContext;

    public VolleyService(IResult resultCallback, Context context) {

        mResultCallback = resultCallback;
        mContext = context;
    }


    public void DataProvide(int method, String url, JSONObject json, final String requestType) {
        NukeSSLCerts.nuke();
        JsonObjectRequest objectRequest = new JsonObjectRequest(method, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (mResultCallback != null)
                    mResultCallback.notifySuccess(requestType, response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mResultCallback != null)
                    mResultCallback.notifyError(requestType, error);
            }
        });

        int sockettime = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(sockettime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objectRequest.setRetryPolicy(policy);
        AppController.getInstance().getRequestQueue().getCache().remove(url);
        objectRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(objectRequest);


    }

    public void tokenBase(int method, String url, JSONObject json, final String requestType) {
        NukeSSLCerts.nuke();
        JsonObjectRequest objectRequest = new JsonObjectRequest(method, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (mResultCallback != null)
                    mResultCallback.notifySuccess(requestType, response);


            }

        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mResultCallback != null)
                    mResultCallback.notifyError(requestType, error);
            }
        }) {

            /**
             * Passing some request headers**/


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };

        int sockettime = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(sockettime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objectRequest.setRetryPolicy(policy);
        //AppController.getInstance().getRequestQueue().getCache().remove(url);
        // objectRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(objectRequest);
    }


    public void tokenBaseA(int method, String url, JSONObject json, final String requestType) {
        NukeSSLCerts.nuke();
        JsonObjectRequest objectRequest = new JsonObjectRequest(method, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (mResultCallback != null)
                    mResultCallback.notifySuccess(requestType, response);


            }

        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mResultCallback != null)
                    mResultCallback.notifyError(requestType, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                // headers.put("consumer_key","ck_e9a6ed5e326f8d5f3597a78230eb0a8a49693420");
                // headers.put("consumer_secret","cs_7d3c9f90fef71b921921fcd66fac95e61789ec63");

                // add headers <key,value>
                String credentials = "ck_e9a6ed5e326f8d5f3597a78230eb0a8a49693420"+":"+"cs_7d3c9f90fef71b921921fcd66fac95e61789ec63";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(),
                        Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }
        };
        int sockettime = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(sockettime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objectRequest.setRetryPolicy(policy);
        AppController.getInstance().getRequestQueue().getCache().remove(url);
        objectRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(objectRequest);
    }


    public void tokenBaseB(int method, String url, JSONArray json, final String requestType) {
        NukeSSLCerts.nuke();

        JsonArrayRequest objectRequest = new JsonArrayRequest(method, url, json, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (mResultCallback != null)
                    mResultCallback.notifySuccess(requestType, response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mResultCallback != null)
                    mResultCallback.notifyError(requestType, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                // headers.put("consumer_key","ck_e9a6ed5e326f8d5f3597a78230eb0a8a49693420");
                // headers.put("consumer_secret","cs_7d3c9f90fef71b921921fcd66fac95e61789ec63");

                // add headers <key,value>
                String credentials = "ck_e9a6ed5e326f8d5f3597a78230eb0a8a49693420"+":"+"cs_7d3c9f90fef71b921921fcd66fac95e61789ec63";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(),
                        Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };
        int sockettime = 10000;
        RetryPolicy policy = new DefaultRetryPolicy(sockettime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objectRequest.setRetryPolicy(policy);
        AppController.getInstance().getRequestQueue().getCache().remove(url);
        objectRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(objectRequest);
    }
    public void tokenBaseC(int method, String url, JSONArray json, final String requestType) {
        NukeSSLCerts.nuke();
        MetaRequest metaRequest = new MetaRequest( method,  url,json, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (mResultCallback != null)
                    mResultCallback.notifySuccess(requestType, response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mResultCallback != null)
                    mResultCallback.notifyError(requestType, error);
            }
        }){

            /**
             * Passing some request headers**/


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();

                headers.put("consumer_key","ck_e9a6ed5e326f8d5f3597a78230eb0a8a49693420");
                headers.put("consumer_secret","cs_7d3c9f90fef71b921921fcd66fac95e61789ec63");
                return headers;
            }
        };

        int sockettime = 10000;
        RetryPolicy policy = new DefaultRetryPolicy(sockettime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        metaRequest.setRetryPolicy(policy);
        AppController.getInstance().getRequestQueue().getCache().remove(url);
        AppController.getInstance().addToRequestQueue(metaRequest);
    }
    public void tokenBaseS(int method, String url,  final String requestType) {
        NukeSSLCerts.nuke();
        StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (mResultCallback != null)
                    mResultCallback.notifySuccess(requestType, response);


            }

        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mResultCallback != null)
                    mResultCallback.notifyError(requestType, error);
            }
        }) {

            /**
             * Passing some request headers**/


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        int sockettime = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(sockettime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        AppController.getInstance().getRequestQueue().getCache().remove(url);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0){
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkClientTrusted", e.toString());
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0){
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkServerTrusted", e.toString());
                        }
                    }
                }
        };
    }

}



