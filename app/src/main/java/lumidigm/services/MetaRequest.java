package lumidigm.services;

import android.annotation.SuppressLint;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;


public class MetaRequest extends JsonArrayRequest {
    public MetaRequest(int method, String url, JSONArray jsonRequest, Response.Listener
            <JSONArray> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public MetaRequest(String url, Response.Listener<JSONArray>
            listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @SuppressLint("NewApi")
    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            JSONArray jsonResponse = new JSONArray(jsonString);
            jsonResponse.put(Integer.parseInt("headers"), new JSONArray(response.headers));
            return Response.success(jsonResponse,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }
}
