package entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by husaynhakeem on 6/12/17.
 */

@Entity
public class Fingerentity {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public Fingerentity(String hoomanId) {
        this.hoomanId = hoomanId;
    }

    public String getHoomanId() {
        return hoomanId;
    }

    public void setHoomanId(String hoomanId) {
        this.hoomanId = hoomanId;
    }

    @ColumnInfo(name = "fingerdata")
    public String hoomanId;
}
