package digit9home.apps.manager;


import lumidigm.captureandmatch.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class AppLauncherActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_app_launcher);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.app_launcher, menu);
		return true;
	}

}
